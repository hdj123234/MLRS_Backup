
#pragma once
#ifndef HEADER_INTERFACE_FRAMEWORK
#define HEADER_INTERFACE_FRAMEWORK

#include"..\Interface_Global.h"

//전방선언
class IGameObjectManager;


class IFramework {
public:
	virtual ~IFramework() = 0 {}

	virtual bool getReady()=0;
	virtual bool advanceFrame() = 0;
	virtual void processMessage(IMsg*) = 0;

	virtual const bool isReady()const = 0;
};

class RIFramework_DivideFrameUnit {
public:
	virtual ~RIFramework_DivideFrameUnit() = 0 {}

	//순서 제어를 위해 한 프레임의 처리를 각각의 유닛으로 분할
	virtual float advanceFrameUnit_Tick() = 0;
	virtual void advanceFrameUnit_Calculate(const float fElapsedTime) = 0;
	virtual void advanceFrameUnit_Render() = 0;
	virtual bool advanceFrameUnit_PostProcessing() = 0;
};
class RIFramework_ObjectManagerGetter {
public:
	virtual ~RIFramework_ObjectManagerGetter() = 0 {}

	virtual IGameObjectManager * getObjectManager() = 0;
};

#endif