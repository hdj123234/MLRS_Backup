
#pragma once
#ifndef HEADER_GAMEFRAMEWORK
#define HEADER_GAMEFRAMEWORK
#include"Interface_Framework.h"

#ifndef HEADER_STL
#include<vector>
#include<array>
#include<map>
#include<chrono>
#include<thread>
#include<sstream>
#include<fstream>
#endif

//전방선언
class IGameWorld;
class RIGameWorld_ObjectManagerGetter;
class IGameWorldController;
class IRenderer;
class IGameTimer;
class IAdapter_Msg_WinToFramework;
class IMsg_CS;
class CWindow;

enum Retval_GameWorld : ENUMTYPE_256;



//----------------------------------- Struct -------------------------------------------------------------------------------------
//----------------------------------- Message -------------------------------------------------------------------------------------
class CMsgGame : virtual public IMsg {
public:
	virtual ~CMsgGame() {}
};
//----------------------------------- Creator -------------------------------------------------------------------------------------
class IFactory_GameFramework {
public:
	virtual ~IFactory_GameFramework() = 0 {}
	//virtual function
	virtual std::vector<IGameWorld *> createWorlds() = 0;
	virtual IGameWorldController * createWorldController() = 0;
//	virtual IRenderer* createPrinter() = 0;
	virtual IGameTimer* createTimer() = 0;
	virtual IAdapter_Msg_WinToFramework * createMsgAdapter()=0;
};
//----------------------------------- Class -------------------------------------------------------------------------------------

/*
	모든 월드는 RIGameWorld_ObjectManagerGetter 인터페이스를 상속받은 경우에만 정상동작
*/
class CGameFramework : virtual public IFramework,
						virtual public RIFramework_DivideFrameUnit,
						virtual public RIFramework_ObjectManagerGetter{
protected:
	CWindow * m_pWindow;

	std::vector<IGameWorld *> m_pWorlds;
	IGameWorldController * m_pWorldController;
//	IRenderer *m_pPrinter;
	IGameTimer *m_pTimer;
	IAdapter_Msg_WinToFramework * m_pMsgAdapter;

	IGameWorld * m_pCurrentWorld;
	RIGameWorld_ObjectManagerGetter * m_pCurrentWorld_ObjManagerGetter;
	bool m_bReady;
public:
	CGameFramework();
	virtual ~CGameFramework() { CGameFramework::release(); }

	//override IFramework
	virtual bool getReady();
	virtual bool advanceFrame();
	virtual void processMessage(IMsg *);
	virtual const bool isReady()const { return m_bReady; }

	//override RIFramework_DivideFrameUnit
	virtual float advanceFrameUnit_Tick() ;
	virtual void advanceFrameUnit_Calculate(const float fElapsedTime) ;
	virtual void advanceFrameUnit_Render() ;
	virtual bool advanceFrameUnit_PostProcessing() ;

	//override RIFramework_ObjectManagerGetter
	IGameObjectManager * getObjectManager() ;

	bool createObject(IFactory_GameFramework *pFactory);
	void release();

	void setWindow(CWindow *pWindow) { m_pWindow = pWindow; }
};

#endif