#pragma once

#ifndef HEADER_MSG_FRAMEWORK
#define HEADER_MSG_FRAMEWORK
#include"..\Interface_Global.h"

class IMsg_Framework : virtual public IMsg {
public:
	virtual ~IMsg_Framework() = 0 {}

};

class CMsg_Exit : virtual public IMsg_Framework {
public:
	virtual ~CMsg_Exit()  {}

};


class IAdapter_Msg_WinToFramework : virtual public IAdapter<IMsg_Framework, IMsg>{
public:
	virtual ~IAdapter_Msg_WinToFramework() = 0 {}
};
#endif