#pragma once
#include "stdafx.h"
#include "GameFramework.h"

#include"Msg_Framework.h"
#include"..\Module_Network\Interface_Network.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"
#include"..\Module_Renderer\Interface_Renderer.h"
#include"..\Module_Timer\Interface_Timer.h"
#include"..\Module_World\Interface_World.h"
#include"..\Module_Platform_Windows\Platform_Window.h"
#include"..\Module_World\GameWorld.h"
#include"..\Module_MLRS\GameObjectManager_MLRS_Lobby.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif

#ifndef HEADER_STL
#include<iomanip>
#endif
//----------------------------------- CGameFramework -------------------------------------------------------------------------------------

CGameFramework::CGameFramework()
	:m_pWorldController(nullptr),
//	m_pPrinter(nullptr), 
	m_pTimer(nullptr), 
	m_pMsgAdapter(nullptr),
	m_pCurrentWorld(nullptr),
	m_pCurrentWorld_ObjManagerGetter(nullptr),
	m_bReady(false)
{
}

IGameObjectManager * CGameFramework::getObjectManager()
{
	if (m_pCurrentWorld_ObjManagerGetter)
		return m_pCurrentWorld_ObjManagerGetter->getObjectManager();
	else
		return nullptr;
}

bool CGameFramework::createObject(IFactory_GameFramework * pFactory)
{
	if (!pFactory)
	{
		MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CGameFramework.createObject(), pFactory Emtpy");
		return false;
	}

//	m_pPrinter = pFactory->createPrinter();
	m_pTimer = pFactory->createTimer();
	m_pWorlds = pFactory->createWorlds();
	m_pWorldController = pFactory->createWorldController();
	m_pMsgAdapter = pFactory->createMsgAdapter();
//	if (m_pWorlds.empty() || !m_pTimer || !m_pPrinter||!m_pWorldController)
	if (m_pWorlds.empty() || !m_pTimer || !m_pWorldController)
	{
		MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CGameFramework.createObject(), pWorld or pTimer or pPrinter Emtpy");
		return false;
	}
	return true;
}

void CGameFramework::release()
{
//	if (m_pPrinter)
//		delete m_pPrinter;
//	m_pPrinter = nullptr;

	if (m_pTimer)
		delete m_pTimer;
	m_pTimer = nullptr;

	for (IGameWorld* pWorld : m_pWorlds)
	{
		if (pWorld)
			delete pWorld;
	}
	m_pWorlds.clear();

	if (m_pMsgAdapter)
		delete m_pMsgAdapter;
	m_pMsgAdapter = nullptr;

	if (m_pWorldController)
		delete m_pWorldController;
	m_pWorldController = nullptr;

	m_pCurrentWorld = nullptr;
	m_bReady = false;
}

bool CGameFramework::getReady()
{
//	if (m_pWorlds.empty() || !m_pPrinter || !m_pTimer || !m_pWorldController)
	if (m_pWorlds.empty() || !m_pTimer || !m_pWorldController)
		return false;
	m_pCurrentWorld = m_pWorlds[0];
	m_pCurrentWorld_ObjManagerGetter = dynamic_cast<RIGameWorld_ObjectManagerGetter *>(m_pCurrentWorld);
	m_pTimer->getReady();
	m_bReady = true;
	return true;
}
bool CGameFramework::advanceFrame()
{
	if (!m_bReady)
		return false;

	float fElapsedTime;
	if ((fElapsedTime = CGameFramework::advanceFrameUnit_Tick()) < 0)
		return true;

	CGameFramework::advanceFrameUnit_Calculate(fElapsedTime);
	CGameFramework::advanceFrameUnit_Render();
	return CGameFramework::advanceFrameUnit_PostProcessing();
}

void CGameFramework::processMessage(IMsg* pMsg)
{
	if (!m_bReady)
	{
		delete pMsg;
		return;
	}
	IMsg_Framework *pMsg_Framework = m_pMsgAdapter->adapt(pMsg);
	if(!pMsg_Framework)
		m_pCurrentWorld->processInputMessage(pMsg);
	else
	{
		if (dynamic_cast<CMsg_Exit*>(pMsg_Framework))
			m_bReady = false;
		delete pMsg_Framework;
	}
}

float CGameFramework::advanceFrameUnit_Tick()
{
	return m_pTimer->tick();
}

void CGameFramework::advanceFrameUnit_Calculate(const float fElapsedTime)
{
	m_pCurrentWorld->advanceFrame(fElapsedTime);
}

void CGameFramework::advanceFrameUnit_Render()
{
	m_pCurrentWorld->renderWorld();
//	m_pPrinter->drawScene(m_pCurrentWorld->getScene());
}

bool CGameFramework::advanceFrameUnit_PostProcessing()
{
	std::wstringstream oss;
	unsigned int nNextWorld = m_pWorldController->getNextWorld(m_pCurrentWorld);

	if (nNextWorld == 2)
	{
		m_pWindow->hideCursor();
		m_pWindow->lockFocus();
		CMsg_SC_ClientID * ptmp = new CMsg_SC_ClientID;
		ptmp->m_nID =  dynamic_cast<CGameObjectManager_MLRS_Lobby*>(dynamic_cast<CGameWorld*>(m_pCurrentWorld)->getObjectManager())->getClientID();
		m_pWorlds[nNextWorld]->processInputMessage(static_cast<CMsg_MLRS*>(ptmp));
	}
	if (nNextWorld ==0xffffffff)
		return false;
	else if(nNextWorld<m_pWorlds.size())
	{
		m_pCurrentWorld = m_pWorlds[nNextWorld];
		m_pCurrentWorld_ObjManagerGetter = dynamic_cast<RIGameWorld_ObjectManagerGetter *>(m_pCurrentWorld);
	}

	//<< std::setprecision(3)
	oss << "Title" << " (FPS : " << std::setprecision(3) << m_pTimer->getFPS() << ")";
	CGlobalWindow::changeTitle(oss.str());

	return true;
}
