#pragma once
#ifndef HEADER_ADAPTER_MSG_WINTOFRAMEWORK
#define HEADER_ADAPTER_MSG_WINTOFRAMEWORK

#include"..\Interface_Global.h"
#include"..\Module_Platform_Windows\Platform_Window.h"
#include"Msg_Framework.h"

class CAdapter_Msg_WinToFramework : virtual public IAdapter_Msg_WinToFramework
{
public:
	CAdapter_Msg_WinToFramework() {}
	~CAdapter_Msg_WinToFramework() {}

	IMsg_Framework* adapt(IMsg *pMsg)
	{

		CMsg_Windows *pWinMsg = dynamic_cast<CMsg_Windows *>(pMsg);
		if (!pWinMsg)	return nullptr;
		IMsg_Framework *pResult = nullptr;
		switch (pWinMsg->getMessage())
		{
		case WM_KEYDOWN:
			switch (pWinMsg->getWParam())
			{
			case VK_ESCAPE:
			case 'q':
			case 'Q':
				pResult = new CMsg_Exit();
				delete pMsg;
				break;
			}
		}
		return pResult;
	}
};

#endif