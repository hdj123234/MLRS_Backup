
#include "stdafx.h"
#include "GameWorld.h"

#include"..\Interface_Global.h"
#include"..\Module_Object\Interface_Object.h"
#include"..\Module_Scene\Interface_Scene.h"
#include"..\Module_Renderer\Interface_Renderer.h"

#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif




//----------------------------------- CGameWorld -------------------------------------------------------------------------------------


CGameWorld::CGameWorld()
	:m_pScene(nullptr),
	m_pRenderer(nullptr)
{
}
bool CGameWorld::createObject(IFactory_GameWorld * pFactory)
{

	if (!pFactory)
	{
		MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CGameWorld.createObject(), pFactory Emtpy");
		return false;
	}
	m_pObjects_Origin = pFactory->createObjects();
	m_pObjectManager = pFactory->createObjectManager();
	m_pScene = pFactory->createScene();
	m_pMsgAdapter = pFactory->createMsgAdapter();
	m_pRenderer = pFactory->createRenderer();
	if (!m_pObjectManager ||!m_pRenderer || !m_pScene|| m_pObjects_Origin.empty())
	{
		MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CGameWorld.createObject(), m_pScene or m_pObjectManager Emtpy");
		return false;
	}

	m_pObjectManager->setOriginData(&m_pObjects_Origin);
	m_pObjectManager->initScene(m_pScene);
	m_pRenderer->getReady();
	return true;
}
void CGameWorld::release()
{
	if (m_pScene)
		delete m_pScene;
	m_pScene = nullptr;

	for (auto &data : m_pObjects_Origin)
		delete data;
	m_pObjects_Origin.clear();

	for (auto &data : m_pMessages)
		delete data;
	m_pMessages.clear();

	if (m_pObjectManager)
		delete m_pObjectManager;
	m_pObjectManager = nullptr;

	if (m_pRenderer)
		delete m_pRenderer;
	m_pRenderer = nullptr;
}

void CGameWorld::advanceFrame(const float fElapsedTime)
{
	this->processBeforeAnimate();
	auto &rpMsgs = m_pObjectManager->animateObjects(fElapsedTime);
	m_pMessages.insert(m_pMessages.end(), rpMsgs.begin(), rpMsgs.end());
	this->processAfterAnimate();
}

void CGameWorld::processBeforeAnimate()
{
	m_pObjectManager->handleMessage(m_pMessages);
}

void CGameWorld::processAfterAnimate()
{
	m_pObjectManager->processCollision();
	m_pObjectManager->handleMessage(m_pMessages);
	m_pObjectManager->updateObjectGroup();
	m_pObjectManager->updateScene(m_pScene);
}
void CGameWorld::processInputMessage(IMsg * pInputMessage)
{
	IMsg_GameObject * pMessage = m_pMsgAdapter->adapt(pInputMessage);
	if (pMessage)
		m_pMessages.push_back(pMessage);
	else
		delete pInputMessage;
}

void CGameWorld::renderWorld()
{
	m_pRenderer->drawScene(m_pScene);
}

