
#pragma once

#ifndef HEADER_GAMEWORLDCONTROLLER
#define HEADER_GAMEWORLDCONTROLLER
#include"Interface_World.h"

class CGameWorldController_Dummy : public IGameWorldController {
public:
	virtual const unsigned int getNextWorld(IGameWorld *pWorld)
	{
		switch (pWorld->getWorldState())
		{
			return -1;
		case EState_GameWorld::eEnterRoom:
			return 1;
		case EState_GameWorld::eEnterGame:
			return 2;
		case EState_GameWorld::eEndGame:
			return 0xffffffff;
		case EState_GameWorld::eNone:
		default:
			return 0xfffffff0;
		}

	}
};

#endif