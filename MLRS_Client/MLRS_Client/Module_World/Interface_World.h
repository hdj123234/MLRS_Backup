
#pragma once

#ifndef HEADER_INTERFACE_WORLD
#define HEADER_INTERFACE_WORLD

#include"..\Interface_Global.h"

//���漱��
class IScene;
class IMsg;
class IGameObject;
class IMsg_GameObject;

enum EState_GameWorld : ENUMTYPE_256 {
	eNone,
	eInGameEnd, 
	eEnterRoom, 
	eOutRoom,
	eEnterGame,
	eEndGame,
};


class IGameObjectManager {
public:
	virtual ~IGameObjectManager() = 0 {}

	virtual void setOriginData(std::vector<IGameObject *> *pObjects_Origin) = 0;
	virtual std::vector<IMsg_GameObject *> animateObjects(const float fElapsedTime) = 0;
	virtual void handleMessage(std::vector<IMsg_GameObject *> &rpMsgs) = 0;
	virtual void processCollision() = 0;
	virtual void updateObjectGroup() = 0;
	virtual void updateScene(IScene * pScene) = 0;
	virtual void initScene(IScene * pScene) = 0;
	virtual std::vector<IMsg *> getReturnMsgs() = 0;
	virtual EState_GameWorld  getWorldState()=0;
};
class IGameWorld {
public:
	virtual ~IGameWorld() = 0 {}

	virtual void advanceFrame(const float fElapsedTime) = 0;
	virtual void processInputMessage(IMsg *pInputMessage) = 0;
	virtual void renderWorld() = 0;
//	virtual IScene * getScene() = 0;
	virtual EState_GameWorld getWorldState() = 0;
};

class RIGameWorld_ObjectManagerGetter {
public:
	virtual ~RIGameWorld_ObjectManagerGetter() = 0 {}

	virtual IGameObjectManager * getObjectManager() = 0;
};

class IGameWorldController {
public:
	virtual const unsigned int getNextWorld(IGameWorld *pWorld) = 0;
};

#endif