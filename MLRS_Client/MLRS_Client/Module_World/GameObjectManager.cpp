
#include "stdafx.h"
#include "GameObjectManager.h"

#include "..\Module_Scene\Interface_Scene.h"
#include "..\Module_Object\Interface_Object.h"


#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif

//----------------------------------- CGameObjectManager -------------------------------------------------------------------------------------

void CGameObjectManager::handleMessage_RealProcessing()
{
	while (!m_pMsgQueue.empty())
	{
		delete m_pMsgQueue.front();
		m_pMsgQueue.pop();
	}
}

CGameObjectManager::CGameObjectManager()
	:	m_bReady(false),
	m_eWorldState(EState_GameWorld::eNone)
{
}

void CGameObjectManager::addObject(IGameObject  * pObject)
{
	if (!m_bReady)		return;
	m_pObjects_Origin->push_back(pObject);

	this->addObject_AfterProcessing(pObject);
}

void  CGameObjectManager::deleteObject(IGameObject  * pObject)
{
	if (!m_bReady)		return;
	auto data_origin = std::find(m_pObjects_Origin->begin(), m_pObjects_Origin->end(), pObject);
	*data_origin = *(m_pObjects_Origin->rbegin());
	m_pObjects_Origin->pop_back();

	this->deleteObject_AfterProcessing(pObject);

	delete pObject;
}


void CGameObjectManager::setOriginData(std::vector<IGameObject *>* pObjects_Origin)
{
	if (!pObjects_Origin)		return;
	m_pObjects_Origin = pObjects_Origin;

	this->setOriginData_AfterProcessing();
	m_bReady = true;
}

void CGameObjectManager::handleMessage(std::vector<IMsg_GameObject*>& rpMsgs)
{
	if (!m_bReady)
	{
		for (auto data : rpMsgs)
			delete data;
		return;
	}
	for (auto data : rpMsgs)
		m_pMsgQueue.push(data);
	rpMsgs.clear();

	this->handleMessage_RealProcessing();
}

void  CGameObjectManager::updateScene(IScene * pScene)
{
	if (!m_bReady)		return;
	if (!pScene)
	{
		MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CGameObjectManager_MLRS_InGame.updateScene(), pGameScene Empty");
		return;
	}

	pScene->clear();
	this->updateScene_RealProcessing(pScene);

}

std::vector<IMsg*> CGameObjectManager::getReturnMsgs()
{
	std::vector<IMsg *> pReturnMsgs = m_pReturnMsgs;
	m_pReturnMsgs.clear();
	return pReturnMsgs;
}

void CGameObjectManager::releaseObject()
{
	if(!m_pMsgQueue.empty())
	{
		if (m_pMsgQueue.front()) delete m_pMsgQueue.front();
		m_pMsgQueue.pop();
	}
	for (auto data : m_pReturnMsgs)
		if (data) delete data;
}
