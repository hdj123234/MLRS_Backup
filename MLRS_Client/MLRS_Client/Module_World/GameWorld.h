#pragma once

#ifndef HEADER_GAMEWORLD
#define HEADER_GAMEWORLD
#include"Interface_World.h"

#ifndef HEADER_STL
#include<vector>
#endif

//���漱��
class IScene;
class IGameObject;
class IGameObjectManager;
class IAdapter_Msg_ToGame;
class IMsg_GameObject;
class IRenderer;


//----------------------------------- Creator -------------------------------------------------------------------------------------
class IFactory_GameWorld  {
public:
	//virtual function
	virtual std::vector<IGameObject *> createObjects() = 0;
	virtual IGameObjectManager * createObjectManager() = 0;
	virtual IScene* createScene() = 0;
	virtual IAdapter_Msg_ToGame* createMsgAdapter() = 0;
	virtual IRenderer* createRenderer() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------
class CGameWorld :	public IGameWorld,
					public RIGameWorld_ObjectManagerGetter{
protected:
	std::vector<IGameObject *> m_pObjects_Origin;
	IGameObjectManager *m_pObjectManager;
	std::vector<IMsg_GameObject *> m_pMessages;
	IScene *m_pScene;
	IAdapter_Msg_ToGame *m_pMsgAdapter;
	IRenderer *m_pRenderer;


	//override IGameWorld
	virtual void processBeforeAnimate();
	virtual void processAfterAnimate();
public:
	CGameWorld();
	virtual ~CGameWorld() { CGameWorld::release();  }



	//override IGameWorld
	virtual void advanceFrame(const float fElapsedTime);
	virtual void processInputMessage(IMsg *pInputMessage);
	virtual EState_GameWorld getWorldState() { return m_pObjectManager->getWorldState(); }
//	IScene * getScene() { return m_pScene; }
	virtual void renderWorld();

	//override RIGameWorld_ObjectManagerGetter
	IGameObjectManager * getObjectManager() { return m_pObjectManager; }

	bool createObject(IFactory_GameWorld *pFactory);
	void release();
};
#endif