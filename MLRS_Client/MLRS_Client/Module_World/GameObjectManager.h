#pragma once
#ifndef HEADER_GAMEOBJECTMANAGER
#define HEADER_GAMEOBJECTMANAGER

#include"Interface_World.h"


#ifndef HEADER_STL
#include<vector>
#include<set>
#include<map>
#include<iterator>
#include<algorithm>
#include<queue>
#endif

class IMsg_GameObject;
class ICamera;
class ISkyBox;
class ILightManager;
class ILight;

class CGameObjectManager : public IGameObjectManager {
protected:
	std::vector<IGameObject *> * m_pObjects_Origin;
	std::queue<IMsg_GameObject *> m_pMsgQueue;
	std::vector<IMsg *> m_pReturnMsgs;
	EState_GameWorld m_eWorldState;
	bool m_bReady;

public:
	CGameObjectManager();
	virtual ~CGameObjectManager() { CGameObjectManager::releaseObject(); }

private:
	//Virtual Func
	virtual void addObject_AfterProcessing(IGameObject *pObject) {}
	virtual void deleteObject_AfterProcessing(IGameObject *pObject) {}
	virtual void setOriginData_AfterProcessing() {}
	virtual void handleMessage_RealProcessing();
	virtual void updateScene_RealProcessing(IScene *pScene) {}

public:
	//override IGameObjectManager
	virtual void addObject(IGameObject *pObject);
	virtual void deleteObject(IGameObject *pObject);
	virtual void setOriginData(std::vector<IGameObject *> *pObjects_Origin);
	virtual void handleMessage(std::vector<IMsg_GameObject *> &rpMsgs);
	virtual void updateScene(IScene *pScene);
	virtual std::vector<IMsg *> getReturnMsgs();

	virtual void initScene(IScene * pScene) {}
	virtual std::vector<IMsg_GameObject *> animateObjects(const float fElapsedTime) { return std::vector<IMsg_GameObject *>(); }
	virtual void processCollision() {  }
	virtual void updateObjectGroup() {}
	virtual EState_GameWorld  getWorldState() { return m_eWorldState; }

	void releaseObject();

};


#endif