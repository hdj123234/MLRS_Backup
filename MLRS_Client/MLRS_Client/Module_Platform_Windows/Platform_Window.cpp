#include "stdafx.h"
#include "Platform_Window.h"

#include "..\Module_Framework\Interface_Framework.h"
#include"GlobalWindows.h"

#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#ifndef HEADER_WINDOWS

#include <windowsx.h>
#endif // !HEADER_WINDOWS





bool CWindow::s_bReady = false;
bool CWindow::s_bLockFocus=false;
bool CWindow::s_bLookFocus = false;


//----------------------------------- WindowData -------------------------------------------------------------------------------------

WindowData::WindowData(std::wstring wsTitle,DWORD dwWinStyle, UINT nWidth, UINT nHeight, UINT nPosX, UINT nPosY)
: m_wsTitle(wsTitle), m_dwWinStyle(dwWinStyle), m_nWidth(nWidth), m_nHeight(nHeight), m_nPosX(nPosX), m_nPosY(nPosY)
{}


//----------------------------------- CWindow -------------------------------------------------------------------------------------
 IFramework * CWindow::s_pFramework=nullptr;

 bool CWindow::createObject(IFactory_Window * pFactory)
 {
	 if (!pFactory)
	 {
		 MYLOGCAT(MYLOGCAT_ERROR_NORMAL, " CWindow.createWindow() ");
		 return false;
	 }
	 m_WinData = pFactory->getWindowData();
	 m_WinClass = pFactory->getWindowClass();

	 m_WinClass.lpfnWndProc = CWindow::wndProc;
	 m_WinClass.hInstance = m_hInst;

	 return true;
 }



 bool CWindow::getReady()
 {
	 if (!m_pFramework)
		 return false;
	 s_pFramework = m_pFramework;
	 return true;
 }

 bool CWindow::createWindow()
{
	RECT rect;

	SetRect(&rect, 0, 0, m_WinData.m_nWidth, m_WinData.m_nHeight);
	AdjustWindowRect(&rect, m_WinData.m_dwWinStyle, TRUE);

	RegisterClassEx(&m_WinClass);


	//윈도우 생성
	m_hWnd = CreateWindowEx(NULL,m_WinClass.lpszClassName, m_WinClass.lpszClassName,
		m_WinData.m_dwWinStyle, m_WinData.m_nPosX, m_WinData.m_nPosY, rect.right - rect.left, rect.bottom - rect.top,
		nullptr, nullptr, m_hInst, nullptr);


	if (!m_hWnd)
	{
		auto result = GetLastError();
		MYLOGCAT(MYLOGCAT_ERROR_NORMAL," CWindow.createObject(), CreateWindow() Error");
		return false;
	}
	
	ShowWindow(m_hWnd, SW_SHOW);


	return true;
}
int CWindow::run()
{
	MSG msg;
	if (!m_pFramework)
		return 0;

	m_pFramework->getReady();
	s_bReady = true;
	while (1)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT) break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if (!m_pFramework->advanceFrame())
 				PostQuitMessage(0);
		}
	}
	return (int)msg.wParam;
}


LRESULT CALLBACK CWindow::wndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int x, y;
	static bool checker;
	static int mx, my;

	static bool s_bLock = false;
	static bool s_bLock_Past = false;
	static bool s_bLockFocus_Past = false;
	switch (message)
	{
	case WM_CREATE:
		break;
	case WM_ACTIVATE:
		switch (wParam)
		{
		case WA_ACTIVE:
		case WA_CLICKACTIVE:
			s_bLock = s_bLock_Past;
			s_bLockFocus = s_bLockFocus_Past;
			if (!s_bLock)
			{
				SetCursor(NULL);
				ShowCursor(FALSE);
				int cx = CGlobalWindow::getWidth() / 2;
				int cy = CGlobalWindow::getHeight() / 2;
				SetCursorPos(CGlobalWindow::getLeft() + cx, CGlobalWindow::getTop() + cy);
				checker = true;
			}
			break;
		case WA_INACTIVE:
			s_bLock_Past = s_bLock;
			s_bLockFocus_Past  = s_bLockFocus;
			s_bLock = true;
			s_bLockFocus = false;
			SetCursor(NULL); 
			ShowCursor(TRUE);
			break;

		}
		break;
	case WM_CHAR:
	case WM_KEYDOWN:
	case WM_KEYUP:
		if (!s_bReady)
			return 0;
		if (wParam == 'z' || wParam == 'Z')
		{
			if (s_bLock&&!s_bLookFocus)
			{
				SetCursor(NULL);
				ShowCursor(FALSE);
				
			}
			else if(!s_bLock&&!s_bLookFocus)
			{
				SetCursor(NULL);
				ShowCursor(TRUE);
			}
			s_bLock = !s_bLock;
		}
		
		if (s_bLock)	return 0; 

		s_pFramework->processMessage(new CMsg_Windows(message, wParam, lParam));
		break;
	case WM_MOUSEWHEEL:
		if (s_bLock)	return 0;
		s_pFramework->processMessage(new CMsg_Windows(message, wParam, lParam));
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
		if (s_bLock)	return 0;
		s_pFramework->processMessage(new CMsgWindows_MouseClick(message, wParam, lParam));
		break;
	case WM_RBUTTONDOWN:
	case WM_MOUSEMOVE:
		if (!s_bReady)
			return 0;
		if (s_bLock)	return 0;
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);
		if (s_bLockFocus)
		{
			if (!checker)
			{
				int cx = CGlobalWindow::getWidth() / 2;
				int cy = CGlobalWindow::getHeight() / 2;
				s_pFramework->processMessage(new CMsgWindows_MouseMove(message, wParam, lParam, x - mx, y - my));
				SetCursorPos(CGlobalWindow::getLeft()+ cx, CGlobalWindow::getTop() + cy);
				checker = true;
			}
			else
				checker = false;
		}
		else
			s_pFramework->processMessage(new CMsgWindows_MouseMove(message, wParam, lParam, x - mx, y - my));
		mx = x;
		my = y;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void CWindow::hideCursor()
{
	SetCursor(NULL);
	ShowCursor(FALSE);
	s_bLookFocus = false;
}

void CWindow::unhideCursor()
{
	SetCursor(NULL);
	ShowCursor(TRUE);
	s_bLookFocus = true;
}
