#pragma once

#ifndef HEADER_FACTORY_WINDOW
#define HEADER_FACTORY_WINDOW
#include"..\Interface_Global.h"

#ifndef HEADER_WINDOWS
#include<Windows.h>
#endif // !HEADER_WINDOWS

#ifndef HEADER_STL
#include<string>
#endif // !HEADER_STL

#include"Platform_Window.h"

class CFactory_Window : public IFactory_Window{
private:
	WNDCLASSEX m_WinClass;
	WindowData m_WindowData;
public:
	CFactory_Window(const WNDCLASSEX &winClass, const WindowData &windowData)
		:m_WinClass(winClass),
		m_WindowData(windowData) {}
	virtual ~CFactory_Window() {}

	//override IFactory_Window
	virtual WNDCLASSEX const & getWindowClass()const { return m_WinClass; }
	virtual WindowData const & getWindowData()const { return m_WindowData; }
};

#endif