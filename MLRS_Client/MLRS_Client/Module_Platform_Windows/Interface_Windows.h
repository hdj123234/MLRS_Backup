#pragma once

#ifndef HEADER_MYINTERFACE_WINDOWS
#define HEADER_MYINTERFACE_WINDOWS


//���漱��
class IFramework;

class IWindow  {
public:
	virtual ~IWindow() = 0 {}

	virtual bool getReady() = 0;
	virtual bool createWindow() = 0;
	virtual void setFramework(IFramework *) = 0;
	virtual int run() = 0;
};

#endif