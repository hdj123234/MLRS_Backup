#pragma once

#ifndef HEADER_PLATFORM_WINDOW
#define HEADER_PLATFORM_WINDOW

#include"Interface_Windows.h"
#include"..\Interface_Global.h"

#ifndef HEADER_WINDOWS
#include<Windows.h>
#endif // !HEADER_WINDOWS

#ifndef HEADER_STL
#include<string>
#endif // !HEADER_STL

//���漱��
class IFramework;


//----------------------------------- Struct -------------------------------------------------------------------------------------
struct WindowData {
	std::wstring m_wsTitle;
	DWORD m_dwWinStyle;
	UINT m_nWidth;
	UINT m_nHeight;
	UINT m_nPosX;
	UINT m_nPosY;

	WindowData() {}
	WindowData(std::wstring wsTitle, DWORD dwWinStyle, UINT nWidth, UINT nHeight, UINT nPosX, UINT nPosY);
	virtual ~WindowData() {}
};

//----------------------------------- Message -------------------------------------------------------------------------------------

class CMsg_Windows : public IMsg {
private:
	UINT m_message;
	WPARAM m_wParam;
	LPARAM m_lParam;
public:
	CMsg_Windows(UINT message, WPARAM wParam, LPARAM lParam) :m_message(message), m_wParam(wParam), m_lParam(lParam) {}
	virtual ~CMsg_Windows() {}
	virtual UINT getMessage()const { return m_message; }
	virtual WPARAM getWParam()const { return m_wParam; }
	virtual LPARAM getLParam()const { return m_lParam; }
};


struct CMsgWindows_MouseMove : public CMsg_Windows {
	int m_nX;
	int m_nY;

	CMsgWindows_MouseMove(UINT message, WPARAM wParam, LPARAM lParam, int nx, int ny)
		:CMsg_Windows(message, wParam, lParam),
		m_nX(nx),
		m_nY(ny) {}
	virtual ~CMsgWindows_MouseMove() {}

	//override IMessage_Windows
	virtual int getX() { return m_nX; }
	virtual int getY() { return m_nY; }
};

struct CMsgWindows_MouseClick : public CMsg_Windows {
	CMsgWindows_MouseClick(UINT message, WPARAM wParam, LPARAM lParam)
		:CMsg_Windows(message, wParam, lParam){}
	virtual ~CMsgWindows_MouseClick() {}
};


//----------------------------------- Interface -------------------------------------------------------------------------------------
class IFactory_Window {
public:
	virtual ~IFactory_Window() = 0 {}

	virtual WNDCLASSEX const &getWindowClass()const	=0;
	virtual WindowData const &getWindowData()const		=0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------
//	 CWindow
class CWindow : public IWindow {
private:
	WNDCLASSEX m_WinClass;
	WindowData m_WinData;
	HINSTANCE m_hInst;
	HWND m_hWnd;
	IFramework * m_pFramework;

	static  IFramework * s_pFramework;
	static bool s_bReady;
	static bool s_bLockFocus;
	static bool s_bLookFocus;

public:
	CWindow(const HINSTANCE hInst) :m_hInst(hInst), m_pFramework(nullptr) {}
	virtual ~CWindow() {  }


	//override IWindow
	virtual bool getReady();
	virtual bool createWindow();
	virtual void setFramework(IFramework *pFramework) { m_pFramework = pFramework; }
	virtual int run();

	bool createObject(IFactory_Window * pFactory);

	const std::wstring & getTitle()const { return m_WinData.m_wsTitle;	} 
	HWND getWindowHandle() { return m_hWnd; }

	static LRESULT CALLBACK	wndProc(HWND, UINT, WPARAM, LPARAM);
	static void lockFocus() { s_bLockFocus = true; }
	static void unlockFocus() { s_bLockFocus = false; }
	static void hideCursor();
	static void unhideCursor();
};


//----------------------------------- Default -------------------------------------------------------------------------------------
struct WindowData_Default : public WindowData {
	WindowData_Default(std::wstring wsTitle = std::wstring(L"Title"), UINT nWidth = 800, UINT nHeight = 600)
		:WindowData(wsTitle, WS_OVERLAPPEDWINDOW , nWidth, nHeight, 100, 100)
	{
	}

};
struct WindowClass_Default : public WNDCLASSEX {
	WindowClass_Default(std::wstring wsTitle = std::wstring(L"WindowClass"), UINT nWidth = 800, UINT nHeight = 600)
		:WNDCLASSEX()
	{
		ZeroMemory(dynamic_cast<WNDCLASSEX*>(this), sizeof(WNDCLASSEX));

		WNDCLASSEX::cbSize = sizeof(WNDCLASSEX);
		WNDCLASSEX::style = CS_HREDRAW | CS_VREDRAW;// ;
		WNDCLASSEX::cbClsExtra = 0;
		WNDCLASSEX::cbWndExtra = 0;
		WNDCLASSEX::hIcon = nullptr;
		WNDCLASSEX::hCursor = LoadCursor(NULL, IDC_ARROW);
		WNDCLASSEX::hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		WNDCLASSEX::lpszMenuName = nullptr;
		WNDCLASSEX::lpszClassName = L"WindowClass";
		WNDCLASSEX::hIconSm = nullptr;
	}
};



#endif