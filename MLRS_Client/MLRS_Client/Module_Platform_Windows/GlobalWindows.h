#pragma once

#ifndef HEADER_GLOBALWINDOWS
#define HEADER_GLOBALWINDOWS

#include"Interface_Windows.h"
#include"..\Interface_Global.h"

#ifndef HEADER_WINDOWS
#include<Windows.h>
#endif // !HEADER_WINDOWS

#ifndef HEADER_STL
#include<string>
#endif // !HEADER_STL

//���漱��
class IFramework;


class CGlobalWindow  {
private:
	static HINSTANCE s_hInst;
	static HWND s_hWnd;
	static std::wstring s_wsTitle;
	static SYSTEM_INFO s_SysInfo;
public:
	static void setInterface(const HINSTANCE hInst, const HWND hWnd, const std::wstring &rwsTitle) { s_hInst = hInst; s_hWnd = hWnd; s_wsTitle = rwsTitle; }
	static void setInstance(const HINSTANCE hInst) { s_hInst = hInst; }
	static void setWindowHandle(const HWND hWnd) { s_hWnd = hWnd; }
	static void setTitle(const std::wstring &rwsTitle) { s_wsTitle = rwsTitle; }
	static void setInfo() { GetSystemInfo(&s_SysInfo); }

	static HWND getHWnd() { return s_hWnd; }
	static HINSTANCE getHInst() { return s_hInst; }
	static void getClientSize(int &width, int &height) { RECT rect; GetClientRect(s_hWnd, &rect); width = rect.right - rect.left; height = rect.bottom - rect.top; }
	static int getWidth() { RECT rect; GetClientRect(s_hWnd, &rect); return rect.right - rect.left; }
	static int getHeight() { RECT rect; GetClientRect(s_hWnd, &rect); return rect.bottom - rect.top; }
	static int getLeft() { RECT rect; GetWindowRect(s_hWnd, &rect); return rect.left; }
	static int getTop() { RECT rect; GetWindowRect(s_hWnd, &rect); return rect.top; }
	static std::wstring getTitle() { return s_wsTitle; }
	static float getAspectRatio() { return static_cast<float>(getWidth()) / getHeight(); }
	static void changeTitle(std::wstring const &rsTitle) { SetWindowText(s_hWnd, rsTitle.c_str()); }
	static UINT getNumberOfCore() { return s_SysInfo.dwNumberOfProcessors; }
};

#endif