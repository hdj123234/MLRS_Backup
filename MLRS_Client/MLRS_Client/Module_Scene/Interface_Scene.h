#pragma once

#ifndef HEADER_INTERFACE_SCENE
#define HEADER_INTERFACE_SCENE

class ISkyBox;

//���漱��
class RIGameObject_Drawable;
class RIGameObject_BeMust_RenewalForDraw;
class RIGameObject_BeMust_InitForDraw;

class IScene {
public:
	virtual ~IScene() = 0  {}

	virtual void setSkyBox(const ISkyBox* pSkyBox) = 0;
	virtual const ISkyBox* getSkyBox() = 0;

	virtual void addObject(const RIGameObject_Drawable *pObject) = 0;
	virtual std::vector<const RIGameObject_Drawable*> & getObjects() = 0;

	virtual void addObject_Renewal(const RIGameObject_BeMust_RenewalForDraw *pObject) = 0;
	virtual std::vector<const RIGameObject_BeMust_RenewalForDraw*> & getObjects_Renewal() = 0;

	virtual void addObject_Init(const RIGameObject_BeMust_InitForDraw *pObject) = 0;

	virtual bool isInit() = 0;
	virtual void init() = 0;
	virtual void clear() = 0;
	
};


class RIScene_InitData {
public:
	virtual ~RIScene_InitData() = 0 {}

	virtual std::vector<const RIGameObject_BeMust_InitForDraw*> & getObjects_Init() = 0;
};

class RIScene_DeferredLighting {
public:
	virtual ~RIScene_DeferredLighting() = 0 {}

	virtual std::vector<const RIGameObject_Drawable*> & getObjects_NoneClear() = 0;
	virtual std::vector<const RIGameObject_Drawable*> & getObjects_Clear() = 0;
};


#endif