#pragma once
#ifndef HEADER_SCENE_DEFERREDLIGHTING
#define HEADER_SCENE_DEFERREDLIGHTING

#include"Scene.h"
#include "..\Module_Object\Interface_Object.h"
  
class CScene_DeferredLighting :	public CScene, 
								virtual public RIScene_DeferredLighting {
private: 
	std::vector<const RIGameObject_Drawable *> m_pObjects_NoneClear;
	std::vector<const RIGameObject_Drawable *> m_pObjects_Clear; 

public:
	CScene_DeferredLighting() : CScene(){}
	virtual ~CScene_DeferredLighting() {}

	//override IScene 
	virtual void addObject(const RIGameObject_Drawable *pObject);
	virtual std::vector<const RIGameObject_Drawable*> & getObjects();

	//ovderride RIScene_InitData
	virtual std::vector<const RIGameObject_BeMust_InitForDraw*> & getObjects_Init() { return CScene::getObjects_Init(); }

	//ovderride RIScene_DeferredLighting
	virtual std::vector<const RIGameObject_Drawable*> & getObjects_NoneClear() { return m_pObjects_NoneClear; }
	virtual std::vector<const RIGameObject_Drawable*> & getObjects_Clear() { return m_pObjects_Clear; }
	
	virtual void clear();
};

//������

inline void CScene_DeferredLighting::addObject(const RIGameObject_Drawable * pObject)
{
	if (pObject->isClear())	m_pObjects_Clear.push_back(pObject);
	else					m_pObjects_NoneClear.push_back(pObject);
}

inline std::vector<const RIGameObject_Drawable*>& CScene_DeferredLighting::getObjects()
{
	auto nNumberOfNoneClear = m_pObjects_NoneClear.size();
	m_pObjects.resize(nNumberOfNoneClear + m_pObjects_Clear.size());

	auto iter_Begin = m_pObjects.begin();
	std::copy(m_pObjects_NoneClear.begin(), m_pObjects_NoneClear.end(), iter_Begin );
	std::copy(m_pObjects_Clear.begin(), m_pObjects_Clear.end(), iter_Begin + nNumberOfNoneClear);

	return m_pObjects;
}

inline void CScene_DeferredLighting::clear()
{
	m_pObjects_Clear.clear();
	m_pObjects_NoneClear.clear();
	CScene::clear();
}




#endif