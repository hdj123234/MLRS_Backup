#pragma once

#ifndef HEADER_SCENE
#define HEADER_SCENE

#include"Interface_Scene.h"

#ifndef HEADER_STL
#include<vector>
#endif

//
class CScene :	public IScene,
				public RIScene_InitData {
protected:
	const ISkyBox* m_pSkyBox;
	std::vector<const RIGameObject_Drawable *> m_pObjects;
	std::vector<const RIGameObject_BeMust_RenewalForDraw *> m_pObjects_Renewal;
	std::vector<const RIGameObject_BeMust_InitForDraw *> m_pObjects_Init;
	bool m_bInit;
public:
	CScene() :m_pSkyBox(nullptr), m_bInit(false){}
	virtual ~CScene() {}

	//override IScene
	virtual void setSkyBox(const ISkyBox* pSkyBox) { m_pSkyBox = pSkyBox; }
	virtual const ISkyBox* getSkyBox() { return m_pSkyBox; }
	virtual void addObject(const RIGameObject_Drawable *pObject) { m_pObjects.push_back(pObject); }
	virtual std::vector<const RIGameObject_Drawable*> & getObjects() { return m_pObjects; }
	virtual void addObject_Renewal(const RIGameObject_BeMust_RenewalForDraw *pObject) { m_pObjects_Renewal.push_back(pObject); }
	virtual std::vector<const RIGameObject_BeMust_RenewalForDraw*> & getObjects_Renewal() {return m_pObjects_Renewal;}
	virtual void addObject_Init(const RIGameObject_BeMust_InitForDraw *pObject) { m_pObjects_Init.push_back(pObject); }

	//ovderride RIScene_InitData
	virtual std::vector<const RIGameObject_BeMust_InitForDraw*> & getObjects_Init() { m_bInit = false; return m_pObjects_Init;  }
	 
	virtual bool isInit() {return m_bInit;}
	virtual void init() { m_bInit = true; }

	virtual void clear() { m_pSkyBox = nullptr; m_pObjects.clear(); m_pObjects_Renewal.clear(); }
};


#endif