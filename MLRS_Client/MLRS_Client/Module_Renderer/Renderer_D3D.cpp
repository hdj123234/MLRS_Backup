#include"stdafx.h"
#include "Renderer_D3D.h"

#include"..\Module_Scene\Interface_Scene.h"
#include"..\Module_Object\Interface_Object.h"
#include"..\Module_Object_Common\Interface_Object_Common.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"RendererState_D3D.h"

#ifndef HEADER_STL
#include<random>
#endif // !HEADER_STL

std::random_device rd;

CRenderer_D3D::CRenderer_D3D()
	:m_bReady(false),
	m_pRasterizerState(nullptr),
	m_pBlendState(nullptr),
	m_nSampleMask(0),
	m_pDepthStencilState(nullptr),
	m_nStencilRef(0),
	m_pDepthStencilView(nullptr),
	m_nClearFlag(0),
	m_fClearDepth(0.0f),
	m_nClearStencil(0),
	m_pRendererState(CRendererState_D3D::getGlobalState()),
	m_pCB_FogState(nullptr)
{
	m_fClearColor[0] = 0.0f;
	m_fClearColor[1] = 0.0f;
	m_fClearColor[2] = 0.0f;
	m_fClearColor[3] = 1.0f;
	m_fBlendFactor[0] = 0.0f;
	m_fBlendFactor[1] = 0.0f;
	m_fBlendFactor[2] = 0.0f;
	m_fBlendFactor[3] = 0.0f;

}


void CRenderer_D3D::getReady()
{
	if (m_pRenderTargetViews.empty()|| m_Viewports.empty()|| !m_pRasterizerState) return;
	m_pRendererState->setDeviceContext(CGlobalDirectXDevice::getDeviceContext());
//	ID3D11DeviceContext *pDeviceContext = CGlobalDirectXDevice::getDeviceContext();
//	pDeviceContext->RSSetViewports(m_Viewports.size(), m_Viewports.data());
//	pDeviceContext->RSSetState(m_pRasterizerState);
//	if(m_pBlendState)
//		pDeviceContext->OMSetBlendState(m_pBlendState, m_fBlendFactor.data(), m_nSampleMask);
//	pDeviceContext->OMSetDepthStencilState(m_pDepthStencilState, m_nStencilRef);
//	pDeviceContext->OMSetRenderTargets(m_pRenderTargetViews.size(), m_pRenderTargetViews.data(), m_pDepthStencilView);
////	pDeviceContext->OMSetRenderTargets(m_pRenderTargetViews.size(), m_pRenderTargetViews.data(), nullptr);
	m_bReady = true;
}

void CRenderer_D3D::clearScreen()
{
	ID3D11DeviceContext *pDeviceContext = m_pRendererState->getDeviceContext();
	for (auto data : m_pRenderTargetViews)
		pDeviceContext->ClearRenderTargetView(data, m_fClearColor.data());

	if (m_pDepthStencilView)
		pDeviceContext->ClearDepthStencilView(m_pDepthStencilView, m_nClearFlag, m_fClearDepth, m_nClearStencil);
}

void CRenderer_D3D::drawReady(IScene * pScene)
{
	if (!pScene)
		return;
	if (!pScene->isInit())
	{
		std::vector<const RIGameObject_BeMust_InitForDraw*>const & pObjects = dynamic_cast<RIScene_InitData *>(pScene)->getObjects_Init();
		for (auto data : pObjects)
			data->initializeOnDX(m_pRendererState);
		pScene->init();
	}

	std::vector<const RIGameObject_BeMust_RenewalForDraw*>const & pObjects = pScene->getObjects_Renewal();
	for (auto data : pObjects)
		data->updateOnDX(m_pRendererState);

	m_pRendererState->setViewports(m_Viewports);
	m_pRendererState->setRasterizerState(m_pRasterizerState);
	m_pRendererState->setBlend(m_pBlendState, m_fBlendFactor, m_nSampleMask);
	m_pRendererState->setDepthStencilState(m_pDepthStencilState, m_nStencilRef);
	m_pRendererState->setRenderTargetViews(m_pRenderTargetViews, m_pDepthStencilView);

	//static int n;
	//if (n++ % 30 == 0)
	//{
	//	std::uniform_real_distribution<float> range(0.0f, 1.0f);
	//	//Test
	//	for (auto &data : m_fClearColor)
	//		data = range(rd);
	//}

}
void CRenderer_D3D::drawObjects(IScene * pScene)
{
	if(auto p=pScene->getSkyBox())
		p->drawOnDX(m_pRendererState);
	std::vector<const RIGameObject_Drawable*>const & pObjects = pScene->getObjects();
	for (auto data : pObjects)
		data->drawOnDX(m_pRendererState);
}

void CRenderer_D3D::setDefaultRendererState()
{
	m_pRendererState = CRendererState_D3D::getGlobalState();
}

void CRenderer_D3D::drawScene(IScene * pScene)
{
	if (!m_bReady ||!pScene)	return;

	this->drawReady(pScene);
	this->clearScreen();
	this->drawObjects(pScene);
	this->drawPostProcessing(pScene);
	
	CGlobalDirectXDevice::getSwapChain()->Present(0, 0);
}

bool CRenderer_D3D::createObject(IBuilder_Renderer_D3D * pBuilder)
{
	if(!pBuilder)	return false;
	if (!pBuilder->build(this)) return false;

	if (m_pRenderTargetViews.empty() || m_Viewports.empty() ) return false;

	return true;
}

void CRenderer_D3D::release()
{
	m_Viewports.clear();
	m_pRasterizerState = nullptr;

	m_pBlendState = nullptr;
	m_pDepthStencilState = nullptr;

	m_pRenderTargetViews.clear();

	m_pDepthStencilView = nullptr;

	m_fClearColor[0] = 1.0f;
	m_fClearColor[1] = 1.0f;
	m_fClearColor[2] = 1.0f;
	m_fClearColor[3] = 1.0f;
	m_fBlendFactor[0] = 0.0f;
	m_fBlendFactor[1] = 0.0f;
	m_fBlendFactor[2] = 0.0f;
	m_fBlendFactor[3] = 0.0f;
	m_nSampleMask = 0;
	m_nStencilRef=0;

	m_bReady = false;
}

void CRenderer_D3D::setRasterizer(std::vector<D3D11_VIEWPORT>&& rViewports, ID3D11RasterizerState * pRasterizerState)
{
	m_Viewports = rViewports;
	m_pRasterizerState = pRasterizerState;
}

void CRenderer_D3D::setBlend(ID3D11BlendState * pBlendState, std::array<float, 4>& rfBlendFactor, UINT nSampleMask)
{
	m_pBlendState = pBlendState;
	m_fBlendFactor = rfBlendFactor;
	m_nSampleMask = nSampleMask;
}

void CRenderer_D3D::setDepthStencil(ID3D11DepthStencilState * pDepthStencilState, UINT nStencilRef, ID3D11DepthStencilView * pDepthStencilView)
{
	m_pDepthStencilState = pDepthStencilState;
	m_nStencilRef = nStencilRef;
	m_pDepthStencilView = pDepthStencilView;
}

void CRenderer_D3D::setRenderTargetViews(std::vector<ID3D11RenderTargetView*>&& rpRenderTargetViews)
{
	m_pRenderTargetViews = rpRenderTargetViews;
}

void CRenderer_D3D::setClearData(std::array<float, 4>& rfClearColor, UINT nClearFlag, float fClearDepth, UINT8 nClearStencil)
{
	m_fClearColor = rfClearColor;
	m_nClearFlag = nClearFlag;
	m_fClearDepth = fClearDepth;
	m_nClearStencil = nClearStencil;
}