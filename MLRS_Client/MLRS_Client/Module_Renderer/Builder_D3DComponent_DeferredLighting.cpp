#include"stdafx.h"
#include "Builder_D3DComponent_DeferredLighting.h"


#include"..\Module_DXComponent\Factory_DXTexture.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"
#include"RendererState_D3D.h"

#ifndef HEADER_STL
#include<random>
#endif // !HEADER_STL

bool CBuilder_D3DComponent_DeferredLighting::build(CD3DComponent_DeferredLighting *pTarget)
{
	if (!pTarget)	return false;


	std::vector<ID3D11ShaderResourceView *> pShaderResourceViews;
	std::vector<ID3D11RenderTargetView *>	pRenderTargetViews;


	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Width=CGlobalWindow::getWidth();
	texDesc.Height = CGlobalWindow::getHeight();
	texDesc.MipLevels=1;
	texDesc.ArraySize=1;
	texDesc.SampleDesc.Count = 1;
	texDesc.SampleDesc.Quality = 0;
	texDesc.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_RENDER_TARGET | D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE ;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = 0;

	ID3D11Texture2D *pTexture;
	ID3D11RenderTargetView *pRenderTargetView;
	ID3D11ShaderResourceView *pShaderResourceView;

	texDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

	pTexture = CFactory_Texture2D("TX2D_DeferredLighting_Position_World", texDesc).createTexture2D();
	pRenderTargetView = CFactory_RenderTargetView_FromTexture2D("RTV_DeferredLighting_Position_World", pTexture, texDesc).createRenderTargetView();
	pShaderResourceView = CFactory_ShaderResourceView_FromTexture2D("SRV_DeferredLighting_Position_World", pTexture, texDesc).createShaderResourceView();
	pRenderTargetViews.push_back(pRenderTargetView);
	pShaderResourceViews.push_back(pShaderResourceView);

	pTexture = CFactory_Texture2D("TX2D_DeferredLighting_DiffuseColor", texDesc).createTexture2D();
	pRenderTargetView = CFactory_RenderTargetView_FromTexture2D("RTV_DeferredLighting_DiffuseColor", pTexture, texDesc).createRenderTargetView();
	pShaderResourceView = CFactory_ShaderResourceView_FromTexture2D("SRV_DeferredLighting_DiffuseColor", pTexture, texDesc).createShaderResourceView();
	pRenderTargetViews.push_back(pRenderTargetView);
	pShaderResourceViews.push_back(pShaderResourceView);

	pTexture = CFactory_Texture2D("TX2D_DeferredLighting_Normal", texDesc).createTexture2D();
	pRenderTargetView = CFactory_RenderTargetView_FromTexture2D("RTV_DeferredLighting_Normal", pTexture, texDesc).createRenderTargetView();
	pShaderResourceView = CFactory_ShaderResourceView_FromTexture2D("SRV_DeferredLighting_Normal", pTexture, texDesc).createShaderResourceView();
	pRenderTargetViews.push_back(pRenderTargetView);
	pShaderResourceViews.push_back(pShaderResourceView);

	pTexture = CFactory_Texture2D("TX2D_DeferredLighting_Specular", texDesc).createTexture2D();
	pRenderTargetView = CFactory_RenderTargetView_FromTexture2D("RTV_DeferredLighting_Specular", pTexture, texDesc).createRenderTargetView();
	pShaderResourceView = CFactory_ShaderResourceView_FromTexture2D("SRV_DeferredLighting_Specular", pTexture, texDesc).createShaderResourceView();
	pRenderTargetViews.push_back(pRenderTargetView);
	pShaderResourceViews.push_back(pShaderResourceView);

	pTarget->setShaderResourceViews(pShaderResourceViews);
	pTarget->setRenderTargetViews(pRenderTargetViews);
	return true;
}

CD3DComponent_DeferredLighting * CBuilder_D3DComponent_DeferredLighting::build()
{
	CD3DComponent_DeferredLighting * pResult = new CD3DComponent_DeferredLighting();
	if (!CBuilder_D3DComponent_DeferredLighting::build(pResult))
	{
		delete pResult;
		pResult = nullptr;
	}
	return pResult;
}
