#pragma once
#ifndef HEADER_DXRENDERERSTATE
#define HEADER_DXRENDERERSTATE

#include"Interface_Renderer.h"

#ifndef HEADER_STL
#include<vector>
#include<array>
#endif

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif


class CRendererState_D3D  : public IRendererState{
private:
	ID3D11DeviceContext * m_pDeviceContext;

	//IA
	std::vector<ID3D11Buffer *>				m_pVBuffers;
	std::vector<UINT>						m_nVertexStrides;
	std::vector<UINT>						m_nVertexOffsets;

	ID3D11Buffer *							m_pIBuffer;
	DXGI_FORMAT								m_IndexBufferFormat;
	UINT									m_nIndexBufferOffset;

	ID3D11InputLayout *						m_pInputLayout;

	D3D11_PRIMITIVE_TOPOLOGY				m_Primitive;
	 
	 //RS
	std::vector<D3D11_VIEWPORT>				m_Viewports;
	ID3D11RasterizerState *					m_pRasterizerState;
	ID3D11RasterizerState *					m_pRasterizerState_Past;
	
	 //OM
	ID3D11BlendState *						m_pBlendState;
	std::array<float, 4>					m_fBlendFactor;
	UINT									m_nSampleMask;
	ID3D11BlendState *						m_pBlendState_Past;
	std::array<float, 4>					m_fBlendFactor_Past;
	UINT									m_nSampleMask_Past;

	ID3D11DepthStencilState *				m_pDepthStencilState;
	UINT									m_nStencilRef;
	ID3D11DepthStencilState *				m_pDepthStencilState_Past;
	UINT									m_nStencilRef_Past;

	std::vector<ID3D11RenderTargetView *>	m_pRenderTargetViews;
	std::vector<ID3D11RenderTargetView *>	m_pRenderTargetViews_Past;
	ID3D11DepthStencilView *				m_pDepthStencilView;
	ID3D11DepthStencilView *				m_pDepthStencilView_Past;
	 
	 //shader
	ID3D11VertexShader *					m_pVS;
	ID3D11HullShader *						m_pHS;
	ID3D11DomainShader *					m_pDS;
	ID3D11GeometryShader *					m_pGS;
	ID3D11PixelShader *						m_pPS;
	 
	std::vector<ID3D11Buffer *> m_pCBufferm_VS;
	std::vector<ID3D11Buffer *> m_pCBufferm_HS;
	std::vector<ID3D11Buffer *> m_pCBufferm_DS;
	std::vector<ID3D11Buffer *> m_pCBufferm_GS;
	std::vector<ID3D11Buffer *> m_pCBufferm_PS;
	 
	std::vector<ID3D11ShaderResourceView *> m_pShaderResourceView_VS;
	std::vector<ID3D11ShaderResourceView *> m_pShaderResourceView_HS;
	std::vector<ID3D11ShaderResourceView *> m_pShaderResourceView_DS;
	std::vector<ID3D11ShaderResourceView *> m_pShaderResourceView_GS;
	std::vector<ID3D11ShaderResourceView *> m_pShaderResourceView_PS;
	 
	std::vector<ID3D11SamplerState *> m_pSamplerStates_VS;
	std::vector<ID3D11SamplerState *> m_pSamplerStates_HS;
	std::vector<ID3D11SamplerState *> m_pSamplerStates_DS;
	std::vector<ID3D11SamplerState *> m_pSamplerStates_GS;
	std::vector<ID3D11SamplerState *> m_pSamplerStates_PS;

	std::vector<ID3D11Buffer *> m_pSOTargets;
	std::vector<UINT> m_nSOOffsets;

	static CRendererState_D3D *s_pRendererState;
public:
	CRendererState_D3D(ID3D11DeviceContext * pDeviceContext=nullptr);
	virtual ~CRendererState_D3D() {}

	void setDeviceContext(ID3D11DeviceContext * pDeviceContext);
	ID3D11DeviceContext * getDeviceContext() { return m_pDeviceContext; }

	//IA
	bool setVertexBuffer(	ID3D11Buffer * const pVertexBuffer,
							const UINT nStride, 
							const UINT nOffset=0,
							const UINT nStartSlot=0);
	bool setVertexBuffers(	const UINT nStartSlot,
							const std::vector<ID3D11Buffer *> &rpVertexBuffers, 
							const std::vector<UINT> &rStrides, 
							const std::vector<UINT> &rOffsets);
	bool setIndexBuffer(ID3D11Buffer * const pIndexBuffer,
						const DXGI_FORMAT indexBufferFormat, 
						const UINT nOffset);
	bool setPrimitive(const D3D11_PRIMITIVE_TOPOLOGY primitive);
	bool setInputLayout(ID3D11InputLayout * const pInputLayout);

	//RS
	bool setViewports(const std::vector<D3D11_VIEWPORT> &rViewports);
	bool setViewports(const D3D11_VIEWPORT &rViewport);
	bool setRasterizerState(ID3D11RasterizerState *pRasterizerState);

	//SO
	bool setSOTarget(	const std::vector<ID3D11Buffer *> &rpSOTargets,
						const std::vector<UINT> &rnSOOffsets);
	bool setSOTarget(const std::vector<ID3D11Buffer *> &rpSOTargets);
	bool setSOTarget(ID3D11Buffer * pSOTarget, const UINT m_nSOOffset=0);

	//OM
	bool setBlend(	ID3D11BlendState *pBlendState, 
					const std::array<float, 4> &rfBlendFactor = {1,1,1,1},
					const UINT nSampleMask = 0xffffffff);
	bool setDepthStencilState(	ID3D11DepthStencilState *pDepthStencilState, 
								const UINT nStencilRef = 0xffffffff);
	bool setRenderTargetViews(	const std::vector<ID3D11RenderTargetView *> &rpRenderTargetViews,
								ID3D11DepthStencilView *pDepthStencilView);
	bool setRenderTargetView(	ID3D11RenderTargetView *pRenderTargetViews,
								ID3D11DepthStencilView *pDepthStencilView);
	bool setDepthStencilView(	ID3D11DepthStencilView *pDepthStencilView);

	//Shader
	bool setShader_VS(ID3D11VertexShader *pShader);
	bool setShader_HS(ID3D11HullShader *pShader);
	bool setShader_DS(ID3D11DomainShader *pShader);
	bool setShader_GS(ID3D11GeometryShader *pShader);
	bool setShader_PS(ID3D11PixelShader *pShader);
	bool setConstBuffer_VS(const std::vector<ID3D11Buffer *> &rpConstBuffers);
	bool setConstBuffer_HS(const std::vector<ID3D11Buffer *> &rpConstBuffers);
	bool setConstBuffer_DS(const std::vector<ID3D11Buffer *> &rpConstBuffers);
	bool setConstBuffer_GS(const std::vector<ID3D11Buffer *> &rpConstBuffers);
	bool setConstBuffer_PS(const std::vector<ID3D11Buffer *> &rpConstBuffers);
//	bool setConstBuffer_VS(ID3D11Buffer *pConstBuffer);
//	bool setConstBuffer_HS(ID3D11Buffer *pConstBuffer);
//	bool setConstBuffer_DS(ID3D11Buffer *pConstBuffer);
//	bool setConstBuffer_GS(ID3D11Buffer *pConstBuffer);
//	bool setConstBuffer_PS(ID3D11Buffer *pConstBuffer);
	bool setConstBuffer_VS(ID3D11Buffer *pConstBuffer,UINT slot=0);
	bool setConstBuffer_HS(ID3D11Buffer *pConstBuffer,UINT slot=0);
	bool setConstBuffer_DS(ID3D11Buffer *pConstBuffer,UINT slot=0);
	bool setConstBuffer_GS(ID3D11Buffer *pConstBuffer,UINT slot=0);
	bool setConstBuffer_PS(ID3D11Buffer *pConstBuffer,UINT slot=0);
	bool setSamplerStates_VS(const std::vector<ID3D11SamplerState *> &rpSamplerStates);
	bool setSamplerStates_HS(const std::vector<ID3D11SamplerState *> &rpSamplerStates);
	bool setSamplerStates_DS(const std::vector<ID3D11SamplerState *> &rpSamplerStates);
	bool setSamplerStates_GS(const std::vector<ID3D11SamplerState *> &rpSamplerStates);
	bool setSamplerStates_PS(const std::vector<ID3D11SamplerState *> &rpSamplerStates);
	bool setSamplerStates_VS(ID3D11SamplerState *pSamplerState, UINT slot = 0);
	bool setSamplerStates_HS(ID3D11SamplerState *pSamplerState, UINT slot = 0);
	bool setSamplerStates_DS(ID3D11SamplerState *pSamplerState, UINT slot = 0);
	bool setSamplerStates_GS(ID3D11SamplerState *pSamplerState, UINT slot = 0);
	bool setSamplerStates_PS(ID3D11SamplerState *pSamplerState, UINT slot = 0);
	bool setShaderResourceViews_VS(const std::vector<ID3D11ShaderResourceView *> &rpShaderResourceViews);
	bool setShaderResourceViews_HS(const std::vector<ID3D11ShaderResourceView *> &rpShaderResourceViews);
	bool setShaderResourceViews_DS(const std::vector<ID3D11ShaderResourceView *> &rpShaderResourceViews);
	bool setShaderResourceViews_GS(const std::vector<ID3D11ShaderResourceView *> &rpShaderResourceViews);
	bool setShaderResourceViews_PS(const std::vector<ID3D11ShaderResourceView *> &rpShaderResourceViews);
//	bool setShaderResourceViews_VS(ID3D11ShaderResourceView *pShaderResourceView);
//	bool setShaderResourceViews_HS(ID3D11ShaderResourceView *pShaderResourceView);
//	bool setShaderResourceViews_DS(ID3D11ShaderResourceView *pShaderResourceView);
//	bool setShaderResourceViews_GS(ID3D11ShaderResourceView *pShaderResourceView);
//	bool setShaderResourceViews_PS(ID3D11ShaderResourceView *pShaderResourceView);
	bool setShaderResourceViews_VS(ID3D11ShaderResourceView *pShaderResourceView,UINT slot=0);
	bool setShaderResourceViews_HS(ID3D11ShaderResourceView *pShaderResourceView,UINT slot=0);
	bool setShaderResourceViews_DS(ID3D11ShaderResourceView *pShaderResourceView,UINT slot=0);
	bool setShaderResourceViews_GS(ID3D11ShaderResourceView *pShaderResourceView,UINT slot=0);
	bool setShaderResourceViews_PS(ID3D11ShaderResourceView *pShaderResourceView,UINT slot=0);

	void revertBlend				();
	void revertDepthStencilState	();
	void revertRenderTargetViews	();
	void revertDepthStencilView();
	void revertRasterizerState	();

	static CRendererState_D3D *getGlobalState() { return s_pRendererState; }



};


inline bool CRendererState_D3D::setVertexBuffer(ID3D11Buffer * const pVertexBuffer, const UINT nStride, const UINT nOffset, const UINT nStartSlot)
{
	bool bFlag = false;

	if (m_pVBuffers.size() > nStartSlot )
	{
		if(m_pVBuffers[nStartSlot] != pVertexBuffer)
			bFlag = true;
	}
	else
	{
		m_pVBuffers.resize(nStartSlot + 1);
		m_nVertexStrides.resize(nStartSlot + 1);
		m_nVertexOffsets.resize(nStartSlot + 1);
		bFlag = true;
	}

	if (!bFlag && m_nVertexStrides[nStartSlot] != nStride)
		bFlag = true;

	if (!bFlag && m_nVertexOffsets[nStartSlot] != nOffset)
		bFlag = true;

	if (bFlag)
	{
		m_pVBuffers		[nStartSlot] = pVertexBuffer;
		m_nVertexStrides[nStartSlot] =  nStride;
		m_nVertexOffsets[nStartSlot] =  nOffset;

		m_pDeviceContext->IASetVertexBuffers(nStartSlot, 1, &pVertexBuffer, &nStride, &nOffset);
	}
	return bFlag;
}

inline bool CRendererState_D3D::setVertexBuffers(	const UINT nStartSlot,
												const std::vector<ID3D11Buffer*>& rpVertexBuffers,
												const std::vector<UINT>& rStrides, 
												const std::vector<UINT>& rOffsets)
{
	bool bFlag = false;

	auto nNumberOfBuffer = rpVertexBuffers.size();
	if (m_pVBuffers.size() >= nNumberOfBuffer + nStartSlot)
	{
		auto iter_Begin_VB = m_pVBuffers.begin() + nStartSlot;
		std::vector<ID3D11Buffer*> tmp_VB(iter_Begin_VB, iter_Begin_VB + nNumberOfBuffer);
		if (tmp_VB != rpVertexBuffers)
			bFlag = true;
	}
	else
	{
		m_pVBuffers.resize(nNumberOfBuffer + nStartSlot);
		m_nVertexStrides.resize(nNumberOfBuffer + nStartSlot);
		m_nVertexOffsets.resize(nNumberOfBuffer + nStartSlot);
		bFlag = true;
	}

	if (!bFlag)
	{
		auto iter_Begin_Stride = m_nVertexStrides.begin() + nStartSlot;
		std::vector<UINT> tmp_Strides(iter_Begin_Stride, iter_Begin_Stride + nNumberOfBuffer);
		if (tmp_Strides != rStrides)
			bFlag = true;
	}

	if (!bFlag)
	{
		auto iter_Begin_Offset = m_nVertexOffsets.begin() + nStartSlot;
		std::vector<UINT> tmp_Offsets(iter_Begin_Offset, iter_Begin_Offset + nNumberOfBuffer);
		if (tmp_Offsets != rOffsets)
			bFlag = true;
	}

	if (bFlag)
	{
		std::copy(rpVertexBuffers.begin(), rpVertexBuffers.end(), m_pVBuffers.begin() + nStartSlot);
		std::copy(rStrides.begin(), rStrides.end(), m_nVertexStrides.begin() + nStartSlot);
		std::copy(rOffsets.begin(), rOffsets.end(), m_nVertexOffsets.begin() + nStartSlot);
		
		m_pDeviceContext->IASetVertexBuffers(nStartSlot, static_cast<UINT>(rpVertexBuffers.size()), rpVertexBuffers.data(), rStrides.data(), rOffsets.data());
	}
	return bFlag;
}

inline bool CRendererState_D3D::setIndexBuffer(	ID3D11Buffer * const pIndexBuffer,
												const DXGI_FORMAT indexBufferFormat, 
												const  UINT nOffset)
{
	bool bFlag = false;
	if (m_pIBuffer != pIndexBuffer)
	{
		m_pIBuffer = pIndexBuffer;
		bFlag = true;
	}
	if (m_IndexBufferFormat != indexBufferFormat)
	{
		m_IndexBufferFormat = indexBufferFormat;
		bFlag = true;
	}
	if (m_nIndexBufferOffset != nOffset)
	{
		m_nIndexBufferOffset = nOffset;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->IASetIndexBuffer(m_pIBuffer, m_IndexBufferFormat, m_nIndexBufferOffset);
	return bFlag;
}

inline bool CRendererState_D3D::setPrimitive(const D3D11_PRIMITIVE_TOPOLOGY primitive)
{
	bool bFlag = false;
	if (m_Primitive != primitive)
	{
		m_Primitive = primitive;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->IASetPrimitiveTopology(m_Primitive);
	return bFlag;
}

inline bool CRendererState_D3D::setInputLayout(ID3D11InputLayout * const pInputLayout)
{
	bool bFlag = false;
	if (m_pInputLayout != pInputLayout)
	{
		m_pInputLayout = pInputLayout;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->IASetInputLayout(m_pInputLayout);
	return bFlag;
}

inline bool CRendererState_D3D::setViewports(const std::vector<D3D11_VIEWPORT>& rViewports)
{
	bool bFlag = false;
	if (m_Viewports != rViewports)
	{
		m_Viewports = rViewports;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->RSSetViewports(static_cast<UINT>(m_Viewports.size()), m_Viewports.data());
	return bFlag;
}

inline bool CRendererState_D3D::setViewports(const D3D11_VIEWPORT & rViewport)
{
	bool bFlag = false;
	if (m_Viewports.size() != 1 || m_Viewports[0] != rViewport)
	{
		m_Viewports.clear();
		m_Viewports.push_back(rViewport);
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->RSSetViewports(static_cast<UINT>(m_Viewports.size()), m_Viewports.data());
	return bFlag;
}

inline bool CRendererState_D3D::setRasterizerState(ID3D11RasterizerState * pRasterizerState)
{
	bool bFlag = false;
	if (m_pRasterizerState != pRasterizerState)
	{
		m_pRasterizerState_Past = m_pRasterizerState;
		m_pRasterizerState = pRasterizerState;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->RSSetState(m_pRasterizerState);
	return bFlag;
}

inline bool CRendererState_D3D::setSOTarget(	const std::vector<ID3D11Buffer*>& rpSOTargets, 
											const std::vector<UINT> &rnSOOffsets)
{
	if (m_pSOTargets == rpSOTargets&&m_nSOOffsets == rnSOOffsets)	return false;
	else
	{
		m_pSOTargets = rpSOTargets;
		m_nSOOffsets = rnSOOffsets;

		m_pDeviceContext->SOSetTargets(static_cast<UINT>(m_pSOTargets.size()), m_pSOTargets.data(), m_nSOOffsets.data());
		return true;
	}
}

inline bool CRendererState_D3D::setSOTarget(const std::vector<ID3D11Buffer*>& rpSOTargets)
{
	if (m_pSOTargets == rpSOTargets)	return false;
	else
	{
		m_pSOTargets = rpSOTargets;
		std::vector<UINT> offsets;
		offsets.resize(m_pSOTargets.size());
		for (auto &data : offsets)	data = 0;
		m_nSOOffsets = offsets;

		m_pDeviceContext->SOSetTargets(static_cast<UINT>(m_pSOTargets.size()), m_pSOTargets.data(), m_nSOOffsets.data());
		return true;
	}
}

inline bool CRendererState_D3D::setSOTarget(ID3D11Buffer * pSOTarget, const UINT m_nSOOffset)
{
	if (m_pSOTargets.size() == 1 && m_pSOTargets[0] == pSOTarget)	return false;
	else
	{
		m_pSOTargets.resize(1);
		m_pSOTargets[0] = pSOTarget;

		std::vector<UINT> offsets;
		offsets.push_back(m_nSOOffset);
		m_nSOOffsets = offsets;

		m_pDeviceContext->SOSetTargets(static_cast<UINT>(m_pSOTargets.size()), m_pSOTargets.data(), m_nSOOffsets.data());
		return true;
	}
}


inline bool CRendererState_D3D::setBlend(ID3D11BlendState * pBlendState,
										const std::array<float, 4>& rfBlendFactor, 
										const  UINT nSampleMask)
{
	bool bFlag = false;
	if (m_pBlendState != pBlendState)
	{
		m_pBlendState_Past = m_pBlendState;
		m_pBlendState = pBlendState;
		bFlag = true;
	}
	if (m_fBlendFactor != rfBlendFactor)
	{
		m_fBlendFactor_Past = m_fBlendFactor;
		m_fBlendFactor = rfBlendFactor;
		bFlag = true;
	}
	if (m_nSampleMask != nSampleMask)
	{
		m_nSampleMask_Past = m_nSampleMask;
		m_nSampleMask = nSampleMask;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->OMSetBlendState(m_pBlendState, m_fBlendFactor.data(), m_nSampleMask);
	return bFlag;
}

inline bool CRendererState_D3D::setDepthStencilState(ID3D11DepthStencilState * pDepthStencilState, 
													const UINT nStencilRef)
{
	bool bFlag = false;
	if (m_pDepthStencilState != pDepthStencilState)
	{
		m_pDepthStencilState_Past = m_pDepthStencilState;
		m_pDepthStencilState = pDepthStencilState;
		bFlag = true;
	}
	if (m_nStencilRef != nStencilRef)
	{
		m_nStencilRef = nStencilRef;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->OMSetDepthStencilState(m_pDepthStencilState, m_nStencilRef);
	return bFlag;
}

inline bool CRendererState_D3D::setRenderTargetViews(	const std::vector<ID3D11RenderTargetView*>& rpRenderTargetViews,
													ID3D11DepthStencilView * pDepthStencilView)
{
	bool bFlag = false;
	if (m_pRenderTargetViews != rpRenderTargetViews)
	{
		m_pRenderTargetViews_Past = m_pRenderTargetViews;
		m_pRenderTargetViews = rpRenderTargetViews;
		bFlag = true;
	}
	if (m_pDepthStencilView != pDepthStencilView)
	{
		m_pDepthStencilView = pDepthStencilView;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->OMSetRenderTargets(static_cast<UINT>(m_pRenderTargetViews.size()), m_pRenderTargetViews.data(), m_pDepthStencilView);
	return bFlag;
}


inline bool CRendererState_D3D::setRenderTargetView(ID3D11RenderTargetView * pRenderTargetView, ID3D11DepthStencilView * pDepthStencilView)
{
	bool bFlag = false;
	if (m_pRenderTargetViews.size() != 1 || m_pRenderTargetViews[0] != pRenderTargetView)
	{
		m_pRenderTargetViews_Past = m_pRenderTargetViews;
		m_pRenderTargetViews.clear();
		m_pRenderTargetViews.push_back(pRenderTargetView);
		bFlag = true;
	}
	if (m_pDepthStencilView != pDepthStencilView)
	{
		m_pDepthStencilView_Past = m_pDepthStencilView;
		m_pDepthStencilView = pDepthStencilView;
		bFlag = true;
	}
	if (bFlag)
		m_pDeviceContext->OMSetRenderTargets(static_cast<UINT>(m_pRenderTargetViews.size()), m_pRenderTargetViews.data(), m_pDepthStencilView);
	return bFlag;
}

inline bool CRendererState_D3D::setDepthStencilView(ID3D11DepthStencilView * pDepthStencilView)
{
	if (m_pDepthStencilView != pDepthStencilView)
	{
		m_pDepthStencilView_Past = m_pDepthStencilView;
		m_pDepthStencilView = pDepthStencilView;
		m_pDeviceContext->OMSetRenderTargets(static_cast<UINT>(m_pRenderTargetViews.size()), m_pRenderTargetViews.data(), m_pDepthStencilView);
		return true;
	}
	else
		return false;
}
inline bool CRendererState_D3D::setShader_VS(ID3D11VertexShader * pShader)
{
	if (m_pVS != pShader)
	{
		m_pVS = pShader;
		m_pDeviceContext->VSSetShader(m_pVS, NULL, NULL);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShader_HS(ID3D11HullShader * pShader)
{
	if (m_pHS != pShader)
	{
		m_pHS = pShader;
		m_pDeviceContext->HSSetShader(m_pHS, NULL, NULL);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShader_DS(ID3D11DomainShader * pShader)
{
	bool bFlag = false;
	if (m_pDS != pShader)
	{
		m_pDS = pShader;
		m_pDeviceContext->DSSetShader(m_pDS, NULL, NULL);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShader_GS(ID3D11GeometryShader * pShader)
{
	bool bFlag = false;
	if (m_pGS != pShader)
	{
		m_pGS = pShader;
		m_pDeviceContext->GSSetShader(m_pGS, NULL, NULL);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShader_PS(ID3D11PixelShader * pShader)
{
	if (m_pPS != pShader)
	{
		m_pPS = pShader;
		m_pDeviceContext->PSSetShader(m_pPS, NULL, NULL);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_VS(const std::vector<ID3D11Buffer*>& rpConstBuffers)
{
	if (m_pCBufferm_VS != rpConstBuffers)
	{
		m_pCBufferm_VS = rpConstBuffers;
		m_pDeviceContext->VSSetConstantBuffers(0, static_cast<UINT>(m_pCBufferm_VS.size()), m_pCBufferm_VS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_HS(const std::vector<ID3D11Buffer*>& rpConstBuffers)
{
	if (m_pCBufferm_HS != rpConstBuffers)
	{
		m_pCBufferm_HS = rpConstBuffers;
		m_pDeviceContext->HSSetConstantBuffers(0, static_cast<UINT>(m_pCBufferm_HS.size()), m_pCBufferm_HS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_DS(const std::vector<ID3D11Buffer*>& rpConstBuffers)
{
	if (m_pCBufferm_DS != rpConstBuffers)
	{
		m_pCBufferm_DS = rpConstBuffers;
		m_pDeviceContext->DSSetConstantBuffers(0, static_cast<UINT>(m_pCBufferm_DS.size()), m_pCBufferm_DS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_GS(const std::vector<ID3D11Buffer*>& rpConstBuffers)
{
	if (m_pCBufferm_GS != rpConstBuffers)
	{
		m_pCBufferm_GS = rpConstBuffers;
		m_pDeviceContext->GSSetConstantBuffers(0, static_cast<UINT>(m_pCBufferm_GS.size()), m_pCBufferm_GS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_PS(const std::vector<ID3D11Buffer*>& rpConstBuffers)
{
	if (m_pCBufferm_PS != rpConstBuffers)
	{
		m_pCBufferm_PS = rpConstBuffers;
		m_pDeviceContext->PSSetConstantBuffers(0, static_cast<UINT>(m_pCBufferm_PS.size()), m_pCBufferm_PS.data());
		return true;
	}
	return false;
} 

inline bool CRendererState_D3D::setConstBuffer_VS(ID3D11Buffer * pConstBuffer, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pCBufferm_VS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	else if (container[slot] != pConstBuffer)
	{
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->VSSetConstantBuffers(slot, 1, &pConstBuffer);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_HS(ID3D11Buffer * pConstBuffer, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pCBufferm_HS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	else if (container[slot] != pConstBuffer)
	{
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->HSSetConstantBuffers(slot, 1, &pConstBuffer);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_DS(ID3D11Buffer * pConstBuffer, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pCBufferm_DS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	else if (container[slot] != pConstBuffer)
	{
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->DSSetConstantBuffers(slot, 1, &pConstBuffer);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_GS(ID3D11Buffer * pConstBuffer, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pCBufferm_GS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	else if (container[slot] != pConstBuffer)
	{
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->GSSetConstantBuffers(slot, 1, &pConstBuffer);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setConstBuffer_PS(ID3D11Buffer * pConstBuffer, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pCBufferm_PS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	else if (container[slot] != pConstBuffer)
	{
		container[slot] = pConstBuffer;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->PSSetConstantBuffers(slot, 1, &pConstBuffer);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_VS(const std::vector<ID3D11SamplerState*>& rpSamplerStates)
{
	if (m_pSamplerStates_VS != rpSamplerStates)
	{
		m_pSamplerStates_VS = rpSamplerStates;
		m_pDeviceContext->VSSetSamplers(0, static_cast<UINT>(m_pSamplerStates_VS.size()), m_pSamplerStates_VS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_HS(const std::vector<ID3D11SamplerState*>& rpSamplerStates)
{
	if (m_pSamplerStates_HS != rpSamplerStates)
	{
		m_pSamplerStates_HS = rpSamplerStates;
		m_pDeviceContext->HSSetSamplers(0, static_cast<UINT>(m_pSamplerStates_HS.size()), m_pSamplerStates_HS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_DS(const std::vector<ID3D11SamplerState*>& rpSamplerStates)
{
	if (m_pSamplerStates_DS != rpSamplerStates)
	{
		m_pSamplerStates_DS = rpSamplerStates;
		m_pDeviceContext->DSSetSamplers(0, static_cast<UINT>(m_pSamplerStates_DS.size()), m_pSamplerStates_DS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_GS(const std::vector<ID3D11SamplerState*>& rpSamplerStates)
{
	if (m_pSamplerStates_GS != rpSamplerStates)
	{
		m_pSamplerStates_GS = rpSamplerStates;
		m_pDeviceContext->GSSetSamplers(0, static_cast<UINT>(m_pSamplerStates_GS.size()), m_pSamplerStates_GS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_PS(const std::vector<ID3D11SamplerState*>& rpSamplerStates)
{
	if (m_pSamplerStates_PS != rpSamplerStates)
	{
		m_pSamplerStates_PS = rpSamplerStates;
		m_pDeviceContext->PSSetSamplers(0, static_cast<UINT>(m_pSamplerStates_PS.size()), m_pSamplerStates_PS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_VS(ID3D11SamplerState * pSamplerState, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pSamplerStates_VS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pSamplerState;
		bFlag = true;
	}
	else if (container[slot] != pSamplerState)
	{
		container[slot] = pSamplerState;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->VSSetSamplers(slot, 1, &pSamplerState); 
		return true;
	}
	return false; 
}

inline bool CRendererState_D3D::setSamplerStates_HS(ID3D11SamplerState * pSamplerState, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pSamplerStates_HS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pSamplerState;
		bFlag = true;
	}
	else if (container[slot] != pSamplerState)
	{
		container[slot] = pSamplerState;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->HSSetSamplers(slot, 1, &pSamplerState);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_DS(ID3D11SamplerState * pSamplerState, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pSamplerStates_DS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pSamplerState;
		bFlag = true;
	}
	else if (container[slot] != pSamplerState)
	{
		container[slot] = pSamplerState;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->DSSetSamplers(slot, 1, &pSamplerState);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_GS(ID3D11SamplerState * pSamplerState, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pSamplerStates_GS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pSamplerState;
		bFlag = true;
	}
	else if (container[slot] != pSamplerState)
	{
		container[slot] = pSamplerState;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->GSSetSamplers(slot, 1, &pSamplerState);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setSamplerStates_PS(ID3D11SamplerState * pSamplerState, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pSamplerStates_PS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pSamplerState;
		bFlag = true;
	}
	else if (container[slot] != pSamplerState)
	{
		container[slot] = pSamplerState;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->PSSetSamplers(slot, 1, &pSamplerState);
		return true;
	}
	return false;
}
inline bool CRendererState_D3D::setShaderResourceViews_VS(const std::vector<ID3D11ShaderResourceView*>& rpShaderResourceViews)
{
	if (m_pShaderResourceView_VS != rpShaderResourceViews)
	{
		m_pShaderResourceView_VS = rpShaderResourceViews;
		m_pDeviceContext->VSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_VS.size()), m_pShaderResourceView_VS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShaderResourceViews_HS(const std::vector<ID3D11ShaderResourceView*>& rpShaderResourceViews)
{
	if (m_pShaderResourceView_HS != rpShaderResourceViews)
	{
		m_pShaderResourceView_HS = rpShaderResourceViews;
		m_pDeviceContext->HSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_HS.size()), m_pShaderResourceView_HS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShaderResourceViews_DS(const std::vector<ID3D11ShaderResourceView*>& rpShaderResourceViews)
{
	if (m_pShaderResourceView_DS != rpShaderResourceViews)
	{
		m_pShaderResourceView_DS = rpShaderResourceViews;
		m_pDeviceContext->DSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_DS.size()), m_pShaderResourceView_DS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShaderResourceViews_GS(const std::vector<ID3D11ShaderResourceView*>& rpShaderResourceViews)
{
	if (m_pShaderResourceView_GS != rpShaderResourceViews)
	{
		m_pShaderResourceView_GS = rpShaderResourceViews;
		m_pDeviceContext->GSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_GS.size()), m_pShaderResourceView_GS.data());
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShaderResourceViews_PS(const std::vector<ID3D11ShaderResourceView*>& rpShaderResourceViews)
{
	if (m_pShaderResourceView_PS != rpShaderResourceViews)
	{
		m_pShaderResourceView_PS = rpShaderResourceViews;
		m_pDeviceContext->PSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_PS.size()), m_pShaderResourceView_PS.data());
		return true;
	}
	return false;
}
//
//inline bool CRendererState_D3D::setShaderResourceViews_VS(ID3D11ShaderResourceView * pShaderResourceView)
//{
//	if (m_pShaderResourceView_VS.size() != 1 || m_pShaderResourceView_VS[0] != pShaderResourceView)
//	{
//		m_pShaderResourceView_VS.clear();
//		m_pShaderResourceView_VS.push_back(pShaderResourceView);
//		m_pDeviceContext->VSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_VS.size()), m_pShaderResourceView_VS.data());
//		return true;
//	}
//	return false;
//}
//
//inline bool CRendererState_D3D::setShaderResourceViews_HS(ID3D11ShaderResourceView * pShaderResourceView)
//{
//	if (m_pShaderResourceView_HS.size() != 1 || m_pShaderResourceView_HS[0] != pShaderResourceView)
//	{
//		m_pShaderResourceView_HS.clear();
//		m_pShaderResourceView_HS.push_back(pShaderResourceView);
//		m_pDeviceContext->HSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_HS.size()), m_pShaderResourceView_HS.data());
//		return true;
//	}
//	return false;
//}
//
//inline bool CRendererState_D3D::setShaderResourceViews_DS(ID3D11ShaderResourceView * pShaderResourceView)
//{
//	if (m_pShaderResourceView_DS.size() != 1 || m_pShaderResourceView_DS[0] != pShaderResourceView)
//	{
//		m_pShaderResourceView_DS.clear();
//		m_pShaderResourceView_DS.push_back(pShaderResourceView);
//		m_pDeviceContext->DSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_DS.size()), m_pShaderResourceView_DS.data());
//		return true;
//	}
//	return false;
//}
//
//inline bool CRendererState_D3D::setShaderResourceViews_GS(ID3D11ShaderResourceView * pShaderResourceView)
//{
//	if (m_pShaderResourceView_GS.size() != 1 || m_pShaderResourceView_GS[0] != pShaderResourceView)
//	{
//		m_pShaderResourceView_GS.clear();
//		m_pShaderResourceView_GS.push_back(pShaderResourceView);
//		m_pDeviceContext->GSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_GS.size()), m_pShaderResourceView_GS.data());
//		return true;
//	}
//	return false;
//}
//
//inline bool CRendererState_D3D::setShaderResourceViews_PS(ID3D11ShaderResourceView * pShaderResourceView)
//{
//	if (m_pShaderResourceView_PS.size() != 1 || m_pShaderResourceView_PS[0] != pShaderResourceView)
//	{
//		m_pShaderResourceView_PS.clear();
//		m_pShaderResourceView_PS.push_back(pShaderResourceView);
//		m_pDeviceContext->PSSetShaderResources(0, static_cast<UINT>(m_pShaderResourceView_PS.size()), m_pShaderResourceView_PS.data());
//		return true;
//	}
//	return false;
//}

inline bool CRendererState_D3D::setShaderResourceViews_VS(ID3D11ShaderResourceView * pShaderResourceView, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pShaderResourceView_VS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	else if (container[slot] != pShaderResourceView)
	{
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->VSSetShaderResources(slot, 1, &pShaderResourceView);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShaderResourceViews_HS(ID3D11ShaderResourceView * pShaderResourceView, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pShaderResourceView_HS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	else if (container[slot] != pShaderResourceView)
	{
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->HSSetShaderResources(slot, 1, &pShaderResourceView);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShaderResourceViews_DS(ID3D11ShaderResourceView * pShaderResourceView, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pShaderResourceView_DS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	else if (container[slot] != pShaderResourceView)
	{
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->DSSetShaderResources(slot, 1, &pShaderResourceView);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShaderResourceViews_GS(ID3D11ShaderResourceView * pShaderResourceView, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pShaderResourceView_GS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	else if (container[slot] != pShaderResourceView)
	{
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->GSSetShaderResources(slot, 1, &pShaderResourceView);
		return true;
	}
	return false;
}

inline bool CRendererState_D3D::setShaderResourceViews_PS(ID3D11ShaderResourceView * pShaderResourceView, UINT slot)
{
	bool bFlag = false;
	auto &container = m_pShaderResourceView_PS;
	if (container.size() <= slot)
	{
		container.resize(slot + 1);
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	else if (container[slot] != pShaderResourceView)
	{
		container[slot] = pShaderResourceView;
		bFlag = true;
	}
	if (bFlag)
	{
		m_pDeviceContext->PSSetShaderResources(slot, 1, &pShaderResourceView);
		return true;
	}
	return false;
}

inline void CRendererState_D3D::revertBlend()
{
	setBlend(m_pBlendState_Past, m_fBlendFactor_Past, m_nSampleMask_Past);
}

inline void CRendererState_D3D::revertDepthStencilState()
{
	setDepthStencilState(m_pDepthStencilState_Past, m_nStencilRef_Past);
}

inline void CRendererState_D3D::revertRenderTargetViews()
{
	setRenderTargetViews(m_pRenderTargetViews_Past, m_pDepthStencilView_Past);
}

inline void CRendererState_D3D::revertDepthStencilView()
{
	setDepthStencilView(m_pDepthStencilView_Past);
}

inline void CRendererState_D3D::revertRasterizerState()
{
	setRasterizerState(m_pRasterizerState_Past);
}



#endif