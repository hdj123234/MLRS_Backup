#pragma once
#ifndef HEADER_BUILDER_DXCOMPONENT_DEFERREDLIGHTING
#define HEADER_BUILDER_DXCOMPONENT_DEFERREDLIGHTING

#include"D3DComponent_DeferredLighting.h"

class CBuilder_D3DComponent_DeferredLighting : public IBuilder_D3DComponent_DeferredLighting {
public:
	CBuilder_D3DComponent_DeferredLighting() {}
	virtual ~CBuilder_D3DComponent_DeferredLighting() {}

	virtual bool build(CD3DComponent_DeferredLighting *) ;
	virtual CD3DComponent_DeferredLighting * build() ;
};



#endif