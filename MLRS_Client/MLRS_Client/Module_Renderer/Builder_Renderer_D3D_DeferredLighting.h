#pragma once

#ifndef HEADER_BUILDER_DXRENDERER_LIGHTPOSTPROCESSING
#define HEADER_BUILDER_DXRENDERER_LIGHTPOSTPROCESSING

#include "Builder_Renderer_D3D.h"

#ifndef HEADER_STL
#include<array>
#endif

class CBuilder_Renderer_D3D_DeferredLighting :	public CBuilder_Renderer_D3D {
private:
	std::vector<ID3D11RenderTargetView *> m_pRenderTargetViews;
	std::vector<ID3D11ShaderResourceView *> m_pShaderResourceViews;

	virtual bool createBlendData(ID3D11BlendState **ppBlendState, std::array<float, 4> &rfBlendFactor, UINT &rnSampleMask);
	virtual bool createRenderTargetData(std::vector<ID3D11RenderTargetView *> &rpRenderTargetViews);
public:
	CBuilder_Renderer_D3D_DeferredLighting(	const DirectX::XMFLOAT4 &rvFogColor,
											const DirectX::XMFLOAT2 &rvFogRange)
		:CBuilder_Renderer_D3D(rvFogColor, rvFogRange){}
	virtual ~CBuilder_Renderer_D3D_DeferredLighting() {}

	//ovdrride IBuilder_Renderer_D3D
	bool build(CRenderer_D3D *pTarget);
	CRenderer_D3D * build();


};

#endif