#pragma once
#ifndef HEADER_DXCOMPONENT_DEFERREDLIGHTING
#define HEADER_DXCOMPONENT_DEFERREDLIGHTING

#include"..\Interface_Global.h"

#ifndef HEADER_STL
#include<vector>
#endif

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif

//���漱��
class CD3DComponent_DeferredLighting;

class IBuilder_D3DComponent_DeferredLighting : public IBuilder<CD3DComponent_DeferredLighting> {
public:
	IBuilder_D3DComponent_DeferredLighting() {}
	virtual ~IBuilder_D3DComponent_DeferredLighting() {}

	virtual bool build(CD3DComponent_DeferredLighting *) = 0;
	virtual CD3DComponent_DeferredLighting * build() = 0;
};


class CD3DComponent_DeferredLighting {
private:
	std::vector<ID3D11ShaderResourceView *> m_pShaderResourceViews;
	std::vector<ID3D11RenderTargetView *> m_pRenderTargetViews;

public:
	CD3DComponent_DeferredLighting() {}
	virtual ~CD3DComponent_DeferredLighting() {}

	bool createObject(IBuilder_D3DComponent_DeferredLighting *pBuilder) 
	{
		if (!pBuilder)	return false;
		return  pBuilder->build(this);
	}


	std::vector<ID3D11ShaderResourceView *> const& getShaderResourceViews()	const{return m_pShaderResourceViews;}
	std::vector<ID3D11RenderTargetView *>	const& getRenderTargetViews()	const{return m_pRenderTargetViews;}

	void setShaderResourceViews(std::vector<ID3D11ShaderResourceView *>&v) { m_pShaderResourceViews = v; }
	void setRenderTargetViews	(std::vector<ID3D11RenderTargetView *>	&v)	 { m_pRenderTargetViews=v; }
	
};

#endif