#include"stdafx.h"
#include "Builder_Renderer_D3D_DeferredLighting.h"

#include"Builder_D3DComponent_DeferredLighting.h"
#include "Renderer_D3D_DeferredLighting.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXShader.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT


bool CBuilder_Renderer_D3D_DeferredLighting::build(CRenderer_D3D * _pTarget)
{
	CRenderer_D3D_DeferredLighting *pTarget = dynamic_cast<CRenderer_D3D_DeferredLighting *>(_pTarget);
	if(!pTarget)	return false;

	ID3D11RenderTargetView * pRenderTargetView_BackBuffer = factory_RTV_BackBuffer.createRenderTargetView();
	ID3D11VertexShader *pVS = CFactory_VertexShader(std::string("vsDummy"), std::string("vsDummy")).createShader();
	ID3D11GeometryShader *pGS = CFactory_GeometryShader(std::string("gsScreen"), std::string("gsScreen")).createShader();
	ID3D11PixelShader *pPS = CFactory_PixelShader(std::string("psDeferredLighting_PostProcessing"), std::string("psDeferredLighting_PostProcessing")).createShader();
	ID3D11DepthStencilState *pDepthStencilState_Disable = factory_DepthStencilState_Disable.createDepthStencilState();
	ID3D11DepthStencilState *pDepthStencilState_Default = factory_DepthStencilState_Default.createDepthStencilState();

	CD3DComponent_DeferredLighting component;
	component.createObject(&CBuilder_D3DComponent_DeferredLighting());
	m_pRenderTargetViews = component.getRenderTargetViews();
	m_pShaderResourceViews = component.getShaderResourceViews();

	ID3D11Buffer *pLight = factory_CB_GlobalLighting.createBuffer();
	ID3D11Buffer *pCameraPos = factory_CB_CameraState.createBuffer();
	ID3D11Buffer *pFogState = factory_CB_FogState.createBuffer();

	pTarget->setShader_PostProcessing(pVS, pGS, pPS);
	pTarget->setTexture_PostProcessing(m_pShaderResourceViews);
	pTarget->setRenderTargetView_BackBuffer(pRenderTargetView_BackBuffer);
	pTarget->setDepthStencilState_PostProcessing(pDepthStencilState_Disable, pDepthStencilState_Default);
	pTarget->setPSBuffers(pLight, pCameraPos, pFogState);
	pTarget->setBlendState_PostProcessing(	factory_BlendState_Disable.createBlendState());

	return CBuilder_Renderer_D3D::build(pTarget);
}

CRenderer_D3D * CBuilder_Renderer_D3D_DeferredLighting::build()
{
	CRenderer_D3D_DeferredLighting * pRenderer = new CRenderer_D3D_DeferredLighting();
	if (!CBuilder_Renderer_D3D_DeferredLighting::build(pRenderer))		return nullptr;
	return pRenderer;
}

bool CBuilder_Renderer_D3D_DeferredLighting::createBlendData(ID3D11BlendState ** ppBlendState, std::array<float, 4>& rfBlendFactor, UINT & rnSampleMask)
{
	*ppBlendState = factory_BlendState_Default.createBlendState();
	rfBlendFactor[0] = 1;
	rfBlendFactor[1] = 1;
	rfBlendFactor[2] = 1;
	rfBlendFactor[3] = 0;
	rnSampleMask = 0xFFFFFFFF;
	return true;
}

bool CBuilder_Renderer_D3D_DeferredLighting::createRenderTargetData(std::vector<ID3D11RenderTargetView*>& rpRenderTargetViews)
{
	if(m_pRenderTargetViews.empty())	return false;
	else rpRenderTargetViews = m_pRenderTargetViews;
	return true;
}
