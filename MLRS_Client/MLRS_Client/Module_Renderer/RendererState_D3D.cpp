
#include"stdafx.h"

#include "RendererState_D3D.h"

CRendererState_D3D g_RendererState;

CRendererState_D3D * CRendererState_D3D::s_pRendererState = &g_RendererState;

CRendererState_D3D::CRendererState_D3D(ID3D11DeviceContext * pDeviceContext)
	:m_pDeviceContext(pDeviceContext),
	m_pIBuffer			( nullptr),
	m_IndexBufferFormat		( DXGI_FORMAT::DXGI_FORMAT_R32_UINT),
	m_nIndexBufferOffset	( 0		 ),
	m_pInputLayout			( nullptr),
	m_Primitive				(		 ),
	m_pRasterizerState		( nullptr),
	m_pRasterizerState_Past	( nullptr),
	m_pBlendState			( nullptr),
	m_fBlendFactor			( { 0.0f,0.0f,0.0f,0.0f }),
	m_nSampleMask			( 0xFFFFFFFF),
	m_pBlendState_Past		( nullptr),
	m_fBlendFactor_Past		(	{ 0.0f,0.0f,0.0f,0.0f }),
	m_nSampleMask_Past		( 0xFFFFFFFF),
	m_pDepthStencilState	( nullptr),
	m_nStencilRef			(		 ),
	m_pDepthStencilState_Past ( nullptr),
	m_nStencilRef_Past		(		 ),
	m_pDepthStencilView		( nullptr),	
	m_pDepthStencilView_Past( nullptr),
	m_pVS					( nullptr),
	m_pHS					( nullptr),
	m_pDS					( nullptr),
	m_pGS					( nullptr),
	m_pPS					( nullptr)
{
}

void CRendererState_D3D::setDeviceContext(ID3D11DeviceContext * pDeviceContext)
{
	m_pDeviceContext = pDeviceContext;

	m_pDeviceContext->VSSetShader(m_pVS, NULL, NULL);
	m_pDeviceContext->HSSetShader(m_pHS, NULL, NULL);
	m_pDeviceContext->DSSetShader(m_pDS, NULL, NULL);
	m_pDeviceContext->GSSetShader(m_pGS, NULL, NULL);
	m_pDeviceContext->PSSetShader(m_pPS, NULL, NULL);
}


