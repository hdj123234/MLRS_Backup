#include"stdafx.h"
#include "Builder_Renderer_D3D_MultiThread.h"

#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"
#include"D3DRenderer_MultiThread.h"
#include"RendererState_D3D.h"
#include"..\Module_MultyThread\ThreadManager_STL.h"
#include"..\Module_Scene\Scene.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT



bool CBuilder_Renderer_D3D_MultiThread::build(CRenderer_D3D * pTarget)
{
	CRenderer_D3D_MultiThread *pRenderer = dynamic_cast<CRenderer_D3D_MultiThread *>(pTarget);
	if(!pRenderer)	return false;

	IThreadManager_AsyncOperation *pThreadManager = new CThreadManager_AsyncOperation_STL();

	UINT nThreadSize = CGlobalWindow::getNumberOfCore();
	std::vector<CThreadUnit_Renderer_D3D *> m_pThreadUnits;
	std::vector<IScene *> pScenes;
	m_pThreadUnits.resize(nThreadSize);

	for (auto &data : m_pThreadUnits)
	{
		CThreadUnit_Renderer_D3D *pThreadUnit = new CThreadUnit_Renderer_D3D();
		pThreadUnit->createObject(&CBuilder_Renderer_D3D());
		pThreadUnit->setRendererState(new CRendererState_D3D());
		pThreadUnit->setDeferredContext(CGlobalDirectXDevice::createDeferredContext());
		data = pThreadUnit;
		pScenes.push_back(new CGameScene());
	}
	pRenderer->setThreadManager(pThreadManager);
	pRenderer->setThreadUnits(m_pThreadUnits);
	pRenderer->setScenes(pScenes);


	return CBuilder_Renderer_D3D::build(pTarget);
}
