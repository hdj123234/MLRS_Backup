#include"stdafx.h"
#include "Renderer_D3D_DeferredLighting.h"

#include"..\Module_Scene\Scene.h"
#include"..\Module_Object_Common\Interface_Object_Common.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"RendererState_D3D.h"


#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView.h"


#ifndef HEADER_STL
#include<random>
#endif // !HEADER_STL


CRenderer_D3D_DeferredLighting::CRenderer_D3D_DeferredLighting()
	:m_pRenderTargetView_BackBuffer(nullptr),
	m_pVS(nullptr),
	m_pGS(nullptr),
	m_pPS(nullptr),
	m_pDepthStencilState_Disable(nullptr),
	m_pDepthStencilState_Default(nullptr),
	m_pBlendState(nullptr)
{
	m_pPSTextures_Dummy.resize(4);
	for (auto &data : m_pPSTextures_Dummy)
		data = nullptr;

//	m_pDT_BS = factory_BlendState_Disable.createBlendState();
//	m_pDT_DSS = factory_DepthStencilState_Disable.createDepthStencilState();
//	m_pDT_DSS_Test = CFactory_DepthStencilState("DSS_Test", DepthStencilDesc_Test()).createDepthStencilState();
//	m_pDT_Depth = CFactory_ShaderResourceView_FromTexture2D("DSB_Default", CFactory_DepthStencilView_Default().createDepthStencilBuffer(),DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT).createShaderResourceView();
//	m_pDT_Sampler = factory_SamplerState_Point.createSamplerState();
//	m_pDT_VS = CFactory_VertexShader("vsDummy", "vsDummy").createShader();
//	m_pDT_GS = CFactory_GeometryShader("gsScreen", "gsScreen").createShader();
//	m_pDT_PS = CFactory_PixelShader("psDummy_DrawTexture", "psDummy_DrawTexture").createShader();
}

void CRenderer_D3D_DeferredLighting::clearScreen()
{
	CRenderer_D3D::clearScreen();

	m_pRendererState->getDeviceContext()->ClearRenderTargetView(m_pRenderTargetView_BackBuffer, m_fClearColor.data());

}

void CRenderer_D3D_DeferredLighting::drawReady(IScene * pScene)
{ 
	m_pRendererState->setShaderResourceViews_PS(m_pPSTextures_Dummy);

	CRenderer_D3D::drawReady(pScene);
}

void CRenderer_D3D_DeferredLighting::drawObjects(IScene * pScene)
{
	auto pScene_DL = dynamic_cast<RIScene_DeferredLighting *>(pScene);

//	drawDepth();
//	m_pDT_Depth= CGlobalDirectXStorage::getShaderResourceView("Terrain_Diffuse");
	m_pRendererState->setBlend(m_pBlendState);
	m_pRendererState->setDepthStencilState(m_pDepthStencilState_Default);
	std::vector<const RIGameObject_Drawable*>const & pObjects_NoneClear = pScene_DL->getObjects_NoneClear();
	for (auto data : pObjects_NoneClear)
	{
//		m_pRendererState->setRenderTargetView(m_pRenderTargetView_BackBuffer, m_pDepthStencilView);
//		drawReady(pScene);
//		m_pRendererState->setBlend(m_pBlendState);
//		m_pRendererState->setDepthStencilState(m_pDepthStencilState_Default);

		data->drawOnDX(m_pRendererState);
		
//		drawDepth();
	}
	m_pRendererState->revertBlend();
	m_pRendererState->setRenderTargetView(m_pRenderTargetView_BackBuffer, m_pDepthStencilView);
	std::vector<const RIGameObject_Drawable*>const & pObjects = pScene->getObjects();

	pScene->getSkyBox()->drawOnDX(m_pRendererState);

	m_pRendererState->setShader_VS(m_pVS);
	m_pRendererState->setShader_GS(m_pGS);
	m_pRendererState->setShader_PS(m_pPS);
	m_pRendererState->setShaderResourceViews_PS(m_pPSTextures);
	m_pRendererState->setConstBuffer_PS(m_pPSBuffers);
	m_pRendererState->setDepthStencilState(m_pDepthStencilState_Disable);
	m_pRendererState->getDeviceContext()->Draw(1, 0);

	m_pRendererState->setShaderResourceViews_PS(m_pPSTextures_Dummy);
//	drawDepth();

	m_pRendererState->setDepthStencilState(m_pDepthStencilState_Default);
	std::vector<const RIGameObject_Drawable*>const & pObjects_Clear = pScene_DL->getObjects_Clear();
	for (auto data : pObjects_Clear)
	{
		data->drawOnDX(m_pRendererState); 

//		drawDepth();
	}
}

void CRenderer_D3D_DeferredLighting::drawPostProcessing(IScene * pScene)
{
}

void CRenderer_D3D_DeferredLighting::setShader_PostProcessing(ID3D11VertexShader * pVS, ID3D11GeometryShader * pGS, ID3D11PixelShader * pPS)
{
	m_pVS=pVS;
	m_pGS=pGS;
	m_pPS=pPS;
}

void CRenderer_D3D_DeferredLighting::setPSBuffers(ID3D11Buffer * pLight, ID3D11Buffer * pCameraPos, ID3D11Buffer * pFogState)
{
	m_pPSBuffers.push_back(pFogState);
	m_pPSBuffers.push_back(pLight);
	m_pPSBuffers.push_back(pCameraPos);
}
//
//void CRenderer_D3D_DeferredLighting::drawDepth()
//{
//	m_pRendererState->setBlend(m_pDT_BS);
//	m_pRendererState->setDepthStencilState( m_pDT_DSS );
//	m_pRendererState->setRenderTargetView(m_pRenderTargetView_BackBuffer, nullptr);
//	m_pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
//	m_pRendererState->setShader_VS(m_pDT_VS);
//	m_pRendererState->setShader_HS(nullptr);
//	m_pRendererState->setShader_DS(nullptr);
//	m_pRendererState->setShader_GS(m_pDT_GS);
//	m_pRendererState->setShader_PS(m_pDT_PS);
//	m_pRendererState->setShaderResourceViews_PS(m_pDT_Depth);
//	m_pRendererState->setSamplerStates_PS(m_pDT_Sampler);
//	m_pRendererState->getDeviceContext()->Draw(1, 0);
//	CGlobalDirectXDevice::getSwapChain()->Present(0, 0);
//	m_pRendererState->setShaderResourceViews_PS(nullptr, 0);
//	m_pRendererState->setRenderTargetViews(m_pRenderTargetViews, m_pDepthStencilView);
//
//}
