#pragma once
#ifndef HEADER_DXRENDERER_LIGHTPOSTPRCESSING
#define HEADER_DXRENDERER_LIGHTPOSTPRCESSING

#include"Renderer_D3D.h"

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL


/*	렌더타겟 사용 여부
DSB : 동일
TX0(RGBA) : Position	(A 미사용)
TX1(RGBA) : Diffuse		(A 미사용)
TX2(RGBA) : Normal		(A 미사용)
TX3(RGBA) : Specular Factor (A : Specular Power)

확장 예정

*/

class CRenderer_D3D_DeferredLighting : public CRenderer_D3D {
private:
	ID3D11RenderTargetView * m_pRenderTargetView_BackBuffer;
	std::vector<ID3D11ShaderResourceView *> m_pPSTextures;
	std::vector<ID3D11Buffer *> m_pPSBuffers;
	ID3D11VertexShader *m_pVS;
	ID3D11GeometryShader *m_pGS;
	ID3D11PixelShader *m_pPS;
	ID3D11DepthStencilState *m_pDepthStencilState_Disable;
	ID3D11DepthStencilState *m_pDepthStencilState_Default;
	ID3D11BlendState *m_pBlendState;

//	ID3D11BlendState *m_pDT_BS;
//	ID3D11DepthStencilState *m_pDT_DSS;
//	ID3D11DepthStencilState *m_pDT_DSS_Test;
//	ID3D11ShaderResourceView *m_pDT_Depth;
//	ID3D11SamplerState *m_pDT_Sampler;
//	ID3D11VertexShader *m_pDT_VS;
//	ID3D11GeometryShader *m_pDT_GS;
//	ID3D11PixelShader *m_pDT_PS;

	std::vector<ID3D11ShaderResourceView *> m_pPSTextures_Dummy;
	//override CRenderer_D3D
	virtual void clearScreen();
	virtual void drawReady(IScene *pScene);
	virtual void drawObjects(IScene *pScene);
	virtual void drawPostProcessing(IScene *pScene);
public:
	CRenderer_D3D_DeferredLighting();
	virtual ~CRenderer_D3D_DeferredLighting() { }

	void setRenderTargetView_BackBuffer(ID3D11RenderTargetView *pRenderTargetView) { m_pRenderTargetView_BackBuffer = pRenderTargetView; }
	void setShader_PostProcessing(ID3D11VertexShader *pVS, ID3D11GeometryShader *pGS, ID3D11PixelShader *pPS);
	void setPSBuffers(ID3D11Buffer *pLight, ID3D11Buffer * pCameraPos, ID3D11Buffer * pFogState);
	void setTexture_PostProcessing(std::vector<ID3D11ShaderResourceView *> &rpPSTextures) { m_pPSTextures = rpPSTextures; }
	void setDepthStencilState_PostProcessing(ID3D11DepthStencilState *pDepthStencilState_Disable, ID3D11DepthStencilState *pDepthStencilState_Default) { m_pDepthStencilState_Disable = pDepthStencilState_Disable;m_pDepthStencilState_Default = pDepthStencilState_Default;  }
	void setBlendState_PostProcessing(ID3D11BlendState *pBlendState) { m_pBlendState = pBlendState; }

//	void drawDepth();
};



#endif