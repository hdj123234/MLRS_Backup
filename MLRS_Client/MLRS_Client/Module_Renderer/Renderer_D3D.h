#pragma once
#ifndef HEADER_RENDERER_D3D
#define HEADER_RENDERER_D3D

#include"..\Interface_Global.h"
#include"Interface_Renderer.h"

#ifndef HEADER_STL
#include<array>
#include<vector>
#endif

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif

//���漱��
class IDXScene;
class CRenderer_D3D;
class CRendererState_D3D;

class IBuilder_Renderer_D3D : public IBuilder<CRenderer_D3D> {
public:
	virtual bool build(CRenderer_D3D *pTarget) = 0;
};


class CRenderer_D3D : public IRenderer{
private:
	std::vector<D3D11_VIEWPORT> m_Viewports;
	ID3D11RasterizerState *m_pRasterizerState;
	ID3D11BlendState *m_pBlendState;
	std::array<float, 4> m_fBlendFactor;
	UINT m_nSampleMask;
	ID3D11DepthStencilState *m_pDepthStencilState;
	UINT m_nStencilRef;
	ID3D11Buffer *m_pCB_FogState;

	bool m_bReady;

protected:
	std::vector<ID3D11RenderTargetView *> m_pRenderTargetViews;
	CRendererState_D3D *m_pRendererState;
	ID3D11DepthStencilView * m_pDepthStencilView;
	std::array<float, 4> m_fClearColor;
	UINT m_nClearFlag;
	float m_fClearDepth;
	UINT8 m_nClearStencil;

	virtual void clearScreen();
	virtual void drawReady(IScene *pScene);
	virtual void drawObjects(IScene *pScene);
	virtual void drawPostProcessing(IScene *pScene) {}
	void setRendererState(CRendererState_D3D *pRendererState) { m_pRendererState = pRendererState; }
	void setDefaultRendererState();

public:
	CRenderer_D3D();
	virtual ~CRenderer_D3D() { CRenderer_D3D::release(); 	}

	//override IRenderer
	virtual void getReady() ;
	virtual void drawScene(IScene *pScene);

	bool createObject(IBuilder_Renderer_D3D *pBuilder);
	void release();

	void setRasterizer(std::vector<D3D11_VIEWPORT> &&rViewports, ID3D11RasterizerState *pRasterizerState);
	void setBlend(ID3D11BlendState *pBlendState, std::array<float, 4> &rfBlendFactor, UINT nSampleMask);
	void setDepthStencil(ID3D11DepthStencilState *pDepthStencilState, UINT nStencilRef, ID3D11DepthStencilView *pDepthStencilView);
	void setRenderTargetViews(std::vector<ID3D11RenderTargetView *> &&rpRenderTargetViews);
	void setClearData(std::array<float, 4> &rfClearColor, UINT nClearFlag = 0, float fClearDepth = 0.0f, UINT8 nClearStencil = 0);
	void setFogState(ID3D11Buffer *pCB_FogState) { m_pCB_FogState = pCB_FogState; }

};

#endif