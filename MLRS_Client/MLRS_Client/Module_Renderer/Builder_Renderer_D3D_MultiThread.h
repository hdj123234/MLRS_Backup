#pragma once

#ifndef HEADER_BUILDER_RENDERER_D3D_MULTITHREAD
#define HEADER_BUILDER_RENDERER_D3D_MULTITHREAD

#include "Renderer_D3D.h"
#include "Builder_Renderer_D3D.h"

#ifndef HEADER_STL
#include<array>
#endif

class CBuilder_Renderer_D3D_MultiThread :	public CBuilder_Renderer_D3D{
public:
	CBuilder_Renderer_D3D_MultiThread() {}
	virtual ~CBuilder_Renderer_D3D_MultiThread() {}

	//ovdrride IBuilder_Renderer_D3D
	bool build(CRenderer_D3D *pTarget) ;

	virtual bool createRenderTargetData(std::vector<ID3D11RenderTargetView *> &rpRenderTargetViews);

};

#endif