#include"stdafx.h"
#include "Builder_Renderer_D3D.h"

#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT


CBuilder_Renderer_D3D::CBuilder_Renderer_D3D(
	const DirectX::XMFLOAT4 &rvFogColor,
	const DirectX::XMFLOAT2 &rvFogRange)
	:m_pDevice(CGlobalDirectXDevice::getDevice()),
	m_vFogColor(rvFogColor),
	m_vFogRange(rvFogRange)
{
}


bool CBuilder_Renderer_D3D::build(CRenderer_D3D * pTarget)
{
	if (!m_pDevice||!pTarget) return false;


	//Rasterizer
	std::vector<D3D11_VIEWPORT> viewports;
	ID3D11RasterizerState *pRasterizerState=nullptr;
	if (!this->createRasterizerData(viewports, &pRasterizerState)) 
	{
		return false;
	}

	//OM
	std::vector<ID3D11RenderTargetView *> pRenderTargetViews;
	if (!createRenderTargetData(pRenderTargetViews)) 
	{
		return false;
	}

	ID3D11BlendState *pBlendState=nullptr;
	std::array<float, 4> fBlendFactor = {0,0,0,0};
	UINT nSampleMask=0;
	if (!this->createBlendData(&pBlendState, fBlendFactor, nSampleMask)) 
	{
		return false;
	}

	ID3D11DepthStencilState *pDepthStencilState = nullptr;
	UINT nStencilRef=0;
	ID3D11DepthStencilView * pDepthStencilView = nullptr;
	if (!this->createDepthStencilData(&pDepthStencilState, nStencilRef, &pDepthStencilView))
	{
		return false;
	}

	//ClearData
	std::array<float, 4> fClearColor;
	UINT nClearFlag=0;
	float fClearDepth=0.0f;
	UINT8 nClearStencil=0;
	if (!this->setClearData(fClearColor, nClearFlag, fClearDepth, nClearStencil)) 
	{
		return false;
	}

	ID3D11Buffer *pCB_FogState;
	if (!this->setFogState(pCB_FogState))
	{
		return false;
	}
	//	Set Data
	pTarget->setRasterizer(std::move(viewports), pRasterizerState);
	pTarget->setRenderTargetViews(std::move(pRenderTargetViews));
	pTarget->setDepthStencil(pDepthStencilState, nStencilRef, pDepthStencilView);
	pTarget->setBlend(pBlendState, fBlendFactor, nSampleMask);
	pTarget->setClearData(fClearColor, nClearFlag, fClearDepth, nStencilRef);
	pTarget->setFogState(pCB_FogState);

	return true;
}

CRenderer_D3D * CBuilder_Renderer_D3D::build()
{
	CRenderer_D3D * pRenderer = new CRenderer_D3D();
	if(!CBuilder_Renderer_D3D::build(pRenderer))		return nullptr;
	return pRenderer;
}

bool CBuilder_Renderer_D3D::createRenderTargetData(std::vector<ID3D11RenderTargetView*>& rpRenderTargetViews)
{
	//백버퍼만 사용
	ID3D11RenderTargetView *pRenderTargetView = factory_RTV_BackBuffer.createRenderTargetView();
	if (!pRenderTargetView)
	{
		MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CBuilder_Renderer_D3D.createRenderTargetData(), pRenderTargetView Empth");
		return false;
	}
	rpRenderTargetViews.push_back(pRenderTargetView);

	return true;
}

bool CBuilder_Renderer_D3D::createRasterizerData(std::vector<D3D11_VIEWPORT> &rViewports ,ID3D11RasterizerState ** ppRasterizerState)
{
	//체크
	*ppRasterizerState = factory_RasterizerState_Default.createRasterizerState();

	//윈도우 크기를 사용
	D3D11_VIEWPORT viewport = DXViewport_Default();

	rViewports.push_back(viewport);
	return true;
}

bool CBuilder_Renderer_D3D::createBlendData(ID3D11BlendState ** ppBlendState, std::array<float, 4>& rfBlendFactor, UINT & rnSampleMask)
{
	*ppBlendState = factory_BlendState_Default.createBlendState();
	rfBlendFactor[0] = 1;
	rfBlendFactor[1] = 1;
	rfBlendFactor[2] = 1;
	rfBlendFactor[3] = 1;
	rnSampleMask = 0xFFFFFFFF;

	return true;
}

bool CBuilder_Renderer_D3D::createDepthStencilData(ID3D11DepthStencilState ** ppDepthStencilState, UINT & rnStencilRef, ID3D11DepthStencilView ** ppDepthStencilView)
{
	*ppDepthStencilView = CFactory_DepthStencilView_Default().createDepthStencilView();
	*ppDepthStencilState = factory_DepthStencilState_Default.createDepthStencilState();

	return true;
}

bool CBuilder_Renderer_D3D::setClearData(std::array<float, 4>& rfClearColor, UINT &rnClearFlag, float &rfClearDepth, UINT8 &rnClearStencil)
{
	rfClearColor[0] = 1.0f;
	rfClearColor[1] = 1.0f;
	rfClearColor[2] = 1.0f;
	rfClearColor[3] = 0.0f;

	rnClearFlag = D3D11_CLEAR_DEPTH;
	rfClearDepth = 1.0f;

	return true;
}

bool CBuilder_Renderer_D3D::setFogState(ID3D11Buffer *& rpCB_FogState)
{
	rpCB_FogState = factory_CB_FogState.createBuffer();
	Type_FogState fogState;
	fogState.m_vFogColor = m_vFogColor;
	fogState.m_vFogRange = m_vFogRange;

	return CBufferUpdater::update(rpCB_FogState, fogState);
}
