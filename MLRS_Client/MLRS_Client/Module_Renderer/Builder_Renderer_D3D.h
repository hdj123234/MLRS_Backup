#pragma once

#ifndef HEADER_BUILDER_DXRENDERER
#define HEADER_BUILDER_DXRENDERER

#include "Renderer_D3D.h"

#ifndef HEADER_STL
#include<array>
#endif

class CBuilder_Renderer_D3D :	public IBuilder_Renderer_D3D{
private:
	ID3D11Device *m_pDevice;
	DirectX::XMFLOAT4 m_vFogColor;
	DirectX::XMFLOAT2 m_vFogRange;

public:
	CBuilder_Renderer_D3D(const DirectX::XMFLOAT4 &rvFogColor,
						const DirectX::XMFLOAT2 &rvFogRange);
	virtual ~CBuilder_Renderer_D3D(){}

private:
	virtual bool createRenderTargetData(std::vector<ID3D11RenderTargetView *> &rpRenderTargetViews);
	virtual bool createRasterizerData(std::vector<D3D11_VIEWPORT> &rViewports, ID3D11RasterizerState **ppRasterizerState);
	virtual bool createBlendData(ID3D11BlendState **ppBlendState, std::array<float, 4> &rfBlendFactor, UINT &rnSampleMask);
	virtual bool createDepthStencilData(ID3D11DepthStencilState **ppDepthStencilState, UINT &rnStencilRef, ID3D11DepthStencilView **ppDepthStencilView);
	virtual bool setClearData(std::array<float, 4> &rfClearColor, UINT &rnClearFlag, float &rfClearDepth, UINT8 &rnClearStencil);
	virtual bool setFogState(ID3D11Buffer * &rpCB_FogState);

public:
	//ovdrride IBuilder_Renderer_D3D
	bool build(CRenderer_D3D *pTarget);
	CRenderer_D3D * build(); 
};

#endif