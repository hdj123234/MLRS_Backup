#pragma once
#ifndef HEADER_INTERFACE_RENDERER
#define HEADER_INTERFACE_RENDERER

//���漱��
class IScene;

class IRenderer {
public:
	virtual ~IRenderer() = 0 {}

	virtual void drawScene(IScene *pScene) = 0;
	virtual void getReady() = 0;
};

class IRendererState {
public:
	virtual ~IRendererState() = 0 {}

};


#endif