#pragma once
#ifndef HEADER_DXRENDERER_MULTITHREAD
#define HEADER_DXRENDERER_MULTITHREAD

#include"Renderer_D3D.h"
#include"..\Module_MultyThread\Interface_MultiThread.h"

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL

//���漱��
struct ID3D11DeviceContext;
struct ID3D11CommandList;


class CThreadUnit_Renderer_D3D :	public CRenderer_D3D ,
								public IThreadUnit {
private:
	IScene *m_pScene;
	CRendererState_D3D *m_pRendererState;
	ID3D11CommandList *m_pCommandList;
	ID3D11DeviceContext *m_pDeferredContext;
	
	//override CRenderer_D3D
	virtual void drawObjects(IScene *pScene);

public:
	CThreadUnit_Renderer_D3D();
	virtual ~CThreadUnit_Renderer_D3D() { CThreadUnit_Renderer_D3D::release(); }

	//override IThreadUnit
	virtual int run();

	ID3D11CommandList * getCommandList();

	void setScene(IScene *pScene) { m_pScene = pScene; }
	void setRendererState(CRendererState_D3D *pRendererState);
	void setDeferredContext(ID3D11DeviceContext *pDeferredContext);

	void release();

};


class CRenderer_D3D_MultiThread : public CRenderer_D3D {
private:
	IThreadManager_AsyncOperation *m_pThreadManager;
	std::vector<CThreadUnit_Renderer_D3D *> m_pThreadUnits;
	std::vector<IScene *> m_pScenes;

	//override CRenderer_D3D
	virtual void drawObjects(IScene *pScene);
public:
	CRenderer_D3D_MultiThread() {}
	virtual ~CRenderer_D3D_MultiThread() { CRenderer_D3D_MultiThread::release(); }

	void setThreadUnits(std::vector<CThreadUnit_Renderer_D3D *> &rpThreadUnits) { m_pThreadUnits = rpThreadUnits; }
	void setScenes(std::vector<IScene *> &pScenes) { m_pScenes = pScenes; }
	void setThreadManager(IThreadManager_AsyncOperation *pThreadManager) { m_pThreadManager = pThreadManager; }
	void release();
};



#endif