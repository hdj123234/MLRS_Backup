#include"stdafx.h"
#include "Renderer_D3D_MultiThread.h"

#include"..\Module_Scene\Scene.h"
#include"..\Module_Object\Interface_Object.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"RendererState_D3D.h"
#include"..\Module_Object_Common\SkyBox.h"

#ifndef HEADER_STL
#include<random>
#endif // !HEADER_STL

//----------------------------------- CThreadUnit_Renderer_D3D -------------------------------------------------------------------------------------
CThreadUnit_Renderer_D3D::CThreadUnit_Renderer_D3D()
	:m_pScene(nullptr),
	m_pRendererState(nullptr),
	m_pCommandList(nullptr),
	m_pDeferredContext(nullptr)
{
}
void CThreadUnit_Renderer_D3D::drawObjects(IScene * pScene)
{
	std::vector<const RIGameObject_Drawable*>const & pObjects = pScene->getObjects();
	for (auto data : pObjects)
	{
		data->drawOnDX(m_pRendererState);
	}
	m_pRendererState->getDeviceContext()->FinishCommandList(TRUE, &m_pCommandList);
	m_pScene = nullptr;
}


int CThreadUnit_Renderer_D3D::run()
{ 
	CRenderer_D3D::drawReady(m_pScene);
	CThreadUnit_Renderer_D3D::drawObjects(m_pScene); 
	return 0; 
	
}

ID3D11CommandList * CThreadUnit_Renderer_D3D::getCommandList()
{
	ID3D11CommandList *result = m_pCommandList;
	m_pCommandList = nullptr;
	return result;
}

void CThreadUnit_Renderer_D3D::setRendererState(CRendererState_D3D * pRendererState)
{
	m_pRendererState = pRendererState;
	CRenderer_D3D::setRendererState(m_pRendererState);
}

void CThreadUnit_Renderer_D3D::setDeferredContext(ID3D11DeviceContext * pDeferredContext)
{
	m_pDeferredContext = pDeferredContext;
	m_pRendererState->setDeviceContext(m_pDeferredContext);
}

void CThreadUnit_Renderer_D3D::release()
{
	if (m_pRendererState)
		delete m_pRendererState;
	m_pRendererState = nullptr;

	if (m_pDeferredContext)
		m_pDeferredContext->Release();
	m_pDeferredContext = nullptr;
}

//----------------------------------- CRenderer_D3D_MultiThread -------------------------------------------------------------------------------------
void CRenderer_D3D_MultiThread::drawObjects(IScene * pScene)
{
	if (!pScene)	return;
	CRendererState_D3D *pGlobalRendererState = CRendererState_D3D::getGlobalState();
	ID3D11DeviceContext *pDeviceContext= pGlobalRendererState->getDeviceContext();

	CRenderer_D3D::clearScreen();

	UINT nThread = static_cast<UINT>(m_pThreadUnits.size());
	for (auto data : m_pScenes)
	{
		auto &container_Init = dynamic_cast<RIScene_InitData*>(data)->getObjects_Init();
		if (container_Init.empty())
		{
			auto &container2 = dynamic_cast<RIScene_InitData*>(pScene)->getObjects_Init();
			for (auto initData : container2)		data->addObject_Init(initData);
		}
		auto &container_Renewal = pScene->getObjects_Renewal();
		for (auto renewalData : container_Renewal)		data->addObject_Renewal(renewalData);
	}
	std::vector<const RIGameObject_Drawable*> pObjects = pScene->getObjects();

	//스카이박스 우선적으로 그림
	for (UINT i = 0; i < pObjects.size();++i)
	{
		const ISkyBox *pSkyBox = dynamic_cast<const ISkyBox *>(pObjects[i]);
		if (pSkyBox)
		{
			pSkyBox->drawOnDX(pGlobalRendererState);
			pObjects[i] = pObjects.back();
			pObjects.pop_back();
			break;
		}
	}

	//투명한 객체 분리
	auto iter =  std::partition(pObjects.begin(), pObjects.end(), [](RIGameObject_Drawable const *p) {
		return !(p->isClear()) ;
	});

	std::vector<RIGameObject_Drawable*> pObjects_Clear(iter, pObjects.end());
	pObjects.erase(iter, pObjects.end());

	//신 분할
	for (UINT i = 0; i < pObjects.size(); ++i)
	{
		UINT nSceneIndex = i / (static_cast<UINT>(pObjects.size()) / nThread +1 );
		m_pScenes[nSceneIndex]->addObject(pObjects[i]);
	}
	for (UINT i = 0; i < m_pThreadUnits.size(); ++i)
	{
		m_pThreadUnits[i]->setScene(m_pScenes[i]);
		m_pThreadManager->addAndRun(m_pThreadUnits[i]);
	}
	m_pThreadManager->waitAll();

	for (auto data : m_pThreadUnits)
	{
		ID3D11CommandList *pCommandList = data->getCommandList();
		pDeviceContext->ExecuteCommandList(pCommandList, TRUE);
		pCommandList->Release();
	}

	for (auto data : pObjects_Clear)
		data->drawOnDX(pGlobalRendererState);
	for(auto data : m_pScenes)
		data->clear();
}



void CRenderer_D3D_MultiThread::release()
{
	for (auto data : m_pThreadUnits)
		delete data;
	m_pThreadUnits.clear();

	for (auto data : m_pScenes)
		delete data;
	m_pScenes.clear();

	if (m_pThreadManager)
		delete m_pThreadManager;
	m_pThreadManager = nullptr;
}
