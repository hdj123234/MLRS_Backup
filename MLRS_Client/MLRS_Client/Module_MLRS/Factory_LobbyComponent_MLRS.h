#pragma once
#ifndef HEADER_FACTORY_LOBBYCOMPONENT_MLRS
#define HEADER_FACTORY_LOBBYCOMPONENT_MLRS

#include"LobbyComponent_MLRS.h"

#include"..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Factory_Material.h"
#include"..\Module_Network\Decorator_Network.h"
#include"..\MyINI.h"

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif // !HEADER_DIRECTX



class CFactory_LobbyComponent: virtual public IFactory_LobbyComponent_Drawable_MLRS,
								virtual public IFactory_GameModel,
								virtual public IFactory_Mesh_VertexAndGS{
protected:
	struct VertexData_PosAndSize {
		DirectX::XMFLOAT2 m_vPos;
		DirectX::XMFLOAT2 m_vSize;
		VertexData_PosAndSize(	const DirectX::XMFLOAT2 &rvPos,
								const DirectX::XMFLOAT2 &rvSize)
			:m_vPos(rvPos), m_vSize(rvSize) {}
	};

protected:
	const std::string m_sName;
	VertexData_PosAndSize m_VertexData;

private:
	CFactory_Material_NoneLighting_Diffuse m_Factory_Material;
	static const std::string s_sVSName;
	static const std::string s_sGSName;
	static const std::string s_sPSName;

public:
	CFactory_LobbyComponent(const std::string &rsName,
							const DirectX::XMFLOAT2 &rvPos,
							const DirectX::XMFLOAT2 &rvSize,
							const std::string &rsFileName_DiffuseMap);
	virtual ~CFactory_LobbyComponent()  {}

	//override IFactory_LobbyComponent_Drawable_MLRS
	virtual IGameModel * getModel() ;

	//override IFactory_GameModel
	virtual std::vector<IMesh			*> createMeshs() ;
	virtual std::vector<IMaterial		*> createMaterials() ;
	virtual IAnimData	* createAnimData() { return nullptr; }


	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11InputLayout	*	createInputLayout();
	virtual ID3D11Buffer		*	createVertexBuffer();
	virtual unsigned int			getStride() { return sizeof(DirectX::XMFLOAT4); }
	virtual unsigned int			getOffset() { return 0; }
	virtual unsigned int			getNumberOfVertex() { return 1; }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST; }

	//override IFactory_Mesh_GS  
	virtual ID3D11GeometryShader	* createGeometryShader();

	//override IFactory_Mesh
	virtual ID3D11VertexShader		* createVertexShader();
	virtual const unsigned int getMaterialID() { return 0; }
};

class CFactory_LobbyComponent_Button : public CFactory_LobbyComponent,
										virtual public IFactory_LobbyComponent_Button_MLRS { 
public:
	CFactory_LobbyComponent_Button(const std::string &rsName,
		const DirectX::XMFLOAT2 &rvPos,
		const DirectX::XMFLOAT2 &rvSize,
		const std::string &rsFileName_DiffuseMap );
	virtual ~CFactory_LobbyComponent_Button() {}

	virtual std::vector<CLobbyComponent_Drawable *>  getComponents();

	virtual RECT getRect();
	virtual float getOnTime() { return 0; }
};


class CFactory_LobbyComponent_Button_OnOff : virtual public IFactory_LobbyComponent_Button_MLRS { 
private:
	CFactory_LobbyComponent m_Factory1;
	CFactory_LobbyComponent m_Factory2;
	DirectX::XMFLOAT2 m_vPos;
	DirectX::XMFLOAT2 m_vSize;
public:
	CFactory_LobbyComponent_Button_OnOff(const std::string &rsName,
									const DirectX::XMFLOAT2 &rvPos,
									const DirectX::XMFLOAT2 &rvSize,
									const std::string &rsFileName_DiffuseMap_Off,
									const std::string &rsFileName_DiffuseMap_On);
	virtual ~CFactory_LobbyComponent_Button_OnOff() {}

	virtual std::vector<CLobbyComponent_Drawable *>  getComponents() ;

	virtual RECT getRect() ;
	virtual float getOnTime() { return 1; }
};




class CFactory_LobbyComponent_List :public CFactory_LobbyComponent ,
										virtual public IFactory_LobbyComponent_ButtonList_MLRS {
private:
	typedef CFactory_LobbyComponent SUPER;
private:
	const unsigned int m_nNumberOfList;
	const float m_fGap;

public:
	CFactory_LobbyComponent_List(	const std::string &rsName,
									const DirectX::XMFLOAT2 &rvPos,
									const DirectX::XMFLOAT2 &rvSize,
									const std::string &rsFileName_DiffuseMap,
									const unsigned int nNumberOfList,
									const float fGap);
	virtual ~CFactory_LobbyComponent_List() {}

	virtual IGameModel * getModel() { return SUPER::getModel(); }

	virtual ID3D11Buffer		*	createVertexBuffer();
	virtual unsigned int			getNumberOfVertex() { return m_nNumberOfList; }

	virtual std::vector<RECT> getRects() ;
};
#endif