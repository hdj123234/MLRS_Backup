#include"stdafx.h"
#include "Factory_Framework_MLRS.h"

#include"..\MyUtility.h"
#include"Factory_World_MLRS_InGame.h"
#include"Factory_World_MLRS_Lobby.h"
#include"Factory_World_MLRS_Room.h"
#include"..\Module_Timer\GameTimer.h"
//#include"..\Module_Renderer\Renderer_D3D.h"
//#include"..\Module_Renderer\Builder_Renderer_D3D.h"
#include"..\Module_World\GameWorldController_Dummy.h"
#include"..\Module_World\GameWorld.h"
#include"..\Module_Framework\Adapter_Msg_WinToFramework.h"
#include"..\Module_Network\NetworkManager.h"
#include"..\Module_MLRS_Network\PacketConverter_MLRS.h"

//----------------------------------- CFactory_Framework_MLRS -------------------------------------------------------------------------------------

CFactory_Framework_MLRS::CFactory_Framework_MLRS(const std::string & rsIniFileName) 
	:m_IniData(rsIniFileName)
{
	CMyINISection defaultSection;
	defaultSection.setParam("color", "1,1,1,0");
	defaultSection.setParam("range", "4000,5000");
	m_IniData.setSection_DefaultData("fogstate", defaultSection);
}

std::vector<IGameWorld *> CFactory_Framework_MLRS::createWorlds()
{
	std::vector<IGameWorld *> result;

	CGameWorld * pWorld = nullptr;

	pWorld = new CGameWorld();
	if (!pWorld->createObject(&CFactory_World_MLRS_Lobby(m_IniData)))
	{
		delete pWorld;
		return result;
	}
	result.push_back(pWorld);

	pWorld = new CGameWorld();
	if (!pWorld->createObject(&CFactory_World_MLRS_Room(m_IniData)))
	{
		delete pWorld;
		return result;
	}
	result.push_back(pWorld);

	pWorld = new CGameWorld();
	if (!pWorld->createObject(&CFactory_World_MLRS_InGame(m_IniData)))
	{
		delete pWorld;
		return result;
	}
	result.push_back(pWorld);


	return result;
}

IGameWorldController * CFactory_Framework_MLRS::createWorldController()
{
	return new CGameWorldController_Dummy();
}


IGameTimer* CFactory_Framework_MLRS::createTimer()
{
	IGameTimer *pTimer=nullptr;
#ifdef USE_TIMER_CHRONO
	pTimer = new CGameTimer_Chrono();
#else
#ifdef USE_TIMER_QUERYPERFORMANCECOUNTER
	pTimer = new CGameTimer_QueryPerformanceCounter();
#endif
#endif
	return pTimer;
}

IAdapter_Msg_WinToFramework * CFactory_Framework_MLRS::createMsgAdapter()
{
	CAdapter_Msg_WinToFramework *pAdapter = new CAdapter_Msg_WinToFramework();
	return pAdapter;
}

//----------------------------------- CFactory_Framework_Network_MLRS -------------------------------------------------------------------------------------

CFactory_Framework_Network_MLRS::CFactory_Framework_Network_MLRS(const std::string & rsIniFileName)
	:CFactory_Framework_MLRS(rsIniFileName)
{
	CMyINISection defaultSection;
	defaultSection.setParam("serverip", "127.0.0.1");
	defaultSection.setParam("port", "9000");
	m_IniData.setSection_DefaultData("network", defaultSection);
}

IFramework * CFactory_Framework_Network_MLRS::createFramework()
{
	CGameFramework *pFramework = new CGameFramework;
	if (!pFramework->createObject(this))
	{
		delete pFramework;
		pFramework = nullptr;
	}
	return pFramework;
}

INetworkManager * CFactory_Framework_Network_MLRS::createNetworkManager()
{
	CNetworkManager *pNetworkManager = new CNetworkManager(new CPacketConverter());
	return pNetworkManager;
}

const std::wstring CFactory_Framework_Network_MLRS::getServerIP()
{
	return m_IniData.getSection("network").getParam_wstring("serverip");
}

const short CFactory_Framework_Network_MLRS::getPort()
{
	return m_IniData.getSection("network").getParam_short("port");
}
