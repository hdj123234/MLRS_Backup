#pragma once
#ifndef HEADER_FACTORY_WORLD_MLRS_ROOM
#define HEADER_FACTORY_WORLD_MLRS_ROOM

#include"..\Module_World\GameWorld.h"
#include"..\Module_Renderer\Builder_Renderer_D3D.h"

//���漱��
class CMyINIData;

enum EType_RoomComponent : ENUMTYPE_256
{
	eImage,
	eButton_Ready,
	eButton_Exit,

};

class CFactory_World_MLRS_Room  : public IFactory_GameWorld {
private:
	const CMyINIData &m_rIniData;

public:
	CFactory_World_MLRS_Room(const CMyINIData& rIniData) :m_rIniData(rIniData){}
	virtual ~CFactory_World_MLRS_Room()  {}

private:
	void processCreateObjectsFailed(std::vector<IGameObject *> &result);

public:
	//override IFactory_GameWorld
	virtual std::vector<IGameObject *> createObjects();
	virtual IGameObjectManager * createObjectManager();
	virtual IScene* createScene();
	virtual IAdapter_Msg_ToGame* createMsgAdapter();
	virtual IRenderer* createRenderer() ;

private:
	EType_RoomComponent convertType(const std::string &rsType);
};

#endif