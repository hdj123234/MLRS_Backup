#include "stdafx.h"
#include "LobbyComponent_MLRS.h"

#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

using namespace DirectX;

//----------------------------------- CLobbyComponent_Drawable -------------------------------------------------------------------------------------

std::vector<IMsg*> CLobbyComponent_Drawable::animateObject(const float fElapsedTime)
{
	return std::vector<IMsg*>();
}

bool CLobbyComponent_Drawable::createObject(IFactory_LobbyComponent_Drawable_MLRS * pFactory)
{
	return SUPER::createObject(pFactory);
}



//----------------------------------- ALobbyComponent_Button -------------------------------------------------------------------------------------

std::vector<IMsg*> ALobbyComponent_Button::animateObject(const float fElapsedTime)
{
	if (m_bOn)
	{
		m_fOnElapsedTime += fElapsedTime;
		if (m_fOnElapsedTime > m_fOnTime)
		{
			m_fOnElapsedTime = 0;
			m_bOn = false;
		}
	} 
	return std::vector<IMsg*>();
}

void ALobbyComponent_Button::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (m_pComponents.size() == 2 && m_bOn)
		m_pComponents[1]->drawOnDX(pRendererState);
	else 	m_pComponents[0]->drawOnDX(pRendererState);
	
}

bool ALobbyComponent_Button::checkInRect(unsigned int nPosX, unsigned int nPosY) const
{
	return (nPosX > m_Rect.left&&nPosX<m_Rect.right&&nPosY>m_Rect.top&&nPosY < m_Rect.bottom);
		
}

bool ALobbyComponent_Button::createObject(IFactory_LobbyComponent_Button_MLRS * pFactory)
{
	m_Rect = pFactory->getRect();
	m_pComponents = pFactory->getComponents();
	m_fOnTime = pFactory->getOnTime();
	return true;
}

void ALobbyComponent_Button::releaseObject()
{
	for (auto pData : m_pComponents)
		if(pData)delete pData;
	m_pComponents.clear();
}

//----------------------------------- CLobbyComponent_MakeRoom -------------------------------------------------------------------------------------

IMsg * CLobbyComponent_MakeRoom::click()
{
	ALobbyComponent_Button::setOn();
	return new CMsg_CS_CreateRoom();
}
//----------------------------------- CLobbyComponent_ButtonList -------------------------------------------------------------------------------------


IMsg * CLobbyComponent_Refresh::click()
{
	return nullptr;
}

//----------------------------------- CLobbyComponent_ButtonList -------------------------------------------------------------------------------------

bool CLobbyComponent_RoomList::createObject(IFactory_LobbyComponent_ButtonList_MLRS * pFactory)
{
	m_Rects = pFactory->getRects();
	return SUPER::createObject(pFactory);
}

bool CLobbyComponent_RoomList::checkInRect(unsigned int nPosX, unsigned int nPosY) const
{
	for (auto &rData : m_Rects)
	{
		if (nPosX > rData.left&&nPosX<rData.right&&nPosY>rData.top&&nPosY < rData.bottom)
			return true;
	}
	return false;
}

IMsg * CLobbyComponent_RoomList::click()
{
//	MessageBox(CGlobalWindow::getHWnd(),L"Check",L"Check",MB_OK);
	return nullptr;
}
