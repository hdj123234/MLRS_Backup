
#ifndef HEADER_GLOBALVSINPUTMAP
#define HEADER_GLOBALVSINPUTMAP

#ifndef HEADER_STL
#include<map>
#include<vector>
#endif // !HEADER_STL

#ifndef HEADER_DIRECTX
#include<d3d11.h>
#endif // !HEADER_DIRECTX


class CGlobalVSInputMap {
private:
	typedef std::map<std::string, std::vector<D3D11_INPUT_ELEMENT_DESC>> THISMAP;
private:
	static const THISMAP s_Map;

	CGlobalVSInputMap() {}
	~CGlobalVSInputMap() {}

public:
	static std::vector<D3D11_INPUT_ELEMENT_DESC> getInputElementDesc(const std::string &rsVSName)
	{
		auto iter_End = s_Map.end();
		auto iter = s_Map.find(rsVSName);
		if (iter == iter_End)
			return std::vector<D3D11_INPUT_ELEMENT_DESC>();
		else
			return iter->second;
	}
};


#endif