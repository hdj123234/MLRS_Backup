#pragma once
#ifndef HEADER_FACTORY_FRAMEWORK_MLRS
#define HEADER_FACTORY_FRAMEWORK_MLRS
#include"..\Module_Framework\GameFramework.h"
#include"..\Module_Network\Decorator_Network.h"
#include"..\MyINI.h"


class CFactory_Framework_MLRS  : public IFactory_GameFramework {
protected:
	CMyINIData m_IniData;
public:
	CFactory_Framework_MLRS(const std::string &rsIniFileName);
	virtual ~CFactory_Framework_MLRS()  {}

	//override IFactory_GameFramework
	std::vector<IGameWorld *> createWorlds() ;
	IGameWorldController * createWorldController() ;
//	IRenderer* createPrinter() ;
	IGameTimer* createTimer() ;
	IAdapter_Msg_WinToFramework * createMsgAdapter() ;

};

class CFactory_Framework_Network_MLRS :virtual  public IFactory_GameFramework_NetworkDecorator,
										public CFactory_Framework_MLRS {
public:
	CFactory_Framework_Network_MLRS(const std::string &rsIniFileName);
	virtual ~CFactory_Framework_Network_MLRS() {}

	//virtual function
	virtual IFramework * createFramework() ;
	virtual INetworkManager * createNetworkManager() ;
	virtual const std::wstring getServerIP() ;
	virtual const short getPort() ;

};
#endif