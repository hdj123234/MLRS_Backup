﻿#include"stdafx.h"

#include "Factory_World_MLRS_InGame.h"
#include"..\Module_Renderer\Renderer_D3D_DeferredLighting.h"
#include"..\Module_Renderer\Builder_Renderer_D3D_DeferredLighting.h"

#include"..\Module_MLRS_Object\Factory_Fume.h"
#include"..\Module_MLRS_Object\Creator_BossMecha_MLRS.h"
#include"..\Module_MLRS_Object\Factory_UIManager_MLRS.h"
#include"..\Module_MLRS_Object\Factory_BaseCamp_MLRS.h"
#include"..\Module_MLRS_Object\UIManager_MLRS.h"
#include"..\Module_MLRS_Object\Creator_MainMecha_MLRS.h"
#include"..\Module_MLRS_Object\Creator_EnemyMecha_MLRS.h"
#include"..\Module_MLRS_Object\Creator_Missile_MLRS.h"
#include"..\Module_MLRS_Object\Missile_MLRS.h"
#include"..\Module_MLRS_Object\LightTester.h"
#include"..\Module_Object_Common\Factory_SkyBox.h"
#include"..\Module_Object_Common\Builder_EffectType_Billboard.h"
#include"..\Module_Object_Common\Factory_Terrain_Tess.h"
#include"..\Module_Object_Camera\Factory_Camera_ThirdPerson.h"
#include"..\Module_Object_Common\Factory_LightManager.h"
#include"..\Module_Object_UI\UIManager.h"
#include"..\Module_Object_Common\Light.h"
#include"..\Module_Sound\Factory_SoundManager.h"
#include"..\Module_Sound\GlobalXAudio2Engine.h"
//#include"..\Module_Scene\Scene.h"
#include"..\Module_Scene\Scene_DeferredLighting.h"
#include"Adapter_Msg_ToGame_MLRS_InGame.h"
#include"GameObjectManager_MLRS_InGame.h"
#include"..\Module_MLRS_Object\Creator_Model_CustomFBX.h" 

#include"..\MyUtility.h"
#include"..\MyINI.h"
#include"..\Module_MLRS_Object\GlobalDataMap_MLRS.h"


//DEBUG
#include"..\Module_Object_Common\Painter_Common.h"
#include"..\Module_Object_Common\Factory_Painter_Common.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT

#ifndef HEADER_STL
#include<iostream>
#include<sstream>
#include<iomanip>
#endif // !HEADER_MYLOGCAT

using namespace DirectX;

void CFactory_World_MLRS_InGame::processCreateObjectsFailed(std::vector<IGameObject*>& result)
{
	MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CFactory_World_MLRS_InGame.createObjects() Error");
	for (auto data : result)
		delete data;
	result.clear();
}

std::vector<IGameObject *> CFactory_World_MLRS_InGame::createObjects()
{
	std::vector<IGameObject *> result;

	//Sound
	{
		auto &rSection = m_rIniData.getSection("Sound");
		CSoundManager *pSoundManager = new CSoundManager;
		if (!pSoundManager->createObjects(&CFactory_SoundManager(rSection.getParam("SoundTable")))){
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pSoundManager);
		
		CGlobalXAudio2Engine::setMasterVolume(rSection.getParam_float("MasterVolume"));
	}


	CSkyBox *pSkyBox = new CSkyBox();
	if(!pSkyBox->createObject(&CFactory_SkyBox(std::string("SkyBox.dds"))))	{
		processCreateObjectsFailed(result);
		return result;
	};
	result.push_back(pSkyBox);
	 
	{
		std::vector<std::string> sDetailMaps;
		std::vector<std::string> sAlphaMaps;

		for (int i = 1;; ++i)
		{
			auto &rSection_Splatting = m_rIniData.getSection("Terrain_TextureSplattingLayer" + std::to_string(i));
			if (rSection_Splatting.isEmpty())	break;
			sDetailMaps.push_back(rSection_Splatting.getParam("Detail"));
			sAlphaMaps.push_back(rSection_Splatting.getParam("Alpha"));
		}
		CTerrain *pTerrain = new CTerrain();
		if (!pTerrain->createObject(&CFactory_Terrain_Tess_Splatting(	"Terrain",
																		m_rIniData.getSection("Terrain").setDefaultData(CFactory_Terrain_Tess::getDefaultINISection()),
																		sDetailMaps, sAlphaMaps,true))) {
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pTerrain);
	}
	
	CStrategy_CreateModel_CustomFBX_Material::setDeferredLighting(true);

	IGameModel *pModel = nullptr;
	//플레이어 
	{
		auto &rSection = m_rIniData.getSection("Model_Abattre");
		rSection.setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model_Abattre());
		CFactory_FBXModel factory_Model_MainMecha(rSection.getParam("FileName"),
													new CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim(	4, 
																										CGlobalDataMap_MLRS::getStartPointList(rSection.getParam("AnimationData")),
																										60),
													new CStrategy_CreateModel_CustomFBX_Material());
		CBuilder_FBXModel_Instancing builder_Model_MainMecha(factory_Model_MainMecha);
		pModel = builder_Model_MainMecha.build();
		if (!pModel)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pModel);
		CGlobalDataMap_MLRS::setPlayerModel(EPlayerType::eAbattre, pModel);
		
		IGameObject *pObject = nullptr;
		
		CameraState_MLRS cameraState(m_rIniData);

		auto &rSection_Weapon1 = m_rIniData.getSection("Weapon1");
		auto &rSection_Weapon2 = m_rIniData.getSection("Weapon2");
		auto &rSection_Weapon3 = m_rIniData.getSection("Weapon3");
		rSection_Weapon1.setDefaultData(CFactory_MainMecha_Abattre_Instanced::getDefaultINISection_Weapon1());
		rSection_Weapon2.setDefaultData(CFactory_MainMecha_Abattre_Instanced::getDefaultINISection_Weapon2());
		rSection_Weapon3.setDefaultData(CFactory_MainMecha_Abattre_Instanced::getDefaultINISection_Weapon3());


		CMultiBuilder_MainMecha_Instanced multiBuilder_Main(pModel,
															cameraState,
															factory_Model_MainMecha.getGuides(),
															rSection_Weapon1, rSection_Weapon2, rSection_Weapon3);
		pObject = multiBuilder_Main.build_ClientPlayer();
		if (!pObject)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pObject);

		//CPainter_Bone *pBone = new CPainter_Bone;
		//pBone->createObject(&CFactory_Mesh_Painter_Bone( builder_Model_MainMecha.getBoneStructure(), builder_Model_MainMecha.getFrameData()));
		//result.push_back(pBone);
		
		for (unsigned int i = 0; i < 3; ++i)
		{
			pObject = multiBuilder_Main.build();
			if (!pObject)
			{
				processCreateObjectsFailed(result);
				return result;
			}
			result.push_back(pObject);
			dynamic_cast<IMainMecha *>(pObject)->disable();
		} 

	}

	{
		auto &rSection = m_rIniData.getSection("Model_Zephyros");
		rSection.setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eZephyros));
		CFactory_FBXModel factory_Model_Zephyros(rSection.getParam("FileName"),
			new CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim(100,
				CGlobalDataMap_MLRS::getStartPointList(rSection.getParam("AnimationData")),
				60),
			new CStrategy_CreateModel_CustomFBX_Material());
		CBuilder_FBXModel_Instancing builder_Model_Enemy_Zephyros(factory_Model_Zephyros);
		pModel = builder_Model_Enemy_Zephyros.build();
		if (!pModel)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pModel);
		CGlobalDataMap_MLRS::setEnemyModel(EEnemyType::eZephyros, pModel);
	}

	{
		auto &rSection = m_rIniData.getSection("Model_Combat");
		rSection.setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eCombat));

		CFactory_FBXModel factory_Model_Combat(rSection.getParam("FileName"),
			new CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim(100,
				CGlobalDataMap_MLRS::getStartPointList(rSection.getParam("AnimationData")),
				60),
			new CStrategy_CreateModel_CustomFBX_Material());
		CBuilder_FBXModel_Instancing builder_Model_Enemy_Combat(factory_Model_Combat);
		pModel = builder_Model_Enemy_Combat.build();
		if (!pModel)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pModel);
		CGlobalDataMap_MLRS::setEnemyModel(EEnemyType::eCombat, pModel);
	}
	{
		auto &rSection = m_rIniData.getSection("Model_TrollBatRider");
		rSection.setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eTrollBatRider));

		CFactory_FBXModel factory_Model_TrollBatRider(rSection.getParam("FileName"),
			new CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim(100,
				CGlobalDataMap_MLRS::getStartPointList(rSection.getParam("AnimationData")),
				60),
			new CStrategy_CreateModel_CustomFBX_Material());
		CBuilder_FBXModel_Instancing builder_Model_Enemy_TrollBatRider(factory_Model_TrollBatRider);
		pModel = builder_Model_Enemy_TrollBatRider.build();
		if (!pModel)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pModel);
		CGlobalDataMap_MLRS::setEnemyModel(EEnemyType::eTrollBatRider, pModel);
	}

	//보스 
	{
		auto &rSection = m_rIniData.getSection("Model_Boss");
		rSection.setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model_Boss());

		CFactory_FBXModel factory_Model_Boss(rSection.getParam("FileName"),
			new CStrategy_CreateModel_CustomFBX_Mesh_Anim(
				CGlobalDataMap_MLRS::getStartPointList(rSection.getParam("AnimationData")),
				60),
			new CStrategy_CreateModel_CustomFBX_Material());
		CBuilder_FBXModel builder_Model_Boss(factory_Model_Boss);
		pModel = builder_Model_Boss.build();
		if (!pModel)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pModel);
		auto pBossMecha = new CBossMecha();
		if (!pBossMecha->createObject(&CFactory_BossMecha("Boss", pModel)))
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pBossMecha);
		pBossMecha->disable();
	}
	
	//베이스 캠프  
	{
		auto &rSection = m_rIniData.getSection("Model_BaseCamp");
		rSection.setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model_BaseCamp());

		CFactory_FBXModel factory_Model_BaseCamp(rSection.getParam("FileName"),
			new CStrategy_CreateModel_CustomFBX_Mesh_Anim(CGlobalDataMap_MLRS::getStartPointList(rSection.getParam("AnimationData")),
				60),
			new CStrategy_CreateModel_CustomFBX_Material());
		CBuilder_FBXModel builder_Model_BaseCamp(factory_Model_BaseCamp);
		pModel = builder_Model_BaseCamp.build();
		if (!pModel)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pModel);
		auto pBaseCamp = new CBaseCamp();
		if (!pBaseCamp->createObject(&CFactory_BaseCamp(pModel)))
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pBaseCamp);
	}

	//미사일
	for (int i = 1;; ++i)
	{
		auto &rSection_Missile = m_rIniData.getSection("Model_Missile" + std::to_string(i));
		if (rSection_Missile.isEmpty())	break; 
		CFactory_FBXModel factory_Model_Missile(rSection_Missile,
			new CStrategy_CreateModel_CustomFBX_Mesh_Instancing_NoAnim(300),
			new CStrategy_CreateModel_CustomFBX_Material());
		CBuilder_FBXModel_Instancing builder_Missile_Enemy(factory_Model_Missile);
		pModel = builder_Missile_Enemy.build();
		if (!pModel)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pModel);
		CGlobalDataMap_MLRS::setMissileModel(static_cast<EMissileType>(i), pModel);
	}

	//효과
	CEffectManager *pEffectManager = new CEffectManager();
	result.push_back(pEffectManager);

	for (int i = 1;; ++i)
	{
		auto &rSection_Effect = m_rIniData.getSection("Effect" + std::to_string(i));
		if (rSection_Effect.isEmpty())	break;

		CEffectType effectType ;
		rSection_Effect.setDefaultData(CBuilder_EffectType_Billboard::getDefaultINISection());
		unsigned int nStart = rSection_Effect.getParam_uint("IndexStart");
		unsigned int nEnd = rSection_Effect.getParam_uint("IndexEnd") + 1;
		std::string sName = rSection_Effect.getParam("Filename");
		std::string sExpension = rSection_Effect.getParam("Expension");
		unsigned int nNumberLength = rSection_Effect.getParam_uint("NumberLength");
		DirectX::XMFLOAT2 vEffectSize = rSection_Effect.getParam_XMFLOAT2("Size");
		std::vector<std::string> effectFileNames;
		for (unsigned int i = nStart; i < nEnd; ++i)
		{
			std::string sTmp;
			std::stringstream ss;
			ss << sName << std::setw(nNumberLength) << std::setfill('0') << i << '.' << sExpension;
			ss >> sTmp;
			effectFileNames.push_back(sTmp);
		}
		if (CBuilder_EffectType_Billboard("EffectManager", vEffectSize, 500, effectFileNames).build(&effectType))
			pEffectManager->addEffectType(effectType);
		else
		{
			processCreateObjectsFailed(result);
			return result;
		}
	}

	//UI
	{
		auto &rSection = m_rIniData.getSection("UI");
		rSection.setDefaultData(CFactory_UIManager_MLRS::getDefaultINISection());

		CUIManager_MLRS *pUIManager = new CUIManager_MLRS();
		 
		if (!pUIManager->createObject(&CFactory_UIManager_MLRS(rSection.getParam("INIFile"), rSection.getParam("ScriptTable"))))
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pUIManager);
	}
	//조명
	auto &rSection_GlobalLight = m_rIniData.getSection("GlobalLight");
	rSection_GlobalLight.setDefaultData(CFactory_LightManager::getDefaultINISection_GlobalLight());

	CLightManager *pLightManager = new CLightManager();
	if (!pLightManager->createObject(&CFactory_LightManager(rSection_GlobalLight.getParam_XMFLOAT4("GlobalAmbient") ))) {
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pLightManager);

	
//	result.push_back(new CLightTester);
	for (int i = 1;; ++i)
	{
		auto &rSection_Light = m_rIniData.getSection( "Light" + std::to_string(i) );
		if (rSection_Light.isEmpty())	break;
		if (rSection_Light.getParam("Type") == "directional")
		{
			CLight_Directional *pLight = new CLight_Directional();
			pLight->setAmbient(rSection_Light.getParam_XMFLOAT4("Ambient"));
			pLight->setDiffuse(rSection_Light.getParam_XMFLOAT4("Diffuse"));
			pLight->setSpecular(rSection_Light.getParam_XMFLOAT4("Specular"));
			pLight->setDirection(rSection_Light.getParam_XMFLOAT3_Normalize("Direction"));

			result.push_back(pLight);
			pLightManager->addLight(pLight);
		}

		else if (rSection_Light.getParam("Type") == "point")
		{
			//체크
			CLight_Point *pLight = new CLight_Point();
			pLight->setAmbient(rSection_Light.getParam_XMFLOAT4("Ambient"));
			pLight->setDiffuse(rSection_Light.getParam_XMFLOAT4("Diffuse"));
			pLight->setSpecular(rSection_Light.getParam_XMFLOAT4("Specular"));
			pLight->setPosition(rSection_Light.getParam_XMFLOAT3("Position"));
			pLight->setAttenuation(rSection_Light.getParam_XMFLOAT3("Attenuation"));
			pLight->setMaxRange(rSection_Light.getParam_float("MaxRange"));

			result.push_back(pLight);
			pLightManager->addLight(pLight);
		}

		else if (rSection_Light.getParam("Type") == "spot")
		{
			//체크
			CLight_Spot *pLight = new CLight_Spot();
			pLight->setAmbient(rSection_Light.getParam_XMFLOAT4("Ambient"));
			pLight->setDiffuse(rSection_Light.getParam_XMFLOAT4("Diffuse"));
			pLight->setSpecular(rSection_Light.getParam_XMFLOAT4("Specular"));
			pLight->setPosition(rSection_Light.getParam_XMFLOAT3("Position"));
			pLight->setDirection(rSection_Light.getParam_XMFLOAT3_Normalize("Direction"));
			pLight->setAttenuation(rSection_Light.getParam_XMFLOAT3("Attenuation"));
			pLight->setMaxRange(rSection_Light.getParam_float("MaxRange"));

			result.push_back(pLight);
			pLightManager->addLight(pLight);
		}
	}

	//Fume 
	for (int i = 1;; ++i)
	{
		std::string sFumeName = "Fume" + std::to_string(i);
		auto &rSection_Fume = m_rIniData.getSection(sFumeName);
		if (rSection_Fume.isEmpty())	break;
		 
		rSection_Fume.setDefaultData(CFactory_Fume::getDefaultINISection());
		unsigned int nStart = rSection_Fume.getParam_uint("IndexStart");
		unsigned int nEnd = rSection_Fume.getParam_uint("IndexEnd") + 1;
		std::string sName = rSection_Fume.getParam("Filename");
		std::string sExpension = rSection_Fume.getParam("Expension");
		unsigned int nNumberLength = rSection_Fume.getParam_uint("NumberLength");
		DirectX::XMFLOAT2 vFumeSize = rSection_Fume.getParam_XMFLOAT2("Size");
		float fVariationAngle = rSection_Fume.getParam_float("VariationAngle");
		float fInitialAngle= rSection_Fume.getParam_float("InitialSpeed");
		float fSpeedAttenuation= rSection_Fume.getParam_float("SpeedAttenuation");
		
		std::vector<std::string> fumeFileNames;
		for (unsigned int i = nStart; i < nEnd; ++i)
		{
			std::string sTmp;
			std::stringstream ss;
			ss << sName << std::setw(nNumberLength) << std::setfill('0') << i << '.' << sExpension;
			ss >> sTmp;
			fumeFileNames.push_back(sTmp);
		}
		CFume *pFume = new CFume();
		if (!pFume->createObject(&CFactory_Fume(sFumeName, vFumeSize, 
												300, 2000, 
												fVariationAngle, fInitialAngle, fSpeedAttenuation, 
												fumeFileNames))) {
			processCreateObjectsFailed(result);
			return result;
		}  
		result.push_back(pFume);
	}
	 

	//DEBUG
	auto * pBBManager = new CPainter_BoundingBoxManager();
	result.push_back(pBBManager);
	auto * pLine = new CPainter_Line();
	pLine->createObject(&CFactory_Painter_Line());
	result.push_back(pLine);

	return result;
}
IGameObjectManager * CFactory_World_MLRS_InGame::createObjectManager()
{
	CGameObjectManager_MLRS_InGame *pResult = new CGameObjectManager_MLRS_InGame();
	return pResult;

}
IScene* CFactory_World_MLRS_InGame::createScene()
{
//	CScene *pResult = new CScene();
	CScene_DeferredLighting *pResult = new CScene_DeferredLighting();
	return pResult;
}

IAdapter_Msg_ToGame * CFactory_World_MLRS_InGame::createMsgAdapter()
{
	CAdapter_Msg_ToGame_MLRS_InGame* pMsgAdapter = new CAdapter_Msg_ToGame_MLRS_InGame();
	return pMsgAdapter;
}

IRenderer * CFactory_World_MLRS_InGame::createRenderer()
{
	auto vColor = m_rIniData.getSection("fogstate").getParam_XMFLOAT4("color");
	auto vRange = m_rIniData.getSection("fogstate").getParam_XMFLOAT2("range");

	//	CRenderer_D3D *pRenderer=new CRenderer_D3D();
	//	if (!CBuilder_Renderer_D3D(vColor, vRange).build(pRenderer))
	CRenderer_D3D_DeferredLighting *pRenderer = new CRenderer_D3D_DeferredLighting();
	if (!CBuilder_Renderer_D3D_DeferredLighting(vColor, vRange).build(pRenderer))
	{
		delete pRenderer;
		return nullptr;
	}
	return pRenderer;
} 
