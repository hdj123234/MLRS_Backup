#include"stdafx.h"

#include "Factory_World_MLRS.h"

#include"..\Module_MLRS_Object\Creator_BossMecha_MLRS.h"
#include"..\Module_MLRS_Object\Factory_UIManager_MLRS.h"
#include"..\Module_MLRS_Object\Factory_BaseCamp_MLRS.h"
#include"..\Module_MLRS_Object\UIManager_MLRS.h"
#include"..\Module_MLRS_Object\Creator_MainMecha_MLRS.h"
#include"..\Module_MLRS_Object\Creator_EnemyMecha_MLRS.h"
#include"..\Module_MLRS_Object\Creator_Missile_MLRS.h"
#include"..\Module_MLRS_Object\Missile_MLRS.h"
#include"..\Module_MLRS_Object\LightTester.h"
#include"Adapter_Msg_ToGame_MLRS.h"
#include"GameObjectManager_Dummy.h"
#include"Dummy_Creator_FBXModel.h"
#include"..\Module_Object_Common\Factory_SkyBox.h"
#include"..\Module_Object_Common\Builder_EffectType_Billboard.h"
#include"..\Module_Object_Common\Factory_Terrain_Tess.h"
#include"..\Module_Object_Camera\Factory_Camera_ThirdPerson.h"
#include"..\Module_Object_Common\Factory_LightManager.h"
#include"..\Module_Object_Common\UIManager.h"
#include"..\Module_Object_Common\Light.h"
//#include"..\Module_Scene\Scene.h"
#include"..\Module_Scene\Scene_DeferredLighting.h"

#include"..\MyUtility.h"
#include"..\MyINI.h"
#include"..\Module_MLRS_Object\GlobalDataMap_MLRS.h"


//DEBUG
#include"..\Module_Object_Common\DummyPainter.h"
#include"..\Module_Object_Common\Factory_DummyPainter.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT

#ifndef HEADER_STL
#include<iostream>
#include<sstream>
#include<iomanip>
#endif // !HEADER_MYLOGCAT

using namespace DirectX;

void CFactory_World_InGame_MLRS::processCreateObjectsFailed(std::vector<IGameObject*>& result)
{
	MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CFactory_World_InGame_MLRS.createObjects() Error");
	for (auto data : result)
		delete data;
	result.clear();
}

std::vector<IGameObject *> CFactory_World_InGame_MLRS::createObjects()
{
	std::vector<IGameObject *> result;


	CSkyBox *pSkyBox = new CSkyBox();
	if(!pSkyBox->createObject(&CFactory_SkyBox(std::string("SkyBox.dds"))))	{
		processCreateObjectsFailed(result);
		return result;
	};
	result.push_back(pSkyBox);
	
	
	CTerrain *pTerrain = new CTerrain();
	if (!pTerrain->createObject(&CFactory_Terrain_Tess("Terrain",m_rIniData.getSection("Terrain").setDefaultData(CFactory_Terrain_Tess::getDefaultINISection())))) {
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pTerrain);
	
	IGameModel *pModel = nullptr;
	
	
	//플레이어
	//CBuilder_FBXModel_Instancing_Anim builder_Model_MainMecha( m_rIniData.getSection("Model_Abattre"),
	//	4,
	//	CFactory_MainMecha_Abattre_Instanced::getStartPointList()	);
	CFactory_FBXModel factory_Model_MainMecha(	m_rIniData.getSection("Model_Abattre"),
												new CStrategy_CreateFBXModel_Mesh_Instancing_Anim(	4, 
																									CFactory_MainMecha_Abattre_Instanced::getStartPointList(),
																									60),
												new CStrategy_CreateFBXModel_Material());
	CBuilder_FBXModel_Instancing builder_Model_MainMecha(factory_Model_MainMecha);
	pModel = builder_Model_MainMecha.build();
	if (!pModel)
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pModel);
	CGlobalDataMap_MLRS::setPlayerModel(EPlayerType::eAbattre, pModel);
	
	IGameObject *pObject = nullptr;
	
	CameraState_MLRS cameraState(m_rIniData);

	auto &rSection_Weapon1 = m_rIniData.getSection("Weapon1");
	rSection_Weapon1.setDefaultData(CFactory_MainMecha_Abattre_Instanced::getDefaultINISection_Weapon1());


	CMultiBuilder_MainMecha_Instanced multiBuilder_Main(pModel,
														cameraState,
														rSection_Weapon1.getParam_XMFLOAT3("Pos"),
														rSection_Weapon1.getParam_float("ShootDelay"));
	pObject = multiBuilder_Main.build_ClientPlayer();
	if (!pObject)
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pObject);

//	CPainter_Bone *pBone = new CPainter_Bone;
//	pBone->createObject(&CFactory_Mesh_Painter_Bone( builder_Model_MainMecha.getBoneStructure(), builder_Model_MainMecha.getFrameData()));
//	result.push_back(pBone);
	
	for (unsigned int i = 0; i < 3; ++i)
	{
		pObject = multiBuilder_Main.build();
		if (!pObject)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pObject);
		dynamic_cast<IMainMecha *>(pObject)->disable();
	}
	
	//CBuilder_FBXModel_Instancing_Anim builder_Model_Enemy_Zephyros(
	//	m_rIniData.getSection("Model_Zephyros").setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eZephyros)),
	//	100,
	//	CGlobalDataMap_MLRS::getStartPointList(eZephyros));

	CFactory_FBXModel factory_Model_Zephyros(m_rIniData.getSection("Model_Zephyros").setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eZephyros)),
		new CStrategy_CreateFBXModel_Mesh_Instancing_Anim(100,
			CGlobalDataMap_MLRS::getStartPointList(eZephyros),
			60),
		new CStrategy_CreateFBXModel_Material());
	CBuilder_FBXModel_Instancing builder_Model_Enemy_Zephyros(factory_Model_Zephyros);
	pModel = builder_Model_Enemy_Zephyros.build();
	if (!pModel)
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pModel);
	CGlobalDataMap_MLRS::setEnemyModel(EEnemyType::eZephyros, pModel);

	//CBuilder_FBXModel_Instancing_Anim builder_Model_Enemy_Combat(
	//	m_rIniData.getSection("Model_Combat").setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eCombat)),
	//	100,
	//	CGlobalDataMap_MLRS::getStartPointList(eCombat));
	CFactory_FBXModel factory_Model_Combat(m_rIniData.getSection("Model_Combat").setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eCombat)),
		new CStrategy_CreateFBXModel_Mesh_Instancing_Anim(100,
			CGlobalDataMap_MLRS::getStartPointList(eCombat),
			60),
		new CStrategy_CreateFBXModel_Material());
	CBuilder_FBXModel_Instancing builder_Model_Enemy_Combat(factory_Model_Combat);
	pModel = builder_Model_Enemy_Combat.build();
	if (!pModel)
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pModel);
	CGlobalDataMap_MLRS::setEnemyModel(EEnemyType::eCombat, pModel);

	//CBuilder_FBXModel_Instancing_Anim builder_Model_Enemy_TrollBatRider(
	//	m_rIniData.getSection("Model_TrollBatRider").setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eTrollBatRider)),
	//	100,
	//	CGlobalDataMap_MLRS::getStartPointList(eTrollBatRider));
	CFactory_FBXModel factory_Model_TrollBatRider(m_rIniData.getSection("Model_TrollBatRider").setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eTrollBatRider)),
		new CStrategy_CreateFBXModel_Mesh_Instancing_Anim(100,
			CGlobalDataMap_MLRS::getStartPointList(eTrollBatRider),
			60),
		new CStrategy_CreateFBXModel_Material());
	CBuilder_FBXModel_Instancing builder_Model_Enemy_TrollBatRider(factory_Model_TrollBatRider);
	pModel = builder_Model_Enemy_TrollBatRider.build();
	if (!pModel)
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pModel);
	CGlobalDataMap_MLRS::setEnemyModel(EEnemyType::eTrollBatRider, pModel);


	//보스 
	//pModel = CBuilder_FBXModel_Anim(m_rIniData.getSection("Model_Boss").setDefaultData(CFactory_BossMecha::getDefaultINISection())
	CFactory_FBXModel factory_Model_Boss(m_rIniData.getSection("Model_Boss").setDefaultData(CFactory_BossMecha::getDefaultINISection()),
		new CStrategy_CreateFBXModel_Mesh_Anim(CFactory_BossMecha::getStartPointList(),
			60),
		new CStrategy_CreateFBXModel_Material());
	CBuilder_FBXModel builder_Model_Boss(factory_Model_Boss);
	pModel = builder_Model_Boss.build();
	if (!pModel)
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pModel);
	auto pBossMecha = new CBossMecha();
	if (!pBossMecha->createObject(&CFactory_BossMecha("Boss",pModel)))
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pBossMecha);
	pBossMecha->disable();

	//베이스 캠프 
	//pModel = CBuilder_FBXModel_Anim(m_rIniData.getSection("Model_BaseCamp").setDefaultData(CFactory_BaseCamp::getDefaultINISection())
	//	, CFactory_BaseCamp::getStartPointList()).build();
	CFactory_FBXModel factory_Model_BaseCamp(m_rIniData.getSection("Model_BaseCamp").setDefaultData(CFactory_BaseCamp::getDefaultINISection()),
		new CStrategy_CreateFBXModel_Mesh_Anim(CFactory_BaseCamp::getStartPointList(),
			60),
		new CStrategy_CreateFBXModel_Material());
	CBuilder_FBXModel builder_Model_BaseCamp(factory_Model_BaseCamp);
	pModel = builder_Model_BaseCamp.build();
	if (!pModel)
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pModel);
	auto pBaseCamp = new CBaseCamp();
	if (!pBaseCamp->createObject(&CFactory_BaseCamp(pModel)))
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pBaseCamp);

//	pObject = CBuilder_FBXModelTester_Anim("BaseCamp/BaseCapn_Low_Mapping_160508.fbx",XMFLOAT3(0,100,0)).build();
//	result.push_back(pObject);

	//CFactory_FBXModel factory_BaseCamp_Model(
	//	m_rIniData.getSection("Model_Combat").setDefaultData(CGlobalDataMap_MLRS::getDefaultINISection_Model(eCombat)),
	//	CGlobalDataMap_MLRS::getStartPointList(eCombat));
	//pModel = builder_Model_Enemy_Roach.build();
	//if (!pModel)
	//{
	//	processCreateObjectsFailed(result);
	//	return result;
	//}
	//result.push_back(pModel);
	//CGlobalDataMap_MLRS::setEnemyModel(EEnemyType::eCombat, pModel);


	//미사일

	for (int i = 1;; ++i)
	{
		auto &rSection_Missile = m_rIniData.getSection("Model_Missile" + std::to_string(i));
		if (rSection_Missile.isEmpty())	break;
		//CBuilder_FBXModel_Instancing_NoAnim builder_Missile_Enemy(
		//	rSection_Missile, 300);
		//pModel = builder_Missile_Enemy.build();
		CFactory_FBXModel factory_Model_Missile(rSection_Missile,
			new CStrategy_CreateFBXModel_Mesh_Instancing_NoAnim(300),
			new CStrategy_CreateFBXModel_Material());
		CBuilder_FBXModel_Instancing builder_Missile_Enemy(factory_Model_Missile);
		pModel = builder_Missile_Enemy.build();
		if (!pModel)
		{
			processCreateObjectsFailed(result);
			return result;
		}
		result.push_back(pModel);
		CGlobalDataMap_MLRS::setMissileModel(static_cast<EMissileType>(i), pModel);
	}

	//효과
	CEffectManager *pEffectManager = new CEffectManager();
	result.push_back(pEffectManager);

	for (int i = 1;; ++i)
	{
		auto &rSection_Effect = m_rIniData.getSection("Effect" + std::to_string(i));
		if (rSection_Effect.isEmpty())	break;

		CEffectType effectType ;
		rSection_Effect.setDefaultData(CBuilder_EffectType_Billboard::getDefaultINISection());
		unsigned int nStart = rSection_Effect.getParam_uint("IndexStart");
		unsigned int nEnd = rSection_Effect.getParam_uint("IndexEnd");
		std::string sName = rSection_Effect.getParam("Filename");
		std::string sExpension = rSection_Effect.getParam("Expension");
		unsigned int nNumberLength = rSection_Effect.getParam_uint("NumberLength");
		DirectX::XMFLOAT2 vEffectSize = rSection_Effect.getParam_XMFLOAT2("Size");
		std::vector<std::string> effectFileNames;
		for (unsigned int i = nStart; i < nEnd; ++i)
		{
			std::string sTmp;
			std::stringstream ss;
			ss << sName << std::setw(nNumberLength) << std::setfill('0') << i << '.' << sExpension;
			ss >> sTmp;
			effectFileNames.push_back(sTmp);
		}
		if (CBuilder_EffectType_Billboard("EffectManager", vEffectSize, 500, effectFileNames).build(&effectType))
			pEffectManager->addEffectType(effectType);
		else
		{
			processCreateObjectsFailed(result);
			return result;
		}
	}

	//UI
	CUIManager_MLRS *pUIManager = new CUIManager_MLRS();

	if (!pUIManager->createObject(&CFactory_UIManager_MLRS(m_rIniData.getSection("UI").setDefaultData(CFactory_UIManager_MLRS::getDefaultINISection()))))
	{
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pUIManager);

	//조명
	auto &rSection_GlobalLight = m_rIniData.getSection("GlobalLight");
	rSection_GlobalLight.setDefaultData(CFactory_LightManager::getDefaultINISection_GlobalLight());

	CLightManager *pLightManager = new CLightManager();
	if (!pLightManager->createObject(&CFactory_LightManager(rSection_GlobalLight.getParam_XMFLOAT4("GlobalAmbient") ))) {
		processCreateObjectsFailed(result);
		return result;
	}
	result.push_back(pLightManager);

	
//	result.push_back(new CLightTester);
	for (int i = 1;; ++i)
	{
		auto &rSection_Light = m_rIniData.getSection( "Light" + std::to_string(i) );
		if (rSection_Light.isEmpty())	break;
		if (rSection_Light.getParam("Type") == "directional")
		{
			CLight_Directional *pLight = new CLight_Directional();
			pLight->setAmbient(rSection_Light.getParam_XMFLOAT4("Ambient"));
			pLight->setDiffuse(rSection_Light.getParam_XMFLOAT4("Diffuse"));
			pLight->setSpecular(rSection_Light.getParam_XMFLOAT4("Specular"));
			pLight->setDirection(rSection_Light.getParam_XMFLOAT3_Normalize("Direction"));

			result.push_back(pLight);
			pLightManager->addLight(pLight);
		}

		else if (rSection_Light.getParam("Type") == "point")
		{
			//체크
			CLight_Point *pLight = new CLight_Point();
			pLight->setAmbient(rSection_Light.getParam_XMFLOAT4("Ambient"));
			pLight->setDiffuse(rSection_Light.getParam_XMFLOAT4("Diffuse"));
			pLight->setSpecular(rSection_Light.getParam_XMFLOAT4("Specular"));
			pLight->setPosition(rSection_Light.getParam_XMFLOAT3("Position"));
			pLight->setAttenuation(rSection_Light.getParam_XMFLOAT3("Attenuation"));
			pLight->setMaxRange(rSection_Light.getParam_float("MaxRange"));

			result.push_back(pLight);
			pLightManager->addLight(pLight);
		}

		else if (rSection_Light.getParam("Type") == "spot")
		{
			//체크
			CLight_Spot *pLight = new CLight_Spot();
			pLight->setAmbient(rSection_Light.getParam_XMFLOAT4("Ambient"));
			pLight->setDiffuse(rSection_Light.getParam_XMFLOAT4("Diffuse"));
			pLight->setSpecular(rSection_Light.getParam_XMFLOAT4("Specular"));
			pLight->setPosition(rSection_Light.getParam_XMFLOAT3("Position"));
			pLight->setDirection(rSection_Light.getParam_XMFLOAT3_Normalize("Direction"));
			pLight->setAttenuation(rSection_Light.getParam_XMFLOAT3("Attenuation"));
			pLight->setMaxRange(rSection_Light.getParam_float("MaxRange"));

			result.push_back(pLight);
			pLightManager->addLight(pLight);
		}
	}
	
	//CObject_FBXDummy_ModelOrigin_Test_NoAnim *pDummy = new CObject_FBXDummy_ModelOrigin_Test_NoAnim;
	//if (!pDummy->createObject(&CFactory_FBXObject_Dummy_Test_ModelOrigin_NoAnim("test.fbx"
	//	//,DirectX::XMFLOAT4X4(10,0,0,0,0,10,0,0,0,0,10,0,0,0,0,1)
	//)))
	//{
	//	processCreateObjectsFailed(result);
	//	return result;
	//}
	//result.push_back(pDummy);

	//DEBUG
	auto * pBBManager = new CPainter_BoundingBoxManager();
	result.push_back(pBBManager);
	auto * pLine = new CPainter_Line();
	pLine->createObject(&CFactory_Painter_Line());
	result.push_back(pLine);

	return result;
}
IGameObjectManager * CFactory_World_InGame_MLRS::createObjectManager()
{
	CGameObjectManager_Dummy *pResult = new CGameObjectManager_Dummy();
	return pResult;

}
IScene* CFactory_World_InGame_MLRS::createScene()
{
//	CScene *pResult = new CScene();
	CScene_DeferredLighting *pResult = new CScene_DeferredLighting();
	return pResult;
}

IAdapter_Msg_ToGame * CFactory_World_InGame_MLRS::createMsgAdapter()
{
	CAdapter_Msg_ToGame_MLRS* pMsgAdapter = new CAdapter_Msg_ToGame_MLRS();
	return pMsgAdapter;
}

