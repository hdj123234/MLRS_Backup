#include "stdafx.h"

#include "FBXLoader.h"

using namespace DirectX;

void CFBXLoader::addMaterial(FbxSurfaceMaterial *pMaterial)
{
	if (!pMaterial)	return;

	XMFLOAT4	vAmbient;
	XMFLOAT4	vDiffuse;
	XMFLOAT4	vSpecular;
	XMFLOAT4	vEmissive ;
	XMFLOAT4	vReflection;
	float					fShininess;

	if (pMaterial->ClassId.Is(FbxSurfacePhong::ClassId))
	{
		FbxSurfacePhong *pMaterial_Phong = static_cast<FbxSurfacePhong *>(pMaterial);
		
		FbxDouble3 ambient = pMaterial_Phong->Ambient.Get();
		FbxDouble ambientFactor = pMaterial_Phong->AmbientFactor.Get();
		FbxDouble3 diffuse = pMaterial_Phong->Diffuse.Get();
		FbxDouble diffuseFactor = pMaterial_Phong->DiffuseFactor.Get();
		FbxDouble3 emissive = pMaterial_Phong->Emissive.Get();
		FbxDouble emissiveFactor = pMaterial_Phong->EmissiveFactor.Get();
		FbxDouble3 specular = pMaterial_Phong->Specular.Get();
		FbxDouble specularFactor = pMaterial_Phong->SpecularFactor.Get();
		FbxDouble shininess = pMaterial_Phong->Shininess.Get();
		FbxDouble3 reflection = pMaterial_Phong->Reflection.Get();
		FbxDouble reflectionFactor = pMaterial_Phong->ReflectionFactor.Get();

		vAmbient	={ static_cast<float>(ambient[0]   ), static_cast<float>(ambient[1]   ), static_cast<float>(ambient[2]   ), static_cast<float>(ambientFactor    )};
		vDiffuse	={ static_cast<float>(diffuse[0]   ), static_cast<float>(diffuse[1]   ), static_cast<float>(diffuse[2]   ), static_cast<float>(diffuseFactor    )};
		vSpecular	={ static_cast<float>(specular[0]  ), static_cast<float>(specular[1]  ), static_cast<float>(specular[2]  ), static_cast<float>(specularFactor   )};
		vEmissive	={ static_cast<float>(emissive[0]  ), static_cast<float>(emissive[1]  ), static_cast<float>(emissive[2]  ), static_cast<float>(emissiveFactor   )};
		vReflection	={ static_cast<float>(reflection[0]), static_cast<float>(reflection[1]), static_cast<float>(reflection[2]), static_cast<float>(reflectionFactor )};
		fShininess = static_cast<float>(shininess);

		m_Materials.push_back(CMaterial(vAmbient,
										vDiffuse  ,
										vSpecular ,
										vEmissive,
										vReflection,
										fShininess));				
	}
	if (pMaterial->GetClassId().Is(FbxSurfaceLambert::ClassId))
	{
		FbxSurfaceLambert *pMaterial_Lambert = static_cast<FbxSurfaceLambert *>(pMaterial);

		FbxDouble3 ambient = pMaterial_Lambert->Ambient.Get();
		FbxDouble  ambientFactor = pMaterial_Lambert->AmbientFactor.Get();
		FbxDouble3 diffuse = pMaterial_Lambert->Diffuse.Get();
		FbxDouble  diffuseFactor = pMaterial_Lambert->DiffuseFactor.Get();
		FbxDouble3 emissive = pMaterial_Lambert->Emissive.Get();
		FbxDouble  emissiveFactor = pMaterial_Lambert->EmissiveFactor.Get();

		vAmbient	={ static_cast<float>(ambient[0]   ), static_cast<float>(ambient[1]   ), static_cast<float>(ambient[2]   ), static_cast<float>(ambientFactor    )};
		vDiffuse	={ static_cast<float>(diffuse[0]   ), static_cast<float>(diffuse[1]   ), static_cast<float>(diffuse[2]   ), static_cast<float>(diffuseFactor    )};
		vEmissive	={ static_cast<float>(emissive[0]  ), static_cast<float>(emissive[1]  ), static_cast<float>(emissive[2]  ), static_cast<float>(emissiveFactor   )};
		
		m_Materials.push_back(CMaterial(vAmbient,
										vDiffuse,
										vEmissive ));
	}


}

void CFBXLoader::addAttribute(FbxNodeAttribute * pAttribute)
{
	if (!pAttribute)	return;

	switch (pAttribute->GetAttributeType())
	{
	case FbxNodeAttribute::EType::eMesh:
		addMesh(static_cast<FbxMesh *>(pAttribute));
		return;
	case FbxNodeAttribute::EType::eSkeleton:
		addSkeleton(static_cast<FbxSkeleton *>(pAttribute));
		return;
	}
}

void CFBXLoader::addMesh(FbxMesh * pMesh)
{
	if (!pMesh)	return;

	//재질
	//unsigned int nNumberOfMaterial = pMesh->GetElementMaterialCount();
	//for (unsigned int i = 0; i < nNumberOfMaterial; ++i)
	//{
	//	FbxGeometryElementMaterial * pElementMaterial = pMesh->GetElementMaterial(i);
	//	auto e1 = pElementMaterial->GetMappingMode();
	//	auto e2 = pElementMaterial->GetReferenceMode();
	//	FbxLayerElementArrayTemplate<int> materialIndices = pElementMaterial->GetIndexArray();
	//	unsigned int nNumberOfMaterialIndices = materialIndices.GetCount();
	//	for (unsigned int i = 0; i < nNumberOfMaterialIndices; ++i)
	//	{
	//		materialIndices[i];
	//	}
	//	pMesh->GetNode()->GetMaterialCount();
	//	continue;
	//}

	//정점
	FbxVector4 * pControlPoints = pMesh->GetControlPoints();
	unsigned int nNumberOfControlPoint = pMesh->GetControlPointsCount();

	m_Vertices.reserve(nNumberOfControlPoint);
	for (unsigned int i = 0; i < nNumberOfControlPoint; ++i)
		m_Vertices.push_back(XMFLOAT3(	static_cast<float>(pControlPoints[i][0]),
										static_cast<float>(pControlPoints[i][1]),
										static_cast<float>(pControlPoints[i][2]) ));

	//uv
	FbxGeometryElementUV *pUV;
	FbxStringList uvSetNameList;
	pMesh->GetUVSetNames(uvSetNameList);
	if (!(pUV = pMesh->GetElementUV(uvSetNameList[0]))) return;
	FbxLayerElementArrayTemplate<FbxVector2> &rvUVs = pUV->GetDirectArray();
	unsigned int nNumberOfUV = rvUVs.GetCount();

	m_UVs.reserve(nNumberOfUV);
	for (unsigned int i = 0; i < nNumberOfUV; ++i)
		m_UVs.push_back(XMFLOAT2(	static_cast<float>(rvUVs[i][0]),
									static_cast<float>(rvUVs[i][1])));

	//폴리곤
	int * pVertexIndices = pMesh->GetPolygonVertices();
	unsigned int nNumberOfPolygonVertex = pMesh->GetPolygonVertexCount();
	FbxLayerElementArrayTemplate<FbxVector4> *pvNormals;
	FbxLayerElementArrayTemplate<FbxVector4> *pvTangents;
	if(!pMesh->GetNormals(&pvNormals))	return;
	if(!pMesh->GetTangents(&pvTangents))	return;
	FbxLayerElementArrayTemplate<int> &pUVsIndices = pUV->GetIndexArray();
	
	//폴리곤
	m_Indices.reserve(nNumberOfPolygonVertex);
	for (unsigned int i = 0; i < nNumberOfPolygonVertex; ++i)
	{
		FbxVector4 vNormal = pvNormals->GetAt(i);
		FbxVector4 vTangent = pvTangents->GetAt(i);
		m_Indices.push_back(CIndex(	pVertexIndices[i] , 
									pUVsIndices[i] ,
									XMFLOAT3(	static_cast<float>(vNormal[0]),
												static_cast<float>(vNormal[1]),
												static_cast<float>(vNormal[2])),
									XMFLOAT3(	static_cast<float>(vTangent[0]),
												static_cast<float>(vTangent[1]),
												static_cast<float>(vTangent[2])) ));
	}


	//삼각형으로 변환했기 때문에 모든 폴리곤은 삼각형으로 가정

}

void CFBXLoader::addSkeleton(FbxSkeleton * pMesh)
{
}

void CFBXLoader::addNode(FbxNode * pNode)
{
	if (!pNode)	return;

	const char* nodeName = pNode->GetName();
	FbxDouble3 translation = pNode->LclTranslation.Get();
	FbxDouble3 rotation = pNode->LclRotation.Get();
	FbxDouble3 scaling = pNode->LclScaling.Get();

	const unsigned int nNumberOfMaterial = pNode->GetMaterialCount();
	for (unsigned int i = 0; i <nNumberOfMaterial; i++)
		addMaterial(pNode->GetMaterial(i));


	const unsigned int nNumberOfAttribute = pNode->GetNodeAttributeCount();
	for (unsigned int i = 0; i <nNumberOfAttribute; i++)
		addAttribute(pNode->GetNodeAttributeByIndex(i));

	//재귀호출
	const unsigned int nNumberOfChild = pNode->GetChildCount();
	for (unsigned int i = 0; i < nNumberOfChild; ++i)
		addNode(pNode->GetChild(i));
}

CFBXLoader::CFBXLoader()
	:m_pFBXManager(FbxManager::Create()),
	m_pFBXIOSettings(FbxIOSettings::Create(m_pFBXManager,IOSROOT)),
	m_pFBXImporter(FbxImporter::Create(m_pFBXManager,""))
{
	m_pFBXManager->SetIOSettings(m_pFBXIOSettings);
}


CFBXLoader::~CFBXLoader()
{
	m_pFBXImporter->Destroy();
	m_pFBXIOSettings->Destroy();
	m_pFBXManager->Destroy();
}

bool CFBXLoader::loadFile(const std::wstring & sFileName)
{
	//파일 열기(Scene 생성)
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> wstring_converter;

	return CFBXLoader::loadFile(wstring_converter.to_bytes(sFileName));
}

bool CFBXLoader::loadFile(const std::string & sFileName)
{
	FbxIOSettings *pIOSettings = FbxIOSettings::Create(m_pFBXManager, "IOSettings");
	if (!m_pFBXImporter->Initialize(sFileName.c_str()))
	{
		return false;
	}
	FbxScene *pScene = FbxScene::Create(m_pFBXManager, "LoadScene");
	if (!m_pFBXImporter->Import(pScene))	return false;

	//컨버트
	FbxGeometryConverter converter(m_pFBXManager);
	FbxAxisSystem axisSystem(FbxAxisSystem::eDirectX);
	axisSystem.ConvertScene(pScene);
	converter.Triangulate(pScene, true);


	if (FbxNode* pRootNode = pScene->GetRootNode())
	{
		//재질 로드
		const int nNumberOfMaterial = pRootNode->GetMaterialCount();
		for (int i = 0; i < nNumberOfMaterial; i++)
			addMaterial(pRootNode->GetMaterial(i));

		//노드 순회
		addNode(pRootNode);
	}

	//소멸
	pScene->Destroy();
	return true;
}

CFBXLoader::CMaterial::CMaterial(
	const DirectX::XMFLOAT4& rvAmbient,
	const DirectX::XMFLOAT4& rvDiffuse,
	const DirectX::XMFLOAT4& rvEmissive)
	:m_bPhong(false),
	m_vAmbient(rvAmbient),
	m_vDiffuse(rvDiffuse),
	m_vSpecular({1,1,1,1}),
	m_vEmissive(rvEmissive)
{
}

CFBXLoader::CMaterial::CMaterial(
	const DirectX::XMFLOAT4& rvAmbient, 
	const DirectX::XMFLOAT4& rvDiffuse, 
	const DirectX::XMFLOAT4& rvSpecular, 
	const DirectX::XMFLOAT4& rvEmissive, 
	const DirectX::XMFLOAT4& rvReflection, 
	const float fShininess)
	:m_bPhong(true),
	m_vAmbient(rvAmbient),
	m_vDiffuse(rvDiffuse),
	m_vSpecular(rvSpecular),
	m_vEmissive(rvEmissive),
	m_vReflection(rvReflection),
	m_fShininess(fShininess)
{
}

CFBXLoader::CIndex::CIndex(
	const unsigned int nVertexIndex,
	const unsigned int nUVIndex,
	const DirectX::XMFLOAT3 & rvNormal,
	const DirectX::XMFLOAT3 & rvTangent)
	:m_nVertexIndex(nVertexIndex),
	m_nUVIndex(nUVIndex),
	m_vNormal(rvNormal),
	m_vTangent(rvTangent)
{
}
