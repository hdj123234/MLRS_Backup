#pragma once
#ifndef HEADER_FACTORY_WORLD_TESTER
#define HEADER_FACTORY_WORLD_TESTER
#include"..\Module_World\GameWorld.h"

//���漱��
class CMyINIData;

class CFactory_World_InGame_MLRS  : public IFactory_GameWorld {
private:
	const CMyINIData &m_rIniData;

public:
	CFactory_World_InGame_MLRS(const CMyINIData& rIniData) :m_rIniData(rIniData){}
	virtual ~CFactory_World_InGame_MLRS()  {}

private:
	void processCreateObjectsFailed(std::vector<IGameObject *> &result);

public:
	//override IFactory_GameWorld
	virtual std::vector<IGameObject *> createObjects();
	virtual IGameObjectManager * createObjectManager();
	virtual IScene* createScene();
	virtual IAdapter_Msg_ToGame* createMsgAdapter();


};

#endif