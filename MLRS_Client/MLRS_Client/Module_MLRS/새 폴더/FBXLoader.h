#ifndef HEADER_FBXLOADER
#define HEADER_FBXLOADER

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_DIRECTXMATH

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

#include<fbxsdk.h>

class CFBXLoader{
protected:
	struct CMaterial {
		bool				m_bPhong;
		DirectX::XMFLOAT4	m_vAmbient;
		DirectX::XMFLOAT4	m_vDiffuse;
		DirectX::XMFLOAT4	m_vSpecular;
		DirectX::XMFLOAT4	m_vEmissive;
		DirectX::XMFLOAT4	m_vReflection;
		float				m_fShininess;
		CMaterial(	const DirectX::XMFLOAT4	&rvAmbient,
					const DirectX::XMFLOAT4	&rvDiffuse,
					const DirectX::XMFLOAT4	&rvEmissive);
		CMaterial(	const DirectX::XMFLOAT4	&rvAmbient,
					const DirectX::XMFLOAT4	&rvDiffuse,
					const DirectX::XMFLOAT4	&rvSpecular,
					const DirectX::XMFLOAT4	&rvEmissive,
					const DirectX::XMFLOAT4	&rvReflection,
					const float					fShininess);
	};

	struct CIndex {
		unsigned int m_nVertexIndex;
		unsigned int m_nUVIndex;
		DirectX::XMFLOAT3 m_vNormal;
		DirectX::XMFLOAT3 m_vTangent;
		CIndex(	const unsigned int nVertexIndex,
				const unsigned int nUVIndex,
				const DirectX::XMFLOAT3 &rvNormal,
				const DirectX::XMFLOAT3 &rvTangent);
	};
private:


	FbxManager *m_pFBXManager;
	FbxIOSettings *m_pFBXIOSettings;
	FbxImporter *m_pFBXImporter;

	std::vector<DirectX::XMFLOAT3> m_Vertices;
	std::vector<DirectX::XMFLOAT2> m_UVs;
	std::vector<CIndex> m_Indices;
	std::vector<CMaterial> m_Materials;

	void addMaterial(FbxSurfaceMaterial *pMaterial);
	void addAttribute(FbxNodeAttribute *pAttribute);

	void addMesh(FbxMesh *pMesh);
	void addSkeleton(FbxSkeleton *pMesh);

	//����Լ�
	void addNode(FbxNode *pNode);

protected:
	std::vector<DirectX::XMFLOAT3>	&getVertices() { return m_Vertices; }
	std::vector<DirectX::XMFLOAT2>	&getUVs() { return m_UVs; }
	std::vector<CIndex>				&getIndices() { return m_Indices; }

public:
	CFBXLoader();
	~CFBXLoader();


	bool loadFile(const std::wstring &sFileName);
	bool loadFile(const std::string &sFileName);

};

#endif