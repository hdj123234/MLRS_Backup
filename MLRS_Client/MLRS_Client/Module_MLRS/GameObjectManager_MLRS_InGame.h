#pragma once
#ifndef HEADER_GAMEOBJECTMANAGER_MLRS_INGAME
#define HEADER_GAMEOBJECTMANAGER_MLRS_INGAME

#include"..\Module_World\GameObjectManager.h"
#include"..\Module_DXComponent\Type_InDirectX.h"

//���漱��
class IEnemy;
class IBoss;
class IMainMecha;
class CBaseCamp;
class IGameModel;
class IUIManager;
class IEffectManager;
class CMissile;
class CObjectDecorator_ClientPlayer;
class AObject_FBXDummy;
class CMultiBuilder_EnemyMecha_Instanced;
class CMultiBuilder_Missile;
class CSpace;
class CTerrain;
template<class Type>
class CParticleSystem_Spray ;
typedef CParticleSystem_Spray<Type_Fume_SprayPoint> CFume;
class CSoundManager;

//DEBUG
class CPainter_BoundingBoxManager;

class CGameObjectManager_MLRS_InGame : public CGameObjectManager {
private:
	typedef BYTE ModelID;
	typedef std::pair<CMultiBuilder_EnemyMecha_Instanced *, std::queue<IEnemy *>> EnemyMechaBuilderAndReuseList;
	typedef std::pair<CMultiBuilder_Missile *, std::queue<CMissile *>> MissileBuilderAndReuseList;
protected:
	//override IGameObjectManager
	std::vector<IGameObject *> m_pObjects;
//	std::queue<IMsg_GameObject *> m_pMsgQueue;
	CSpace *m_pSpace;
	ICamera *m_pCamera;
	ISkyBox *m_pSkyBox;
	CBaseCamp *m_pBaseCamp;
	IUIManager *m_pUIManager;
	IEffectManager *m_pEffectManager;
	ILightManager *m_pLightManager;
	CObjectDecorator_ClientPlayer *m_pPlayer;
	IBoss *m_pBoss;
	CTerrain *m_pTerrain;
	CFume *m_pFume;
	CSoundManager *m_pSoundManager;

	std::map<ModelID, EnemyMechaBuilderAndReuseList> m_pEnemyModelMap;
	std::map<ModelID, MissileBuilderAndReuseList> m_pMissileModelMap;
	std::vector<IGameModel *> m_pModels;
	std::vector<IMainMecha *> m_pPlayers;
	std::vector<IEnemy *> m_pEnemys;
	std::vector<CMissile *> m_pMissiles;
	std::vector<ILight *> m_pLights;
	unsigned int m_nClientID;

	bool m_bDrawBoundingObject;
	bool m_bDrawTerrianWire;

	//DEBUG
	CPainter_BoundingBoxManager *m_pBBManager;
public:
	CGameObjectManager_MLRS_InGame();
	virtual ~CGameObjectManager_MLRS_InGame() { CGameObjectManager_MLRS_InGame::releaseObject(); }

private:
	//Virtual Func
	virtual void addObject_AfterProcessing(IGameObject *pObject);
	virtual void deleteObject_AfterProcessing(IGameObject *pObject) {}
	virtual void setOriginData_AfterProcessing();
	virtual void handleMessage_RealProcessing();
	virtual void updateScene_RealProcessing(IScene *pScene);

public:
	//override IGameObjectManager
	virtual void initScene(IScene * pScene);
	virtual std::vector<IMsg_GameObject *> animateObjects(const float fElapsedTime);
	virtual void processCollision() {  }
	virtual void updateObjectGroup() {} 

	void releaseObject();
};

#endif