#include "stdafx.h"
#include "GameObjectManager_MLRS_Lobby.h"

#include"..\Module_Object\Interface_Object.h"
#include"..\Module_Scene\Interface_Scene.h"
#include"Msg_InGame_MLRS.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include"LobbyComponent_MLRS.h"
#include"Msg_Lobby_MLRS.h"


#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif

#ifndef HEADER_STL
#include<iostream>
#endif // !HEADER_STL

//----------------------------------- CGameObjectManager_MLRS_InGame -------------------------------------------------------------------------------------


CGameObjectManager_MLRS_Lobby::CGameObjectManager_MLRS_Lobby()
	: 
	m_nClientID(0), 
	m_pSoundManager(nullptr) 
{
//	m_pReturnMsgs.push_back(new CMsg_CS_NoName()); 
}




void CGameObjectManager_MLRS_Lobby::addObject_AfterProcessing(IGameObject * pObject)
{
	if (auto p = dynamic_cast<RIGameObject_Button *>(pObject))
	{
		m_pObjects_Button.push_back(p);
	}
	m_pObjects.push_back(pObject);
//	if (auto p = dynamic_cast<CObjectDecorator_ClientPlayer *>(pObject))
//	{
//		m_pPlayer = p;
//		m_pPlayers.insert(m_pPlayers.begin(), m_pPlayer);
////		m_pObjects.push_back(m_pPlayer);		
//	}
// 
//	else if (auto p = dynamic_cast<CSoundManager *>(pObject))
//	{
//		m_pSoundManager = p;
//		m_pObjects.push_back(p);
//		m_pMsgQueue.push(new CMsg_MLRS_Sound(new CMsg_Sound_RunBGM(0)));
//	}
//	else
//		m_pObjects.push_back(pObject);
}

void CGameObjectManager_MLRS_Lobby::setOriginData_AfterProcessing()
{
	for (auto data : *m_pObjects_Origin)
		addObject_AfterProcessing(data);


	m_bReady = true;
}

void CGameObjectManager_MLRS_Lobby::handleMessage_RealProcessing()
{
	//for (unsigned int i = 0; i < m_pMsgQueue.size();++i)
	//{
	//	auto data = m_pMsgQueue.front();
	//	m_pMsgQueue.pop();
	//	m_pMsgQueue.push(data);
	//	if (IMsg_Movement * pMsg = dynamic_cast<IMsg_Movement *>(data))
	//		if(	pMsg->getMovementType() == EMsgType_Movement::Move_LookBase)
	//			break;
	//}
	while (!m_pMsgQueue.empty())
	{
		auto data = m_pMsgQueue.front();


		auto pMsg_MLRS = static_cast<CMsg_MLRS *>(data);
		switch (pMsg_MLRS->getMsgType())
		{
		case EMsgType_MLRS::eLobby:
		{
			auto pMsg_Lobby = static_cast<CMsg_Lobby_MLRS *>(pMsg_MLRS);
			switch (pMsg_Lobby->getMsgType())
			{

			case EMsgType_Lobby_MLRS::eClick:
				for (auto pData : m_pObjects_Button)
				{
					if (pData->checkInRect(static_cast<CMsg_Lobby_MLRS_Click *>(pMsg_Lobby)->getPosX(), static_cast<CMsg_Lobby_MLRS_Click *>(pMsg_Lobby)->getPosY()))
					{
						auto pMsg = pData->click();
						if (pMsg) m_pReturnMsgs.push_back(pMsg);

						break;
					}
				}
				break;
			case EMsgType_Lobby_MLRS::eDEBUG_InGame: 
				m_eWorldState = eEnterGame;
				break;
			} 
			break;
		}
		case EMsgType_MLRS::eReceivedFromServer:
		{
			CMsg_SC *pMsg = static_cast<CMsg_SC *>(data);
			switch (pMsg->getType())
			{
			case EMsgType_SC::eClientID:
				m_nClientID = static_cast<CMsg_SC_ClientID *>(pMsg)->m_nID;
				break;
			case EMsgType_SC::eLobby_RoomList:
			{ 
				break;
			}
			case EMsgType_SC::eLobby_RoomData:
			{ 
				m_eWorldState = EState_GameWorld::eEnterRoom;
				break;
			}

			}
			break;
		} 

		}

//		switch (pMsg_MLRS->getMsgType())
//		{
//		case EMsgType_MLRS::eReceivedFromServer:
//		{
//			CMsg_SC *pMsg = static_cast<CMsg_SC *>(data);
//			switch (pMsg->getType())
//			{
//			case EMsgType_SC::eChangeWeapon:
//				m_pPlayers[static_cast<CMsg_SC_ChangeWeapon *>(pMsg)->m_nID]->handleMsg(pMsg);
//				break; 
//			case EMsgType_SC::eHP:
//			{
//				auto p = static_cast<CMsg_SC_HP *>(pMsg);
//				if(p->m_eObjType==EObjTarget_SC::ePlayer&&p->m_nID == m_nClientID)
//					m_pUIManager->handleMsg(pMsg);
//				break;
//			}
//			}
//			break;
//		} 
//		case EMsgType_MLRS::eSound:
//			m_pSoundManager->handleMsg(static_cast<CMsg_MLRS_Sound*>(pMsg_MLRS)->getSoundMsg());
//			break; 
//		}

		delete data;
		m_pMsgQueue.pop();
	}
}

void CGameObjectManager_MLRS_Lobby::updateScene_RealProcessing(IScene * pScene)
{  
	for (auto data : m_pObjects)
	{
		if (data->getTypeFlag() & FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW)
			if (auto *pObj = dynamic_cast<RIGameObject_BeMust_RenewalForDraw *>(data))
				pScene->addObject_Renewal(pObj);
		if (data->getTypeFlag() & FLAG_OBJECTTYPE_DRAWALBE)
			if (RIGameObject_Drawable *pObj = dynamic_cast<RIGameObject_Drawable *>(data))
					pScene->addObject(pObj);
	}
	  
}

void CGameObjectManager_MLRS_Lobby::initScene(IScene * pScene)
{
	if (!m_bReady)		return;
	//pScene->addObject_Init(dynamic_cast<const RIGameObject_BeMust_InitForDraw *>(m_pPlayer->getCamera()));
	//for (auto data : *m_pObjects_Origin)
	//	if (dynamic_cast<RIGameObject_BeMust_InitForDraw *>(data))
	//		pScene->addObject_Init(dynamic_cast<RIGameObject_BeMust_InitForDraw *>(data));

}

std::vector<IMsg_GameObject *> CGameObjectManager_MLRS_Lobby::animateObjects(const float fElapsedTime)
{ 
	std::vector<IMsg_GameObject *> retval;
	for (auto data : m_pObjects)
	{
		if (data->getTypeFlag() & FLAG_OBJECTTYPE_ANIMATE)
		{
			if (RIGameObject_Animate *pObj = dynamic_cast<RIGameObject_Animate *>(data))
			{
				std::vector<IMsg *> &rpMsgs = pObj->animateObject(fElapsedTime);
				for (auto pMsg : rpMsgs)
				{
					if(dynamic_cast<CMsg_CS *>(pMsg))						
						m_pReturnMsgs.push_back(pMsg);
					else if (auto p = dynamic_cast<IMsg_GameObject *>(pMsg) )
						retval.push_back(p);
					else
						m_pReturnMsgs.push_back(pMsg);
				}
			}
		}
	} 
	return retval;
}

void CGameObjectManager_MLRS_Lobby::releaseObject()
{ 
}
