#pragma once
#ifndef HEADER_MSG_INGAME_MLRS
#define HEADER_MSG_INGAME_MLRS


#include "..\Module_Object\Interface_Object.h"
#include "..\Module_Object\Msg_Movement.h"
#include "..\Module_Sound\Msg_Sound.h"
#include "..\Module_Object\Proxy.h"
 

enum class EMsgType_MLRS : ENUMTYPE_256 {
	eReceivedFromServer,
	eControlPlayer,
	eUI,
	eDebug,
	eSound,
	eLobby,
//	eSetState,
};

class CMsg_MLRS : public IMsg_GameObject {
private:
	EMsgType_MLRS m_eType;
public:
	CMsg_MLRS(EMsgType_MLRS eType):m_eType(eType){}
	virtual ~CMsg_MLRS() {}

	const EMsgType_MLRS getMsgType()const { return m_eType; }
};



//----------------------------------- ControlPlayer -------------------------------------------------------------------------------------

enum EMsgType_MLRS_ControlPlayer : ENUMTYPE_256 {
	eKey_Flag,
	eAction,
	eCamera_Distance,
	eCamera_Rotate,
	eHideClient,
	eShowClient,
};


class CMsg_MLRS_ControlPlayer : public CMsg_MLRS {
private:
	EMsgType_MLRS_ControlPlayer m_eType_CtlPlayer;
public:
	CMsg_MLRS_ControlPlayer(const EMsgType_MLRS_ControlPlayer eMsgType_CtlPlayer)
		:CMsg_MLRS(EMsgType_MLRS::eControlPlayer),
		m_eType_CtlPlayer(eMsgType_CtlPlayer){}
	virtual ~CMsg_MLRS_ControlPlayer() {}

	const EMsgType_MLRS_ControlPlayer getMsgType_CtlPlayer()const { return m_eType_CtlPlayer; }
};
 
enum FLAG_KEY : FLAG_8
{
	FLAG_MOVEMENT_FRONT = 0x01,
	FLAG_MOVEMENT_BACK = 0x02,
	FLAG_MOVEMENT_LEFT = 0x04,
	FLAG_MOVEMENT_RIGHT = 0x08,
	FLAG_ATTACK = 0x10,
	FLAG_JUMP = 0x20,
	FLAG_BOOST = 0x40
};

static const FLAG_8 FLAGMASK_KEY_MOVE_DIR = FLAG_MOVEMENT_FRONT 
										| FLAG_MOVEMENT_BACK 
										| FLAG_MOVEMENT_LEFT 
										| FLAG_MOVEMENT_RIGHT;

static const FLAG_8 FLAGMASK_KEY_MOVE = FLAG_MOVEMENT_FRONT 
										| FLAG_MOVEMENT_BACK 
										| FLAG_MOVEMENT_LEFT 
										| FLAG_MOVEMENT_RIGHT
										| FLAG_JUMP
										| FLAG_BOOST;


class CMsg_MLRS_KeyFlag : public CMsg_MLRS_ControlPlayer {
private:
	bool m_bOn;
	FLAG_8 m_Flag;
public:
	CMsg_MLRS_KeyFlag(bool bOn, const FLAG_8 flag)
		:CMsg_MLRS_ControlPlayer(eKey_Flag),
		m_Flag(flag),
		m_bOn(bOn){}
	virtual ~CMsg_MLRS_KeyFlag() {}

	const FLAG_8 getFlag()	const { return m_Flag; }
	const bool isOnFlag()	const { return m_bOn; }
};

enum EAction_MLRS : ENUMTYPE_256
{
	eAction_Weapon1,
	eAction_Weapon2,
	eAction_Weapon3,
	eAction_DebugCamera,
};
class CMsg_MLRS_Action : public CMsg_MLRS_ControlPlayer {
private: 
	EAction_MLRS m_eAction;
public:
	CMsg_MLRS_Action(  const EAction_MLRS eAct)
		:CMsg_MLRS_ControlPlayer(eAction),
		m_eAction(eAct)  {}
	virtual ~CMsg_MLRS_Action() {}

	const EAction_MLRS getAction()	const { return m_eAction; }
};

class CMsg_MLRS_CtlCamera_Distance : public CMsg_MLRS_ControlPlayer {
private:
	CMsg_Movement_Float m_Msg_Movement;
public:
	CMsg_MLRS_CtlCamera_Distance(const float fDistance)
		:CMsg_MLRS_ControlPlayer(eCamera_Distance) ,
		m_Msg_Movement(EMsgType_Movement::eControl_Distance,fDistance){}
	virtual ~CMsg_MLRS_CtlCamera_Distance() {   }

	CMsg_Movement_Float * getMovementMsg() { return &m_Msg_Movement; }
};

class CMsg_MLRS_CtlCamera_Rotate : public CMsg_MLRS_ControlPlayer {
private:
	CMsg_Movement_Vector2 m_Msg_Movement;
public:
	CMsg_MLRS_CtlCamera_Rotate(const DirectX::XMFLOAT2 &rvPitchYaw)
		:CMsg_MLRS_ControlPlayer(eCamera_Rotate),
		m_Msg_Movement(EMsgType_Movement::eRotate_PitchYaw, rvPitchYaw) {}
	CMsg_MLRS_CtlCamera_Rotate(const float fPitch, const float fYaw)
		:CMsg_MLRS_ControlPlayer(eCamera_Rotate),
		m_Msg_Movement(EMsgType_Movement::eRotate_PitchYaw, DirectX::XMFLOAT2(fPitch,fYaw)) {}
	virtual ~CMsg_MLRS_CtlCamera_Rotate() {   }

	CMsg_Movement_Vector2 * getMovementMsg() { return &m_Msg_Movement; }
};
// 


//----------------------------------- UI -------------------------------------------------------------------------------------

enum EMsgType_MLRS_UI : ENUMTYPE_256 {
	eUI_AimFlag,
	eUI_WeaponFlag,
	eUI_LaunchGuide_Parabola,
	eUI_LockOn,
	eUI_ReleaseLockOn,
	eUI_BoostGauge, 
}; 

class CMsg_MLRS_UI : public CMsg_MLRS {
private:
	EMsgType_MLRS_UI m_eType_UI;
public:
	CMsg_MLRS_UI(const EMsgType_MLRS_UI eMsgType_UI)
		:CMsg_MLRS(EMsgType_MLRS::eUI),
		m_eType_UI(eMsgType_UI) {}
	virtual ~CMsg_MLRS_UI() {}

	const EMsgType_MLRS_UI getMsgType_UI()const { return m_eType_UI; }
};

class CMsg_MLRS_UI_AimFlag : public CMsg_MLRS_UI {
private:
	FLAG_8 m_Flag;
public:
	CMsg_MLRS_UI_AimFlag(FLAG_8 flag)
		:CMsg_MLRS_UI(eUI_AimFlag),
		m_Flag(flag) {}
	virtual ~CMsg_MLRS_UI_AimFlag() {}

	const FLAG_8 getFlag() const { return m_Flag; }
};

class CMsg_MLRS_UI_WeaponFlag : public CMsg_MLRS_UI {
private:
	FLAG_8 m_Flag;
public:
	CMsg_MLRS_UI_WeaponFlag(FLAG_8 flag)
		:CMsg_MLRS_UI(eUI_WeaponFlag),
		m_Flag(flag) {}
	virtual ~CMsg_MLRS_UI_WeaponFlag() {}

	const FLAG_8 getFlag() const { return m_Flag; }
};

class CMsg_MLRS_UI_LaunchGuide_Parabola : public CMsg_MLRS_UI {
private: 
	bool m_bOn;
	std::vector<DirectX::XMFLOAT3> m_vVertices;

public:
	CMsg_MLRS_UI_LaunchGuide_Parabola(std::vector<DirectX::XMFLOAT3> &rvVertices)
		:CMsg_MLRS_UI(eUI_LaunchGuide_Parabola),
		m_bOn(true),
		m_vVertices(rvVertices){}

private:
	CMsg_MLRS_UI_LaunchGuide_Parabola()
		:CMsg_MLRS_UI(eUI_LaunchGuide_Parabola),
		m_bOn(false) {}

public:
	virtual ~CMsg_MLRS_UI_LaunchGuide_Parabola() {}

	const bool isOn() { return m_bOn; }
	std::vector<DirectX::XMFLOAT3> &&getVertices_RVR() { return std::move(m_vVertices); }

	static CMsg_MLRS_UI_LaunchGuide_Parabola * createOffMsg() { return new CMsg_MLRS_UI_LaunchGuide_Parabola(); }
};

class CMsg_MLRS_UI_LockOn: public CMsg_MLRS_UI {
private: 
	CProxy<RIGameObject_PositionGetter>* m_pProxy;

public:
	CMsg_MLRS_UI_LockOn(CProxy<RIGameObject_PositionGetter>* pProxy)
		:CMsg_MLRS_UI(eUI_LockOn), 
		m_pProxy(pProxy) {}
	virtual ~CMsg_MLRS_UI_LockOn() {}
	 
	CProxy<RIGameObject_PositionGetter>* getProxy() { return m_pProxy; }
};
 

class CMsg_MLRS_UI_BoostGauge : public CMsg_MLRS_UI {
private:
	float m_fGauge;

public:
	CMsg_MLRS_UI_BoostGauge(const float fGauge)
		:CMsg_MLRS_UI(eUI_BoostGauge),
		m_fGauge(fGauge) {}
	virtual ~CMsg_MLRS_UI_BoostGauge() {}

	float  getGauge() { return m_fGauge; }
};
//----------------------------------- Sound -------------------------------------------------------------------------------------

class CMsg_MLRS_Sound : public CMsg_MLRS { 
private:
	IMsg_Sound *m_pMsg_Sound;
public:
	CMsg_MLRS_Sound(IMsg_Sound * pMsg_Sound)
		:CMsg_MLRS(EMsgType_MLRS::eSound),
		m_pMsg_Sound(pMsg_Sound) {}
	virtual ~CMsg_MLRS_Sound() { if(m_pMsg_Sound)delete m_pMsg_Sound; }

	IMsg_Sound * getSoundMsg(){ return m_pMsg_Sound; }
};

//----------------------------------- Debug -------------------------------------------------------------------------------------

enum EMsgType_MLRS_DEBUG : ENUMTYPE_256 {
	eDebug_SwapBoundingBox,
	eDebug_SwapTerrainWire,
};

class CMsg_MLRS_DEBUG : public CMsg_MLRS {
private:
	EMsgType_MLRS_DEBUG m_eType_DEBUG;
public:
	CMsg_MLRS_DEBUG(const EMsgType_MLRS_DEBUG eMsgType_DEBUG)
		:CMsg_MLRS(EMsgType_MLRS::eDebug),
		m_eType_DEBUG(eMsgType_DEBUG) {}
	virtual ~CMsg_MLRS_DEBUG() {}

	const EMsgType_MLRS_DEBUG getMsgType_DEBUG()const { return m_eType_DEBUG; }
};

#endif