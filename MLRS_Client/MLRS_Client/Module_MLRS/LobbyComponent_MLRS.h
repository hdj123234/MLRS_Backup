#ifndef HEADER_LOBBYCOMPONENT_MLRS
#define HEADER_LOBBYCOMPONENT_MLRS


#include"..\Module_Object\Interface_Object.h"
#include"..\Module_Object\GameObject_ModelBase.h"
#include"..\Module_Object_GameModel\Mesh.h"

//���漱��
class CLobbyComponent_Drawable;



class RIGameObject_Button {
public:
	virtual ~RIGameObject_Button() = 0 {}

	virtual bool checkInRect(unsigned int nPosX, unsigned int nPosY)const = 0;
	virtual IMsg * click() = 0;
};

//----------------------------------- Creator Interface -------------------------------------------------------------------------------------
 
class IFactory_LobbyComponent_Drawable_MLRS : public IFactory_GameObject_ModelBase{
public:
	virtual ~IFactory_LobbyComponent_Drawable_MLRS() = 0 {}
 };

class IFactory_LobbyComponent_Button_MLRS   {
public:
	virtual ~IFactory_LobbyComponent_Button_MLRS() = 0 {}
	 
	virtual std::vector<CLobbyComponent_Drawable *>  getComponents() = 0;
	virtual RECT getRect() = 0;
	virtual float getOnTime() = 0;
};
class IFactory_LobbyComponent_ButtonList_MLRS : public IFactory_LobbyComponent_Drawable_MLRS {
public:
	virtual ~IFactory_LobbyComponent_ButtonList_MLRS() = 0 {}

	virtual std::vector<RECT> getRects() = 0;
};
//----------------------------------- Class -------------------------------------------------------------------------------------
 
class CLobbyComponent_Statics : virtual public IGameObject ,
								public std::vector<IGameObject  *>,
								virtual public RIGameObject_Animate,
								virtual public RIGameObject_Drawable{
private:
	typedef std::vector<IGameObject  *> SUPER;

private:
	FLAG_8 m_ObjectFlag;
public:
	CLobbyComponent_Statics():m_ObjectFlag(0){}
	virtual ~CLobbyComponent_Statics() { releaseObject(); }

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return m_ObjectFlag; }

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime)
	{
		std::vector<IMsg *> retval;
		for (auto pData : *this)
		{
			std::vector<IMsg *> tmp;
			if (pData->getTypeFlag()&FLAG_OBJECTTYPE_ANIMATE)
				tmp = dynamic_cast<RIGameObject_Animate*>(pData)->animateObject(fElapsedTime);
			retval.insert(retval.end(), tmp.begin(), tmp.end());
		}
		return std::move(retval);
	}

	//override std::vector
	void push_back(IGameObject  * pObject)
	{
		SUPER::push_back(pObject);
		m_ObjectFlag |= pObject->getTypeFlag();
	}
	//override RIGameObject_Drawable;
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const
	{
		for (auto pData : *this)
			if (pData->getTypeFlag()&FLAG_OBJECTTYPE_DRAWALBE)
				dynamic_cast<RIGameObject_Drawable*>(pData)->drawOnDX(pRendererState);
	} 
	virtual const  bool isClear() const { return false; }

	void releaseObject() 
	{
		for (auto pData : *this)
			delete pData;
	}
};

class CLobbyComponent_Drawable : public AGameObject_ModelOrigin{
private:
	typedef AGameObject_ModelOrigin SUPER;
	
private:  
	static const FLAG_8 s_ObjectFlag = FLAG_OBJECTTYPE_DRAWALBE; 

public:
	CLobbyComponent_Drawable() {}
	virtual ~CLobbyComponent_Drawable() { CLobbyComponent_Drawable::releaseObject(); }

private:

public:
	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_ObjectFlag; }
	  
	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject *pMsg) {} 

	bool createObject(IFactory_LobbyComponent_Drawable_MLRS *pFactory);
	void releaseObject() {}

//	void setAnimState(ENUMTYPE_256 eAnimState) { m_eAnimState = eAnimState; }
};



class ALobbyComponent_Button :  virtual public IGameObject,
								virtual public RIGameObject_Drawable,
								virtual public RIGameObject_Animate, 
								virtual public RIGameObject_Button {
private:
	typedef CLobbyComponent_Drawable SUPER;

private:
	std::vector<CLobbyComponent_Drawable *> m_pComponents;
	RECT m_Rect;
	float m_fOnElapsedTime;
	float m_fOnTime;
	bool m_bOn;

public:
	ALobbyComponent_Button() :m_bOn (false),m_fOnElapsedTime(0),m_fOnTime(0){}
	virtual ~ALobbyComponent_Button() {}

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_DRAWALBE|FLAG_OBJECTTYPE_ANIMATE; }

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	virtual void drawOnDX(CRendererState_D3D *pRendererState)const ;
	virtual const  bool isClear() const { return false; }

	virtual bool checkInRect(unsigned int nPosX, unsigned int nPosY)const;

	bool createObject(IFactory_LobbyComponent_Button_MLRS *pFactory);
	void releaseObject();

	void setOn() { m_bOn = true; }
};

class CLobbyComponent_MakeRoom : public ALobbyComponent_Button {
public:
	CLobbyComponent_MakeRoom() {}
	virtual ~CLobbyComponent_MakeRoom() {}

	virtual IMsg * click();
};

class CLobbyComponent_Refresh : public ALobbyComponent_Button {
public:
	CLobbyComponent_Refresh() {}
	virtual ~CLobbyComponent_Refresh() {}

	virtual IMsg * click();
};

class CLobbyComponent_RoomList :	public CLobbyComponent_Drawable,
										virtual public RIGameObject_Button{
private:
	typedef CLobbyComponent_Drawable SUPER;


private:
	std::vector<RECT> m_Rects;
public:
	CLobbyComponent_RoomList() {}
	virtual ~CLobbyComponent_RoomList() {}

	virtual bool checkInRect(unsigned int nPosX, unsigned int nPosY)const ;
	virtual IMsg * click() ;

	bool createObject(IFactory_LobbyComponent_ButtonList_MLRS *pFactory);
};
  

#endif