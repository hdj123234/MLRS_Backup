#include "stdafx.h"
#include "GameObjectManager_MLRS_InGame.h"

#include"..\Module_MLRS_Object\Factory_Fume.h"
#include "Msg_InGame_MLRS.h"
#include "..\Module_MLRS_Object\ClientPlayer.h"
#include "..\Module_MLRS_Object\GlobalDataMap_MLRS.h"
#include "..\Module_MLRS_Object\BaseCamp_MLRS.h"
#include "..\Module_MLRS_Object\Creator_Missile_MLRS.h"
#include "..\Module_MLRS_Object\Creator_EnemyMecha_MLRS.h"
#include "..\Module_MLRS_Object\Interface_Object_MLRS.h"
#include "..\Module_MLRS_Object\Missile_MLRS.h"
#include "..\Module_MLRS_Object\EnemyMecha_MLRS.h"
#include "..\Module_MLRS_Object\MainMecha_MLRS.h"
#include "..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include "..\Module_Object_Common\Interface_Object_Common.h"
#include "..\Module_Object_Common\Terrain.h"
#include "..\Module_Object_Common\Msg_Effect.h"
#include "..\Module_Object_UI\Interface_UI.h"
#include "..\Module_Object\Msg_Movement.h"
#include "..\Module_Object\Space.h"
#include "..\Module_Object_Camera\Interface_Camera.h"
#include "..\Module_Sound\SoundManager.h"
#include "..\Module_Sound\Msg_Sound.h"
#include "..\Module_Scene\Interface_Scene.h"

#include"..\Module_MLRS_Object\State_ClientPlayer_MLRS.h"
//DEBUG
#include "..\Module_Object_Common\Painter_Common.h"

#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif

#ifndef HEADER_STL
#include<iostream>
#endif // !HEADER_STL

//----------------------------------- CGameObjectManager_MLRS_InGame -------------------------------------------------------------------------------------


CGameObjectManager_MLRS_InGame::CGameObjectManager_MLRS_InGame()
	:m_pLightManager(nullptr),
	m_pEffectManager(nullptr),
	m_pUIManager(nullptr),
	m_pPlayer(nullptr),
	m_pSkyBox(nullptr),
	m_pBaseCamp(nullptr),
	m_pBoss(nullptr),
	m_pTerrain(nullptr),
	m_bDrawBoundingObject(false),
	m_bDrawTerrianWire(false),
	m_nClientID(0),
	m_pFume(nullptr),
	m_pSoundManager(nullptr),
	m_pSpace(new CSpace())
{
	m_pReturnMsgs.push_back(new CMsg_CS_NoName());
	CObjectDecorator_ClientPlayer::setSpace(m_pSpace);
	
	AState_ClientPlayer::setSpace(m_pSpace);
}




void CGameObjectManager_MLRS_InGame::addObject_AfterProcessing(IGameObject * pObject)
{
	if (auto p = dynamic_cast<CObjectDecorator_ClientPlayer *>(pObject))
	{
		m_pPlayer = p;
		m_pPlayers.insert(m_pPlayers.begin(), m_pPlayer);
//		m_pObjects.push_back(m_pPlayer);		
	}
	else if (auto p = dynamic_cast<IGameModel *>(pObject))
	{
		m_pModels.push_back(p);
		m_pObjects.push_back(p);
		switch (CGlobalDataMap_MLRS::getModelType(p))		{
		case EModelType::eEnemy:
		{
			//체크
			EnemyMechaBuilderAndReuseList builderAndReuseList;
			builderAndReuseList.first = new CMultiBuilder_EnemyMecha_Instanced(p,new CFactory_EnemyMecha_Zephyros_Instanced(p));
			m_pEnemyModelMap.insert(std::make_pair(CGlobalDataMap_MLRS::getEnemyType(p), builderAndReuseList));
			break;
		}
		case EModelType::eMissile:
		{
			MissileBuilderAndReuseList builderAndReuseList;
			builderAndReuseList.first = new CMultiBuilder_Missile(p);
			m_pMissileModelMap.insert(std::make_pair(CGlobalDataMap_MLRS::getMissileType(p), builderAndReuseList));
			break;
		}
		}
	}
	else if (auto p = dynamic_cast<IMainMecha *>(pObject))
	{
		p->setID(static_cast<unsigned int>(m_pPlayers.size()));
		m_pPlayers.push_back(p);
//		m_pObjects.push_back(p);
//		m_pSpace->addObject(p);

	}
	else if (auto p = dynamic_cast<CBaseCamp *>(pObject))
	{
		m_pBaseCamp = p;
		m_pObjects.push_back(p);
		m_pSpace->addObject(p);
	}

	else if (auto p = dynamic_cast<IEnemy *>(pObject))
	{
		p->setID(static_cast<unsigned int>(m_pEnemys.size()));
		m_pEnemys.push_back(p);
//		m_pObjects.push_back(p);
	}
	else if (auto p = dynamic_cast<IBoss *>(pObject))
	{
		m_pBoss = p;
		m_pObjects.push_back(p);
		m_pSpace->addObject(p);
	}
	else if (auto p = dynamic_cast<CMissile *>(pObject))
	{
		p->setID(static_cast<unsigned int>(m_pMissiles.size()));
		m_pMissiles.push_back(p);
//		m_pObjects.push_back(p);

//		m_pSpace->addObject(dynamic_cast<RIGameObject_Collision *>(p));
	}
	else if (auto p = dynamic_cast<IUIManager *>(pObject))
	{
		m_pUIManager = p;
//		m_pObjects.push_back(p);
	}
	else if (auto p = dynamic_cast<IEffectManager *>(pObject))
	{
		m_pEffectManager = p;
		m_pObjects.push_back(p);
	}
	else if (auto p = dynamic_cast<CTerrain *>(pObject))
	{
		m_pTerrain = p;
		m_pObjects.push_back(p);
		m_pSpace->setTerrain(p);
	}
	else if (auto p = dynamic_cast<ISkyBox *>(pObject))
		m_pSkyBox = p;
	else if (auto p = dynamic_cast<ILightManager *>(pObject))
		m_pLightManager = p;
	//		else if (dynamic_cast<ILight *>(pObjects_Origin->at(i)))
	//			m_pLights.push_back( dynamic_cast<ILight *>(pObjects_Origin->at(i)));
	//DEBUG
	else if (auto p = dynamic_cast<CPainter_BoundingBoxManager *>(pObject))
	{
		m_pBBManager = p;
		m_pObjects.push_back(p);
	}
	else if (auto p = dynamic_cast<CPainter_Line *>(pObject))
	{
		dynamic_cast<CSpace *>(m_pSpace)->setLine( p);
		m_pObjects.push_back(p);
	}
	else if (auto p = dynamic_cast<CFume *>(pObject))
	{
		m_pFume = p;
		m_pObjects.push_back(p);
	}
	else if (auto p = dynamic_cast<CSoundManager *>(pObject))
	{
		m_pSoundManager = p;
		m_pObjects.push_back(p);
		m_pMsgQueue.push(new CMsg_MLRS_Sound(new CMsg_Sound_RunBGM(0)));
	}
	else
		m_pObjects.push_back(pObject);
}

void CGameObjectManager_MLRS_InGame::setOriginData_AfterProcessing()
{
	for (auto data : *m_pObjects_Origin)
		addObject_AfterProcessing(data);

	m_pSoundManager->setListener(m_pPlayer);

	m_bReady = true;
}

void CGameObjectManager_MLRS_InGame::handleMessage_RealProcessing()
{
	//for (unsigned int i = 0; i < m_pMsgQueue.size();++i)
	//{
	//	auto data = m_pMsgQueue.front();
	//	m_pMsgQueue.pop();
	//	m_pMsgQueue.push(data);
	//	if (IMsg_Movement * pMsg = dynamic_cast<IMsg_Movement *>(data))
	//		if(	pMsg->getMovementType() == EMsgType_Movement::Move_LookBase)
	//			break;
	//}
	while (!m_pMsgQueue.empty())
	{
		auto data = m_pMsgQueue.front();


		auto pMsg_MLRS = static_cast<CMsg_MLRS *>(data);

		switch (pMsg_MLRS->getMsgType())
		{
		case EMsgType_MLRS::eReceivedFromServer:
		{
			CMsg_SC *pMsg = static_cast<CMsg_SC *>(data);
			switch (pMsg->getType())
			{
			case EMsgType_SC::eChangeWeapon:
				m_pPlayers[static_cast<CMsg_SC_ChangeWeapon *>(pMsg)->m_nID]->handleMsg(pMsg);
				break;
			case EMsgType_SC::eClientID:
			{
				unsigned int nID = static_cast<CMsg_SC_ID *>(pMsg)->m_nID;
				auto tmp = m_pPlayers[nID];
				m_pPlayers[nID] = m_pPlayers[0];
				m_pPlayers[0] = tmp;
				m_nClientID = nID;
				m_pPlayers[nID]->setID(nID);
				m_pPlayers[0]->setID(0);
				break;
			}
			case EMsgType_SC::eCreateObj:
			{
				auto pMsg_Create = static_cast<CMsg_SC_CreateObj *>(pMsg);
				switch (pMsg_Create->m_eTarget)
				{
				case EObjTarget_SC::ePlayer:
					m_pPlayers[pMsg_Create->m_nID]->enable();
					m_pPlayers[pMsg_Create->m_nID]->setID(pMsg_Create->m_nID);
					m_pPlayers[pMsg_Create->m_nID]->handleMsg(static_cast<CMsg_SC*>(pMsg_Create));
					m_pSpace->addObject(m_pPlayers[pMsg_Create->m_nID]);
					break;
				case EObjTarget_SC::eEnemy:
					if (m_pEnemys.size() <= pMsg_Create->m_nID)
						m_pEnemys.resize(pMsg_Create->m_nID + 1);
					//if (m_pEnemyModelMap[pMsg_Create->getModelID()].second.empty())
						m_pEnemys[pMsg_Create->m_nID] = m_pEnemyModelMap[pMsg_Create->getModelID()].first->build();
					//else
					//{
					//	m_pEnemys[pMsg_Create->m_nID] = m_pEnemyModelMap[pMsg_Create->getModelID()].second.front();
					//	m_pEnemyModelMap[pMsg_Create->getModelID()].second.pop();
					//}
					m_pEnemys[pMsg_Create->m_nID]->enable();
					m_pEnemys[pMsg_Create->m_nID]->setID(pMsg_Create->m_nID);
					m_pEnemys[pMsg_Create->m_nID]->handleMsg(static_cast<CMsg_SC*>(pMsg_Create));
					m_pSpace->addObject(m_pEnemys[pMsg_Create->m_nID]);
					break;
				case EObjTarget_SC::eMissile:
					if (m_pMissiles.size() <= pMsg_Create->m_nID)
						m_pMissiles.resize(pMsg_Create->m_nID + 1);
					if (m_pMissileModelMap[pMsg_Create->getModelID()].second.empty())
						m_pMissiles[pMsg_Create->m_nID] = dynamic_cast<CMissile*>(m_pMissileModelMap[pMsg_Create->getModelID()].first->build());
					else
					{
						m_pMissiles[pMsg_Create->m_nID] = m_pMissileModelMap[pMsg_Create->getModelID()].second.front();
						m_pMissileModelMap[pMsg_Create->getModelID()].second.pop();
					}
					m_pMissiles[pMsg_Create->m_nID]->enable();
					m_pMissiles[pMsg_Create->m_nID]->setID(pMsg_Create->m_nID);
					m_pMissiles[pMsg_Create->m_nID]->move(pMsg_Create->getMovementMsg());
					m_pFume->addSprayPoint(m_pMissiles[pMsg_Create->m_nID]);//체크
					//m_pSpace->addObject(m_pMissiles[pMsg_Move->m_nID]);
					break;
				}
				break;
			}
			case EMsgType_SC::eRemoveObj:
				switch (static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_eObjType)
				{
				case EObjTarget_SC::ePlayer:
					m_pPlayers[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]->disable();
					m_pSpace->releaseObject(m_pPlayers[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]);
					break;
				case EObjTarget_SC::eEnemy:
					if (m_pEnemys.size() <= static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID)break;
					if (!m_pEnemys[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID])	break;
					m_pEnemys[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]->disable();
					m_pSpace->releaseObject(m_pEnemys[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]);
					m_pEnemyModelMap[m_pEnemys[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]->getType()].second.push(m_pEnemys[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]);
					m_pEnemys[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID] = nullptr;
					break;
				case EObjTarget_SC::eMissile:
				{
					if (m_pMissiles.size() <= static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID)break;
					if (!m_pMissiles[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID])	break;
					//auto pPosOrigin = m_pMissiles[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]->getPositionOrigin();
					//m_pSpace->releaseObject(m_pMissiles[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]);
					//DirectX::XMFLOAT3 pos(pPosOrigin->x, pPosOrigin->y, pPosOrigin->z);

					m_pFume->removeSprayPoint(m_pMissiles[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]);//체크
					m_pMissiles[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]->disable();
					m_pEffectManager->handleMsg(&CMsg_Effect_Create(0, static_cast<CMsg_SC_RemoveObj *>(pMsg)->m_vPos));
					m_pSoundManager->handleMsg(&CMsg_Sound_Run3DSE(1, static_cast<CMsg_SC_RemoveObj *>(pMsg)->m_vPos));
					m_pMissileModelMap[m_pMissiles[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]->getType()].second.push(m_pMissiles[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID]);
					m_pMissiles[static_cast<CMsg_SC_ObjTypeAndID *>(pMsg)->m_nID] = nullptr;


					break;
				}
				}
				break;
			case EMsgType_SC::eMove:
			{
				unsigned int nID = static_cast<CMsg_SC_Move *>(pMsg)->m_nID;
				switch (static_cast<CMsg_SC_Move *>(pMsg)->m_eTarget)
				{
				case EObjTarget_SC::ePlayer:
					if (!m_pPlayers[nID]) break;
					m_pPlayers[nID]->enable();
					m_pPlayers[nID]->handleMsg(static_cast<CMsg_SC *>(pMsg));
					break;
				case EObjTarget_SC::eEnemy:
					if (nID >= m_pEnemys.size()) break;
					if (!m_pEnemys[nID]) break;
					m_pEnemys[nID]->enable();
					m_pEnemys[nID]->handleMsg(static_cast<CMsg_SC *>(pMsg));
					break;
				case EObjTarget_SC::eMissile:
					if (nID >= m_pMissiles.size())	break;
					if (!m_pMissiles[nID]) break;
					m_pMissiles[nID]->enable();
					m_pMissiles[nID]->move(static_cast<CMsg_SC_Move *>(pMsg)->getMovementMsg());
					//					if(nID>10)
					//						m_pEnemys[nID]->disable();

					break;

				case EObjTarget_SC::eBaseCamp:
					m_pBaseCamp->move(static_cast<CMsg_SC_Move *>(pMsg)->getMovementMsg());
					break;
				case EObjTarget_SC::eBoss:
					m_pBoss->enable();
					m_pBoss->move(static_cast<CMsg_SC_Move *>(pMsg)->getMovementMsg());
					break;
				}
				break;
			}
			case EMsgType_SC::ePlayerDown:
				m_pPlayers[static_cast<CMsg_SC_ClientID *>(pMsg)->m_nID]->handleMsg(static_cast<CMsg_SC *>(pMsg));
				break;

			case EMsgType_SC::eDrawScript:
				m_pUIManager->handleMsg(pMsg);
				break;
			case EMsgType_SC::eHP:
			{
				auto p = static_cast<CMsg_SC_HP *>(pMsg);
				if(p->m_eObjType==EObjTarget_SC::ePlayer&&p->m_nID == m_nClientID)
					m_pUIManager->handleMsg(pMsg);
				break;
			}
			}
			break;
		}
		case EMsgType_MLRS::eControlPlayer:  
			m_pPlayer->handleMsg(data);
			break; 
		case EMsgType_MLRS::eUI:
			m_pUIManager->handleMsg(data);
			break;
		case EMsgType_MLRS::eSound:
			m_pSoundManager->handleMsg(static_cast<CMsg_MLRS_Sound*>(pMsg_MLRS)->getSoundMsg());
			break;
		case EMsgType_MLRS::eDebug:
			switch (static_cast<CMsg_MLRS_DEBUG *>(data)->getMsgType_DEBUG()){
			case EMsgType_MLRS_DEBUG::eDebug_SwapBoundingBox:
				m_bDrawBoundingObject = !m_bDrawBoundingObject;
				if(m_bDrawBoundingObject)
					dynamic_cast<CSpace *>(m_pSpace)->enable_Line();
				else
					dynamic_cast<CSpace *>(m_pSpace)->disable_Line();

				break;
			case EMsgType_MLRS_DEBUG::eDebug_SwapTerrainWire:
				m_bDrawTerrianWire = !m_bDrawTerrianWire;
				dynamic_cast<CTerrain *>(m_pTerrain)->setWire(m_bDrawTerrianWire);

				break;
			}
		}

		delete data;
		m_pMsgQueue.pop();
	}
}

void CGameObjectManager_MLRS_InGame::updateScene_RealProcessing(IScene * pScene)
{
	//DEBUG
	m_pBBManager->clearBoundingBox();
	if (m_bDrawBoundingObject)
	{
		for (auto pData : m_pSpace->getObjects())
		{
			if(auto p = dynamic_cast<const CBoundingObject_OOBB *>(pData->getBoundingObject()))
				m_pBBManager->addBoundingBox(p->getOOBB());
		}
	}

	//for (auto pData : m_pPlayers)
	//	m_pBBManager->addBoundingBox(dynamic_cast<const CBoundingObject_OOBB *>(pData->getBoundingObject())->getOOBB());

	pScene->addObject_Renewal(m_pPlayer->getCamera());
	pScene->addObject_Renewal(m_pLightManager);

	if(m_pSkyBox)
	pScene->setSkyBox(m_pSkyBox);

	for (auto data : m_pObjects)
	{
		if (data->getTypeFlag() & FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW)
			if (auto *pObj = dynamic_cast<RIGameObject_BeMust_RenewalForDraw *>(data))
				pScene->addObject_Renewal(pObj);
		if (data->getTypeFlag() & FLAG_OBJECTTYPE_DRAWALBE)
			if (RIGameObject_Drawable *pObj = dynamic_cast<RIGameObject_Drawable *>(data))
					pScene->addObject(pObj);
	}

	//std::vector<RIGameObject_Drawable *> vClear;
	//for (auto data : m_pObjects)
	//{
	//	if (data->getTypeFlag() & FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW)
	//	{
	//		if (auto *pObj = dynamic_cast<RIGameObject_BeMust_RenewalForDraw *>(data))
	//				pScene->addObject_Renewal(pObj);
	//	}
	//	if (data->getTypeFlag() & FLAG_OBJECTTYPE_DRAWALBE)
	//	{
	//		if (RIGameObject_Drawable *pObj = dynamic_cast<RIGameObject_Drawable *>(data))
	//		{
	//			if (data->getTypeFlag()&FLAG_OBJECTTYPE_DRAWALBE_CLEAR)
	//				vClear.push_back(pObj);
	//			else
	//				pScene->addObject(pObj);
	//		}
	//	}
	//}
	//for (auto data : vClear)
	//	pScene->addObject(data);
	pScene->addObject(m_pUIManager);
}

void CGameObjectManager_MLRS_InGame::initScene(IScene * pScene)
{
	if (!m_bReady)		return;
	pScene->addObject_Init(dynamic_cast<const RIGameObject_BeMust_InitForDraw *>(m_pPlayer->getCamera()));
	for (auto data : *m_pObjects_Origin)
		if (dynamic_cast<RIGameObject_BeMust_InitForDraw *>(data))
			pScene->addObject_Init(dynamic_cast<RIGameObject_BeMust_InitForDraw *>(data));

}

std::vector<IMsg_GameObject *> CGameObjectManager_MLRS_InGame::animateObjects(const float fElapsedTime)
{ 
	std::vector<IMsg_GameObject *> retval;
	for (auto data : m_pObjects)
	{
		if (data->getTypeFlag() & FLAG_OBJECTTYPE_ANIMATE)
		{
			if (RIGameObject_Animate *pObj = dynamic_cast<RIGameObject_Animate *>(data))
			{
				std::vector<IMsg *> &rpMsgs = pObj->animateObject(fElapsedTime);
				for (auto pMsg : rpMsgs)
				{
					if(dynamic_cast<CMsg_CS *>(pMsg))						
						m_pReturnMsgs.push_back(pMsg);
					else if (auto p = dynamic_cast<IMsg_GameObject *>(pMsg) )
						retval.push_back(p);
					else
						m_pReturnMsgs.push_back(pMsg);
				}
			}
		}
	}
	for (auto pObj : m_pPlayers)
	{

		std::vector<IMsg *> &rpMsgs = pObj->animateObject(fElapsedTime);
		for (auto pMsg : rpMsgs)
		{
			if (dynamic_cast<CMsg_CS *>(pMsg))
				m_pReturnMsgs.push_back(pMsg);
			else if (auto p = dynamic_cast<IMsg_GameObject *>(pMsg))
				retval.push_back(p);
			else
				m_pReturnMsgs.push_back(pMsg);
		}
	}
	for (auto pObj : m_pEnemys)
	{
		if (!pObj)	continue;

		std::vector<IMsg *> &rpMsgs = pObj->animateObject(fElapsedTime);
		for (auto pMsg : rpMsgs)
		{
			if (dynamic_cast<CMsg_CS *>(pMsg))
				m_pReturnMsgs.push_back(pMsg);
			else if (auto p = dynamic_cast<IMsg_GameObject *>(pMsg))
				retval.push_back(p);
			else
				m_pReturnMsgs.push_back(pMsg);
		}
	}
	for (auto pObj : m_pMissiles)
	{
		if (!pObj)	continue;

		std::vector<IMsg *> &rpMsgs = pObj->animateObject(fElapsedTime);
		for (auto pMsg : rpMsgs)
		{
			if (dynamic_cast<CMsg_CS *>(pMsg))
				m_pReturnMsgs.push_back(pMsg);
			else if (auto p = dynamic_cast<IMsg_GameObject *>(pMsg))
				retval.push_back(p);
			else
				m_pReturnMsgs.push_back(pMsg);
		}
	}
	{
		std::vector<IMsg *> &rpMsgs = m_pBoss->animateObject(fElapsedTime);
		for (auto pMsg : rpMsgs)
		{
			if (dynamic_cast<CMsg_CS *>(pMsg))
				m_pReturnMsgs.push_back(pMsg);
			else if (auto p = dynamic_cast<IMsg_GameObject *>(pMsg))
				retval.push_back(p);
			else
				m_pReturnMsgs.push_back(pMsg);
		}
	}
	{
		std::vector<IMsg *> &rpMsgs = m_pUIManager->animateObject(fElapsedTime);
		for (auto pMsg : rpMsgs)
		{
			if (auto p = dynamic_cast<IMsg_GameObject *>(pMsg))
				retval.push_back(p);
			else
				m_pReturnMsgs.push_back(pMsg);
		}
	}
	//if (RIGameObject_Animate *pObject_Animate = dynamic_cast<RIGameObject_Animate *>(m_pPlayer))
	//	  pMsg = pObject_Animate->animateObject(fElapsedTime);
	////if (IGameObjectDecorator * pDecorator = dynamic_cast<IGameObjectDecorator *>(m_pPlayer))
	////{
	////	if(RIGameObject_Animate *pObject_Animate = dynamic_cast<RIGameObject_Animate *>(pDecorator->getObject()))
	////		pObject_Animate->animateObject(fElapsedTime);
	////}
	//if (pMsg)
	return retval;
}

void CGameObjectManager_MLRS_InGame::releaseObject()
{
	delete m_pSpace;
	for (auto &rData : m_pEnemys)
		if (rData) delete rData;
	for (auto &rData : m_pMissiles)
		if(rData) delete rData;

	for (auto &rData : m_pEnemyModelMap)
	{
		delete rData.second.first;
		while (!rData.second.second.empty())
		{
			delete rData.second.second.front();
			rData.second.second.pop();
		}
	}
	for (auto &rData : m_pMissileModelMap)
	{
		delete rData.second.first;
		while (!rData.second.second.empty())
		{
			delete rData.second.second.front();
			rData.second.second.pop();
		}
	}
}
