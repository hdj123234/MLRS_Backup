#pragma once
#ifndef HEADER_GAMEOBJECTMANAGER_MLRS_INGAME
#define HEADER_GAMEOBJECTMANAGER_MLRS_INGAME

#include"..\Module_World\GameObjectManager.h"
#include"..\Module_DXComponent\Type_InDirectX.h"

//���漱��
class CSoundManager;
class CScene;
class RIGameObject_Button;
 

class CGameObjectManager_MLRS_Lobby : public CGameObjectManager {
protected:
	//override IGameObjectManager
	std::vector<IGameObject *> m_pObjects;
	std::vector<RIGameObject_Button *> m_pObjects_Button;
//	std::queue<IMsg_GameObject *> m_pMsgQueue;
	CSoundManager *m_pSoundManager; 

	unsigned int m_nClientID;

public:
	CGameObjectManager_MLRS_Lobby();
	virtual ~CGameObjectManager_MLRS_Lobby() { CGameObjectManager_MLRS_Lobby::releaseObject(); }

private:
	//Virtual Func
	virtual void addObject_AfterProcessing(IGameObject *pObject);
	virtual void deleteObject_AfterProcessing(IGameObject *pObject) {}
	virtual void setOriginData_AfterProcessing();
	virtual void handleMessage_RealProcessing();
	virtual void updateScene_RealProcessing(IScene *pScene);

public:
	//override IGameObjectManager
	virtual void initScene(IScene * pScene);
	virtual std::vector<IMsg_GameObject *> animateObjects(const float fElapsedTime);
	virtual void processCollision() {  }
	virtual void updateObjectGroup() {}

	void releaseObject();

	unsigned int  getClientID() { return m_nClientID; }
};

#endif