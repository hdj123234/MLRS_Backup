#pragma once
#ifndef HEADER_ADAPTER_MSG_TOGAME_MLRS_INGAME
#define HEADER_ADAPTER_MSG_TOGAME_MLRS_INGAME

#include"..\Module_Object\Interface_Object.h"

//���漱��
class CMsg_Windows;


class CAdapter_Msg_ToGame_MLRS_InGame : public IAdapter_Msg_ToGame {
public:
	CAdapter_Msg_ToGame_MLRS_InGame();
	~CAdapter_Msg_ToGame_MLRS_InGame();

	IMsg_GameObject * adapt(IMsg *pMsg);

	IMsg_GameObject * createFlagMsg(CMsg_Windows * pMsg, bool bOn,bool bAttack=false);
	IMsg_GameObject * createSetTypeMsg(CMsg_Windows * pMsg);
};

#endif