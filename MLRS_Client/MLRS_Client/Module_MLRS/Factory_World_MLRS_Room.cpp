﻿#include"stdafx.h"
#include "Factory_World_MLRS_Room.h"

#include"..\Module_Scene\Scene.h"
#include"GameObjectManager_MLRS_Lobby.h"
#include"Adapter_Msg_ToGame_MLRS_Lobby.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"Factory_LobbyComponent_MLRS.h"
#include"LobbyComponent_MLRS.h"
#include"..\MyCSVTable.h"
#include"Factory_World_MLRS_Lobby.h"

 
#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT

#ifndef HEADER_STL
#include<iostream>
#include<sstream>
#include<iomanip>
#endif // !HEADER_MYLOGCAT

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DirectX


using namespace DirectX;
 

//----------------------------------- CFactory_World_MLRS_Lobby -------------------------------------------------------------------------------------

void CFactory_World_MLRS_Room::processCreateObjectsFailed(std::vector<IGameObject*>& result)
{
	MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CFactory_World_MLRS_InGame.createObjects() Error");
	for (auto data : result)
		delete data;
	result.clear();
}

std::vector<IGameObject *> CFactory_World_MLRS_Room::createObjects()
{ 
	std::vector<IGameObject *> retval; 

	auto &rSection_Lobby = m_rIniData.getSection("Lobby");
	CMyINIData lobbyIni(g_sDataFolder + rSection_Lobby.getParam("LobbyIni"));
	CMyCSVTable roomTable(g_sDataFolder + rSection_Lobby.getParam("RoomTable"));
	auto &rTable = roomTable.getTable();

	CLobbyComponent_Statics *pComponents = new CLobbyComponent_Statics;
	retval.push_back(pComponents);

	for (auto &rLabel : rTable)
	{
		auto eType = convertType(rLabel[6]);

		if (eType == EType_RoomComponent::eImage) {
			CLobbyComponent_Drawable *pComponent = new CLobbyComponent_Drawable;
			//	retval.push_back(pComponent);
			pComponents->push_back(pComponent);
			if (!pComponent->createObject(&CFactory_LobbyComponent(rLabel[0],
				DirectX::XMFLOAT2(std::stof(rLabel[2]), std::stof(rLabel[3])),
				DirectX::XMFLOAT2(std::stof(rLabel[4]), std::stof(rLabel[5])),
				"Lobby/" + rLabel[0])))
			{
				for (auto pData : retval)
					delete pData;
				return std::vector<IGameObject *>();
				break;
			}
		}
		else if (eType == EType_RoomComponent::eButton_Ready|| eType == EType_RoomComponent::eButton_Exit)
		{
			CLobbyComponent_MakeRoom *pComponent = new CLobbyComponent_MakeRoom;
			//	retval.push_back(pComponent);
			retval.push_back(pComponent);
			if (!pComponent->createObject(&CFactory_LobbyComponent_Button_OnOff(rLabel[0],
				DirectX::XMFLOAT2(std::stof(rLabel[2]), std::stof(rLabel[3])),
				DirectX::XMFLOAT2(std::stof(rLabel[4]), std::stof(rLabel[5])),
				"Lobby/" + rLabel[0],
				"Lobby/" + rLabel[1])))
			{
				for (auto pData : retval)
					delete pData;
				return std::vector<IGameObject *>();
				break;
			}
		} 

	}
	return retval;
}
IGameObjectManager * CFactory_World_MLRS_Room::createObjectManager()
{
	CGameObjectManager_MLRS_Lobby *pResult = new CGameObjectManager_MLRS_Lobby();
	return pResult;

}
IScene* CFactory_World_MLRS_Room::createScene()
{
	CScene *pResult = new CScene();
//	CScene_DeferredLighting *pResult = new CScene_DeferredLighting();
	return pResult;
}

IAdapter_Msg_ToGame * CFactory_World_MLRS_Room::createMsgAdapter()
{
	CAdapter_Msg_ToGame_MLRS_Lobby* pMsgAdapter = new CAdapter_Msg_ToGame_MLRS_Lobby();
	return pMsgAdapter;
}

IRenderer * CFactory_World_MLRS_Room::createRenderer()
{ 
	CRenderer_D3D *pRenderer=new CRenderer_D3D();
	if (!CBuilder_Renderer_D3D_Lobby().build(pRenderer))
	//CRenderer_D3D_DeferredLighting *pRenderer = new CRenderer_D3D_DeferredLighting();
	//if (!CBuilder_Renderer_D3D_DeferredLighting(vColor, vRange).build(pRenderer))
	{
		delete pRenderer;
		return nullptr;
	}
	return pRenderer;
}

EType_RoomComponent CFactory_World_MLRS_Room::convertType(const std::string & rsType)
{
	if (CUtility_String::convertToLower(rsType) == "image")	return EType_RoomComponent::eImage;
	else if (CUtility_String::convertToLower(rsType) == "ready")	return EType_RoomComponent::eButton_Ready;
	else if (CUtility_String::convertToLower(rsType) == "exit")	return EType_RoomComponent::eButton_Exit;
	else return EType_RoomComponent::eImage;
}
