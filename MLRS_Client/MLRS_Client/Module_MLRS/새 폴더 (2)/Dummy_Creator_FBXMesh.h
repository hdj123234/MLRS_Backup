#ifndef HEADER_DUMMY_CREATOR_FBXMESH
#define HEADER_DUMMY_CREATOR_FBXMESH

#include"..\Module_MLRS_Object\Mesh_MLRS.h"

#include "..\Module_Object_Instancing\ModelDecorator_Instancing.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"

#include "..\Module_Object\Interface_Object.h"
//#include "..\Module_Object_Common\Bone.h"
//#include "Model_FBX_Dummy.h"
//#include"..\Module_MLRS_Object\Mesh_MLRS.h"

#include"..\..\FBXConverter\FBXLoader.h"

//전방선언
struct Type_Bone;
class CMyINISection;
enum EActionType : ENUMTYPE_256;

class CFactory_FBXMesh_Dummy :	virtual public IFactory_Mesh_MLRS {
private:
	const CMesh_Converted &m_rMesh;
	const std::string m_sVSName;
public:
	CFactory_FBXMesh_Dummy(const CMesh_Converted &rMesh,
		const std::string &rsVSName = "vsDummy_FBX");
	virtual ~CFactory_FBXMesh_Dummy() {}

	//override IFactory_Mesh_Vertex
	virtual ID3D11VertexShader	*	createVertexShader();
	virtual ID3D11InputLayout	*	createInputLayout();
	virtual ID3D11Buffer		*	createVertexBuffer();
	virtual ID3D11Buffer		*	createIndexBuffer() { return nullptr; }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST; }
	virtual unsigned int			getStride() { return sizeof(CVertex_Mesh_MLRS); }
	virtual unsigned int			getOffset() { return 0; }
	virtual unsigned int			getNumberOfVertex();
	virtual unsigned int			getNumberOfIndex() { return 0; }
	virtual const unsigned int		getMaterialID();
	
	std::vector<ID3D11ShaderResourceView *> createTBuffers();
	std::vector<ID3D11Buffer*> createConstBuffer_Transform();
};


//----------------------------------- 인스턴싱용 -------------------------------------------------------------------------------------

class CFactory_FBXMesh_Dummy_Instancing : virtual public IFactory_Mesh_MLRS_Instancing {
private:
	const CMesh_Converted &m_rMesh;
	const unsigned int m_nMaxNumberOfInstance;
	const std::string m_sVSName;

public:
	CFactory_FBXMesh_Dummy_Instancing(const CMesh_Converted &rMesh,
									const unsigned int nMaxNumberOfInstance,
									const std::string &rsVSName = "vsDummy_FBX_Instancing");
	virtual ~CFactory_FBXMesh_Dummy_Instancing() {}

	//override IFactory_Mesh_Vertex
	virtual ID3D11VertexShader	*	createVertexShader();
	virtual ID3D11InputLayout	*	createInputLayout();
	virtual ID3D11Buffer		*	createVertexBuffer();
	virtual ID3D11Buffer		*	createIndexBuffer() { return nullptr; }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST; }
	virtual unsigned int			getStride() { return sizeof(CVertex_Mesh_MLRS); }
	virtual unsigned int			getOffset() { return 0; }
	virtual unsigned int			getNumberOfVertex();
	virtual unsigned int			getNumberOfIndex() { return 0; }
	virtual const unsigned int		getMaterialID();

	std::vector<ID3D11ShaderResourceView *> createTBuffers();
	std::vector<ID3D11Buffer*> createConstBuffer_Transform();

	virtual const unsigned int getMaxNumberOfInstance() { return m_nMaxNumberOfInstance; }
	virtual ID3D11Buffer * createInstanceBuffer() ;
};


#endif