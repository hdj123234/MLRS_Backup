#include "stdafx.h"
#include "Dummy_Creator_FBXModel.h"

#include "Dummy_Creator_FBXMesh.h"

#include"..\Module_Object_Instancing\Factory_AnimData_Instancing.h"
#include"..\Module_Object_Instancing\ModelDecorator_Instancing.h"
#include"..\Module_Object_GameModel\Factory_Material.h"
#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"
#include"..\Module_Object_GameModel\Factory_AnimData.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Type_InDirectX.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_Object_Camera\Factory_Camera_ThirdPerson.h"
#include"..\Module_Object\Factory_AnimController.h"
#include"..\Module_Object_Common\ModelTester.h"

#include"..\Module_Printer\DXRendererState.h"
#include "..\Module_MLRS_Object\MainMecha_MLRS.h"
#include "..\MyINI.h"

using namespace DirectX;


//----------------------------------- AFactory_FBXModel -------------------------------------------------------------------------------------

AFactory_FBXModel::AFactory_FBXModel(
	const std::string & rsFileName,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform,
	const std::string & rsVSName, 
	const std::string & rsPSName)
	:CFBXLoader(rmGlobalTransform), 
	m_sFileName(rsFileName),
	m_sVSName(rsVSName),
	m_sPSName(rsPSName)
{
	CFBXLoader::loadFile(g_sDataFolder + m_sFileName);
}

AFactory_FBXModel::AFactory_FBXModel(
	const CMyINISection & rSection,
	const std::string & rsVSName, 
	const std::string & rsPSName)
	:CFBXLoader(rSection.getParam_XMFLOAT4X4_FromPosAndScale("PosAndScale")),
	m_sFileName(rSection.getParam("FileName")),
	m_sVSName(rsVSName),
	m_sPSName(rsPSName)
{
	CFBXLoader::loadFile(g_sDataFolder + m_sFileName);
}


std::vector<IMesh *> AFactory_FBXModel::createMeshs()
{
	auto &rMeshs = CFBXLoader::getMeshs_Converted();


	std::vector<IMesh *> result;

	for (auto &data : rMeshs)
	{
		CFactory_FBXMesh_Dummy factory(data, m_sVSName);

		CMesh_MLRS *pMesh = new CMesh_MLRS;
		if (!pMesh->createObject(&factory))
		{
			delete pMesh;
			for (auto data2 : result)
				delete data2;
			return result;
		}
		result.push_back(pMesh);
	}
	return  result;
}
std::vector<IMaterial *> AFactory_FBXModel::createMaterials()
{

	auto &rMaterials = CFBXLoader::getMaterials();
	std::vector<Type_Material> materials;
	std::vector<std::string> names_DiffuseMap;
	std::vector<std::string> names_NormalMap;
	for (auto &rData : rMaterials)
	{
		Type_Material material;
		material.m_vAmbient = rData.m_vAmbient;
		material.m_vDiffuse = rData.m_vDiffuse;
		material.m_vSpecular = rData.m_vSpecular;

		materials.push_back(material);
		names_DiffuseMap.push_back(rData.m_Maps[EMapType::eDiffuse]);
		names_NormalMap.push_back(rData.m_Maps[EMapType::eNormal]);
	}


	CFactory_MaterialSet_Lambert_DiffuseNormal factory_Light(m_sFileName + "_Maps",
		m_sPSName,
		materials,
		names_DiffuseMap,
		names_NormalMap);
	CMaterialSet_Lambert_DiffuseNormal *pMaterial = new CMaterialSet_Lambert_DiffuseNormal();
	if (!pMaterial->createObject(&factory_Light))
	{
		delete pMaterial;
		return std::vector<IMaterial *>();
	}

	return std::vector<IMaterial *>{pMaterial};

}

//----------------------------------- CFactory_FBXModel_NoAnim -------------------------------------------------------------------------------------

CFactory_FBXModel_NoAnim::CFactory_FBXModel_NoAnim(
	const std::string & rsFileName, 
	const DirectX::XMFLOAT4X4 & rmGlobalTransform,
	const std::string & rsVSName,
	const std::string & rsPSName)
	:AFactory_FBXModel(rsFileName, rmGlobalTransform, rsVSName, rsPSName)
{
}

CFactory_FBXModel_NoAnim::CFactory_FBXModel_NoAnim(
	const CMyINISection & rSection, 
	const std::string & rsVSName,
	const std::string & rsPSName)
	: AFactory_FBXModel(rSection, rsVSName,  rsPSName)
{
}

//----------------------------------- CFactory_FBXModel -------------------------------------------------------------------------------------



CFactory_FBXModel::CFactory_FBXModel(
	const std::string & rsFileName, 
	const StartPointList &rAnimList,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform,
	const float fAnimFPS,
	const std::string & rsVSName,
	const std::string & rsPSName)
	:AFactory_FBXModel(rsFileName,rmGlobalTransform, rsVSName, rsPSName),
	m_AnimList(rAnimList),
	m_fAnimFPS(fAnimFPS)
{
}

CFactory_FBXModel::CFactory_FBXModel(
	const CMyINISection & rSection, 
	const StartPointList & rAnimList, 
	const float fAnimFPS, 
	const std::string & rsVSName, 
	const std::string & rsPSName)
	:AFactory_FBXModel(rSection,rsVSName, rsPSName),
	m_AnimList(rAnimList),
	m_fAnimFPS(fAnimFPS)
{
}

IAnimData	* CFactory_FBXModel::createAnimData()
{
	auto &rData = CFBXLoader::getBoneStructure();
	const unsigned int nNumberOfBone = static_cast<unsigned int>(rData.size());
	std::vector<Type_Bone> bones(nNumberOfBone);
	for (unsigned int i = 0; i < nNumberOfBone; ++i)
	{
		bones[i].m_nParentIndex = rData[i].m_nParentIndex;
		
		DirectX::XMStoreFloat4x4(&bones[i].m_mTransform_ToLocal, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&rData[i].m_mTransform_ToLocal)));
	}

	CAnimData_NoInstancing * pAnimData = new CAnimData_NoInstancing();
	if (!pAnimData->createObject(&CFactory_AnimData_NoInstancing(m_sFileName, 
													bones, 
													CFBXLoader::getFrameData(), 
													m_AnimList, 
													m_fAnimFPS)))
	{
		delete pAnimData;
		pAnimData = nullptr;
	}
	return pAnimData;
}

//----------------------------------- AFactory_FBXModel_Instancing -------------------------------------------------------------------------------------

AFactory_FBXModel_Instancing::AFactory_FBXModel_Instancing(
	const std::string & rsFileName,
	const unsigned int nMaxNumberOfInstance,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform,
	const std::string & rsVSName,
	const std::string & rsPSName)
	: AFactory_FBXModel(rsFileName, rmGlobalTransform, rsVSName, rsPSName),
	m_nMaxNumberOfInstance(nMaxNumberOfInstance)
{
}

AFactory_FBXModel_Instancing::AFactory_FBXModel_Instancing(
	const CMyINISection & rSection,
	const unsigned int nMaxNumberOfInstance,
	const std::string & rsVSName, 
	const std::string & rsPSName)
	: AFactory_FBXModel(rSection, rsVSName, rsPSName),
	m_nMaxNumberOfInstance(nMaxNumberOfInstance)
{
}

std::vector<IMesh*> AFactory_FBXModel_Instancing::createMeshs()
{
	auto &rMeshs = CFBXLoader::getMeshs_Converted();

	std::vector<IMesh *> result;

	for (auto &data : rMeshs)
	{
		CFactory_FBXMesh_Dummy_Instancing factory(data, m_nMaxNumberOfInstance,m_sVSName);

		CMesh_MLRS_Instancing *pMesh = new CMesh_MLRS_Instancing;
		if (!pMesh->createObject(&factory))
		{
			delete pMesh;
			for (auto data2 : result)
				delete data2;
			return result;
		}
		result.push_back(pMesh);
	}
	return  result;
}

IGameModel * AFactory_FBXModel_Instancing::createModelOrigin()
{
	CGameModel_BoundingBox *pGameModel = new CGameModel_BoundingBox;
	if (!pGameModel->createObject(this))
	{
		delete pGameModel;
		pGameModel = nullptr;

	}

	return pGameModel;
}

//----------------------------------- CFactory_FBXModel_Instancing_NoAnim -------------------------------------------------------------------------------------

CFactory_FBXModel_Instancing_NoAnim::CFactory_FBXModel_Instancing_NoAnim(
	const std::string & rsFileName,
	const unsigned int nMaxNumberOfInstance,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform, 
	const std::string & rsVSName,
	const std::string & rsPSName)
	:AFactory_FBXModel_Instancing(rsFileName, nMaxNumberOfInstance, rmGlobalTransform, rsVSName, rsPSName)
{
}

CFactory_FBXModel_Instancing_NoAnim::CFactory_FBXModel_Instancing_NoAnim(
	const CMyINISection & rSection,
	const unsigned int nMaxNumberOfInstance, 
	const std::string & rsVSName, 
	const std::string & rsPSName)
	: AFactory_FBXModel_Instancing(rSection, nMaxNumberOfInstance, rsVSName, rsPSName)

{
}


//----------------------------------- CFactory_FBXModel_Dummy_Instancing -------------------------------------------------------------------------------------

CFactory_FBXModel_Instancing_Anim::CFactory_FBXModel_Instancing_Anim(
	const std::string & rsFileName, 
	const unsigned int nMaxNumberOfInstance,
	const StartPointList & rAnimList,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform,
	const float fAnimFPS,
	const std::string &rsVSName,
	const std::string &rsPSName)
	:AFactory_FBXModel_Instancing(rsFileName, nMaxNumberOfInstance, rmGlobalTransform, rsVSName, rsPSName),
	m_AnimList(rAnimList),
	m_fAnimFPS(fAnimFPS)
{
}

CFactory_FBXModel_Instancing_Anim::CFactory_FBXModel_Instancing_Anim(
	const CMyINISection & rSection,
	const unsigned int nMaxNumberOfInstance, 
	const StartPointList & rAnimList,
	const float fAnimFPS,
	const std::string &rsVSName ,
	const std::string &rsPSName )
	: AFactory_FBXModel_Instancing(rSection, nMaxNumberOfInstance, rsVSName, rsPSName),
	m_AnimList(rAnimList),
	m_fAnimFPS(fAnimFPS) 
{
}

IAnimData * CFactory_FBXModel_Instancing_Anim::createAnimData()
{
	//üũ
	auto &rData = CFBXLoader::getBoneStructure();
	const unsigned int nNumberOfBone = static_cast<unsigned int>(rData.size());
	std::vector<Type_Bone> bones(nNumberOfBone);
	for (unsigned int i = 0; i < nNumberOfBone; ++i)
	{
		bones[i].m_nParentIndex = rData[i].m_nParentIndex;

		DirectX::XMStoreFloat4x4(&bones[i].m_mTransform_ToLocal, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&rData[i].m_mTransform_ToLocal)));
	}

	CAnimData_Instancing * pAnimData = new CAnimData_Instancing();
	if (!pAnimData->createObject(&CFactory_AnimData_Instancing(m_sFileName,
		bones,
		CFBXLoader::getFrameData(),
		m_nMaxNumberOfInstance,
		m_AnimList,
		m_fAnimFPS)))
	{
		delete pAnimData;
		pAnimData = nullptr;
	}
	return pAnimData;
}


//----------------------------------- ABuilder_FBXModel -------------------------------------------------------------------------------------

ABuilder_FBXModel::ABuilder_FBXModel(AFactory_FBXModel * pFactory)
	:m_rFactory(*pFactory)
{
}

bool ABuilder_FBXModel::build(IGameModel * pModel)
{
	if (auto p = dynamic_cast<CGameModel_BoundingBox *>(pModel))
		return p->createObject(&m_rFactory);
	else
		return false;
}

IGameModel * ABuilder_FBXModel::build()
{
	auto p = new CGameModel_BoundingBox;
	if (!p->createObject(&m_rFactory))
	{
		delete p;
		p = nullptr;
	}
	return p;
}


//----------------------------------- CBuilder_FBXModel_Anim -------------------------------------------------------------------------------------

CBuilder_FBXModel_Anim::CBuilder_FBXModel_Anim(
	const std::string & rsFileName,
	const StartPointList & rAnimList,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform,
	const float fAnimFPS)
	: ABuilder_FBXModel(new CFactory_FBXModel(rsFileName, rAnimList, rmGlobalTransform, fAnimFPS))
{
}

CBuilder_FBXModel_Anim::CBuilder_FBXModel_Anim(
	const CMyINISection & rSection,
	const StartPointList & rAnimList,
	const float fAnimFPS)
	: ABuilder_FBXModel(new CFactory_FBXModel(rSection, rAnimList, fAnimFPS))
{
}

//----------------------------------- ABuilder_FBXModel_Instancing -------------------------------------------------------------------------------------

ABuilder_FBXModel_Instancing::ABuilder_FBXModel_Instancing(AFactory_FBXModel * pFactory)
	:ABuilder_FBXModel(pFactory)
{
}
 

bool ABuilder_FBXModel_Instancing::build(IGameModel *pModel)
{
	if (auto p = dynamic_cast<CGameModelDecorator_Instancing<Type_InstanceData_Mecha> *>(pModel))
		return p->createObject(dynamic_cast<IFactory_InstancingModel_Decorator *>(&m_rFactory));
	else
		return false;

}
IGameModel * ABuilder_FBXModel_Instancing::build()
{
	auto p = new CGameModelDecorator_Instancing<Type_InstanceData_Mecha>;
	if (!p->createObject(dynamic_cast<IFactory_InstancingModel_Decorator *>(&m_rFactory)))
	{
		delete p;
		p = nullptr;
	}
	return p;
}


//----------------------------------- CBuilder_FBXModel_Instancing_NoAnim -------------------------------------------------------------------------------------

CBuilder_FBXModel_Instancing_NoAnim::CBuilder_FBXModel_Instancing_NoAnim(
	const std::string & rsFileName,
	const unsigned int nMaxNumberOfInstance,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform)
	:ABuilder_FBXModel_Instancing(new CFactory_FBXModel_Instancing_NoAnim(rsFileName, nMaxNumberOfInstance, rmGlobalTransform))
{
}
CBuilder_FBXModel_Instancing_NoAnim::CBuilder_FBXModel_Instancing_NoAnim(
	const CMyINISection & rSection, 
	const unsigned int nMaxNumberOfInstance)
	: ABuilder_FBXModel_Instancing(new CFactory_FBXModel_Instancing_NoAnim(rSection, nMaxNumberOfInstance))
{
}
//

//----------------------------------- CBuilder_FBXModel_Instancing_Anim -------------------------------------------------------------------------------------

CBuilder_FBXModel_Instancing_Anim::CBuilder_FBXModel_Instancing_Anim(
	const std::string & rsFileName,
	const unsigned int nMaxNumberOfInstance, 
	const StartPointList & rAnimList,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform,
	const float fAnimFPS)
	: ABuilder_FBXModel_Instancing(new CFactory_FBXModel_Instancing_Anim(rsFileName, nMaxNumberOfInstance, rAnimList, rmGlobalTransform, fAnimFPS))
{
}

CBuilder_FBXModel_Instancing_Anim::CBuilder_FBXModel_Instancing_Anim(
	const CMyINISection & rSection, 
	const unsigned int nMaxNumberOfInstance,
	const StartPointList & rAnimList,
	const float fAnimFPS) 
	: ABuilder_FBXModel_Instancing(new CFactory_FBXModel_Instancing_Anim(rSection, nMaxNumberOfInstance, rAnimList, fAnimFPS))
{
}



//----------------------------------- CBuilder_FBXModelTester_Anim -------------------------------------------------------------------------------------

CBuilder_FBXModelTester_Anim::CBuilder_FBXModelTester_Anim(const std::string & rsFileName,
	DirectX::XMFLOAT3 &rvPos, const DirectX::XMFLOAT4X4 & rmGlobalTransform, const float fAnimFPS)
	: m_Builder(rsFileName, CBuilder_FBXModelTester_Anim::getStartPointList(), rmGlobalTransform, fAnimFPS),
	m_vPos(rvPos)
{
}

CBuilder_FBXModelTester_Anim::CBuilder_FBXModelTester_Anim(const CMyINISection & rSection,
	DirectX::XMFLOAT3 &rvPos, const float fAnimFPS)
	: m_Builder(rSection, CBuilder_FBXModelTester_Anim::getStartPointList(), fAnimFPS),
	m_vPos(rvPos)
{
}


bool CBuilder_FBXModelTester_Anim::build(IGameObject * pObject)
{
	if (auto *p = dynamic_cast<CModelTester *>(pObject))
		if (p->createObject(&CFactory_ModelTester(m_Builder.build())))
		{
			p->setPos(m_vPos);
			return true;
		}
	return false;
}

IGameObject * CBuilder_FBXModelTester_Anim::build()
{
	CModelTester *pResult = new CModelTester();
	if (!pResult->createObject(&CFactory_ModelTester( m_Builder.build())))
	{
		delete pResult;
		pResult = nullptr;
	}

	pResult->setPos(m_vPos);
	return pResult;
}

CBuilder_FBXModelTester_Anim::StartPointList CBuilder_FBXModelTester_Anim::getStartPointList()
{
	std::map<unsigned int, EActionType> result;
	result[0] = EActionType(0);
	return result;
}
