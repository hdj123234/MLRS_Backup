#include "stdafx.h"

#include "Dummy_Creator_FBXMesh.h"

#include"..\Module_Object_Instancing\Factory_AnimData_Instancing.h"
#include"..\Module_Object_GameModel\Factory_Material.h"
//#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"
//#include"..\Module_Object_GameModel\Factory_AnimData.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Type_InDirectX.h"
//#include"..\Module_Platform_DirectX11\DXUtility.h"
//#include"..\Module_Object\Msg_Movement.h"
//#include"..\Module_Object_Camera\Factory_Camera_ThirdPerson.h"
//#include"..\Module_Object\Factory_AnimController.h"
//#include"..\Module_MLRS_Object\Decorator_Player.h"
//#include"..\Module_MLRS_Object\Decorator_Enemy.h"

//#include"..\Module_Printer\DXRendererState.h"
//#include "..\Module_MLRS_Object\MainMecha_MLRS.h"
#include "..\MyINI.h"

using namespace DirectX;


//----------------------------------- CFactory_FBXMesh_Dummy -------------------------------------------------------------------------------------

CFactory_FBXMesh_Dummy::CFactory_FBXMesh_Dummy(
	const CMesh_Converted & rMesh,
	const std::string &rsVSName)
	: m_rMesh(rMesh),
	m_sVSName(rsVSName)
{
}

ID3D11VertexShader * CFactory_FBXMesh_Dummy::createVertexShader()
{
	return CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
}

ID3D11InputLayout * CFactory_FBXMesh_Dummy::createInputLayout()
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputElementDescs = { 
		{ "VERTEXINDEX", 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UVINDEX"	, 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "NORMAL"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "TANGENT"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } };
	

	return CFactory_InputLayout(m_sVSName, m_sVSName,
		"IL_FBXDummy"+m_rMesh.m_sName,
		inputElementDescs)
		.createInputLayout();
}

ID3D11Buffer * CFactory_FBXMesh_Dummy::createVertexBuffer()
{
	const std::vector<CIndex> &rIndices = m_rMesh.m_Indices;
	std::vector<CVertex_Mesh_MLRS> vertices;
	vertices.resize(rIndices.size());
	unsigned int nSize = static_cast<unsigned int>(rIndices.size());

	for (unsigned int i = 0; i < nSize; ++i)
	{
		vertices[i].m_nVertexIndex = rIndices[i].m_nVertexIndex;
		vertices[i].m_nUVIndex = rIndices[i].m_nUVIndex;
		vertices[i].m_vNormal = rIndices[i].m_vNormal;
		vertices[i].m_vTangent = rIndices[i].m_vTangent;
	}

	return CFactory_VertexBuffer<CVertex_Mesh_MLRS>("VB_FBXDummy" + m_rMesh.m_sName, nSize).createBuffer(vertices.data());
}
const unsigned int CFactory_FBXMesh_Dummy::getMaterialID()
{
	return 0;
}

std::vector<ID3D11ShaderResourceView*> CFactory_FBXMesh_Dummy::createTBuffers()
{
	const std::vector<CVertex_Converted> &rvPositions = m_rMesh.m_Vertices;

	ID3D11ShaderResourceView *pSRV_Pos = CFactory_SRV_BufferEX_FromData<CVertex_Converted>("FBXDummy_Position" + m_rMesh.m_sName,rvPositions)
		.createShaderResourceView();

	const std::vector<XMFLOAT2> &rvUVs = m_rMesh.m_UVs;

	ID3D11ShaderResourceView *pSRV_UVs = CFactory_SRV_Texture1D_FromData<XMFLOAT2>("FBXDummy_UV" + m_rMesh.m_sName,
		rvUVs,
		DXGI_FORMAT::DXGI_FORMAT_R32G32_FLOAT)
		.createShaderResourceView();
	if (!(pSRV_Pos&&pSRV_UVs)) return std::vector<ID3D11ShaderResourceView*>();

	return std::vector<ID3D11ShaderResourceView*>{pSRV_Pos, pSRV_UVs};
}

std::vector<ID3D11Buffer*> CFactory_FBXMesh_Dummy::createConstBuffer_Transform()
{
	ID3D11Buffer *pBuffer_World = factory_WorldBuffer.createBuffer();
	ID3D11Buffer *pBuffer_View = factory_ViewBuffer.createBuffer();
	ID3D11Buffer *pBuffer_Projection = factory_ProjectionBuffer.createBuffer();

	if(!(pBuffer_World&&pBuffer_View&&pBuffer_Projection))	return std::vector<ID3D11Buffer*>();

	return std::vector<ID3D11Buffer*>{pBuffer_World, pBuffer_View, pBuffer_Projection};
}


unsigned int CFactory_FBXMesh_Dummy::getNumberOfVertex()
{
	return static_cast<unsigned int>(m_rMesh.m_Indices.size());
}


//----------------------------------- CFactory_FBXMesh_Dummy_Instancing -------------------------------------------------------------------------------------

CFactory_FBXMesh_Dummy_Instancing::CFactory_FBXMesh_Dummy_Instancing(
	const CMesh_Converted & rMesh ,
	const unsigned int nMaxNumberOfInstance,
	const std::string &rsVSName)
	:m_rMesh(rMesh) ,
	m_nMaxNumberOfInstance(nMaxNumberOfInstance),
	m_sVSName(rsVSName)
{
}

ID3D11VertexShader * CFactory_FBXMesh_Dummy_Instancing::createVertexShader()
{
	return CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
}

ID3D11InputLayout * CFactory_FBXMesh_Dummy_Instancing::createInputLayout()
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputElementDescs = {
		{ "VERTEXINDEX", 0, DXGI_FORMAT_R32_UINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UVINDEX"	, 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "NORMAL"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "TANGENT"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "WORLDMATRIX"	, 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WORLDMATRIX"	, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 } ,
		{ "WORLDMATRIX"	, 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 } ,
		{ "WORLDMATRIX"	, 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 } };
	
	
	return CFactory_InputLayout("vsDummy_FBX_Instancing", "vsDummy_FBX_Instancing",
		"IL_FBXDummy_Instancing" + m_rMesh.m_sName,
		inputElementDescs)
		.createInputLayout();
}

ID3D11Buffer * CFactory_FBXMesh_Dummy_Instancing::createVertexBuffer()
{
	const std::vector<CIndex> &rIndices = m_rMesh.m_Indices;
	std::vector<CVertex_Mesh_MLRS> vertices;
	vertices.resize(rIndices.size());
	unsigned int nSize = static_cast<unsigned int>(rIndices.size());

	for (unsigned int i = 0; i < nSize; ++i)
	{
		vertices[i].m_nVertexIndex = rIndices[i].m_nVertexIndex;
		vertices[i].m_nUVIndex = rIndices[i].m_nUVIndex;
		vertices[i].m_vNormal = rIndices[i].m_vNormal;
		vertices[i].m_vTangent = rIndices[i].m_vTangent;
	}

	return CFactory_VertexBuffer<CVertex_Mesh_MLRS>("VB_FBXDummy" + m_rMesh.m_sName, nSize).createBuffer(vertices.data());
}

unsigned int CFactory_FBXMesh_Dummy_Instancing::getNumberOfVertex()
{
	return static_cast<unsigned int>(m_rMesh.m_Indices.size());
}

const unsigned int CFactory_FBXMesh_Dummy_Instancing::getMaterialID()
{
	return 0;
}

std::vector<ID3D11ShaderResourceView*> CFactory_FBXMesh_Dummy_Instancing::createTBuffers()
{
	const std::vector<CVertex_Converted> &rvPositions = m_rMesh.m_Vertices;
	
	ID3D11ShaderResourceView *pSRV_Pos = CFactory_SRV_BufferEX_FromData<CVertex_Converted>("FBXDummy_Position" + m_rMesh.m_sName, rvPositions)
		.createShaderResourceView();
	
	const std::vector<XMFLOAT2> &rvUVs = m_rMesh.m_UVs;
	
	ID3D11ShaderResourceView *pSRV_UVs = CFactory_SRV_Texture1D_FromData<XMFLOAT2>("FBXDummy_UV" + m_rMesh.m_sName,
		rvUVs,
		DXGI_FORMAT::DXGI_FORMAT_R32G32_FLOAT)
		.createShaderResourceView();
	if (!(pSRV_Pos&&pSRV_UVs)) return std::vector<ID3D11ShaderResourceView*>();
	
	return std::vector<ID3D11ShaderResourceView*>{pSRV_Pos, pSRV_UVs};
}

std::vector<ID3D11Buffer*> CFactory_FBXMesh_Dummy_Instancing::createConstBuffer_Transform()
{
	ID3D11Buffer *pBuffer_View = factory_ViewBuffer.createBuffer();
	ID3D11Buffer *pBuffer_Projection = factory_ProjectionBuffer.createBuffer();
	
	if (!(pBuffer_View&&pBuffer_Projection))	return std::vector<ID3D11Buffer*>();
	
	return std::vector<ID3D11Buffer*>{pBuffer_View, pBuffer_Projection};
}
ID3D11Buffer * CFactory_FBXMesh_Dummy_Instancing::createInstanceBuffer()
{
	return CFactory_InstanceBuffer<Type_InstanceData_Mecha>("VB(Instance)_FBXDummy" + m_rMesh.m_sName, m_nMaxNumberOfInstance).createBuffer();
}
//