#ifndef HEADER_DUMMY_CREATOR_FBXMODEL
#define HEADER_DUMMY_CREATOR_FBXMODEL

#include"Dummy_Creator_FBXMesh.h"

#include"..\Module_MLRS_Object\Mesh_MLRS.h"

#include "..\Module_Object_Instancing\ModelDecorator_Instancing.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"

#include "..\Module_Object\Interface_Object.h"
//#include "..\Module_Object_Common\Bone.h"
//#include "Model_FBX_Dummy.h"
//#include"..\Module_MLRS_Object\Mesh_MLRS.h"

#include"..\..\FBXConverter\FBXLoader.h"

//전방선언
struct Type_Bone;
class CMyINISection;
enum EActionType : ENUMTYPE_256;


class AFactory_FBXModel : virtual public IFactory_GameModel_BoundingBox,
										protected CFBXLoader {
protected:
	typedef std::map<unsigned int, EActionType> StartPointList;


protected:
	const std::string m_sFileName;
	const std::string m_sVSName;
	const std::string m_sPSName;

public:
	AFactory_FBXModel(const std::string &rsFileName,
		const DirectX::XMFLOAT4X4 &rmGlobalTransform ,
		const std::string &rsVSName ,
		const std::string &rsPSName );
	AFactory_FBXModel(const CMyINISection &rSection,
		const std::string &rsVSName ,
		const std::string &rsPSName );
	virtual ~AFactory_FBXModel()=0 {}

	//override IFactory_GameModel
	virtual std::vector<IMesh			*> createMeshs();
	virtual std::vector<IMaterial		*> createMaterials();
	virtual IAnimData	* createAnimData() { return nullptr; }

	//override IFactory_GameModel_BoundingBox
	virtual const DirectX::XMFLOAT3 getCenter() { return CFBXLoader::getCenter(); }
	virtual const DirectX::XMFLOAT3 getExtent() { return CFBXLoader::getExtent(); }

	const unsigned int getNumberOfBone() { return static_cast<const unsigned int>(CFBXLoader::getBoneStructure().size()); }
	const unsigned int getNumberOfFrameData() { return static_cast<const unsigned int>(CFBXLoader::getFrameData()[0].size()); }

	std::vector<CBone> &getBoneStructure() { return CFBXLoader::getBoneStructure(); }
	std::vector<std::vector<DirectX::XMFLOAT4X4>> &getFrameData() { return CFBXLoader::getFrameData(); }
};


class CFactory_FBXModel_NoAnim : public AFactory_FBXModel {
public:
	CFactory_FBXModel_NoAnim(	const std::string &rsFileName,
								const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
								const std::string &rsVSName = "vsDummy_FBX",
								const std::string &rsPSName = "psDummy_FBX");
	CFactory_FBXModel_NoAnim(	const CMyINISection &rSection,
								const std::string &rsVSName = "vsDummy_FBX",
								const std::string &rsPSName = "psDummy_FBX");
	virtual ~CFactory_FBXModel_NoAnim() = 0 {}
};

class CFactory_FBXModel :	public AFactory_FBXModel {
protected:
	typedef std::map<unsigned int, EActionType> StartPointList;

private:
	const StartPointList m_AnimList;
	const float m_fAnimFPS;

public:
	CFactory_FBXModel(const std::string &rsFileName,
							const StartPointList &rAnimList,
							const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
							const float fAnimFPS = 60.0f,
							const std::string &rsVSName = "vsDummy_FBX",
							const std::string &rsPSName = "psDummy_FBX");
	CFactory_FBXModel(const CMyINISection &rSection,
							const StartPointList &rAnimList,
							const float fAnimFPS = 60.0f,
							const std::string &rsVSName = "vsDummy_FBX",
							const std::string &rsPSName = "psDummy_FBX");
	virtual ~CFactory_FBXModel() {}

	//override IFactory_GameModel
	virtual IAnimData	* createAnimData() ;
};



//----------------------------------- 인스턴싱용 -------------------------------------------------------------------------------------

class AFactory_FBXModel_Instancing : virtual public IFactory_InstancingModel_Decorator,
									 public AFactory_FBXModel {
protected:
	const unsigned int m_nMaxNumberOfInstance;

public:
	AFactory_FBXModel_Instancing(	const std::string &rsFileName,
									const unsigned int nMaxNumberOfInstance , 
									const DirectX::XMFLOAT4X4 &rmGlobalTransform,
									const std::string &rsVSName,
									const std::string &rsPSName);
	AFactory_FBXModel_Instancing(	const CMyINISection &rSection,
									const unsigned int nMaxNumberOfInstance , 
									const std::string &rsVSName,
									const std::string &rsPSName);
	virtual ~AFactory_FBXModel_Instancing()=0 {  }

	virtual std::vector<IMesh			*> createMeshs();

	virtual IGameModel * createModelOrigin() ;

	std::vector<CBone> &getBoneStructure() { return CFBXLoader::getBoneStructure(); }
	std::vector<std::vector<DirectX::XMFLOAT4X4>> &getFrameData() { return CFBXLoader::getFrameData(); }

};


class CFactory_FBXModel_Instancing_NoAnim : public AFactory_FBXModel_Instancing{
public:
	CFactory_FBXModel_Instancing_NoAnim(const std::string &rsFileName,
		const unsigned int nMaxNumberOfInstance,
		const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
		const std::string &rsVSName="vsDummy_FBX_Instancing_NoAnim",
		const std::string &rsPSName="psDummy_FBX");
	CFactory_FBXModel_Instancing_NoAnim(const CMyINISection &rSection,
		const unsigned int nMaxNumberOfInstance,
		const std::string &rsVSName="vsDummy_FBX_Instancing_NoAnim",
		const std::string &rsPSName="psDummy_FBX");
	virtual ~CFactory_FBXModel_Instancing_NoAnim() {}

};


class CFactory_FBXModel_Instancing_Anim :	public AFactory_FBXModel_Instancing{
private:
	typedef std::map<unsigned int, EActionType> StartPointList;

private:
	const StartPointList m_AnimList;
	const float m_fAnimFPS;

public:
	CFactory_FBXModel_Instancing_Anim(const std::string &rsFileName,
		const unsigned int nMaxNumberOfInstance,
		const StartPointList &rAnimList,
		const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
		const float fAnimFPS = 60.0f,
		const std::string &rsVSName = "vsDummy_FBX_Instancing",
		const std::string &rsPSName = "psDummy_FBX");
	CFactory_FBXModel_Instancing_Anim(const CMyINISection &rSection,
		const unsigned int nMaxNumberOfInstance,
		const StartPointList &rAnimList,
		const float fAnimFPS = 60.0f,
		const std::string &rsVSName = "vsDummy_FBX_Instancing",
		const std::string &rsPSName = "psDummy_FBX");
	virtual ~CFactory_FBXModel_Instancing_Anim() {}

	//override IFactory_GameModel
	virtual IAnimData	* createAnimData();

	//override IFactory_InstancingModel_Decorator_Anim
	virtual IGameModel * createModelOrigin() { return AFactory_FBXModel_Instancing::createModelOrigin(); }
};

//----------------------------------- Builder -------------------------------------------------------------------------------------

class ABuilder_FBXModel : virtual public IBuilder<IGameModel>{
protected:
	AFactory_FBXModel & m_rFactory;

public:
	ABuilder_FBXModel(AFactory_FBXModel * pFactory);
	virtual ~ABuilder_FBXModel() = 0 {delete &m_rFactory;}

	//override IBuilder<IGameModel>
	virtual bool build(IGameModel *pModel);
	virtual IGameModel * build();

	const unsigned int getNumberOfBone() { return m_rFactory.getNumberOfBone(); }
	const unsigned int getNumberOfFrameData() { return m_rFactory.getNumberOfFrameData(); }

	std::vector<CBone> &getBoneStructure() { return m_rFactory.getBoneStructure(); }
	std::vector<std::vector<DirectX::XMFLOAT4X4>> &getFrameData() { return m_rFactory.getFrameData(); }
};

class CBuilder_FBXModel_Anim : public ABuilder_FBXModel {
private:
	typedef std::map<unsigned int, EActionType> StartPointList;

public:
	CBuilder_FBXModel_Anim(const std::string &rsFileName,
		const StartPointList &rAnimList,
		const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
		const float fAnimFPS = 60.0f);
	CBuilder_FBXModel_Anim(const CMyINISection &rSection,
		const StartPointList &rAnimList,
		const float fAnimFPS = 60.0f);
	virtual ~CBuilder_FBXModel_Anim() {  }
};

class ABuilder_FBXModel_Instancing  : public ABuilder_FBXModel {
public:
	ABuilder_FBXModel_Instancing(AFactory_FBXModel * pFactory);
	virtual ~ABuilder_FBXModel_Instancing() { }

	//override IBuilder<IGameModel>
	virtual bool build(IGameModel *pModel);
	virtual IGameModel * build();
};
class CBuilder_FBXModel_Instancing_NoAnim : public ABuilder_FBXModel_Instancing {
public:
	CBuilder_FBXModel_Instancing_NoAnim(const std::string &rsFileName,
		const unsigned int nMaxNumberOfInstance,
		const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1));
	CBuilder_FBXModel_Instancing_NoAnim(const CMyINISection &rSection,
		const unsigned int nMaxNumberOfInstance);
	virtual ~CBuilder_FBXModel_Instancing_NoAnim(){ }

};

class CBuilder_FBXModel_Instancing_Anim : public ABuilder_FBXModel_Instancing {
private:
	typedef std::map<unsigned int, EActionType> StartPointList;
public:
	CBuilder_FBXModel_Instancing_Anim(const std::string &rsFileName,
		const unsigned int nMaxNumberOfInstance,
		const StartPointList &rAnimList,
		const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
		const float fAnimFPS = 60.0f);
	CBuilder_FBXModel_Instancing_Anim(const CMyINISection &rSection,
		const unsigned int nMaxNumberOfInstance,
		const StartPointList &rAnimList,
		const float fAnimFPS = 60.0f);
	virtual ~CBuilder_FBXModel_Instancing_Anim() { }

};

class CBuilder_FBXModelTester_Anim : virtual public IBuilder<IGameObject> {
private:
	typedef std::map<unsigned int, EActionType> StartPointList;
	CBuilder_FBXModel_Anim m_Builder;
	DirectX::XMFLOAT3 m_vPos;
public:
	CBuilder_FBXModelTester_Anim(const std::string &rsFileName,
		DirectX::XMFLOAT3 &rvPos = DirectX::XMFLOAT3(0,0,0),
		const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
		const float fAnimFPS = 60.0f);
	CBuilder_FBXModelTester_Anim(const CMyINISection &rSection,
		DirectX::XMFLOAT3 &rvPos = DirectX::XMFLOAT3(0, 0, 0),
		const float fAnimFPS = 60.0f);
	virtual ~CBuilder_FBXModelTester_Anim() { }

	//override IBuilder<IGameModel>
	virtual bool build(IGameObject *pModel);
	virtual IGameObject * build();

	static StartPointList getStartPointList();
};


#endif