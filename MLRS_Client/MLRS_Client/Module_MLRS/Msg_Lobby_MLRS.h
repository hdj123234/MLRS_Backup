#pragma once
#ifndef HEADER_MSG_LOBBY_MLRS
#define HEADER_MSG_LOBBY_MLRS


#include "Msg_InGame_MLRS.h"
#include "..\Module_Object\Interface_Object.h"
#include "..\Module_Object\Msg_Movement.h"
#include "..\Module_Sound\Msg_Sound.h"
#include "..\Module_Object\Proxy.h"
 

enum class EMsgType_Lobby_MLRS : ENUMTYPE_256 {
	eClick,
	eDEBUG_InGame,
//	eSetState,
};

class CMsg_Lobby_MLRS : public CMsg_MLRS {
private:
	EMsgType_Lobby_MLRS m_eType;
public:
	CMsg_Lobby_MLRS(EMsgType_Lobby_MLRS eType):CMsg_MLRS(EMsgType_MLRS::eLobby),m_eType(eType){}
	virtual ~CMsg_Lobby_MLRS() {}

	const EMsgType_Lobby_MLRS getMsgType()const { return m_eType; }
};


class CMsg_Lobby_MLRS_Click : public CMsg_Lobby_MLRS {
private:
	unsigned int m_nPosX;
	unsigned int m_nPosY;
public:
	CMsg_Lobby_MLRS_Click(const unsigned int nPosX, const unsigned int nPosY)
		:CMsg_Lobby_MLRS(EMsgType_Lobby_MLRS::eClick),
		m_nPosX(nPosX), m_nPosY(nPosY)
	{}
	virtual ~CMsg_Lobby_MLRS_Click() {} 

	const unsigned int getPosX()const { return m_nPosX; }
	const unsigned int getPosY()const { return m_nPosY; }
};


 
#endif