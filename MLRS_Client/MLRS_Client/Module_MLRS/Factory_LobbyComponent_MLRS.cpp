#include"stdafx.h"
#include "Factory_LobbyComponent_MLRS.h"

#include "GlobalVSInputMap.h"
#include"..\MyUtility.h"
#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"


//----------------------------------- CFactory_LobbyComponent -------------------------------------------------------------------------------------

const std::string CFactory_LobbyComponent::s_sVSName = "vsPosAndSizeXY";
const std::string CFactory_LobbyComponent::s_sGSName = "gsUI_ScreenBase_Size_LT";
const std::string CFactory_LobbyComponent::s_sPSName = "psTexOnly";
 

CFactory_LobbyComponent::CFactory_LobbyComponent(
	const std::string & rsName, 
	const DirectX::XMFLOAT2 & rvPos, 
	const DirectX::XMFLOAT2 & rvSize, 
	const std::string & rsFileName_DiffuseMap)
	:m_sName(rsName),
	m_Factory_Material(rsName+"_Material_"+ rsFileName_DiffuseMap, s_sPSName, rsFileName_DiffuseMap),
	m_VertexData(rvPos,rvSize)
{
	m_VertexData.m_vPos.x *= 2;
	m_VertexData.m_vPos.x -= 1;
	m_VertexData.m_vPos.y = 1 - m_VertexData.m_vPos.y;
	m_VertexData.m_vPos.y *= 2;
	m_VertexData.m_vPos.y -= 1;
	m_VertexData.m_vSize.x *= 2;
	m_VertexData.m_vSize.y *= 2;
}

IGameModel * CFactory_LobbyComponent::getModel()
{
	CGameModel *pRetval = new CGameModel;

	if (!pRetval->createObject(this))
	{
		delete pRetval;
		return nullptr;
	}

	return pRetval;
}

std::vector<IMesh*> CFactory_LobbyComponent::createMeshs()
{
	CMesh_VertexAndGS * pMesh = new CMesh_VertexAndGS;
	if (!pMesh->createObject(this))
	{
		delete pMesh;
		return std::vector<IMesh*>();
	}
	return std::vector<IMesh*>({ pMesh });
}

std::vector<IMaterial*> CFactory_LobbyComponent::createMaterials()
{
	CMaterial_NoneLighting_Diffuse *pMaterial = new CMaterial_NoneLighting_Diffuse;
	if (!pMaterial->createObject(&m_Factory_Material))
	{
		delete pMaterial;
		return std::vector<IMaterial*>();

	}
	return std::vector<IMaterial*>({ pMaterial });
}

ID3D11InputLayout * CFactory_LobbyComponent::createInputLayout()
{
	auto inputElementDescs = CGlobalVSInputMap::getInputElementDesc(s_sVSName);
	
	return 	CFactory_InputLayout(s_sVSName, s_sVSName,"IL_"+ s_sVSName,inputElementDescs).createInputLayout();
}

ID3D11Buffer * CFactory_LobbyComponent::createVertexBuffer()
{
	return CFactory_VertexBuffer<VertexData_PosAndSize>(m_sName,1).createBuffer(m_VertexData);
}

ID3D11GeometryShader * CFactory_LobbyComponent::createGeometryShader()
{
	return CFactory_GeometryShader(s_sGSName, s_sGSName).createShader();
}

ID3D11VertexShader * CFactory_LobbyComponent::createVertexShader()
{
	return CFactory_VertexShader(s_sVSName, s_sVSName).createShader();
}


//----------------------------------- CFactory_LobbyComponent_Button -------------------------------------------------------------------------------------

CFactory_LobbyComponent_Button::CFactory_LobbyComponent_Button(const std::string & rsName, const DirectX::XMFLOAT2 & rvPos, const DirectX::XMFLOAT2 & rvSize, const std::string & rsFileName_DiffuseMap)
	:CFactory_LobbyComponent(rsName, rvPos, rvSize, rsFileName_DiffuseMap) 
{
}

std::vector<CLobbyComponent_Drawable*> CFactory_LobbyComponent_Button::getComponents()
{
	CLobbyComponent_Drawable *pComponent = new CLobbyComponent_Drawable;
	if (!pComponent->createObject(this))
	{
		delete pComponent;
		return std::vector<CLobbyComponent_Drawable*>();

	}
	return std::vector<CLobbyComponent_Drawable*>({ pComponent });
}

RECT CFactory_LobbyComponent_Button::getRect()
{
	RECT retval;
	auto w = CGlobalWindow::getWidth();
	auto h = CGlobalWindow::getHeight();
	auto tmp = m_VertexData;

	tmp.m_vPos.x += 1;
	tmp.m_vPos.x /= 2;
	tmp.m_vPos.y += 1;
	tmp.m_vPos.y /= 2;
	tmp.m_vPos.y = 1 - m_VertexData.m_vPos.y;
	tmp.m_vSize.x /= 2;
	tmp.m_vSize.y /= 2;

	retval.left = static_cast<LONG>((tmp.m_vPos.x)*w);
	retval.top = static_cast<LONG>((tmp.m_vPos.y)*h);
	retval.right = static_cast<LONG>((tmp.m_vPos.x + tmp.m_vSize.x)*w);
	retval.bottom = static_cast<LONG>((tmp.m_vPos.y + tmp.m_vSize.y)*h);

	return retval;
}

//----------------------------------- CFactory_LobbyComponent_Button_OnOff -------------------------------------------------------------------------------------


CFactory_LobbyComponent_Button_OnOff::CFactory_LobbyComponent_Button_OnOff(const std::string & rsName, const DirectX::XMFLOAT2 & rvPos, const DirectX::XMFLOAT2 & rvSize, const std::string & rsFileName_DiffuseMap_Off, const std::string & rsFileName_DiffuseMap_On)
	:m_Factory1(rsName, rvPos, rvSize, rsFileName_DiffuseMap_Off),
	m_Factory2(rsName, rvPos, rvSize, rsFileName_DiffuseMap_On),
	m_vPos (rvPos),
	m_vSize(rvSize) 
{
}

std::vector<CLobbyComponent_Drawable*> CFactory_LobbyComponent_Button_OnOff::getComponents()
{
	CLobbyComponent_Drawable *pComponent1 = new CLobbyComponent_Drawable;
	if (!pComponent1->createObject(&m_Factory1))
	{
		delete pComponent1;
		return std::vector<CLobbyComponent_Drawable*>();
	}
	CLobbyComponent_Drawable *pComponent2 = new CLobbyComponent_Drawable;
	if (!pComponent2->createObject(&m_Factory2))
	{
		delete pComponent1;
		delete pComponent2;
		return std::vector<CLobbyComponent_Drawable*>();
	}
	return std::vector<CLobbyComponent_Drawable*>({pComponent1, pComponent2});
}

RECT CFactory_LobbyComponent_Button_OnOff::getRect()
{
	RECT retval;
	auto w = CGlobalWindow::getWidth();
	auto h = CGlobalWindow::getHeight(); 

	retval.left		= static_cast<LONG>((m_vPos.x)*w);
	retval.top		= static_cast<LONG>((m_vPos.y)*h);
	retval.right	= static_cast<LONG>((m_vPos.x + m_vSize.x)*w);
	retval.bottom	= static_cast<LONG>((m_vPos.y + m_vSize.y)*h);

	return retval;
}

//----------------------------------- CFactory_LobbyComponent_List -------------------------------------------------------------------------------------

CFactory_LobbyComponent_List::CFactory_LobbyComponent_List(
	const std::string & rsName, 
	const DirectX::XMFLOAT2 & rvPos, 
	const DirectX::XMFLOAT2 & rvSize,
	const std::string & rsFileName_DiffuseMap,
	const unsigned int nNumberOfList, 
	const float fGap)
	:CFactory_LobbyComponent(rsName, rvPos, rvSize, rsFileName_DiffuseMap),
	m_nNumberOfList(nNumberOfList),
	m_fGap(fGap*2.0f)
{
}

ID3D11Buffer * CFactory_LobbyComponent_List::createVertexBuffer()
{
	std::vector<VertexData_PosAndSize> vertexDatas;
	VertexData_PosAndSize vertexData(m_VertexData);
	float fGap = m_fGap + m_VertexData.m_vSize.y;
	for (auto i = 0U; i < m_nNumberOfList; ++i)
	{
		VertexData_PosAndSize vertexData(m_VertexData);
		vertexData.m_vPos.y -= fGap*static_cast<float>(i);
		vertexDatas.push_back(vertexData);

	}


	return CFactory_VertexBuffer<VertexData_PosAndSize>(m_sName, m_nNumberOfList).createBuffer(vertexDatas.data());
}

std::vector<RECT> CFactory_LobbyComponent_List::getRects()
{
	std::vector<RECT> retval;
	RECT rect;
	auto w = CGlobalWindow::getWidth();
	auto h = CGlobalWindow::getHeight();
	auto tmp = m_VertexData;

	tmp.m_vPos.x += 1;
	tmp.m_vPos.x /= 2;
	tmp.m_vPos.y += 1;
	tmp.m_vPos.y /= 2;
	tmp.m_vPos.y = 1 - m_VertexData.m_vPos.y;
	tmp.m_vSize.x /= 2;
	tmp.m_vSize.y /= 2;

	float fGap = m_fGap/2.0f + tmp.m_vSize.y;
	for (auto i = 0U; i < m_nNumberOfList; ++i)
	{

		rect.left = static_cast<LONG>((tmp.m_vPos.x)*w);
		rect.top = static_cast<LONG>((tmp.m_vPos.y)*h);
		rect.right = static_cast<LONG>((tmp.m_vPos.x + tmp.m_vSize.x)*w);
		rect.bottom = static_cast<LONG>((tmp.m_vPos.y + tmp.m_vSize.y)*h);
		retval.push_back(rect);
		tmp.m_vPos.y += fGap;
	}
	return retval;
}
