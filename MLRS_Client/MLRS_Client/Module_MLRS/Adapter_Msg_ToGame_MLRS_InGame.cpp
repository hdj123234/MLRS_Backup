#include"stdafx.h"
#include "Adapter_Msg_ToGame_MLRS_InGame.h"

#include "Msg_InGame_MLRS.h"

#include"..\Module_Platform_Windows\Platform_Window.h"
#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include"..\Module_MLRS_Object\State_MainMecha_MLRS.h"

#ifndef HEADER_WINDOWS
#include <windowsx.h>
#endif

using namespace DirectX;

CAdapter_Msg_ToGame_MLRS_InGame::CAdapter_Msg_ToGame_MLRS_InGame()
{
}


CAdapter_Msg_ToGame_MLRS_InGame::~CAdapter_Msg_ToGame_MLRS_InGame()
{
}

IMsg_GameObject * CAdapter_Msg_ToGame_MLRS_InGame::adapt(IMsg * pMsg)
{
	IMsg_GameObject *pResult = nullptr;
	if (CMsg_Windows *pWinMsg = dynamic_cast<CMsg_Windows *>(pMsg))
	{
		float x, y;
		switch (UINT msgType = pWinMsg->getMessage())		{
		case WM_LBUTTONDOWN:
			pResult = createFlagMsg(pWinMsg,true,true);
			delete pMsg;
			break;
		case WM_LBUTTONUP:
			pResult = createFlagMsg(pWinMsg, false, true);
			delete pMsg;
			break;
		case WM_KEYDOWN:
			{
				WPARAM wParam = pWinMsg->getWParam();
				if (wParam == 'w' || wParam == 'W' ||
					wParam == 'a' || wParam == 'A' ||
					wParam == 's' || wParam == 'S' ||
					wParam == 'd' || wParam == 'D' ||
					wParam == VK_SPACE || 
					wParam == VK_SHIFT)
				{
					pResult = createFlagMsg(pWinMsg, true);
					delete pMsg;
				}
				//else if (wParam == '0')
				//{
				//	pResult = new CMsg_MLRS_Action(EAction_MLRS::eAction_DefaultMode);
				//	delete pMsg;
				//}
				else if (wParam == '1')
				{	
					pResult = new CMsg_MLRS_Action(EAction_MLRS::eAction_Weapon1);
					delete pMsg;
				}
				else if (wParam == '2')
				{
					pResult = new CMsg_MLRS_Action(EAction_MLRS::eAction_Weapon2 );
					delete pMsg;
				}
				else if (wParam == '3')
				{
					pResult = new CMsg_MLRS_Action(EAction_MLRS::eAction_Weapon3);
					delete pMsg;
				}
				else if (wParam == '9')
				{	
					pResult = new CMsg_MLRS_Action(EAction_MLRS::eAction_DebugCamera);	//디버그
					delete pMsg;
				}
				else if (wParam == VK_F1)
				{
					pResult = new CMsg_MLRS_DEBUG(eDebug_SwapBoundingBox);	//디버그
					delete pMsg;
				}
				else if (wParam == VK_F3)
				{
					pResult = new CMsg_MLRS_DEBUG(eDebug_SwapTerrainWire);	//디버그
					delete pMsg;
				}
				break;
			}
		case WM_KEYUP:
		{

			WPARAM wParam = pWinMsg->getWParam();
			if (wParam == 'w' || wParam == 'W' ||
				wParam == 'a' || wParam == 'A' ||
				wParam == 's' || wParam == 'S' ||
				wParam == 'd' || wParam == 'D' ||
				wParam == VK_SPACE ||
				wParam == VK_SHIFT)
			{
				pResult = createFlagMsg(pWinMsg, false);
				delete pMsg;
			}
			break;
		}
		
		case WM_MOUSEMOVE:
			if (CMsgWindows_MouseMove * pMsg_Mouse = dynamic_cast<CMsgWindows_MouseMove *>(pWinMsg))
			{
				x = static_cast<float>(pMsg_Mouse->getX());
				y = static_cast<float>(pMsg_Mouse->getY());
//				if (abs(x) > abs(y))
					pResult = new CMsg_MLRS_CtlCamera_Rotate( y, x);			
//				else
//					pResult = new CMsg_Movement_Vector2(EMsgType_Movement::Rotate_PitchYaw, y, 0);

					delete pMsg;
			}
			break;

		case WM_MOUSEWHEEL:
		{
			const float f = static_cast<float>(GET_WHEEL_DELTA_WPARAM(pWinMsg->getWParam())/ WHEEL_DELTA);
			pResult = new CMsg_MLRS_CtlCamera_Distance(f);
			delete pMsg;
			break;
		}
		}
	}
	else if (CMsg_SC *pNetMsg = dynamic_cast<CMsg_SC *>(pMsg))
		pResult = pNetMsg;
	else
		pResult= nullptr;
	return pResult;
}


IMsg_GameObject * CAdapter_Msg_ToGame_MLRS_InGame::createFlagMsg(CMsg_Windows * pMsg, bool bOn, bool bAttack)
{
	FLAG_8 flag;
	UINT msgType = pMsg->getMessage();
	if (bAttack)
		flag = FLAG_KEY::FLAG_ATTACK;
	else
	{
		switch (pMsg->getWParam())
		{
		case 'w':
		case 'W':
			flag = FLAG_MOVEMENT_FRONT;
			break;
		case 'a':
		case 'A':
			flag = FLAG_MOVEMENT_LEFT;
			break;
		case 's':
		case 'S':
			flag = FLAG_MOVEMENT_BACK;
			break;
		case 'd':
		case 'D':
			flag = FLAG_MOVEMENT_RIGHT;
			break;
		case VK_SPACE:
			flag = FLAG_JUMP;
			break;
		case VK_SHIFT:
			flag = FLAG_BOOST;
			break;
		default:
			return nullptr;
		}
	}
	return  new CMsg_MLRS_KeyFlag(bOn, flag);
}

IMsg_GameObject * CAdapter_Msg_ToGame_MLRS_InGame::createSetTypeMsg(CMsg_Windows * pMsg)
{
	return nullptr;
}
