#include"stdafx.h"
#include "Adapter_Msg_ToGame_MLRS_Lobby.h"

#include "Msg_Lobby_MLRS.h"

#include"..\Module_Platform_Windows\Platform_Window.h"
#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include"..\Module_MLRS_Object\State_MainMecha_MLRS.h"

#ifndef HEADER_WINDOWS
#include <windowsx.h>
#endif

using namespace DirectX;

IMsg_GameObject * CAdapter_Msg_ToGame_MLRS_Lobby::adapt(IMsg * pMsg)
{
	IMsg_GameObject *pResult = nullptr;
	if (CMsg_Windows *pWinMsg = dynamic_cast<CMsg_Windows *>(pMsg))
	{
		float x, y;
		switch (UINT msgType = pWinMsg->getMessage())		{
		case WM_LBUTTONDOWN:
		{ 
			auto lParam = pWinMsg->getLParam();
			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);
			pResult = new CMsg_Lobby_MLRS_Click(x,y);
			delete pMsg;
			break; 
		}
 
		case WM_MOUSEMOVE: 
			break;

		case WM_KEYDOWN: 
			if (pWinMsg->getWParam() == 'a'|| pWinMsg->getWParam() == 'A' )
				pResult = new CMsg_Lobby_MLRS(EMsgType_Lobby_MLRS::eDEBUG_InGame);

		}
	}
	else if (CMsg_SC *pNetMsg = dynamic_cast<CMsg_SC *>(pMsg))
		pResult = pNetMsg;
	else
		pResult= nullptr;
	return pResult;
}
