
#ifndef HEADER_INITIALIZER_MLRS
#define HEADER_INITIALIZER_MLRS

/*	CInitializer_MLRS 
	초기화가 필요한 라이브러리에 대해 초기화 및 해제 작업을 실행하는 클래스
	생성자에서 초기화 및 전역 객체 생성
	소멸자에서 생성한 객체 제거 및 초기화 해제 
*/

#include"..\Module_Platform_DirectX11\Builder_DirectXDevice.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"
#include"..\Module_Sound\GlobalXAudio2Engine.h"
#include"..\Module_Sound\XAudio2Engine.h"



class CInitializer_MLRS {
private:
	CWindow *m_pWindow;
	CDirectXDevice *m_pDevice;
	CXAudio2Engine *m_pXAudio2;
public:  
	CInitializer_MLRS(	HINSTANCE hInst, 
						CFactory_Window *pFactory_Windows,
						CBuilder_DirectXDevice *pBuilder_DirectXDevice);
	~CInitializer_MLRS();

	CWindow *getWindow() { return m_pWindow; }
};


//구현부
CInitializer_MLRS::CInitializer_MLRS(
	HINSTANCE hInst,
	CFactory_Window *pFactory_Windows,
	CBuilder_DirectXDevice *pBuilder_DirectXDevice)
	:m_pWindow(nullptr),
	m_pDevice(nullptr),
	m_pXAudio2(nullptr)
{
	CoInitialize(NULL);
	m_pWindow = new CWindow(hInst);
	if (!m_pWindow->createObject(pFactory_Windows))abort();
	if (!m_pWindow->createWindow())		abort();

	CGlobalWindow::setWindowHandle(m_pWindow->getWindowHandle());

	CGlobalWindow::setInstance(hInst);
	CGlobalWindow::setInfo();
	CGlobalWindow::setTitle(m_pWindow->getTitle());


	m_pDevice = new CDirectXDevice();
	if (!m_pDevice->createObject(pBuilder_DirectXDevice)) abort();
	CGlobalDirectXDevice::setDeviceInstance(m_pDevice->getDevice(),
											m_pDevice->getDeviceContext(),
											m_pDevice->getSwapChain());

	m_pXAudio2 = new CXAudio2Engine();
	m_pXAudio2->createObjects();
	CGlobalXAudio2Engine::setInstance(	m_pXAudio2->getXAudio2(),
										m_pXAudio2->getMasterVoice(),
										m_pXAudio2->getX3DAudioHandle(),
										m_pXAudio2->getChannel());
}
CInitializer_MLRS::~CInitializer_MLRS()
{
	if(m_pDevice) delete m_pDevice;
	if(m_pWindow) delete m_pWindow;
	if (m_pXAudio2) delete m_pXAudio2;

	CoUninitialize();
}
#endif