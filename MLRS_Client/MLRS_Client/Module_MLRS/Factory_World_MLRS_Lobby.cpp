﻿#include"stdafx.h"
#include "Factory_World_MLRS_Lobby.h"

#include"..\Module_Scene\Scene.h"
#include"GameObjectManager_MLRS_Lobby.h"
#include"Adapter_Msg_ToGame_MLRS_Lobby.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"Factory_LobbyComponent_MLRS.h"
#include"LobbyComponent_MLRS.h"
#include"..\MyCSVTable.h"

 
#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT

#ifndef HEADER_STL
#include<iostream>
#include<sstream>
#include<iomanip>
#endif // !HEADER_MYLOGCAT

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DirectX


using namespace DirectX;

//----------------------------------- CBuilder_Renderer_D3D_Lobby -------------------------------------------------------------------------------------

CBuilder_Renderer_D3D_Lobby::CBuilder_Renderer_D3D_Lobby()
	:CBuilder_Renderer_D3D(DirectX::XMFLOAT4(0, 0, 0, 0), DirectX::XMFLOAT2(0, 0))
{
}

bool CBuilder_Renderer_D3D_Lobby::createDepthStencilData(ID3D11DepthStencilState ** ppDepthStencilState, UINT & rnStencilRef, ID3D11DepthStencilView ** ppDepthStencilView)
{
	*ppDepthStencilView = nullptr;
	*ppDepthStencilState = factory_DepthStencilState_Disable.createDepthStencilState();

	return true;
}

bool CBuilder_Renderer_D3D_Lobby::setClearData(std::array<float, 4>& rfClearColor, UINT & rnClearFlag, float & rfClearDepth, UINT8 & rnClearStencil)
{
	rfClearColor[0] = 1.0f;
	rfClearColor[1] = 1.0f;
	rfClearColor[2] = 1.0f;
	rfClearColor[3] = 0.0f;

	rnClearFlag = 0;
	rfClearDepth = 0.0f;

	return true;
}
 


//----------------------------------- CFactory_World_MLRS_Lobby -------------------------------------------------------------------------------------

void CFactory_World_MLRS_Lobby::processCreateObjectsFailed(std::vector<IGameObject*>& result)
{
	MYLOGCAT(MYLOGCAT_ERROR_NORMAL, "CFactory_World_MLRS_InGame.createObjects() Error");
	for (auto data : result)
		delete data;
	result.clear();
}

std::vector<IGameObject *> CFactory_World_MLRS_Lobby::createObjects()
{ 
	std::vector<IGameObject *> retval;

	
	CLobbyComponent_Statics *pComponents = new CLobbyComponent_Statics;
	retval.push_back(pComponents);

	auto &rSection_Lobby = m_rIniData.getSection("Lobby");
	CMyINIData lobbyIni(g_sDataFolder + rSection_Lobby.getParam("LobbyIni"));
	CMyCSVTable lobbyTable(g_sDataFolder+ rSection_Lobby.getParam("LobbyTable"));
	auto &rTable = lobbyTable.getTable();
	for (auto &rLabel : rTable)
	{
		auto eType = convertType(rLabel[6]);

		if(eType==EType_LobbyComponent::eImage){ 
			CLobbyComponent_Drawable *pComponent = new CLobbyComponent_Drawable;
			//	retval.push_back(pComponent);
			pComponents->push_back(pComponent);
			if (!pComponent->createObject(&CFactory_LobbyComponent(rLabel[0],
				DirectX::XMFLOAT2(std::stof(rLabel[2]), std::stof(rLabel[3])),
				DirectX::XMFLOAT2(std::stof(rLabel[4]), std::stof(rLabel[5])),
				"Lobby/" + rLabel[0])))
			{
				for (auto pData : retval)
					delete pData;
				return std::vector<IGameObject *>();
				break;
			}
		}
		else if (eType == EType_LobbyComponent::eButton_MakeRoom)
		{
			CLobbyComponent_MakeRoom *pComponent = new CLobbyComponent_MakeRoom;
			//	retval.push_back(pComponent);
			retval.push_back(pComponent);
			if (!pComponent->createObject(&CFactory_LobbyComponent_Button_OnOff(rLabel[0],
				DirectX::XMFLOAT2(std::stof(rLabel[2]), std::stof(rLabel[3])),
				DirectX::XMFLOAT2(std::stof(rLabel[4]), std::stof(rLabel[5])),
				"Lobby/" + rLabel[0],
				"Lobby/" + rLabel[1])))
			{
				for (auto pData : retval)
					delete pData;
				return std::vector<IGameObject *>();
				break;
			} 
		}
		else if (eType == EType_LobbyComponent::eButton_Refresh)
		{
			CLobbyComponent_Refresh *pComponent = new CLobbyComponent_Refresh;
			//	retval.push_back(pComponent);
			retval.push_back(pComponent);
			if (!pComponent->createObject(&CFactory_LobbyComponent_Button(rLabel[0],
				DirectX::XMFLOAT2(std::stof(rLabel[2]), std::stof(rLabel[3])),
				DirectX::XMFLOAT2(std::stof(rLabel[4]), std::stof(rLabel[5])),
				"Lobby/" + rLabel[0] )))
			{
				for (auto pData : retval)
					delete pData;
				return std::vector<IGameObject *>();
				break;
			}
		}

	}

	auto &rSection_RoomList = lobbyIni.getSection("RoomList");
	CLobbyComponent_RoomList *pComponent = new CLobbyComponent_RoomList;
	//	retval.push_back(pComponent);
	retval.push_back(pComponent);
	if (!pComponent->createObject(&CFactory_LobbyComponent_List(rSection_RoomList.getParam("FileName"),
		rSection_RoomList.getParam_XMFLOAT2("StartPos"),
		rSection_RoomList.getParam_XMFLOAT2("Size"),
		"Lobby/" + rSection_RoomList.getParam("FileName"),
		rSection_RoomList.getParam_uint("Count"),
		rSection_RoomList.getParam_float("Gap"))))
	{
		for (auto pData : retval)
			delete pData;
		return std::vector<IGameObject *>();
	}


	return retval;
}
IGameObjectManager * CFactory_World_MLRS_Lobby::createObjectManager()
{
	CGameObjectManager_MLRS_Lobby *pResult = new CGameObjectManager_MLRS_Lobby();
	return pResult;

}
IScene* CFactory_World_MLRS_Lobby::createScene()
{
	CScene *pResult = new CScene();
//	CScene_DeferredLighting *pResult = new CScene_DeferredLighting();
	return pResult;
}

IAdapter_Msg_ToGame * CFactory_World_MLRS_Lobby::createMsgAdapter()
{
	CAdapter_Msg_ToGame_MLRS_Lobby* pMsgAdapter = new CAdapter_Msg_ToGame_MLRS_Lobby();
	return pMsgAdapter;
}

IRenderer * CFactory_World_MLRS_Lobby::createRenderer()
{ 
	CRenderer_D3D *pRenderer=new CRenderer_D3D();
	if (!CBuilder_Renderer_D3D_Lobby().build(pRenderer))
	//CRenderer_D3D_DeferredLighting *pRenderer = new CRenderer_D3D_DeferredLighting();
	//if (!CBuilder_Renderer_D3D_DeferredLighting(vColor, vRange).build(pRenderer))
	{
		delete pRenderer;
		return nullptr;
	}
	return pRenderer;
}

EType_LobbyComponent CFactory_World_MLRS_Lobby::convertType(const std::string & rsType)
{
	if (CUtility_String::convertToLower(rsType) == "image")	return EType_LobbyComponent::eImage;
	else if (CUtility_String::convertToLower(rsType) == "makeroom")	return EType_LobbyComponent::eButton_MakeRoom;
	else if (CUtility_String::convertToLower(rsType) == "refresh")	return EType_LobbyComponent::eButton_Refresh;
	else return EType_LobbyComponent::eImage;
}
