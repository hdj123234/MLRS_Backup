#pragma once
#ifndef HEADER_FACTORY_WORLD_MLRS_LOBBY
#define HEADER_FACTORY_WORLD_MLRS_LOBBY

#include"..\Module_World\GameWorld.h"
#include"..\Module_Renderer\Builder_Renderer_D3D.h"

//���漱��
class CMyINIData;

enum class EType_LobbyComponent : ENUMTYPE_256
{
	eImage,
	eButton_MakeRoom,
	eButton_Refresh,

};


class CBuilder_Renderer_D3D_Lobby : public CBuilder_Renderer_D3D { 

public:
	CBuilder_Renderer_D3D_Lobby( );
	virtual ~CBuilder_Renderer_D3D_Lobby() {}

private:
// 	virtual bool createRasterizerData(std::vector<D3D11_VIEWPORT> &rViewports, ID3D11RasterizerState **ppRasterizerState);
//	virtual bool createBlendData(ID3D11BlendState **ppBlendState, std::array<float, 4> &rfBlendFactor, UINT &rnSampleMask);
	virtual bool createDepthStencilData(ID3D11DepthStencilState **ppDepthStencilState, UINT &rnStencilRef, ID3D11DepthStencilView **ppDepthStencilView);
	virtual bool setClearData(std::array<float, 4> &rfClearColor, UINT &rnClearFlag, float &rfClearDepth, UINT8 &rnClearStencil);
	virtual bool setFogState(ID3D11Buffer * &rpCB_FogState) { rpCB_FogState = nullptr; return true; }
	 
};

class CFactory_World_MLRS_Lobby  : public IFactory_GameWorld {
private:
	const CMyINIData &m_rIniData;

public:
	CFactory_World_MLRS_Lobby(const CMyINIData& rIniData) :m_rIniData(rIniData){}
	virtual ~CFactory_World_MLRS_Lobby()  {}

private:
	void processCreateObjectsFailed(std::vector<IGameObject *> &result);

public:
	//override IFactory_GameWorld
	virtual std::vector<IGameObject *> createObjects();
	virtual IGameObjectManager * createObjectManager();
	virtual IScene* createScene();
	virtual IAdapter_Msg_ToGame* createMsgAdapter();
	virtual IRenderer* createRenderer() ;

private:
	EType_LobbyComponent convertType(const std::string &rsType);
};

#endif