#include "stdafx.h"
#include "MsgConverter_MLRS.h"

#include "Msg_Network_MLRS.h"
#include "..\..\..\MLRS_Server\MLRS_Server\protocol.h"

#include<WinSock2.h>


std::vector<char> CMsgConverter::getBytes(IMsg_CS * pMsg)
{
//	static bool s_bHostByteOrder_LittleEndian = !(htons(1) == u_short(1));
	std::vector<char> result;

	switch (pMsg->getType())
	{
	case EMsgType_CS::eNoName:
	{
		result.resize(2);
		unsigned char nSize = 2;
		unsigned char cDummyData = 126;
		memcpy(result.data(), &nSize, sizeof(nSize));
		memcpy(result.data()+1, &cDummyData, sizeof(cDummyData));
	}
	break;
	case EMsgType_CS::eMove:
	{
		CMsg_CS_Move *pMsg_Move = static_cast<CMsg_CS_Move *>(pMsg);
		cs_packet_move sendData;
		sendData.type = CS_MOVE;
		sendData.move = pMsg_Move->getMoveFlag();
		sendData.size = sizeof(sendData);
		sendData.direction_x = pMsg_Move->m_vDirection.x;
		sendData.direction_z = pMsg_Move->m_vDirection.z;

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	}
	break;
	case EMsgType_CS::eAttack:
	{
		CMsg_CS_Attack *pMsg_Attack = static_cast<CMsg_CS_Attack *>(pMsg);
		cs_packet_shoot sendData;
		sendData.type = CS_SHOOT1;
		sendData.size = sizeof(sendData);
		sendData.point_x = pMsg_Attack->getPos().x;
		sendData.point_y = pMsg_Attack->getPos().y;
		sendData.point_z = pMsg_Attack->getPos().z;
		sendData.direction_x = pMsg_Attack->getDir().x;
		sendData.direction_y = pMsg_Attack->getDir().y;
		sendData.direction_z = pMsg_Attack->getDir().z;

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	}
	break;
	case EMsgType_CS::eMoveAndAim:
	{
		CMsg_CS_MoveAndAim *pMsg_Move = static_cast<CMsg_CS_MoveAndAim *>(pMsg);
		cs_packet_aim_move1 sendData;
		sendData.type = CS_AIM_MOVE1;
		sendData.move = pMsg_Move->getFlag();
		sendData.size = sizeof(sendData);
		sendData.direction_x = pMsg_Move->getMoveDirection().x;
		sendData.direction_z = pMsg_Move->getMoveDirection().y;
		sendData.look_x = pMsg_Move->getLook().x;
		sendData.look_z = pMsg_Move->getLook().y;
		sendData.arm_y = pMsg_Move->getArmAngle();

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);

	}
	break;
	case EMsgType_CS::eStop:
	{
		CMsg_CS_Stop *pMsg_Stop = static_cast<CMsg_CS_Stop *>(pMsg);
		cs_packet_move sendData;
		sendData.type = CS_MOVE;
		sendData.move = 0;
		sendData.size = sizeof(sendData);
		sendData.direction_x = 0;
		sendData.direction_z = 0;

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	}
	break;
	}

//	if (s_bHostByteOrder_LittleEndian)
//		std::reverse(result.begin(), result.end());
	return result;
//	return std::vector<char>();
}

std::vector<char> CMsgConverter::getBytes(std::vector<IMsg_CS*>& rpMsgs)
{
	std::vector<char> result;

	for (auto data : rpMsgs)
	{
		std::vector<char> &bytes = getBytes(data);
		result.insert(result.end(), bytes.begin(), bytes.end());
	}

	return result;
}

std::vector<IMsg_SC*> CMsgConverter::getMsgs(std::vector<char>& rBytes)
{
	return std::vector<IMsg_SC*>();
}

IMsg_SC * CMsgConverter::getMsg(std::vector<char>& rBytes)
{
//	static bool s_bHostByteOrder_LittleEndian = !(htons(1) == u_short(1));
	IMsg_SC *pResult=nullptr;
	if (rBytes.empty())	return nullptr;
	//if(s_bHostByteOrder_LittleEndian)
	//	std::reverse(rBytes.begin(), rBytes.end());

	switch (rBytes[1])
	{
	case SC_PLAYER_MOVE:
	{
		sc_packet_move packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;
		pMsg->m_eTarget = EObjTarget_SC::ePlayer;
		pMsg->m_nID = packet.id;
		switch (packet.move)
		{
		case object_translate::OBJECT_MOVE:
		case object_translate::OBJECT_JUMP:
		case object_translate::OBJECT_BOOST:
			pMsg->m_bStop = false;
			break;
		case object_translate::OBJECT_STOP:
			pMsg->m_bStop = true;
			break;
		}
		pMsg->m_vPosition.x = packet.pos_x;
		pMsg->m_vPosition.y = packet.pos_y;
		pMsg->m_vPosition.z = packet.pos_z;
		pResult = pMsg;
		break;	
	}
	case SC_FIRE_MOVE:
	{
		sc_packet_aim_move packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move_PosAndDir* pMsg = new CMsg_SC_Move_PosAndDir;
		pMsg->m_eTarget = EObjTarget_SC::ePlayer;
		pMsg->m_nID = packet.id;
		pMsg->m_bStop = true;
		
		pMsg->m_vPosition.x = packet.pos_x;
		pMsg->m_vPosition.y = packet.pos_y;
		pMsg->m_vPosition.z = packet.pos_z;
		pMsg->m_vDirection.x = packet.look_x;
		pMsg->m_vDirection.y = packet.arm_y;
		pMsg->m_vDirection.z = packet.look_z;
		pResult = pMsg;

		break;
	}
	case SC_ENEMY_MOVE:
	{
		sc_packet_emove packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;
		pMsg->m_eTarget = EObjTarget_SC::eEnemy;
		pMsg->m_nID = packet.id;
		switch (packet.move)
		{
		case object_translate::OBJECT_MOVE:
			pMsg->m_bStop = false;
			break;
		case object_translate::OBJECT_STOP:
			pMsg->m_bStop = true;
			break;
		}
		pMsg->m_vPosition.x = packet.pos_x;
		pMsg->m_vPosition.y = packet.pos_y;
		pMsg->m_vPosition.z = packet.pos_z;
		pResult = pMsg;
		break;
	}
	case SC_MISSILE_MOVE:
	{
		sc_packet_mmove packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;
		pMsg->m_eTarget = EObjTarget_SC::eMissile;
		pMsg->m_nID = packet.id;
		pMsg->m_bStop = false;
		pMsg->m_vPosition.x = packet.pos_x;
		pMsg->m_vPosition.y = packet.pos_y;
		pMsg->m_vPosition.z = packet.pos_z;
		pResult = pMsg;
		break;
	}
	case SC_JOIN_PLAYER:
	{
		sc_packet_join packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_CreateObj* pMsg = new CMsg_SC_CreateObj;
		pMsg->getMoveMsg()->m_eTarget = EObjTarget_SC::ePlayer;
		pMsg->getMoveMsg()->m_bStop = true;
		pMsg->getMoveMsg()->m_nID = packet.id;
		pMsg->getMoveMsg()->m_vPosition.x = packet.pos_x;
		pMsg->getMoveMsg()->m_vPosition.y = packet.pos_y;
		pMsg->getMoveMsg()->m_vPosition.z = packet.pos_z;
		pResult = pMsg;
		break;
	}
	case SC_REMOVE_PLAYER:
	{
		sc_packet_remove_player packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_RemoveObj* pMsg = new CMsg_SC_RemoveObj;
		pMsg->m_nID = packet.id;
		pMsg->m_eObjType = EObjTarget_SC::ePlayer;

		pResult = pMsg;
		break;
	}
	case SC_CLIENTID:
	{
		sc_packet_enter_user packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_ClientID* pMsg = new CMsg_SC_ClientID;
		pMsg->m_nID = packet.id;

		pResult = pMsg;
		break;
	}
	case SC_OBJECT_REMOVE:
	{
		sc_packet_objRemove packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_RemoveObj* pMsg = new CMsg_SC_RemoveObj;
		pMsg->m_nID = packet.id;
		switch (packet.obj_type)
		{
		case object_Type::OBJECT_PLAYER:
			pMsg->m_eObjType = EObjTarget_SC::ePlayer;
			break;
		case object_Type::OBJECT_ENEMY:
			pMsg->m_eObjType = EObjTarget_SC::eEnemy;
			break;
		case object_Type::OBJECT_MISSILE:
			pMsg->m_eObjType = EObjTarget_SC::eMissile;
			break;
		}

		pResult = pMsg;
		break;
	}
	case SC_MISSILE_BOOM:
	{	
		sc_packet_mboom packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_RemoveObj* pMsg = new CMsg_SC_RemoveObj;
		pMsg->m_nID = packet.id;
		pMsg->m_eObjType = EObjTarget_SC::eMissile;
		pMsg->m_vPos = DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z);
			
		pResult = pMsg;
		break;
	}
	case SC_MISSILE_CREATE:
	{
		sc_packet_mcreate packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_CreateObj* pMsg = new CMsg_SC_CreateObj;
		pMsg->m_ModelID = 0;// packet.m_type;
		pMsg->getMoveMsg()->m_nID = packet.id;
		pMsg->getMoveMsg()->m_eTarget = EObjTarget_SC::eMissile;
		pMsg->getMoveMsg()->m_bStop = true;

		pMsg->getMoveMsg()->m_vPosition.x = packet.pos_x;
		pMsg->getMoveMsg()->m_vPosition.y = packet.pos_y;
		pMsg->getMoveMsg()->m_vPosition.z = packet.pos_z;

		pResult = pMsg;
		break;
	}
	case SC_ENEMY_CREATE:
	{
		sc_packet_ecreate packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_CreateObj* pMsg = new CMsg_SC_CreateObj;

		pMsg->m_ModelID = packet.m_type;
		pMsg->getMoveMsg()->m_nID = packet.id;
		pMsg->getMoveMsg()->m_eTarget = EObjTarget_SC::eEnemy;
		pMsg->getMoveMsg()->m_bStop = true;

		pMsg->getMoveMsg()->m_vPosition.x = packet.pos_x;
		pMsg->getMoveMsg()->m_vPosition.y = packet.pos_y;
		pMsg->getMoveMsg()->m_vPosition.z = packet.pos_z;

		pResult = pMsg;
		break;
	}
	}

	rBytes = std::vector<char>(rBytes.begin() + rBytes[0], rBytes.end());

	return pResult;
}
