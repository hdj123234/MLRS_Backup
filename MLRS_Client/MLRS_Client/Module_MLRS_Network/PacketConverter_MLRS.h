
#pragma once
#ifndef HEADER_PACKETCONVERTER_MLRS
#define HEADER_PACKETCONVERTER_MLRS

#include"..\Module_Network\Interface_Network.h"

class CPacketConverter : public INetworkMsgConverter {
public:
	CPacketConverter() {}
	virtual ~CPacketConverter() {}

	virtual std::vector<char> getBytes(IMsg_CS *pMsg);
	virtual std::vector<char> getBytes(std::vector<IMsg_CS *>&rpMsgs);
	
	virtual std::vector<IMsg_SC *> getMsgs(std::vector<char> &rBytes);
	virtual IMsg_SC * getMsg(std::vector<char> &rBytes);
};

#endif