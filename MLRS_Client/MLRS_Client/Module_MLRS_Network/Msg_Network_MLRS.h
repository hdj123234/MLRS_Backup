#pragma once
#ifndef HEADER_MSG_NETWORK
#define HEADER_MSG_NETWORK

#include"..\Module_MLRS\Msg_InGame_MLRS.h"
#include"..\Module_Network\Interface_Network.h"
#include"..\Module_Object\Interface_Object.h"
#include"..\Module_Object\Msg_Movement.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif

#ifndef HEADER_STL
#include<array>
#endif // !HEADER_STL


//���漱��
enum class EWeaponType : char;

//----------------------------------- CMsg_SC -------------------------------------------------------------------------------------

enum class EMsgType_SC : ENUMTYPE_256 {
	eMove,
	eClientID,
	eCreateObj,
	eRemoveObj,
	eHP,
	eDrawScript,
	eChangeWeapon,
	ePlayerDown,
	eLobby_RoomList,
	eLobby_RoomData,
	eLobby_EnterRoom,
};

class CMsg_SC : public CMsg_MLRS,
				virtual public IMsg_SC {
private:
	EMsgType_SC m_eMsgType;
public:
	CMsg_SC(EMsgType_SC eMsgType) 
		:CMsg_MLRS(EMsgType_MLRS::eReceivedFromServer),
		m_eMsgType(eMsgType) {}
	virtual ~CMsg_SC() {}

	//override IMsg_CS
	EMsgType_SC getType() { return m_eMsgType; }
};

enum class EObjTarget_SC : ENUMTYPE_256 {
	ePlayer,
	eEnemy,
	eMissile,
	eBoss,
	eBaseCamp
};


class CMsg_SC_Move : public CMsg_SC{
public:
	enum EMoveFlag :ENUMTYPE_FLAG_8 {
		eStop = 0x01,
		eAttack1 = 0x02,
		eJump = 0x04,
		eBoost = 0x08,
	};
private:
	IMsg_Movement *m_pMsg_Movement;

public:
	EObjTarget_SC m_eTarget;
	unsigned int m_nID;
	FLAG_8 m_Flag;
//	bool m_bStop;
protected:
	CMsg_SC_Move(EMsgType_SC eType) :CMsg_SC(eType), m_pMsg_Movement(nullptr), m_Flag(0){}

public:
	CMsg_SC_Move() :CMsg_SC(EMsgType_SC::eMove), m_pMsg_Movement(nullptr){}
	virtual ~CMsg_SC_Move() { delete m_pMsg_Movement; }
	
	void setMovementMsg(IMsg_Movement *pMsg_Movement) { m_pMsg_Movement = pMsg_Movement; }
	IMsg_Movement *getMovementMsg() { return m_pMsg_Movement; }
	const bool checkFlag(const EMoveFlag eFlag)const { return (m_Flag&eFlag)!=0; }
	void onFlag(const EMoveFlag eFlag)  {  m_Flag|=eFlag  ; }
};


class CMsg_SC_ID : public CMsg_SC {
public:
	CMsg_SC_ID(EMsgType_SC eMsgType) :CMsg_SC(eMsgType) {}
	virtual ~CMsg_SC_ID() {}

	unsigned int m_nID;
};

class CMsg_SC_ChangeWeapon : public CMsg_SC_ID {
public:
	CMsg_SC_ChangeWeapon() :CMsg_SC_ID(EMsgType_SC::eChangeWeapon) {}
	virtual ~CMsg_SC_ChangeWeapon() {}

	EWeaponType m_eWeaponType;
};
class CMsg_SC_ClientID : public CMsg_SC_ID {
public:
	CMsg_SC_ClientID() :CMsg_SC_ID(EMsgType_SC::eClientID) {}
	virtual ~CMsg_SC_ClientID() {}
};

class CMsg_SC_PlayerDown : public CMsg_SC_ID {
public:
	CMsg_SC_PlayerDown() :CMsg_SC_ID(EMsgType_SC::ePlayerDown) {}
	virtual ~CMsg_SC_PlayerDown() {}
};
class CMsg_SC_ObjTypeAndID : public CMsg_SC_ID {
public:
	CMsg_SC_ObjTypeAndID(EMsgType_SC eMsgType) :CMsg_SC_ID(eMsgType) {}
	virtual ~CMsg_SC_ObjTypeAndID() {}

	EObjTarget_SC m_eObjType;
};

class CMsg_SC_RemoveObj : public CMsg_SC_ObjTypeAndID {
public:
	DirectX::XMFLOAT3 m_vPos;
public:
	CMsg_SC_RemoveObj() :CMsg_SC_ObjTypeAndID(EMsgType_SC::eRemoveObj) {}
	virtual ~CMsg_SC_RemoveObj() {}

};

class CMsg_SC_CreateObj : public CMsg_SC_Move {
public:
	BYTE m_ModelID;
public:
	CMsg_SC_CreateObj() :CMsg_SC_Move(EMsgType_SC::eCreateObj) {}
	virtual ~CMsg_SC_CreateObj() {}
	
	BYTE getModelID() { return m_ModelID; }
};

class CMsg_SC_DrawScript : public CMsg_SC {
private:
	const unsigned short m_nScriptID;
public:
	CMsg_SC_DrawScript(const unsigned short nScriptID) :CMsg_SC(EMsgType_SC::eDrawScript), m_nScriptID(nScriptID) {}
	virtual ~CMsg_SC_DrawScript() {}

	const unsigned short getScriptID() { return m_nScriptID; }
};

class CMsg_SC_HP : public CMsg_SC_ObjTypeAndID {
private:
	const float m_fHP;
public:
	CMsg_SC_HP(const float fHP) :CMsg_SC_ObjTypeAndID(EMsgType_SC::eHP), m_fHP(fHP){}
	virtual ~CMsg_SC_HP() {}

	const float getHP() { return m_fHP; }
};

class CMsg_SC_RoomList : public CMsg_SC {
public:
	struct RoomData {
		int m_nRoomID;
		int m_nTitleID;
		int m_nNumberOfPlayer;
	};
private:
	std::vector<RoomData> m_RoomDatas;
public:
	CMsg_SC_RoomList(std::vector<RoomData> &rRoomDatas ) :CMsg_SC(EMsgType_SC::eLobby_RoomList), m_RoomDatas(rRoomDatas) {}
	virtual ~CMsg_SC_RoomList() {}

	const std::vector<RoomData>& getRoomDatas() { return m_RoomDatas; }
};

class CMsg_SC_RoomData : public CMsg_SC_ID {
public:
	struct PlayerData{
		bool m_bOn;
		int m_nID;
		int m_nMecha;
		bool m_bReady;
	};
	std::array<PlayerData,4> m_PlayerDatas;
public:
	CMsg_SC_RoomData(const std::array<PlayerData, 4> &rPlayerDatas) 
		:CMsg_SC_ID(EMsgType_SC::eLobby_RoomData),
		m_PlayerDatas(rPlayerDatas){}
	virtual ~CMsg_SC_RoomData() {}

	const std::array<PlayerData, 4>& getPlayerDatas() { return m_PlayerDatas; }
};
class CMsg_SC_EnterRoom : public CMsg_SC_ID {
public:
	CMsg_SC_EnterRoom() :CMsg_SC_ID(EMsgType_SC::eLobby_EnterRoom) {}
	virtual ~CMsg_SC_EnterRoom() {}
};
//----------------------------------- CMsg_CS -------------------------------------------------------------------------------------

enum class EMsgType_CS:ENUMTYPE_256 {
	eNoName,
	eMove,
	eStop,
	eChangeWeapon,
	eMoveAndAim_Weapon1,
	eAim_Weapon2,
	eAttack_Weapon1,
	eAttack_Weapon2,
	eAttack_Weapon3,
	eLobby_CreateRoom,
};

class CMsg_CS : public IMsg_CS {
private:
	EMsgType_CS m_eMsgType;
public:
	CMsg_CS(EMsgType_CS eMsgType) :m_eMsgType(eMsgType) {}
	virtual ~CMsg_CS() {}

	//override IMsg_CS
	EMsgType_CS getType() { return m_eMsgType; }
};

class CMsg_CS_NoName : public CMsg_CS {
public:
	CMsg_CS_NoName() :CMsg_CS(EMsgType_CS::eNoName) {}
	virtual ~CMsg_CS_NoName() {}
};

class CMsg_CS_Move : public CMsg_CS {
private:
	FLAG_8 m_Flag_Move;
public:
	DirectX::XMFLOAT3 m_vDirection;

	CMsg_CS_Move(const FLAG_8 flag_Move) :CMsg_CS(EMsgType_CS::eMove), m_Flag_Move(flag_Move) {}
	virtual ~CMsg_CS_Move() {}

	FLAG_8 getMoveFlag() { return m_Flag_Move; }
};

enum class EWeaponType : char
{
	eNoWeapon = -1,
	eWeapon1 = 0,
	eWeapon2 = 1,
	eWeapon3 = 2,
};

class CMsg_CS_ChangeWeapon : public CMsg_CS {
private:
	EWeaponType m_eWeaponType;
public:

	CMsg_CS_ChangeWeapon(EWeaponType eWeaponType) :CMsg_CS(EMsgType_CS::eChangeWeapon) , m_eWeaponType(eWeaponType){}
	virtual ~CMsg_CS_ChangeWeapon() {}

	EWeaponType getWeaponType() { return m_eWeaponType; }
};

class CMsg_CS_Stop : public CMsg_CS {
public:

	CMsg_CS_Stop() :CMsg_CS(EMsgType_CS::eStop) {}
	virtual ~CMsg_CS_Stop() {}
};

class CMsg_CS_Attack1 : public CMsg_CS {
private:
	const DirectX::XMFLOAT3 m_vPos;
	const DirectX::XMFLOAT3 m_vDir;
	const unsigned char m_MissileType;
public:
	CMsg_CS_Attack1(	const DirectX::XMFLOAT3 &rvPos,
					const DirectX::XMFLOAT3 &rvDir,
					const unsigned char missileType=1)
		:CMsg_CS(EMsgType_CS::eAttack_Weapon1),
		m_vPos(rvPos),
		m_vDir(rvDir),
		m_MissileType(missileType){}
	virtual ~CMsg_CS_Attack1() {}

	const DirectX::XMFLOAT3& getPos() { return m_vPos; }
	const DirectX::XMFLOAT3& getDir() { return m_vDir; }
	const unsigned char getType()const { return m_MissileType; }
};

class CMsg_CS_Attack2 : public CMsg_CS {
private:
	const DirectX::XMFLOAT3 m_vPos;
	const DirectX::XMFLOAT3X3 m_mMissilePort; 
public:
	CMsg_CS_Attack2(const DirectX::XMFLOAT3 &rvPos,
		const DirectX::XMFLOAT3X3 &rmMissilePort )
		:CMsg_CS(EMsgType_CS::eAttack_Weapon2),
		m_vPos(rvPos),
		m_mMissilePort(rmMissilePort)  {}
	virtual ~CMsg_CS_Attack2() {}

	const DirectX::XMFLOAT3& getPos() { return m_vPos; }
	const DirectX::XMFLOAT3X3& getMatrix_MissilePort() { return m_mMissilePort; } 
};

class CMsg_CS_Attack3 : public CMsg_CS {
private:
	std::vector<unsigned short> m_nIDs;
public:
	CMsg_CS_Attack3(const std::vector<unsigned short> & nIDs)
		:CMsg_CS(EMsgType_CS::eAttack_Weapon3),
		m_nIDs(nIDs) {}
	virtual ~CMsg_CS_Attack3() {}

	const std::vector<unsigned short>& getIDs_LockOn() { return m_nIDs; }
};

class CMsg_CS_MoveAndAim_Weapon1 : public CMsg_CS {
private:
	const DirectX::XMFLOAT2 m_vMoveDirection;
	const DirectX::XMFLOAT2 m_vLook;
	const float m_fArmAngle;
	const FLAG_8 m_Flag;

public:
	CMsg_CS_MoveAndAim_Weapon1(	const DirectX::XMFLOAT2 &rvMoveDirection,
						const DirectX::XMFLOAT2 &rvLook,
						const float fArmAngle,
						const FLAG_8 flag) 
		:CMsg_CS(EMsgType_CS::eMoveAndAim_Weapon1),
		m_vMoveDirection(rvMoveDirection),
		m_vLook(rvLook),
		m_fArmAngle(fArmAngle),
		m_Flag(flag)
	{}
	virtual ~CMsg_CS_MoveAndAim_Weapon1() {}

	const DirectX::XMFLOAT2& getMoveDirection() { return m_vMoveDirection; }
	const DirectX::XMFLOAT2& getLook(){ return m_vLook; }
	const float				getArmAngle() { return m_fArmAngle; }

	const FLAG_8 getFlag() { return m_Flag; }
};

class CMsg_CS_Aim_Weapon2 : public CMsg_CS {
private:
	const DirectX::XMFLOAT2 m_vLook;
	const float m_fArmAngle; 

public:
	CMsg_CS_Aim_Weapon2(const DirectX::XMFLOAT2 &rvLook,
		const float fArmAngle )
		:CMsg_CS(EMsgType_CS::eAim_Weapon2), 
		m_vLook(rvLook),
		m_fArmAngle(fArmAngle) 
	{}
	virtual ~CMsg_CS_Aim_Weapon2() {}
	 
	const DirectX::XMFLOAT2& getLook() { return m_vLook; }
	const float				getArmAngle() { return m_fArmAngle; }
	 
};

class CMsg_CS_CreateRoom : public CMsg_CS {
private: 

public:
	CMsg_CS_CreateRoom( )
		:CMsg_CS(EMsgType_CS::eLobby_CreateRoom) 
	{}
	virtual ~CMsg_CS_CreateRoom() {}
	 

};

#endif