#include "stdafx.h"
#include "PacketConverter_MLRS.h"

#include "Msg_Network_MLRS.h"
#include "..\..\..\MLRS_Server\MLRS_Server\protocol.h"

#include<WinSock2.h>


std::vector<char> CPacketConverter::getBytes(IMsg_CS * pMsg)
{
//	static bool s_bHostByteOrder_LittleEndian = !(htons(1) == u_short(1));
	std::vector<char> result;

	switch (pMsg->getType())
	{
	case EMsgType_CS::eNoName:
	{
		result.resize(3);
		unsigned char nSize = 3;
		unsigned char cDummyData = 126;
		unsigned char cDummyData2 = 24;
		memcpy(result.data(), &nSize, sizeof(nSize));
		memcpy(result.data() + 1, &cDummyData, 1);
		memcpy(result.data() + 2, &cDummyData2, 1);
	break;
	}
	case EMsgType_CS::eMove:
	{
		CMsg_CS_Move *pMsg_Move = static_cast<CMsg_CS_Move *>(pMsg);
		cs_packet_move sendData;
		sendData.type = CS_MOVE;
		sendData.move = pMsg_Move->getMoveFlag();
		sendData.size = sizeof(sendData);
		sendData.direction_x = pMsg_Move->m_vDirection.x;
		sendData.direction_z = pMsg_Move->m_vDirection.z;

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	break;
	}
	case EMsgType_CS::eAttack_Weapon1:
	{
		CMsg_CS_Attack1 *pMsg_Attack = static_cast<CMsg_CS_Attack1 *>(pMsg);
		cs_packet_shoot sendData;
		sendData.type = CS_SHOOT1;
		switch (pMsg_Attack->getType())
		{
		case 1:
			sendData.type = CS_SHOOT1;
			break;
		case 2:
			sendData.type = CS_SHOOT2;
			break;

		}
		sendData.m_type = 1;
		sendData.size = sizeof(sendData);
		sendData.point_x = pMsg_Attack->getPos().x;
		sendData.point_y = pMsg_Attack->getPos().y;
		sendData.point_z = pMsg_Attack->getPos().z;
		sendData.direction_x = pMsg_Attack->getDir().x;
		sendData.direction_y = pMsg_Attack->getDir().y;
		sendData.direction_z = pMsg_Attack->getDir().z;

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	break;
	}
	case EMsgType_CS::eAttack_Weapon2:
	{
		CMsg_CS_Attack2 *pMsg_Attack = static_cast<CMsg_CS_Attack2 *>(pMsg);
		cs_packet_shoot_multiple sendData;
		sendData.type = CS_SHOOT2; 
		sendData.size = sizeof(sendData);
		auto &rvPos = pMsg_Attack->getPos();
		auto &rmLocal = pMsg_Attack->getMatrix_MissilePort();
		sendData.pos[0] = rvPos.x; sendData.pos[1] = rvPos.y; sendData.pos[2] = rvPos.z;
		sendData.right[0] = rmLocal.m[0][0];	sendData.right[1] = rmLocal.m[0][1];	sendData.right[2] = rmLocal.m[0][2];
		sendData.up[0] =	rmLocal.m[1][0];	sendData.up[1] =	rmLocal.m[1][1];	sendData.up[2] =	rmLocal.m[1][2];
		sendData.look[0] =	rmLocal.m[2][0];	sendData.look[1] =	rmLocal.m[2][1];	sendData.look[2] =	rmLocal.m[2][2];

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	break;
	}
	case EMsgType_CS::eAttack_Weapon3:
	{
		CMsg_CS_Attack3 *pMsg_Attack = static_cast<CMsg_CS_Attack3 *>(pMsg);
		auto &rnIDs = pMsg_Attack->getIDs_LockOn();
		cs_packet_shoot_lockon sendData;
		sendData.type = CS_SHOOT3;
		sendData.size = sizeof(sendData);
		for (int i = 0; i < 12; ++i)
		{
			if (rnIDs.size() <= i)
			{
				sendData.lock_on[i] = 0xffff;
				break;
			}
			else
				sendData.lock_on[i] = rnIDs[i];
		}

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	break;
	}
	case EMsgType_CS::eChangeWeapon:
	{
		CMsg_CS_ChangeWeapon *pMsg_ChangeWeapon = static_cast<CMsg_CS_ChangeWeapon *>(pMsg);
		cs_packet_change_weapon sendData;
		sendData.type = CS_CHANGE_WEAPON;
		sendData.size = sizeof(sendData);
		sendData.w_type = static_cast<char>(pMsg_ChangeWeapon->getWeaponType());
		
		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	break;
	}
	case EMsgType_CS::eMoveAndAim_Weapon1:
	{
		CMsg_CS_MoveAndAim_Weapon1 *pMsg_Move = static_cast<CMsg_CS_MoveAndAim_Weapon1 *>(pMsg);
		cs_packet_aim_move1 sendData;
		sendData.type = CS_AIM_MOVE1;
		sendData.move = pMsg_Move->getFlag();
		sendData.size = sizeof(sendData);
		sendData.direction_x = pMsg_Move->getMoveDirection().x;
		sendData.direction_z = pMsg_Move->getMoveDirection().y;
		sendData.look_x = pMsg_Move->getLook().x;
		sendData.look_z = pMsg_Move->getLook().y;
		sendData.arm_y = pMsg_Move->getArmAngle();

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);

	break;
	}
	case EMsgType_CS::eAim_Weapon2:
	{
		CMsg_CS_Aim_Weapon2 *pMsg_Move = static_cast<CMsg_CS_Aim_Weapon2 *>(pMsg);
		cs_packet_aim_move2 sendData;
		sendData.type = CS_AIM_MOVE2;
		sendData.size = sizeof(sendData);
		sendData.look_x = pMsg_Move->getLook().x;
		sendData.look_z = pMsg_Move->getLook().y;
		sendData.angle_y = pMsg_Move->getArmAngle();

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);

	break;
	}
	case EMsgType_CS::eStop:
	{
		CMsg_CS_Stop *pMsg_Stop = static_cast<CMsg_CS_Stop *>(pMsg);
		cs_packet_move sendData;
		sendData.type = CS_MOVE;
		sendData.move = 0;
		sendData.size = sizeof(sendData);
		sendData.direction_x = 0;
		sendData.direction_z = 0;

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
	break;
	}
	case EMsgType_CS::eLobby_CreateRoom:
	{
		CMsg_CS_CreateRoom *pMsg_Stop = static_cast<CMsg_CS_CreateRoom *>(pMsg);
		cs_packet_create_room sendData;
		sendData.type = CS_CREATE_ROOM; 
		sendData.size = sizeof(sendData); 

		result.resize(sendData.size);
		memcpy(result.data(), &sendData, sendData.size);
		break;
	}
	}

//	if (s_bHostByteOrder_LittleEndian)
//		std::reverse(result.begin(), result.end());
	return result;
//	return std::vector<char>();
}

std::vector<char> CPacketConverter::getBytes(std::vector<IMsg_CS*>& rpMsgs)
{
	std::vector<char> result;

	for (auto data : rpMsgs)
	{
		std::vector<char> &bytes = getBytes(data);
		result.insert(result.end(), bytes.begin(), bytes.end());
	}

	return result;
}

std::vector<IMsg_SC*> CPacketConverter::getMsgs(std::vector<char>& rBytes)
{
	return std::vector<IMsg_SC*>();
}

IMsg_SC * CPacketConverter::getMsg(std::vector<char>& rBytes)
{
//	static bool s_bHostByteOrder_LittleEndian = !(htons(1) == u_short(1));
	IMsg_SC *pResult=nullptr;
	if (rBytes.empty())	return nullptr;
	//if(s_bHostByteOrder_LittleEndian)
	//	std::reverse(rBytes.begin(), rBytes.end());

	switch (rBytes[1])
	{
	case SC_CHANGE_WEAPON:
	{
		sc_packet_change_weapon packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_ChangeWeapon* pMsg = new CMsg_SC_ChangeWeapon;
		pMsg->m_nID = packet.id;
		pMsg->m_eWeaponType = static_cast<EWeaponType>(packet.w_type);
		pResult = pMsg;
		break;
	}
	case SC_PLAYER_MOVE:
	{
		sc_packet_move packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;
		pMsg->m_eTarget = EObjTarget_SC::ePlayer;
		pMsg->m_nID = packet.id;
		if (!packet.move&MOVE_TYPE::MT_MOVE)
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);
		if (packet.move&MOVE_TYPE::MT_JUMP)
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eJump);
		if (packet.move&MOVE_TYPE::MT_BOOST)
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eBoost);
		pMsg->setMovementMsg( new CMsg_Movement_Vector3(EMsgType_Movement::eReplace_Position,
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z)));
		pResult = pMsg;
		break;	
	}
	case SC_FIRE_MOVE1:
	{
		sc_packet_aim_move1 packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;
		pMsg->m_eTarget = EObjTarget_SC::ePlayer;
		pMsg->m_nID = packet.id;
		
		pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eAttack1);

		if (!packet.move&MOVE_TYPE::MT_MOVE)
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);
		if (packet.move&MOVE_TYPE::MT_JUMP)
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eJump);
		if (packet.move&MOVE_TYPE::MT_BOOST)
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eBoost);

		pMsg->setMovementMsg(new CMsg_Movement_PosAndDir(
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z),
			DirectX::XMFLOAT3(packet.look_x, packet.arm_y, packet.look_z)
		));

		pResult = pMsg;

		break;
	}
	case SC_ENEMY_MOVE:
	{
		sc_packet_emove packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;
		pMsg->m_eTarget = EObjTarget_SC::eEnemy;
		pMsg->m_nID = packet.id;
		switch (packet.move)		{
		case object_translate::OBJECT_STOP:
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);
			break;

		}
		//if (!packet.move&MOVE_TYPE::MT_MOVE)
		//	pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);
		//if (packet.move&MOVE_TYPE::MT_JUMP)
		//	pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eJump);
		//if (packet.move&MOVE_TYPE::MT_BOOST)
		//	pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eBoost);
		pMsg->setMovementMsg(new CMsg_Movement_Vector3(EMsgType_Movement::eReplace_Position,
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z)
		));
		pResult = pMsg;
		break;
	}
	case SC_MISSILE_MOVE:
	{
		sc_packet_mmove packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;
		pMsg->m_eTarget = EObjTarget_SC::eMissile;
		pMsg->m_nID = packet.id;
//		pMsg->m_bStop = false;

		pMsg->setMovementMsg(new CMsg_Movement_Vector3(EMsgType_Movement::eReplace_Position,
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z)
		));
		pResult = pMsg;
		break;
	}
	case SC_JOIN_PLAYER:
	{
		sc_packet_join packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_CreateObj* pMsg = new CMsg_SC_CreateObj;

		pMsg->m_eTarget = EObjTarget_SC::ePlayer;
		pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);
		 
		pMsg->m_nID = packet.id;

		pMsg->setMovementMsg(new CMsg_Movement_Vector3(EMsgType_Movement::eReplace_Position,
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z)
		));
		pResult = pMsg;
		break;
	}
	case SC_REMOVE_PLAYER:
	{
		sc_packet_remove_player packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_RemoveObj* pMsg = new CMsg_SC_RemoveObj;
		pMsg->m_nID = packet.id;
		pMsg->m_eObjType = EObjTarget_SC::ePlayer;

		pResult = pMsg;
		break;
	}
	case SC_CLIENTID:
	{
		sc_packet_enter_user packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_ClientID* pMsg = new CMsg_SC_ClientID;
		pMsg->m_nID = packet.id;

		pResult = pMsg;
		break;
	}
	case SC_OBJECT_REMOVE:
	{
		sc_packet_objRemove packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_RemoveObj* pMsg = new CMsg_SC_RemoveObj;
		pMsg->m_nID = packet.id;
		switch (packet.obj_type)
		{
		case object_Type::OBJECT_PLAYER:
			pMsg->m_eObjType = EObjTarget_SC::ePlayer;
			break;
		case object_Type::OBJECT_ENEMY:
			pMsg->m_eObjType = EObjTarget_SC::eEnemy;
			break;
		case object_Type::OBJECT_MISSILE:
			pMsg->m_eObjType = EObjTarget_SC::eMissile;
			break;
		}

		pResult = pMsg;
		break;
	}
	case SC_MISSILE_BOOM:
	{	
		sc_packet_mboom packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_RemoveObj* pMsg = new CMsg_SC_RemoveObj;
		pMsg->m_nID = packet.id;
		pMsg->m_eObjType = EObjTarget_SC::eMissile;
		pMsg->m_vPos = DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z);
			
		pResult = pMsg;
		break;
	}
	case SC_MISSILE_CREATE:
	{
		sc_packet_mcreate packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_CreateObj* pMsg = new CMsg_SC_CreateObj;
		pMsg->m_ModelID = 1;// packet.m_type;
		pMsg->m_nID = packet.id;
		pMsg->m_eTarget = EObjTarget_SC::eMissile;
		pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);

		pMsg->setMovementMsg(new CMsg_Movement_PosAndDir(
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z),
			DirectX::XMFLOAT3(packet.look_x, packet.look_y, packet.look_z)
		));

		pResult = pMsg;
		break;
	}
	case SC_ENEMY_CREATE:
	{
		sc_packet_ecreate packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_CreateObj* pMsg = new CMsg_SC_CreateObj;

		pMsg->m_ModelID = packet.m_type;
		pMsg->m_nID = packet.id;
		pMsg->m_eTarget = EObjTarget_SC::eEnemy;
		pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);

		//체크
		pMsg->setMovementMsg(new CMsg_Movement_PosAndDir(
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z),
//			DirectX::XMFLOAT3(0, 0, 1)
			DirectX::XMFLOAT3(packet.look_x, packet.look_y, packet.look_z)
		));
		pResult = pMsg;
		break;
	}
	case SC_OBJECT_CREATE:
	{
		//üũ
		sc_packet_ocreate packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;

		pMsg->m_nID = packet.id;
		pMsg->m_eTarget = EObjTarget_SC::eBaseCamp;
		pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);

		//체크
		pMsg->setMovementMsg(new CMsg_Movement_PosAndDir(
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z),
			DirectX::XMFLOAT3(packet.look_x, packet.look_y, packet.look_z)
//			DirectX::XMFLOAT3(0, 0, 1)
//			DirectX::XMFLOAT3(packet.look_x, packet.look_y, packet.look_z)
		));


		pResult = pMsg;

		break;
	}
	case SC_OBJECT_MOVE:
	{
		//üũ
		sc_packet_omove packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;

		pMsg->m_nID = packet.id;
		pMsg->m_eTarget = EObjTarget_SC::eBaseCamp;
		pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);

		pMsg->setMovementMsg(new CMsg_Movement_PosAndDir(
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z),
			DirectX::XMFLOAT3(packet.look_x, packet.look_y, packet.look_z)
//			DirectX::XMFLOAT3(0, 0, 1)
		));


		pResult = pMsg;

		break;
	}
	case SC_BOSS_CREATE:
	{
		//üũ
		sc_packet_bcreate packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;

		pMsg->m_nID = packet.id;
		pMsg->m_eTarget = EObjTarget_SC::eBoss;
		pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);

		pMsg->setMovementMsg(new CMsg_Movement_PosAndDir(
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z),
			DirectX::XMFLOAT3(packet.look_x, packet.look_y, packet.look_z)
			//			DirectX::XMFLOAT3(0, 0, 1)
		));


		pResult = pMsg;
		break;
	}
	case SC_BOSS_MOVE:
	{	
		sc_packet_bmove packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_Move* pMsg = new CMsg_SC_Move;

		pMsg->m_nID = packet.id;
		pMsg->m_eTarget = EObjTarget_SC::eBoss;
		switch (packet.move)
		{
		case object_translate::OBJECT_JUMP:
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eJump);
			break;
		case object_translate::OBJECT_BOOST:
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eBoost);
			break;
		case object_translate::OBJECT_STOP:
			pMsg->onFlag(CMsg_SC_Move::EMoveFlag::eStop);
			break;
		}

		pMsg->setMovementMsg(new CMsg_Movement_Vector3(
			EMsgType_Movement::eReplace_Position,
			DirectX::XMFLOAT3(packet.pos_x, packet.pos_y, packet.pos_z)
			//			DirectX::XMFLOAT3(0, 0, 1)
		));


		pResult = pMsg;
		break;
	}
	case SC_PLAYER_DOWN:
	{		
		sc_packet_pdown packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_PlayerDown* pMsg = new CMsg_SC_PlayerDown();
		pMsg->m_nID = packet.id;
		pResult = pMsg;
		break;
	}
	case SC_SCRIPT:
	{
		sc_packet_script packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_DrawScript* pMsg = new CMsg_SC_DrawScript(packet.string_id);

		pResult = pMsg;
		break;
	}
	case SC_HP:
	{
		sc_packet_HP packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		CMsg_SC_HP* pMsg = new CMsg_SC_HP((packet.hp/100.0f)); 
		switch (packet.obj_type)
		{
		case object_Type::OBJECT_PLAYER:
			pMsg->m_eObjType = EObjTarget_SC::ePlayer;
			break;
		case object_Type::OBJECT_ENEMY:
			pMsg->m_eObjType = EObjTarget_SC::eEnemy;
			break;
		default:
//		case object_Type::OBJECT_MISSILE:
			pMsg->m_eObjType = EObjTarget_SC::eMissile;
			break;
		}
		pMsg->m_nID = packet.id;


		pResult = pMsg;
		break;
	}

	case SC_ROOMLIST:
	{
		sc_packet_room_list packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);

		std::vector<CMsg_SC_RoomList::RoomData> roomDatas;
		CMsg_SC_RoomList::RoomData tmp;
		for (auto i = 0U; i < packet.list_size; ++i)
		{
			tmp.m_nRoomID = packet.rlist[i].roomid;
			tmp.m_nTitleID = packet.rlist[i].title_id;
			tmp.m_nNumberOfPlayer = packet.rlist[i].curp;
			roomDatas.push_back(tmp);
		}


		CMsg_SC_RoomList* pMsg = new CMsg_SC_RoomList(roomDatas);

		pResult = pMsg;
		break;
	}

	case SC_INIT_ROOMDATA:
	{
		sc_packet_room_data packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);

		std::array<CMsg_SC_RoomData::PlayerData,4> PlayerDatas;
		for (int i = 0; i < 4; ++i)
		{
			if (packet.user[i].id == 65535)
			{
				PlayerDatas[i].m_bOn = false;
				continue;
			}
			PlayerDatas[i].m_nID = packet.user[i].id;
			PlayerDatas[i].m_nMecha = packet.user[i].mecha;
			PlayerDatas[i].m_bReady = (packet.user[i].is_ready!=0);
		}
		
		CMsg_SC_RoomData* pMsg = new CMsg_SC_RoomData(PlayerDatas);
		pMsg->m_nID = packet.roomid;

		pResult = pMsg;

		break;
	}
	case SC_IS_ENTER_ROOM:
	{
		sc_packet_enter_room packet;
		memcpy(&packet, rBytes.data(), rBytes[0]);
		 

		CMsg_SC_EnterRoom* pMsg = new CMsg_SC_EnterRoom();
		pMsg->m_nID = packet.result;

		pResult = pMsg;
		  
		break;
	}

	}

	rBytes = std::vector<char>(rBytes.begin() + rBytes[0], rBytes.end());

	return pResult;
}
