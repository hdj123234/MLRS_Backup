#pragma once

#ifndef HEADER_FACTORY_DXTEXTURE_FORMDATA
#define HEADER_FACTORY_DXTEXTURE_FORMDATA

#include"Factory_DXTexture.h"

#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"GlobalDirectXStorage.h"

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL

#ifndef HEADER_DIRECTX
#include<D3DX11.h>
#endif

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif

template<class TYPE>
class CFactory_Texture1D_FromData : public CFactory_Texture1D {
private:
	const std::vector<TYPE> &m_rData;

	//override CFactory_Texture1D
	ID3D11Texture1D *createTexture1D_RealProcessing()
	{
		ID3D11Texture1D *pResult = nullptr;
		D3D11_SUBRESOURCE_DATA data;
		ZeroMemory(&data, sizeof(data));

		data.pSysMem = static_cast<void const *>(m_rData.data());
		data.SysMemPitch = m_TextureDesc.Width *sizeof(TYPE);
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateTexture1D(&m_TextureDesc, &data, &pResult)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_Texture1D_FromData.createTexture1D_RealProcessing(), CreateTexture1D ERROR");
			return nullptr;
		}
		else
			return pResult;
	}
public:
	CFactory_Texture1D_FromData(const std::string &rsName, const std::vector<TYPE> &rData, const  DXGI_FORMAT format, const  UINT bindFlags)
		:CFactory_Texture1D(rsName,Texture1DDesc_Default()),
		m_rData(rData)
	{
		m_TextureDesc.Format = format;
		m_TextureDesc.BindFlags = bindFlags;
		UINT nSize = static_cast<UINT>(rData.size());
		m_TextureDesc.Width = nSize;
	}
	virtual ~CFactory_Texture1D_FromData() {}

};

template<class TYPE>
class CFactory_Texture2D_FromData : public CFactory_Texture2D {
private:
	const std::vector<TYPE> &m_rData;

	//override CFactory_Texture2D
	ID3D11Texture2D *createTexture2D_RealProcessing()
	{
		ID3D11Texture2D *pResult = nullptr;
		D3D11_SUBRESOURCE_DATA data;
		ZeroMemory(&data, sizeof(data));

		data.pSysMem = static_cast<void const *>(m_rData.data());
		data.SysMemPitch = m_TextureDesc.Width *sizeof(TYPE);
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateTexture2D(&m_TextureDesc, &data, &pResult)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_Texture2D_FromData.createTexture2D_RealProcessing(), CreateTexture2D ERROR");
			return nullptr;
		}
		else
			return pResult;
	}
public:
	CFactory_Texture2D_FromData(
		const std::string &rsName,
		const std::vector<TYPE> &rData,
		const  DXGI_FORMAT format,
		const  UINT bindFlags,
		unsigned int nWidth = 0,
		unsigned int nHeight = 0,
		D3D11_USAGE usage = D3D11_USAGE::D3D11_USAGE_DEFAULT,
		UINT cpuAccenssFlag = NULL)
		:CFactory_Texture2D(rsName),
		m_rData(rData)
	{
		m_TextureDesc.Format = format;
		m_TextureDesc.BindFlags = bindFlags;
		m_TextureDesc.Usage= usage;
		m_TextureDesc.CPUAccessFlags= cpuAccenssFlag;
		if ((nWidth == 0 && nHeight == 0) ||
			(nWidth *nHeight != rData.size()))
			m_TextureDesc.Height = m_TextureDesc.Width = static_cast<UINT>(sqrt(rData.size()));
		else
		{
			m_TextureDesc.Width = nWidth;
			m_TextureDesc.Height = nHeight;
		}
	}
};

#endif