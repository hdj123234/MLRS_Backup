
#include"stdafx.h"
#include "Factory_DXTexture.h"

#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"
#include"GlobalDirectXStorage.h"

#include"..\MyUtility.h"

#ifndef HEADER_STL
#include<iostream>
#endif

#ifndef HEADER_DIRECTX
#include<D3DX11.h>
#endif

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif

const EImageType CUtility_ImageTypeGetter::getImageType(const std::string & rsFileName)
{
	std::string sExpension;
	std::transform(rsFileName.end() - 4, rsFileName.end(), std::back_inserter(sExpension), tolower);
	if (sExpension == ".tga")
		return  eTGA;
	else if (sExpension == ".dds")
		return eDDS;
	else
		return eWIC;
}

//----------------------------------- Texture1DDesc_Default -------------------------------------------------------------------------------------

Texture1DDesc_Default::Texture1DDesc_Default(
	UINT nSize,
	DXGI_FORMAT Format,
	UINT bindFlags,
	UINT accessFlags, 
	D3D11_USAGE usage)
{
	D3D11_TEXTURE1D_DESC::MipLevels = 1;
	D3D11_TEXTURE1D_DESC::ArraySize = 1;
	D3D11_TEXTURE1D_DESC::Usage = usage;
	D3D11_TEXTURE1D_DESC::CPUAccessFlags = accessFlags;
	D3D11_TEXTURE1D_DESC::MiscFlags = 0;

	D3D11_TEXTURE1D_DESC::Width = nSize;

	D3D11_TEXTURE1D_DESC::Format = Format;
	D3D11_TEXTURE1D_DESC::BindFlags = bindFlags;
}

//----------------------------------- Texture2DDesc_Default -------------------------------------------------------------------------------------

Texture2DDesc_Default::Texture2DDesc_Default(
	DXGI_FORMAT Format, 
	UINT bindFlags,
	UINT nWidth,
	UINT nHeight,
	UINT accessFlags,
	D3D11_USAGE usage)
{
	D3D11_TEXTURE2D_DESC::MipLevels = 1;
	D3D11_TEXTURE2D_DESC::ArraySize = 1;
	D3D11_TEXTURE2D_DESC::SampleDesc.Count = 1;
	D3D11_TEXTURE2D_DESC::SampleDesc.Quality = 0;
	D3D11_TEXTURE2D_DESC::Usage = usage;
	D3D11_TEXTURE2D_DESC::CPUAccessFlags = accessFlags;
	D3D11_TEXTURE2D_DESC::MiscFlags = 0;

	D3D11_TEXTURE2D_DESC::Width = nWidth;
	D3D11_TEXTURE2D_DESC::Height = nHeight;

	D3D11_TEXTURE2D_DESC::Format = Format;
	D3D11_TEXTURE2D_DESC::BindFlags = bindFlags;
}


//----------------------------------- ImageLoadInfo_Default -------------------------------------------------------------------------------------

ImageLoadInfo_Default::ImageLoadInfo_Default(DXGI_FORMAT format, UINT bindFlags, D3D11_USAGE usage, UINT accessFlags, UINT filter, UINT mipFilter)
{
	D3DX11_IMAGE_LOAD_INFO::Width = D3DX11_DEFAULT;
	D3DX11_IMAGE_LOAD_INFO::Height = D3DX11_DEFAULT;
	D3DX11_IMAGE_LOAD_INFO::Depth = D3DX11_DEFAULT;
	D3DX11_IMAGE_LOAD_INFO::FirstMipLevel = 0;
	D3DX11_IMAGE_LOAD_INFO::MipLevels = D3DX11_DEFAULT;
	D3DX11_IMAGE_LOAD_INFO::Usage = usage;
	D3DX11_IMAGE_LOAD_INFO::BindFlags = bindFlags;
	D3DX11_IMAGE_LOAD_INFO::CpuAccessFlags = accessFlags;
	D3DX11_IMAGE_LOAD_INFO::MiscFlags = 0;
	D3DX11_IMAGE_LOAD_INFO::Format = format;
	D3DX11_IMAGE_LOAD_INFO::Filter = filter;
	D3DX11_IMAGE_LOAD_INFO::MipFilter = mipFilter;
	D3DX11_IMAGE_LOAD_INFO::pSrcInfo = 0;
}

//----------------------------------- ImageLoadInfo_FromTextureArray -------------------------------------------------------------------------------------

ImageLoadInfo_FromTextureArray::ImageLoadInfo_FromTextureArray(DXGI_FORMAT format)
{

	D3DX11_IMAGE_LOAD_INFO::Width = D3DX11_FROM_FILE;
	D3DX11_IMAGE_LOAD_INFO::Height = D3DX11_FROM_FILE;
	D3DX11_IMAGE_LOAD_INFO::Depth = D3DX11_FROM_FILE;
	D3DX11_IMAGE_LOAD_INFO::FirstMipLevel = 0;
	D3DX11_IMAGE_LOAD_INFO::MipLevels = D3DX11_DEFAULT;
	D3DX11_IMAGE_LOAD_INFO::Usage = D3D11_USAGE::D3D11_USAGE_STAGING;
	D3DX11_IMAGE_LOAD_INFO::BindFlags = 0;
	D3DX11_IMAGE_LOAD_INFO::CpuAccessFlags = D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_READ;
	D3DX11_IMAGE_LOAD_INFO::MiscFlags = 0;
	D3DX11_IMAGE_LOAD_INFO::Format = format;
	D3DX11_IMAGE_LOAD_INFO::Filter = D3DX11_FILTER_NONE;
	D3DX11_IMAGE_LOAD_INFO::MipFilter = D3DX11_FILTER_LINEAR;
	D3DX11_IMAGE_LOAD_INFO::pSrcInfo = 0;
}

//----------------------------------- CFactory_Texture1D -------------------------------------------------------------------------------------

CFactory_Texture1D::CFactory_Texture1D(
	const std::string & rsName,
	const D3D11_TEXTURE1D_DESC & rTextureDesc)
	:m_sName(rsName),
	m_TextureDesc(rTextureDesc)
{
}

ID3D11Texture1D * CFactory_Texture1D::createTexture1D_RealProcessing()
{
	ID3D11Texture1D *pResult;
	if (FAILED(CGlobalDirectXDevice::getDevice()->CreateTexture1D(&m_TextureDesc, NULL, &pResult)))		return nullptr;
	else	return pResult;
}

ID3D11Texture1D * CFactory_Texture1D::createTexture1D()
{
	if (CGlobalDirectXStorage::checkTexture1D(m_sName))
		return CGlobalDirectXStorage::getTexture1D(m_sName);

	ID3D11Texture1D *pResult = this->createTexture1D_RealProcessing();

	CGlobalDirectXStorage::setTexture1D(m_sName, pResult);
	return pResult;
}

//----------------------------------- AFactory_Texture2D -------------------------------------------------------------------------------------

AFactory_Texture2D::AFactory_Texture2D(const std::string & rsName)
	: m_sName(rsName)
{
}

ID3D11Texture2D * AFactory_Texture2D::createTexture2D()
{
	if (CGlobalDirectXStorage::checkTexture2D(m_sName))
		return CGlobalDirectXStorage::getTexture2D(m_sName);

	ID3D11Texture2D *pResult = this->createTexture2D_RealProcessing();

	CGlobalDirectXStorage::setTexture2D(m_sName, pResult);
	return pResult;
}


//----------------------------------- CFactory_Texture2D -------------------------------------------------------------------------------------

CFactory_Texture2D::CFactory_Texture2D(const std::string& rsName, const D3D11_TEXTURE2D_DESC & rTextureDesc)
	:AFactory_Texture2D(rsName),
	m_TextureDesc(rTextureDesc)
{
}

ID3D11Texture2D * CFactory_Texture2D::createTexture2D_RealProcessing()
{
	ID3D11Texture2D *pResult;
	if (FAILED(CGlobalDirectXDevice::getDevice()->CreateTexture2D(&m_TextureDesc, NULL, &pResult)))		return nullptr;
	else	return pResult;
}

//----------------------------------- CFactory_Texture2D_FromFile -------------------------------------------------------------------------------------

CFactory_Texture2D_FromFile::CFactory_Texture2D_FromFile(
	const std::string & rsFileName,
	const UINT bindFlags, 
	const D3D11_USAGE usage, 
	const UINT accessFlags, 
	const UINT miscFlag, 
	const bool bForceSRGB)
	:AFactory_Texture2D("TX2D_" + rsFileName),
	m_sFileName(rsFileName),
	m_BindFlags(bindFlags),
	m_Usage(usage),
	m_CPUAccessFlags(accessFlags),
	m_MiscFlag(miscFlag),
	m_bForceSRGB(bForceSRGB),
	m_eImageType(CUtility_ImageTypeGetter::getImageType(rsFileName))
{
}

ID3D11Texture2D * CFactory_Texture2D_FromFile::createTexture2D_RealProcessing()
{
	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
	ID3D11Texture2D * pTexture = nullptr;
	DirectX::ScratchImage scratchImage;
	DWORD flags = 0 ;

	std::string sFileName = m_sFileName;
	if (sFileName.back() == '\r')
		sFileName.pop_back();
//	int n;
	switch (m_eImageType)
	{
	case eTGA:
		if (FAILED(DirectX::LoadFromTGAFile(CUtility_String::convertToWString(g_sDataFolder + sFileName).data(),
			&m_MetaData,	scratchImage)))
		{
			abort();
			return nullptr;
		}
		
//		n = *reinterpret_cast<int*>(scratchImage.GetImages()->pixels);

		break;
	case eDDS:
		if (FAILED(DirectX::LoadFromDDSFile(CUtility_String::convertToWString(g_sDataFolder + sFileName).data(),
			flags,	&m_MetaData,	scratchImage)))
		{
			abort();
			return nullptr;
		}
		break;
	case eWIC:
		if (FAILED(DirectX::LoadFromWICFile(CUtility_String::convertToWString(g_sDataFolder + sFileName).data(),
			flags, &m_MetaData, scratchImage)))
		{
			abort();
			return nullptr;
		}
		break;
	}
	if (FAILED(DirectX::CreateTextureEx(pDevice, scratchImage.GetImages(), scratchImage.GetImageCount(), m_MetaData, 
		m_Usage, m_BindFlags, m_CPUAccessFlags, m_MiscFlag,m_bForceSRGB,reinterpret_cast<ID3D11Resource **>(&pTexture))))
	{
		abort();
		return nullptr;
	}
	scratchImage.Release();
	return pTexture;


	//std::wstring name = L"asd0013.tga";
	//DirectX::LoadFromTGAFile()

	//if(FAILED(D3DX11CreateTextureFromFile(pDevice,
	//	,
	//									&m_ImageLoadInfo,
	//									NULL,
	//									reinterpret_cast<ID3D11Resource **>(&pTexture),
	//									NULL)))
	//	if (FAILED(D3DX11CreateTextureFromFile(pDevice,
	//		s_stringConverter.from_bytes(g_sDataFolder + m_sFileName).data(),
	//		&m_ImageLoadInfo,
	//		NULL,
	//		reinterpret_cast<ID3D11Resource **>(&pTexture),
	//		NULL)))
	//	{
	//	return nullptr;
	//}
	//return pTexture;
}


//----------------------------------- CFactory_Texture2DArray_FromFiles -------------------------------------------------------------------------------------

CFactory_Texture2DArray_FromFiles::CFactory_Texture2DArray_FromFiles(
	const std::string & rsName,
	const std::vector<std::string>& rsFileNames,
	const UINT bindFlags,
	const D3D11_USAGE usage,
	const UINT accessFlags)
	:AFactory_Texture2D("TX2DArray_" + rsName),
	m_sFileNames(rsFileNames) 
{
	ZeroMemory(&m_TextureDesc, sizeof(m_TextureDesc));
	m_TextureDesc.Usage = usage;
	m_TextureDesc.CPUAccessFlags = accessFlags;
	m_TextureDesc.BindFlags = bindFlags;
	m_TextureDesc.MipLevels = 1;
	m_TextureDesc.SampleDesc.Count = 1;
	m_TextureDesc.SampleDesc.Quality = 0;
}

ID3D11Texture2D * CFactory_Texture2DArray_FromFiles::createTexture2D_RealProcessing()
{
	std::vector<ID3D11Texture2D *> pTextures;
	std::vector<DirectX::TexMetadata> imageInfos;
	for (auto &rData : m_sFileNames)
	{
		CFactory_Texture2D_FromFile factory(rData,NULL,D3D11_USAGE::D3D11_USAGE_STAGING,D3D11_CPU_ACCESS_READ);
		ID3D11Texture2D *pTexture = factory.createTexture2D();
		if (!pTexture)
			return nullptr;
		pTextures.push_back(pTexture);
		imageInfos.push_back(factory.getImageInfo());
	}
	DirectX::TexMetadata imageInfo;
	ZeroMemory(&imageInfo, sizeof(imageInfo));
	imageInfo = imageInfos[0];

	for (auto &rData : imageInfos)
	{
		if (!(imageInfo.format == rData.format
			&&	imageInfo.height == rData.height
			&&	imageInfo.width == rData.width
			&&	imageInfo.mipLevels == rData.mipLevels))
		{
			abort();
			return nullptr;
		}
	}

	m_TextureDesc.ArraySize = static_cast<UINT>( pTextures.size());
	m_TextureDesc.MipLevels =	static_cast<UINT>(imageInfo.mipLevels	);
	m_TextureDesc.Width =		static_cast<UINT>(imageInfo.width		);
	m_TextureDesc.Height =		static_cast<UINT>(imageInfo.height		);
	m_TextureDesc.Format = imageInfo.format;

	ID3D11Texture2D* pTexture  = nullptr;
	if (FAILED(CGlobalDirectXDevice::getDevice()->CreateTexture2D(&m_TextureDesc, NULL, &pTexture)))
	{
		abort();
		return nullptr;
	}
	ID3D11DeviceContext *pDeviceContext =  CGlobalDirectXDevice::getDeviceContext();

	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	const unsigned int nNumberOfTexture = static_cast<const unsigned int>(pTextures.size());
	for (unsigned int i = 0; i < nNumberOfTexture; ++i)
	{
		for (unsigned int j = 0; j < imageInfos[i].mipLevels; ++j)
		{
			pDeviceContext->Map(pTextures[i], j, D3D11_MAP::D3D11_MAP_READ, NULL, &mappedSubresource);
//			int n = *reinterpret_cast<int*>(mappedSubresource.pData);
			pDeviceContext->UpdateSubresource(pTexture,D3D11CalcSubresource(j,i, m_TextureDesc.MipLevels) ,NULL , mappedSubresource.pData, mappedSubresource.RowPitch, mappedSubresource.DepthPitch);
			pDeviceContext->Unmap(pTextures[i], j);
		}

	}

	for (auto &rData : m_sFileNames)
		CGlobalDirectXStorage::deleteTexture2D("TX2D_"+rData);

	return pTexture;

}

