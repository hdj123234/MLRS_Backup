#include"stdafx.h"
#include "Factory_DXShaderResourceView.h"
#include "Factory_DXTexture.h"

#include"..\MyUtility.h"

#ifndef HEADER_DIRECTXTEX
#include<DirectXTex\DirectXTex.h>
#endif

#ifndef HEADER_DIRECTX
#include<D3DX11.h>
#endif

#ifndef HEADER_STL
#include<vector>
#include<string>
#include<locale>
#include <codecvt>
#endif


#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT



//----------------------------------- AFactory_ShaderResourceView -------------------------------------------------------------------------------------

AFactory_ShaderResourceView::AFactory_ShaderResourceView(const std::string & rsName)
	:m_sName(rsName)
{
}

ID3D11ShaderResourceView * AFactory_ShaderResourceView::createShaderResourceView()
{
	if (CGlobalDirectXStorage::checkShaderResourceView(m_sName))
		return CGlobalDirectXStorage::getShaderResourceView(m_sName);

	ID3D11ShaderResourceView *pShaderResourceView = this->createShaderResourceView_RealProcessing();

	if (!pShaderResourceView)	return nullptr;

	CGlobalDirectXStorage::setShaderResourceView(m_sName, pShaderResourceView);
	return pShaderResourceView;
}

//----------------------------------- CFactory_ShaderResourceView_FromTextureFile -------------------------------------------------------------------------------------

CFactory_ShaderResourceView_FromTextureFile::CFactory_ShaderResourceView_FromTextureFile(
	const std::string & rsName, 
	const std::string & rsFileName, 
	const UINT bindFlags,
	const D3D11_USAGE usage, 
	const UINT cpuAccessFlags, 
	const UINT miscFlag, 
	const bool bForceSRGB)
	:AFactory_ShaderResourceView(rsName), 
	m_sFileName(rsFileName),
	m_BindFlags(bindFlags),
	m_Usage(usage),
	m_CPUAccessFlags(cpuAccessFlags),
	m_MiscFlag(miscFlag),
	m_bForceSRGB(bForceSRGB),
	m_eImageType(CUtility_ImageTypeGetter::getImageType(rsFileName))
{
}
ID3D11ShaderResourceView * CFactory_ShaderResourceView_FromTextureFile::createShaderResourceView_RealProcessing()
{

	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
//	ID3D11Texture2D * pTexture = nullptr;
	ID3D11ShaderResourceView *pShaderResourceView = nullptr;
	DirectX::ScratchImage scratchImage;
	DWORD flags = 0;
	//	int n;
	switch (m_eImageType)
	{
	case eTGA:
		if (FAILED(DirectX::LoadFromTGAFile(CUtility_String::convertToWString(g_sDataFolder + m_sFileName).data(),
			&m_MetaData, scratchImage)))
		{
			abort();
			return nullptr;
		}

		//		n = *reinterpret_cast<int*>(scratchImage.GetImages()->pixels);

		break;
	case eDDS:
		if (FAILED(DirectX::LoadFromDDSFile(CUtility_String::convertToWString(g_sDataFolder + m_sFileName).data(),
			flags, &m_MetaData, scratchImage)))
		{
			abort();
			return nullptr;
		}
		break;
	case eWIC:
		if (FAILED(DirectX::LoadFromWICFile(CUtility_String::convertToWString(g_sDataFolder + m_sFileName).data(),
			flags, &m_MetaData, scratchImage)))
		{
			abort();
			return nullptr;
		}
		break;
	}

	if (FAILED(DirectX::CreateShaderResourceViewEx(pDevice, scratchImage.GetImages(), scratchImage.GetImageCount(), m_MetaData,
		m_Usage, m_BindFlags, m_CPUAccessFlags, m_MiscFlag, m_bForceSRGB, &pShaderResourceView)))
	{
		abort();
		return nullptr;
	}
	scratchImage.Release();

	return pShaderResourceView;
}



//----------------------------------- CFactory_ShaderResourceView_FromTexture2D -------------------------------------------------------------------------------------

ID3D11ShaderResourceView * CFactory_ShaderResourceView_FromTexture2D::createShaderResourceView_RealProcessing()
{
	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
	ID3D11ShaderResourceView *pShaderResourceView;

	D3D11_SHADER_RESOURCE_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Format = m_Format;
	desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	desc.Texture2D.MipLevels = 1; 
	if (FAILED(pDevice->CreateShaderResourceView(m_pTexture2D, &desc, &pShaderResourceView)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_ShaderResourceView_FromTexture2D.createShaderResourceView_RealProcessing() CreateShaderResourceView() Error");
		return nullptr;
	}
	else
		return pShaderResourceView;
}

CFactory_ShaderResourceView_FromTexture2D::CFactory_ShaderResourceView_FromTexture2D(const std::string &rsName,ID3D11Texture2D * pTexture2D, D3D11_TEXTURE2D_DESC &rTexture2DDesc)
	:AFactory_ShaderResourceView(rsName),
	m_pTexture2D(pTexture2D),
	m_Format(rTexture2DDesc.Format)
{
}

CFactory_ShaderResourceView_FromTexture2D::CFactory_ShaderResourceView_FromTexture2D(const std::string & rsName, ID3D11Texture2D * pTexture2D, DXGI_FORMAT format)
	:AFactory_ShaderResourceView(rsName),
	m_pTexture2D(pTexture2D),
	m_Format(format)
{
}

//----------------------------------- CFactory_ShaderResourceView_FromBuffer -------------------------------------------------------------------------------------

ID3D11ShaderResourceView * CFactory_ShaderResourceView_FromBuffer::createShaderResourceView_RealProcessing()
{
	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
	ID3D11ShaderResourceView *pShaderResourceView;

	D3D11_SHADER_RESOURCE_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Format = m_Format;
	desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	desc.Buffer.ElementOffset = 0;
	desc.Buffer.NumElements = m_nNumberOfElement;
	if (FAILED(pDevice->CreateShaderResourceView(m_pBuffer, &desc, &pShaderResourceView)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_ShaderResourceView_FromBuffer.createShaderResourceView_RealProcessing() CreateShaderResourceView() Error");
		return nullptr;
	}
	else
		return pShaderResourceView;
}


CFactory_ShaderResourceView_FromBuffer::CFactory_ShaderResourceView_FromBuffer(
	const std::string & rsName, 
	ID3D11Buffer * pBuffer,
	DXGI_FORMAT format, 
	const unsigned int nNumberOfElement)
	:AFactory_ShaderResourceView(rsName),
	m_pBuffer(pBuffer),
	m_Format(format),
	m_nNumberOfElement(nNumberOfElement)
{
}

//----------------------------------- CFactory_ShaderResourceView_FromTextureFiles -------------------------------------------------------------------------------------

CFactory_ShaderResourceView_FromTextureFiles::CFactory_ShaderResourceView_FromTextureFiles(
	const std::string & rsName, 
	const std::vector<std::string>& rsFileNames)
	:AFactory_ShaderResourceView(rsName),
	m_sName(rsName),
	m_sFileNames(rsFileNames)
{
}

ID3D11ShaderResourceView * CFactory_ShaderResourceView_FromTextureFiles::createShaderResourceView_RealProcessing()
{
	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
	ID3D11ShaderResourceView *pShaderResourceView;

	CFactory_Texture2DArray_FromFiles factory(m_sName, m_sFileNames);
	ID3D11Texture2D *pTexture = factory.createTexture2D();
	D3D11_TEXTURE2D_DESC desc_TX2D = factory.getDesc();
	D3D11_SHADER_RESOURCE_VIEW_DESC desc_SRV;
	ZeroMemory(&desc_SRV, sizeof(desc_SRV));
	desc_SRV.Format = desc_TX2D.Format;
	desc_SRV.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
	desc_SRV.Texture2DArray.ArraySize = desc_TX2D.ArraySize;
	desc_SRV.Texture2DArray.MostDetailedMip = 0;
	desc_SRV.Texture2DArray.MipLevels = 1;
	desc_SRV.Texture2DArray.FirstArraySlice = 0;
	
	if (FAILED(pDevice->CreateShaderResourceView(pTexture, &desc_SRV, &pShaderResourceView)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_ShaderResourceView_FromBuffer.createShaderResourceView_RealProcessing() CreateShaderResourceView() Error");
		return nullptr;
	}
	return pShaderResourceView;
}
