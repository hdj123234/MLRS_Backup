#pragma once
#ifndef HEADER_FACTORY_DXSHADER
#define HEADER_FACTORY_DXSHADER

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif // !HEADER_DIRECTX


class AFactory_Shader  	  {
protected:
	const std::string m_sModel;

	std::string m_sFileName;
	std::string m_sFunctionName;
	std::vector<BYTE> m_ByteCode;

	static std::string s_sFilenameExtension;

	bool loadByteCode();
public:
	AFactory_Shader(const std::string &rsFileName, const std::string &rsFunctionName, const std::string &rsShaderModel);
	virtual ~AFactory_Shader() { AFactory_Shader::release(); }

	void release(){	m_ByteCode.clear();}
};


class	CFactory_VertexShader : public AFactory_Shader {
public:
	CFactory_VertexShader(const std::string &rsFileName, const std::string&rsFunctionName);
	virtual ~CFactory_VertexShader() { }

	ID3D11VertexShader *createShader();
};


class	CFactory_HullShader : public AFactory_Shader {
public:
	CFactory_HullShader(const std::string &rsFileName, const std::string &rsFunctionName);
	virtual ~CFactory_HullShader() { }

	ID3D11HullShader *createShader();
};

class	CFactory_DomainShader : public AFactory_Shader {
public:
	CFactory_DomainShader(const std::string &rsFileName, const std::string &rsFunctionName);
	virtual ~CFactory_DomainShader() { }

	ID3D11DomainShader *createShader();
};

class	CFactory_GeometryShader : public AFactory_Shader {
public:
	CFactory_GeometryShader(const std::string &rsFileName, const std::string &rsFunctionName);
	virtual ~CFactory_GeometryShader() { }

	ID3D11GeometryShader *createShader();
};

class	CFactory_GeometryShaderForStreamOutput : public AFactory_Shader {
private:
	std::vector<D3D11_SO_DECLARATION_ENTRY > m_SODeclarations;
	std::vector<UINT > m_nBufferStrides;
	UINT m_nRasterizerStream;
public:
	CFactory_GeometryShaderForStreamOutput(const std::string &rsFileName, const std::string &rsFunctionName, std::vector<D3D11_SO_DECLARATION_ENTRY > soDeclarations,std::vector<UINT > nBufferStrides,	UINT nRasterizerStream = D3D11_SO_NO_RASTERIZED_STREAM);
	virtual ~CFactory_GeometryShaderForStreamOutput() { }

	ID3D11GeometryShader *createShader();
};


class CFactory_PixelShader :	public AFactory_Shader {
public:
	CFactory_PixelShader(const std::string &rsFileName, const std::string &rsFunctionName);
	virtual ~CFactory_PixelShader() {}

	ID3D11PixelShader *createShader();
};


class CFactory_InputLayout : public AFactory_Shader {
private:
	std::string m_sName;
	std::vector<D3D11_INPUT_ELEMENT_DESC> m_InputElementDescs;
public:
	CFactory_InputLayout(const std::string &rsFileName, const std::string &rsFunctionName, const std::string &rsName, std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs);
	virtual ~CFactory_InputLayout() {}

	virtual ID3D11InputLayout *createInputLayout();
};

#endif