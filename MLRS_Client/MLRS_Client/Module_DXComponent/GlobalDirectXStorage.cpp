#include"stdafx.h"
#include "GlobalDirectXStorage.h"

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif

using std::string;

std::map<string, ID3D11VertexShader *>			CGlobalDirectXStorage::s_pVSs;
std::map<string, ID3D11HullShader *>			CGlobalDirectXStorage::s_pHSs;
std::map<string, ID3D11DomainShader *>			CGlobalDirectXStorage::s_pDSs;
std::map<string, ID3D11GeometryShader *>		CGlobalDirectXStorage::s_pGSs;
std::map<string, ID3D11PixelShader *>			CGlobalDirectXStorage::s_pPSs;
std::map<string, ID3D11Buffer *>				CGlobalDirectXStorage::s_pBuffers;
std::map<string, ID3D11Texture1D *>				CGlobalDirectXStorage::s_pTextures_1D;
std::map<string, ID3D11Texture2D *>				CGlobalDirectXStorage::s_pTextures_2D;
std::map<string, ID3D11ShaderResourceView *>	CGlobalDirectXStorage::s_pShaderResourceViews;
std::map<string, ID3D11SamplerState *>			CGlobalDirectXStorage::s_pSamplerStates;
std::map<string, ID3D11InputLayout *>			CGlobalDirectXStorage::s_pInputLayouts;
std::map<string, ID3D11RasterizerState *>		CGlobalDirectXStorage::s_pRasterizerStates;
std::map<string, ID3D11BlendState*>				CGlobalDirectXStorage::s_pBlendStates;
std::map<string, ID3D11DepthStencilState*>		CGlobalDirectXStorage::s_pDepthStencilStates;
std::map<string, ID3D11DepthStencilView*>		CGlobalDirectXStorage::s_pDepthStencilViews;
std::map<string, ID3D11RenderTargetView*>		CGlobalDirectXStorage::s_pRenderTargetViews;



ID3D11VertexShader * CGlobalDirectXStorage::getVertexShader(string const & rsName)
{
	auto &container = s_pVSs;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11HullShader * CGlobalDirectXStorage::getHullShader(const std::string & rsName)
{
	auto &container = s_pHSs;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11DomainShader * CGlobalDirectXStorage::getDomainShader(const std::string & rsName)
{
	auto &container = s_pDSs;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11GeometryShader * CGlobalDirectXStorage::getGeometryShader(string const & rsName)
{
	auto &container = s_pGSs;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11PixelShader * CGlobalDirectXStorage::getPixelShader(string const & rsName)
{
	auto &container = s_pPSs;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11Buffer * CGlobalDirectXStorage::getBuffer(string const & rsName)
{
	auto &container = s_pBuffers;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11Texture1D * CGlobalDirectXStorage::getTexture1D(const std::string & rsName)
{
	auto &container = s_pTextures_1D;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11Texture2D * CGlobalDirectXStorage::getTexture2D(string const & rsName)
{
	auto &container = s_pTextures_2D;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11ShaderResourceView * CGlobalDirectXStorage::getShaderResourceView(string const & rsName)
{
	auto &container = s_pShaderResourceViews;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11SamplerState * CGlobalDirectXStorage::getSamplerState(string const & rsName)
{
	auto &container = s_pSamplerStates;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11InputLayout * CGlobalDirectXStorage::getInputLayout(string const & rsName)
{
	auto &container = s_pInputLayouts;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11RasterizerState * CGlobalDirectXStorage::getRasterizerState(const std::string & rsName)
{
	auto &container = s_pRasterizerStates;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11BlendState * CGlobalDirectXStorage::getBlendState(const std::string & rsName)
{
	auto &container = s_pBlendStates;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11DepthStencilState * CGlobalDirectXStorage::getDepthStencilState(const std::string & rsName)
{
	auto &container = s_pDepthStencilStates;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11DepthStencilView * CGlobalDirectXStorage::getDepthStencilView(const std::string & rsName)
{
	auto &container = s_pDepthStencilViews;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

ID3D11RenderTargetView * CGlobalDirectXStorage::getRenderTargetView(const std::string & rsName)
{
	auto &container = s_pRenderTargetViews;
	auto iter = container.find(rsName);
	if (iter == container.end())
		return nullptr;
	else
		return iter->second;
}

bool CGlobalDirectXStorage::checkVertexShader(string const & rsName)
{
	auto &container = s_pVSs;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkHullShader(const std::string & rsName)
{
	auto &container = s_pHSs;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkDomainShader(const std::string & rsName)
{
	auto &container = s_pDSs;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkGeometryShader(string const & rsName)
{
	auto &container = s_pGSs;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkPixelShader(string const & rsName)
{
	auto &container = s_pPSs;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkBuffer(string const & rsName)
{
	auto &container = s_pBuffers;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkTexture1D(const std::string & rsName)
{
	auto &container = s_pTextures_1D;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkTexture2D(string const & rsName)
{
	auto &container = s_pTextures_2D;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkShaderResourceView(string const & rsName)
{
	auto &container = s_pShaderResourceViews;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkSamplerState(string const & rsName)
{
	auto &container = s_pSamplerStates;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkInputLayout(string const & rsName)
{
	auto &container = s_pInputLayouts;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkRasterizerState(const std::string & rsName)
{
	auto &container = s_pRasterizerStates;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkBlendState(const std::string & rsName)
{
	auto &container = s_pBlendStates;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkDepthStencilState(const std::string & rsName)
{
	auto &container = s_pDepthStencilStates;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkDepthStencilView(const std::string & rsName)
{
	auto &container = s_pDepthStencilViews;
	auto iter = container.find(rsName);
	return iter != container.end();
}

bool CGlobalDirectXStorage::checkRenderTargetView(const std::string & rsName)
{
	auto &container = s_pRenderTargetViews;
	auto iter = container.find(rsName);
	return iter != container.end();
}

void CGlobalDirectXStorage::setVertexShader(string const & rsName, ID3D11VertexShader * p)
{
	auto &container = s_pVSs;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setHullShader(const std::string & rsName, ID3D11HullShader * p)
{
	auto &container = s_pHSs;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setDomainShader(const std::string & rsName, ID3D11DomainShader * p)
{
	auto &container = s_pDSs;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setGeometryShader(string const & rsName, ID3D11GeometryShader * p)
{
	auto &container = s_pGSs;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setPixelShader(string const & rsName, ID3D11PixelShader * p)
{
	auto &container = s_pPSs;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setBuffer(string const & rsName, ID3D11Buffer * p)
{
	auto &container = s_pBuffers;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setTexture1D(const std::string & rsName, ID3D11Texture1D * p)
{
	auto &container = s_pTextures_1D;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setTexture2D(string const & rsName, ID3D11Texture2D * p)
{
	auto &container = s_pTextures_2D;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setShaderResourceView(string const & rsName, ID3D11ShaderResourceView * p)
{
	auto &container = s_pShaderResourceViews;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setSamplerState(string const & rsName, ID3D11SamplerState * p)
{
	auto &container = s_pSamplerStates;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setInputLayout(string const & rsName, ID3D11InputLayout * p)
{
	auto &container = s_pInputLayouts;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setRasterizerState(const std::string & rsName, ID3D11RasterizerState * p)
{
	auto &container = s_pRasterizerStates;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setBlendState(const std::string & rsName, ID3D11BlendState * p)
{
	auto &container = s_pBlendStates;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setDepthStencilState(const std::string & rsName, ID3D11DepthStencilState * p)
{
	auto &container = s_pDepthStencilStates;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setDepthStencilView(const std::string & rsName, ID3D11DepthStencilView * p)
{
	auto &container = s_pDepthStencilViews;
	container[rsName] = p;
}

void CGlobalDirectXStorage::setRenderTargetView(const std::string & rsName, ID3D11RenderTargetView * p)
{
	auto &container = s_pRenderTargetViews;
	container[rsName] = p;
}


//--------------------------------------------------------------------------------

void CGlobalDirectXStorage::resetVertexShader(string const & rsName, ID3D11VertexShader * p)
{
	auto &container = s_pVSs;
	if (checkVertexShader(rsName))
		container[rsName]->Release();
	
	container[rsName] = p;
}

void CGlobalDirectXStorage::resetHullShader(const std::string & rsName, ID3D11HullShader * p)
{
	auto &container = s_pHSs;
	if (checkGeometryShader(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetDomainShader(const std::string & rsName, ID3D11DomainShader * p)
{
	auto &container = s_pDSs;
	if (checkGeometryShader(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetGeometryShader(string const & rsName, ID3D11GeometryShader * p)
{
	auto &container = s_pGSs;
	if (checkGeometryShader(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetPixelShader(string const & rsName, ID3D11PixelShader * p)
{
	auto &container = s_pPSs;
	if (checkPixelShader(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetBuffer(string const & rsName, ID3D11Buffer * p)
{
	auto &container = s_pBuffers;
	if (checkBuffer(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetTexture1D(const std::string & rsName, ID3D11Texture1D * p)
{
	auto &container = s_pTextures_1D;
	if (checkTexture2D(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetTexture2D(string const & rsName, ID3D11Texture2D * p)
{
	auto &container = s_pTextures_2D;
	if (checkTexture2D(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetShaderResourceView(string const & rsName, ID3D11ShaderResourceView * p)
{
	auto &container = s_pShaderResourceViews;
	if (checkShaderResourceView(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetSamplerState(string const & rsName, ID3D11SamplerState * p)
{
	auto &container = s_pSamplerStates;
	if (checkSamplerState(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetInputLayout(string const & rsName, ID3D11InputLayout * p)
{
	auto &container = s_pInputLayouts;
	if (checkInputLayout(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetRasterizerState(const std::string & rsName, ID3D11RasterizerState * p)
{
	auto &container = s_pRasterizerStates;
	if (checkRasterizerState(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetBlendState(const std::string & rsName, ID3D11BlendState * p)
{
	auto &container = s_pBlendStates;
	if (checkBlendState(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetDepthStencilState(const std::string & rsName, ID3D11DepthStencilState * p)
{
	auto &container = s_pDepthStencilStates;
	if (checkDepthStencilState(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetDepthStencilView(const std::string & rsName, ID3D11DepthStencilView * p)
{
	auto &container = s_pDepthStencilViews;
	if (checkDepthStencilView(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::resetRenderTargetView(const std::string & rsName, ID3D11RenderTargetView * p)
{
	auto &container = s_pRenderTargetViews;
	if (checkRenderTargetView(rsName))
		container[rsName]->Release();

	container[rsName] = p;
}

void CGlobalDirectXStorage::deleteVertexShader(const std::string & rsName)
{
	auto &container = s_pVSs;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
	
}

void CGlobalDirectXStorage::deleteHullShader(const std::string & rsName)
{
	auto &container = s_pHSs;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteDomainShader(const std::string & rsName)
{
	auto &container = s_pDSs;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteGeometryShader(const std::string & rsName)
{
	auto &container = s_pGSs;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deletePixelShader(const std::string & rsName)
{
	auto &container = s_pPSs;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteBuffer(const std::string & rsName)
{
	auto &container = s_pBuffers;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteTexture1D(const std::string & rsName)
{
	auto &container = s_pTextures_1D;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteTexture2D(const std::string & rsName)
{
	auto &container = s_pTextures_2D;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteShaderResourceView(const std::string & rsName)
{
	auto &container = s_pShaderResourceViews;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteSamplerState(const std::string & rsName)
{
	auto &container = s_pSamplerStates;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteInputLayout(const std::string & rsName)
{
	auto &container = s_pInputLayouts;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteRasterizerState(const std::string & rsName)
{
	auto &container = s_pRasterizerStates;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteBlendState(const std::string & rsName)
{
	auto &container = s_pBlendStates;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteDepthStencilState(const std::string & rsName)
{
	auto &container = s_pDepthStencilStates;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteDepthStencilView(const std::string & rsName)
{
	auto &container = s_pDepthStencilViews;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::deleteRenderTargetView(const std::string & rsName)
{
	auto &container = s_pRenderTargetViews;
	auto iter = container.find(rsName);
	if (iter != container.end())
	{
		iter->second->Release();
		container.erase(iter);
	}
}

void CGlobalDirectXStorage::clear()
{
	for(auto &data : s_pVSs)
		if(data.second) data.second->Release();
	s_pVSs.clear();

	for (auto &data : s_pHSs)
		if (data.second) data.second->Release();
	s_pHSs.clear();

	for (auto &data : s_pDSs)
		if (data.second) data.second->Release();
	s_pDSs.clear();

	for (auto &data : s_pGSs)
		if (data.second) data.second->Release();
	s_pGSs.clear();

	for (auto &data : s_pPSs)
		if (data.second) data.second->Release();
	s_pPSs.clear();


	for (auto &data : s_pDepthStencilViews)
		if (data.second) data.second->Release();
	s_pDepthStencilViews.clear();

	for (auto &data : s_pRenderTargetViews)
		if (data.second) data.second->Release();
	s_pRenderTargetViews.clear();

	for (auto &data : s_pBuffers)
		if (data.second) data.second->Release();
	s_pBuffers.clear();

	for (auto &data : s_pTextures_1D)
		if (data.second) data.second->Release();
	s_pTextures_1D.clear();

	for (auto &data : s_pTextures_2D)
		if (data.second) data.second->Release();
	s_pTextures_2D.clear();

	for (auto &data : s_pShaderResourceViews)
		if (data.second) data.second->Release();
	s_pShaderResourceViews.clear();

	for (auto &data : s_pSamplerStates)
		if (data.second) data.second->Release();
	s_pSamplerStates.clear();

	for (auto &data : s_pInputLayouts)
		if (data.second) data.second->Release();
	s_pInputLayouts.clear();

	for (auto &data : s_pRasterizerStates)
		if (data.second) data.second->Release();
	s_pRasterizerStates.clear();

	for (auto &data : s_pBlendStates)
		if (data.second) data.second->Release();
	s_pBlendStates.clear();

	for (auto &data : s_pDepthStencilStates)
		if (data.second) data.second->Release();
	s_pDepthStencilStates.clear();
}


static CGlobalDirectXStorage cleaner;