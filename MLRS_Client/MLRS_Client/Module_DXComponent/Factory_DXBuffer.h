#pragma once
#ifndef HEADER_FACTORY_DXBUFFER
#define HEADER_FACTORY_DXBUFFER

#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"GlobalDirectXStorage.h"

#ifndef HEADER_STL
#include<string>
#endif

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT


class CFactory_Buffer_FromSize{
private:
	std::string m_sName;
	D3D11_USAGE m_Usage;
	UINT m_nBindFlags;
	UINT m_nCPUAccessFlags;
	UINT m_nMiscFlag;
	UINT m_nSizeOfElement;
	UINT m_nNumberOfElement;
	 
public:
	CFactory_Buffer_FromSize(const std::string &rsName,
		const UINT nSizeOfElement,
		const UINT nNumberOfElement,
		const D3D11_USAGE usage,
		const UINT nBindFlags,
		const UINT nCPUAccessFlags,
		const UINT nMiscFlag = NULL)
		:m_sName(rsName),
		m_Usage(usage),
		m_nBindFlags(nBindFlags),
		m_nCPUAccessFlags(nCPUAccessFlags),
		m_nMiscFlag(nMiscFlag),
		m_nSizeOfElement(nSizeOfElement),
		m_nNumberOfElement(nNumberOfElement) {}

	virtual ~CFactory_Buffer_FromSize() {}

	ID3D11Buffer *createBuffer()
	{

		if (CGlobalDirectXStorage::checkBuffer(m_sName))
			return CGlobalDirectXStorage::getBuffer(m_sName);

		ID3D11Buffer * pBuffer;
		D3D11_BUFFER_DESC bd; 
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = m_Usage;
		bd.BindFlags = m_nBindFlags;
		bd.CPUAccessFlags = m_nCPUAccessFlags;
		bd.ByteWidth = m_nSizeOfElement * m_nNumberOfElement;
		if (m_nMiscFlag)
		{
			bd.MiscFlags = m_nMiscFlag;
			bd.StructureByteStride = m_nSizeOfElement;
		} 
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateBuffer(&bd, NULL, &pBuffer)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_Buffer.createBuffer(), CreateBuffer() Error");
			return nullptr;
		} 
		CGlobalDirectXStorage::setBuffer(m_sName, pBuffer);
		return pBuffer;
	}

};


template<class TYPE>
class CFactory_Buffer {
private:
	std::string m_sName;
	D3D11_USAGE m_Usage;
	UINT m_nBindFlags;
	UINT m_nCPUAccessFlags;
	UINT m_nMiscFlag;
	UINT m_nNumberOfElement;

	virtual UINT getElementSize()
	{
		return sizeof(TYPE);
	}
public:
	CFactory_Buffer(const std::string &rsName, 
		const UINT nNumberOfElement,
		const D3D11_USAGE usage,
		const UINT nBindFlags,
		const UINT nCPUAccessFlags,
		const UINT nMiscFlag = NULL)
		:m_sName(rsName),
		m_Usage(usage),
		m_nBindFlags(nBindFlags),
		m_nCPUAccessFlags(nCPUAccessFlags),
		m_nMiscFlag(nMiscFlag),
		m_nNumberOfElement(nNumberOfElement){}

	virtual ~CFactory_Buffer() {}

	ID3D11Buffer *createBuffer(const TYPE *pInitData = nullptr);
	ID3D11Buffer *createBuffer(const TYPE &rInitData );

};





template<class TYPE>
class CFactory_ConstBuffer : public CFactory_Buffer<TYPE> {
private:
	virtual UINT getElementSize()
	{
		UINT size = sizeof(TYPE);
		if (size % 16)
			size += 16 - size % 16;
		return size;
	}
public:
	CFactory_ConstBuffer(
		const std::string &rsName, 
		UINT nNumberOfElement = 1)
		:CFactory_Buffer(rsName, nNumberOfElement, D3D11_USAGE_DYNAMIC, D3D11_BIND_CONSTANT_BUFFER, D3D11_CPU_ACCESS_WRITE) {}
	virtual ~CFactory_ConstBuffer() {}
};

template<class TYPE>
class CFactory_ConstBuffer_NonRenewal : public CFactory_Buffer<TYPE> {
private:
	virtual UINT getElementSize()
	{
		UINT size = sizeof(TYPE);
		if (size % 16)
			size += 16 - size % 16;
		return size;
	}

public:
	CFactory_ConstBuffer_NonRenewal(
		const std::string &rsName,
		UINT nNumberOfElement = 1)
		:CFactory_Buffer(rsName, nNumberOfElement, D3D11_USAGE_DEFAULT, D3D11_BIND_CONSTANT_BUFFER, NULL) {}
	virtual ~CFactory_ConstBuffer_NonRenewal() {}

	//override CFactory_Buffer
	//갱신이 불가능하므로 초기값이 반드시 필요함
	ID3D11Buffer *createBuffer(const TYPE *pInitData) { return CFactory_Buffer<TYPE>::createBuffer(pInitData); }
	ID3D11Buffer *createBuffer(const TYPE &rInitData) { return CFactory_Buffer<TYPE>::createBuffer(rInitData); }
};

template<class TYPE>
class CFactory_VertexBuffer : public CFactory_Buffer<TYPE> {
public:
	CFactory_VertexBuffer(	const std::string &rsName,
							const UINT nNumberOfElement,
							const D3D11_USAGE usage = D3D11_USAGE_DEFAULT,
							const UINT nCPUAccessFlags = NULL )
		:CFactory_Buffer(rsName, nNumberOfElement, usage, D3D11_BIND_VERTEX_BUFFER, nCPUAccessFlags) {}
	virtual ~CFactory_VertexBuffer() {}
};
template<class TYPE>
class CFactory_InstanceBuffer : public CFactory_Buffer<TYPE> {
public:
	CFactory_InstanceBuffer(const std::string &rsName, UINT nNumberOfElement)
		:CFactory_Buffer(rsName, nNumberOfElement, D3D11_USAGE_DYNAMIC, D3D11_BIND_VERTEX_BUFFER, D3D11_CPU_ACCESS_WRITE) {}
	virtual ~CFactory_InstanceBuffer() {}
};

class CFactory_IndexBuffer : public CFactory_Buffer<UINT> {
public:
	CFactory_IndexBuffer(const std::string &rsName, UINT nNumberOfElement )
		:CFactory_Buffer(rsName, nNumberOfElement, D3D11_USAGE_DEFAULT, D3D11_BIND_INDEX_BUFFER, NULL) {}
	virtual ~CFactory_IndexBuffer() {}
};


//----------------------------------- 구현부 -------------------------------------------------------------------------------------

template<class TYPE>
inline ID3D11Buffer * CFactory_Buffer<TYPE>::createBuffer(const TYPE * pInitData)
{

	if (CGlobalDirectXStorage::checkBuffer(m_sName))
		return CGlobalDirectXStorage::getBuffer(m_sName);

	ID3D11Buffer * pBuffer;
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA bufferData;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = m_Usage;
	bd.BindFlags = m_nBindFlags;
	bd.CPUAccessFlags = m_nCPUAccessFlags;
	bd.ByteWidth = this->getElementSize() * m_nNumberOfElement;
	if (m_nMiscFlag)
	{
		bd.MiscFlags = m_nMiscFlag;
		bd.StructureByteStride = this->getElementSize();
	}
	if (pInitData) {
		ZeroMemory(&bufferData, sizeof(bufferData));
		bufferData.pSysMem = static_cast<const void*>(pInitData);
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateBuffer(&bd, &bufferData, &pBuffer)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_Buffer.createBuffer(), CreateBuffer() Error");
			return nullptr;
		}
	}
	else
	{
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateBuffer(&bd, NULL, &pBuffer)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_Buffer.createBuffer(), CreateBuffer() Error");
			return nullptr;
		}
	}
	CGlobalDirectXStorage::setBuffer(m_sName, pBuffer);
	return pBuffer;

}

template<class TYPE>
inline ID3D11Buffer * CFactory_Buffer<TYPE>::createBuffer(const TYPE & rInitData)
{
	if (CGlobalDirectXStorage::checkBuffer(m_sName))
		return CGlobalDirectXStorage::getBuffer(m_sName);

	ID3D11Buffer * pBuffer;
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA bufferData;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = m_Usage;
	bd.BindFlags = m_nBindFlags;
	bd.CPUAccessFlags = m_nCPUAccessFlags;
	bd.ByteWidth = this->getElementSize() * m_nNumberOfElement;
	if (m_nMiscFlag)
	{
		bd.MiscFlags = m_nMiscFlag;
		bd.StructureByteStride = this->getElementSize();
	}

	ZeroMemory(&bufferData, sizeof(bufferData));
	bufferData.pSysMem = static_cast<const void*>(&rInitData);
	if (FAILED(CGlobalDirectXDevice::getDevice()->CreateBuffer(&bd, &bufferData, &pBuffer)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_Buffer.createBuffer(), CreateBuffer() Error");
		return nullptr;
	}

	CGlobalDirectXStorage::setBuffer(m_sName, pBuffer);
	return pBuffer;
}





#endif