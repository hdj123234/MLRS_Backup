#pragma once

#ifndef HEADER_FACTORY_DXCOMPONENT
#define HEADER_FACTORY_DXCOMPONENT
#include"..\Interface_Global.h"


#ifndef HEADER_DIRECTX
#include<D3DX11.h>
#endif


//----------------------------------- State -------------------------------------------------------------------------------------

class CFactory_DepthStencilState  {
protected:
	std::string m_sName;
	D3D11_DEPTH_STENCIL_DESC m_DepthStencilDesc;
public:
	CFactory_DepthStencilState(const std::string &rsName, D3D11_DEPTH_STENCIL_DESC &rDepthStencilDesc);
	virtual ~CFactory_DepthStencilState() {}

	virtual ID3D11DepthStencilState * createDepthStencilState();
};

class CFactory_RasterizerState {
private:
	std::string m_sName;
	D3D11_RASTERIZER_DESC m_RasterizerDesc;
public:
	CFactory_RasterizerState(const std::string &rsName, D3D11_RASTERIZER_DESC &rRasterizerDesc);
	virtual ~CFactory_RasterizerState() {}

	virtual ID3D11RasterizerState *createRasterizerState();
};

class CFactory_SamplerState {
private:
	std::string m_sName;
	D3D11_SAMPLER_DESC m_SamplerDesc;
public:
	CFactory_SamplerState(const std::string &rsName, D3D11_SAMPLER_DESC  &rSamplerDesc);
	virtual ~CFactory_SamplerState() {}

	virtual ID3D11SamplerState *createSamplerState();
};

class CFactory_BlendState {
private:
	std::string m_sName;
	D3D11_BLEND_DESC m_BlendDesc;
public:
	CFactory_BlendState(const std::string &rsName, D3D11_BLEND_DESC  &rBlendDesc);
	virtual ~CFactory_BlendState() {}

	virtual ID3D11BlendState *createBlendState();
};
//----------------------------------- View -------------------------------------------------------------------------------------

class AFactory_DepthStencilView {
protected:
	const std::string m_sName;
	DXGI_FORMAT m_Format;
	UINT m_Flag_DSV;

	static const std::string s_sPrefix_View;
public:
	AFactory_DepthStencilView(const std::string &rsName, const DXGI_FORMAT format, const UINT flag_DSV );
	virtual ~AFactory_DepthStencilView(){}

protected:
	virtual ID3D11DepthStencilView * createDepthStencilView_RealProcessing(ID3D11Texture2D *);

public:
	virtual ID3D11DepthStencilView * createDepthStencilView()=0;
};


class CFactory_DepthStencilView : public AFactory_DepthStencilView {
protected:
	D3D11_TEXTURE2D_DESC m_DepthStencilBufferDesc;
	ID3D11Texture2D * m_pDSB;
	const std::string m_sBufferName;
	static const std::string s_sPrefix_Buffer;
public:
	CFactory_DepthStencilView(	const std::string &rsViewName, 
								const std::string &rsBufferName,
								D3D11_TEXTURE2D_DESC &rDepthStencilBufferDesc,
								const UINT flag_DSV=0 );
	virtual ~CFactory_DepthStencilView() {}

private:
	bool createDepthStencilBuffer_RealProcessing();

public:
	virtual ID3D11Texture2D * createDepthStencilBuffer();
	virtual ID3D11DepthStencilView * createDepthStencilView();
};

class CFactory_DepthStencilView_FromTexture2D : public AFactory_DepthStencilView {
protected:
	ID3D11Texture2D * m_pDepthStencilBuffer;
public:
	CFactory_DepthStencilView_FromTexture2D(const std::string &rsName, 
		ID3D11Texture2D * pDepthStencilBuffer, 
		const DXGI_FORMAT format,
		const UINT flag_DSV=0);
	virtual ~CFactory_DepthStencilView_FromTexture2D() {}

	virtual ID3D11DepthStencilView * createDepthStencilView();
};

class AFactory_RenderTargetView {
private:
	virtual ID3D11RenderTargetView *createRenderTargetView_RealProcessing() = 0;
protected:
	std::string m_sName;
public:
	AFactory_RenderTargetView(const std::string &rsName);
	virtual ~AFactory_RenderTargetView() {}

	virtual ID3D11RenderTargetView *createRenderTargetView();
};

class CFactory_RenderTargetView_BackBuffer : public AFactory_RenderTargetView {
private:
	virtual ID3D11RenderTargetView *createRenderTargetView_RealProcessing() ;
public:
	CFactory_RenderTargetView_BackBuffer(const std::string &rsName);
	virtual ~CFactory_RenderTargetView_BackBuffer() {}
};

class CFactory_RenderTargetView_FromTexture2D : public AFactory_RenderTargetView {
private:
	ID3D11Texture2D *m_pTexture2D;
	DXGI_FORMAT m_Format;
	virtual ID3D11RenderTargetView *createRenderTargetView_RealProcessing();
public:
	CFactory_RenderTargetView_FromTexture2D(const std::string &rsName, ID3D11Texture2D *pTexture2D, DXGI_FORMAT format);
	CFactory_RenderTargetView_FromTexture2D(const std::string &rsName, ID3D11Texture2D *pTexture2D, D3D11_TEXTURE2D_DESC const &desc);
	virtual ~CFactory_RenderTargetView_FromTexture2D() {}
};




#endif