#pragma once
#ifndef HEADER_FACTORY_DXCOMPONENT_COMMON
#define HEADER_FACTORY_DXCOMPONENT_COMMON

#include"Factory_DXBuffer.h"
#include"Factory_DXComponent.h"

#include"..\Module_Platform_Windows\GlobalWindows.h"
#include"Type_InDirectX.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

//----------------------------------- Struct_Default -------------------------------------------------------------------------------------

struct DXViewport_Default : public D3D11_VIEWPORT {
	DXViewport_Default() {
		ZeroMemory((D3D11_VIEWPORT*)this, sizeof(D3D11_VIEWPORT));
		this->Width = static_cast<FLOAT>(CGlobalWindow::getWidth());
		this->Height = static_cast<FLOAT>(CGlobalWindow::getHeight());
		this->TopLeftX = 0.0f;
		this->TopLeftY = 0.0f;
		this->MinDepth = 0.0f;
		this->MaxDepth = 1.0f;
	}
};

struct DepthStencilBufferDesc_Default : public D3D11_TEXTURE2D_DESC {
	DepthStencilBufferDesc_Default()
	{
		ZeroMemory(dynamic_cast<D3D11_TEXTURE2D_DESC*>(this), sizeof(D3D11_TEXTURE2D_DESC));
		this->Width = CGlobalWindow::getWidth();
		this->Height = CGlobalWindow::getHeight();
		this->MipLevels = 1;
		this->ArraySize = 1;
//		this->Format = DXGI_FORMAT_R32_TYPELESS;
		this->Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		this->SampleDesc.Count = 1;
		this->SampleDesc.Quality = 0;
		this->Usage = D3D11_USAGE_DEFAULT;
//		this->BindFlags = D3D11_BIND_DEPTH_STENCIL|D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
		this->BindFlags = D3D11_BIND_DEPTH_STENCIL;
		this->CPUAccessFlags = 0;
		this->MiscFlags = 0;
	}
};

struct DepthStencilBufferDesc_WithSRV: public D3D11_TEXTURE2D_DESC {
	DepthStencilBufferDesc_WithSRV()
	{
		ZeroMemory(dynamic_cast<D3D11_TEXTURE2D_DESC*>(this), sizeof(D3D11_TEXTURE2D_DESC));
		this->Width = CGlobalWindow::getWidth();
		this->Height = CGlobalWindow::getHeight();
		this->MipLevels = 1;
		this->ArraySize = 1;
		this->Format = DXGI_FORMAT_R32_TYPELESS;
		//		this->Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		this->SampleDesc.Count = 1;
		this->SampleDesc.Quality = 0;
		this->BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
		//		this->BindFlags = D3D11_BIND_DEPTH_STENCIL;
		//üũ
//		this->Usage = D3D11_USAGE_STAGING;
//		this->CPUAccessFlags = D3D11_CPU_ACCESS_READ;
		this->Usage = D3D11_USAGE_DEFAULT;
		this->CPUAccessFlags = 0;
		this->MiscFlags = 0;
	}
};

class SamplerDesc_Default :public D3D11_SAMPLER_DESC {
public:
	SamplerDesc_Default()
	{
		ZeroMemory(this, sizeof(D3D11_SAMPLER_DESC));
		AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		MaxLOD = 1;
		MinLOD = 1;
		ComparisonFunc = D3D11_COMPARISON_NEVER;
		Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	}
};

class SamplerDesc_Point :public D3D11_SAMPLER_DESC {
public:
	SamplerDesc_Point()
	{
		ZeroMemory(this, sizeof(D3D11_SAMPLER_DESC));
		AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		MaxLOD = 1;
		MinLOD = 1;
		ComparisonFunc = D3D11_COMPARISON_NEVER;
		Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	}
};

class BlendDesc_Default :public D3D11_BLEND_DESC {
public:
	BlendDesc_Default()
	{
		ZeroMemory(this, sizeof(D3D11_BLEND_DESC));
		AlphaToCoverageEnable = FALSE;
		IndependentBlendEnable = FALSE;
		
		RenderTarget[0].BlendEnable = TRUE;
		RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE::D3D11_COLOR_WRITE_ENABLE_ALL;
		RenderTarget[0].BlendOp = D3D11_BLEND_OP::D3D11_BLEND_OP_ADD;
		RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP::D3D11_BLEND_OP_ADD;
		RenderTarget[0].SrcBlend = D3D11_BLEND::D3D11_BLEND_SRC_ALPHA;
		RenderTarget[0].SrcBlendAlpha = D3D11_BLEND::D3D11_BLEND_ONE;
		RenderTarget[0].DestBlend = D3D11_BLEND::D3D11_BLEND_INV_SRC_ALPHA;
		RenderTarget[0].DestBlendAlpha = D3D11_BLEND::D3D11_BLEND_ONE;
	}
};

class BlendDesc_Disable :public D3D11_BLEND_DESC {
public:
	BlendDesc_Disable()
	{
		ZeroMemory(this, sizeof(D3D11_BLEND_DESC));

		AlphaToCoverageEnable = FALSE;
		IndependentBlendEnable = FALSE;
		RenderTarget[0].BlendEnable = FALSE;
		RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE::D3D11_COLOR_WRITE_ENABLE_ALL;
		RenderTarget[0].BlendOp = D3D11_BLEND_OP::D3D11_BLEND_OP_ADD;
		RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP::D3D11_BLEND_OP_ADD;
		RenderTarget[0].SrcBlend = D3D11_BLEND::D3D11_BLEND_SRC_ALPHA;
		RenderTarget[0].SrcBlendAlpha = D3D11_BLEND::D3D11_BLEND_ONE;
		RenderTarget[0].DestBlend = D3D11_BLEND::D3D11_BLEND_INV_SRC_ALPHA;
		RenderTarget[0].DestBlendAlpha = D3D11_BLEND::D3D11_BLEND_ONE;
	}
};

class BlendDesc_Effect :public D3D11_BLEND_DESC {
public:
	BlendDesc_Effect()
	{
		ZeroMemory(this, sizeof(D3D11_BLEND_DESC));
		AlphaToCoverageEnable = FALSE;
		IndependentBlendEnable = FALSE;

		RenderTarget[0].BlendEnable = TRUE;
		RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE::D3D11_COLOR_WRITE_ENABLE_ALL;
		RenderTarget[0].BlendOp = D3D11_BLEND_OP::D3D11_BLEND_OP_ADD;
		RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP::D3D11_BLEND_OP_ADD;
		RenderTarget[0].SrcBlend = D3D11_BLEND::D3D11_BLEND_SRC_ALPHA;
		RenderTarget[0].SrcBlendAlpha = D3D11_BLEND::D3D11_BLEND_ONE;
		RenderTarget[0].DestBlend = D3D11_BLEND::D3D11_BLEND_INV_SRC_ALPHA;
		RenderTarget[0].DestBlendAlpha = D3D11_BLEND::D3D11_BLEND_ONE;
	}
};
class DepthStencilDesc_Default :public D3D11_DEPTH_STENCIL_DESC {
public:
	DepthStencilDesc_Default()
	{
		ZeroMemory(this, sizeof(D3D11_DEPTH_STENCIL_DESC));

		D3D11_DEPTH_STENCIL_DESC::DepthEnable = true;
		D3D11_DEPTH_STENCIL_DESC::DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		D3D11_DEPTH_STENCIL_DESC::DepthFunc = D3D11_COMPARISON_LESS;
		D3D11_DEPTH_STENCIL_DESC::StencilEnable = false;

	}
};

class DepthStencilDesc_Test :public D3D11_DEPTH_STENCIL_DESC {
public:
	DepthStencilDesc_Test()
	{
		ZeroMemory(this, sizeof(D3D11_DEPTH_STENCIL_DESC));

		D3D11_DEPTH_STENCIL_DESC::DepthEnable = true;
		D3D11_DEPTH_STENCIL_DESC::DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		D3D11_DEPTH_STENCIL_DESC::DepthFunc = D3D11_COMPARISON_ALWAYS;
		D3D11_DEPTH_STENCIL_DESC::StencilEnable = false;
	}
};
class DepthStencilDesc_Disable :public D3D11_DEPTH_STENCIL_DESC {
public:
	DepthStencilDesc_Disable()
	{
		ZeroMemory(this, sizeof(D3D11_DEPTH_STENCIL_DESC));

		D3D11_DEPTH_STENCIL_DESC::DepthEnable = false;
		D3D11_DEPTH_STENCIL_DESC::StencilEnable = false;
	}
};
class DepthStencilDesc_CheckOnly :public D3D11_DEPTH_STENCIL_DESC {
public:
	DepthStencilDesc_CheckOnly()
	{
		ZeroMemory(this, sizeof(D3D11_DEPTH_STENCIL_DESC));

		D3D11_DEPTH_STENCIL_DESC::DepthEnable = true;
		D3D11_DEPTH_STENCIL_DESC::DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		D3D11_DEPTH_STENCIL_DESC::DepthFunc = D3D11_COMPARISON_LESS;
		D3D11_DEPTH_STENCIL_DESC::StencilEnable = false;
	}
};
class RasterizerDesc_Default : public D3D11_RASTERIZER_DESC {
public:
	RasterizerDesc_Default()
	{
		ZeroMemory(this, sizeof(D3D11_RASTERIZER_DESC));

		D3D11_RASTERIZER_DESC::FillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;
		D3D11_RASTERIZER_DESC::CullMode = D3D11_CULL_MODE::D3D11_CULL_BACK;
	}
};
class RasterizerDesc_Wire : public D3D11_RASTERIZER_DESC {
public:
	RasterizerDesc_Wire()
	{
		ZeroMemory(this, sizeof(D3D11_RASTERIZER_DESC));

		D3D11_RASTERIZER_DESC::FillMode = D3D11_FILL_MODE::D3D11_FILL_WIREFRAME;
		D3D11_RASTERIZER_DESC::CullMode = D3D11_CULL_MODE::D3D11_CULL_NONE;
	}
};
class RasterizerDesc_DisableCulling : public D3D11_RASTERIZER_DESC {
public:
	RasterizerDesc_DisableCulling()
	{
		ZeroMemory(this, sizeof(D3D11_RASTERIZER_DESC));

		D3D11_RASTERIZER_DESC::FillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;
		D3D11_RASTERIZER_DESC::CullMode = D3D11_CULL_MODE::D3D11_CULL_NONE;
	}
};

class StencilOPDesc_Disable : public D3D11_DEPTH_STENCILOP_DESC {
public:
	StencilOPDesc_Disable()
	{
		ZeroMemory(this, sizeof(D3D11_DEPTH_STENCILOP_DESC));

		D3D11_DEPTH_STENCILOP_DESC::StencilDepthFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
		D3D11_DEPTH_STENCILOP_DESC::StencilFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
		D3D11_DEPTH_STENCILOP_DESC::StencilPassOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
		D3D11_DEPTH_STENCILOP_DESC::StencilFunc = D3D11_COMPARISON_FUNC::D3D11_COMPARISON_NEVER ;
	}
};

//----------------------------------- Creator_Default -------------------------------------------------------------------------------------
//
//class CFactory_SamplerState_Default : public CFactory_SamplerState {
//public:
//	CFactory_SamplerState_Default()
//		:CFactory_SamplerState(std::string("SamplerState_Default"), SamplerDesc_Default()) {}
//	virtual ~CFactory_SamplerState_Default() {}
//};
//
//class CFactory_SamplerState_Default_NoFiltering : public CFactory_SamplerState {
//public:
//	CFactory_SamplerState_Default_NoFiltering()
//		:CFactory_SamplerState(std::string("SamplerState_Default_NoFiltering"), SamplerDesc_Point()) {}
//	virtual ~CFactory_SamplerState_Default_NoFiltering() {}
//};
//
class CFactory_DepthStencilView_Default : public CFactory_DepthStencilView {
public:
	CFactory_DepthStencilView_Default()
		:CFactory_DepthStencilView("DSV_Default", "DSB_Default", DepthStencilBufferDesc_WithSRV()) {}
	virtual ~CFactory_DepthStencilView_Default() {}
};

class CFactory_DepthStencilView_ReadOnly : public CFactory_DepthStencilView {
public:
	CFactory_DepthStencilView_ReadOnly()
		:CFactory_DepthStencilView("DSV_Default_ReadOnly", "DSB_Default", DepthStencilBufferDesc_WithSRV(),D3D11_DSV_FLAG::D3D11_DSV_READ_ONLY_DEPTH) {}
	virtual ~CFactory_DepthStencilView_ReadOnly() {}
};

 

static CFactory_SamplerState factory_SamplerState_Default(std::string("SamplerState_Default"), SamplerDesc_Default());
static CFactory_SamplerState factory_SamplerState_Point(std::string("SamplerState_Default_Point"), SamplerDesc_Point());
//static CFactory_DepthStencilView  factory_DepthStencilView_Default("DSV_Default", DepthStencilBufferDesc_Default()); 
//static CFactory_SamplerState_Default factory_SamplerState_Default;
//static CFactory_SamplerState_Default_NoFiltering factory_SamplerState_Point;
static CFactory_DepthStencilState factory_DepthStencilState_Default("DSS_Default", DepthStencilDesc_Default());
static CFactory_DepthStencilState factory_DepthStencilState_Disable("DSS_Disable", DepthStencilDesc_Disable());
static CFactory_DepthStencilState factory_DepthStencilState_CheckOnly("DSS_CheckOnly", DepthStencilDesc_CheckOnly());
static CFactory_RasterizerState factory_RasterizerState_Default("RS_Default", RasterizerDesc_Default());
static CFactory_RasterizerState factory_RasterizerState_Wire("RS_Wire", RasterizerDesc_Wire());
static CFactory_RasterizerState factory_RasterizerState_DisableCulling("RS_DisableCulling", RasterizerDesc_DisableCulling());
static CFactory_BlendState factory_BlendState_Default("BS_Default", BlendDesc_Default());
static CFactory_BlendState factory_BlendState_Disable("BS_Disable", BlendDesc_Disable());
static CFactory_BlendState factory_BlendState_Effect("BS_Effect", BlendDesc_Effect());
static CFactory_RenderTargetView_BackBuffer factory_RTV_BackBuffer("RTV_BackBuffer");


static CFactory_ConstBuffer<DirectX::XMFLOAT4X4A>	factory_CB_Matrix_World(std::string("CB_World"), 1);
static CFactory_ConstBuffer<DirectX::XMFLOAT4X4A>	factory_CB_Matrix_View(std::string("CB_View"), 1);
static CFactory_ConstBuffer<DirectX::XMFLOAT4X4A>	factory_CB_Matrix_Projection(std::string("CB_Projection"), 1);
static CFactory_ConstBuffer<DirectX::XMFLOAT4X4A>	factory_CB_Matrix_ProjectionInverse(std::string("CB_ProjectionInverse"), 1);
static CFactory_ConstBuffer<Type_CameraState>		factory_CB_CameraState(std::string("CB_CameraState"), 1);
static CFactory_ConstBuffer<Type_Material>			factory_CB_Material(std::string("CB_Material"), 1);
static CFactory_ConstBuffer<Type_GlobalLighting>	factory_CB_GlobalLighting(std::string("CB_GlobalLight"), 1);
static CFactory_ConstBuffer<float>					factory_CB_ElapsedTime("CB_ElapsedTime", 1);
static CFactory_ConstBuffer<Type_FogState>			factory_CB_FogState(std::string("CB_FogState"), 1);



#endif