#pragma once

#ifndef HEADER_FACTORY_DXSHADERRESOURCEVIEW_FROMDATA
#define HEADER_FACTORY_DXSHADERRESOURCEVIEW_FROMDATA

#include"Factory_DXTexture_FromData.h"
#include"Factory_DXShaderResourceView.h"

template<class TYPE>
class CFactory_SRV_Texture1D_FromData : public AFactory_ShaderResourceView {
protected:
	const std::string m_sName_Shared;
	const std::vector<TYPE> &m_rData;
	const DXGI_FORMAT m_Format;

	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing()
	{
		ID3D11Texture1D  * pTexture = CFactory_Texture1D_FromData<TYPE>(std::string("TX1D_") + m_sName_Shared, m_rData, m_Format, D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE).createTexture1D();
		ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
		ID3D11ShaderResourceView *pShaderResourceView;

		D3D11_SHADER_RESOURCE_VIEW_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Format = m_Format;
		desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
		desc.Texture1D.MipLevels = 1;
		if (FAILED(pDevice->CreateShaderResourceView(pTexture, &desc, &pShaderResourceView)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_SRV_Texture1D_FromData.createShaderResourceView_RealProcessing() CreateShaderResourceView() Error");
			return nullptr;
		}
		else
			return pShaderResourceView;
	}

public:
	CFactory_SRV_Texture1D_FromData(const std::string &rsName,
		const std::vector<TYPE> &rData,
		const DXGI_FORMAT format)
		:AFactory_ShaderResourceView(std::string("SRV_") + rsName),
		m_rData(rData),
		m_sName_Shared(rsName),
		m_Format(format)
	{
	}
	virtual ~CFactory_SRV_Texture1D_FromData() {}

};




template<class TYPE>
class CFactory_SRV_Texture2D_FromData : public AFactory_ShaderResourceView {
protected:
	const std::string m_sName_Shared;
	const std::vector<TYPE> &m_rData;
	const DXGI_FORMAT m_Format;
	const unsigned int m_nWidth;
	const unsigned int m_nHeight;

	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing()
	{
		ID3D11Texture2D  * pTexture = CFactory_Texture2D_FromData<TYPE>(std::string("TX2D_") + m_sName_Shared, 
																		m_rData,
																		m_Format,
																		D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
																		m_nWidth,
																		m_nHeight)
			.createTexture2D();
		ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
		ID3D11ShaderResourceView *pShaderResourceView;

		D3D11_SHADER_RESOURCE_VIEW_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Format = m_Format;
		desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

		desc.Texture2D.MipLevels = 1;
		if (FAILED(pDevice->CreateShaderResourceView(pTexture, &desc, &pShaderResourceView)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_SRV_Texture2D_FromData.createShaderResourceView_RealProcessing() CreateShaderResourceView() Error");
			return nullptr;
		}
		else
			return pShaderResourceView;
	}

public:
	CFactory_SRV_Texture2D_FromData(
		const std::string &rsName,
		const std::vector<TYPE> &rData ,
		const DXGI_FORMAT format,
		const unsigned int nWidth =0,
		const unsigned int nHeight =0)
		:AFactory_ShaderResourceView(std::string("SRV_") + rsName),
		m_rData(rData),
		m_sName_Shared(rsName),
		m_Format(format),
		m_nWidth	(nWidth),
		m_nHeight	(nHeight)
	{
	}
	virtual ~CFactory_SRV_Texture2D_FromData() {}

};

template<class TYPE>
class CFactory_SRV_Buffer_FromData : public AFactory_ShaderResourceView {
protected:
	const std::string m_sName_Shared;
	const std::vector<TYPE> &m_rData;
	const DXGI_FORMAT m_Format;

	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing()
	{
		ID3D11Buffer  * pBuffer = CFactory_Buffer<TYPE>(std::string("BUF_") + m_sName_Shared,
			static_cast<unsigned int>(m_rData.size()), 
			D3D11_USAGE::D3D11_USAGE_DEFAULT,
			D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
			NULL)
			.createBuffer(m_rData.data());
		ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
		ID3D11ShaderResourceView *pShaderResourceView;

		D3D11_SHADER_RESOURCE_VIEW_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Format = m_Format;
		desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		desc.Buffer.ElementOffset = 0;
		desc.Buffer.NumElements = static_cast<unsigned int>(m_rData.size());
		if (FAILED(pDevice->CreateShaderResourceView(pBuffer, &desc, &pShaderResourceView)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_SRV_Texture2D_FromData.createShaderResourceView_RealProcessing() CreateShaderResourceView() Error");
			return nullptr;
		}
		else
			return pShaderResourceView;
	}

public:
	CFactory_SRV_Buffer_FromData(
		const std::string &rsName,
		const std::vector<TYPE> &rData,
		const DXGI_FORMAT format)
		:AFactory_ShaderResourceView(std::string("SRV_") + rsName),
		m_rData(rData),
		m_sName_Shared(rsName),
		m_Format(format)
	{
	}
	virtual ~CFactory_SRV_Buffer_FromData() {}

};


template<class TYPE>
class CFactory_SRV_BufferEX_FromData : public AFactory_ShaderResourceView {
protected:
	const std::string m_sName_Shared;
	const std::vector<TYPE> &m_rData;

	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing()
	{
		ID3D11Buffer  * pBuffer = CFactory_Buffer<TYPE>(std::string("BUFEX_") + m_sName_Shared, 
														static_cast<UINT>(m_rData.size()),
														D3D11_USAGE::D3D11_USAGE_DEFAULT,
														D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
														NULL,
														D3D11_RESOURCE_MISC_FLAG::D3D11_RESOURCE_MISC_BUFFER_STRUCTURED)
			.createBuffer(m_rData.data());
		ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
		ID3D11ShaderResourceView *pShaderResourceView;

		D3D11_SHADER_RESOURCE_VIEW_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Format = DXGI_FORMAT_UNKNOWN;
		desc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
		desc.BufferEx.FirstElement = 0;
		desc.BufferEx.NumElements = static_cast<UINT>(m_rData.size());
		desc.BufferEx.Flags = NULL;
		if (FAILED(pDevice->CreateShaderResourceView(pBuffer, &desc, &pShaderResourceView)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_SRV_BufferEX_FromData.createShaderResourceView_RealProcessing() CreateShaderResourceView() Error");
			return nullptr;
		}
		else
			return pShaderResourceView;
	}

public:
	CFactory_SRV_BufferEX_FromData(const std::string &rsName,
		const std::vector<TYPE> &rData)
		:AFactory_ShaderResourceView(std::string("SRV_") + rsName),
		m_rData(rData),
		m_sName_Shared(rsName)
	{
	}
	virtual ~CFactory_SRV_BufferEX_FromData() {}

};

#endif
