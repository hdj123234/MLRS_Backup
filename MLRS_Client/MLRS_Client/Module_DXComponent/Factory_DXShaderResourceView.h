#pragma once

#ifndef HEADER_FACTORY_DXSHADERRESOURCEVIEW
#define HEADER_FACTORY_DXSHADERRESOURCEVIEW
#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"GlobalDirectXStorage.h"

#ifndef HEADER_STL
#include<vector>
#endif

#ifndef HEADER_DIRECTX
#include<DXGI.h>
#include<D3DX11.h>
#endif

//���漱��
enum EImageType : char;

class AFactory_ShaderResourceView {
protected:
	std::string m_sName;

public:
	AFactory_ShaderResourceView(const std::string &rsName);
	virtual ~AFactory_ShaderResourceView() {}

protected:
	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing()=0;

public:
	ID3D11ShaderResourceView *createShaderResourceView();
};


class CFactory_ShaderResourceView_FromTextureFile : public AFactory_ShaderResourceView {
protected:
	const std::string m_sFileName;
	DirectX::TexMetadata m_MetaData;
	EImageType m_eImageType;
	const UINT m_BindFlags;
	const D3D11_USAGE m_Usage;
	const UINT m_CPUAccessFlags;
	const UINT m_MiscFlag;
	const bool m_bForceSRGB;
	//D3DX11_IMAGE_LOAD_INFO *m_pImageLoadInfo;
	//D3DX11_IMAGE_INFO m_ResultImageInfo;

public:
	CFactory_ShaderResourceView_FromTextureFile(const std::string &rsName, 
												const  std::string &rsFileName ,
												const UINT bindFlags		= D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE	,
												const D3D11_USAGE usage		= D3D11_USAGE::D3D11_USAGE_DEFAULT				,
												const UINT cpuAccessFlags	=0												,
												const UINT miscFlag			=0												,
												const bool bForceSRGB		=false											);
	virtual ~CFactory_ShaderResourceView_FromTextureFile() {}

protected:
	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing() ;

public:
//	D3DX11_IMAGE_INFO *getInfo() { return &m_ResultImageInfo; }
};

class CFactory_ShaderResourceView_FromTextureFiles : public AFactory_ShaderResourceView {
protected:
	const std::string m_sName;
	const std::vector<std::string> m_sFileNames;
	
public:
	CFactory_ShaderResourceView_FromTextureFiles(const std::string &rsName, const std::vector<std::string> &rsFileNames);
	virtual ~CFactory_ShaderResourceView_FromTextureFiles() {}

protected:
	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing();
};


class CFactory_ShaderResourceView_FromTexture2D : public AFactory_ShaderResourceView {
private:
	ID3D11Texture2D *m_pTexture2D;
	DXGI_FORMAT m_Format;

public:
	CFactory_ShaderResourceView_FromTexture2D(const std::string &rsName, ID3D11Texture2D *pTexture2D, D3D11_TEXTURE2D_DESC &rTexture2DDesc);
	CFactory_ShaderResourceView_FromTexture2D(const std::string &rsName, ID3D11Texture2D *pTexture2D, DXGI_FORMAT format);
	virtual ~CFactory_ShaderResourceView_FromTexture2D() {}

protected:
	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing();
};

class CFactory_ShaderResourceView_FromBuffer : public AFactory_ShaderResourceView {
private:
	ID3D11Buffer *m_pBuffer;
	DXGI_FORMAT m_Format;
	const unsigned int m_nNumberOfElement;

public:
	CFactory_ShaderResourceView_FromBuffer(const std::string &rsName, ID3D11Buffer *pBuffer, DXGI_FORMAT format, const unsigned int nNumberOfElement);
	virtual ~CFactory_ShaderResourceView_FromBuffer() {}

protected:
	virtual ID3D11ShaderResourceView *createShaderResourceView_RealProcessing();
};



#endif