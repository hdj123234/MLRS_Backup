#include"stdafx.h"
#include "Factory_DXShader.h"

#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"GlobalDirectXStorage.h"

#ifndef HEADER_STL
#include<string>
#include<locale>
#include <codecvt>
#include <fstream>
#include <iterator>
#endif

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#ifdef _DEBUG
#include<D3DX11core.h>
#include<D3Dcompiler.h>
#include<D3DX11async.h>

#endif
#endif

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_MYLOGCAT


#ifdef _DEBUG
std::string AFactory_Shader::s_sFilenameExtension = std::string(".fx");
#else
std::string AFactory_Shader::s_sFilenameExtension = std::string(".cso");
#endif

AFactory_Shader::AFactory_Shader(const std::string &rsFileName, const std::string  &rsFunctionName, const std::string &rsShaderModel)
	:m_sFileName(rsFileName),
	m_sFunctionName(rsFunctionName),
	m_sModel(rsShaderModel)
{
}

bool AFactory_Shader::loadByteCode()
{
	UINT nSize;
#ifdef _DEBUG 
	ID3DBlob * pShaderBlob=nullptr, *pErrorBlob = nullptr;
	BYTE *pByteCode;
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS |D3DCOMPILE_DEBUG;
	if (FAILED(D3DX11CompileFromFile(converter.from_bytes(g_sShaderFolder + m_sFileName + s_sFilenameExtension).c_str(), NULL, NULL, m_sFunctionName.c_str(), m_sModel.c_str(), dwShaderFlags, 0, NULL, &pShaderBlob, &pErrorBlob, NULL)))
	{
		if (pErrorBlob) {
			std::string err = (char*)(pErrorBlob->GetBufferPointer());
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, err);
			pErrorBlob->Release();
		}
		else
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "AFactory_Shader.loadByteCode(), D3DX11CompileFromFile() Error");

		return false;
	}
	else if (!pShaderBlob || pShaderBlob->GetBufferSize() <= 0)
	{
		return false;

	}
	pByteCode = static_cast<BYTE*>(pShaderBlob->GetBufferPointer());
	nSize = static_cast<UINT>(pShaderBlob->GetBufferSize());
	m_ByteCode.resize(nSize);
	std::copy(pByteCode, pByteCode + nSize, m_ByteCode.begin());

	if (pShaderBlob)
		pShaderBlob->Release();
	if (pErrorBlob)
		pErrorBlob->Release();
#else

	std::ifstream inFile((g_sShaderFolder + m_sFileName + s_sFilenameExtension),std::ios::binary);
	if (inFile.fail())
		return false;

	//ũ����
	inFile.seekg(0,std::ios::end);
	nSize = static_cast<UINT>(inFile.tellg());
	inFile.seekg(0, std::ios::beg);
//	m_ByteCode.resize(nSize);
	//	std::copy(std::istreambuf_iterator<char>(inFile), std::istreambuf_iterator<char>(), m_ByteCode.begin());
	std::copy(std::istreambuf_iterator<char>(inFile), std::istreambuf_iterator<char>(), back_inserter(m_ByteCode));
	inFile.close();
#endif
	return true;
}


//----------------------------------- CFactory_VertexShader -------------------------------------------------------------------------------------
CFactory_VertexShader::CFactory_VertexShader(const std::string &rsFileName, const std::string&rsFunctionName)
	: AFactory_Shader(rsFileName, rsFunctionName, std::string("vs_5_0"))
{

}

ID3D11VertexShader * CFactory_VertexShader::createShader()
{
	ID3D11VertexShader *pShader;
	if (CGlobalDirectXStorage::checkVertexShader(m_sFileName))
		pShader = CGlobalDirectXStorage::getVertexShader(m_sFileName);
	else
	{
		AFactory_Shader::loadByteCode();
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateVertexShader(m_ByteCode.data(), m_ByteCode.size(), NULL, &pShader)))
			return nullptr;
		CGlobalDirectXStorage::setVertexShader(m_sFileName, pShader);
	}
	return pShader;
}

//----------------------------------- CFactory_HullShader -------------------------------------------------------------------------------------

CFactory_HullShader::CFactory_HullShader(const std::string & rsFileName, const std::string & rsFunctionName)
	:AFactory_Shader(rsFileName, rsFunctionName, std::string("hs_5_0"))
{
}

ID3D11HullShader * CFactory_HullShader::createShader()
{
	ID3D11HullShader *pShader;
	if (CGlobalDirectXStorage::checkHullShader(m_sFileName))
		pShader = CGlobalDirectXStorage::getHullShader(m_sFileName);
	else
	{
		AFactory_Shader::loadByteCode();
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateHullShader(m_ByteCode.data(), m_ByteCode.size(), NULL, &pShader)))
			return nullptr;
		CGlobalDirectXStorage::setHullShader(m_sFileName, pShader);
	}
	return pShader;
}

//----------------------------------- CFactory_DomainShader -------------------------------------------------------------------------------------

CFactory_DomainShader::CFactory_DomainShader(const std::string & rsFileName, const std::string & rsFunctionName)
	:AFactory_Shader(rsFileName, rsFunctionName, std::string("ds_5_0"))
{
}

ID3D11DomainShader * CFactory_DomainShader::createShader()
{
	ID3D11DomainShader *pShader;
	if (CGlobalDirectXStorage::checkDomainShader(m_sFileName))
		pShader = CGlobalDirectXStorage::getDomainShader(m_sFileName);
	else
	{
		AFactory_Shader::loadByteCode();
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateDomainShader(m_ByteCode.data(), m_ByteCode.size(), NULL, &pShader)))
			return nullptr;
		CGlobalDirectXStorage::setDomainShader(m_sFileName, pShader);
	}
	return pShader;
}

//----------------------------------- CFactory_GeometryShader -------------------------------------------------------------------------------------
CFactory_GeometryShader::CFactory_GeometryShader(const std::string &rsFileName, const std::string &rsFunctionName)
	:AFactory_Shader(rsFileName, rsFunctionName, std::string("gs_5_0"))
{
}

ID3D11GeometryShader * CFactory_GeometryShader::createShader()
{
	ID3D11GeometryShader *pShader;
	if (CGlobalDirectXStorage::checkGeometryShader(m_sFileName))
		pShader = CGlobalDirectXStorage::getGeometryShader(m_sFileName);
	else
	{
		AFactory_Shader::loadByteCode();
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateGeometryShader(	m_ByteCode.data(), m_ByteCode.size(), NULL, &pShader)))
			return nullptr;
		CGlobalDirectXStorage::setGeometryShader(m_sFileName, pShader);
	}
	return pShader;
}

//----------------------------------- CFactory_PixelShader -------------------------------------------------------------------------------------
CFactory_PixelShader::CFactory_PixelShader(const std::string &rsFileName, const std::string &rsFunctionName)
	:AFactory_Shader(rsFileName, rsFunctionName, std::string("ps_5_0"))
{
}

ID3D11PixelShader * CFactory_PixelShader::createShader()
{
	ID3D11PixelShader *pShader;

	if (CGlobalDirectXStorage::checkPixelShader(m_sFileName))
		pShader = CGlobalDirectXStorage::getPixelShader(m_sFileName);
	else
	{
		AFactory_Shader::loadByteCode();
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreatePixelShader(m_ByteCode.data(), m_ByteCode.size(), NULL, &pShader)))
			return nullptr;
		CGlobalDirectXStorage::setPixelShader(m_sFileName, pShader);
	}
	return pShader;
}



//----------------------------------- CFactory_InputLayout -------------------------------------------------------------------------------------

CFactory_InputLayout::CFactory_InputLayout(const std::string & rsFileName, const std::string & rsFunctionName, const std::string & rsName, std::vector<D3D11_INPUT_ELEMENT_DESC>& rInputElementDescs)
	:AFactory_Shader(rsFileName, rsFunctionName, std::string("vs_5_0")),
	m_sName(rsName),
	m_InputElementDescs(rInputElementDescs)
{
}

ID3D11InputLayout * CFactory_InputLayout::createInputLayout()
{
	ID3D11InputLayout *pInputLayout;
	if (CGlobalDirectXStorage::checkInputLayout(m_sName))
		pInputLayout = CGlobalDirectXStorage::getInputLayout(m_sName);
	else
	{
		AFactory_Shader::loadByteCode();
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateInputLayout(m_InputElementDescs.data(),static_cast<UINT>( m_InputElementDescs.size()), m_ByteCode.data(), m_ByteCode.size(), &pInputLayout)))
			return nullptr;
		CGlobalDirectXStorage::setInputLayout(m_sName, pInputLayout);
	}
	return pInputLayout;
}

CFactory_GeometryShaderForStreamOutput::CFactory_GeometryShaderForStreamOutput(const std::string & rsFileName, const std::string & rsFunctionName, std::vector<D3D11_SO_DECLARATION_ENTRY> soDeclarations, std::vector<UINT> nBufferStrides, UINT nRasterizerStream)
	:AFactory_Shader(rsFileName, rsFunctionName, std::string("gs_5_0")),
	m_SODeclarations( soDeclarations),
	m_nBufferStrides( nBufferStrides),
	m_nRasterizerStream(nRasterizerStream)
{
}

ID3D11GeometryShader * CFactory_GeometryShaderForStreamOutput::createShader()
{
	ID3D11GeometryShader *pShader;
	if (CGlobalDirectXStorage::checkGeometryShader(m_sFileName))
		pShader = CGlobalDirectXStorage::getGeometryShader(m_sFileName);
	else
	{
		AFactory_Shader::loadByteCode();
		if (FAILED(CGlobalDirectXDevice::getDevice()->CreateGeometryShaderWithStreamOutput(m_ByteCode.data(), m_ByteCode.size(),
			m_SODeclarations.data(), static_cast<UINT>(m_SODeclarations.size()),m_nBufferStrides.data(), static_cast<UINT>(m_nBufferStrides.size()), m_nRasterizerStream,NULL, &pShader)))
			return nullptr;
		CGlobalDirectXStorage::setGeometryShader(m_sFileName, pShader);
	}
	return pShader;
}
