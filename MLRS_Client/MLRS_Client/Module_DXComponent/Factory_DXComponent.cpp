#include"stdafx.h"
#include "Factory_DXComponent.h"

#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"
#include"GlobalDirectXStorage.h"

#ifndef HEADER_DIRECTX
#include<D3DX11.h>
#endif


#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif




//----------------------------------- CFactory_DepthStencilState -------------------------------------------------------------------------------------

CFactory_DepthStencilState::CFactory_DepthStencilState(const std::string & rsName, D3D11_DEPTH_STENCIL_DESC & rDepthStencilDesc)
	:m_sName(rsName),
	m_DepthStencilDesc(rDepthStencilDesc)
{
}

ID3D11DepthStencilState * CFactory_DepthStencilState::createDepthStencilState()
{
	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();

	//DepthStencilState 持失
	if (CGlobalDirectXStorage::checkDepthStencilState(m_sName))
		return  CGlobalDirectXStorage::getDepthStencilState(m_sName);
	else
	{
		ID3D11DepthStencilState *pDepthStencilState = nullptr;
		if (FAILED(pDevice->CreateDepthStencilState(&m_DepthStencilDesc, &pDepthStencilState)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_DepthStencilView.build(), CreateDepthStencilView()");
			return false;
		}
		CGlobalDirectXStorage::setDepthStencilState(m_sName, pDepthStencilState);
		return pDepthStencilState;
	}
}

//----------------------------------- CFactory_RasterizerState -------------------------------------------------------------------------------------

CFactory_RasterizerState::CFactory_RasterizerState(const std::string & rsName, D3D11_RASTERIZER_DESC & rRasterizerDesc)
	:m_sName(rsName),
	m_RasterizerDesc(rRasterizerDesc)
{
}

ID3D11RasterizerState * CFactory_RasterizerState::createRasterizerState()
{

	if (CGlobalDirectXStorage::checkRasterizerState(m_sName))
		return CGlobalDirectXStorage::getRasterizerState(m_sName);
	else
	{
		ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
		ID3D11RasterizerState *pRasterizerState = nullptr;
		if (FAILED(pDevice->CreateRasterizerState(&m_RasterizerDesc, &pRasterizerState)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_RasterizerState.createRasterizerState(), CreateRasterizerState() Error");
			return nullptr;
		}
		CGlobalDirectXStorage::setRasterizerState(m_sName, pRasterizerState);
		return pRasterizerState;
	}
}

//----------------------------------- CFactory_SamplerState -------------------------------------------------------------------------------------
CFactory_SamplerState::CFactory_SamplerState(const std::string &rsName, D3D11_SAMPLER_DESC &rSamplerDesc)
	:m_sName(rsName),
	m_SamplerDesc(rSamplerDesc)
{
}

ID3D11SamplerState * CFactory_SamplerState::createSamplerState()
{
	if (CGlobalDirectXStorage::checkSamplerState(m_sName))
		return CGlobalDirectXStorage::getSamplerState(m_sName);

	ID3D11SamplerState * pSamplerState;
	if (FAILED(CGlobalDirectXDevice::getDevice()->CreateSamplerState(&m_SamplerDesc, &pSamplerState)))
		return nullptr;
	CGlobalDirectXStorage::setSamplerState(m_sName, pSamplerState);
	return pSamplerState;

}


//----------------------------------- AFactory_DepthStencilView -------------------------------------------------------------------------------------


const std::string AFactory_DepthStencilView::s_sPrefix_View = std::string("DSV_");


AFactory_DepthStencilView::AFactory_DepthStencilView(const std::string & rsName,const DXGI_FORMAT format,const UINT flag_DSV)
	:m_sName(rsName),
	m_Format(format),
	m_Flag_DSV( flag_DSV)
{
}

ID3D11DepthStencilView * AFactory_DepthStencilView::createDepthStencilView_RealProcessing(ID3D11Texture2D *pDepthStencilBuffer)
{
	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();
	//DepthStencilView 持失
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));
	depthStencilViewDesc.Format = m_Format;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;
	depthStencilViewDesc.Flags = m_Flag_DSV;
	if (CGlobalDirectXStorage::checkDepthStencilView(s_sPrefix_View + m_sName))
		return  CGlobalDirectXStorage::getDepthStencilView(s_sPrefix_View + m_sName);
	else
	{
		ID3D11DepthStencilView *pDepthStencilView = nullptr;
		if (FAILED(pDevice->CreateDepthStencilView(dynamic_cast<ID3D11Resource*>(pDepthStencilBuffer), &depthStencilViewDesc, &pDepthStencilView)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_DepthStencilView.build(), CreateDepthStencilView()");
			return false;
		}
		CGlobalDirectXStorage::setDepthStencilView(s_sPrefix_View + m_sName, pDepthStencilView);
		return pDepthStencilView;
	}
}

////----------------------------------- CFactory_DepthStencilView_FromTexture2D -------------------------------------------------------------------------------------
//

const std::string CFactory_DepthStencilView::s_sPrefix_Buffer = std::string("DSB_");

CFactory_DepthStencilView_FromTexture2D::CFactory_DepthStencilView_FromTexture2D(
	const std::string & rsName, 
	ID3D11Texture2D * pDepthStencilBuffer, 
	const DXGI_FORMAT format,
	const UINT flag_DSV)
	:AFactory_DepthStencilView(rsName, format, flag_DSV),
	m_pDepthStencilBuffer(pDepthStencilBuffer)
{
}

ID3D11DepthStencilView * CFactory_DepthStencilView_FromTexture2D::createDepthStencilView()
{
	return  AFactory_DepthStencilView::createDepthStencilView_RealProcessing(m_pDepthStencilBuffer);
}

//----------------------------------- CFactory_DepthStencilView -------------------------------------------------------------------------------------


CFactory_DepthStencilView::CFactory_DepthStencilView(
	const std::string &rsViewName,
	const std::string &rsBufferName, 
	D3D11_TEXTURE2D_DESC & rDepthStencilBufferDesc,
	const UINT flag_DSV )
	:AFactory_DepthStencilView(rsViewName, DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT, flag_DSV),
	m_DepthStencilBufferDesc(rDepthStencilBufferDesc),
	m_pDSB(nullptr),
	m_sBufferName(rsBufferName)
{
}

bool CFactory_DepthStencilView::createDepthStencilBuffer_RealProcessing()
{
	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();

	//DepthStencilBuffer 持失
	ID3D11Texture2D *pDepthStencilBuffer = nullptr;
	if (CGlobalDirectXStorage::checkTexture2D(s_sPrefix_Buffer + m_sBufferName))
		pDepthStencilBuffer = CGlobalDirectXStorage::getTexture2D(s_sPrefix_Buffer + m_sBufferName);
	else
	{
		if (FAILED(pDevice->CreateTexture2D(&m_DepthStencilBufferDesc, NULL, &pDepthStencilBuffer)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_DepthStencilView.build(), CreateTexture2D()");
			return false;
		}
		CGlobalDirectXStorage::setTexture2D(s_sPrefix_Buffer + m_sBufferName, pDepthStencilBuffer);
	}
	m_pDSB = pDepthStencilBuffer;
	return true;
}

ID3D11Texture2D * CFactory_DepthStencilView::createDepthStencilBuffer()
{
	if(!m_pDSB && !CFactory_DepthStencilView::createDepthStencilBuffer_RealProcessing())
		return nullptr;
	else return m_pDSB;
}

ID3D11DepthStencilView * CFactory_DepthStencilView::createDepthStencilView()
{
	if (!m_pDSB && !CFactory_DepthStencilView::createDepthStencilBuffer_RealProcessing())
		return nullptr;


	return  AFactory_DepthStencilView::createDepthStencilView_RealProcessing(m_pDSB);
	
}

//----------------------------------- AFactory_RenderTargetView -------------------------------------------------------------------------------------

AFactory_RenderTargetView::AFactory_RenderTargetView(const std::string & rsName)
	: m_sName(rsName)
{
}

ID3D11RenderTargetView * AFactory_RenderTargetView::createRenderTargetView()
{
	if (CGlobalDirectXStorage::checkRenderTargetView(m_sName))
		return CGlobalDirectXStorage::getRenderTargetView(m_sName);

	ID3D11RenderTargetView *pResult = this->createRenderTargetView_RealProcessing();

	CGlobalDirectXStorage::setRenderTargetView(m_sName, pResult);

	return pResult;
}


//----------------------------------- CFactory_RenderTargetView_BackBuffer -------------------------------------------------------------------------------------

ID3D11RenderTargetView * CFactory_RenderTargetView_BackBuffer::createRenderTargetView_RealProcessing()
{
	if (CGlobalDirectXStorage::checkRenderTargetView(m_sName))
		return CGlobalDirectXStorage::getRenderTargetView(m_sName);

	ID3D11RenderTargetView *pResult;
	ID3D11Buffer *pBackBuffer;
	if (FAILED(CGlobalDirectXDevice::getSwapChain()->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_RenderTargetView_BackBuffer.createRenderTargetView(), GetBuffer() Error");
		return nullptr;
	}
	if (FAILED(CGlobalDirectXDevice::getDevice()->CreateRenderTargetView(pBackBuffer, NULL, &pResult)))
	{
		pBackBuffer->Release();
		return nullptr;
	}
	pBackBuffer->Release();
	CGlobalDirectXStorage::setRenderTargetView(m_sName, pResult);
	return pResult;
}

CFactory_RenderTargetView_BackBuffer::CFactory_RenderTargetView_BackBuffer(const std::string &rsName)
	: AFactory_RenderTargetView(rsName)
{
}


//----------------------------------- CFactory_RenderTargetView_FromTexture2D -------------------------------------------------------------------------------------
ID3D11RenderTargetView * CFactory_RenderTargetView_FromTexture2D::createRenderTargetView_RealProcessing()
{
	if (CGlobalDirectXStorage::checkRenderTargetView(m_sName))
		return CGlobalDirectXStorage::getRenderTargetView(m_sName);

	ID3D11RenderTargetView *pResult;

	D3D11_RENDER_TARGET_VIEW_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	
	desc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2D;
	desc.Format = m_Format;
	desc.Texture2D.MipSlice = 0;
	
	if (FAILED(CGlobalDirectXDevice::getDevice()->CreateRenderTargetView(m_pTexture2D, &desc, &pResult)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_RenderTargetView_FromTexture2D.createRenderTargetView_RealProcessing(), CreateRenderTargetView() Error");
		pResult =  nullptr;
	}
	CGlobalDirectXStorage::setRenderTargetView(m_sName, pResult);
	return pResult;
}

CFactory_RenderTargetView_FromTexture2D::CFactory_RenderTargetView_FromTexture2D(const std::string & rsName, ID3D11Texture2D *pTexture2D, DXGI_FORMAT format)
	: AFactory_RenderTargetView(rsName),
	m_pTexture2D(pTexture2D),
	m_Format(format)
{
}

CFactory_RenderTargetView_FromTexture2D::CFactory_RenderTargetView_FromTexture2D(const std::string & rsName, ID3D11Texture2D *pTexture2D, D3D11_TEXTURE2D_DESC const & desc)
	: AFactory_RenderTargetView(rsName),
	m_pTexture2D(pTexture2D),
	m_Format(desc.Format)
{
}


//----------------------------------- CFactory_BlendState -------------------------------------------------------------------------------------
CFactory_BlendState::CFactory_BlendState(const std::string & rsName, D3D11_BLEND_DESC & rBlendDesc)
	:m_sName(rsName),
	m_BlendDesc(rBlendDesc)
{
}

ID3D11BlendState * CFactory_BlendState::createBlendState()
{
	if (CGlobalDirectXStorage::checkBlendState(m_sName))
		return CGlobalDirectXStorage::getBlendState(m_sName);

	ID3D11BlendState *pResult;
	if (FAILED(CGlobalDirectXDevice::getDevice()->CreateBlendState(&m_BlendDesc, &pResult)))		return nullptr;
	CGlobalDirectXStorage::setBlendState(m_sName, pResult);
	return pResult;
}

