#pragma once
#ifndef HEADER_TYPE_INDIRECTX
#define HEADER_TYPE_INDIRECTX

#ifndef HEADER_WINDOWS
#include <Windows.h>
#endif

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

static const unsigned int MAX_LIGHT = 100;
static const unsigned int  LIGHTTYPE_DIRECTIONAL = 0;
static const unsigned int  LIGHTTYPE_POINT		=1;
static const unsigned int  LIGHTTYPE_SPOT = 2;
static const unsigned int  MAX_BONE = 52;

struct Type_Material {
	DirectX::XMFLOAT4 m_vAmbient;
	DirectX::XMFLOAT4 m_vDiffuse;
	DirectX::XMFLOAT4 m_vSpecular;
};

struct Type_Light {
	unsigned int m_nType;	BYTE dummy1[12];//0번만 의미 있음
	DirectX::XMFLOAT4 m_vAmbient;
	DirectX::XMFLOAT4 m_vDiffuse;
	DirectX::XMFLOAT4 m_vSpecular;
	DirectX::XMFLOAT3 m_vPosition;	BYTE dummy2[4];		
	DirectX::XMFLOAT3 m_vDirection;	BYTE dummy3[4];		
	DirectX::XMFLOAT4 m_vAttenuation;	// w = MaxRange
	DirectX::XMFLOAT3 m_vSpotlightData;	BYTE dummy4[4];	//x = Theta, y = Phi, z = Falloff	
};

struct Type_GlobalLighting {
	unsigned int m_nNumberOfLight; BYTE dummy1[12];	
	DirectX::XMFLOAT4 m_vGlobalAmbient;
	Type_Light m_Lights[MAX_LIGHT];
};


struct Type_CameraState {
	DirectX::XMFLOAT4 m_vPosition;
	DirectX::XMFLOAT4 m_vDirection;
	DirectX::XMFLOAT2 m_vFlat;
};

struct Type_Bone
{
	int m_nParentIndex;
	DirectX::XMFLOAT4X4 m_mTransform_ToLocal;
};

typedef unsigned int Type_AnimIndicatorElement;


struct Type_FogState {
	DirectX::XMFLOAT4 m_vFogColor;
	DirectX::XMFLOAT2 m_vFogRange;
};

struct Type_Fume_SprayPoint {
	DirectX::XMFLOAT3 m_vPosition;
	DirectX::XMFLOAT3 m_vDirection;
};

struct Type_FumeState {
	float m_fVaiationAngle;			
	float m_fInitalSpeed;		
	float m_fSpeedAttenuation;	
};

typedef DirectX::XMFLOAT2 Type_BillboardSize;


#endif
