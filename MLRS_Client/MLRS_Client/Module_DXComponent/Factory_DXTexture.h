#pragma once

#ifndef HEADER_FACTORY_DXTEXTURE
#define HEADER_FACTORY_DXTEXTURE
#include"..\Interface_Global.h"

#include"..\Module_Platform_Windows\GlobalWindows.h"

#ifndef HEADER_DIRECTX
#include<D3DX11.h>
#endif

#ifndef HEADER_DIRECTXTEX
#include<DirectXTex/DirectXTex.h>
#endif


enum EImageType : char { eWIC, eTGA, eDDS };

class CUtility_ImageTypeGetter {
public:
	static const EImageType getImageType(const std::string &rsFileName);
};
//----------------------------------- Default -------------------------------------------------------------------------------------

class Texture1DDesc_Default : public D3D11_TEXTURE1D_DESC {
public:
	Texture1DDesc_Default(
		UINT nSize =0,
		DXGI_FORMAT Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT,
		UINT bindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
		UINT accessFlags = 0,
		D3D11_USAGE usage = D3D11_USAGE_DEFAULT);
	virtual ~Texture1DDesc_Default() {}
};

class Texture2DDesc_Default : public D3D11_TEXTURE2D_DESC {
public:
	Texture2DDesc_Default(
		DXGI_FORMAT Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT,
		UINT bindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
		UINT nWidth = CGlobalWindow::getWidth(),
		UINT nHeight = CGlobalWindow::getHeight(),
		UINT accessFlags = 0,
		D3D11_USAGE usage = D3D11_USAGE_DEFAULT);
	virtual ~Texture2DDesc_Default() {}
};

class ImageLoadInfo_Default : public D3DX11_IMAGE_LOAD_INFO{
public:
	ImageLoadInfo_Default(	DXGI_FORMAT format = DXGI_FORMAT_FROM_FILE,
							UINT bindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
							D3D11_USAGE usage = D3D11_USAGE_DEFAULT,
							UINT accessFlags = 0,
							UINT filter = D3DX11_FILTER_FLAG::D3DX11_FILTER_NONE,
							UINT mipFilter = D3DX11_FILTER_FLAG::D3DX11_FILTER_LINEAR		);
	virtual ~ImageLoadInfo_Default() {}
};

class ImageLoadInfo_FromTextureArray : public D3DX11_IMAGE_LOAD_INFO {
public:
	ImageLoadInfo_FromTextureArray(DXGI_FORMAT format = DXGI_FORMAT_FROM_FILE);
	virtual ~ImageLoadInfo_FromTextureArray() {}
};
//----------------------------------- Class -------------------------------------------------------------------------------------

class CFactory_Texture1D {
protected:
	const std::string m_sName;
	D3D11_TEXTURE1D_DESC m_TextureDesc;

	//virtual function
	virtual ID3D11Texture1D *createTexture1D_RealProcessing();
public:
	CFactory_Texture1D(const std::string &rsName,
		const  D3D11_TEXTURE1D_DESC &rTextureDesc );
	virtual ~CFactory_Texture1D() {}

	virtual ID3D11Texture1D *createTexture1D();

};


class AFactory_Texture2D {
protected:
	const std::string m_sName;

public:
	AFactory_Texture2D(const std::string &rsName);
	virtual ~AFactory_Texture2D() {}

protected:
	//virtual function
	virtual ID3D11Texture2D *createTexture2D_RealProcessing()=0;

public:
	virtual ID3D11Texture2D *createTexture2D();

};

class CFactory_Texture2D : public AFactory_Texture2D {
protected:
	D3D11_TEXTURE2D_DESC m_TextureDesc;

	//virtual function
	virtual ID3D11Texture2D *createTexture2D_RealProcessing();
public:
	CFactory_Texture2D(const std::string &rsName,
		const  D3D11_TEXTURE2D_DESC &rTextureDesc = Texture2DDesc_Default());
	virtual ~CFactory_Texture2D() {}
};

class CFactory_Texture2D_FromFile : public AFactory_Texture2D {
private:

protected:
	const std::string m_sFileName;
	DirectX::TexMetadata m_MetaData;
	const EImageType m_eImageType;
	const UINT m_BindFlags;
	const D3D11_USAGE m_Usage;
	const UINT m_CPUAccessFlags ;
	const UINT m_MiscFlag ;
	const bool m_bForceSRGB;
public:
	CFactory_Texture2D_FromFile(const std::string &rsFileName,
								const UINT bindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
								const D3D11_USAGE usage = D3D11_USAGE::D3D11_USAGE_DEFAULT,
								const UINT accessFlags = 0,
								const UINT miscFlag  = 0,
								const bool bForceSRGB=false);
	virtual ~CFactory_Texture2D_FromFile() {}

protected:
	//override AFactory_Texture2D
	virtual ID3D11Texture2D *createTexture2D_RealProcessing();

public:
	DirectX::TexMetadata getImageInfo() { return m_MetaData; }
};

class CFactory_Texture2DArray_FromFiles : public AFactory_Texture2D {
protected:
	const std::vector<std::string> m_sFileNames;
	D3D11_TEXTURE2D_DESC m_TextureDesc;
public:
	CFactory_Texture2DArray_FromFiles(const std::string &rsName,
		const std::vector<std::string> &rsFileNames,
		const UINT bindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
		const D3D11_USAGE usage = D3D11_USAGE::D3D11_USAGE_DEFAULT,
		const UINT accessFlags = NULL	);
	virtual ~CFactory_Texture2DArray_FromFiles() {}

protected:
	//virtual function
	virtual ID3D11Texture2D *createTexture2D_RealProcessing();

public:
	D3D11_TEXTURE2D_DESC getDesc() { return m_TextureDesc;}
};




#endif