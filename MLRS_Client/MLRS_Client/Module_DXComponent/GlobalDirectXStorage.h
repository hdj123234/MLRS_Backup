#pragma once
#ifndef HEADER_DIRECTXSTORAGE
#define HEADER_DIRECTXSTORAGE

#ifndef HEADER_STL
#include<utility>
#include<map>
#endif

//���漱��
struct ID3D11VertexShader 			;
struct ID3D11HullShader 			;
struct ID3D11DomainShader 			;
struct ID3D11GeometryShader 		;
struct ID3D11PixelShader 			;
struct ID3D11Buffer 				;
struct ID3D11Texture1D 				;
struct ID3D11Texture2D 				;
struct ID3D11ShaderResourceView 	;
struct ID3D11SamplerState 			;
struct ID3D11InputLayout 			;
struct ID3D11RasterizerState		;
struct ID3D11BlendState				;
struct ID3D11DepthStencilState		;
struct ID3D11DepthStencilView		;
struct ID3D11RenderTargetView		;


class CGlobalDirectXStorage {
private:
	static std::map<std::string, ID3D11VertexShader *> s_pVSs;
	static std::map<std::string, ID3D11HullShader *> s_pHSs;
	static std::map<std::string, ID3D11DomainShader *> s_pDSs;
	static std::map<std::string, ID3D11GeometryShader *> s_pGSs;
	static std::map<std::string, ID3D11PixelShader *> s_pPSs;
	static std::map<std::string, ID3D11Buffer *> s_pBuffers;
	static std::map<std::string, ID3D11Texture1D *> s_pTextures_1D;
	static std::map<std::string, ID3D11Texture2D *> s_pTextures_2D;
	static std::map<std::string, ID3D11ShaderResourceView *> s_pShaderResourceViews;
	static std::map<std::string, ID3D11SamplerState *> s_pSamplerStates;
	static std::map<std::string, ID3D11InputLayout *> s_pInputLayouts;
	static std::map<std::string, ID3D11RasterizerState *> s_pRasterizerStates;
	static std::map<std::string, ID3D11BlendState*> s_pBlendStates;
	static std::map<std::string, ID3D11DepthStencilState*> s_pDepthStencilStates;
	static std::map<std::string, ID3D11DepthStencilView*> s_pDepthStencilViews;
	static std::map<std::string, ID3D11RenderTargetView*> s_pRenderTargetViews;
public:
	virtual ~CGlobalDirectXStorage() { clear(); }
	
	static ID3D11VertexShader *			getVertexShader			(const std::string &rsName);
	static ID3D11HullShader *			getHullShader			(const std::string &rsName);
	static ID3D11DomainShader *			getDomainShader			(const std::string &rsName);
	static ID3D11GeometryShader *		getGeometryShader		(const std::string &rsName);
	static ID3D11PixelShader *			getPixelShader			(const std::string &rsName);
	static ID3D11Buffer *				getBuffer				(const std::string &rsName);
	static ID3D11Texture1D *			getTexture1D			(const std::string &rsName);
	static ID3D11Texture2D *			getTexture2D			(const std::string &rsName);
	static ID3D11ShaderResourceView *	getShaderResourceView	(const std::string &rsName);
	static ID3D11SamplerState *			getSamplerState			(const std::string &rsName);
	static ID3D11InputLayout *			getInputLayout			(const std::string &rsName);
	static ID3D11RasterizerState *		getRasterizerState		(const std::string &rsName);
	static ID3D11BlendState*			getBlendState			(const std::string &rsName);
	static ID3D11DepthStencilState*		getDepthStencilState	(const std::string &rsName);
	static ID3D11DepthStencilView*		getDepthStencilView		(const std::string &rsName);
	static ID3D11RenderTargetView*		getRenderTargetView		(const std::string &rsName);
	
	static bool checkVertexShader		(const std::string &rsName);
	static bool checkHullShader			(const std::string &rsName);
	static bool checkDomainShader		(const std::string &rsName);
	static bool checkGeometryShader		(const std::string &rsName);
	static bool checkPixelShader		(const std::string &rsName);
	static bool checkBuffer				(const std::string &rsName);
	static bool checkTexture1D			(const std::string &rsName);
	static bool checkTexture2D			(const std::string &rsName);
	static bool checkShaderResourceView	(const std::string &rsName);
	static bool checkSamplerState		(const std::string &rsName);
	static bool checkInputLayout		(const std::string &rsName);
	static bool	checkRasterizerState	(const std::string &rsName);
	static bool	checkBlendState			(const std::string &rsName);
	static bool	checkDepthStencilState	(const std::string &rsName);
	static bool	checkDepthStencilView	(const std::string &rsName);
	static bool	checkRenderTargetView	(const std::string &rsName);
	
	static void setVertexShader			(const std::string &rsName, ID3D11VertexShader *p);
	static void setHullShader			(const std::string &rsName, ID3D11HullShader *p);
	static void setDomainShader			(const std::string &rsName, ID3D11DomainShader *p);
	static void setGeometryShader		(const std::string &rsName, ID3D11GeometryShader *p);
	static void setPixelShader			(const std::string &rsName, ID3D11PixelShader *p);
	static void setBuffer				(const std::string &rsName, ID3D11Buffer *p);
	static void setTexture1D			(const std::string &rsName, ID3D11Texture1D *p);
	static void setTexture2D			(const std::string &rsName, ID3D11Texture2D *p);
	static void setShaderResourceView	(const std::string &rsName, ID3D11ShaderResourceView *p);
	static void setSamplerState			(const std::string &rsName, ID3D11SamplerState *p);
	static void setInputLayout			(const std::string &rsName, ID3D11InputLayout *p);
	static void setRasterizerState		(const std::string &rsName, ID3D11RasterizerState *	p	);
	static void setBlendState			(const std::string &rsName, ID3D11BlendState*			p	);
	static void setDepthStencilState	(const std::string &rsName, ID3D11DepthStencilState*	p	);
	static void setDepthStencilView		(const std::string &rsName, ID3D11DepthStencilView*	p	);
	static void setRenderTargetView		(const std::string &rsName, ID3D11RenderTargetView*	p	);

	static void resetVertexShader(const std::string &rsName, ID3D11VertexShader *p);
	static void resetHullShader(const std::string &rsName, ID3D11HullShader *p);
	static void resetDomainShader(const std::string &rsName, ID3D11DomainShader *p);
	static void resetGeometryShader(const std::string &rsName, ID3D11GeometryShader *p);
	static void resetPixelShader(const std::string &rsName, ID3D11PixelShader *p);
	static void resetBuffer(const std::string &rsName, ID3D11Buffer *p);
	static void resetTexture1D(const std::string &rsName, ID3D11Texture1D *p);
	static void resetTexture2D(const std::string &rsName, ID3D11Texture2D *p);
	static void resetShaderResourceView(const std::string &rsName, ID3D11ShaderResourceView *p);
	static void resetSamplerState(const std::string &rsName, ID3D11SamplerState *p);
	static void resetInputLayout(const std::string &rsName, ID3D11InputLayout *p);
	static void resetRasterizerState(const std::string &rsName, ID3D11RasterizerState *	p);
	static void resetBlendState(const std::string &rsName, ID3D11BlendState*			p);
	static void resetDepthStencilState(const std::string &rsName, ID3D11DepthStencilState*	p);
	static void resetDepthStencilView(const std::string &rsName, ID3D11DepthStencilView*	p);
	static void resetRenderTargetView(const std::string &rsName, ID3D11RenderTargetView*	p);
	
	
	static void deleteVertexShader			(const std::string &rsName);
	static void deleteHullShader			(const std::string &rsName);
	static void deleteDomainShader			(const std::string &rsName);
	static void deleteGeometryShader		(const std::string &rsName);
	static void deletePixelShader			(const std::string &rsName);
	static void deleteBuffer				(const std::string &rsName);
	static void deleteTexture1D				(const std::string &rsName);
	static void deleteTexture2D				(const std::string &rsName);
	static void deleteShaderResourceView	(const std::string &rsName);
	static void deleteSamplerState			(const std::string &rsName);
	static void deleteInputLayout			(const std::string &rsName);
	static void deleteRasterizerState		(const std::string &rsName);
	static void deleteBlendState			(const std::string &rsName);
	static void deleteDepthStencilState		(const std::string &rsName);
	static void deleteDepthStencilView		(const std::string &rsName);
	static void deleteRenderTargetView		(const std::string &rsName);
	

	static void clear();
};

#endif