#pragma once
#ifndef HEADER_FACTORY_SOUNDMANAGER
#define HEADER_FACTORY_SOUNDMANAGER
 
#include"SoundManager.h"
   
class CFactory_SoundManager: public IFactory_SoundManager {
private:
	const std::string m_sTableFileName;

public:
	CFactory_SoundManager(const std::string &rsTableFileName);
	virtual ~CFactory_SoundManager() {};

	virtual std::vector<CVoiceData *> createVoiceDatas();
};
 
#endif // !HEADER_XAUDIO2
