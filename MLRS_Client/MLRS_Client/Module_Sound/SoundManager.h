#pragma once
#ifndef HEADER_AUDIOMANAGER
#define HEADER_AUDIOMANAGER
 
#include"Interface_Sound.h"

#ifndef HEADER_XAUDIO2
#include<xaudio2.h>
#include<X3daudio.h>
#endif

#ifndef HEADER_STL 
#include<vector>
#include<set> 
#endif // !HEADER_STL


//���漱��
class IMsg;
struct CVoiceData;
class AVoice;
class CVoice_3D;

class IFactory_SoundManager {
public:
	virtual ~IFactory_SoundManager() = 0 {};

	virtual std::vector<CVoiceData *> createVoiceDatas()=0;
};

class CSoundManager : public IGameObject,
						virtual public RIGameObject_Animate{
private:  
	std::vector<CVoiceData *> m_pVoiceDatas;
	std::set<AVoice *> m_pVoices;
	 
	RIGameObject_SoundListener *m_pListener;
//	X3DAUDIO_LISTENER m_3DListener;

	X3DAUDIO_DSP_SETTINGS m_3DDSPSettings;
	std::vector<FLOAT32> m_MatrixCoefficients;

public:
	CSoundManager();
	~CSoundManager() { releaseObjects(); }

private:
	void calc3DVoice(CVoice_3D * pVoice);

public:
	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_ANIMATE; }
	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime) ;

//	void advanceFrame(float fElapsedTime);
	void handleMsg(IMsg_Sound * pMsg);
	void clearAllVoice();
	void setListener(RIGameObject_SoundListener * pListener) { m_pListener = pListener; }

	bool createObjects(IFactory_SoundManager * pFactory);
	void releaseObjects();
};

#endif // !HEADER_XAUDIO2
