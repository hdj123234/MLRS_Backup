#include "stdafx.h"
#include "GlobalXAudio2Engine.h"

#ifndef HEADER_XAUDIO2
#include<XAudio2.h>
#endif // !HEADER_XAUDIO2


 IXAudio2					* CGlobalXAudio2Engine::s_pXAudio = nullptr;
 IXAudio2MasteringVoice		* CGlobalXAudio2Engine::s_pMasterVoice = nullptr;
 X3DAUDIO_HANDLE			  CGlobalXAudio2Engine::s_hX3DAudio ;
 unsigned int				  CGlobalXAudio2Engine::s_nChannels = 0;

 void CGlobalXAudio2Engine::setMasterVolume(const float fVolume)
 {
	 s_pMasterVoice->SetVolume(fVolume);
 }
