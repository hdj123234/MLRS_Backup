#include "stdafx.h"
#include "Factory_SoundManager.h"

#include "SoundManager.h"
#include"..\MyCSVTable.h"
#include"VoiceDataBuilder.h"
#include"VoiceData.h"

 
//---------------------------------------- CFactory_SoundManager ------------------------------------------


CFactory_SoundManager::CFactory_SoundManager(const std::string & rsTableFileName)
	: m_sTableFileName(rsTableFileName)
{
}

std::vector<CVoiceData*> CFactory_SoundManager::createVoiceDatas()
{
	std::vector<CVoiceData*> retval;
	CMyCSVTable table(g_sDataFolder + m_sTableFileName);
	auto &rTable = table.getTable();
	const std::string sPath = CUtility_Path::getPathOnly(g_sDataFolder + m_sTableFileName);

	for (auto &rLabel : rTable)
	{
		const EVoiceDataType eVoiceDataType = CGlobalVoiceDataTypeConverter::getType(rLabel[4]);
		bool bLoop = false;
		float fVolumnRate = std::stof(rLabel[2]);
		float fCurveRate = std::stof(rLabel[3]);

		switch (eVoiceDataType)
		{
		case EVoiceDataType::eBGM:
			bLoop = true;
			break;
		case EVoiceDataType::eSE:
			bLoop = false;
			break;
		}

		CVoiceData* pVoiceData = CGlobalVoiceDataBuilder::buildVoice(sPath+rLabel[1] );
		if (bLoop) 
			pVoiceData->m_Buffer.LoopCount = XAUDIO2_LOOP_INFINITE;
		pVoiceData->m_fVolumeRate = fVolumnRate;
		pVoiceData->m_fCurveRate = fCurveRate;

		retval.push_back (pVoiceData);
	}


	return std::move(retval);
}
