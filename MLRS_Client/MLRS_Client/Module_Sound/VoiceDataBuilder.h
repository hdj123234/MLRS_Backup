#pragma once
#ifndef HEADER_VOICEDATABUILDER
#define HEADER_VOICEDATABUILDER

#include"Voice.h" 
#include"..\MyUtility.h"

#ifndef HEADER_WINDOWS
#include<windows.h> 
#endif // !HEADER_WINDOWS

#ifndef HEADER_XAUDIO2
#include<xaudio2.h>
#include<X3daudio.h>
#endif
 
enum EVoiceDataType {
	eBGM,
	eSE,
	eUnknown
};
class CGlobalVoiceDataTypeConverter {
public:
	static EVoiceDataType getType(const std::wstring &rsName);
	static EVoiceDataType getType(const std::string &rsName);
};
class CGlobalVoiceDataBuilder {  
private:
	static CVoiceData * buildVoice_RealProcessing_wav(const std::wstring &rsFileName);
	static CVoiceData * buildVoice_RealProcessing_ogg(const std::wstring &rsFileName);
	static bool buildVoice_PostProcessing(CVoiceData * pVoice );
	
public:
	static CVoiceData * buildVoice(const std::string &rsFileName ){ return buildVoice(CUtility_String::convertToWString(rsFileName));	}
	static CVoiceData * buildVoice(const std::wstring &rsFileName );
	 
};
 

//��ó : MSDN ( https://msdn.microsoft.com/ko-kr/library/windows/desktop/ee415781(v=vs.85)/ ) 

class CUtility_WaveReader {
public: 

	//To find a chunk in a RIFF file :
#ifdef _XBOX //Big-Endian
	static const DWORD fourccRIFF = 'RIFF';
	static const DWORD fourccDATA = 'data';
	static const DWORD fourccFMT = 'fmt ' ;
	static const DWORD fourccWAVE = 'WAVE';
	static const DWORD fourccXWMA = 'XWMA';
	static const DWORD fourccDPDS = 'dpds';
#endif

#ifndef _XBOX //Little-Endian
	static const DWORD fourccRIFF = 'FFIR';
	static const DWORD fourccDATA = 'atad';
	static const DWORD fourccFMT = ' tmf';
	static const DWORD fourccWAVE = 'EVAW';
	static const DWORD fourccXWMA = 'AMWX';
	static const DWORD fourccDPDS = 'sdpd';
#endif


	static HRESULT FindChunk(HANDLE hFile, DWORD fourcc, DWORD & dwChunkSize, DWORD & dwChunkDataPosition)
	{
		HRESULT hr = S_OK;
		if (INVALID_SET_FILE_POINTER == SetFilePointer(hFile, 0, NULL, FILE_BEGIN))
			return HRESULT_FROM_WIN32(GetLastError());

		DWORD dwChunkType;
		DWORD dwChunkDataSize;
		DWORD dwRIFFDataSize = 0;
		DWORD dwFileType;
		DWORD bytesRead = 0;
		DWORD dwOffset = 0;

		while (hr == S_OK)
		{
			DWORD dwRead;
			if (0 == ReadFile(hFile, &dwChunkType, sizeof(DWORD), &dwRead, NULL))
				hr = HRESULT_FROM_WIN32(GetLastError());

			if (0 == ReadFile(hFile, &dwChunkDataSize, sizeof(DWORD), &dwRead, NULL))
				hr = HRESULT_FROM_WIN32(GetLastError());

			switch (dwChunkType)
			{
			case fourccRIFF:
				dwRIFFDataSize = dwChunkDataSize;
				dwChunkDataSize = 4;
				if (0 == ReadFile(hFile, &dwFileType, sizeof(DWORD), &dwRead, NULL))
					hr = HRESULT_FROM_WIN32(GetLastError());
				break;

			default:
				if (INVALID_SET_FILE_POINTER == SetFilePointer(hFile, dwChunkDataSize, NULL, FILE_CURRENT))
					return HRESULT_FROM_WIN32(GetLastError());
			}

			dwOffset += sizeof(DWORD) * 2;

			if (dwChunkType == fourcc)
			{
				dwChunkSize = dwChunkDataSize;
				dwChunkDataPosition = dwOffset;
				return S_OK;
			}

			dwOffset += dwChunkDataSize;

			if (bytesRead >= dwRIFFDataSize) return S_FALSE;

		}

		return S_OK;

	}


	//To read data in a chunk after it has been located.
	//Once a desired chunk is found, its data can be read by adjusting the file pointer to the beginning of the data section of the chunk.A function to read the data from a chunk once it is found might look like this.

	static HRESULT ReadChunkData(HANDLE hFile, void * buffer, DWORD buffersize, DWORD bufferoffset)
	{
		HRESULT hr = S_OK;
		if (INVALID_SET_FILE_POINTER == SetFilePointer(hFile, bufferoffset, NULL, FILE_BEGIN))
			return HRESULT_FROM_WIN32(GetLastError());
		DWORD dwRead;
		if (0 == ReadFile(hFile, buffer, buffersize, &dwRead, NULL))
			hr = HRESULT_FROM_WIN32(GetLastError());
		return hr;
	}
};



#endif // !HEADER_XAUDIO2
