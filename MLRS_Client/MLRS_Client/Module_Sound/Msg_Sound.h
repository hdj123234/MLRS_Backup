#pragma once
#ifndef HEADER_MSG_SOUND
#define HEADER_MSG_SOUND
 
#include"Interface_Sound.h"
  
enum EMsgType_Sound :ENUMTYPE_256{
	eSound_RunBGM, 
	eSound_RunSE,
	eSound_Run3DSE,
};


class CMsg_Sound : public IMsg_Sound {
private:
	EMsgType_Sound m_eMsgType;
public:
	CMsg_Sound(const EMsgType_Sound eMsgType)
		:m_eMsgType(eMsgType){}
	virtual ~CMsg_Sound() {}
	
	virtual const EMsgType_Sound getMsgType()const { return m_eMsgType; }
};


class CMsg_Sound_RunBGM : public CMsg_Sound {
private:
	unsigned int nID_BGM;
public:
	CMsg_Sound_RunBGM(const unsigned int nID)
		:CMsg_Sound(eSound_RunBGM),
		nID_BGM(nID){}
	virtual ~CMsg_Sound_RunBGM() {}

	const unsigned int getID()const { return nID_BGM; }
};


class CMsg_Sound_RunSE : public CMsg_Sound {
private:
	unsigned int nID_SE; 
public:
	CMsg_Sound_RunSE(const unsigned int nID )
		:CMsg_Sound(eSound_RunSE),
		nID_SE(nID) {}
	virtual ~CMsg_Sound_RunSE() {}

	const unsigned int getID()const { return nID_SE; } 
};


class CMsg_Sound_Run3DSE : public CMsg_Sound {
private:
	unsigned int nID_SE;
	DirectX::XMFLOAT3 m_vPosition;
public:
	CMsg_Sound_Run3DSE(const unsigned int nID, const DirectX::XMFLOAT3 &rvPosition)
		:CMsg_Sound(eSound_Run3DSE),
		nID_SE(nID),
		m_vPosition(rvPosition) {}
	virtual ~CMsg_Sound_Run3DSE() {}

	const unsigned int getID()const { return nID_SE; }
	const DirectX::XMFLOAT3 & getPos()const { return m_vPosition; }
};

#endif