#include "stdafx.h"
#include "Voice.h"

#include"VoiceData.h"
#include"GlobalXAudio2Engine.h"

#ifndef HEADER_WINDOWS
#include<Windows.h>
#endif // !HEADER_WINDOWS

AVoice::AVoice(CVoiceData * pVoiceData, const EVoiceType eVoiceType)
	:m_pVoiceData(pVoiceData),
	m_pSourceVoice(nullptr),
	m_eVoiceType(eVoiceType),
	m_bEnd(false)
{  
	if (FAILED(CGlobalXAudio2Engine::getXAudio2Engine()
		->CreateSourceVoice(&m_pSourceVoice,
			&m_pVoiceData->m_WaveFormatEx,
			0, 2,
			this)))
		abort();
	if (FAILED(m_pSourceVoice->SubmitSourceBuffer(&pVoiceData->m_Buffer)))
		abort();

	m_pSourceVoice->SetVolume(pVoiceData->m_fVolumeRate);
}

CVoice_Normal::CVoice_Normal(CVoiceData * pVoiceData)
	:AVoice(pVoiceData,EVoiceType::eVoiceType_BGM)
{
}

 
CVoice_3D::CVoice_3D(CVoiceData * pVoiceData, const DirectX::XMFLOAT3 & rvPos)
	:AVoice(pVoiceData, EVoiceType::eVoiceType_3D),
	m_3DEmitter({ 0 })
{
	DirectX::XMFLOAT3 vTmp;
	m_3DEmitter.ChannelCount = 1;
	m_3DEmitter.CurveDistanceScaler = pVoiceData->m_fCurveRate;
	m_3DEmitter.Position.x = rvPos.x;
	m_3DEmitter.Position.y = rvPos.y;
	m_3DEmitter.Position.z = rvPos.z;

	vTmp = DirectX::XMFLOAT3(0, 0, 1);
	m_3DEmitter.OrientFront.x = vTmp.x;
	m_3DEmitter.OrientFront.y = vTmp.y;
	m_3DEmitter.OrientFront.z = vTmp.z;
	vTmp = DirectX::XMFLOAT3(0, 1, 0);
	m_3DEmitter.OrientTop.x = vTmp.x;
	m_3DEmitter.OrientTop.y = vTmp.y;
	m_3DEmitter.OrientTop.z = vTmp.z;
	vTmp = DirectX::XMFLOAT3(0, 0, 0);
	m_3DEmitter.Velocity.x = vTmp.x;
	m_3DEmitter.Velocity.y = vTmp.y;
	m_3DEmitter.Velocity.z = vTmp.z;
}
