#include "stdafx.h"
#include "VoiceDataBuilder.h"

#include"VoiceData.h"

#ifndef HEADER_WINDOWS
#include<Windows.h>
#endif // !HEADER_WINDOWS

#ifndef HEADER_STL
#include<algorithm>
#include<cwctype>
#endif // !HEADER_STL

#ifndef HEADER_OGG
#include<vorbis\vorbisfile.h>
#include<vorbis\codec.h>
#endif // !HEADER_OGG
 

//---------------------------------------- CGlobalVoiceDataBuilder ------------------------------------------
 
CVoiceData * CGlobalVoiceDataBuilder::buildVoice_RealProcessing_wav(const std::wstring & rsFileName)
{
	CVoiceData *pRetval = new CVoiceData;

	// MSDN 코드 일부 수정
	// 출처 : MSDN ( https://msdn.microsoft.com/ko-kr/library/windows/desktop/ee415781(v=vs.85)/ )

	HANDLE hFile = CreateFile(rsFileName.c_str(),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		0,
		NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		delete pRetval;
		return nullptr;
	}


	if (INVALID_SET_FILE_POINTER == SetFilePointer(hFile, 0, NULL, FILE_BEGIN))
	{
		CloseHandle(hFile);
		delete pRetval;
		return nullptr;
	}

	DWORD dwChunkSize;
	DWORD dwChunkPosition;
	//check the file type, should be fourccWAVE or 'XWMA'
	CUtility_WaveReader::FindChunk(hFile, CUtility_WaveReader::fourccRIFF, dwChunkSize, dwChunkPosition);
	DWORD filetype = 0;
	CUtility_WaveReader::ReadChunkData(hFile, &filetype, sizeof(DWORD), dwChunkPosition);
	if (filetype != CUtility_WaveReader::fourccWAVE)
	{
		CloseHandle(hFile);
		delete pRetval;
		return nullptr;
	}


	//Locate the 'fmt ' chunk, and copy its contents into a WAVEFORMATEXTENSIBLE structure.

	CUtility_WaveReader::FindChunk(hFile, CUtility_WaveReader::fourccFMT, dwChunkSize, dwChunkPosition);
	CUtility_WaveReader::ReadChunkData(hFile, &pRetval->m_WaveFormatEx, dwChunkSize, dwChunkPosition);


	//Locate the 'data' chunk, and read its contents into a buffer.

	//fill out the audio data buffer with the contents of the fourccDATA chunk
	CUtility_WaveReader::FindChunk(hFile, CUtility_WaveReader::fourccDATA, dwChunkSize, dwChunkPosition);
	pRetval->m_Data.resize(dwChunkSize);
	//	BYTE * pDataBuffer = new BYTE[dwChunkSize];
	CUtility_WaveReader::ReadChunkData(hFile, pRetval->m_Data.data(), dwChunkSize, dwChunkPosition);

	//Populate an XAUDIO2_BUFFER structure.

	pRetval->m_Buffer.AudioBytes = dwChunkSize;  //buffer containing audio data
	
	return pRetval;
}

CVoiceData * CGlobalVoiceDataBuilder::buildVoice_RealProcessing_ogg(const std::wstring & rsFileName)
{
	// Check
	CVoiceData *pRetval = new CVoiceData;
	 
	OggVorbis_File vf;
	int eof = 0;
	int current_section;
	FILE * fp = nullptr; 
	
	fopen_s(&fp, std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>>().to_bytes(rsFileName).c_str(), "rb");
	 
	if (ov_open_callbacks(fp, &vf, NULL, 0, OV_CALLBACKS_NOCLOSE) < 0) {
		fprintf(stderr, "Input does not appear to be an Ogg bitstream.\n");
		fclose(fp);
		exit(1);
	}
	/* Throw the comments plus a few lines about the bitstream we're
	decoding */
	
	char **ptr = ov_comment(&vf, -1)->user_comments;
	vorbis_info *vi = ov_info(&vf, -1);
	while (*ptr) {
		printf("%s\n", *ptr);
		++ptr;
	}
	printf("\nBitstream is %d channel, %ldHz\n", vi->channels, vi->rate);
	printf("\nDecoded length: %ld samples\n",
		(long)ov_pcm_total(&vf, -1));
	printf("Encoded by: %s\n\n", ov_comment(&vf, -1)->vendor);

	pRetval->m_WaveFormatEx.wFormatTag = WAVE_FORMAT_PCM;
	pRetval->m_WaveFormatEx.nChannels = vi->channels;
	pRetval->m_WaveFormatEx.nSamplesPerSec = vi->rate;
	pRetval->m_WaveFormatEx.wBitsPerSample = 16;	// Check
	pRetval->m_WaveFormatEx.nBlockAlign = pRetval->m_WaveFormatEx.nChannels * (pRetval->m_WaveFormatEx.wBitsPerSample/8);	// Check
	pRetval->m_WaveFormatEx.nAvgBytesPerSec = pRetval->m_WaveFormatEx.nSamplesPerSec * pRetval->m_WaveFormatEx.nBlockAlign;
	pRetval->m_WaveFormatEx.cbSize = 0; 

	auto nTotalSize = ov_pcm_total(&vf, -1)*pRetval->m_WaveFormatEx.nBlockAlign;
	pRetval->m_Data.resize(nTotalSize);
	//Populate an XAUDIO2_BUFFER structure.
	int nCount = 0;
	const unsigned int nReadMaxSize = 4096;
	auto iter = pRetval->m_Data.begin();
	while (!eof) {
		++nCount;
		long ret = ov_read(&vf, reinterpret_cast<char *>(iter._Ptr), nReadMaxSize, 0, 2, 1, &current_section);
		if (ret == 0) {
			/* EOF */
			eof = 1;
		}
		else if (ret < 0) {
			if (ret == OV_EBADLINK) {
				printf("Corrupt bitstream section! Exiting.\n");
				fclose(fp);
				delete pRetval;
				return nullptr;
			}

			/* some other error in the stream.  Not a problem, just reporting it in
			case we (the app) cares.  In this case, we don't. */
		}
		else
			iter += ret;
		//{
		//	/* we don't bother dealing with sample rate changes, etc, but
		//	you'll have to*/
		//	//      fwrite(pcmout,1,ret,stdout);
		//}
	}

	/* cleanup */
	ov_clear(&vf);

	printf( "Done.\n");

	if (fp)  fclose(fp);

 	pRetval->m_Buffer.AudioBytes = static_cast<UINT32>(nTotalSize);  //buffer containing audio data
	
	return pRetval ;
}

bool CGlobalVoiceDataBuilder::buildVoice_PostProcessing(CVoiceData * pVoice)
{
	if (!pVoice)	return true;

	HRESULT hr = 0;

	pVoice->m_Buffer.pAudioData = pVoice->m_Data.data();  //size of the audio buffer in bytes
	pVoice->m_Buffer.Flags = XAUDIO2_END_OF_STREAM; // tell the source voice not to expect any data after this buffer

//	if(bLoop) pVoice->m_Buffer.LoopCount = XAUDIO2_LOOP_INFINITE;  

	//if (FAILED(hr = s_pAudioManager->getXAudio2()
	//	->CreateSourceVoice(&pVoice->m_pSourceVoice, 
	//						&pVoice->m_WaveFormatEx, 
	//						0, 2, 
	//						CVoiceCallback::getInstance())))
	//	return false;
	//
	////Submit an XAUDIO2_BUFFER to the source voice using the function SubmitSourceBuffer.
	//if (FAILED(pVoice->m_pSourceVoice->SubmitSourceBuffer(&pVoice->m_Buffer)))	
	//	return false;
	//
	//pVoice->m_3DEmitter.OrientFront = X3DAUDIO_VECTOR(0, 0, -1);
	//pVoice->m_3DEmitter.OrientTop = X3DAUDIO_VECTOR(0, 1, 0);
	//pVoice->m_3DEmitter.Position = X3DAUDIO_VECTOR(0, 0, 0);
	//pVoice->m_3DEmitter.Velocity = X3DAUDIO_VECTOR(0, 0, 0);
	//
	//pVoice->m_3DEmitter.ChannelCount = 1;
	//pVoice->m_3DEmitter.CurveDistanceScaler = 100;
	//
	//pVoice->m_3DDSPSettings.pDelayTimes = nullptr;
	//pVoice->m_3DDSPSettings.SrcChannelCount = pVoice->m_WaveFormatEx.nChannels;
	//pVoice->m_3DDSPSettings.DstChannelCount = s_pAudioManager->getNumberOfMasterChannel();
	//pVoice->m_MatrixCoefficients.resize( pVoice->m_3DDSPSettings.SrcChannelCount * pVoice->m_3DDSPSettings.DstChannelCount);
	//pVoice->m_3DDSPSettings.pMatrixCoefficients = pVoice->m_MatrixCoefficients.data();
	//
	//if (FAILED(hr = s_pAudioManager->getXAudio2()
	//	->CreateSourceVoice(&pVoice->m_pSourceVoice2,
	//		&pVoice->m_WaveFormatEx,
	//		0, 2,
	//		CVoiceCallback::getInstance())))
	//	return false;
	//
	//if (FAILED(pVoice->m_pSourceVoice2->SubmitSourceBuffer(&pVoice->m_Buffer)))
	//	return false;

	return true;
}

CVoiceData * CGlobalVoiceDataBuilder::buildVoice(const std::wstring & rsFileName)
{
	CVoiceData * pRetval = nullptr;
	std::wstring sExtension = std::wstring(rsFileName.end() - 3, rsFileName.end()); 

	if (sExtension == L"wav") pRetval  = buildVoice_RealProcessing_wav(rsFileName);
	else if (sExtension == L"ogg")pRetval  = buildVoice_RealProcessing_ogg(rsFileName);

	if (!buildVoice_PostProcessing(pRetval))
	{
		delete pRetval;
		pRetval = nullptr;
	}

	return pRetval;
}

EVoiceDataType CGlobalVoiceDataTypeConverter::getType(const std::wstring & rsName)
{
	return getType(CUtility_String::convertToString(rsName));
}

EVoiceDataType CGlobalVoiceDataTypeConverter::getType(const std::string & rsName)
{
	const std::string sTmp = CUtility_String::convertToLower(rsName);
	if (sTmp == "bgm")	return EVoiceDataType::eBGM;
	else if (sTmp == "se")	return EVoiceDataType::eSE;
	else return EVoiceDataType::eUnknown;
}
