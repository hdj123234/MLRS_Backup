#pragma once
#ifndef HEADER_GLOBALXAUDIO2ENGINE
#define HEADER_GLOBALXAUDIO2ENGINE
 
#ifndef HEADER_XAUDIO2 
#include<X3daudio.h>
#endif
 
//���漱��
struct IXAudio2;
struct IXAudio2MasteringVoice;

class CGlobalXAudio2Engine {
private:
	static IXAudio2					* s_pXAudio;
	static IXAudio2MasteringVoice	* s_pMasterVoice;
	static X3DAUDIO_HANDLE			  s_hX3DAudio;
	static unsigned int				  s_nChannels;

public:
	static void setInstance(IXAudio2* pXAudio, IXAudio2MasteringVoice* pMasterVoice, const X3DAUDIO_HANDLE &rhX3DAudio, unsigned int nChannels)
	{
		s_pXAudio		= pXAudio;
		s_pMasterVoice	= pMasterVoice;
		memcpy_s(s_hX3DAudio, sizeof(X3DAUDIO_HANDLE),rhX3DAudio, sizeof(X3DAUDIO_HANDLE));
		s_nChannels		= nChannels;
	}


	static IXAudio2					* getXAudio2Engine(){return s_pXAudio;}
	static IXAudio2MasteringVoice	* getMasterVoice(){return s_pMasterVoice;}
	static const X3DAUDIO_HANDLE	& getX3DAudioHandle(){return s_hX3DAudio;}
	static unsigned int				  getChannels(){return s_nChannels;}
	static void setMasterVolume(const float fVolume);
};

#endif // !HEADER_XAUDIO2
