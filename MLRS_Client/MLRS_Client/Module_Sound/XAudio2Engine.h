#pragma once
#ifndef HEADER_XAUDIO2ENGINE
#define HEADER_XAUDIO2ENGINE

#ifndef HEADER_XAUDIO2
#include<xaudio2.h>
#include<X3daudio.h>
#endif 

//���漱�� 

class CXAudio2Engine {
private: 
	IXAudio2* m_pXAudio;
	IXAudio2MasteringVoice* m_pMasterVoice;
	X3DAUDIO_HANDLE m_hX3DAudio;
	unsigned int m_nChannels;
	 
public:
	CXAudio2Engine() {}
	~CXAudio2Engine() { releaseObjects(); }

private:
	void releaseObjects()
	{ 
		if (m_pMasterVoice) m_pMasterVoice->DestroyVoice();  
		if (m_pXAudio) m_pXAudio->Release();  
	}

public:
	IXAudio2				*getXAudio2(){return m_pXAudio;}
	IXAudio2MasteringVoice	*getMasterVoice(){return m_pMasterVoice;}
	const X3DAUDIO_HANDLE	&getX3DAudioHandle()const{return m_hX3DAudio;}
	const unsigned int getChannel() { return m_nChannels; }

	bool createObjects()
	{
		HRESULT hr = 0;
		DWORD dwChannelMask;
		if (FAILED(hr = XAudio2Create(&m_pXAudio, 0, XAUDIO2_DEFAULT_PROCESSOR)))	return false;
		if (FAILED(hr = m_pXAudio->CreateMasteringVoice(&m_pMasterVoice)))	return false;
		 
		if (FAILED(hr = m_pMasterVoice->GetChannelMask(&dwChannelMask)))return false; 
		if (FAILED(hr = X3DAudioInitialize(dwChannelMask, X3DAUDIO_SPEED_OF_SOUND, m_hX3DAudio)))	return false;

		XAUDIO2_VOICE_DETAILS deviceDetails;
		m_pMasterVoice->GetVoiceDetails(&deviceDetails);
		m_nChannels = deviceDetails.InputChannels;
		return true;
	}
};

#endif // !HEADER_XAUDIO2
