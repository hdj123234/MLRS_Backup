#pragma once

#ifndef HEADER_INTERFACE_SOUND
#define HEADER_INTERFACE_SOUND

#include"..\Module_Object\Interface_Object.h"

//���漱��
struct X3DAUDIO_LISTENER;

class RIGameObject_SoundListener {
public:
	virtual ~RIGameObject_SoundListener() = 0 {}

	virtual X3DAUDIO_LISTENER * getListenerPtr()=0;
};

enum EMsgType_Sound :ENUMTYPE_256;

class IMsg_Sound : virtual public IMsg_GameObject {
public:
	virtual ~IMsg_Sound() = 0 {}

	virtual const EMsgType_Sound getMsgType() const = 0;
};


#endif