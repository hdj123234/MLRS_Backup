#pragma once
#ifndef HEADER_VOICEDATA
#define HEADER_VOICEDATA
 

#ifndef HEADER_XAUDIO2
#include<xaudio2.h> 
#endif

#ifndef HEADER_STL 
#include<vector> 
#endif // !HEADER_STL

struct CVoiceData {
	XAUDIO2_BUFFER m_Buffer;
	WAVEFORMATEX m_WaveFormatEx;
	std::vector<BYTE> m_Data;
	float m_fVolumeRate;
	float m_fCurveRate;

	CVoiceData()
		:m_Buffer({ 0 }),
		m_WaveFormatEx({ 0 }),
		m_fVolumeRate(1.0f)
	{ }
	~CVoiceData() { }
};
 

#endif // !HEADER_XAUDIO2
