#include "stdafx.h"
#include "SoundManager.h"

#include"Voice.h"
#include"VoiceData.h" 
#include"GlobalXAudio2Engine.h"
#include"Msg_Sound.h"


//---------------------------------------- CSoundManager ------------------------------------------

 

CSoundManager::CSoundManager()
	:m_pListener(nullptr),
	m_3DDSPSettings({0}),
	m_MatrixCoefficients(CGlobalXAudio2Engine::getChannels()*5){
	m_3DDSPSettings.pMatrixCoefficients = m_MatrixCoefficients.data();
}

void CSoundManager::calc3DVoice(CVoice_3D * pVoice)
{
	HRESULT hr = 0;  
	m_3DDSPSettings.SrcChannelCount = pVoice->getVoiceData()->m_WaveFormatEx.nChannels;

	X3DAudioCalculate(CGlobalXAudio2Engine::getX3DAudioHandle(), m_pListener->getListenerPtr(), pVoice->getEmitterPtr(),
		X3DAUDIO_CALCULATE_MATRIX | X3DAUDIO_CALCULATE_DOPPLER | X3DAUDIO_CALCULATE_LPF_DIRECT | X3DAUDIO_CALCULATE_REVERB,
		&m_3DDSPSettings);
	if (FAILED(hr = pVoice->getSourceVoice()->SetOutputMatrix(NULL, m_3DDSPSettings.SrcChannelCount, m_3DDSPSettings.DstChannelCount, m_3DDSPSettings.pMatrixCoefficients)))
		return;
	if (FAILED(hr = pVoice->getSourceVoice()->SetFrequencyRatio(m_3DDSPSettings.DopplerFactor)))
		return;
	
	//	pSourceVoice->SetOutputMatrix(pSubmixVoice, 1, 1, m_3DDSPSettings.ReverbLevel);
	//Note  The audio sample data to which buffer points is still 'owned' by the app and must remain allocated and accessible until the sound stops playing.
	
	XAUDIO2_FILTER_PARAMETERS FilterParameters = { LowPassFilter, 2.0f * sinf(X3DAUDIO_PI / 6.0f * m_3DDSPSettings.LPFDirectCoefficient), 1.0f };
	if (FAILED(hr = pVoice->getSourceVoice()->SetFilterParameters(&FilterParameters)))
		return;

//	pVoice->m_pSourceVoice->Start();
}

std::vector<IMsg*> CSoundManager::animateObject(const float fElapsedTime)
{ 
	m_3DDSPSettings.DstChannelCount = CGlobalXAudio2Engine::getChannels();
	
	std::set<AVoice *> pRemoveList;

	for (auto pData : m_pVoices)
	{
		if(pData->isEnd())	
			pRemoveList.insert(pData);
		else if(pData->getVoiceType()==EVoiceType::eVoiceType_3D)
			calc3DVoice(static_cast<CVoice_3D *>(pData)); 
	}
	for (auto pData : pRemoveList)		m_pVoices.erase(pData);
	return std::vector<IMsg*>();
}

void CSoundManager::handleMsg(IMsg_Sound * pMsg)
{
	switch (pMsg->getMsgType())
	{
	case EMsgType_Sound::eSound_RunBGM:
	{
		CVoice_Normal * pVoice = CVoice_Normal::createVoice(m_pVoiceDatas[static_cast<CMsg_Sound_RunBGM*>(pMsg)->getID()]);
		pVoice->getSourceVoice()->Start();
		m_pVoices.insert(pVoice);
		break;
	}
	case EMsgType_Sound::eSound_RunSE:
	{
		CVoice_Normal * pVoice = CVoice_Normal::createVoice(m_pVoiceDatas[static_cast<CMsg_Sound_RunSE*>(pMsg)->getID()]);
		pVoice->getSourceVoice()->Start();
		m_pVoices.insert(pVoice);
		break;
	}
	case EMsgType_Sound::eSound_Run3DSE:
	{
		CVoice_3D * pVoice = CVoice_3D::createVoice(m_pVoiceDatas[static_cast<CMsg_Sound_Run3DSE*>(pMsg)->getID()],
													static_cast<CMsg_Sound_Run3DSE*>(pMsg)->getPos());
		pVoice->getSourceVoice()->Start();
		m_pVoices.insert(pVoice);
		break;
	}
	}
}

void CSoundManager::clearAllVoice()
{
	for (auto pData : m_pVoices)
		delete pData;
	m_pVoices.clear();
}

bool CSoundManager::createObjects(IFactory_SoundManager * pFactory)
{
	if (!pFactory)	return false;
	m_pVoiceDatas = std::move(pFactory->createVoiceDatas());
	return true;
}

void CSoundManager::releaseObjects()
{  
	for (auto pData : m_pVoices)
	{
		pData->getSourceVoice()->Stop();
		delete pData;
	}
	m_pVoices.clear();
	for (auto pData : m_pVoiceDatas)
		delete pData;
	m_pVoiceDatas.clear();
}
 