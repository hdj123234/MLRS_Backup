#pragma once
#ifndef HEADER_VOICE
#define HEADER_VOICE
 

#ifndef HEADER_XAUDIO2
#include<xaudio2.h>
#include<X3daudio.h>
#endif 

//���漱��
struct CVoiceData;

enum EVoiceType {
	eVoiceType_BGM,
	eVoiceType_3D
};

class AVoice : public IXAudio2VoiceCallback{
private:
	CVoiceData * m_pVoiceData;
	IXAudio2SourceVoice * m_pSourceVoice;
	EVoiceType m_eVoiceType;
	bool m_bEnd;

protected:
	AVoice(CVoiceData * pVoiceData,const EVoiceType eVoiceType);

public:  
	virtual ~AVoice(){ if (m_pSourceVoice)m_pSourceVoice->DestroyVoice();}
	
	IXAudio2SourceVoice * getSourceVoice() { return m_pSourceVoice; }
	const CVoiceData * getVoiceData()const { return m_pVoiceData; }
	const bool isEnd() const{ return m_bEnd; }
	const EVoiceType getVoiceType()const { return m_eVoiceType; }

	//override IXAudio2VoiceCallback
	//Called when the voice has just finished playing a contiguous audio stream.
	void OnStreamEnd() { } 
	//Unused methods are stubs
	void OnVoiceProcessingPassEnd() { }
	void OnVoiceProcessingPassStart(UINT32 SamplesRequired) {    }
	void OnBufferEnd(void * pBufferContext) { m_bEnd = true; }
	void OnBufferStart(void * pBufferContext) { }
	void OnLoopEnd(void * pBufferContext) {    }
	void OnVoiceError(void * pBufferContext, HRESULT Error) { }
	 
};


class CVoice_Normal : public AVoice {  
private:
	CVoice_Normal(CVoiceData * pVoiceData);

public:
	virtual ~CVoice_Normal() {  } 

	static CVoice_Normal * createVoice(CVoiceData * pVoiceData) { return new CVoice_Normal(pVoiceData); }
};


class CVoice_3D : public AVoice {
private: 
	X3DAUDIO_EMITTER m_3DEmitter; 

private:
	CVoice_3D(CVoiceData * pVoiceData, const DirectX::XMFLOAT3 &rvPos);

public:
	virtual ~CVoice_3D() {  }
	 
	X3DAUDIO_EMITTER * getEmitterPtr() { return &m_3DEmitter; } 

	static CVoice_3D * createVoice(CVoiceData * pVoiceData, const DirectX::XMFLOAT3 &rvPos) { return new CVoice_3D(pVoiceData, rvPos); }
};


#endif // !HEADER_XAUDIO2
