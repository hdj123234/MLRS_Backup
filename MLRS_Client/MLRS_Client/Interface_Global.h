#pragma once

#ifndef HEADER_INTERFACE_GLOBAL

#ifndef HEADER_STL
#include<string>
#endif // !HEADER_STL

typedef unsigned char FLAG_8;
typedef unsigned char ENUMTYPE_256;
typedef unsigned char ENUMTYPE_FLAG_8;
typedef unsigned char SLOT;

static const FLAG_8 FLAG_8_ALL = FLAG_8(0xff);

class IMsg {
public:
	virtual ~IMsg() = 0 {}
};

template<class TYPE>
class IBuilder {
public:
	virtual ~IBuilder() = 0 {}

	virtual bool build(TYPE *) = 0;
	virtual TYPE * build() = 0;
};

template<class TYPE>
class IMultiBuilder {
public:
	virtual ~IMultiBuilder() = 0 {}

	virtual TYPE * build() = 0;
};

template<class TYPE>
class IPrototype {
public:
	virtual ~IPrototype() = 0 {}

	virtual TYPE * createClone() = 0;
};

template<class TARGET,class ADAPTEE>
class IAdapter {
public:
	virtual ~IAdapter() = 0 {}

	virtual TARGET * adapt(ADAPTEE *) = 0;	//메시지 변환이 성공하면 이전 메시지 삭제
};


static std::string g_sDataFolder = std::string("Data/");
static std::string g_sShaderFolder = std::string("Shader/");



#endif // !HEADER_INTERFACE_GLOBAL

