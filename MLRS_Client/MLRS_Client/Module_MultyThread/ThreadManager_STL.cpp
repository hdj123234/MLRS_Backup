#include"stdafx.h"
#include "ThreadManager_STL.h"

#ifndef HEADER_STL
#include<random>
#endif // !HEADER_STL

CThreadManager_AsyncOperation_Single_STL::CThreadManager_AsyncOperation_Single_STL()
	:m_bRun(false),
	m_pFuture(nullptr)
{
}
 
void CThreadManager_AsyncOperation_Single_STL::run(IThreadUnit * p)
{
	std::lock_guard<std::mutex> lock(m_Mutex);
	
	if (!m_bRun)
	{
		m_pFuture = new std::future<int>(std::async(std::launch::async, IThreadUnit::launcher, p));
		m_bRun = true;
	}
}

void CThreadManager_AsyncOperation_Single_STL::wait()
{
	std::lock_guard<std::mutex> lock(m_Mutex);
	if (m_bRun)
	{
		m_pFuture->wait();
		delete m_pFuture;
		m_pFuture = nullptr;
		m_bRun = false;
	}
}

void CThreadManager_AsyncOperation_STL::addAndRun(IThreadUnit * p)
{
	std::lock_guard<std::mutex> lock(m_Mutex);
	m_pFutures.push_back(  new std::future<int>(std::async(std::launch::async, IThreadUnit::launcher, p)));
}

void CThreadManager_AsyncOperation_STL::waitAll()
{
	std::lock_guard<std::mutex> lock(m_Mutex);
	for (auto data : m_pFutures)
	{
		data->wait();
		delete data;
	}
	m_pFutures.clear();
}

CThreadManager_Single_STL::CThreadManager_Single_STL()
	:m_pThread(nullptr),
	m_pThreadUnit(nullptr),
	m_bRun(false)
{
}

CThreadManager_Single_STL::~CThreadManager_Single_STL()
{
	shutdown();
	m_bRun = false;
}


void CThreadManager_Single_STL::run()
{
	if (!m_pThread&&m_pThreadUnit && !m_bRun)
	{
		m_pThread = new std::thread(IThreadUnit::launcher, m_pThreadUnit);
		m_bRun = true;

	}
}

void CThreadManager_Single_STL::join()
{
	if(m_pThread)
		m_pThread->join();
}

void CThreadManager_Single_STL::releaseThread()
{
	if (m_pThread&&m_pThreadUnit)
	{
		m_pThreadUnit->releaseThread();
		m_pThread->join();
	}
}

void CThreadManager_Single_STL::shutdown()
{
	if (m_pThread)
		delete m_pThread;
	m_pThread = nullptr;
}
