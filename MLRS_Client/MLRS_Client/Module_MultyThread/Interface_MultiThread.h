#pragma once
#ifndef HEADER_INTERFACE_MULTITHREAD
#define HEADER_INTERFACE_MULTITHREAD

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL


class IThreadUnit{
public:
	virtual ~IThreadUnit() = 0 {}
	virtual int run() = 0;
	virtual void releaseThread() = 0;

	static int launcher(IThreadUnit *p) { return p->run(); };
};

//	새로운 스레드를 하나 생성하여 반복동작하는 스레드관리자
class IThreadManager_Single {
public:
	virtual ~IThreadManager_Single() = 0 {}

	virtual void setTask(IThreadUnit *p)=0 ;
	virtual void run() = 0;
	virtual void join() = 0;
	virtual void releaseThread() = 0;
	virtual void shutdown() = 0;
};

//	새로운 스레드를 하나 생성하여 작업을 진행하는 스레드 관리자
class IThreadManager_AsyncOperation_Single {
public:
	virtual ~IThreadManager_AsyncOperation_Single() = 0 {}

	virtual void run(IThreadUnit *p) = 0;
	virtual void wait() = 0;
};

//	새로운 스레드를 하나 생성하여 작업을 진행하는 스레드 관리자
class IThreadManager_AsyncOperation {
public:
	virtual ~IThreadManager_AsyncOperation() = 0 {}

	virtual void addAndRun(IThreadUnit *p) = 0;
	virtual void waitAll() = 0;
};

#endif