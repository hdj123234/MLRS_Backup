#pragma once
#ifndef HEADER_THREADMANAGER_STL
#define HEADER_THREADMANAGER_STL

#include"Interface_MultiThread.h"

#ifndef HEADER_STL
#include<future>
#include<mutex>
#endif // !HEADER_STL

class CThreadManager_Single_STL : virtual public IThreadManager_Single{
private:
	std::thread *m_pThread;
	IThreadUnit *m_pThreadUnit;
	std::mutex m_Mutex;
	bool m_bRun;

public:
	CThreadManager_Single_STL();
	virtual ~CThreadManager_Single_STL();

	virtual void setTask(IThreadUnit *p) { m_pThreadUnit = p; }
	virtual void run() ;
	virtual void join() ;
	virtual void releaseThread() ;
	virtual void shutdown();

};

class CThreadManager_AsyncOperation_Single_STL :virtual public IThreadManager_AsyncOperation_Single {
private:
	std::mutex m_Mutex;
	bool m_bRun;
	std::future<int> *m_pFuture;

public:
	CThreadManager_AsyncOperation_Single_STL();
	virtual ~CThreadManager_AsyncOperation_Single_STL() { wait(); }

	virtual void run(IThreadUnit *p) ;
	virtual void wait() ;
};


class CThreadManager_AsyncOperation_STL:virtual public IThreadManager_AsyncOperation {
private:
	std::mutex m_Mutex;
	std::vector<std::future<int> *> m_pFutures;
public:
	virtual ~CThreadManager_AsyncOperation_STL() { waitAll(); }

	virtual void addAndRun(IThreadUnit *p) ;
	virtual void waitAll() ;
};

#endif