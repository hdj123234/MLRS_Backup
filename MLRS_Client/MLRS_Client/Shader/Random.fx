/*
의사난수 생성용 쉐이더 코드
선형 합동 생성기(LCG) 알고리즘 사용

최초 1회 seed값을 설정하여 사용할것
*/
#define UINT_MAX uint(0xffffffff)
#define LCG_PARAMETER_a 1664525
#define LCG_PARAMETER_c 1013904223
#define LCG_PARAMETER_m 0x80000000	//2^32
 
static uint s_nRandomSeed;

void setSeed(uint nSeed)
{
	s_nRandomSeed = nSeed;
}

uint getRandomNumber()
{ 
	s_nRandomSeed = (LCG_PARAMETER_a * s_nRandomSeed + LCG_PARAMETER_c) % LCG_PARAMETER_m;
	return s_nRandomSeed;

}