


struct GS_OUTPUT_SKYBOX {
	float4 vPosition : SV_POSITION;
    float3 vTexCoord : TEXCOORD;
	uint nRenderTarget: SV_RenderTargetArrayIndex;
};

TextureCube g_TextureCube : register(t0);
SamplerState g_SamplerState : register(s0);

float4 psSkyBox(GS_OUTPUT_SKYBOX input) : SV_Target
{
	return  g_TextureCube.Sample(g_SamplerState,input.vTexCoord);
}

