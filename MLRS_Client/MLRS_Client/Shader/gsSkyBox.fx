#define SIZE_SKYBOX 10000


struct GS_OUTPUT_SKYBOX {
	float4 m_vPosition : SV_POSITION;
    float3 m_vTexCoord : TEXCOORD;
	uint   m_nRenderTarget: SV_RenderTargetArrayIndex;
};

cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
};

[maxvertexcount(16)]
void gsSkyBox(point float4 input[1] : SV_POSITION,inout TriangleStream<GS_OUTPUT_SKYBOX> triStream)
{
	GS_OUTPUT_SKYBOX output[8];

	[unroll(8)]for (int i = 0; i < 8; ++i)
	{
		output[i] = (GS_OUTPUT_SKYBOX)0;
	}

    output[0].m_vPosition = float4(-SIZE_SKYBOX, +SIZE_SKYBOX, -SIZE_SKYBOX, 1);
    output[1].m_vPosition = float4(-SIZE_SKYBOX, -SIZE_SKYBOX, -SIZE_SKYBOX, 1);
    output[2].m_vPosition = float4(+SIZE_SKYBOX, -SIZE_SKYBOX, -SIZE_SKYBOX, 1);
    output[3].m_vPosition = float4(+SIZE_SKYBOX, +SIZE_SKYBOX, -SIZE_SKYBOX, 1);
    output[4].m_vPosition = float4(-SIZE_SKYBOX, +SIZE_SKYBOX, +SIZE_SKYBOX, 1);
    output[5].m_vPosition = float4(-SIZE_SKYBOX, -SIZE_SKYBOX, +SIZE_SKYBOX, 1);
    output[6].m_vPosition = float4(+SIZE_SKYBOX, -SIZE_SKYBOX, +SIZE_SKYBOX, 1);
    output[7].m_vPosition = float4(+SIZE_SKYBOX, +SIZE_SKYBOX, +SIZE_SKYBOX, 1);

	float4x4 mView_Rotation = g_mView;
	mView_Rotation._41 = 0;
	mView_Rotation._42 = 0;
	mView_Rotation._43 = 0;
//	float4x4 mView_Rotation = float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

	mView_Rotation = mul(mView_Rotation, g_mProjection);
//
	[unroll(8)]for (int i = 0; i < 8; ++i)
	{
        output[i].m_vTexCoord = normalize(output[i].m_vPosition.xyz);
		output[i].m_vPosition = mul(output[i].m_vPosition, mView_Rotation);

		output[i].m_nRenderTarget = 0;
	}


	//A
	triStream.Append(output[1]);
	triStream.Append(output[2]);
	triStream.Append(output[0]);
	triStream.Append(output[3]);
	triStream.Append(output[4]);
	triStream.Append(output[7]);
	triStream.Append(output[5]);
	triStream.Append(output[6]);
	triStream.RestartStrip();
	//B
	triStream.Append(output[7]);
	triStream.Append(output[3]);
	triStream.Append(output[6]);
	triStream.Append(output[2]);
	triStream.Append(output[5]);
	triStream.Append(output[1]);
	triStream.Append(output[4]);
	triStream.Append(output[0]);
	triStream.RestartStrip();

}