
struct Line
{
	float3 m_vPos;
	float3 m_vTarget;
};
struct GS_OUTPUT_LINE {
	float4 m_vPosition : SV_POSITION;
	uint   m_nRenderTarget: SV_RenderTargetArrayIndex;
};

cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b2)
{
	Line g_BoundingBox : packoffset(c0);
};



[maxvertexcount(2)]
void gsLine(point float4 input[1] : SV_POSITION, inout LineStream<GS_OUTPUT_LINE> triStream)
{
	GS_OUTPUT_LINE output[2];

	[unroll(2)]for (int i = 0; i < 2; ++i)
	{
		output[i] = (GS_OUTPUT_LINE) 0;
	}
	output[0].m_vPosition = float4(g_BoundingBox.m_vPos,1);
	output[1].m_vPosition = float4(g_BoundingBox.m_vTarget,1);

	float4x4 mtx = mul(g_mView, g_mProjection);
	[unroll(2)]for (int i = 0; i < 2; ++i)
	{
		output[i].m_vPosition = mul(output[i].m_vPosition, mtx);

		output[i].m_nRenderTarget = 0;
	}

	
	//A
	triStream.Append(output[0]);
	triStream.Append(output[1]);
	triStream.RestartStrip();

}