//
//
#include"Type_FBX.fx"

cbuffer cbWorldMatrix : register(b0)
{
    float4x4 g_mWorld : packoffset(c0);
};

cbuffer cbViewMatrix : register(b1)
{
    float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b2)
{
    float4x4 g_mProjection : packoffset(c0);
}

StructuredBuffer<FBXDummy_Vertex> g_Vertices : register(t0);
Texture1D<float2> g_vUVs : register(t1);
Buffer<uint> g_nMaterialIndices : register(t2);

Texture2D<float4> g_vFrameData : register(t3);
StructuredBuffer<FBXDummy_Bone> g_Bonse : register(t4);
tbuffer cbAnimIndicator : register(t5)
{
    uint g_nFrameIndex[MAX_BONE];
}


float4x4 getMatrixAboutBone(int nBoneIndex)
{
	float4x4 mToLocal = g_Bonse[nBoneIndex].m_mTransform_ToLocal;
	float4x4 mToWorld = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

	uint nIndex = (uint) nBoneIndex;
	uint nFrame = (uint) 0;
	[loop]
	while (1)
	{

		nFrame = g_nFrameIndex[nIndex];

		mToWorld = mul(mToWorld, float4x4(	g_vFrameData[uint2(nFrame,(nIndex * 4)		)],
											g_vFrameData[uint2(nFrame,(nIndex * 4) + 1	)],
											g_vFrameData[uint2(nFrame,(nIndex * 4) + 2	)],
											g_vFrameData[uint2(nFrame,(nIndex * 4) + 3	)]));
		if (nIndex == 0)
			break;
		nIndex = g_Bonse[nIndex].m_nParentIndex;

	}
	return mul(mToLocal, mToWorld);

}

PS_INPUT_FBXDummy_MultiMaterial vsDummy_FBX(VS_INPUT_FBXDummy input,uint nVertexID:SV_VertexID)
{
 
	PS_INPUT_FBXDummy_MultiMaterial output = (PS_INPUT_FBXDummy_MultiMaterial) 0;

	output.m_nMaterialIndex = g_nMaterialIndices[nVertexID/3];
    
	output.m_vTexCoord = g_vUVs[input.m_nUVIndex];
    
	output.m_vPosition_World = g_Vertices[input.m_nVertexIndex].m_vPos;
	output.m_vPosition = float4(output.m_vPosition_World.xyz, 1);
	float4 fWeight = g_Vertices[input.m_nVertexIndex].m_vWeight;
	uint4 nBoneIndex = g_Vertices[input.m_nVertexIndex].m_vBoneIndex;



	float4 vPos_Anim = float4(0, 0, 0, 0);
	float4x4 mAnim = float4x4(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	float4x4 mAnim_v[4];
	float4 vPos_Anim_v[4] = { (float4) 0, (float4) 0, (float4) 0, (float4) 0 };
    [unroll(4)]
	for (int i = 0; i < 4; ++i)
	{
		if (fWeight[i] > 0)
		{
			mAnim += getMatrixAboutBone(nBoneIndex[i]) * fWeight[i];

		}
	}

	float4x4 mAnim_World = mul(mAnim, g_mWorld);
	mAnim_World = g_mWorld;
	output.m_vPosition = mul(output.m_vPosition, (mAnim_World));
    
	output.m_vPosition_World = output.m_vPosition.xyz;
	output.m_vPosition = mul(output.m_vPosition, g_mView);
	output.m_vPosition = mul(output.m_vPosition, g_mProjection);

	float3x3 mWorld_Rotation =
                                float3x3(
                                    mAnim_World._11, mAnim_World._12, mAnim_World._13,
                                    mAnim_World._21, mAnim_World._22, mAnim_World._23,
                                    mAnim_World._31, mAnim_World._32, mAnim_World._33);

	output.m_vNormal_World = mul(input.m_vNormal_World, mWorld_Rotation);
	output.m_vTangent_World = mul(input.m_vTangent_World, mWorld_Rotation);


    return output;
}
