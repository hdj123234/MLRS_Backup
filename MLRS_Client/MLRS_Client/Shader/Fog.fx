

struct FOGSTATE
{
	float4 m_vFogColor;
	float2 m_vFogRange;	// min , max
};

cbuffer cbFogState : register(b0)
{
	FOGSTATE g_FogState;
};

float4 applyFog(float4 vColor_Origin, float fDistance)
{
	float fFogFactor = 0;
	if(fDistance < g_FogState.m_vFogRange.x) fFogFactor = 0;
	else if (fDistance > g_FogState.m_vFogRange.y) fFogFactor = 1;
	else
		fFogFactor = (fDistance - g_FogState.m_vFogRange.x)
						/ (g_FogState.m_vFogRange.y - g_FogState.m_vFogRange.x);

	return vColor_Origin * (1 - fFogFactor) + g_FogState.m_vFogColor * fFogFactor;
}
 