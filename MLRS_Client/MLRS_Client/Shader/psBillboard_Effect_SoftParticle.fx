#include"Type_Billboard_Effect.fx"

Texture2DArray g_TextureArray : register(t0);
Texture2D<float> g_DepthBuffer : register(t1);
SamplerState g_SamplerState : register(s0);
SamplerState g_SamplerState_Point : register(s1);
 
cbuffer cbProjectionMatrix : register(b0)
{
	float4x4 g_mProjectionInverse : packoffset(c0);
};


float4 applySoftParticle(float4 vColor, float fDistance,float fDistance_Buffer)
{
	
	float fDDistance = (fDistance_Buffer - fDistance);
	if (fDistance < 0)
		clip(0);
	float fAlphaFactor = saturate(fDDistance /10);
	return float4(vColor.rgb, vColor.a * fAlphaFactor);
}

float4 psBillboard_Effect_SoftParticle(GS_OUTPUT_BILLBOARD_EFFECT input) : SV_Target
{
    float3 uvw = float3(input.m_vTexCoord, input.m_nTexIndex);
	float2 uv_Depth = input.m_vPos_Proj / input.m_vPosition.w;
	uv_Depth.x = uv_Depth.x + 1.0f;
	uv_Depth.y = 1.0f - uv_Depth.y;
	uv_Depth *= 0.5f;
	float fDepthSample = g_DepthBuffer.Sample(g_SamplerState_Point, uv_Depth);
//								
	float4 vTmp = mul(float4(input.m_vPos_Proj, fDepthSample, 1), g_mProjectionInverse);

	return applySoftParticle(	g_TextureArray.Sample(g_SamplerState, uvw), 
								input.m_vPosition.w,
								vTmp.z / vTmp.w
//								g_DepthBuffer.Load( int3(input.m_vPosition.xy,0))
	);

}

