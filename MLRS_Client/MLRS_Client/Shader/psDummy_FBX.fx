/*
PS_Common

라이트, 텍스처 적용
*/
#include"Lighting.fx"
#include"Type_FBX.fx"


StructuredBuffer<MATERIAL> g_Materials : register(t0);
Texture2DArray<float4> g_DiffuseMaps : register(t1);
Texture2DArray<float4> g_NormalMaps : register(t2);
SamplerState g_SamplerState : register(s0);

float4 psDummy_FBX(PS_INPUT_FBXDummy_MultiMaterial input) : SV_Target0
{
    float3 vTBNTangent = normalize(input.m_vTangent_World);
	float3 vTBNNoraml = normalize(input.m_vNormal_World);
	float3 vTBNBiTangent = cross(vTBNTangent, vTBNNoraml);
    float3x3 mTBN = float3x3(vTBNTangent.x, vTBNBiTangent.x, vTBNNoraml.x,
                                 vTBNTangent.y, vTBNBiTangent.y, vTBNNoraml.y,
                                 vTBNTangent.z, vTBNBiTangent.z, vTBNNoraml.z);

    float3 uvw = float3(input.m_vTexCoord, input.m_nMaterialIndex);

    float3 vNormal = mul(g_NormalMaps.Sample(g_SamplerState, uvw).xyz, mTBN);
    float4 vLightColor = Lighting(g_Materials[input.m_nMaterialIndex], input.m_vPosition_World, vNormal);
    float4 vBaseTexColor = g_DiffuseMaps.Sample(g_SamplerState, uvw);
//    vBaseTexColor.w = 1;
//    return vBaseTexColor;
//    return float4(vNormal,1);
//    return float4(1, 0, 0, 1);
    return saturate(vLightColor * vBaseTexColor);
}