#include"Type_Billboard_Effect.fx"

Texture2DArray g_TextureArray : register(t0);
SamplerState g_SamplerState : register(s0);
 

float4 psBillboard_Effect(GS_OUTPUT_BILLBOARD_EFFECT input) : SV_Target
{
    float3 uvw = float3(input.m_vTexCoord, input.m_nTexIndex);
	return g_TextureArray.Sample(g_SamplerState, uvw);

}

