
struct VS_HIGHTMAP_TESS_IN
{
	float2 m_vPositionXZ : POSITION2D;
	float2 m_vTexCoord : TEXCOORD;
};

struct VS_HIGHTMAP_TESS_OUT
{
	float3 m_vPosition : POSITION;
	float2 m_vTexCoord : TEXCOORD;
};

struct HS_HIGHTMAP_CONSTANT_OUT
{
	float m_fTessEdges[4] : SV_TessFactor;
	float m_fTessInside[2] : SV_InsideTessFactor;
};

struct HS_HIGHTMAP_OUT
{
	float3 m_vPosition : POSITION;
	float2 m_vTexCoord : TEXCOORD;
};

struct DS_HIGHTMAP_OUT
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
	float3 m_vTangent_World : TANGENT;
	float2 m_vTexCoord : TEXCOORD;
};
