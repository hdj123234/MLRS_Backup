#include"Type_Common.fx"

struct GS_OUTPUT_UI_SCREENBASE
{
    float4 m_vPosition : SV_Position;
	float2 m_vTexCoord : TEXCOORD;
    uint m_nRenderTarget : SV_RenderTargetArrayIndex;
};
