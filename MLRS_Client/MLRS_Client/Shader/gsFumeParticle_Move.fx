#include "Type_Fume.fx"
 

[maxvertexcount(1)]
void gsFumeParticle_Move(point Fume_Particle input[1], inout PointStream<Fume_Particle> outStream)
{
	if (input[0].m_fLifeTime < 2)
	{
		Fume_Particle output = input[0];

		float fDTime = 0.016;
		output.m_vPosition += output.m_vVelocity * fDTime;
		output.m_vVelocity -= output.m_vVelocity*( output.m_fSpeedAttenuation * fDTime);
		output.m_fLifeTime += fDTime;
	 
		outStream.Append(output); 
	} 
}