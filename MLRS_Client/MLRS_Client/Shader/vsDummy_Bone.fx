



struct GS_INPUT_BoneDummy
{
    uint m_nVertexID : VertexID;
};

GS_INPUT_BoneDummy vsDummy_Bone(uint nVertexID : SV_VertexID)
{
    GS_INPUT_BoneDummy output = (GS_INPUT_BoneDummy) 0;
    output.m_nVertexID = nVertexID;

    return output;
}