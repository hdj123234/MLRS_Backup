/*
PS_Common

라이트, 텍스처 적용
*/
#include"Lighting.fx"
#include"Type_DeferredLighting.fx" 
#include"Type_Terrain.fx" 

cbuffer cbMaterial : register(b0)
{
	MATERIAL g_Material;
};

cbuffer cbNumberOfLayer : register(b3)
{
	uint g_nNumberOfLayer;
};
cbuffer cbBlockSizeOfDetail : register(b4)
{
	float2 g_vBlockSizeOfDetail;
};

Texture2D g_DiffuseMap : register(t0);
Texture2D g_NormalMap : register(t1);

Texture2DArray g_DiffuseMaps_Splatting : register(t2);
Texture2DArray<float> g_AlphaMaps_Splatting : register(t3);

SamplerState g_SamplerState : register(s0);
//Texture2D gtxtDetailTexture : register(t1);

PS_OUTPUT_DEFERREDLIGHTING psTerrain_DeferredLighting(DS_HIGHTMAP_OUT input) 
{
	PS_OUTPUT_DEFERREDLIGHTING output;
    float3 vTBNTangent = normalize(input.m_vTangent_World);
    float3 vTBNNoraml = normalize(input.m_vNormal_World);
    float3 vTBNBiTangent = cross(vTBNNoraml,vTBNTangent);
	float3x3 mTBN = float3x3(    vTBNTangent.x, vTBNBiTangent.x,vTBNNoraml.x,
                                vTBNTangent.y, vTBNBiTangent.y,vTBNNoraml.y,
                                vTBNTangent.z, vTBNBiTangent.z,vTBNNoraml.z );
    float3 vNormal = mul(g_NormalMap.Sample(g_SamplerState, input.m_vTexCoord).xyz, mTBN);
	float3 vDiffuseColor = saturate(g_DiffuseMap.Sample(g_SamplerState, input.m_vTexCoord).rgb * g_Material.m_vDiffuse.xyz);

	float2 uv_Detail = input.m_vPosition_World.xz / g_vBlockSizeOfDetail;
	uv_Detail -= int2(uv_Detail);
	if (uv_Detail.x < 0)
		uv_Detail.x = 1 + uv_Detail.x;
	if (uv_Detail.y < 0)
		uv_Detail.y = 1 + uv_Detail.y;

	for (uint i = 0; i < g_nNumberOfLayer;++i)
	{
		float3 uvw_Alpha = float3(input.m_vTexCoord, i);
		float alpha = g_AlphaMaps_Splatting.Sample(g_SamplerState, uvw_Alpha);
		float3 uvw = float3(uv_Detail, i);
		vDiffuseColor = alpha * g_DiffuseMaps_Splatting.Sample(g_SamplerState, uvw).rgb + (1 - alpha) * vDiffuseColor;
	}
	output.m_vPosition_World = float4(input.m_vPosition_World, input.m_vPosition.w);
	output.m_vDiffuseColor = float4(vDiffuseColor,1);
	output.m_vNormal = float4(vNormal,1);
	output.m_vMaterial_Specular = g_Material.m_vSpecular;
	return output;

//    float4 vLightColor = Lighting(g_Material, input.m_vPosition_World, vNormal);
//    float4 vBaseTexColor = g_DiffuseMap.Sample(g_SamplerState, input.m_vTexCoord);
//    return float4(g_NormalMap.Sample(g_SamplerState, input.m_vTexCoord).xyz,1);
//	return saturate(vLightColor* vBaseTexColor);
}