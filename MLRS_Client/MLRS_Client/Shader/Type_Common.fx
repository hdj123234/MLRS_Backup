
struct VS_INOUT_POSANDSIZE_XY
{
	float2 m_vPosition : POSITION;
	float2 m_vSize : SIZE;
};

struct VS_INOUT_POSANDSCALE
{
	float2 m_vPosition : POSITION;
	float m_fScale : SCALE;
};