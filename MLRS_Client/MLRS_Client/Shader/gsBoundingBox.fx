
struct BoundingBox
{
	float3 m_vPos;
	float3 m_vExtent;
	row_major float4x4 m_mRotation;
};
struct GS_OUTPUT_BOUNDINGBOX {
	float4 m_vPosition : SV_POSITION;
	uint   m_nRenderTarget: SV_RenderTargetArrayIndex;
};

cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b2)
{
	BoundingBox g_BoundingBox : packoffset(c0);
};



[maxvertexcount(22)]
void gsBoundingBox(point float4 input[1] : SV_POSITION, inout LineStream<GS_OUTPUT_BOUNDINGBOX> triStream)
{
	GS_OUTPUT_BOUNDINGBOX output[8];

	[unroll(8)]for (int i = 0; i < 8; ++i)
	{
		output[i] = (GS_OUTPUT_BOUNDINGBOX) 0;
	}

	output[0].m_vPosition = float4(-g_BoundingBox.m_vExtent.x, +g_BoundingBox.m_vExtent.y, -g_BoundingBox.m_vExtent.z, 1);
	output[1].m_vPosition = float4(-g_BoundingBox.m_vExtent.x, -g_BoundingBox.m_vExtent.y, -g_BoundingBox.m_vExtent.z, 1);
	output[2].m_vPosition = float4(+g_BoundingBox.m_vExtent.x, -g_BoundingBox.m_vExtent.y, -g_BoundingBox.m_vExtent.z, 1);
	output[3].m_vPosition = float4(+g_BoundingBox.m_vExtent.x, +g_BoundingBox.m_vExtent.y, -g_BoundingBox.m_vExtent.z, 1);
	output[4].m_vPosition = float4(-g_BoundingBox.m_vExtent.x, +g_BoundingBox.m_vExtent.y, +g_BoundingBox.m_vExtent.z, 1);
	output[5].m_vPosition = float4(-g_BoundingBox.m_vExtent.x, -g_BoundingBox.m_vExtent.y, +g_BoundingBox.m_vExtent.z, 1);
	output[6].m_vPosition = float4(+g_BoundingBox.m_vExtent.x, -g_BoundingBox.m_vExtent.y, +g_BoundingBox.m_vExtent.z, 1);
	output[7].m_vPosition = float4(+g_BoundingBox.m_vExtent.x, +g_BoundingBox.m_vExtent.y, +g_BoundingBox.m_vExtent.z, 1);

	float4x4 mtx =	float4x4(g_BoundingBox.m_mRotation._11, g_BoundingBox.m_mRotation._12, g_BoundingBox.m_mRotation._13, 0,
							g_BoundingBox.m_mRotation._21, g_BoundingBox.m_mRotation._22, g_BoundingBox.m_mRotation._23, 0,
							g_BoundingBox.m_mRotation._31, g_BoundingBox.m_mRotation._32, g_BoundingBox.m_mRotation._33, 0,
							g_BoundingBox.m_vPos.x, g_BoundingBox.m_vPos.y, g_BoundingBox.m_vPos.z, 1);
//	float4x4 mtx = float4x4(1,0,0, 0,
//							0,1,0, 0,
//							0,0,1, 0,
//							g_BoundingBox.m_vPos.x, g_BoundingBox.m_vPos.y, g_BoundingBox.m_vPos.z, 1);
	mtx = mul(mtx, g_mView);
	mtx = mul(mtx, g_mProjection);
	[unroll(8)]for (int i = 0; i < 8; ++i)
	{
		output[i].m_vPosition = mul(output[i].m_vPosition, mtx);

		output[i].m_nRenderTarget = 0;
	}

	
	//A
	triStream.Append(output[0]);
	triStream.Append(output[1]);
	triStream.Append(output[2]);
	triStream.Append(output[3]);
	triStream.Append(output[0]);
	triStream.Append(output[4]);
	triStream.Append(output[5]);
	triStream.Append(output[1]);
	triStream.Append(output[2]);
	triStream.Append(output[6]);
	triStream.Append(output[5]);
	triStream.RestartStrip();
	//B
	triStream.Append(output[4]);
	triStream.Append(output[5]);
	triStream.Append(output[6]);
	triStream.Append(output[7]);
	triStream.Append(output[4]);
	triStream.Append(output[0]);
	triStream.Append(output[3]);
	triStream.Append(output[7]);
	triStream.Append(output[6]);
	triStream.Append(output[2]);
	triStream.Append(output[3]);
	triStream.RestartStrip();

}