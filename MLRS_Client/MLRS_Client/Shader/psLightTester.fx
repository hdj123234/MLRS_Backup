/*
PS_Common

라이트, 텍스처 적용
*/
#include"Lighting.fx"

struct PS_INPUT_LIGHTTESTER
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
};


float4 psLightTester(PS_INPUT_LIGHTTESTER input) : SV_Target0
{
    MATERIAL material = (MATERIAL) 0;
    
    material.m_vAmbient = float4(0, 0, 0, 1);
    material.m_vDiffuse = float4(1, 1, 1, 1);
    material.m_vSpecular = float4(0.0, 0.0, 0.0, 1);

    float3 vNormal = normalize(input.m_vNormal_World);

    float4 vLightColor = Lighting(material, input.m_vPosition_World, vNormal);
    float4 vBaseTexColor = (1,1,1,1);
//    return float4(g_NormalMap.Sample(g_SamplerState, input.m_vTexCoord).xyz,1);
	return saturate(vLightColor* vBaseTexColor);
}