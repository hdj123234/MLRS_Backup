
struct VS_INOUT_BILLBOARD_EFFECT
{
    float3 m_vPosition : POSITION;
    uint m_nTexIndex : TEXINDEX;
};


struct GS_OUTPUT_BILLBOARD_EFFECT
{
	float4 m_vPosition : SV_POSITION;
	float2 m_vPos_Proj: PROJ_POS;
	float2 m_vTexCoord : TEXCOORD;
	uint m_nTexIndex : TEXINDEX;

	uint m_nRenderTarget : SV_RenderTargetArrayIndex;
};