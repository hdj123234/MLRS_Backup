#include"TYPE_UI.fx"

Texture2D g_DiffuseMap : register(t0);
cbuffer cbProjectionMatrix : register(b0)
{
	float4x4 g_mProjection : packoffset(c0);
}

[maxvertexcount(4)]
void gsUI_ScreenBase_Scale_LT(point VS_INOUT_POSANDSCALE input[1], inout TriangleStream<GS_OUTPUT_UI_SCREENBASE> triStream)
{
    GS_OUTPUT_UI_SCREENBASE output[4];
	float2 pos = float2(input[0].m_vPosition.x * 2 - 1, 1 - input[0].m_vPosition.y * 2);
	float w = 2;
	uint Image_w, Image_h;
	g_DiffuseMap.GetDimensions(Image_w, Image_h);
	const float fAspectRatio_Screen = g_mProjection._11 / g_mProjection._22; //float(w) / float(h);


	const float fAspectRatio_Image_Inverse = float(Image_h) /float(Image_w) ;

	float h = w * fAspectRatio_Image_Inverse / fAspectRatio_Screen;
	w *= input[0].m_fScale;
	h *= input[0].m_fScale;
	/*	0	1
		2	3	*/
	output[0].m_vPosition = float4(pos.x - 0, pos.y + 0, 0, 1);
	output[1].m_vPosition = float4(pos.x + w, pos.y + 0, 0, 1);
	output[2].m_vPosition = float4(pos.x - 0, pos.y - h, 0, 1);
	output[3].m_vPosition = float4(pos.x + w, pos.y - h, 0, 1);

//	output[0].m_vPosition = float4(-input[0].m_fScale + input[0].m_vPosition.x, +fSizeY * +input[0].m_vPosition.y, 0, 1);
//	output[1].m_vPosition = float4(+input[0].m_fScale + input[0].m_vPosition.x, +fSizeY * +input[0].m_vPosition.y, 0, 1);
//	output[2].m_vPosition = float4(-input[0].m_fScale + input[0].m_vPosition.x, -fSizeY * +input[0].m_vPosition.y, 0, 1);
//	output[3].m_vPosition = float4(+input[0].m_fScale + input[0].m_vPosition.x, -fSizeY * +input[0].m_vPosition.y, 0, 1);

//	output[0].m_vPosition = float4(-1, +1, 0, 1);
//	output[1].m_vPosition = float4(+1, +1, 0, 1);
//	output[2].m_vPosition = float4(-1, -1, 0, 1);
//	output[3].m_vPosition = float4(+1, -1, 0, 1);
        
    output[0].m_vTexCoord = float2(0, 0);
    output[1].m_vTexCoord = float2(1, 0);
    output[2].m_vTexCoord = float2(0, 1);
    output[3].m_vTexCoord = float2(1, 1);

    output[0].m_nRenderTarget 
		= output[1].m_nRenderTarget 
		= output[2].m_nRenderTarget 
		= output[3].m_nRenderTarget = 0;

	triStream.Append(output[0]);
	triStream.Append(output[1]);
	triStream.Append(output[2]);
	triStream.Append(output[3]);
	triStream.RestartStrip();

}