#include "Type_Fume.fx"
#include"Type_Billboard_Effect.fx"

cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
};

cbuffer cbBillboardSize : register(b2)
{
	float2 g_vBillboardSize : packoffset(c0);
};

[maxvertexcount(4)]
void gsFumeParticle_Draw(point Fume_Particle input[1], inout TriangleStream<GS_OUTPUT_BILLBOARD_EFFECT> outStream)
{
	GS_OUTPUT_BILLBOARD_EFFECT output[4];

	output[0].m_vPosition = float4(-g_vBillboardSize.x, g_vBillboardSize.y, 0, 1);
	output[1].m_vPosition = float4(g_vBillboardSize.x, g_vBillboardSize.y, 0, 1);
	output[2].m_vPosition = float4(-g_vBillboardSize.x, -g_vBillboardSize.y, 0, 1);
	output[3].m_vPosition = float4(g_vBillboardSize.x, -g_vBillboardSize.y, 0, 1);
        
	output[0].m_vTexCoord = float2(0, 0);
	output[1].m_vTexCoord = float2(1, 0);
	output[2].m_vTexCoord = float2(0, 1);
	output[3].m_vTexCoord = float2(1, 1);

    //float4x4 mWorld =
    //{
    //    -g_mView._11, -g_mView._21, -g_mView._31, 0,
    //     g_mView._12,  g_mView._22,  g_mView._32, 0,
    //    -g_mView._13, -g_mView._23, -g_mView._33, 0,
    //    input[0].m_vPosition.x, input[0].m_vPosition.y, input[0].m_vPosition.z, 1
    //};
	float4x4 mWorld =
	{
		g_mView._11, g_mView._21, g_mView._31, 0,
         g_mView._12, g_mView._22, g_mView._32, 0,
        g_mView._13, g_mView._23, g_mView._33, 0,
        input[0].m_vPosition.x, input[0].m_vPosition.y, input[0].m_vPosition.z, 1
	};

	[unroll(4)]
	for (int i = 0; i < 4; ++i)
	{
		output[i].m_vPosition = mul(mul(mul(output[i].m_vPosition, mWorld), g_mView), g_mProjection);
		output[i].m_nTexIndex = input[0].m_fLifeTime*60;
		output[i].m_vPos_Proj = output[i].m_vPosition.xy;
		output[i].m_nRenderTarget = 0;
	}

	outStream.Append(output[0]);
	outStream.Append(output[1]);
	outStream.Append(output[2]);
	outStream.Append(output[3]);
	outStream.RestartStrip();

}
 