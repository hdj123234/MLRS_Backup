
cbuffer cbColor : register(b0)
{
	float4 g_vColor : packoffset(c0);
};

float4 psColorOnly(float4 input : SV_POSITION) : SV_Target0
{
	return g_vColor;
}