#include "Type_Fume.fx"
#include"Random.fx"

cbuffer cbFumeState : register(b0)
{
	FumeState g_FumeState : packoffset(c0);
};


float3x3 getRotationMatrix_FromAxis(float3 vAxis, float fAngle)
{
	float c = cos(fAngle);
	float s = sin(fAngle);
	float t = 1 - c;
	return float3x3(t * vAxis.x * vAxis.x + c, t * vAxis.x * vAxis.y - s * vAxis.z, t * vAxis.x * vAxis.z + s * vAxis.y,
					t * vAxis.y * vAxis.x + s * vAxis.z, t * vAxis.y * vAxis.y + c, t * vAxis.y * vAxis.z - s * vAxis.x,
					t * vAxis.x * vAxis.z - vAxis.y * s, t * vAxis.y * vAxis.z + vAxis.x * s, t * vAxis.z * vAxis.z + c);

}

[maxvertexcount(1)]
void gsFumeSpray_Spray(point Fume_SprayPoint input[1], inout PointStream<Fume_Particle> outStream)
{ 
	Fume_Particle output = (Fume_Particle) 0;
	setSeed(asuint(input[0].m_vPosition.x) ^ asuint(input[0].m_vPosition.y) ^ asuint(input[0].m_vPosition.z) ^
			asuint(input[0].m_vDirection.x) ^ asuint(input[0].m_vDirection.y) ^ asuint(input[0].m_vDirection.z));

	float3 vUp = float3(0, 1, 0);
	float3 vRight = cross(vUp, input[0].m_vDirection);
	bool3 bFail = isnan(vRight);
	if (bFail.x || bFail.y || bFail.z || (vRight.x == 0 && vRight.y == 0 && vRight.z == 0))
	{
		vUp = float3(0, 0, 1);
		vRight = cross(vUp, input[0].m_vDirection);
	}
	vUp = cross(input[0].m_vDirection, vRight);
	float2 fAngles = float2(getRandomNumber(), getRandomNumber());
	fAngles /= (UINT_MAX)/2;
	fAngles -= 1;
	fAngles *= g_FumeState.m_fVariationAngle;
	
	float3x3 mRotation = mul(getRotationMatrix_FromAxis(vRight,fAngles.x),getRotationMatrix_FromAxis(vUp,fAngles.y));

	float3 vVelocity;
	output.m_vPosition = input[0].m_vPosition;
	output.m_vVelocity = mul(input[0].m_vDirection, mRotation) * g_FumeState.m_fInitalSpeed;
	
	output.m_fLifeTime = 0;
	output.m_fSpeedAttenuation = g_FumeState.m_fSpeedAttenuation;
	 
	outStream.Append(output); 

} 