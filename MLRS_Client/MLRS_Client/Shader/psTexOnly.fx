
struct PS_INPUT_TEXONLY
{
	float4 m_vPosition : SV_Position;
	float2 m_vTexCoord : TEXCOORD;
};

Texture2D g_DiffuseMap : register(t0);
SamplerState g_SamplerState : register(s0);

float4 psTexOnly(PS_INPUT_TEXONLY input ) : SV_Target0
{
	float4 retval = g_DiffuseMap.Sample(g_SamplerState, input.m_vTexCoord);
	clip(retval.a);
	return retval;
}