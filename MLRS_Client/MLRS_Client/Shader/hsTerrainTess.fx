#include"Type_Terrain.fx"

//제곱 좌표계
#define MINLENGTH 90000.0f
#define MAXLENGTH 16000000.0f
#define MAXRANGE 0.60f




cbuffer cbCameraPosition : register(b0)
{
    float3 g_vCameraPosition : packoffset(c0);
    float3 g_vCameraDirection : packoffset(c1);
};
	//   0   1
	//   2   3

HS_HIGHTMAP_CONSTANT_OUT hsConstant(InputPatch<VS_HIGHTMAP_TESS_OUT, 4> input,uint nPatchID:SV_PrimitiveID)
{
	HS_HIGHTMAP_CONSTANT_OUT output = (HS_HIGHTMAP_CONSTANT_OUT)0;
	float3 vLinePos[4] = { (float3)0,(float3)0,(float3)0,(float3)0 };		//좌상우하 가로세로
    float3 vPointDir[4] = { (float3) 0, (float3) 0, (float3) 0, (float3) 0 }; //좌상우하 가로세로

    float3 vCameraDirection = normalize(g_vCameraDirection);
    float3 fDis = (float3) 0;
    float fLength = (float) 0;

    //가로,세로 길이를 구해 더 긴쪽에 fLineLength를 세팅
    fDis = input[1].m_vPosition - input[3].m_vPosition;
    float fLineLength = dot(fDis, fDis);


	//중점 계산
	vLinePos[0] = ( input[0].m_vPosition + input[2].m_vPosition) /2.0;
	vLinePos[1] = ( input[0].m_vPosition + input[1].m_vPosition) /2.0;
	vLinePos[2] = ( input[1].m_vPosition + input[3].m_vPosition) /2.0;
    vLinePos[3] = ( input[2].m_vPosition + input[3].m_vPosition) / 2.0;

    vPointDir[0] = input[0].m_vPosition - g_vCameraPosition;
    vPointDir[1] = input[1].m_vPosition - g_vCameraPosition;
    vPointDir[2] = input[2].m_vPosition - g_vCameraPosition;
    vPointDir[3] = input[3].m_vPosition - g_vCameraPosition;
    if (dot(vPointDir[0], vPointDir[0]) <= fLineLength ||
        dot(vPointDir[1], vPointDir[1]) <= fLineLength ||
        dot(vPointDir[2], vPointDir[2]) <= fLineLength ||
        dot(vPointDir[3], vPointDir[3]) <= fLineLength)
    {
        output.m_fTessEdges[0] = 64;
        output.m_fTessEdges[1] = 64;
        output.m_fTessEdges[2] = 64;
        output.m_fTessEdges[3] = 64;
    }
    else if (dot(vCameraDirection, normalize(vPointDir[0])) <= MAXRANGE &&
             dot(vCameraDirection, normalize(vPointDir[1])) <= MAXRANGE &&
             dot(vCameraDirection, normalize(vPointDir[2])) <= MAXRANGE &&
             dot(vCameraDirection, normalize(vPointDir[3])) <= MAXRANGE)
    {
        output.m_fTessEdges[0] = 1;
        output.m_fTessEdges[1] = 1;
        output.m_fTessEdges[2] = 1;
        output.m_fTessEdges[3] = 1;
    }
    else
    {
    [unroll(4)]
        for (uint i = 0; i < 4; ++i)
        {
            fDis = g_vCameraPosition - vLinePos[i];
            fLength = dot(fDis, fDis);
            if (fLength < MINLENGTH)
                output.m_fTessEdges[i] = 64;
            else if (fLength > MAXLENGTH)
                output.m_fTessEdges[i] = 5;
            else
                output.m_fTessEdges[i] = 5.0f + 59.0f * (1.0 - abs(fLength - MINLENGTH) / (MAXLENGTH - MINLENGTH));
            
        }
    }
    output.m_fTessInside[0] = (output.m_fTessEdges[0] + output.m_fTessEdges[2])/2;
    output.m_fTessInside[1] = (output.m_fTessEdges[1] + output.m_fTessEdges[3])/2;


	return output;

}

[domain("quad")]
[partitioning("pow2")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("hsConstant")]
[maxtessfactor(64.0f)]
HS_HIGHTMAP_OUT hsTerrainTess(InputPatch<VS_HIGHTMAP_TESS_OUT, 4> input, uint i:SV_OutputControlPointID)
{
	HS_HIGHTMAP_OUT output = (HS_HIGHTMAP_OUT)0;
	output.m_vPosition = input[i].m_vPosition;
	output.m_vTexCoord = input[i].m_vTexCoord;

	return output;

}
