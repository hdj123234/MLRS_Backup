/*

라이트, 텍스처 적용
*/
#include"Type_DeferredLighting.fx"


struct PS_INPUT_TextureCube
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vTexCoord: TEXCOORD;
	float3 m_vNormal : NORMAL;
};

cbuffer cbMaterial : register(b2)
{
	MATERIAL g_Material;
};

TextureCube g_TextureCube : register(t0);
SamplerState g_SamplerState : register(s0);


PS_OUTPUT_DEFERREDLIGHTING psDeferredLighting_TextureCube(PS_INPUT_TextureCube input) 
{
	PS_OUTPUT_DEFERREDLIGHTING result = (PS_OUTPUT_DEFERREDLIGHTING) 0;

    result.m_vDiffuseColor = saturate(g_TextureCube.Sample(g_SamplerState, input.m_vTexCoord) * g_Material.m_vDiffuse);
	result.m_vPosition_World = float4(input.m_vPosition_World,1);
	result.m_vNormal = float4(input.m_vNormal,1);
	result.m_vMaterial_Specular = g_Material.m_vSpecular;
	return result;
}