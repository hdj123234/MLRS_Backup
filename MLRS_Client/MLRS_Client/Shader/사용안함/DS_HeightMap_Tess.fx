//
//

struct HS_HIGHTMAP_CONSTANT_OUT
{
	float m_fTessEdges[4]:SV_TessFactor;
	float m_fTessInside[2]:SV_InsideTessFactor;

	uint m_nBlockID :PRIMITIVEID;
};
struct HS_HIGHTMAP_OUT
{
	float3 m_vPosition : POSITION;
    float2 m_vTexCoord : TEXCOORD;
};

struct DS_HIGHTMAP_OUT
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
	float3 m_vTangent_World: TANGENT;
    float2 m_vTexCoord : TEXCOORD;
};


cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);	
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
}


Texture2D<float> g_HeightMap : register(t0);
SamplerState g_SamplerState : register(s0);

[domain("quad")]
DS_HIGHTMAP_OUT dsHeightMap(HS_HIGHTMAP_CONSTANT_OUT input, float2 uv:SV_DomainLocation, OutputPatch<HS_HIGHTMAP_OUT,4> patch)
{
	DS_HIGHTMAP_OUT output = (DS_HIGHTMAP_OUT)0;
	float tessFactor = input.m_fTessEdges[0];
	//위치(XY)
	float3 v1 = lerp(patch[0].m_vPosition, patch[1].m_vPosition,uv.x);
	float3 v2 = lerp(patch[2].m_vPosition, patch[3].m_vPosition, uv.x);
	float3 position_Tmp = lerp(v1, v2, uv.y);

	//높이
	float2 uv1 = lerp(patch[0].m_vTexCoord, patch[1].m_vTexCoord, uv.x);
	float2 uv2 = lerp(patch[2].m_vTexCoord, patch[3].m_vTexCoord, uv.x);
	float2 uv_MapBase = lerp(uv1, uv2, uv.y);

	float3 position = position_Tmp;
	position.y = g_HeightMap.SampleLevel(g_SamplerState, uv_MapBase, 0);

	//법선
	float3 adjPosition[4] = { (float3)0,(float3)0,(float3)0,(float3)0 };
	float2 adjUV[4] = { (float2)0,(float2)0,(float2)0,(float2)0 };
	float2 uvTmp = (float2)0;
	float slice_Tess[4] = { (float)0,(float)0,(float)0,(float)0 };
	for (uint i = 0; i < 4; ++i)
	{
		slice_Tess[i] = 1.0f / input.m_fTessEdges[0];
		if (i == 0)
		{
			uvTmp.x = uv.x - slice_Tess[0];
			uvTmp.y = uv.y;
		}
		else if (i == 1)
		{
			uvTmp.x = uv.x;
			uvTmp.y = uv.y + slice_Tess[1];
		}
		else if (i == 2)
		{
			uvTmp.x = uv.x + slice_Tess[2];
			uvTmp.y = uv.y ;
		}
		else if (i == 3)
		{
			uvTmp.x = uv.x;
			uvTmp.y = uv.y -slice_Tess[3];
		}
		v1 = lerp(patch[0].m_vPosition, patch[1].m_vPosition, uvTmp.x);
		v2 = lerp(patch[2].m_vPosition, patch[3].m_vPosition, uvTmp.x);
		adjPosition[i] = lerp(v1, v2, uvTmp.y);

		uv1 = lerp(patch[0].m_vTexCoord, patch[1].m_vTexCoord, uvTmp.x);
		uv2 = lerp(patch[2].m_vTexCoord, patch[3].m_vTexCoord, uvTmp.x);
		adjUV[i] = lerp(uv1, uv2, uvTmp.y);
		adjPosition[i].y = g_HeightMap.SampleLevel(g_SamplerState, adjUV[i], 0);
	}


	float3 vNormal= (float3)0;
	vNormal += normalize(cross(adjPosition[0] - position, adjPosition[1] - position));
	vNormal += normalize(cross(adjPosition[1] - position, adjPosition[2] - position));
	vNormal += normalize(cross(adjPosition[2] - position, adjPosition[3] - position));
	vNormal += normalize(cross(adjPosition[3] - position, adjPosition[0] - position));


	//TBN좌표계
	float3 vTangent = (float3)0;

	float2x3 mTan_Bi = (float2x3)0;
	float2x3 mdp = (float2x3)0;
	float2x2 mduv= (float2x2)0;
	float2 duv1	=(float2)0;
	float2 duv2	=(float2)0;
	float3 dp1	=(float3)0;
	float3 dp2	=(float3)0;

	[unroll(4)]for (uint i = 0; i < 4; ++i)
	{
		if (i == 0)
		{
			duv1 = adjUV[0] - uv_MapBase;
			duv2 = adjUV[1] - uv_MapBase;
			dp1 = adjPosition[0] - position;
			dp2 = adjPosition[1] - position;
		}
		else if (i == 1)
		{
			duv1 = adjUV[1] - uv_MapBase;
			duv2 = adjUV[2] - uv_MapBase;
			dp1 = adjPosition[1] - position;
			dp2 = adjPosition[2] - position;
		}
		else if (i == 2)
		{
			duv1 = adjUV[2] - uv_MapBase;
			duv2 = adjUV[3] - uv_MapBase;
			dp1 = adjPosition[2] - position;
			dp2 = adjPosition[3] - position;
		}
		else if (i == 3)
		{
			duv1 = adjUV[3] - uv_MapBase;
			duv2 = adjUV[0] - uv_MapBase;
			dp1 = adjPosition[3] - position;
			dp2 = adjPosition[0] - position;
		}

		mdp = float2x3(dp1.x, dp1.y, dp1.z,
			dp2.x, dp2.y, dp2.z);
		mduv = float2x2(duv2.y, -duv1.y,
			-duv2.x, duv1.x) / (duv1.x*duv2.y - duv1.y*duv2.x);
		mTan_Bi = mul(mduv, mdp);
		vTangent += normalize(float3(mTan_Bi._11, mTan_Bi._12, mTan_Bi._13));
	}

	output.m_vPosition_World = position;
	output.m_vNormal_World = vNormal;
	output.m_vTangent_World = vTangent;
	output.m_vTexCoord = uv_MapBase;
	output.m_vPosition = mul(mul(float4(position.xyz,1), g_mView), g_mProjection);


	return output;

}
