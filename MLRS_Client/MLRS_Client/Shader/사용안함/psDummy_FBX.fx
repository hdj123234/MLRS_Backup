/*
PS_Common

라이트, 텍스처 적용
*/
#include"Lighting.fx"
#include"Type_FBX.fx"



cbuffer cbMaterial : register(b2)
{
    MATERIAL g_Material;
};


Texture2D g_DiffuseMap : register(t0);
Texture2D g_NormalMap : register(t1);
SamplerState g_SamplerState : register(s0);

float4 psDummy_FBX(PS_INPUT_FBXDummy input) : SV_Target0
{
    float3 vTBNTangent = normalize(input.m_vTangent_World);
    float3 vTBNNoraml = normalize(input.m_vNormal_World);
    float3 vTBNBiTangent = normalize(cross(vTBNNoraml, vTBNTangent));
    float3x3 mTBN = float3x3(vTBNTangent.x, vTBNBiTangent.x, vTBNNoraml.x,
                                 vTBNTangent.y, vTBNBiTangent.y, vTBNNoraml.y,
                                 vTBNTangent.z, vTBNBiTangent.z, vTBNNoraml.z);
    float3 vNormal = mul(g_NormalMap.Sample(g_SamplerState, input.m_vTexCoord).xyz, mTBN);

    float4 vLightColor = Lighting(g_Material, input.m_vPosition_World, vNormal);
    float4 vBaseTexColor = g_DiffuseMap.Sample(g_SamplerState, input.m_vTexCoord);
//    return float4(vNormal,1);
    return saturate(vLightColor * vBaseTexColor);
//    return float4(1, 0, 0, 1);
}