
struct PS_INPUT_TEXTURECUBE
{
	float4 m_vPosition : SV_POSITION;
	uint m_nRenderTarget: SV_RenderTargetArrayIndex;
};


[maxvertexcount(4)]
void gsScreen(point float4 input[1] : SV_POSITION, inout TriangleStream<PS_INPUT_TEXTURECUBE> triStream)
{
	PS_INPUT_TEXTURECUBE output[4] ;

	for (int i = 0; i < 4; ++i)
	{
		output[i] = (PS_INPUT_TEXTURECUBE)0;
	}
	output[0].m_vPosition = float4(-1, 1, 0, 1);
	output[0].m_nRenderTarget = 0;
	output[1].m_vPosition = float4(1, 1, 0, 1);
	output[1].m_nRenderTarget = 0;
	output[2].m_vPosition = float4(-1, -1, 0, 1);
	output[2].m_nRenderTarget = 0;
	output[3].m_vPosition = float4(1, -1, 0, 1);
	output[3].m_nRenderTarget = 0;

	//�ո�
	triStream.Append(output[0]);
	triStream.Append(output[1]);
	triStream.Append(output[2]);
	triStream.Append(output[3]);
}