
struct GS_PARTICLE_OUT
{
    float4 m_vPosition : SV_Position;
    float4 m_vColor : COLOR;
};

float4 psParticle(GS_PARTICLE_OUT input) : SV_Target0
{
    return input.m_vColor;
}