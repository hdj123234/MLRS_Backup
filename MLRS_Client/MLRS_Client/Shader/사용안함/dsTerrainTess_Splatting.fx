#include"Type_Terrain_Splatting.fx"
//
//
 


cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);	
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
}


Texture2D<float> g_HeightMap : register(t0);
SamplerState g_SamplerState : register(s0);

[domain("quad")]
DS_HIGHTMAP_SPLATTING_OUT dsTerrainTess_Splatting(HS_HIGHTMAP_CONSTANT_OUT input, float2 uv : SV_DomainLocation, OutputPatch<HS_HIGHTMAP_SPLATTING_OUT, 4> patch)
{ 
	DS_HIGHTMAP_SPLATTING_OUT output = (DS_HIGHTMAP_SPLATTING_OUT) 0;
	float tessFactor = input.m_fTessEdges[0];
	//위치(XZ)
	float3 vPos1 = lerp(patch[0].m_vPosition, patch[1].m_vPosition,uv.x);
	float3 vPos2 = lerp(patch[2].m_vPosition, patch[3].m_vPosition, uv.x);
	float3 position = lerp(vPos1, vPos2, uv.y);
	
	//위치(XZ)
	float2 vDetailUV_1 = lerp(patch[0].m_vTexCoord_Detail, patch[1].m_vTexCoord_Detail, uv.x);
	float2 vDetailUV_2 = lerp(patch[2].m_vTexCoord_Detail, patch[3].m_vTexCoord_Detail, uv.x);
	float2 uv_Detail = lerp(vDetailUV_1, vDetailUV_2, uv.y);

	//높이
	float2 uv1 = lerp(patch[0].m_vTexCoord, patch[1].m_vTexCoord, uv.x);
	float2 uv2 = lerp(patch[2].m_vTexCoord, patch[3].m_vTexCoord, uv.x);
	float2 uv_MapBase = lerp(uv1, uv2, uv.y);

	position.y = g_HeightMap.SampleLevel(g_SamplerState, uv_MapBase, 0);

	output.m_vPosition_World = position;
	output.m_vNormal_World = float3(0, 1, 0);
	output.m_vTangent_World = float3(1, 0, 0);
	output.m_vTexCoord = uv_MapBase;
	output.m_vTexCoord_Detail = uv_Detail;
	output.m_vPosition = mul(mul(float4(position.xyz,1), g_mView), g_mProjection);


	return output;

}
