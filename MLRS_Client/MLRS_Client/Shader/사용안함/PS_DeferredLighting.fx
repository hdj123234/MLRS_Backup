/*
PS_Common

라이트, 텍스처 적용
*/
#include"Type_DeferredLighting.fx"

struct PS_INPUT_COMMON
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
    float2 m_vTexCoord : TEXCOORD;
};

cbuffer cbMaterial : register(b2)
{
	MATERIAL g_Material;
};


Texture2D g_DiffuseMap: register(t0);
SamplerState g_SamplerState : register(s0);
//Texture2D gtxtDetailTexture : register(t1);

PS_OUTPUT_DEFERREDLIGHTING psDeferredLighting(PS_INPUT_COMMON input) 
{
	PS_OUTPUT_DEFERREDLIGHTING result = (PS_OUTPUT_DEFERREDLIGHTING) 0;
    result.m_vDiffuseColor = saturate(g_DiffuseMap.Sample(g_SamplerState, input.m_vTexCoord) * g_Material.m_vDiffuse);
	result.m_vPosition_World = float4(input.m_vPosition_World,1);
	result.m_vNormal = float4(input.m_vNormal_World,1);
	result.m_vMaterial_Specular = g_Material.m_vSpecular;

	return result;
}