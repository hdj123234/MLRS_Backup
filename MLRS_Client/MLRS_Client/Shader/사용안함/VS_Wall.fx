

struct VS_WALL_IN
{
	float2 m_vPositionXY : POSITIONXY;
	float2 m_vTexCoord: TEXTURE;
};

struct VS_WALL_OUT
{
	float3 m_vPosition : POSITION;
	float2 m_vTexCoord: TEXTURE;
};
VS_WALL_OUT vsWall(VS_WALL_IN input)
{
	VS_WALL_OUT output = (VS_WALL_OUT)0;
	output.m_vPosition.x = input.m_vPositionXY.x;
	output.m_vPosition.y = input.m_vPositionXY.y;
	output.m_vPosition.z = 0;


	output.m_vTexCoord = input.m_vTexCoord;

	return output;
}
