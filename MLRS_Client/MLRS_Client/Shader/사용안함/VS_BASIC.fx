

struct VS_INPUT
{
	float4 position : POSITION;
	float4 color : COLOR;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float4 color : COLOR0;
};

cbuffer cbWorldMatrix : register(b0)
{
	matrix gmtxWorld : packoffset(c0);
};
cbuffer cbCameraMatrix : register(b1)
{
	matrix gmtxCamera : packoffset(c0);
};
cbuffer cbProjectionMatrix : register(b2)
{
	matrix gmtxProjection : packoffset(c0);
};

//VS_OUTPUT VS(VS_INPUT input ) 
//{
//	VS_OUTPUT output = (VS_OUTPUT)0;
//	matrix mtx = mul(gmtxCamera, gmtxProjection);
//	mtx = mul(gmtxWorld, mtx);
//	input.position = mul(input.position, mtx);
//	output.position = input.position;
//	output.color = input.color;
//	return output;
//}
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	matrix mtx = mul(gmtxCamera, gmtxProjection);
	mtx = mul(gmtxWorld, mtx);
	input.position = mul(input.position, mtx);
	output.position = input.position;
	output.color = input.color;
	return output;
}
