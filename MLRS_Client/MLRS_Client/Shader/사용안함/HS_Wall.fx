//���� ��ǥ��
#define MINLENGTH 1000
#define MAXLENGTH 5000000



struct VS_HIGHTMAP_TESS_OUT
{
	float3 m_vPosition : POSITION;
	float2 m_vTexture: TEXTURE;
};

struct HS_HIGHTMAP_CONSTANT_OUT
{
	float m_fTessEdges[4]:SV_TessFactor;
	float m_fTessInside[2]:SV_InsideTessFactor;

	uint m_nBlockID :PRIMITIVEID;
};

struct HS_HIGHTMAP_OUT
{
	float3 m_vPosition : POSITION;
	float2 m_vTexture: TEXTURE;
};


cbuffer cbCameraPosition : register(b0)
{
	float3 g_vCameraPosition : packoffset(c0);
};

cbuffer cbWorldMatrix : register(b1)
{
	matrix g_mWorld : packoffset(c0);
};

static const float g_fUnit = (MAXLENGTH - MINLENGTH) / 65;


HS_HIGHTMAP_CONSTANT_OUT hsConstant(InputPatch<VS_HIGHTMAP_TESS_OUT, 4> input,uint nPatchID:SV_PrimitiveID)
{
	HS_HIGHTMAP_CONSTANT_OUT output = (HS_HIGHTMAP_CONSTANT_OUT)0;
	float tessFactor= (float)0;
	float3 vPosition = (float3)0;
	float3 fDis = (float3)0;
	float fLength= (float)0;


	vPosition.x = g_mWorld._14;
	vPosition.y = g_mWorld._24;
	vPosition.z = g_mWorld._34;

	fDis = g_vCameraPosition - vPosition;
	fLength = dot(fDis, fDis);

	if (fLength > MAXLENGTH)
		tessFactor = 1;
	else if (fLength < MINLENGTH)
		tessFactor = 64;
	else
		tessFactor = (63  * saturate(fLength - MINLENGTH) / (MAXLENGTH- MINLENGTH))+1;
	if (tessFactor > 64)
		tessFactor = 64;

	output.m_fTessEdges[0] = tessFactor;
	output.m_fTessEdges[1] = tessFactor;
	output.m_fTessEdges[2] = tessFactor;
	output.m_fTessEdges[3] = tessFactor;
	output.m_fTessInside[0] = tessFactor;
	output.m_fTessInside[1] = tessFactor;

	output.m_nBlockID = nPatchID;
	return output;

}

[domain("quad")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("hsConstant")]
[maxtessfactor(64.0f)]
HS_HIGHTMAP_OUT hsWall(InputPatch<VS_HIGHTMAP_TESS_OUT, 4> input, uint i:SV_OutputControlPointID)
{
	HS_HIGHTMAP_OUT output = (HS_HIGHTMAP_OUT)0;
	output.m_vPosition = input[i].m_vPosition;
	output.m_vTexture = input[i].m_vTexture;

	return output;

}
