//
//

struct VS_HIGHTMAP_TESS_IN
{
	float2 m_vPositionXZ : POSITIONXZ;
    float2 m_vTexture : TEXCOORD;
};

struct VS_HIGHTMAP_TESS_OUT
{
	float3 m_vPosition : POSITION;
    float2 m_vTexture : TEXCOORD;
};

Texture2D<float> g_HeightMap : register(t0);
SamplerState g_SamplerState : register(s0);

VS_HIGHTMAP_TESS_OUT vsHeightMap_Tess(VS_HIGHTMAP_TESS_IN input)
{
	VS_HIGHTMAP_TESS_OUT output = (VS_HIGHTMAP_TESS_OUT)0;
	output.m_vPosition.y = g_HeightMap.SampleLevel(g_SamplerState, input.m_vTexture,0);
	output.m_vPosition.x = input.m_vPositionXZ.x;
	output.m_vPosition.z= input.m_vPositionXZ.y;
	output.m_vTexture = input.m_vTexture;
	return output;

}
