
static const float PI = 3.14159265f;
struct VS_PARTICLE_INOUT
{
    float3 m_vPosition : POSITION;
    float3 m_vVelocity : VELOCITY;
    float m_fAge : AGE;
};

cbuffer cbElapsedTime : register(b0)
{
    float g_fElapsedTime;
};

[maxvertexcount(8)]
void gsParticle_SO(point VS_PARTICLE_INOUT input[1], inout PointStream<VS_PARTICLE_INOUT> outStream)
{
    
    VS_PARTICLE_INOUT output[8];
    output[0].m_fAge = input[0].m_fAge + g_fElapsedTime;
    output[1].m_fAge = input[0].m_fAge + g_fElapsedTime;
    output[2].m_fAge = input[0].m_fAge + g_fElapsedTime;
    output[3].m_fAge = input[0].m_fAge + g_fElapsedTime;
    output[4].m_fAge = input[0].m_fAge + g_fElapsedTime;
    output[5].m_fAge = input[0].m_fAge + g_fElapsedTime;
    output[6].m_fAge = input[0].m_fAge + g_fElapsedTime;
    output[7].m_fAge = input[0].m_fAge + g_fElapsedTime;


    float fSpeed = length(input[0].m_vVelocity);
    float3 vLook = input[0].m_vVelocity / fSpeed;
    fSpeed *= 1.2;
    
    //로컬좌표 계산
    float3 vUp = float3(0, 1, 0);
    float3 vRight = cross(vUp, vLook);
    float fDistance = fSpeed; //* g_fElapsedTime;
    vUp = cross(vLook, vRight);

    //로컬좌표 번환 행렬
    float3x3 mLocal =
    {
        vRight.x, vRight.y, vRight.z,
        vUp.x, vUp.y, vUp.z,
        vLook.x, vLook.y, vLook.z
    };
    float3x3 mLocalR =
    {
        vRight.x, vUp.x, vLook.x,
        vRight.y, vUp.y, vLook.y,
        vRight.z, vUp.z, vLook.z
    };

    //회전행렬
    float sinP30 = sin(PI / 6);
    float sinM30 = sin(-PI / 6);
    float cosP30 = cos(PI / 6);
    float cosM30 = cos(-PI / 6);
    
    float3x3 mXP =
    {   1, 0, 0,
        0, cosP30, -sinP30,
       0, sinP30, cosP30};
    float3x3 mXM =
    {  1,0,0,
        0,cosM30, -sinM30,
       0, sinM30,  cosM30 };
    float3x3 mYP =
    {
        cosP30, 0, sinP30,
        0,  1, 0,
       -sinP30, 0, cosP30
    };
    float3x3 mYM =
    {
        cosM30, 0, sinM30 ,
        0,  1, 0,
       -sinM30,0 , cosM30
    };

    
    float3 vVelocity_Local = mul(input[0].m_vVelocity, mLocal);
     
    output[0].m_vVelocity = mul(vVelocity_Local,mXP);
    output[1].m_vVelocity = mul(vVelocity_Local,mXM);
    output[2].m_vVelocity = mul(mul(vVelocity_Local, mXP),mYP);
    output[3].m_vVelocity = mul(mul(vVelocity_Local, mXM),mYP);
    output[4].m_vVelocity = mul(mul(vVelocity_Local, mXP),mYM);
    output[5].m_vVelocity = mul(mul(vVelocity_Local, mXM),mYM);
    output[6].m_vVelocity = mul(vVelocity_Local,mYP);
    output[7].m_vVelocity = mul(vVelocity_Local,mYM);

    output[0].m_vVelocity = mul(output[0].m_vVelocity,mLocalR);
    output[1].m_vVelocity = mul(output[1].m_vVelocity,mLocalR);
    output[2].m_vVelocity = mul(output[2].m_vVelocity,mLocalR);
    output[3].m_vVelocity = mul(output[3].m_vVelocity,mLocalR);
    output[4].m_vVelocity = mul(output[4].m_vVelocity,mLocalR);
    output[5].m_vVelocity = mul(output[5].m_vVelocity,mLocalR);
    output[6].m_vVelocity = mul(output[6].m_vVelocity,mLocalR);
    output[7].m_vVelocity = mul(output[7].m_vVelocity,mLocalR);

    output[0].m_vPosition = input[0].m_vPosition + output[0].m_vVelocity * fSpeed*g_fElapsedTime;
    output[1].m_vPosition = input[0].m_vPosition + output[1].m_vVelocity * fSpeed*g_fElapsedTime;
    output[2].m_vPosition = input[0].m_vPosition + output[2].m_vVelocity * fSpeed*g_fElapsedTime;
    output[3].m_vPosition = input[0].m_vPosition + output[3].m_vVelocity * fSpeed*g_fElapsedTime;
    output[4].m_vPosition = input[0].m_vPosition + output[4].m_vVelocity * fSpeed*g_fElapsedTime;
    output[5].m_vPosition = input[0].m_vPosition + output[5].m_vVelocity * fSpeed*g_fElapsedTime;
    output[6].m_vPosition = input[0].m_vPosition + output[6].m_vVelocity * fSpeed*g_fElapsedTime;
    output[7].m_vPosition = input[0].m_vPosition + output[7].m_vVelocity * fSpeed*g_fElapsedTime;
    
    outStream.Append(output[0]);
    outStream.Append(output[1]);
    outStream.Append(output[2]);
    outStream.Append(output[3]);
    outStream.Append(output[4]);
    outStream.Append(output[5]);
    outStream.Append(output[6]);
    outStream.Append(output[7]);

}