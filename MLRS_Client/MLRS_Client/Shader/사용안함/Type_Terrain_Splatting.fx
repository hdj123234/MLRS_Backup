#include"Type_Terrain.fx"



struct HS_HIGHTMAP_SPLATTING_OUT
{
	float3 m_vPosition : POSITION;
	float2 m_vTexCoord : TEXCOORD;
	float2 m_vTexCoord_Detail : DETAILTEXCOORD;
};

struct DS_HIGHTMAP_SPLATTING_OUT
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
	float3 m_vTangent_World : TANGENT;
	float2 m_vTexCoord : TEXCOORD;
	float2 m_vTexCoord_Detail : DETAILTEXCOORD;
};

struct PS_TERRAIN_SPLATTING_INPUT
{
	float4 m_vPosition : SV_Position;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
	float3 m_vTangent_World : TANGENT;
	float2 m_vTexCoord : TEXCOORD;
	float2 m_vTexCoord_Detail : DETAILTEXCOORD;
};