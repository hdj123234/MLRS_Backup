#define SIZE_UNIT 100


struct PS_INPUT_TEXTURECUBE
{
	float4 vPosition : SV_POSITION;
	float3 vPosition_World : POSITION;
    float3 vTexture : TEXCOORD;
	float3 vNormal : NORMAL;
	uint nRenderTarget: SV_RenderTargetArrayIndex;
};


cbuffer cbWorldMatrix : register(b0)
{
	float4x4 g_mWorld : packoffset(c0);
};
cbuffer cbViewMatrix : register(b1)
{
	float4x4 g_mView : packoffset(c0);
};
cbuffer cbProjectionMatrix : register(b2)
{
	float4x4 g_mProjection : packoffset(c0);
};

[maxvertexcount(18)]
void gsBox(point float4 input[1] : SV_POSITION, inout TriangleStream<PS_INPUT_TEXTURECUBE> triStream)
{
	PS_INPUT_TEXTURECUBE output[8] ;

    
    output[0] = (PS_INPUT_TEXTURECUBE) 0;
    output[1] = (PS_INPUT_TEXTURECUBE) 0;
    output[2] = (PS_INPUT_TEXTURECUBE) 0;
    output[3] = (PS_INPUT_TEXTURECUBE) 0;
    output[4] = (PS_INPUT_TEXTURECUBE) 0;
    output[5] = (PS_INPUT_TEXTURECUBE) 0;
    output[6] = (PS_INPUT_TEXTURECUBE) 0;
    output[7] = (PS_INPUT_TEXTURECUBE) 0;

	output[0].vPosition = float4(-SIZE_UNIT, +SIZE_UNIT, -SIZE_UNIT, 1);
	output[1].vPosition = float4(-SIZE_UNIT, -SIZE_UNIT, -SIZE_UNIT, 1);
	output[2].vPosition = float4(+SIZE_UNIT, -SIZE_UNIT, -SIZE_UNIT, 1);
	output[3].vPosition = float4(+SIZE_UNIT, +SIZE_UNIT, -SIZE_UNIT, 1);
	output[4].vPosition = float4(-SIZE_UNIT, +SIZE_UNIT, +SIZE_UNIT, 1);
	output[5].vPosition = float4(-SIZE_UNIT, -SIZE_UNIT, +SIZE_UNIT, 1);
	output[6].vPosition = float4(+SIZE_UNIT, -SIZE_UNIT, +SIZE_UNIT, 1);
	output[7].vPosition = float4(+SIZE_UNIT, +SIZE_UNIT, +SIZE_UNIT, 1);

	float3x3 mWorld_Rotation = float3x3(g_mWorld._11, g_mWorld._12, g_mWorld._13,
										g_mWorld._21, g_mWorld._22, g_mWorld._23, 
										g_mWorld._31, g_mWorld._32, g_mWorld._33);
	

	for (int i = 0; i < 8; ++i)
	{
		output[i].vNormal = normalize(output[i].vPosition.xyz);
		output[i].vTexture = output[i].vNormal;
		output[i].vPosition = mul(output[i].vPosition, g_mWorld);
		output[i].vPosition_World = output[i].vPosition.xyz;
		output[i].vNormal = mul(output[i].vNormal, mWorld_Rotation);
		output[i].vPosition = mul(mul(output[i].vPosition, g_mView), g_mProjection);
		output[i].nRenderTarget = 0;
    }

	//����
    triStream.Append(output[0]);
    triStream.Append(output[4]);
    triStream.Append(output[3]);
    triStream.Append(output[7]);
    triStream.RestartStrip();

	//�Ʒ���
    triStream.Append(output[1]);
    triStream.Append(output[2]);
    triStream.Append(output[5]);
    triStream.Append(output[6]);
    triStream.RestartStrip();

	//�ո�
    triStream.Append(output[3]);
    triStream.Append(output[2]);
    triStream.Append(output[0]);
    triStream.Append(output[1]);
    triStream.Append(output[4]);
    triStream.Append(output[5]);
    triStream.Append(output[7]);
    triStream.Append(output[6]);
    triStream.Append(output[3]);
    triStream.Append(output[2]);

}