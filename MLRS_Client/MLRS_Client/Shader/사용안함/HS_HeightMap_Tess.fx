//���� ��ǥ��
#define MINLENGTH 1000.0f
#define MAXLENGTH 5000000.0f



struct VS_HIGHTMAP_TESS_OUT
{
	float3 m_vPosition : POSITION;
    float2 m_vTexture : TEXCOORD;
};

struct HS_HIGHTMAP_CONSTANT_OUT
{
	float m_fTessEdges[4]:SV_TessFactor;
	float m_fTessInside[2]:SV_InsideTessFactor;

	uint m_nBlockID :PRIMITIVEID;
};

struct HS_HIGHTMAP_OUT
{
	float3 m_vPosition : POSITION;
    float2 m_vTexture : TEXCOORD;
};


cbuffer cbCameraPosition : register(b0)
{
	float3 g_vCameraPosition;
};

static const float g_fMaxRange = (MAXLENGTH - MINLENGTH) ;


HS_HIGHTMAP_CONSTANT_OUT hsConstant(InputPatch<VS_HIGHTMAP_TESS_OUT, 4> input,uint nPatchID:SV_PrimitiveID)
{
	HS_HIGHTMAP_CONSTANT_OUT output = (HS_HIGHTMAP_CONSTANT_OUT)0;
	float3 LinePos[4] = { (float3)0,(float3)0,(float3)0,(float3)0 };		//�»���� ���μ���
	float tmp = (float)0;
	float tessFactor= (float)0;
	//���� ���
	LinePos[0] = (input[0].m_vPosition + input[2].m_vPosition) / 2;
	LinePos[1]=( input[0].m_vPosition+ input[1].m_vPosition)/2;
	LinePos[2]=( input[1].m_vPosition+ input[3].m_vPosition)/2;
	LinePos[3]=( input[2].m_vPosition+ input[3].m_vPosition)/2;

	float3 fDis	=(float3)0;
	float  fLength = float(0);
	for (uint i = 0; i < 4; ++i)
	{
		fDis = g_vCameraPosition - LinePos[i];
		fLength = dot(fDis, fDis);

		if (fLength > MAXLENGTH)
			output.m_fTessEdges[i] = 32;
		else if (fLength < MINLENGTH)
			output.m_fTessEdges[i] = 64;
		else
			output.m_fTessEdges[i] = 32.0f+32.0f*(1.0 - abs(fLength - MINLENGTH) / MAXLENGTH);
	}


	output.m_fTessInside[0] = (output.m_fTessEdges[0]+ output.m_fTessEdges[2])/2;
	output.m_fTessInside[1] = (output.m_fTessEdges[1]+ output.m_fTessEdges[3])/2;

	output.m_nBlockID = nPatchID;
	return output;

}

[domain("quad")]
[partitioning("integer")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("hsConstant")]
[maxtessfactor(64.0f)]
HS_HIGHTMAP_OUT hsHeightMap(InputPatch<VS_HIGHTMAP_TESS_OUT, 4> input, uint i:SV_OutputControlPointID)
{
	HS_HIGHTMAP_OUT output = (HS_HIGHTMAP_OUT)0;
	output.m_vPosition = input[i].m_vPosition;
	output.m_vTexture = input[i].m_vTexture;

	return output;

}
