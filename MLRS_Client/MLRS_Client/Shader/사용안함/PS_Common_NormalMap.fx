
#include"Lighting.fx"



struct PS_INPUT_COMMON
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
	float3 m_vTangent_World: TANGENT;
    float2 m_vTexCoord : TEXCOORD;
};

cbuffer cbMaterial : register(b2)
{
	MATERIAL g_Material;
};

Texture2D g_DiffuseMap: register(t0);
Texture2D g_NormalMap: register(t1);
SamplerState g_SamplerState : register(s0);
//Texture2D gtxtDetailTexture : register(t1);

float4 psCommon_NormalMap(PS_INPUT_COMMON input) : SV_Target0
{
	float3 vNormal = normalize(input.m_vNormal_World);
	float3 vBi_Normal = normalize(cross(vNormal, input.m_vTangent_World));
	float3 vTangent = cross(vBi_Normal, vNormal);

	float3x3 mTBNr = transpose(float3x3 (vTangent.xyz, vBi_Normal.xyz, vNormal.xyz));
	float3 vNormalVector =mul( float3(g_NormalMap.SampleLevel(g_SamplerState, input.m_vTexCoord, 0).xyz), mTBNr);

	float4 vLightColor = Lighting(g_Material,input.m_vPosition_World, vNormalVector);
	float4 vBaseTexColor = g_DiffuseMap.Sample(g_SamplerState, input.m_vTexCoord);

	return  saturate(vLightColor* vBaseTexColor);
}