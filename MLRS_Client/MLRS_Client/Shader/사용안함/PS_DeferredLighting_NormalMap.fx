
#include"Type_DeferredLighting.fx"



struct PS_INPUT_COMMON
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
	float3 m_vTangent_World: TANGENT;
    float2 m_vTexCoord : TEXCOORD;
};

cbuffer cbMaterial : register(b2)
{
	MATERIAL g_Material;
};

Texture2D g_DiffuseMap: register(t0);
Texture2D g_NormalMap: register(t1);
SamplerState g_SamplerState : register(s0);
//Texture2D gtxtDetailTexture : register(t1);

PS_OUTPUT_DEFERREDLIGHTING psDeferredLighting_NormalMap(PS_INPUT_COMMON input) 
{
    PS_OUTPUT_DEFERREDLIGHTING result = (PS_OUTPUT_DEFERREDLIGHTING) 0;
    if (result.m_vDiffuseColor.a<0.1)
        clip(1);
	float3 vNormal = normalize(input.m_vNormal_World);
	float3 vBi_Normal = normalize(cross(vNormal, input.m_vTangent_World));
	float3 vTangent = cross(vBi_Normal, vNormal);

	float3x3 mTBNr = transpose(float3x3 (vTangent.xyz, vBi_Normal.xyz, vNormal.xyz));
	float3 vNormalVector =mul( float3(g_NormalMap.SampleLevel(g_SamplerState, input.m_vTexCoord, 0).xyz), mTBNr);

	result.m_vPosition_World = float4(input.m_vPosition_World,1);
	result.m_vNormal = float4(vNormalVector,1);
    result.m_vMaterial_Specular = g_Material.m_vSpecular;
    result.m_vDiffuseColor = saturate(g_DiffuseMap.Sample(g_SamplerState, input.m_vTexCoord) * g_Material.m_vDiffuse);

	return result;
}