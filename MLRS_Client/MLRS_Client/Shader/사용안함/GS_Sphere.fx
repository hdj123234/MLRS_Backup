#define SIZE_UNIT 50
#define SLICE 5
static const float PI = 3.14159265f;

struct PS_INPUT_TEXTURECUBE
{
	float4 vPosition : SV_POSITION;
	float3 vPosition_World : POSITION;
    float3 vTexCoord : TEXCOORD;
	float3 vNormal : NORMAL;
	uint nRenderTarget: SV_RenderTargetArrayIndex;
};


cbuffer cbWorldMatrix : register(b0)
{
	float4x4 g_mWorld : packoffset(c0);
};
cbuffer cbViewMatrix : register(b1)
{
	float4x4 g_mView : packoffset(c0);
};
cbuffer cbProjectionMatrix : register(b2)
{
	float4x4 g_mProjection : packoffset(c0);
};

[maxvertexcount(2*SLICE*SLICE+SLICE*4+1)]
void gsSphere(point float4 input[1] : SV_POSITION, inout TriangleStream<PS_INPUT_TEXTURECUBE> triStream)
{
	PS_INPUT_TEXTURECUBE top = (PS_INPUT_TEXTURECUBE)0;
	PS_INPUT_TEXTURECUBE bottom = (PS_INPUT_TEXTURECUBE)0;
	PS_INPUT_TEXTURECUBE output[SLICE][SLICE] =
	{ (PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,
		(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,
		(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,
		(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,
		(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 ,(PS_INPUT_TEXTURECUBE)0 };

	float fRadius_XZ = float(0);
	float fposY = float(0);
	float3x3 mWorld_Rotation = float3x3(
		g_mWorld._11_12_13,
		g_mWorld._21_22_23,
		g_mWorld._31_32_33);

	top.vPosition.x = 0;
	top.vPosition.y = SIZE_UNIT;
	top.vPosition.z = 0;
	top.vPosition.w = 1;
	top.vNormal = normalize(top.vPosition.xyz);
	top.vTexCoord = top.vNormal;
	top.vPosition = mul(top.vPosition, g_mWorld);
	top.vPosition_World = top.vPosition.xyz;
	top.vNormal = mul(top.vNormal, mWorld_Rotation);
	top.vPosition = mul(mul(top.vPosition, g_mView), g_mProjection);
	top.nRenderTarget = 0;

	bottom.vPosition.x = 0;
	bottom.vPosition.y = -SIZE_UNIT;
	bottom.vPosition.z = 0;
	bottom.vPosition.w = 1;
	bottom.vNormal = normalize(bottom.vPosition.xyz);
	bottom.vTexCoord = bottom.vNormal;
	bottom.vPosition = mul(bottom.vPosition, g_mWorld);
	bottom.vPosition_World = bottom.vPosition.xyz;
	bottom.vNormal = mul(bottom.vNormal, mWorld_Rotation);
	bottom.vPosition = mul(mul(bottom.vPosition, g_mView), g_mProjection);
	bottom.nRenderTarget = 0;


	[unroll(5)]for (int i = 0; i < SLICE; ++i)
	{
		fposY = SIZE_UNIT * sin((PI / (SLICE+1))*(i + 1) - (PI/2));
		fRadius_XZ = SIZE_UNIT * cos((PI / (SLICE+1))*(i + 1) - (PI/2));
		[unroll(5)]	for (int j = 0; j < SLICE; ++j)
		{
			output[i][j].vPosition.y = fposY;
			output[i][j].vPosition.x = fRadius_XZ * cos(2 * PI / (SLICE)*j);
			output[i][j].vPosition.z = fRadius_XZ * sin(2 * PI / (SLICE)*j);
			output[i][j].vPosition.w = 1;
			output[i][j].vNormal = normalize(output[i][j].vPosition.xyz);
			output[i][j].vTexCoord = output[i][j].vNormal;
			output[i][j].vPosition = mul(output[i][j].vPosition, g_mWorld);
			output[i][j].vPosition_World = output[i][j].vPosition.xyz;
			output[i][j].vNormal = mul(output[i][j].vNormal, mWorld_Rotation);
			output[i][j].vPosition = mul(mul(output[i][j].vPosition, g_mView),g_mProjection);
			output[i][j].nRenderTarget = 0;
		}
	}
	triStream.RestartStrip();


	////맨 위
	for (int i = 0; i < SLICE; ++i)
	{
		triStream.Append(output[SLICE - 1][i]);
		triStream.Append(top);
	}
	triStream.Append(output[SLICE - 1][0]);
	triStream.RestartStrip();
	//중간
	for (int i = 0; i < SLICE-1; ++i)
	{
		for (int j = 0; j < SLICE ; ++j)
		{
			triStream.Append(output[i][j]);
			triStream.Append(output[i + 1][j]);
		}

		triStream.Append(output[i][0]);
		triStream.Append(output[i + 1][0]);
		triStream.RestartStrip();
	}
	//맨 아래
	for (int i = 0; i < SLICE; ++i)
	{
		triStream.Append(bottom);
		triStream.Append(output[0][i]);
	}
	triStream.Append(bottom);
	triStream.Append(output[0][0]);
	triStream.RestartStrip();


}