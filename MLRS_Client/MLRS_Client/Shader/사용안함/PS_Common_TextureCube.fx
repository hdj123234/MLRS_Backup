/*

라이트, 텍스처 적용
*/
#include"Lighting.fx"


struct PS_INPUT_TextureCube
{
	float4 vPosition : SV_POSITION;
	float3 vPosition_World : POSITION;
    float3 vTexCoord : TEXCOORD;
	float3 vNormal : NORMAL;
};

cbuffer cbMaterial : register(b2)
{
	MATERIAL g_Material;
};

TextureCube g_TextureCube : register(t0);
SamplerState g_SamplerState : register(s0);


float4 psCommon_TextureCube(PS_INPUT_TextureCube input) : SV_Target0
{
	float4 vLightColor = Lighting(g_Material,input.vPosition_World, input.vNormal);
	float4 vBaseTexColor = g_TextureCube.Sample(g_SamplerState, input.vTexCoord);
	return saturate(vLightColor* vBaseTexColor);
}