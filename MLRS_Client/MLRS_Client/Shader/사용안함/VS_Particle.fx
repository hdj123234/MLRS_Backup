

struct VS_PARTICLE_INOUT
{
    float3 m_vPosition : POSITION;
    float3 m_vVelocity : VELOCITY;
    float m_fAge : AGE;
};

VS_PARTICLE_INOUT vsParticle(VS_PARTICLE_INOUT input)
{
    return input;
}
