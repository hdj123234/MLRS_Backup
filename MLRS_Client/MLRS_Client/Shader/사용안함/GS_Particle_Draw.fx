#define PARTICLESIZE 2
struct VS_PARTICLE_INOUT
{
    float3 m_vPosition : POSITION;
    float3 m_vVelocity : VELOCITY;
    float m_fAge : AGE;
};


struct GS_PARTICLE_OUT
{
    float4 m_vPosition : SV_Position;
    float4 m_vColor : COLOR;
};

cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};
cbuffer cbProjectionMatrix : register(b1)
{
    float4x4 g_mProjection : packoffset(c0);
};
cbuffer cbElapsedTime : register(b2)
{
    float g_fElapsedTime : packoffset(c0);
};


[maxvertexcount(18)]
void gsParticle_Draw(point VS_PARTICLE_INOUT input[1], inout TriangleStream<GS_PARTICLE_OUT> triStream)
{
    GS_PARTICLE_OUT output[8];
    output[0] = (GS_PARTICLE_OUT) 0;
    output[1] = (GS_PARTICLE_OUT) 0;
    output[2] = (GS_PARTICLE_OUT) 0;
    output[3] = (GS_PARTICLE_OUT) 0;
    output[4] = (GS_PARTICLE_OUT) 0;
    output[5] = (GS_PARTICLE_OUT) 0;
    output[6] = (GS_PARTICLE_OUT) 0;
    output[7] = (GS_PARTICLE_OUT) 0;
    
    output[0].m_vColor = float4(1,1,0,0.8);
    output[1].m_vColor = float4(1,1,0,0.8);
    output[2].m_vColor = float4(1,1,0,0.8);
    output[3].m_vColor = float4(1,1,0,0.8);
    output[4].m_vColor = float4(1,1,0,0.8);
    output[5].m_vColor = float4(1,1,0,0.8);
    output[6].m_vColor = float4(1,1,0,0.8);
    output[7].m_vColor = float4(1,1,0,0.8);
    
    output[0].m_vPosition = float4(-PARTICLESIZE, +PARTICLESIZE, -PARTICLESIZE, 1);
    output[1].m_vPosition = float4(+PARTICLESIZE, +PARTICLESIZE, -PARTICLESIZE, 1);
    output[2].m_vPosition = float4(-PARTICLESIZE, +PARTICLESIZE, +PARTICLESIZE, 1);
    output[3].m_vPosition = float4(+PARTICLESIZE, +PARTICLESIZE, +PARTICLESIZE, 1);
    output[4].m_vPosition = float4(-PARTICLESIZE, -PARTICLESIZE, -PARTICLESIZE, 1);
    output[5].m_vPosition = float4(+PARTICLESIZE, -PARTICLESIZE, -PARTICLESIZE, 1);
    output[6].m_vPosition = float4(-PARTICLESIZE, -PARTICLESIZE, +PARTICLESIZE, 1);
    output[7].m_vPosition = float4(+PARTICLESIZE, -PARTICLESIZE, +PARTICLESIZE, 1);
    
    float4x4 mWorld =
    {   1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
       input[0].m_vPosition.x, input[0].m_vPosition.y, input[0].m_vPosition.z, 1
    };

    float4x4 mWVP = mul(mul(mWorld, g_mView), g_mProjection);

    output[0].m_vPosition = mul(output[0].m_vPosition,mWVP);
    output[1].m_vPosition = mul(output[1].m_vPosition,mWVP);
    output[2].m_vPosition = mul(output[2].m_vPosition,mWVP);
    output[3].m_vPosition = mul(output[3].m_vPosition,mWVP);
    output[4].m_vPosition = mul(output[4].m_vPosition,mWVP);
    output[5].m_vPosition = mul(output[5].m_vPosition,mWVP);
    output[6].m_vPosition = mul(output[6].m_vPosition,mWVP);
    output[7].m_vPosition = mul(output[7].m_vPosition, mWVP);

    //��
    triStream.Append(output[0]);
    triStream.Append(output[2]);
    triStream.Append(output[1]);
    triStream.Append(output[3]);
    triStream.RestartStrip();

    //��
    triStream.Append(output[4]);
    triStream.Append(output[5]);
    triStream.Append(output[6]);
    triStream.Append(output[7]);
    triStream.RestartStrip();
    
    //��
    triStream.Append(output[1]);
    triStream.Append(output[5]);
    triStream.Append(output[0]);
    triStream.Append(output[4]);
    triStream.Append(output[2]);
    triStream.Append(output[6]);
    triStream.Append(output[3]);
    triStream.Append(output[7]);
    triStream.Append(output[1]);
    triStream.Append(output[5]);
    triStream.RestartStrip();

}