//
//

struct HS_HIGHTMAP_CONSTANT_OUT
{
	float m_fTessEdges[4]:SV_TessFactor;
	float m_fTessInside[2]:SV_InsideTessFactor;

	uint m_nBlockID :PRIMITIVEID;
};
struct HS_HIGHTMAP_OUT
{
	float3 m_vPosition : POSITION;
	float2 m_vTexCoord: TEXTURE;
};

struct DS_HIGHTMAP_OUT
{
	float4 m_vPosition : SV_POSITION;
	float3 m_vPosition_World : POSITION;
	float3 m_vNormal_World : NORMAL;
	float3 m_vTangent_World: TANGENT;
	float2 m_vTexCoord : TEXTURE;
};

cbuffer cbWorldMatrix : register(b0)
{
	float4x4 g_mWorld : packoffset(c0);
};

cbuffer cbViewMatrix : register(b1)
{
	float4x4 g_mView : packoffset(c0);	
};

cbuffer cbProjectionMatrix : register(b2)
{
	float4x4 g_mProjection : packoffset(c0);
}

cbuffer cbCameraPosition : register(b3)
{
	float3 g_vCameraPosition : packoffset(c0);
};
cbuffer cbDisplacementRate : register(b4)
{
	float g_fDisplacementRate : packoffset(c0);
};

Texture2D g_NormalMap : register(t0);
SamplerState g_SamplerState : register(s0);

[domain("quad")]
DS_HIGHTMAP_OUT dsWall(HS_HIGHTMAP_CONSTANT_OUT input, float2 uv:SV_DomainLocation, OutputPatch<HS_HIGHTMAP_OUT,4> patch)
{
	DS_HIGHTMAP_OUT output = (DS_HIGHTMAP_OUT)0;

	float3 vWorldPos = (float3)0;
	vWorldPos.x = g_mWorld._14;
	vWorldPos.y = g_mWorld._24;
	vWorldPos.z = g_mWorld._34;
	
	float fDistance = length(g_vCameraPosition - vWorldPos);

	//��ġ(XY)
	float3 v1 = lerp(patch[0].m_vPosition, patch[1].m_vPosition,uv.x);
	float3 v2 = lerp(patch[2].m_vPosition, patch[3].m_vPosition, uv.x);
	float3 position_Tmp = lerp(v1, v2, uv.y);

	//����
	float2 uv1 = lerp(patch[0].m_vTexCoord, patch[1].m_vTexCoord, uv.x);
	float2 uv2 = lerp(patch[2].m_vTexCoord, patch[3].m_vTexCoord, uv.x);
	float2 uv_MapBase = lerp(uv1, uv2, uv.y);

	float3 position = position_Tmp;
	position.z -= g_fDisplacementRate * g_NormalMap.SampleLevel(g_SamplerState, uv_MapBase, 0).w;
	

	float3x3 mWorldRotation = (float3x3)0;
	mWorldRotation._11 = g_mWorld._11;
	mWorldRotation._12 = g_mWorld._12;
	mWorldRotation._13 = g_mWorld._13;
	mWorldRotation._21 = g_mWorld._21;
	mWorldRotation._22 = g_mWorld._22;
	mWorldRotation._23 = g_mWorld._23;
	mWorldRotation._31 = g_mWorld._31;
	mWorldRotation._32 = g_mWorld._32;
	mWorldRotation._33 = g_mWorld._33;

	output.m_vPosition =  mul(float4(position.xyz, 1), g_mWorld);
	output.m_vPosition_World = output.m_vPosition.xyz;
	output.m_vNormal_World = mul(float3(0,0,1), mWorldRotation);
	output.m_vTangent_World = mul(float3(1, 0, 0), mWorldRotation);
	output.m_vTexCoord =  uv_MapBase;
	output.m_vPosition = mul(mul(float4(output.m_vPosition), g_mView), g_mProjection);


	return output;

}
