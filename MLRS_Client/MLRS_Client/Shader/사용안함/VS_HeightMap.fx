//
//

struct VS_INPUT_HIGHTMAP
{
	float3 vPosition : POSITION;
	float3 vNormal : NORMAL;
    float2 vTexCoord : TEXCOORD;
};

struct VS_OUTPUT_HIGHTMAP
{
	float4 vPosition : SV_POSITION;
	float3 vPosition_World : POSITION;
	float3 vNormal_World : NORMAL;
    float2 vTexCoord : TEXCOORD;
};


cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);	
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
}
VS_OUTPUT_HIGHTMAP vsHeightMap(VS_INPUT_HIGHTMAP input)
{
	float4x4 mVP = mul(g_mView, g_mProjection);
	VS_OUTPUT_HIGHTMAP output = (VS_OUTPUT_HIGHTMAP)0;

	output.vPosition_World = input.vPosition;
	output.vPosition = mul(float4(input.vPosition,1), mVP);
	output.vNormal_World = input.vNormal;
	output.vTexCoord = input.vTexCoord;

	return output;

}
