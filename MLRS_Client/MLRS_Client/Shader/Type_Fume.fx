
struct Fume_SprayPoint
{
	float3 m_vPosition : POSITION;
	float3 m_vDirection : DIRECTION;
};

struct Fume_Particle
{
	float3 m_vPosition : POSITION;
	float3 m_vVelocity : VELOCITY;
	float m_fSpeedAttenuation : SPEEDATTENUATION;
	float m_fLifeTime : LIFETIME;
};

struct FumeState
{
	float m_fVariationAngle;
	float m_fInitalSpeed;
	float m_fSpeedAttenuation;
};

