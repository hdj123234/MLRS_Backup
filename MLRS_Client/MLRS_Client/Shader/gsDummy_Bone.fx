#define SIZE_BONE 3
#define SIZE_BONEPATH 0.3

#define DRAWPOSE

//
#define MAX_BONE 52

struct GS_OUTPUT_BoneDummy{
	float4 vPosition : SV_POSITION;
	uint nRenderTarget: SV_RenderTargetArrayIndex;
};

struct FBXDummy_Bone
{
	int m_nParentIndex;
	float4x4 m_mTransform_ToLocal;
};

cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
};

StructuredBuffer<FBXDummy_Bone> g_Bonse : register(t0);
Texture2D<float4> g_vFrameData : register(t1);
tbuffer cbAnimIndicator : register(t2)
{
    uint g_nFrameIndex[MAX_BONE];
}


//작성이 오래걸려 해당 사이트에서 소스코드 카피
//http://answers.unity3d.com/questions/218333/shader-inversefloat4x4-function.html
float4x4 inverseMatrix(float4x4 input)
//float4x4 inverse(float4x4 input)
{
#define minor(a,b,c) determinant(float3x3(input.a, input.b, input.c))
	//determinant(float3x3(input._22_23_23, input._32_33_34, input._42_43_44))

	float4x4 cofactors = float4x4(
		minor(_22_23_24, _32_33_34, _42_43_44),
		-minor(_21_23_24, _31_33_34, _41_43_44),
		minor(_21_22_24, _31_32_34, _41_42_44),
		-minor(_21_22_23, _31_32_33, _41_42_43),

		-minor(_12_13_14, _32_33_34, _42_43_44),
		minor(_11_13_14, _31_33_34, _41_43_44),
		-minor(_11_12_14, _31_32_34, _41_42_44),
		minor(_11_12_13, _31_32_33, _41_42_43),

		minor(_12_13_14, _22_23_24, _42_43_44),
		-minor(_11_13_14, _21_23_24, _41_43_44),
		minor(_11_12_14, _21_22_24, _41_42_44),
		-minor(_11_12_13, _21_22_23, _41_42_43),

		-minor(_12_13_14, _22_23_24, _32_33_34),
		minor(_11_13_14, _21_23_24, _31_33_34),
		-minor(_11_12_14, _21_22_24, _31_32_34),
		minor(_11_12_13, _21_22_23, _31_32_33)
		);
#undef minor
	return transpose(cofactors) / determinant(input);
}




[maxvertexcount(32)]
void gsDummy_Bone(point uint id[1] : VertexID, inout TriangleStream<GS_OUTPUT_BoneDummy> triStream)
{
    GS_OUTPUT_BoneDummy output[8];
    GS_OUTPUT_BoneDummy outputLine[8];

	[unroll(8)]for (int i = 0; i < 8; ++i)
	{
        output[i] = (GS_OUTPUT_BoneDummy) 0;
        outputLine[i] = (GS_OUTPUT_BoneDummy) 0;
    }

	output[0].vPosition = float4(-SIZE_BONE,  SIZE_BONE, -SIZE_BONE,1);
	output[1].vPosition = float4(-SIZE_BONE, -SIZE_BONE, -SIZE_BONE, 1);
	output[2].vPosition = float4( SIZE_BONE, -SIZE_BONE, -SIZE_BONE, 1);
	output[3].vPosition = float4( SIZE_BONE,  SIZE_BONE, -SIZE_BONE, 1);
	output[4].vPosition = float4(-SIZE_BONE,  SIZE_BONE,  SIZE_BONE, 1);
	output[5].vPosition = float4(-SIZE_BONE, -SIZE_BONE,  SIZE_BONE, 1);
	output[6].vPosition = float4( SIZE_BONE, -SIZE_BONE,  SIZE_BONE, 1);
	output[7].vPosition = float4( SIZE_BONE,  SIZE_BONE,  SIZE_BONE, 1);
    
    outputLine[0].vPosition = float4(-SIZE_BONEPATH,  SIZE_BONEPATH, -SIZE_BONEPATH, 1);
    outputLine[3].vPosition = float4( SIZE_BONEPATH,  SIZE_BONEPATH, -SIZE_BONEPATH, 1);
    outputLine[4].vPosition = float4(-SIZE_BONEPATH,  SIZE_BONEPATH,  SIZE_BONEPATH, 1);
    outputLine[7].vPosition = float4( SIZE_BONEPATH,  SIZE_BONEPATH,  SIZE_BONEPATH, 1);
    outputLine[1].vPosition = float4(-SIZE_BONEPATH, -SIZE_BONEPATH, -SIZE_BONEPATH, 1);
    outputLine[2].vPosition = float4( SIZE_BONEPATH, -SIZE_BONEPATH, -SIZE_BONEPATH, 1);
    outputLine[5].vPosition = float4(-SIZE_BONEPATH, -SIZE_BONEPATH,  SIZE_BONEPATH, 1);
    outputLine[6].vPosition = float4( SIZE_BONEPATH, -SIZE_BONEPATH,  SIZE_BONEPATH, 1);


	float4x4 mToRoot =(float4x4)0;
	float4x4 mParentToRoot = { 1,0,0,0, 0,1,0,0,0,0,1,0,0,0,0,1 };
	uint nID = id[0];
#ifdef DRAWPOSE
	mToRoot = inverseMatrix(g_Bonse[nID].m_mTransform_ToLocal);
	nID = g_Bonse[nID].m_nParentIndex;
	mParentToRoot = inverseMatrix(g_Bonse[nID].m_mTransform_ToLocal);
#else
	mToRoot = float4x4( g_vFrameData[uint2(g_nFrameIndex[nID], (nID * 4))],
						g_vFrameData[uint2(g_nFrameIndex[nID], (nID * 4) + 1)],
						g_vFrameData[uint2(g_nFrameIndex[nID], (nID * 4) + 2)],
						g_vFrameData[uint2(g_nFrameIndex[nID], (nID * 4) + 3)]   );
    while(1)
    {
        nID = g_Bonse[nID].m_nParentIndex;
        float4x4 mTmp = float4x4(	g_vFrameData[uint2(g_nFrameIndex[nID], (nID * 4))],
									g_vFrameData[uint2(g_nFrameIndex[nID], (nID * 4) + 1)],
									g_vFrameData[uint2(g_nFrameIndex[nID], (nID * 4) + 2)],
									g_vFrameData[uint2(g_nFrameIndex[nID], (nID * 4) + 3)]    );
        
        mParentToRoot = mul(mParentToRoot, mTmp);
        if (nID == 0)
            break;
    }
	mToRoot = mul(mToRoot, mParentToRoot);
#endif


    [unroll(8)]
    for (uint i = 0; i < 8;++i)
    {
        output[i].vPosition = mul(output[i].vPosition, mToRoot);
        output[i].vPosition = mul(output[i].vPosition, g_mView);
        output[i].vPosition = mul(output[i].vPosition, g_mProjection);
        output[i].nRenderTarget = 0;
        if (i == 0 || i == 3 || i == 4 || i == 7)
        {
            //위의 점 4개는 부모좌표계에 위치하는 점으로 처리
            outputLine[i].vPosition = mul(outputLine[i].vPosition, mParentToRoot);
            outputLine[i].vPosition = mul(outputLine[i].vPosition, g_mView);
            outputLine[i].vPosition = mul(outputLine[i].vPosition, g_mProjection);
            outputLine[i].nRenderTarget = 0;
        }
        else
        {
            //아래의 점 4개는 로컬좌표계에 위치하는 점으로 처리
            outputLine[i].vPosition = mul(outputLine[i].vPosition, mToRoot);
            outputLine[i].vPosition = mul(outputLine[i].vPosition, g_mView);
            outputLine[i].vPosition = mul(outputLine[i].vPosition, g_mProjection);
            outputLine[i].nRenderTarget = 0;
        }

    }


	//A
	triStream.Append(output[2]);
	triStream.Append(output[1]);
	triStream.Append(output[3]);
	triStream.Append(output[0]);
	triStream.Append(output[7]);
	triStream.Append(output[4]);
	triStream.Append(output[6]);
	triStream.Append(output[5]);
	triStream.RestartStrip();
	//B
	triStream.Append(output[3]);
	triStream.Append(output[7]);
	triStream.Append(output[2]);
	triStream.Append(output[6]);
	triStream.Append(output[1]);
	triStream.Append(output[5]);
	triStream.Append(output[0]);
	triStream.Append(output[4]);
	triStream.RestartStrip();

    
	//A
    triStream.Append(outputLine[2]);
    triStream.Append(outputLine[1]);
    triStream.Append(outputLine[3]);
    triStream.Append(outputLine[0]);
    triStream.Append(outputLine[7]);
    triStream.Append(outputLine[4]);
    triStream.Append(outputLine[6]);
    triStream.Append(outputLine[5]);
    triStream.RestartStrip();
	//B
    triStream.Append(outputLine[3]);
    triStream.Append(outputLine[7]);
    triStream.Append(outputLine[2]);
    triStream.Append(outputLine[6]);
    triStream.Append(outputLine[1]);
    triStream.Append(outputLine[5]);
    triStream.Append(outputLine[0]);
    triStream.Append(outputLine[4]);
    triStream.RestartStrip();
}

