//
//
#include"Type_FBX.fx"

cbuffer cbWorldMatrix : register(b0)
{
    float4x4 g_mWorld : packoffset(c0);
};

cbuffer cbViewMatrix : register(b1)
{
    float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b2)
{
    float4x4 g_mProjection : packoffset(c0);
}

StructuredBuffer<FBXDummy_Vertex> g_Vertices : register(t0);
Texture1D<float2> g_vUVs : register(t1);
Buffer<uint> g_nMaterialIndices : register(t2);

PS_INPUT_FBXDummy_MultiMaterial vsDummy_FBX_NoAnim(VS_INPUT_FBXDummy input, uint nVertexID : SV_VertexID)
{
	PS_INPUT_FBXDummy_MultiMaterial output = (PS_INPUT_FBXDummy_MultiMaterial) 0;
	output.m_nMaterialIndex = g_nMaterialIndices[nVertexID / 3];
    output.m_vTexCoord = g_vUVs[input.m_nUVIndex];
    output.m_vNormal_World = input.m_vNormal_World;
    output.m_vTangent_World = input.m_vTangent_World;
    
    output.m_vPosition_World = g_Vertices[input.m_nVertexIndex].m_vPos;
    output.m_vPosition = float4(output.m_vPosition_World.xyz, 1);
    
    output.m_vPosition = mul(output.m_vPosition, g_mWorld);
    output.m_vPosition_World = output.m_vPosition.xyz;
    output.m_vPosition = mul(output.m_vPosition, g_mView);
    output.m_vPosition = mul(output.m_vPosition, g_mProjection);
    
    float3x3 mWorld_Rotation =
    {
        g_mWorld._11, g_mWorld._12, g_mWorld._13,
        g_mWorld._21, g_mWorld._22, g_mWorld._23,
        g_mWorld._31, g_mWorld._32, g_mWorld._33
    };
    output.m_vNormal_World = mul(input.m_vNormal_World, mWorld_Rotation);
    output.m_vTangent_World = mul(input.m_vTangent_World, mWorld_Rotation);

    return output;
}
