  
cbuffer cbViewMatrix : register(b0)
{
    float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
    float4x4 g_mProjection : packoffset(c0);
}

float4 vsCurve(float3 input : POSITION) : SV_POSITION
{
	float4 output = float4(input,1);

	output = mul(output, g_mView);
	output = mul(output, g_mProjection);

    return output;
}
