

Texture2D g_DepthBuffer : register(t0);
SamplerState g_SamplerState_Point : register(s0);
 

float4 psDummy_DrawTexture(float4 m_vPosition : SV_Position) : SV_Target0
{ 
    int3 uvw = int3(m_vPosition.xy,0);
	float f = g_DepthBuffer.Load(uvw);
	return float4(f , 0,0, 1);
}