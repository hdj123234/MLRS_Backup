
struct PS_OUTPUT_DEFERREDLIGHTING
{
	float4 m_vPosition_World	: SV_Target0;
	float4 m_vDiffuseColor		: SV_Target1;
	float4 m_vNormal			: SV_Target2;
	float4 m_vMaterial_Specular : SV_Target3;
};

