#include"Type_Terrain.fx"

Texture2D<float> g_HeightMap : register(t0);
SamplerState g_SamplerState : register(s0);

VS_HIGHTMAP_TESS_OUT vsTerrainTess(VS_HIGHTMAP_TESS_IN input)
{
	VS_HIGHTMAP_TESS_OUT output = (VS_HIGHTMAP_TESS_OUT)0;
	output.m_vPosition.y = g_HeightMap.SampleLevel(g_SamplerState, input.m_vTexCoord, 0);
	output.m_vPosition.x = input.m_vPositionXZ.x;
	output.m_vPosition.z= input.m_vPositionXZ.y;
	output.m_vTexCoord = input.m_vTexCoord;
	return output;

}
