#define SIZE_TESTER 50


struct PS_INPUT_LIGHTTESTER
{
    float4 m_vPosition : SV_POSITION;
    float3 m_vPosition_World : POSITION;
    float3 m_vNormal_World : NORMAL;
    uint m_nRenderTarget : SV_RenderTargetArrayIndex;
};


cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
};

[maxvertexcount(16)]
void gsLightTester(point float4 input[1] : SV_POSITION, inout TriangleStream<PS_INPUT_LIGHTTESTER> triStream)
{
    PS_INPUT_LIGHTTESTER output[8];

	[unroll(8)]for (int i = 0; i < 8; ++i)
	{
        output[i] = (PS_INPUT_LIGHTTESTER) 0;
    }

    output[0].m_vPosition = float4(-SIZE_TESTER, SIZE_TESTER, -SIZE_TESTER, 1);
    output[1].m_vPosition = float4(-SIZE_TESTER, -SIZE_TESTER, -SIZE_TESTER, 1);
    output[2].m_vPosition = float4(SIZE_TESTER, -SIZE_TESTER, -SIZE_TESTER, 1);
    output[3].m_vPosition = float4(SIZE_TESTER, SIZE_TESTER, -SIZE_TESTER, 1);
    output[4].m_vPosition = float4(-SIZE_TESTER, SIZE_TESTER, SIZE_TESTER, 1);
    output[5].m_vPosition = float4(-SIZE_TESTER, -SIZE_TESTER, SIZE_TESTER, 1);
    output[6].m_vPosition = float4(SIZE_TESTER, -SIZE_TESTER, SIZE_TESTER, 1);
    output[7].m_vPosition = float4(SIZE_TESTER, SIZE_TESTER, SIZE_TESTER, 1);

    float4x4 mWorld = float4x4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 200, 0, 1);
    float3x3 mWorldR = float3x3(mWorld._11, mWorld._12, mWorld._13,
                                mWorld._21, mWorld._22, mWorld._23,
                                mWorld._31, mWorld._32, mWorld._33);
    float4x4 mVP = mul(g_mView,g_mProjection);

//
	[unroll(8)]for (int i = 0; i < 8; ++i)
	{
		
        output[i].m_vPosition_World = output[i].m_vPosition.xyz;
        output[i].m_vNormal_World = mul(normalize(output[i].m_vPosition_World), mWorldR);
        output[i].m_vPosition = mul(output[i].m_vPosition, mWorld);
        output[i].m_vPosition = mul(output[i].m_vPosition, mVP);
		output[i].m_nRenderTarget = 0;
	}


	//A
	triStream.Append(output[2]);
	triStream.Append(output[1]);
	triStream.Append(output[3]);
	triStream.Append(output[0]);
	triStream.Append(output[7]);
	triStream.Append(output[4]);
	triStream.Append(output[6]);
	triStream.Append(output[5]);
	triStream.RestartStrip();
	//B
	triStream.Append(output[3]);
	triStream.Append(output[7]);
	triStream.Append(output[2]);
	triStream.Append(output[6]);
	triStream.Append(output[1]);
	triStream.Append(output[5]);
	triStream.Append(output[0]);
	triStream.Append(output[4]);
	triStream.RestartStrip();

}