/*
이하의 리소스를 반드시 연결하여 사용
	상수버퍼 1 : 라이트
	상수버퍼 2 : 재질
	상수버퍼 3 : 카메라 위치


*/
#define MAX_LIGHTS 100
#define LIGHTTYPE_DIRECTIONAL	0
#define LIGHTTYPE_POINT		1
#define LIGHTTYPE_SPOT		2

struct LIGHT
{
	uint m_nType;
	float4 m_vAmbient;
	float4 m_vDiffuse;
	float4 m_vSpecular;
	float3 m_vPosition;
	float3 m_vDirection;
	float4 m_vAttenuation;	// w = MaxRange
	float3 m_vSpotlightData;	//x = Theta, y = Phi, z = Falloff
};

struct MATERIAL
{
	float4 m_vAmbient;
	float4 m_vDiffuse;
	float4 m_vSpecular;
};

cbuffer cbLight : register(b1)
{
	uint g_nNumberOfLight;
	float4 g_vLightGlobalAmbient;
	LIGHT g_Lights[MAX_LIGHTS];
};

cbuffer cbCameraPosition : register(b2)
{
    float3 g_vCameraPosition;
	float3 g_vCameraDirection;
	float2 g_vCameraFlat;	//nearZ, farZ
};

float getDistanceByCameraFlat(float fSVPos_w)
{
	return g_vCameraFlat.x + fSVPos_w * (g_vCameraFlat.y - g_vCameraFlat.x);
}

float getDiffuseFactor_DirectionalLight(uint i, float3 vNormal)
{
    return saturate(dot(vNormal, -g_Lights[i].m_vDirection));
}

float getSpecularFactor_DirectionalLight(uint i, float3 vNormal, float3 vToCamera, float fSpecularPow)
{
    return pow(saturate(dot(reflect(g_Lights[i].m_vDirection, vNormal), vToCamera)), fSpecularPow);
//	return pow(max(dot(reflect(g_Lights[i].m_vDirection, vNormal), vToCamera), 0.0f), g_Material.m_vSpecular.a);
}

float getAttenuationRate(int i, float fDistance)
{
    return saturate(1 / (g_Lights[i].m_vAttenuation.x 
                            + g_Lights[i].m_vAttenuation.y * fDistance 
                            + g_Lights[i].m_vAttenuation.z * fDistance * fDistance));
}

float getDiffuseFactor_PointLight(uint i, float3 vDirection, float3 vNormal)
{
    return saturate(dot(vNormal, -vDirection));
}

float getSpecularFactor_PointLight(uint i, float3 vDirection, float3 vNormal, float3 vToCamera, float fSpecularPow)
{
    return pow(saturate(dot(reflect(vDirection, vNormal), vToCamera)), fSpecularPow);
}

float getSpotFactor(uint i,  float3 vToLight)
{
    float fAlpha = saturate(dot(-g_Lights[i].m_vDirection, vToLight));
    return pow(saturate(((fAlpha - g_Lights[i].m_vSpotlightData.y) / (g_Lights[i].m_vSpotlightData.x - g_Lights[i].m_vSpotlightData.y))), g_Lights[i].m_vSpotlightData.z);
}

float getDiffuseFactor_SpotLight(uint i, float3 vNormal)
{
    return saturate(dot(vNormal, -g_Lights[i].m_vDirection));
}
float4 getSpecularFactor_SpotLight(uint i, float3 vNormal, float3 vToCamera, float fSpecularPow)
{
    return pow(saturate(dot(reflect(g_Lights[i].m_vDirection, vNormal), vToCamera)), fSpecularPow);
}

float4 Lighting(MATERIAL material, float3 vPosition, float3 _vNormal)
{
	uint i;
	float4 vResult = (float4)0;
	float4 vColor = (float4)0;
	float4 vAmbiant = (float4) 0;
	float4 vDeffuse = (float4) 0;
	float4 vSpecular = (float4) 0;

	float3 vNormal = normalize(_vNormal);
	float3 vToCamera = normalize(g_vCameraPosition - vPosition);
	[unroll(10)]
    for (i = 0; i < MAX_LIGHTS; i++)
	{
        vAmbiant = (float4) 0;
        vDeffuse = (float4) 0;
        vSpecular = (float4) 0;
        if (i>=g_nNumberOfLight) break;
		vAmbiant = material.m_vAmbient * g_Lights[i].m_vAmbient;
		if (g_Lights[i].m_nType == LIGHTTYPE_DIRECTIONAL)
		{
            vDeffuse = g_Lights[i].m_vDiffuse * material.m_vDiffuse * getDiffuseFactor_DirectionalLight(i, vNormal);
            vSpecular = g_Lights[i].m_vSpecular * material.m_vSpecular * getSpecularFactor_DirectionalLight(i, vNormal, vToCamera, material.m_vSpecular.a);
        }
		else if (g_Lights[i].m_nType == LIGHTTYPE_POINT)
		{
			float3 vDirection = vPosition - g_Lights[i].m_vPosition;
			float fDistance = length(vDirection);
			vDirection = vDirection / fDistance;
			
			float fAttenuationRate = getAttenuationRate(i,fDistance);

            vDeffuse = g_Lights[i].m_vDiffuse * material.m_vDiffuse * getDiffuseFactor_PointLight(i, vDirection, vNormal) * fAttenuationRate;
            vSpecular = g_Lights[i].m_vSpecular * material.m_vSpecular * getSpecularFactor_PointLight(i, vDirection, vNormal, vToCamera, material.m_vSpecular.a) * fAttenuationRate;
        }
		else if (g_Lights[i].m_nType == LIGHTTYPE_SPOT)
		{
			float3 vToLight = g_Lights[i].m_vPosition - vPosition;
			float fDistance = length(vToLight);
			vToLight = vToLight / fDistance;

			float fSpotFactor = getSpotFactor(i, vToLight);
			float fAttenuationRate = getAttenuationRate(i, fDistance);
			
            vDeffuse = g_Lights[i].m_vDiffuse * material.m_vDiffuse * getDiffuseFactor_SpotLight(i, vNormal) * fAttenuationRate * fSpotFactor;
            vSpecular = g_Lights[i].m_vSpecular * material.m_vSpecular * getSpecularFactor_SpotLight(i, vNormal, vToCamera, material.m_vSpecular.a) * fAttenuationRate * fSpotFactor;
        }
		vColor = saturate(saturate(vAmbiant) + saturate(vDeffuse) + saturate(vSpecular));
//		vColor = saturate(saturate(vAmbiant) + saturate(vDeffuse) );
        vResult += vColor;
    }

	//vResult += saturate(g_vLightGlobalAmbient * material.m_vAmbient);
	vResult.a = material.m_vDiffuse.a;
	return(vResult);
}
