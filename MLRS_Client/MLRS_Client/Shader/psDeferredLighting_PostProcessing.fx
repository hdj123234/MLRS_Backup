/*
PS_Common

라이트, 텍스처 적용
*/
#include"Lighting.fx"
#include"Fog.fx"

struct PS_INPUT_TEXTURECUBE
{
    float4 m_vPosition : SV_POSITION;
    uint m_nRenderTarget : SV_RenderTargetArrayIndex;
};


Texture2D g_txPosition_World	: register(t0);
Texture2D g_txDiffuseColor		: register(t1);
Texture2D g_txNormal			: register(t2);
Texture2D g_txMaterial_Specular : register(t3);


float4 psDeferredLighting_PostProcessing(PS_INPUT_TEXTURECUBE input) : SV_Target0
{
    int3 uvw = int3(input.m_vPosition.xy,0);
	float fDistance= g_txPosition_World.Load(uvw).w;
	float3 vPosition_World = g_txPosition_World.Load(uvw).xyz;
	float3 vNormal = g_txNormal.Load(uvw).xyz;
	MATERIAL material = (MATERIAL) 0;
    material.m_vAmbient = float4(0.1, 0.1, 0.1, 1.0);
    material.m_vDiffuse = float4(1.0, 1.0, 1.0, 1.0);
	material.m_vSpecular = g_txMaterial_Specular.Load(uvw);
	float4 vLightColor = Lighting(material, vPosition_World, vNormal);
    float4 vBaseTexColor = g_txDiffuseColor.Load(uvw);
//	return vBaseTexColor;
//	return saturate(vLightColor * vBaseTexColor);
	return applyFog(saturate(vLightColor * vBaseTexColor), fDistance);
}