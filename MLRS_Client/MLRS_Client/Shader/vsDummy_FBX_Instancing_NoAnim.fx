//
//
#include"Type_FBX.fx"

cbuffer cbViewMatrix : register(b0)
{
    float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
    float4x4 g_mProjection : packoffset(c0);
}

StructuredBuffer<FBXDummy_Vertex> g_Vertices : register(t0);
Texture1D<float2> g_vUVs : register(t1);
Buffer<uint> g_nMaterialIndices : register(t2);

PS_INPUT_FBXDummy_MultiMaterial vsDummy_FBX_Instancing_NoAnim(VS_INPUT_FBXDummy_Instancing input, uint nVertexID : SV_VertexID)
{
	PS_INPUT_FBXDummy_MultiMaterial output = (PS_INPUT_FBXDummy_MultiMaterial) 0;
	output.m_nMaterialIndex = g_nMaterialIndices[nVertexID / 3];
    output.m_vTexCoord = g_vUVs[input.m_nUVIndex];
    output.m_vNormal_World = input.m_vNormal_World;
    output.m_vTangent_World = input.m_vTangent_World;
    
    output.m_vPosition_World = g_Vertices[input.m_nVertexIndex].m_vPos;
    output.m_vPosition = float4(output.m_vPosition_World.xyz, 1);
    
    output.m_vPosition = mul(output.m_vPosition, input.m_mWorld);
    output.m_vPosition_World = output.m_vPosition.xyz;
    output.m_vPosition = mul(output.m_vPosition, g_mView);
    output.m_vPosition = mul(output.m_vPosition, g_mProjection);
    
    float3x3 mWorld_Rotation =
    {
        input.m_mWorld._11, input.m_mWorld._12, input.m_mWorld._13,
        input.m_mWorld._21, input.m_mWorld._22, input.m_mWorld._23,
        input.m_mWorld._31, input.m_mWorld._32, input.m_mWorld._33
    };
    output.m_vNormal_World = mul(input.m_vNormal_World, mWorld_Rotation);
    output.m_vTangent_World = mul(input.m_vTangent_World, mWorld_Rotation);

    return output;
}
