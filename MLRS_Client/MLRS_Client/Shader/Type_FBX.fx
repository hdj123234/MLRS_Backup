
#define MAX_BONE 52

struct FBXDummy_Vertex
{
    float3 m_vPos;
    float4 m_vWeight;
    uint4 m_vBoneIndex;
};

struct FBXDummy_Bone
{
    int m_nParentIndex;
    float4x4 m_mTransform_ToLocal;
};

struct VS_INPUT_FBXDummy
{
    uint m_nVertexIndex : VERTEXINDEX;
    uint m_nUVIndex : UVINDEX;
    float3 m_vNormal_World : NORMAL;
    float3 m_vTangent_World : TANGENT;
};
struct VS_INPUT_FBXDummy_Instancing
{
    uint m_nVertexIndex : VERTEXINDEX;
    uint m_nUVIndex : UVINDEX;
    float3 m_vNormal_World : NORMAL;
    float3 m_vTangent_World : TANGENT;
    row_major float4x4 m_mWorld : WORLDMATRIX;
};
 
struct PS_INPUT_FBXDummy
{
    float4 m_vPosition : SV_POSITION;
    float3 m_vPosition_World : POSITION;
    float3 m_vNormal_World : NORMAL;
    float3 m_vTangent_World : TANGENT;
    float2 m_vTexCoord : TEXCOORD;
};

struct PS_INPUT_FBXDummy_MultiMaterial
{
    float4 m_vPosition : SV_POSITION;
    float3 m_vPosition_World : POSITION;
    float3 m_vNormal_World : NORMAL;
    float3 m_vTangent_World : TANGENT;
    float2 m_vTexCoord : TEXCOORD;
    uint m_nMaterialIndex : MATERIALINDEX;
};
