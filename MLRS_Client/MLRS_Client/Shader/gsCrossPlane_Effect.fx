#include"Type_Billboard_Effect.fx"

cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
};

cbuffer cbBillboardSize : register(b2)
{
    float2 g_vBillboardSize : packoffset(c0);
};

[maxvertexcount(12)]
void gsCrossPlane_Effect(point VS_INOUT_BILLBOARD_EFFECT input[1] , inout TriangleStream<GS_OUTPUT_BILLBOARD_EFFECT> triStream)
{
    GS_OUTPUT_BILLBOARD_EFFECT output[12];

	output[0].m_vPosition = float4(-g_vBillboardSize.x, g_vBillboardSize.y, 0,1);
	output[1].m_vPosition = float4( g_vBillboardSize.x, g_vBillboardSize.y, 0, 1);
	output[2].m_vPosition = float4(-g_vBillboardSize.x,-g_vBillboardSize.y, 0, 1);
    output[3].m_vPosition = float4(g_vBillboardSize.x, -g_vBillboardSize.y, 0, 1);
    output[4].m_vPosition = float4(-g_vBillboardSize.x, 0, g_vBillboardSize.y, 1);
    output[5].m_vPosition = float4(g_vBillboardSize.x, 0, g_vBillboardSize.y,  1);
    output[6].m_vPosition = float4(-g_vBillboardSize.x, 0, -g_vBillboardSize.y,  1);
    output[7].m_vPosition = float4(g_vBillboardSize.x, 0, -g_vBillboardSize.y,  1);
    output[8].m_vPosition = float4(0, -g_vBillboardSize.x, g_vBillboardSize.y, 1);
    output[9].m_vPosition = float4(0, g_vBillboardSize.x, g_vBillboardSize.y,  1);
    output[10].m_vPosition = float4(0, -g_vBillboardSize.x, -g_vBillboardSize.y,  1);
    output[11].m_vPosition = float4(0, g_vBillboardSize.x, -g_vBillboardSize.y,  1);
        
    output[0].m_vTexCoord = float2(0, 0);
    output[1].m_vTexCoord = float2(1, 0);
    output[2].m_vTexCoord = float2(0, 1);
    output[3].m_vTexCoord = float2(1, 1);
    output[4].m_vTexCoord = float2(0, 0);
    output[5].m_vTexCoord = float2(1, 0);
    output[6].m_vTexCoord = float2(0, 1);
    output[7].m_vTexCoord = float2(1, 1);
    output[8].m_vTexCoord = float2(0, 0);
    output[9].m_vTexCoord = float2(1, 0);
    output[10].m_vTexCoord = float2(0, 1);
    output[11].m_vTexCoord = float2(1, 1);

    //float4x4 mWorld =
    //{
    //    -g_mView._11, -g_mView._21, -g_mView._31, 0,
    //     g_mView._12,  g_mView._22,  g_mView._32, 0,
    //    -g_mView._13, -g_mView._23, -g_mView._33, 0,
    //    input[0].m_vPosition.x, input[0].m_vPosition.y, input[0].m_vPosition.z, 1
    //};
    float4x4 mWorld =
    {
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        input[0].m_vPosition.x, input[0].m_vPosition.y, input[0].m_vPosition.z, 1
    };

	[unroll(12)]for (int i = 0; i < 12; ++i)
	{
        output[i].m_vPosition = mul(mul(mul(output[i].m_vPosition, mWorld), g_mView), g_mProjection);
		output[i].m_nTexIndex = input[0].m_nTexIndex;
		output[i].m_vTexCoord_Depth.x = (output[i].m_vPosition.x+1)/2;
		output[i].m_vTexCoord_Depth.y = 1-(output[i].m_vPosition.y+1)/2;
		output[i].m_nRenderTarget = 0;
	}

	triStream.Append(output[0]);
	triStream.Append(output[1]);
	triStream.Append(output[2]);
	triStream.Append(output[3]);
	triStream.RestartStrip();
    
    triStream.Append(output[4]);
    triStream.Append(output[5]);
    triStream.Append(output[6]);
    triStream.Append(output[7]);
    triStream.RestartStrip();

    triStream.Append(output[8]);
    triStream.Append(output[9]);
    triStream.Append(output[10]);
    triStream.Append(output[11]);
    triStream.RestartStrip();
}