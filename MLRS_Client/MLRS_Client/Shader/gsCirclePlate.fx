#define NUMBER_OF_EDGE_DEVIDED3 40
#define NUMBER_OF_EDGE (NUMBER_OF_EDGE_DEVIDED3 * 3)
#define NUMBER_OF_POLYGONVERTEX ((NUMBER_OF_EDGE_DEVIDED3+1) * 5)
#define ANGLE_BY_INDEX (radians(360/NUMBER_OF_EDGE))

struct GS_OUTPUT_CIRCLEPLATE{
	float4 m_vPos : SV_POSITION;
};

cbuffer cbViewMatrix : register(b0)
{
	float4x4 g_mView : packoffset(c0);
};

cbuffer cbProjectionMatrix : register(b1)
{
	float4x4 g_mProjection : packoffset(c0);
}

[maxvertexcount(NUMBER_OF_POLYGONVERTEX)]
void gsCirclePlate(point float4 input[1] : POSANDRADIUS, inout TriangleStream<GS_OUTPUT_CIRCLEPLATE> triStream)
{
	int i;

	float4x4 mWVP 
		= mul(mul(	float4x4(	input[0].w, 0, 0, 0,
								0, input[0].w, 0, 0,
								0, 0, input[0].w, 0,
								input[0].xyz, 1), g_mView), g_mProjection);

	GS_OUTPUT_CIRCLEPLATE vPos_Center = (GS_OUTPUT_CIRCLEPLATE)0;
	vPos_Center.m_vPos = mul(float4(0, 0, 0, 1), mWVP);

	GS_OUTPUT_CIRCLEPLATE vPos_Edge[NUMBER_OF_EDGE];
	[unroll(NUMBER_OF_EDGE)]
	for (i = 0; i < NUMBER_OF_EDGE; ++i)
	{
		vPos_Edge[i] = (GS_OUTPUT_CIRCLEPLATE) 0;
		vPos_Edge[i].m_vPos = mul(float4(cos(i * ANGLE_BY_INDEX), 0, sin(i * ANGLE_BY_INDEX), 1), mWVP);
	}

	
	[unroll(NUMBER_OF_EDGE_DEVIDED3-1)]
	for (i = 0; i < NUMBER_OF_EDGE_DEVIDED3-1;++i)
	{
		triStream.Append(vPos_Edge[(i * 3) + 0]);
		triStream.Append(vPos_Edge[(i * 3) + 1]);
		triStream.Append(vPos_Center);
		triStream.Append(vPos_Edge[(i * 3) + 2]);
		triStream.Append(vPos_Edge[(i * 3) + 3]);
	}
	int n = NUMBER_OF_EDGE_DEVIDED3 - 1;
	triStream.Append(vPos_Edge[(n * 3) + 0]);
	triStream.Append(vPos_Edge[(n * 3) + 1]);
	triStream.Append(vPos_Center);
	triStream.Append(vPos_Edge[(n * 3) + 2]);
	triStream.Append(vPos_Edge[0]);


	triStream.RestartStrip();


}