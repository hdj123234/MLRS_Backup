#include"TYPE_UI.fx"

[maxvertexcount(4)]
void gsUI_ScreenBase_Size_LT(point VS_INOUT_POSANDSIZE_XY input[1], inout TriangleStream<GS_OUTPUT_UI_SCREENBASE> triStream)
{
    GS_OUTPUT_UI_SCREENBASE output[4];
		
    output[0].m_vPosition = float4(							input[0].m_vPosition.x,							input[0].m_vPosition.y, 0, 1);
    output[1].m_vPosition = float4(+input[0].m_vSize.x +	input[0].m_vPosition.x,							input[0].m_vPosition.y, 0, 1);
    output[2].m_vPosition = float4(							input[0].m_vPosition.x, -input[0].m_vSize.y +	input[0].m_vPosition.y, 0, 1);
    output[3].m_vPosition = float4(+input[0].m_vSize.x +	input[0].m_vPosition.x, -input[0].m_vSize.y +	input[0].m_vPosition.y, 0, 1);
        
    output[0].m_vTexCoord = float2(0, 0);
    output[1].m_vTexCoord = float2(1, 0);
    output[2].m_vTexCoord = float2(0, 1);
    output[3].m_vTexCoord = float2(1, 1);

    output[0].m_nRenderTarget 
		= output[1].m_nRenderTarget 
		= output[2].m_nRenderTarget 
		= output[3].m_nRenderTarget = 0;

	triStream.Append(output[0]);
	triStream.Append(output[1]);
	triStream.Append(output[2]);
	triStream.Append(output[3]);
	triStream.RestartStrip();

}