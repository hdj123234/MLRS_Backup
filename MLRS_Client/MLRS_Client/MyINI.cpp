#include "stdafx.h"
#include "MyINI.h"

#include "MyUtility.h"
#ifndef HEADER_STL
#include<map>
#include<fstream>
#include<string>
#include<sstream>
#endif // !HEADER_STL


using namespace std;

//----------------------------------- CMyINISection -------------------------------------------------------------------------------------

void CMyINISection::setParam(const std::string & rsParamName, const std::string & rsParam)
{ 
	SUPER::operator[](convertString(rsParamName)) = convertString(rsParam);
}

const std::string & CMyINISection::getParam(const std::string & rsParamName) const
{
	return SUPER::at(convertString(rsParamName));
}

const std::wstring CMyINISection::getParam_wstring(const std::string & rsParamName) const
{

	return CUtility_String::convertToWString( SUPER::at(convertString(rsParamName)));
}

const DirectX::XMFLOAT4 CMyINISection::getParam_XMFLOAT4(const std::string & rsParamName) const
{
	using DirectX::XMFLOAT4;
	std::stringstream ss(SUPER::at(convertString(rsParamName)));
	float f1, f2, f3, f4;
	char c;
	ss >> f1 >> c >> f2 >> c >> f3 >> c >> f4;

	return XMFLOAT4(f1, f2, f3, f4);
}

const DirectX::XMFLOAT3 CMyINISection::getParam_XMFLOAT3(const std::string & rsParamName) const
{
	using DirectX::XMFLOAT3;
	std::stringstream ss(SUPER::at(convertString(rsParamName)));
	float f1, f2, f3;
	char c;
	ss >> f1 >> c >> f2 >> c >> f3;

	return XMFLOAT3(f1, f2, f3);
}

const DirectX::XMFLOAT3 CMyINISection::getParam_XMFLOAT3_Normalize(const std::string & rsParamName) const
{
	DirectX::XMFLOAT3 retval;
	std::stringstream ss(SUPER::at(convertString(rsParamName)));
	float f1, f2, f3;
	char c;
	ss >> f1 >> c >> f2 >> c >> f3;
	XMStoreFloat3(&retval, DirectX::XMVector3Normalize(DirectX::XMVectorSet(f1, f2, f3, 0)));
	return retval;
}


const DirectX::XMFLOAT2 CMyINISection::getParam_XMFLOAT2(const std::string & rsParamName) const
{
	using DirectX::XMFLOAT2;
	std::stringstream ss(SUPER::at(convertString(rsParamName)));
	float f1, f2;
	char c;
	ss >> f1 >> c >> f2;

	return XMFLOAT2(f1, f2);
}

const DirectX::XMFLOAT2 CMyINISection::getParam_XMFLOAT2_ToRadian(const std::string & rsParamName) const
{
	using DirectX::XMFLOAT2;
	std::stringstream ss(SUPER::at(convertString(rsParamName)));
	float f1, f2;
	char c;
	ss >> f1 >> c >> f2;

	return XMFLOAT2(DirectX::XMConvertToRadians( f1), DirectX::XMConvertToRadians(f2));
}

const DirectX::XMFLOAT4X4 CMyINISection::getParam_XMFLOAT4X4_FromPosAndScale(const std::string & rsParamName) const
{
	using DirectX::XMFLOAT4X4;
	std::stringstream ss(SUPER::at(convertString(rsParamName)));
	float f1, f2, f3, f4;
	char c;
	ss >> f1 >> c >> f2 >> c >> f3 >> c >> f4;
	
	return XMFLOAT4X4(f4,0,0,0,0,f4,0,0,0,0,f4,0,f1,f2,f3,1);
}

const float CMyINISection::getParam_float(const std::string & rsParamName) const
{
	return std::stof(SUPER::at(convertString(rsParamName)));
}

const float CMyINISection::getParam_float_ToRadian(const std::string & rsParamName) const
{
	return DirectX::XMConvertToRadians(std::stof(SUPER::at(convertString(rsParamName))));
}

const short CMyINISection::getParam_short(const std::string & rsParamName) const
{
	return static_cast<short>(std::stoi(SUPER::at(convertString(rsParamName))));
}

const unsigned int CMyINISection::getParam_uint(const std::string & rsParamName) const
{
	return static_cast<unsigned int>(std::stoul(SUPER::at(convertString(rsParamName))));

}

CMyINISection& CMyINISection::setDefaultData(const CMyINISection & rSection)
{
	for (auto &rData : rSection)
		if (!SUPER::count(rData.first))
			SUPER::insert(rData);
	return *this;
}

const std::string CMyINISection::convertString(const std::string & rs)
{
	return CUtility_String::convertToLower(CUtility_String::removeEscape_FrontBack(rs));
}

//----------------------------------- CMyINIData -------------------------------------------------------------------------------------

CMyINIData::CMyINIData(const std::string & rsFileName)
{
	loadData(rsFileName);
}


bool CMyINIData::loadData(const std::string & rsFileName)
{
	using namespace std;

	ifstream inFile(rsFileName);
	if (!inFile.is_open())	return false;
	CMyINISection * pSection = nullptr;
	string s;

	while (!inFile.eof())
	{
		std::getline(inFile, s, '\n');
		std::transform(s.begin(), s.end(), s.begin(), ::tolower);
		if (s[0] == '[')	//���� ó��
		{
			string sectionName(s.begin() + 1, s.end() - 1);
			if (!SUPER::count(sectionName))
				SUPER::insert(make_pair(sectionName,CMyINISection()));
			pSection = &SUPER::at(sectionName);
		}
		else
		{

			auto nIndex = s.find('=');
			if (nIndex == std::string::npos) continue;
			string sParamName;
			string sParamValue;
			char cEqual;
			stringstream ss(s);
			ss >> sParamName;
			ss >> cEqual;
			ss >> sParamValue;
			nIndex = sParamValue.find(';');
			if (nIndex != std::string::npos)
				sParamValue = std::string(sParamValue.begin(), sParamValue.begin() + nIndex);
			else if (!ss.eof())
			{
				string sTmp;
				std::getline(ss, sTmp, ';');
				sParamValue = sParamValue + ' ' + sTmp;
			}
			if (!pSection)
			{
				SUPER::insert(make_pair("", CMyINISection()));
				pSection = &SUPER::at("");
			}
			(*pSection).setParam( sParamName, sParamValue);
		}
	}


	inFile.close();
	return true;
}

CMyINISection CMyINIData::getSection(const std::string & rsSectionName)const
{
	const std::string s = CMyINISection::convertString(rsSectionName);
	if (SUPER::count(s))	return SUPER::at(s);
	else return CMyINISection();
}

CMyINISection & CMyINIData::getSection(const std::string & rsSectionName)
{
	static CMyINISection dummy;
	const std::string s = CMyINISection::convertString(rsSectionName);
	if (SUPER::count(s))	return SUPER::at(s);
	else return dummy;

}

void CMyINIData::setSection_DefaultData(const std::string & rsSectionName, const CMyINISection & rParamiters)
{
	const std::string s = CMyINISection::convertString(rsSectionName);
	if (!SUPER::count(s))	SUPER::insert(make_pair(s,rParamiters));
	else SUPER::at(s).setDefaultData(rParamiters);
}
