#pragma once
#ifndef HEADER_FACTORY_MESH_HEIGHTMAP
#define HEADER_FACTORY_MESH_HEIGHTMAP

#include"Mesh_DynamicLOD.h"
#include"Factory_Mesh_Plane.h"
//#include"Mesh.h"


class CFactory_Mesh_HeightMap_Tess  :	virtual public IFactory_Mesh_Tess,
										private AFactory_Mesh_Plane_VertexIndex{
private:
	const std::string			m_sName;
	const std::string			m_sVSName;
	const std::string			m_sHSName;
	const std::string			m_sDSName;

	static const std::string s_sPrefix_InputLayout;
public:
	CFactory_Mesh_HeightMap_Tess(	const std::string &rsName,
									const std::string &rsVSName,
									const std::string &rsHSName,
									const std::string &rsDSName, 
									const unsigned int	nSizeOfMap,
									const XMFLOAT2		&rvPos,
									const XMFLOAT2		&rvFullSize);
	virtual ~CFactory_Mesh_HeightMap_Tess() {}

	//override IFactory_Mesh_Tess
	virtual ID3D11HullShader	*	createHullShader() ;
	virtual ID3D11DomainShader	*	createDomainShader() ;

	//override IFactory_Mesh_Vertex
	virtual ID3D11VertexShader	*	createVertexShader();
	virtual ID3D11InputLayout	*	createInputLayout();
	virtual ID3D11Buffer		*	createVertexBuffer() { return AFactory_Mesh_Plane_VertexIndex::createVertexBuffer(); }
	virtual ID3D11Buffer		*	createIndexBuffer();
	virtual unsigned int			getStride() { return AFactory_Mesh_Plane_VertexIndex::getStride(); }
	virtual unsigned int			getOffset() { return AFactory_Mesh_Plane_VertexIndex::getOffset(); }
	virtual unsigned int			getNumberOfVertex() { return 0; }
	virtual unsigned int			getNumberOfIndex();

	virtual const unsigned int getMaterialID() { return 0; }
};

#endif