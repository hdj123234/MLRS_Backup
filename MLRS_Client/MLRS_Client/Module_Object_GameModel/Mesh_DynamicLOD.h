#pragma once

#ifndef HEADER_MESH_DISPLACEMENTMAPPING
#define HEADER_MESH_DISPLACEMENTMAPPING

#include"Mesh.h"

struct ID3D11SamplerState;
struct ID3D11HullShader;
struct ID3D11DomainShader;

//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Mesh_Tess : virtual public IFactory_Mesh_VertexIndex {
public:
	virtual ~IFactory_Mesh_Tess() = 0 {}

	virtual ID3D11HullShader	*	createHullShader() = 0;
	virtual ID3D11DomainShader	*	createDomainShader() = 0;
};
class IFactory_Mesh_DynamicLOD_DisplacementMapping : virtual public IFactory_Mesh_Tess {
public:
	virtual ~IFactory_Mesh_DynamicLOD_DisplacementMapping() = 0 {}

	virtual ID3D11ShaderResourceView	*	createDisplacementMap() = 0;
	virtual ID3D11SamplerState	*	createSamplerState() = 0;
	virtual ID3D11Buffer	*	createCameraBuffer() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

class CMesh_Tess : virtual public CMesh_VertexIndex {
private:
	ID3D11HullShader	*	m_pHS;
	ID3D11DomainShader	*	m_pDS;

	//override CMesh_Vertex
	virtual void setOnDX_Shader(CRendererState_D3D *pRendererState)const;
public:
	CMesh_Tess();
	virtual ~CMesh_Tess() {}

	bool createObject(IFactory_Mesh_Tess *pFactory);
};

/* 각 쉐이더의 이하의 슬롯을 사용
	VS : t1, s1
	HS : b1
	DS : t1, s1
*/
class CMesh_DynamicLOD_DisplacementMapping : virtual public CMesh_Tess {
private:
	ID3D11ShaderResourceView	*	m_pDisplacementMap;
	ID3D11SamplerState *m_pSamplerState;
	ID3D11Buffer *m_pCameraBuffer;

protected:
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	CMesh_DynamicLOD_DisplacementMapping();
	virtual ~CMesh_DynamicLOD_DisplacementMapping() {}

	bool createObject(IFactory_Mesh_DynamicLOD_DisplacementMapping *pFactory);
};


#endif