
#include "stdafx.h"
#include "GameModel.h"

#include"..\Module_Renderer\RendererState_D3D.h"

#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif

CGameModel::CGameModel()
	:m_pAnimData(nullptr),
	m_pAnimData_DXSetter(nullptr)
{
}

bool CGameModel::createObject(IFactory_GameModel * pFactory)
{
	if (!pFactory)	return false;
	m_pMeshs				=pFactory->createMeshs();
	m_pMaterials			=pFactory->createMaterials();
	m_pAnimData		=pFactory->createAnimData();

	if (m_pMeshs.empty() || m_pMaterials.empty())	return false;
	
	m_pAnimData_DXSetter = dynamic_cast<RIAnimData_SetterOnDX*>(m_pAnimData);
	if (m_pAnimData&& !m_pAnimData_DXSetter)		return false;
	
	for (auto data : m_pMaterials)
		m_pMaterials_DXSetter.push_back(dynamic_cast<RIMaterial_SetterOnDX*>(data));

	const unsigned int nNumberOfMeshs = static_cast<const unsigned int>(m_pMeshs.size());
	for (unsigned int i = 0; i < nNumberOfMeshs; ++i)
	{
		m_pMeshs_DXSetter.push_back(dynamic_cast<RIMesh_OnDX*>(m_pMeshs[i]));
		m_MaterialMap[m_pMeshs[i]->getMaterialID()].push_back(i);
	}

	return true;
}

void CGameModel::releaseObject()
{
	for(auto data : m_pMeshs)
		delete data;
	m_pMeshs.clear();

	for (auto data : m_pMaterials)
		delete data;
	m_pMeshs.clear();

	if (m_pAnimData)	delete m_pAnimData;
	m_pAnimData = nullptr;

}

void CGameModel::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (!pRendererState)	return ;

	this->drawReady(pRendererState);
	if (m_pAnimData)
		m_pAnimData_DXSetter->setOnDX(pRendererState);
	
	const unsigned int nNumberOfMaterial = static_cast< unsigned int >(m_pMaterials.size());

	for (auto data : m_MaterialMap)
	{

		auto &rIndices = data.second;
		m_pMaterials_DXSetter[data.first]->setOnDX(pRendererState);

		for (auto index : rIndices)
		{
			m_pMeshs_DXSetter[index]->setOnDX(pRendererState);
			m_pMeshs_DXSetter[index]->drawOnDX(pRendererState);

		}
		
	}
//	const unsigned int nNumberOfMesh = static_cast< unsigned int >(m_pMeshs.size());
//	for (unsigned int i = 0; i < nNumberOfMesh; ++i)
//	{
//		m_pMaterials_DXSetter[m_pMeshs[i]->getMaterialID()]->setOnDX(pRendererState);
//	
//		m_pMeshs_DXSetter[i]->setOnDX(pRendererState);
//		m_pMeshs_DXSetter[i]->drawOnDX(pRendererState);
//	}
}

bool CGameModel_BoundingBox::createObject(IFactory_GameModel_BoundingBox * pFactory)
{
	m_vCenter = pFactory->getCenter();
	m_vExtent = pFactory->getExtent();
	return CGameModel::createObject(pFactory);
}
