#include "stdafx.h"
#include "Mesh.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_Renderer\RendererState_D3D.h"

#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif

#ifndef HEADER_DIRECTX
#include<d3d11.h>
#endif // !HEADER_DIRECTX


//----------------------------------- AMesh -------------------------------------------------------------------------------------


AMesh::AMesh()
	:m_nMaterialID(-1),
	m_pVS(nullptr)
{
}

void AMesh::setOnDX_Shader(CRendererState_D3D * pRendererState) const
{
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_HS(nullptr);
	pRendererState->setShader_DS(nullptr);
	pRendererState->setShader_GS(nullptr);
}

void AMesh::setOnDX(CRendererState_D3D * pRendererState) const
{
	this->setOnDX_Shader(pRendererState);
	this->setOnDX_InputAssembler(pRendererState);
	this->setOnDX_ShaderResource(pRendererState);
}

bool AMesh::createObject(IFactory_Mesh * pFactory)
{
	if (!pFactory)	return false;
	m_pVS = pFactory->createVertexShader();
	m_nMaterialID = pFactory->getMaterialID();
	return (m_pVS!=nullptr);
}


//----------------------------------- CMesh_GS -------------------------------------------------------------------------------------

void CMesh_GS::setOnDX_InputAssembler(CRendererState_D3D * pRendererState) const
{
	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	pRendererState->setInputLayout(nullptr);
}

void CMesh_GS::setOnDX_Shader(CRendererState_D3D * pRendererState) const
{
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_HS(nullptr);
	pRendererState->setShader_DS(nullptr);
	pRendererState->setShader_GS(m_pGS);
}

CMesh_GS::CMesh_GS()
	:m_pGS(nullptr)
{
}


void CMesh_GS::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (!pRendererState)	return;
	pRendererState->getDeviceContext()->Draw(1, 0);
}

bool CMesh_GS::createObject(IFactory_Mesh_GS * pFactory)
{
	if (!pFactory)	return false;
	m_pGS = pFactory->createGeometryShader();

	return AMesh::createObject(pFactory);
}

//----------------------------------- CMesh_Vertex -------------------------------------------------------------------------------------

CMesh_Vertex::CMesh_Vertex()
	:m_pInputLayout			(nullptr),
	m_pVBuffer	(nullptr),
	m_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST),
	m_nStride(0),
	m_nOffset(0),
	m_nNumberOfVertex(0)
{
}

void CMesh_Vertex::drawOnDX(CRendererState_D3D * pRendererState) const
{
	pRendererState->getDeviceContext()->Draw(m_nNumberOfVertex, 0);
}

void CMesh_Vertex::setOnDX_InputAssembler(CRendererState_D3D * pRendererState) const
{
	pRendererState->setInputLayout(m_pInputLayout);
	pRendererState->setVertexBuffer(m_pVBuffer, m_nStride, m_nOffset);
	pRendererState->setPrimitive(m_PrimitiveTopology);
}

void CMesh_Vertex::setOnDX_Shader(CRendererState_D3D * pRendererState) const
{
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_HS(nullptr);
	pRendererState->setShader_DS(nullptr);
	pRendererState->setShader_GS(nullptr);
}

bool CMesh_Vertex::createObject(IFactory_Mesh_Vertex * pFactory)
{
	if (!pFactory)	return false;
	m_pVBuffer = pFactory->createVertexBuffer();
	m_pInputLayout = pFactory->createInputLayout();
	m_nStride = pFactory->getStride();
	m_nOffset = pFactory->getOffset();
	m_nNumberOfVertex = pFactory->getNumberOfVertex();
	m_PrimitiveTopology = pFactory->getPrimitiveTopology();
	return AMesh::createObject(pFactory);
}

//----------------------------------- CMesh_VertexIndex -------------------------------------------------------------------------------------

CMesh_VertexIndex::CMesh_VertexIndex()
	:m_pIBuffer(nullptr),
	m_nNumberOfIndex(0)
{
}

void CMesh_VertexIndex::setOnDX_InputAssembler(CRendererState_D3D * pRendererState) const
{
	pRendererState->setIndexBuffer(m_pIBuffer, DXGI_FORMAT::DXGI_FORMAT_R32_UINT, m_nOffset);
	CMesh_Vertex::setOnDX_InputAssembler(pRendererState);
}

void CMesh_VertexIndex::drawOnDX(CRendererState_D3D * pRendererState) const
{
	pRendererState->getDeviceContext()->DrawIndexed(m_nNumberOfIndex, 0, 0);
}

bool CMesh_VertexIndex::createObject(IFactory_Mesh_VertexIndex * pFactory)
{
	if(!pFactory)	return false;

	m_pIBuffer = pFactory->createIndexBuffer();
	m_nNumberOfIndex = pFactory->getNumberOfIndex();
	return CMesh_Vertex::createObject(pFactory);
}

CMesh_VertexAndGS::CMesh_VertexAndGS()
	:CMesh_Vertex(),
	m_pGS(nullptr)
{
}

void CMesh_VertexAndGS::setOnDX_Shader(CRendererState_D3D * pRendererState) const
{
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_HS(nullptr);
	pRendererState->setShader_DS(nullptr);
	pRendererState->setShader_GS(m_pGS);
}

bool CMesh_VertexAndGS::createObject(IFactory_Mesh_VertexAndGS * pFactory)
{
	m_pGS = pFactory->createGeometryShader();
	return CMesh_Vertex::createObject(pFactory);
}

CMesh_VertexIndexAndGS::CMesh_VertexIndexAndGS()
	:CMesh_VertexIndex(),
	m_pGS(nullptr)
{
}

void CMesh_VertexIndexAndGS::setOnDX_Shader(CRendererState_D3D * pRendererState) const
{
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_HS(nullptr);
	pRendererState->setShader_DS(nullptr);
	pRendererState->setShader_GS(m_pGS);
}

bool CMesh_VertexIndexAndGS::createObject(IFactory_Mesh_VertexIndexAndGS * pFactory)
{
	m_pGS = pFactory->createGeometryShader();
	return CMesh_VertexIndex::createObject(pFactory);
}
