#include "stdafx.h"
#include "AnimData.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_Object\Action.h"

#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif

#ifndef HEADER_DIRECTX
#include<d3d11.h>
#endif // !HEADER_DIRECTX

AAnimData::AAnimData()
	:m_pTBuffer_BoneStructure(nullptr),
	m_pTexture_FrameData(nullptr),
	m_pTBuffer_AnimIndicator(nullptr)
{
}

void AAnimData::setOnDX(CRendererState_D3D * pRendererState) const
{
	if (!pRendererState)	return ;
	pRendererState->setShaderResourceViews_VS(m_pTexture_FrameData, 3);
	pRendererState->setShaderResourceViews_VS(m_pTBuffer_BoneStructure, 4);
	pRendererState->setShaderResourceViews_VS(m_pTBuffer_AnimIndicator, 5);
}

bool AAnimData::createObject(IFactory_AnimData * pFactory)
{
	if (!pFactory)	return false;
	m_pTBuffer_BoneStructure = pFactory->createTBuffer_BoneStructure();
	m_pTexture_FrameData = pFactory->createTexture_FrameData();
	m_pTBuffer_AnimIndicator = pFactory->createTBuffer_AnimIndicator();
	m_Actions = pFactory->createActions();
	m_nNumberOfBone = pFactory->getNumberOfBone();
	return (m_pTBuffer_BoneStructure&&m_pTexture_FrameData&&m_pTBuffer_AnimIndicator);
}

CAnimData_NoInstancing::CAnimData_NoInstancing()
	:m_pBuffer_Updater_Indicator(nullptr)
{
}

bool CAnimData_NoInstancing::createObject(IFactory_AnimData_NoInstancing * pFactory)
{
	m_pBuffer_Updater_Indicator = pFactory->createUpdater_Indicator();
	if (!m_pBuffer_Updater_Indicator)	return false;
	return AAnimData::createObject(pFactory);
}
