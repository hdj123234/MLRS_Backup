#include"stdafx.h"

#include "Material.h"
#include"..\Module_Renderer\RendererState_D3D.h"

#include"..\Module_Platform_DirectX11\DXUtility.h"

#ifndef HEADER_DIRECTX
#include<d3d11.h>
#endif // !HEADER_DIRECTX

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX


//----------------------------------- AMaterial -------------------------------------------------------------------------------------

AMaterial::AMaterial()
	:m_pPS(nullptr)
{
}

void AMaterial::setOnDX(CRendererState_D3D * pRendererState) const
{
//	if (!pRendererState)	return;
	pRendererState->setShader_PS(m_pPS);
	this->setOnDX_UpdateBuffer(pRendererState);
	this->setOnDX_ShaderResource(pRendererState);
}

bool AMaterial::createObject(IFactory_Material * pFactory)
{ 
	if (!pFactory)	return false;

	m_pPS = pFactory->createPixelShader();
	return m_pPS != nullptr;
}


//----------------------------------- CMaterial_NoneLighting_ColorOnly -------------------------------------------------------------------------------------

CMaterial_NoneLighting_ColorOnly::CMaterial_NoneLighting_ColorOnly()
	:m_pCB_Color(nullptr)
{
}

void CMaterial_NoneLighting_ColorOnly::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_PS(m_pCB_Color);
}

bool CMaterial_NoneLighting_ColorOnly::createObject(IFactory_Material_NoneLighting_ColorOnly * pFactory)
{
	m_pCB_Color = pFactory->createCB_Color();
	return AMaterial::createObject(pFactory);
}

//----------------------------------- CMaterial_NoneLighting_Diffuse -------------------------------------------------------------------------------------

void CMaterial_NoneLighting_Diffuse::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setSamplerStates_PS(m_pSamplerState);
	pRendererState->setShaderResourceViews_PS(m_pDiffuseMap);
}

bool CMaterial_NoneLighting_Diffuse::createObject(IFactory_Material_NoneLighting_Diffuse * pFactory)
{
	if(!pFactory)	return false;
	m_pDiffuseMap = pFactory->createDiffuseMap();
	m_pSamplerState = pFactory->createSamplerState();
	if (!(m_pDiffuseMap&&m_pSamplerState))	return false;
	
	return AMaterial::createObject(pFactory);

}

//----------------------------------- CMaterial_NoneLighting_Diffuse_DepthBiasBlending -------------------------------------------------------------------------------------

CMaterial_NoneLighting_Diffuse_DepthBiasBlending::CMaterial_NoneLighting_Diffuse_DepthBiasBlending()
	: m_pSRV_DepthBuffer(nullptr), 
	m_pDSS(nullptr), 
	m_pDSV(nullptr),
	m_pSamplerState_Point(nullptr) ,
	m_pCB_ProjectionInverse(nullptr)
{
}
void CMaterial_NoneLighting_Diffuse_DepthBiasBlending::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setDepthStencilState(m_pDSS);
	pRendererState->setDepthStencilView(m_pDSV);

	pRendererState->setSamplerStates_PS(std::vector<ID3D11SamplerState *>{m_pSamplerState, m_pSamplerState_Point});
	pRendererState->setShaderResourceViews_PS(std::vector<ID3D11ShaderResourceView *>{m_pDiffuseMap, m_pSRV_DepthBuffer});
	pRendererState->setConstBuffer_PS(m_pCB_ProjectionInverse);
//	pRendererState->setShaderResourceViews_PS(tmp);
}

bool CMaterial_NoneLighting_Diffuse_DepthBiasBlending::createObject(IFactory_Material_NoneLighting_Diffuse_DepthBiasBlending * pFactory)
{
	m_pSRV_DepthBuffer = pFactory->createSRV_DepthBuffer();
	m_pDSV = pFactory->createDepthStencilView();
	m_pDSS = pFactory->createDepthStencilState();
	m_pSamplerState_Point = pFactory->createSamplerState_Point();
	m_pCB_ProjectionInverse = pFactory->createCB_ProjectionInverse();
	if (!(m_pSRV_DepthBuffer&&m_pDSS))	return false;
	return CMaterial_NoneLighting_Diffuse::createObject(pFactory);
}

//----------------------------------- CMaterial_Lambert -------------------------------------------------------------------------------------

CMaterial_Lambert const * CMaterial_Lambert::s_pLatestMaterial = nullptr;

void CMaterial_Lambert::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_PS(m_pCBuffer_Lighting);

}

CMaterial_Lambert::CMaterial_Lambert()
	:m_pMaterialBuffer(nullptr)
{
}


void CMaterial_Lambert::setOnDX_UpdateBuffer(CRendererState_D3D * pRendererState)const
{
	if (!(s_pLatestMaterial != this&&pRendererState&&m_pMaterialBuffer))	return;

	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();
	
	if (!CBufferUpdater::update<Type_Material>(pDeviceContext, m_pMaterialBuffer, m_Material))
		return;

	s_pLatestMaterial = this;
}

bool CMaterial_Lambert::createObject(IFactory_Material_Lambert * pFactory)
{
	if (!pFactory)	return false;
	m_Material = pFactory->getMaterials();
	m_pMaterialBuffer = pFactory->createMaterialBuffer();
	m_pCBuffer_Lighting = pFactory->createConstBuffer_Lighting();

	return AMaterial::createObject(pFactory);
}

//----------------------------------- CMaterial_Lambert_DiffuseNormal -------------------------------------------------------------------------------------

void CMaterial_Lambert_DiffuseNormal::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	CMaterial_Lambert::setOnDX_ShaderResource(pRendererState);
	pRendererState->setSamplerStates_PS(m_pSamplerState);
	pRendererState->setShaderResourceViews_PS(m_pMaps_DiffuseNormal);
}

CMaterial_Lambert_DiffuseNormal::CMaterial_Lambert_DiffuseNormal()
	:m_pSamplerState(nullptr)
{
}

bool CMaterial_Lambert_DiffuseNormal::createObject(IFactory_Material_Lambert_DiffuseNormal * pFactory)
{
	if (!pFactory)	return false;
	m_pSamplerState = pFactory->createSamplerState();
	m_pMaps_DiffuseNormal = pFactory->createTextureMaps();
	if (!m_pSamplerState ||m_pMaps_DiffuseNormal.size() != 2)	return false;
	return CMaterial_Lambert::createObject(pFactory);
}

//----------------------------------- CMaterialSet_Lambert_DiffuseNormal -------------------------------------------------------------------------------------

void CMaterialSet_Lambert_DiffuseNormal::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_PS(m_pCBuffer_Lighting);
	pRendererState->setShaderResourceViews_PS(m_pSRVs);
}

bool CMaterialSet_Lambert_DiffuseNormal::createObject(IFactory_MaterialSet_Lambert_DiffuseNormal * pFactory)
{
	m_pCBuffer_Lighting = pFactory->createConstBuffer_Lighting();
	m_pSRVs = pFactory->createSRVs();
	if (m_pSRVs.size() != 3)	return false;
	return AMaterial::createObject(pFactory);
}

CMaterial_Lambert_Splatting::CMaterial_Lambert_Splatting()
	:m_pSamplerState(nullptr),
	m_pCB_NumberOfSplatting(nullptr),
	m_pCB_BlockSizeOfDetail(nullptr)
{
}

void CMaterial_Lambert_Splatting::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	CMaterial_Lambert::setOnDX_ShaderResource(pRendererState);
	pRendererState->setSamplerStates_PS(m_pSamplerState);
	pRendererState->setShaderResourceViews_PS(m_pMaps);
	pRendererState->setConstBuffer_PS(m_pCB_NumberOfSplatting,3);
	pRendererState->setConstBuffer_PS(m_pCB_BlockSizeOfDetail,4);
}

bool CMaterial_Lambert_Splatting::createObject(IFactory_Material_Lambert_Splatting * pFactory)
{
	if (!pFactory)	return false;
	m_pSamplerState = pFactory->createSamplerState();
	m_pMaps = pFactory->createTextureMaps();
	m_pCB_NumberOfSplatting = pFactory->createCB_NumberOfSplatting();
	m_pCB_BlockSizeOfDetail = pFactory->createCB_BlockSizeOfDetail();
	if (!(m_pSamplerState && m_pMaps.size() == 4))	return false;
	return CMaterial_Lambert::createObject(pFactory);
}
