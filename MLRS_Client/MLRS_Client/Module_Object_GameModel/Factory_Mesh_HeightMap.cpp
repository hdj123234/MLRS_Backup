#include"stdafx.h"
#include "Factory_Mesh_HeightMap.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXBuffer.h"


const std::string CFactory_Mesh_HeightMap_Tess::s_sPrefix_InputLayout = std::string("IL_");

CFactory_Mesh_HeightMap_Tess::CFactory_Mesh_HeightMap_Tess(
	const std::string & rsName,
	const std::string & rsVSName,
	const std::string & rsHSName,
	const std::string & rsDSName,
	const unsigned int	nSizeOfMap,
	const XMFLOAT2		&rvPos,
	const XMFLOAT2		&rvFullSize)
	:AFactory_Mesh_Plane_VertexIndex(rsName,rvPos,rvFullSize, nSizeOfMap/64, nSizeOfMap/64),
	m_sName	(rsName),
	m_sVSName(rsVSName),
	m_sHSName(rsHSName),
	m_sDSName(rsDSName)
{
}

ID3D11VertexShader * CFactory_Mesh_HeightMap_Tess::createVertexShader()
{
	return CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
}

ID3D11InputLayout * CFactory_Mesh_HeightMap_Tess::createInputLayout()
{
	return CFactory_InputLayout(m_sVSName, 
								m_sVSName, 
								s_sPrefix_InputLayout+m_sName,
								AFactory_Mesh_Plane_VertexIndex::getInputElementDescs())
		.createInputLayout();
}

ID3D11Buffer * CFactory_Mesh_HeightMap_Tess::createIndexBuffer()
{
	//   0   1
	//   2   3
	std::vector<UINT> indices;
	unsigned int nVertexSizeX = m_nSliceX + 1;
	indices.reserve(m_nSliceY * m_nSliceX * 4);

	for (unsigned int i = 0; i < m_nSliceY; ++i)
	{
		for (unsigned int j = 0; j < m_nSliceX; ++j)
		{
			indices.push_back((i + 1)*nVertexSizeX + j);
			indices.push_back((i + 1)*nVertexSizeX + j+1);
			indices.push_back(i*nVertexSizeX + j);
			indices.push_back(i*nVertexSizeX + j+1);
		}
	}

	ID3D11Buffer *pIndexBuffer = 
		CFactory_IndexBuffer(s_sPrefix_IB + m_sName, 
							static_cast<UINT>(indices.size())).createBuffer(indices.data());
	return pIndexBuffer;
}

unsigned int CFactory_Mesh_HeightMap_Tess::getNumberOfIndex()
{
	return m_nSliceY * m_nSliceX  * 4;
}

ID3D11HullShader * CFactory_Mesh_HeightMap_Tess::createHullShader()
{
	return CFactory_HullShader(m_sHSName, m_sHSName).createShader();
}

ID3D11DomainShader * CFactory_Mesh_HeightMap_Tess::createDomainShader()
{
	return CFactory_DomainShader(m_sDSName, m_sDSName).createShader();
}
