#pragma once

#ifndef HEADER_MESH
#define HEADER_MESH

#include"Interface_GameModel.h"

//���漱��

struct D3D11_INPUT_ELEMENT_DESC;

struct ID3D11Buffer;
struct ID3D11InputLayout;
struct ID3D11VertexShader	;
struct ID3D11GeometryShader;



//----------------------------------- Creator Interface -------------------------------------------------------------------------------------
class IFactory_Mesh {
public:
	virtual ~IFactory_Mesh() = 0 {}

	virtual ID3D11VertexShader		* createVertexShader() = 0;
	virtual const unsigned int getMaterialID() = 0;
};

class RIFactory_Mesh_VertexBuffer {
public:
	virtual ~RIFactory_Mesh_VertexBuffer() = 0 {}

	virtual ID3D11InputLayout	*	createInputLayout() = 0;
	virtual ID3D11Buffer		*	createVertexBuffer() = 0;
	virtual unsigned int			getStride() = 0;
	virtual unsigned int			getOffset() = 0;
	virtual unsigned int			getNumberOfVertex() = 0;
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology()=0;
};

class RIFactory_Mesh_IndexBuffer {
public:
	virtual ~RIFactory_Mesh_IndexBuffer() = 0 {}

	virtual ID3D11Buffer		*	createIndexBuffer() = 0;
	virtual unsigned int			getNumberOfIndex() = 0;
};

class IFactory_Mesh_GS : virtual public IFactory_Mesh {
public:
	virtual ~IFactory_Mesh_GS() = 0 {}

	virtual ID3D11GeometryShader	* createGeometryShader()=0;
};

class IFactory_Mesh_Vertex : virtual public IFactory_Mesh,
							 virtual public RIFactory_Mesh_VertexBuffer {
public:
	virtual ~IFactory_Mesh_Vertex() = 0 {}

};
class IFactory_Mesh_VertexAndGS : virtual public IFactory_Mesh_Vertex,
									virtual public IFactory_Mesh_GS {
public:
	virtual ~IFactory_Mesh_VertexAndGS() = 0 {}

};

class IFactory_Mesh_VertexIndex :	virtual public IFactory_Mesh_Vertex,
									virtual public RIFactory_Mesh_IndexBuffer {
public:
	virtual ~IFactory_Mesh_VertexIndex() = 0 {}
};

class IFactory_Mesh_VertexIndexAndGS : virtual public IFactory_Mesh_VertexIndex,
										virtual public IFactory_Mesh_GS {
public:
	virtual ~IFactory_Mesh_VertexIndexAndGS() = 0 {}

};

//----------------------------------- Class -------------------------------------------------------------------------------------

class AMesh : virtual public IMesh,
				virtual public RIMesh_OnDX {
private:
	unsigned int m_nMaterialID;

protected:
	ID3D11VertexShader		* m_pVS;

public:
	AMesh();
	virtual ~AMesh(){}

private:
	//virtual Function
	virtual void setOnDX_Shader(CRendererState_D3D *pRendererState)const;
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const {}

public:
	//override IMesh
	virtual const unsigned int getMaterialID() { return m_nMaterialID; }

	//override IObject_SetOnDX
	virtual void setOnDX(CRendererState_D3D *pRendererState)const;

	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D* pRendererState)const = 0;

	bool createObject(IFactory_Mesh *pFactory);
};

class CMesh_GS : public AMesh {
private:
	ID3D11GeometryShader	* m_pGS;

public:
	CMesh_GS();
	virtual ~CMesh_GS() {}

private:
	virtual void setOnDX_Shader(CRendererState_D3D *pRendererState)const;
	virtual void setOnDX_InputAssembler(CRendererState_D3D *pRendererState)const;

public:
	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D* pRendererState)const ;

	bool createObject(IFactory_Mesh_GS *pFactory);
};

class CMesh_Vertex : public AMesh {
protected:
	ID3D11InputLayout		* m_pInputLayout;
	ID3D11Buffer			* m_pVBuffer;
	unsigned int			m_nStride;
	unsigned int			m_nOffset;
	unsigned int			m_nNumberOfVertex;
	D3D11_PRIMITIVE_TOPOLOGY m_PrimitiveTopology;

public:
	CMesh_Vertex();
	virtual ~CMesh_Vertex() {}

private:
	virtual void setOnDX_Shader(CRendererState_D3D *pRendererState)const;

protected:
	virtual void setOnDX_InputAssembler(CRendererState_D3D *pRendererState)const;

public:
	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D* pRendererState)const;

	bool createObject(IFactory_Mesh_Vertex *pFactory);
};

class CMesh_VertexAndGS : public CMesh_Vertex {
protected:
	ID3D11GeometryShader	* m_pGS;

public:
	CMesh_VertexAndGS();
	virtual ~CMesh_VertexAndGS() {}

private:
	virtual void setOnDX_Shader(CRendererState_D3D *pRendererState)const;

public:
	bool createObject(IFactory_Mesh_VertexAndGS *pFactory);
};

class CMesh_VertexIndex : public CMesh_Vertex {
protected:
	ID3D11Buffer			* m_pIBuffer;
	unsigned int			m_nNumberOfIndex;
public:
	CMesh_VertexIndex();
	virtual ~CMesh_VertexIndex() {}

private:
	//virtual Function
	virtual void setOnDX_InputAssembler(CRendererState_D3D *pRendererState)const;

public:
	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D* pRendererState)const;

	bool createObject(IFactory_Mesh_VertexIndex *pFactory);
};

class CMesh_VertexIndexAndGS : public CMesh_VertexIndex {
protected:
	ID3D11GeometryShader	* m_pGS;

public:
	CMesh_VertexIndexAndGS();
	virtual ~CMesh_VertexIndexAndGS() {}

private:
	virtual void setOnDX_Shader(CRendererState_D3D *pRendererState)const;

public:
	bool createObject(IFactory_Mesh_VertexIndexAndGS *pFactory);
};

#endif