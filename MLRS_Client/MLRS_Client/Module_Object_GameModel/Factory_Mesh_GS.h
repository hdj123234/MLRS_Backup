#pragma once
#ifndef HEADER_FACTORY_MESH_GS
#define HEADER_FACTORY_MESH_GS

#include"Mesh.h"


class CFactory_Mesh_GS  :virtual public IFactory_Mesh_GS {
private:
	std::string			m_sFileName;
	std::string			m_sFuncName;
public:
	CFactory_Mesh_GS(const std::string &rsFileName, const std::string &rsFuncName);
	virtual ~CFactory_Mesh_GS() {}

	virtual ID3D11VertexShader		* createVertexShader() ;
	virtual ID3D11GeometryShader	* createGeometryShader() ;

	virtual const unsigned int getMaterialID() { return 0; }
};

#endif