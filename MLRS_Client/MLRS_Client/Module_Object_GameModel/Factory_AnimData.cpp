#include "stdafx.h"
#include "Factory_AnimData.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_Object\Action.h"


//----------------------------------- AFactory_AnimData -------------------------------------------------------------------------------------

AFactory_AnimData::AFactory_AnimData(
	const std::string &rsName,
	const std::vector<Type_Bone> &rnBoneStructure,
	const std::vector<std::vector<DirectX::XMFLOAT4X4>> &rmFrameData,
	const StartPointList &rList,
	const float fFPS)
	:m_sName(rsName),
	m_nBoneStructure(rnBoneStructure),
	m_mFrameData(rmFrameData),
	m_StartPointList(rList),
	m_fFPS(fFPS)
{
}
ID3D11ShaderResourceView * AFactory_AnimData::createTBuffer_BoneStructure()
{
	ID3D11ShaderResourceView *pBuffer = CFactory_SRV_BufferEX_FromData<Type_Bone>(m_sName + "BoneStructure", m_nBoneStructure)
		.createShaderResourceView();
	return pBuffer;
}

ID3D11ShaderResourceView * AFactory_AnimData::createTexture_FrameData()
{
	unsigned int nHeight = static_cast<unsigned int>(m_mFrameData.size());
	unsigned int nWidth = static_cast<unsigned int>(m_mFrameData[0].size());
	std::vector<DirectX::XMFLOAT4> data;
	data.resize(nWidth*nHeight * 4);
	for (unsigned int i = 0; i < nHeight; ++i)
	{
		for (unsigned int j = 0; j < nWidth; ++j)
		{
			data[(i * 4 + 0)*nWidth + j] = DirectX::XMFLOAT4(
				m_mFrameData[i][j]._11,
				m_mFrameData[i][j]._12,
				m_mFrameData[i][j]._13,
				m_mFrameData[i][j]._14);

			data[(i * 4 + 1)*nWidth + j] = DirectX::XMFLOAT4(
				m_mFrameData[i][j]._21,
				m_mFrameData[i][j]._22,
				m_mFrameData[i][j]._23,
				m_mFrameData[i][j]._24);

			data[(i * 4 + 2)*nWidth + j] = DirectX::XMFLOAT4(
				m_mFrameData[i][j]._31,
				m_mFrameData[i][j]._32,
				m_mFrameData[i][j]._33,
				m_mFrameData[i][j]._34);

			data[(i * 4 + 3)*nWidth + j] = DirectX::XMFLOAT4(
				m_mFrameData[i][j]._41,
				m_mFrameData[i][j]._42,
				m_mFrameData[i][j]._43,
				m_mFrameData[i][j]._44);
		}

	}

	ID3D11ShaderResourceView *pSRV = CFactory_SRV_Texture2D_FromData<DirectX::XMFLOAT4>(m_sName + "FrameData",
		data,
		DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT,
		nWidth,
		nHeight * 4)
		.createShaderResourceView();

	return pSRV;
}

std::vector<CAction> AFactory_AnimData::createActions()
{
	std::vector<CAction> result;
	auto iter = m_StartPointList.begin();
	auto iter_Next = ++m_StartPointList.begin();
	auto iter_End = m_StartPointList.end();
	while (1)
	{
		if (iter_Next == iter_End)
		{
			result.push_back(CAction(iter->first,
				static_cast<unsigned int>(m_mFrameData[0].size()) - 1,
				iter->second,
				m_fFPS));
			break;
		}
		else
			result.push_back(CAction(iter->first, iter_Next->first - 1, iter->second, m_fFPS));
		++iter;
		++iter_Next;

	}
	return result;
}

//----------------------------------- CFactory_AnimData_NoInstancing -------------------------------------------------------------------------------------

CFactory_AnimData_NoInstancing::CFactory_AnimData_NoInstancing(
	const std::string & rsName, 
	const std::vector<Type_Bone>& rnBoneStructure,
	const std::vector<std::vector<DirectX::XMFLOAT4X4>>& rmFrameData, 
	const AFactory_AnimData::StartPointList & rList,
	const float fFPS)
	:AFactory_AnimData(rsName, rnBoneStructure, rmFrameData, rList, fFPS),
	m_pBuffer(nullptr)
{
}

bool CFactory_AnimData_NoInstancing::createBuffer_RealProcessing()
{
	std::vector<Type_AnimIndicatorElement> tmp(m_nBoneStructure.size());
	for (auto &rData : tmp) rData = 0;

	if (m_pBuffer)	return false;
	m_pBuffer = CFactory_Buffer<Type_AnimIndicatorElement>(
		std::string("BUF") + m_sName + std::string("AnimIndicator"),
		static_cast<UINT>(m_nBoneStructure.size()),
		D3D11_USAGE::D3D11_USAGE_DYNAMIC,
		D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
		D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE)
		.createBuffer(tmp.data());
	return (m_pBuffer != nullptr);
}

ID3D11ShaderResourceView * CFactory_AnimData_NoInstancing::createTBuffer_AnimIndicator()
{
	if (!m_pBuffer)
		if (!createBuffer_RealProcessing())
			return nullptr;

	return CFactory_ShaderResourceView_FromBuffer(std::string("SRV") + m_sName + std::string("AnimIndicator"),
		m_pBuffer,
		DXGI_FORMAT::DXGI_FORMAT_R32_UINT,
		static_cast<unsigned int>(m_nBoneStructure.size()))
		.createShaderResourceView();

}

ID3D11Buffer * CFactory_AnimData_NoInstancing::createUpdater_Indicator()
{
	if (!m_pBuffer)
		if (!createBuffer_RealProcessing())
			return nullptr;

	return m_pBuffer;
}
