#include"stdafx.h"
#include "Factory_Mesh_Plane.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXBuffer.h"

std::vector<D3D11_INPUT_ELEMENT_DESC> AFactory_Mesh_Plane::getInputElementDescs()
{
	return std::vector<D3D11_INPUT_ELEMENT_DESC>({
		{ "POSITION2D"	, 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "TEXCOORD"	, 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } });
}

AFactory_Mesh_Plane::AFactory_Mesh_Plane(
	const std::string &rsName,
	const XMFLOAT2 & rvPos,
	const XMFLOAT2 & rvFullSize, 
	const unsigned int nSlice)
	:m_sName(rsName), 
	m_vPos(rvPos),
	m_vFullSize(rvFullSize.x , rvFullSize.y ),
	m_nSliceX(nSlice),
	m_nSliceY(nSlice)
{
}

AFactory_Mesh_Plane::AFactory_Mesh_Plane(
	const std::string &rsName,
	const XMFLOAT2 & rvPos,
	const XMFLOAT2 & rvFullSize, 
	const unsigned int nSliceX, 
	const unsigned int nSliceY)
	:m_sName(rsName),
	m_vPos(rvPos),
	m_vFullSize(rvFullSize.x , rvFullSize.y ),
	m_nSliceX(nSliceX),
	m_nSliceY(nSliceY)
{
}

const std::string AFactory_Mesh_Plane_VertexIndex::s_sPrefix_VB="VB_";
const std::string AFactory_Mesh_Plane_VertexIndex::s_sPrefix_IB="IB_";
AFactory_Mesh_Plane_VertexIndex::AFactory_Mesh_Plane_VertexIndex(
	const std::string &rsName,
	const XMFLOAT2 & rvPos,
	const XMFLOAT2 & rvFullSize,
	const unsigned int nSlice)
	:AFactory_Mesh_Plane(rsName,rvPos,	rvFullSize,nSlice)
{
}

AFactory_Mesh_Plane_VertexIndex::AFactory_Mesh_Plane_VertexIndex(
	const std::string &rsName, 
	const XMFLOAT2  & rvPos, 
	const XMFLOAT2  & rvFullSize, 
	const unsigned int nSliceX, 
	const unsigned int nSliceY)
	: AFactory_Mesh_Plane(rsName,rvPos, rvFullSize, nSliceX, nSliceY)
{
}

ID3D11Buffer * AFactory_Mesh_Plane_VertexIndex::createVertexBuffer()
{
	std::vector<Vertex_Plane> vertices;
	unsigned int nVertexSizeX = m_nSliceX + 1;
	unsigned int nVertexSizeY = m_nSliceY + 1;
	XMFLOAT2 vBlockSize(m_vFullSize.x / m_nSliceX, m_vFullSize.y / m_nSliceY);
	XMFLOAT2 vStartPos(m_vPos.x -m_vFullSize.x / 2, m_vPos.y - m_vFullSize.y / 2);
	vertices.resize(nVertexSizeX * nVertexSizeY);
	for (unsigned int i = 0; i < nVertexSizeY; ++i)	
	{
		float fTexCoordY = static_cast<float>((m_nSliceY-i)) / m_nSliceY;
		float fPosY = ((i)*vBlockSize.y) + vStartPos.y;
		for (unsigned int j = 0; j < nVertexSizeX; ++j)
		{
			vertices[i*nVertexSizeX + j].m_vTexCoord.x = static_cast<float>(j) / m_nSliceX;
			vertices[i*nVertexSizeX + j].m_vTexCoord.y = fTexCoordY;
			vertices[i*nVertexSizeX + j].m_vPos2D.x = (j*vBlockSize.x) + vStartPos.x;
			vertices[i*nVertexSizeX + j].m_vPos2D.y = fPosY;
		}
	}

	ID3D11Buffer *pVertexBuffer = 
		CFactory_VertexBuffer<Vertex_Plane>(s_sPrefix_VB+m_sName, 
											static_cast<UINT>(vertices.size()))
										.createBuffer(vertices.data());
	return pVertexBuffer;
}

ID3D11Buffer * AFactory_Mesh_Plane_VertexIndex::createIndexBuffer()
{
	std::vector<UINT> indices;
	unsigned int nVertexSizeX = m_nSliceX + 1;
	indices.reserve((nVertexSizeX*2) * m_nSliceY);

	for (unsigned int i = 0; i < m_nSliceY; ++i)
	{
		if (i % 2 == 0)
		{
			for (unsigned int j = 0; j < nVertexSizeX; ++j)
			{
				indices.push_back(i*nVertexSizeX + j);
				indices.push_back((i+1)*nVertexSizeX + j);
			}
		}
		else
		{
			for (unsigned int j = m_nSliceX; j < nVertexSizeX; --j)	//언더플로를 이용한 루프
			{
				indices.push_back(i*nVertexSizeX + j);
				indices.push_back((i + 1)*nVertexSizeX + j);
			}
		}
	}

	ID3D11Buffer *pIndexBuffer = 
		CFactory_IndexBuffer(s_sPrefix_IB+m_sName, 
							static_cast<UINT>(indices.size()))
						.createBuffer(indices.data());
	return pIndexBuffer;
}

unsigned int AFactory_Mesh_Plane_VertexIndex::getNumberOfIndex()
{
	return m_nSliceY * (m_nSliceY +1)*2;
}
