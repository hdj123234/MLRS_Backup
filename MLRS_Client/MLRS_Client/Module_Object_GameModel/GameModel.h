#pragma once

#ifndef HEADER_GAMEMODEL
#define HEADER_GAMEMODEL

#include"Interface_GameModel.h"

#ifndef HEADER_STL
#include<map>
#endif // !HEADER_STL



class IFactory_GameModel  {
public:
	virtual ~IFactory_GameModel()=0 {}

	//override IFactory_GameModel
	virtual std::vector<IMesh			*> createMeshs() = 0;
	virtual std::vector<IMaterial		*> createMaterials() = 0;
	virtual IAnimData	* createAnimData() = 0;
};

class  IFactory_GameModel_BoundingBox :virtual public IFactory_GameModel {
public:
	virtual ~IFactory_GameModel_BoundingBox() = 0 {}

	virtual const DirectX::XMFLOAT3 getCenter()=0;
	virtual const DirectX::XMFLOAT3 getExtent()=0;	
};

class CGameModel : virtual public IGameModel,
					virtual public RIGameModel_MeshGetter{
private:
	typedef unsigned int MaterialID;
	typedef unsigned int MeshID;

protected:
	std::vector<IMesh *> m_pMeshs;
	std::vector<IMaterial *> m_pMaterials;
	IAnimData * m_pAnimData;

	std::map <MaterialID, std::vector<MeshID>> m_MaterialMap;
	std::vector<RIMesh_OnDX *> m_pMeshs_DXSetter;
	std::vector<RIMaterial_SetterOnDX *> m_pMaterials_DXSetter;
	RIAnimData_SetterOnDX * m_pAnimData_DXSetter;

	virtual void drawReady(CRendererState_D3D *pRendererState)const{}
public:
	CGameModel();
	virtual ~CGameModel() { CGameModel::releaseObject(); }

	bool createObject(IFactory_GameModel *pFactory);
	void releaseObject();

	//override IGameObejct
	virtual const FLAG_8 getTypeFlag() const { return 0; } 

	//override IObejct_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const ;

	//override IGameModel
	virtual const bool isClear()const { return false; }

	//override RIGameModel_MeshGetter
	virtual std::vector<IMesh *> getMeshs() { return m_pMeshs; }

	IAnimData * getAnimData() { return m_pAnimData; }
};

class CGameModel_BoundingBox : public CGameModel,
					virtual public RIGameModel_BoundingBoxGetter {
private:
	DirectX::XMFLOAT3 m_vCenter;
	DirectX::XMFLOAT3 m_vExtent;
public:
	CGameModel_BoundingBox() {}
	virtual ~CGameModel_BoundingBox() {  }

	//override RIGameModel_BoundingBoxGetter
	virtual DirectX::XMFLOAT3 getCenter() {return m_vCenter;}
	virtual DirectX::XMFLOAT3 getExtent() {return m_vExtent;}

	bool createObject(IFactory_GameModel_BoundingBox *pFactory);

};


#endif