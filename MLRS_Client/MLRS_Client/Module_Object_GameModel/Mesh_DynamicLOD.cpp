#include "stdafx.h"
#include "Mesh_DynamicLOD.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_Renderer\RendererState_D3D.h"

#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif

#ifndef HEADER_DIRECTX
#include<d3d11.h>
#endif // !HEADER_DIRECTX


//----------------------------------- CMesh_Tess -------------------------------------------------------------------------------------

void CMesh_Tess::setOnDX_Shader(CRendererState_D3D * pRendererState) const
{
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_HS(m_pHS);
	pRendererState->setShader_DS(m_pDS);
	pRendererState->setShader_GS(nullptr);
}

CMesh_Tess::CMesh_Tess()
	:CMesh_VertexIndex(),
	m_pHS(nullptr),
	m_pDS(nullptr)
{
}

bool CMesh_Tess::createObject(IFactory_Mesh_Tess * pFactory)
{
	if (!pFactory)	return false;
	m_pHS = pFactory->createHullShader();
	m_pDS = pFactory->createDomainShader();

	if (!(m_pHS&&m_pDS))	return false;
	return CMesh_VertexIndex::createObject(pFactory);
}
//----------------------------------- CMesh_DynamicLOD_DisplacementMapping -------------------------------------------------------------------------------------

CMesh_DynamicLOD_DisplacementMapping::CMesh_DynamicLOD_DisplacementMapping()
	:m_pDisplacementMap(nullptr)
{
}

void CMesh_DynamicLOD_DisplacementMapping::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	//VS
	pRendererState->setSamplerStates_VS(m_pSamplerState);
	pRendererState->setShaderResourceViews_VS(m_pDisplacementMap);

	//HS
	pRendererState->setConstBuffer_HS(m_pCameraBuffer);

	//DS
	pRendererState->setSamplerStates_DS(m_pSamplerState);
	pRendererState->setShaderResourceViews_DS(m_pDisplacementMap);
}

bool CMesh_DynamicLOD_DisplacementMapping::createObject(IFactory_Mesh_DynamicLOD_DisplacementMapping * pFactory)
{
	m_pDisplacementMap = pFactory->createDisplacementMap();
	m_pSamplerState = pFactory->createSamplerState();
	m_pCameraBuffer = pFactory->createCameraBuffer();

	if (!(m_pDisplacementMap&&m_pSamplerState&&m_pCameraBuffer))	return false;
	
	return CMesh_Tess::createObject(pFactory);
}
