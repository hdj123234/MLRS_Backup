#pragma once

#ifndef HEADER_ANIMDATA
#define HEADER_ANIMDATA

#include"Interface_GameModel.h"

//���漱��
struct ID3D11Buffer;
class CAction;

//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------
class IFactory_AnimData {
public:
	virtual ~IFactory_AnimData() = 0 {}

	virtual ID3D11ShaderResourceView * createTBuffer_BoneStructure()=0;
	virtual ID3D11ShaderResourceView *createTexture_FrameData()=0;
	virtual ID3D11ShaderResourceView * createTBuffer_AnimIndicator() = 0;
	virtual std::vector<CAction> createActions() = 0;
	virtual const unsigned int getNumberOfBone() = 0;

};

class IFactory_AnimData_NoInstancing  : public IFactory_AnimData {
public:
	virtual ~IFactory_AnimData_NoInstancing() = 0 {}

	virtual ID3D11Buffer * createUpdater_Indicator() = 0;

};


//----------------------------------- Class -------------------------------------------------------------------------------------

class AAnimData : virtual public IAnimData,
	virtual public RIAnimData_SetterOnDX {
protected:
	ID3D11ShaderResourceView *m_pTBuffer_BoneStructure;
	ID3D11ShaderResourceView *m_pTexture_FrameData;
	ID3D11ShaderResourceView *m_pTBuffer_AnimIndicator;

	std::vector<CAction> m_Actions;
	unsigned int m_nNumberOfBone;
public:
	AAnimData();
	virtual ~AAnimData() {}

	//override IObject_SetOnDX
	virtual void setOnDX(CRendererState_D3D *pRendererState)const;

	bool createObject(IFactory_AnimData *pFactory);

	ID3D11ShaderResourceView *getBoneStructure() { return m_pTBuffer_BoneStructure; }
	ID3D11ShaderResourceView *getFrameData() { return m_pTexture_FrameData; }

	const std::vector<CAction> & getActions()const { return m_Actions; }
	const unsigned int getNumberOfBone() const { return m_nNumberOfBone; }
};

class CAnimData_NoInstancing : public AAnimData {
protected:
	ID3D11Buffer *m_pBuffer_Updater_Indicator;

public:
	CAnimData_NoInstancing();
	virtual ~CAnimData_NoInstancing() {}

	bool createObject(IFactory_AnimData_NoInstancing *pFactory);
	ID3D11Buffer * getUpdater_AnimIndicator() { return m_pBuffer_Updater_Indicator; }
};


#endif