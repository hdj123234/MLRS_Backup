#include"stdafx.h"
#include "Factory_Mesh_GS.h"

#include"..\Module_DXComponent\Factory_DXShader.h"

CFactory_Mesh_GS::CFactory_Mesh_GS(const std::string & rsFileName, const std::string & rsFuncName)
	:m_sFileName(rsFileName),
	m_sFuncName(rsFuncName)
{
}

ID3D11VertexShader * CFactory_Mesh_GS::createVertexShader()
{
	return  CFactory_VertexShader(std::string("vsDummy"), std::string("vsDummy")).createShader();	
}

ID3D11GeometryShader * CFactory_Mesh_GS::createGeometryShader()
{
	return  CFactory_GeometryShader(m_sFileName, m_sFuncName).createShader();
}
