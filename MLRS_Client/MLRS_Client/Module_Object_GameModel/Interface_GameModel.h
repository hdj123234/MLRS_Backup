#pragma once
#ifndef HEADER_INTERFACE_GAMEMODEL
#define HEADER_INTERFACE_GAMEMODEL

#include"..\Interface_Global.h"
#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"
#include"..\Module_Object\Interface_Object.h"

#ifndef HEADER_STL
#include<vector>
#endif


class IMesh {
public:
	virtual ~IMesh() = 0 {}
	
	virtual const unsigned int getMaterialID() = 0;
};

class RIMesh_OnDX  : virtual public IObject_SetOnDX,
					virtual public IObject_DrawOnDX {
public:
	virtual ~RIMesh_OnDX() = 0 {}

protected:
	//virtual Function
	virtual void setOnDX_Shader(CRendererState_D3D *pRendererState)const = 0;
	virtual void setOnDX_InputAssembler(CRendererState_D3D *pRendererState)const =0;
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const =0;
};

class RIMesh_VertexBufferGetter  {
public:
	virtual ~RIMesh_VertexBufferGetter() = 0 {}

	virtual ID3D11Buffer * getVertexBuffer() = 0;
	virtual const UINT getStride() = 0;
};


class IMaterial {
public:
	virtual ~IMaterial() = 0 {}
};

class RIMaterial_SetterOnDX : virtual public IObject_SetOnDX {
public:
	virtual ~RIMaterial_SetterOnDX() = 0 {}
};

class RIMaterial_Mapping {
public:
	virtual ~RIMaterial_Mapping() = 0 {}
};

class IAnimData {
public:
	virtual ~IAnimData() = 0 {}
};

class RIAnimData_SetterOnDX : virtual public IObject_SetOnDX {
public:
	virtual ~RIAnimData_SetterOnDX() = 0 {}
};

class IGameModel : virtual public IGameObject,
					virtual public RIGameObject_Drawable{
public:
	virtual ~IGameModel() = 0 {}
};


class RIGameModel_BoundingBoxGetter {
public:
	virtual ~RIGameModel_BoundingBoxGetter() = 0 {}

	virtual DirectX::XMFLOAT3 getCenter() = 0;
	virtual DirectX::XMFLOAT3 getExtent() = 0;
};

class RIGameModel_MeshGetter {
public:
	virtual ~RIGameModel_MeshGetter() = 0 {}

	virtual std::vector<IMesh *> getMeshs()= 0;
};

#endif