#pragma once
#pragma once
#ifndef HEADER_FACTORY_ANIMDATA
#define HEADER_FACTORY_ANIMDATA

#include"AnimData.h"

//���漱��
struct Type_Bone;
namespace DirectX {
	struct XMFLOAT4X4;
}

enum EActionType : ENUMTYPE_256;


class AFactory_AnimData :virtual public IFactory_AnimData {
public:
	typedef std::map<unsigned int, EActionType> StartPointList;

protected:
	StartPointList m_StartPointList;
	const float m_fFPS;
	std::string m_sName;
	std::vector<std::vector<DirectX::XMFLOAT4X4>> m_mFrameData;

protected:
	std::vector<Type_Bone> m_nBoneStructure;

public:
	AFactory_AnimData(const std::string &rsName,
		const std::vector<Type_Bone> &rnBoneStructure,
		const std::vector<std::vector<DirectX::XMFLOAT4X4>> &rmFrameData,
		const StartPointList &rList,
		const float fFPS  );
	virtual ~AFactory_AnimData() {}

public:
	//override IFactory_AnimData
	virtual ID3D11ShaderResourceView * createTBuffer_BoneStructure();
	virtual ID3D11ShaderResourceView *createTexture_FrameData();
	virtual std::vector<CAction> createActions();
	virtual const unsigned int getNumberOfBone() { return static_cast<unsigned int>(m_nBoneStructure.size()); }
};

class CFactory_AnimData_NoInstancing : public AFactory_AnimData,
	virtual public IFactory_AnimData_NoInstancing {
private:
	ID3D11Buffer *m_pBuffer;
public:
	CFactory_AnimData_NoInstancing(	const std::string &rsName,
						const std::vector<Type_Bone> &rnBoneStructure,
						const std::vector<std::vector<DirectX::XMFLOAT4X4>> &rmFrameData,
						const AFactory_AnimData::StartPointList &rList,
						const float fFPS = 60.0f);
	virtual ~CFactory_AnimData_NoInstancing() {}
private:
	bool createBuffer_RealProcessing();

public:
	//override IFactory_AnimData_NoInstancing
	virtual ID3D11ShaderResourceView * createTBuffer_AnimIndicator() ;
	virtual ID3D11Buffer * createUpdater_Indicator() ;

	//override IFactory_AnimData
	virtual ID3D11ShaderResourceView * createTBuffer_BoneStructure() { return AFactory_AnimData::createTBuffer_BoneStructure(); }
	virtual ID3D11ShaderResourceView *createTexture_FrameData() { return AFactory_AnimData::createTexture_FrameData(); }
	virtual std::vector<CAction> createActions() { return AFactory_AnimData::createActions(); }
	virtual const unsigned int getNumberOfBone() { return AFactory_AnimData::getNumberOfBone(); }


};
#endif