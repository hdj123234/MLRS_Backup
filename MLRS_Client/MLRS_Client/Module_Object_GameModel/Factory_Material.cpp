#include"stdafx.h"
#include "Factory_Material.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"

//----------------------------------- CFactory_Material_None -------------------------------------------------------------------------------------

CFactory_Material_None::CFactory_Material_None(
	const	std::string  &rsName,
	const std::string & rsPSName)
	:m_sName(rsName),
	m_sPSName(rsPSName)
{
}

ID3D11PixelShader * CFactory_Material_None::createPixelShader()
{
	return CFactory_PixelShader(m_sPSName, m_sPSName).createShader();
}

//----------------------------------- CFactory_Material_NoneLighting_ColorOnly -------------------------------------------------------------------------------------

CFactory_Material_NoneLighting_ColorOnly::CFactory_Material_NoneLighting_ColorOnly(
	const std::string & rsName,
	const std::string & rsPSName,
	const DirectX::XMFLOAT4 & rvColor)
	:CFactory_Material_None(rsName, rsPSName),
	m_vColor(rvColor)
{
}

ID3D11Buffer * CFactory_Material_NoneLighting_ColorOnly::createCB_Color()
{
	return CFactory_ConstBuffer_NonRenewal<DirectX::XMFLOAT4>("CB_"+m_sName).createBuffer(m_vColor);
}

//----------------------------------- CFactory_Material_NoneLighting_Diffuse -------------------------------------------------------------------------------------

CFactory_Material_NoneLighting_Diffuse::CFactory_Material_NoneLighting_Diffuse(const	std::string &rsName, const std::string & rsPSName, const std::string & rsFileName_DiffuseMap)
	:CFactory_Material_None(rsName,rsPSName),
	m_sFileName_DiffuseMap(rsFileName_DiffuseMap)
{
}

ID3D11ShaderResourceView * CFactory_Material_NoneLighting_Diffuse::createDiffuseMap()
{
	return CFactory_ShaderResourceView_FromTextureFile("SRV_"+m_sName, m_sFileName_DiffuseMap).createShaderResourceView();
}

ID3D11SamplerState * CFactory_Material_NoneLighting_Diffuse::createSamplerState()
{
	return factory_SamplerState_Default.createSamplerState();
}


//----------------------------------- CFactory_Material_NoneLighting_DiffuseArray -------------------------------------------------------------------------------------

CFactory_Material_NoneLighting_DiffuseArray::CFactory_Material_NoneLighting_DiffuseArray(
	const std::string & rsName, const std::string & rsPSName, 
	const std::vector<std::string>& rsFileNames_DiffuseMap)
	:CFactory_Material_None(rsName, rsPSName),
	m_sFileNames_DiffuseMap(rsFileNames_DiffuseMap)
{
}

ID3D11ShaderResourceView * CFactory_Material_NoneLighting_DiffuseArray::createDiffuseMap()
{
	return CFactory_ShaderResourceView_FromTextureFiles(m_sName + "_DiffuseMaps", m_sFileNames_DiffuseMap).createShaderResourceView();
}

ID3D11SamplerState * CFactory_Material_NoneLighting_DiffuseArray::createSamplerState()
{
	return factory_SamplerState_Default.createSamplerState();
}

//----------------------------------- CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending -------------------------------------------------------------------------------------

ID3D11ShaderResourceView * CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending::createSRV_DepthBuffer()
{
	//üũ
	return CFactory_ShaderResourceView_FromTexture2D(
		"SRV_DSB_Default", 
		CFactory_DepthStencilView_Default().createDepthStencilBuffer(),
		DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT)
		.createShaderResourceView();
}

ID3D11DepthStencilState * CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending::createDepthStencilState()
{
	return factory_DepthStencilState_CheckOnly.createDepthStencilState();
}

ID3D11DepthStencilView * CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending::createDepthStencilView()
{
	return CFactory_DepthStencilView_ReadOnly().createDepthStencilView();
}

ID3D11SamplerState * CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending::createSamplerState_Point()
{
	return factory_SamplerState_Point.createSamplerState();
}

ID3D11Buffer * CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending::createCB_ProjectionInverse()
{
	return factory_CB_Matrix_ProjectionInverse.createBuffer();
}

//----------------------------------- CFactory_Material_Lambert -------------------------------------------------------------------------------------

CFactory_Material_Lambert::CFactory_Material_Lambert(
	const std::string &rsName,
	const std::string & rsPSName,
	const DirectX::XMFLOAT4 & rvAmbient,
	const DirectX::XMFLOAT4 & rvDiffuse,
	const DirectX::XMFLOAT4 & rvSpecular,
	const bool bDeferredLighting)
	:AFactory_Material_Lambert_Light(rsName,rsPSName, bDeferredLighting),
	m_vAmbient(rvAmbient),
	m_vDiffuse(rvDiffuse),
	m_vSpecular(rvSpecular)
{
}

ID3D11Buffer * CFactory_Material_Lambert::createMaterialBuffer()
{
	ID3D11Buffer * pBuffer = factory_CB_Material.createBuffer();
	return pBuffer;
}


Type_Material CFactory_Material_Lambert::getMaterials()
{
	Type_Material result;
	result.m_vAmbient = m_vAmbient;
	result.m_vDiffuse = m_vDiffuse;
	result.m_vSpecular = m_vSpecular;
	return result;
}

//----------------------------------- AFactory_Material_Lambert_Light -------------------------------------------------------------------------------------

AFactory_Material_Lambert_Light::AFactory_Material_Lambert_Light(
	const std::string & rsName,
	const std::string & rsPSName,
	const bool bDeferredLighting)
	:CFactory_Material_None(rsName,rsPSName),
	m_bDeferredLighting(bDeferredLighting)
{
}

std::vector<ID3D11Buffer*> AFactory_Material_Lambert_Light::createConstBuffer_Lighting()
{
	ID3D11Buffer* pGlobalLighting = factory_CB_GlobalLighting.createBuffer();
	ID3D11Buffer* pCameraData = factory_CB_CameraState.createBuffer();
	ID3D11Buffer* pMaterial = factory_CB_Material.createBuffer();

	if(!(pGlobalLighting&&pCameraData&&pMaterial))	return std::vector<ID3D11Buffer*>();

	if(m_bDeferredLighting)
		return std::vector<ID3D11Buffer*>{pMaterial};
	else
		return std::vector<ID3D11Buffer*>{pMaterial,pGlobalLighting, pCameraData};
}


//----------------------------------- CFactory_Material_Lambert_DiffuseNormal -------------------------------------------------------------------------------------

CFactory_Material_Lambert_DiffuseNormal::CFactory_Material_Lambert_DiffuseNormal(
	const std::string &rsName,
	const std::string & rsPSName,
	const std::string & rsFileName_DiffuseMap,
	const std::string & rsFileName_NormalMap,
	const DirectX::XMFLOAT4 & rvAmbient,
	const DirectX::XMFLOAT4 & rvDiffuse,
	const DirectX::XMFLOAT4 & rvSpecular,
	const bool bDeferredLighting)
	:CFactory_Material_Lambert(rsName,rsPSName, rvAmbient, rvDiffuse, rvSpecular, bDeferredLighting),
	m_sFileName_Diffuse(rsFileName_DiffuseMap),
	m_sFileName_Normal(rsFileName_NormalMap)
{
}

std::vector<ID3D11ShaderResourceView*> CFactory_Material_Lambert_DiffuseNormal::createTextureMaps()
{
	ID3D11ShaderResourceView* pDiffuseMap = CFactory_ShaderResourceView_FromTextureFile(m_sName+"_Diffuse",m_sFileName_Diffuse).createShaderResourceView();
	ID3D11ShaderResourceView* pNormalMap = CFactory_ShaderResourceView_FromTextureFile(m_sName + "_Normal",m_sFileName_Normal).createShaderResourceView();
	
	if (!(pDiffuseMap&&pNormalMap))	return std::vector<ID3D11ShaderResourceView*>();

	return std::vector<ID3D11ShaderResourceView*>{pDiffuseMap, pNormalMap};
}

ID3D11SamplerState * CFactory_Material_Lambert_DiffuseNormal::createSamplerState()
{
	return factory_SamplerState_Default.createSamplerState();
}


//----------------------------------- CFactory_MaterialSet_Lambert_DiffuseNormal -------------------------------------------------------------------------------------

CFactory_MaterialSet_Lambert_DiffuseNormal::CFactory_MaterialSet_Lambert_DiffuseNormal(
	const std::string & rsName,
	const std::string & rsPSName, 
	const std::vector<Type_Material>& rMaterials,
	const std::vector<std::string>& rsFileNames_DiffuseMap,
	const std::vector<std::string>& rsFileNames_NormalMap,
	const bool bDeferredLighting)
	:AFactory_Material_Lambert_Light(rsName,rsPSName, bDeferredLighting),
	m_sName(rsName),
	m_Materials(rMaterials),
	m_sFileNames_Diffuse(rsFileNames_DiffuseMap),
	m_sFileNames_Normal(rsFileNames_NormalMap)
{
}

ID3D11ShaderResourceView * CFactory_MaterialSet_Lambert_DiffuseNormal::createMaterials()
{
	return CFactory_SRV_BufferEX_FromData<Type_Material>(m_sName + "_Materials", m_Materials).createShaderResourceView();
}

ID3D11ShaderResourceView * CFactory_MaterialSet_Lambert_DiffuseNormal::createDiffuseMaps()
{
	return CFactory_ShaderResourceView_FromTextureFiles(m_sName + "_DiffuseMaps", m_sFileNames_Diffuse).createShaderResourceView();
}

ID3D11ShaderResourceView * CFactory_MaterialSet_Lambert_DiffuseNormal::createNormalMaps()
{
	return CFactory_ShaderResourceView_FromTextureFiles(m_sName + "_NormalMaps", m_sFileNames_Normal).createShaderResourceView();
}

std::vector<ID3D11Buffer*> CFactory_MaterialSet_Lambert_DiffuseNormal::createConstBuffer_Lighting()
{
	ID3D11Buffer* pGlobalLighting = factory_CB_GlobalLighting.createBuffer();
	ID3D11Buffer* pCameraData = factory_CB_CameraState.createBuffer();
	if(!(pGlobalLighting&&pCameraData))
		return std::vector<ID3D11Buffer*>();
	return std::vector<ID3D11Buffer*>{pGlobalLighting, pCameraData};
}

std::vector<ID3D11ShaderResourceView*> CFactory_MaterialSet_Lambert_DiffuseNormal::createSRVs()
{
	ID3D11ShaderResourceView *pSRV_Material = CFactory_MaterialSet_Lambert_DiffuseNormal::createMaterials();
	ID3D11ShaderResourceView *pSRV_DiffuseMaps = CFactory_MaterialSet_Lambert_DiffuseNormal::createDiffuseMaps();
	ID3D11ShaderResourceView *pSRV_NormalMaps = CFactory_MaterialSet_Lambert_DiffuseNormal::createNormalMaps();
	if (!(pSRV_Material&&pSRV_DiffuseMaps&&pSRV_NormalMaps))	return std::vector<ID3D11ShaderResourceView*>();
	return std::vector<ID3D11ShaderResourceView*>{pSRV_Material,pSRV_DiffuseMaps,pSRV_NormalMaps};
}


//----------------------------------- CFactory_Material_Lambert_Splatting -------------------------------------------------------------------------------------

CFactory_Material_Lambert_Splatting::CFactory_Material_Lambert_Splatting(
	const std::string & rsName,
	const std::string & rsPSName,
	const std::string & rsFileName_DiffuseMap,
	const std::string & rsFileName_NormalMap,
	const std::vector<std::string>& rsFileNames_DetailMaps,
	const std::vector<std::string>& rsFileNames_AlphaMaps,
	const DirectX::XMFLOAT2 &rvBlockSizeOfDetail,
	const DirectX::XMFLOAT4 & rvAmbient,
	const DirectX::XMFLOAT4 & rvDiffuse,
	const DirectX::XMFLOAT4 & rvSpecular,
	const bool bDeferredLighting)
	:CFactory_Material_Lambert(rsName, rsPSName, rvAmbient, rvDiffuse, rvSpecular, bDeferredLighting),
	m_sFileName_Diffuse(rsFileName_DiffuseMap),
	m_sFileName_Normal(rsFileName_NormalMap),
	m_sFileNames_DetailMaps(rsFileNames_DetailMaps),
	m_sFileNames_AlphaMaps(rsFileNames_AlphaMaps),
	m_vBlockSizeOfDetail(rvBlockSizeOfDetail)
{
}

std::vector<ID3D11ShaderResourceView*> CFactory_Material_Lambert_Splatting::createTextureMaps()
{
	//체크
	ID3D11ShaderResourceView* pDiffuseMap = CFactory_ShaderResourceView_FromTextureFile(m_sName + "_Diffuse", m_sFileName_Diffuse).createShaderResourceView();
	ID3D11ShaderResourceView* pNormalMap = CFactory_ShaderResourceView_FromTextureFile(m_sName + "_Normal", m_sFileName_Normal).createShaderResourceView();
	ID3D11ShaderResourceView* pDetailMaps = CFactory_ShaderResourceView_FromTextureFiles(m_sName + "_DetailMaps", m_sFileNames_DetailMaps).createShaderResourceView();
	ID3D11ShaderResourceView* pAlphaMaps = CFactory_ShaderResourceView_FromTextureFiles(m_sName + "_AlphaMaps", m_sFileNames_AlphaMaps).createShaderResourceView();

	if (!(pDiffuseMap&&pNormalMap))	return std::vector<ID3D11ShaderResourceView*>();

	return std::vector<ID3D11ShaderResourceView*>{pDiffuseMap, pNormalMap, pDetailMaps, pAlphaMaps};
}

ID3D11SamplerState * CFactory_Material_Lambert_Splatting::createSamplerState()
{
	return factory_SamplerState_Default.createSamplerState();
}

ID3D11Buffer * CFactory_Material_Lambert_Splatting::createCB_NumberOfSplatting()
{
	return CFactory_ConstBuffer_NonRenewal<unsigned int>("CB_" + m_sName + "_NumberOfSplatting")
		.createBuffer(static_cast<const unsigned int>(m_sFileNames_DetailMaps.size()));
}

ID3D11Buffer * CFactory_Material_Lambert_Splatting::createCB_BlockSizeOfDetail()
{
	return CFactory_ConstBuffer_NonRenewal<DirectX::XMFLOAT2>("CB_" + m_sName + "_BlockSizeOfDetail")
		.createBuffer(m_vBlockSizeOfDetail);
}
