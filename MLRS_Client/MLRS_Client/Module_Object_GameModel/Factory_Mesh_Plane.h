#pragma once
#ifndef HEADER_FACTORY_MESH_PLANE
#define HEADER_FACTORY_MESH_PLANE

#include"Mesh.h"

#include<DirectXMath.h>
#include<D3d11.h>

using DirectX::XMFLOAT2;

class AFactory_Mesh_Plane  :public IFactory_Mesh_Vertex {
protected:
	struct Vertex_Plane {
		XMFLOAT2 m_vPos2D;
		XMFLOAT2 m_vTexCoord;
	};

	const std::string m_sName;
	const XMFLOAT2 m_vPos;
	const XMFLOAT2 m_vFullSize;
	const unsigned int m_nSliceX;
	const unsigned int m_nSliceY;

	virtual std::vector<D3D11_INPUT_ELEMENT_DESC> getInputElementDescs() ;
public:
	AFactory_Mesh_Plane(const std::string &rsName,
						const XMFLOAT2 &rvPos,
						const XMFLOAT2 &rvFullSize,
						const unsigned int nSlice);
	AFactory_Mesh_Plane(const std::string &rsName,
						const XMFLOAT2 &rvPos,
						const XMFLOAT2 &rvFullSize,
						const unsigned int nSliceX,
						const unsigned int nSliceY);
	virtual ~AFactory_Mesh_Plane() {}

	virtual unsigned int			getStride() { return sizeof(Vertex_Plane); }
	virtual unsigned int			getOffset() { return 0; }


	virtual const unsigned int getMaterialID() { return 0; }
};


class AFactory_Mesh_Plane_VertexIndex :public AFactory_Mesh_Plane {
protected:
	static const std::string s_sPrefix_VB;
	static const std::string s_sPrefix_IB;
public:
	AFactory_Mesh_Plane_VertexIndex(const std::string &rsName,
									const XMFLOAT2 &rvPos,
									const XMFLOAT2 &rvFullSize,
									const unsigned int nSlice);
	AFactory_Mesh_Plane_VertexIndex(const std::string &rsName,
									const XMFLOAT2 &rvPos,
									const XMFLOAT2 &rvFullSize,
									const unsigned int nSliceX,
									const unsigned int nSliceY);
	virtual ~AFactory_Mesh_Plane_VertexIndex() {}

	virtual ID3D11Buffer		*	createVertexBuffer() ;
	virtual ID3D11Buffer		*	createIndexBuffer();
	virtual unsigned int			getNumberOfIndex();
};


#endif