#pragma once
#ifndef HEADER_FACTORY_MATERIAL
#define HEADER_FACTORY_MATERIAL

#include"Material.h"

class CFactory_Material_None :virtual public IFactory_Material{
private:
	const std::string			m_sPSName;

protected:
	const std::string			m_sName;
public:
	CFactory_Material_None(const	std::string  &rsName,const	std::string  &rsPSName);
	virtual ~CFactory_Material_None() {}

	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader() ;
};

class CFactory_Material_NoneLighting_ColorOnly : virtual public IFactory_Material_NoneLighting_ColorOnly,
												public CFactory_Material_None{
private:
	const DirectX::XMFLOAT4		m_vColor;
public:
	CFactory_Material_NoneLighting_ColorOnly(	const	std::string &rsName, 
												const std::string &rsPSName, 
												const DirectX::XMFLOAT4 &rvColor);
	virtual ~CFactory_Material_NoneLighting_ColorOnly() {}

	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader() { return CFactory_Material_None::createPixelShader(); }

	//ovdrride IFactory_Material_NoneLighting_ColorOnly
	virtual ID3D11Buffer * createCB_Color() ;
};


class CFactory_Material_NoneLighting_Diffuse : virtual public IFactory_Material_NoneLighting_Diffuse ,
												public CFactory_Material_None{
private:
	const std::string			m_sFileName_DiffuseMap;
public:
	CFactory_Material_NoneLighting_Diffuse(const	std::string &rsName, const std::string &rsPSName, const std::string &rsFileName_DiffuseMap);
	virtual ~CFactory_Material_NoneLighting_Diffuse() {}

	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader() { return CFactory_Material_None::createPixelShader(); }
	virtual ID3D11ShaderResourceView * createDiffuseMap() ;
	virtual ID3D11SamplerState * createSamplerState() ;
};

class CFactory_Material_NoneLighting_DiffuseArray : virtual public IFactory_Material_NoneLighting_Diffuse,
													public CFactory_Material_None {
private:
	const std::vector<std::string>			m_sFileNames_DiffuseMap;
public:
	CFactory_Material_NoneLighting_DiffuseArray(const	std::string &rsName, const std::string &rsPSName, const std::vector<std::string> &rsFileNames_DiffuseMap);
	virtual ~CFactory_Material_NoneLighting_DiffuseArray() {}

	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader() { return CFactory_Material_None::createPixelShader(); }
	virtual ID3D11ShaderResourceView * createDiffuseMap();
	virtual ID3D11SamplerState * createSamplerState();
};


class CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending: virtual public IFactory_Material_NoneLighting_Diffuse_DepthBiasBlending ,
																		public CFactory_Material_NoneLighting_DiffuseArray {
public:
	CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending(const	std::string &rsName, const std::string &rsPSName, const std::vector<std::string> &rsFileNames_DiffuseMap)
		:CFactory_Material_NoneLighting_DiffuseArray(rsName, rsPSName, rsFileNames_DiffuseMap) {}
	virtual ~CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending()   {}

	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader() { return CFactory_Material_NoneLighting_DiffuseArray::createPixelShader(); }
	virtual ID3D11ShaderResourceView * createDiffuseMap(){ return CFactory_Material_NoneLighting_DiffuseArray::createDiffuseMap(); }
	virtual ID3D11SamplerState * createSamplerState(){ return CFactory_Material_NoneLighting_DiffuseArray::createSamplerState(); }

	//override IFactory_Material_NoneLighting_Diffuse_DepthBiasBlending
	virtual ID3D11ShaderResourceView * createSRV_DepthBuffer() ;
	virtual ID3D11DepthStencilState* createDepthStencilState() ;
	virtual ID3D11DepthStencilView * createDepthStencilView() ;
	virtual ID3D11SamplerState * createSamplerState_Point();
	virtual ID3D11Buffer * createCB_ProjectionInverse();
};


class AFactory_Material_Lambert_Light : protected CFactory_Material_None {
private:
	const bool m_bDeferredLighting;
public:
	AFactory_Material_Lambert_Light(const std::string &rsName,
									const std::string  &rsPSName,
									const bool bDeferredLighting =false);
	virtual ~AFactory_Material_Lambert_Light()=0 {}

	virtual std::vector<ID3D11Buffer *>  createConstBuffer_Lighting();
};

class CFactory_Material_Lambert  : virtual public IFactory_Material_Lambert ,
									protected AFactory_Material_Lambert_Light {
private:
	const DirectX::XMFLOAT4	m_vAmbient ;
	const DirectX::XMFLOAT4	m_vDiffuse ;
	const DirectX::XMFLOAT4	m_vSpecular;
public:
	CFactory_Material_Lambert(const std::string &rsName, 
							const std::string  &rsPSName,
							const DirectX::XMFLOAT4 &rvAmbient,
							const DirectX::XMFLOAT4 &rvDiffuse,
							const DirectX::XMFLOAT4 &rvSpecular,
							const bool bDeferredLighting = false);
	virtual ~CFactory_Material_Lambert() {}

	//override IFactory_Material
	virtual ID3D11Buffer *createMaterialBuffer()  ;
	virtual ID3D11PixelShader *createPixelShader() {return CFactory_Material_None::createPixelShader();}
	virtual std::vector<ID3D11Buffer *>  createConstBuffer_Lighting() {return AFactory_Material_Lambert_Light::createConstBuffer_Lighting(); }

	//override IFactory_Material_Lambert
	virtual Type_Material getMaterials();
};

class CFactory_Material_Lambert_DiffuseNormal : virtual public IFactory_Material_Lambert_DiffuseNormal,
												public CFactory_Material_Lambert {
private:
	const std::string			m_sFileName_Diffuse;
	const std::string			m_sFileName_Normal;
public:
	CFactory_Material_Lambert_DiffuseNormal(const std::string &rsName, 
											const std::string &rsPSName,
											const std::string &rsFileName_DiffuseMap,
											const std::string &rsFileName_NormalMap,
											const DirectX::XMFLOAT4 &rvAmbient,
											const DirectX::XMFLOAT4 &rvDiffuse,
											const DirectX::XMFLOAT4 &rvSpecular,
											const bool bDeferredLighting = false);
	virtual ~CFactory_Material_Lambert_DiffuseNormal() {}

	//override IFactory_Material
	virtual ID3D11Buffer *createMaterialBuffer() { return CFactory_Material_Lambert::createMaterialBuffer(); }
	virtual ID3D11PixelShader *createPixelShader() { return CFactory_Material_Lambert::createPixelShader(); }
	virtual std::vector<ID3D11Buffer *>  createConstBuffer_Lighting() { return CFactory_Material_Lambert::createConstBuffer_Lighting(); }

	//override IFactory_Material_Lambert
	virtual Type_Material getMaterials() { return CFactory_Material_Lambert::getMaterials(); }

	virtual std::vector<ID3D11ShaderResourceView *> createTextureMaps() ;
	virtual ID3D11SamplerState * createSamplerState() ;
};


class CFactory_MaterialSet_Lambert_DiffuseNormal : virtual public IFactory_MaterialSet_Lambert_DiffuseNormal,
													protected AFactory_Material_Lambert_Light {
private:
	const std::string m_sName;
	const std::vector<Type_Material>		m_Materials;
	const std::vector<std::string>			m_sFileNames_Diffuse;
	const std::vector<std::string>			m_sFileNames_Normal;
public:
	CFactory_MaterialSet_Lambert_DiffuseNormal(	const std::string &rsName,
												const std::string &rsPSName,
												const std::vector<Type_Material> &rMaterials,
												const std::vector<std::string> &rsFileNames_DiffuseMap,
												const std::vector<std::string> &rsFileNames_NormalMap,
												const bool bDeferredLighting = false);
	virtual ~CFactory_MaterialSet_Lambert_DiffuseNormal() {}

private:
	Type_Material getMaterials() { return Type_Material(); }
	virtual ID3D11Buffer *createMaterialBuffer() { return nullptr; }

	ID3D11ShaderResourceView * createMaterials();
	ID3D11ShaderResourceView * createDiffuseMaps();
	ID3D11ShaderResourceView * createNormalMaps();

public:
	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader() { return CFactory_Material_None::createPixelShader(); }
	
	//override IFactory_MaterialSet_Lambert_DiffuseNormal
	virtual std::vector<ID3D11Buffer *>  createConstBuffer_Lighting();
	virtual std::vector<ID3D11ShaderResourceView *> createSRVs() ;
};

class CFactory_Material_Lambert_Splatting : virtual public IFactory_Material_Lambert_Splatting,
											public CFactory_Material_Lambert {
private:
	const std::string						m_sFileName_Diffuse;
	const std::string						m_sFileName_Normal;
	const std::vector<std::string>			m_sFileNames_DetailMaps;
	const std::vector<std::string>			m_sFileNames_AlphaMaps;
	const DirectX::XMFLOAT2					m_vBlockSizeOfDetail;
public:
	CFactory_Material_Lambert_Splatting(const std::string &rsName,
										const std::string &rsPSName,
										const std::string &rsFileName_DiffuseMap,
										const std::string &rsFileName_NormalMap,
										const std::vector<std::string>	&rsFileNames_DetailMaps,
										const std::vector<std::string>	&rsFileNames_AlphaMaps,
										const DirectX::XMFLOAT2 &rvBlockSizeOfDetail,
										const DirectX::XMFLOAT4 &rvAmbient,
										const DirectX::XMFLOAT4 &rvDiffuse,
										const DirectX::XMFLOAT4 &rvSpecular,
										const bool bDeferredLighting = false);
	virtual ~CFactory_Material_Lambert_Splatting() {}

	//override IFactory_Material
	virtual ID3D11Buffer *createMaterialBuffer() { return CFactory_Material_Lambert::createMaterialBuffer(); }
	virtual ID3D11PixelShader *createPixelShader() { return CFactory_Material_Lambert::createPixelShader(); }
	virtual std::vector<ID3D11Buffer *>  createConstBuffer_Lighting() { return CFactory_Material_Lambert::createConstBuffer_Lighting(); }

	//override IFactory_Material_Lambert
	virtual Type_Material getMaterials() { return CFactory_Material_Lambert::getMaterials(); }

	//override IFactory_Material_Lambert_Splatting
	virtual std::vector<ID3D11ShaderResourceView *> createTextureMaps() ;
	virtual ID3D11SamplerState * createSamplerState() ;
	virtual ID3D11Buffer *createCB_NumberOfSplatting() ;
	virtual ID3D11Buffer *createCB_BlockSizeOfDetail();
};
#endif