#pragma once
#ifndef HEADER_MATERIAL
#define HEADER_MATERIAL

#include "Interface_GameModel.h"
#include"..\Module_DXComponent\Type_InDirectX.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif

class CRendererState_D3D;
struct ID3D11ShaderResourceView;
struct ID3D11Buffer;
struct ID3D11PixelShader;
struct ID3D11SamplerState;

//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------
class IFactory_Material {
public:
	virtual ~IFactory_Material() = 0 {}

	virtual ID3D11PixelShader *createPixelShader() = 0;
};

class IFactory_Material_NoneLighting_ColorOnly : virtual public IFactory_Material {
public:
	virtual ~IFactory_Material_NoneLighting_ColorOnly() = 0 {}

	virtual ID3D11Buffer * createCB_Color() = 0;
};

class IFactory_Material_NoneLighting_Diffuse : virtual public IFactory_Material {
public:
	virtual ~IFactory_Material_NoneLighting_Diffuse() = 0 {}

	virtual ID3D11ShaderResourceView * createDiffuseMap() = 0;
	virtual ID3D11SamplerState * createSamplerState() = 0;
};

class IFactory_Material_NoneLighting_Diffuse_DepthBiasBlending : virtual public IFactory_Material_NoneLighting_Diffuse {
public:
	virtual ~IFactory_Material_NoneLighting_Diffuse_DepthBiasBlending() = 0 {}

	virtual ID3D11ShaderResourceView * createSRV_DepthBuffer() = 0;
	virtual ID3D11DepthStencilState* createDepthStencilState() = 0;
	virtual ID3D11DepthStencilView * createDepthStencilView() = 0;
	virtual ID3D11SamplerState * createSamplerState_Point() = 0;
	virtual ID3D11Buffer * createCB_ProjectionInverse() = 0;
};


class IFactory_Material_Lambert : virtual public IFactory_Material {
public:
	virtual ~IFactory_Material_Lambert() = 0 {}

	virtual Type_Material getMaterials() = 0;
	virtual ID3D11Buffer *createMaterialBuffer() = 0;
	virtual std::vector<ID3D11Buffer *>  createConstBuffer_Lighting() = 0;
};

class IFactory_Material_Lambert_DiffuseNormal : virtual public IFactory_Material_Lambert {
public:
	virtual ~IFactory_Material_Lambert_DiffuseNormal() = 0 {}

	virtual std::vector<ID3D11ShaderResourceView *> createTextureMaps()=0;
	virtual ID3D11SamplerState * createSamplerState() = 0;
};

class IFactory_Material_Lambert_Splatting: virtual public IFactory_Material_Lambert {
public:
	virtual ~IFactory_Material_Lambert_Splatting() = 0 {}

	virtual std::vector<ID3D11ShaderResourceView *> createTextureMaps() = 0;
	virtual ID3D11SamplerState * createSamplerState() = 0;
	virtual ID3D11Buffer *createCB_NumberOfSplatting()=0;
	virtual ID3D11Buffer *createCB_BlockSizeOfDetail()=0;
};

class IFactory_MaterialSet_Lambert_DiffuseNormal : virtual public IFactory_Material {
public:
	virtual ~IFactory_MaterialSet_Lambert_DiffuseNormal() = 0 {}

	virtual std::vector<ID3D11Buffer *>  createConstBuffer_Lighting() = 0;
	virtual std::vector<ID3D11ShaderResourceView *> createSRVs() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------
class AMaterial :	virtual public IMaterial,
					virtual public RIMaterial_SetterOnDX{
private:
	ID3D11PixelShader *m_pPS;
	 
public:
	AMaterial();
	virtual ~AMaterial() =0{}

private:
	//virtual function
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const {}
	virtual void setOnDX_UpdateBuffer(CRendererState_D3D* pRendererState)const {}

public:
	//override IObject_SetOnDX
	virtual void setOnDX(CRendererState_D3D *pRendererState)const;

	bool createObject(IFactory_Material *pFactory);
};


//----------------------------------- Class -------------------------------------------------------------------------------------

class CMaterial_None : public AMaterial {
public:
	CMaterial_None() {}
	virtual ~CMaterial_None() {}

	bool createObject(IFactory_Material *pFactory) { return AMaterial::createObject(pFactory); }
};

class CMaterial_NoneLighting_ColorOnly : public AMaterial {
private:
	ID3D11Buffer * m_pCB_Color;
public:
	CMaterial_NoneLighting_ColorOnly();
	virtual ~CMaterial_NoneLighting_ColorOnly() {}

private:
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	bool createObject(IFactory_Material_NoneLighting_ColorOnly *pFactory);
};

/*
	�ȼ� ���̴��� t0, s0 ���� ���
*/
class CMaterial_NoneLighting_Diffuse : public AMaterial ,
										virtual public RIMaterial_Mapping {
protected:
	ID3D11ShaderResourceView * m_pDiffuseMap;
	ID3D11SamplerState * m_pSamplerState;

public:
	CMaterial_NoneLighting_Diffuse():m_pDiffuseMap(nullptr), m_pSamplerState(nullptr){}
	virtual ~CMaterial_NoneLighting_Diffuse() {}

private:
	//override AMaterial
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	bool createObject(IFactory_Material_NoneLighting_Diffuse *pFactory);
};

/*
�ȼ� ���̴��� t0~1, s0~1 ���� ���
*/
class CMaterial_NoneLighting_Diffuse_DepthBiasBlending : public CMaterial_NoneLighting_Diffuse {
protected:
	ID3D11ShaderResourceView * m_pSRV_DepthBuffer;
	ID3D11DepthStencilView * m_pDSV;
	ID3D11DepthStencilState* m_pDSS;
	ID3D11SamplerState* m_pSamplerState_Point;
	ID3D11Buffer* m_pCB_ProjectionInverse;

public:
	CMaterial_NoneLighting_Diffuse_DepthBiasBlending();
	virtual ~CMaterial_NoneLighting_Diffuse_DepthBiasBlending() {}

private:
	//override AMaterial
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	bool createObject(IFactory_Material_NoneLighting_Diffuse_DepthBiasBlending *pFactory);
};

/*
�ȼ� ���̴���  b0~2 ���� ���
*/
class CMaterial_Lambert : public AMaterial {
private:
	Type_Material m_Material;
	ID3D11Buffer *m_pMaterialBuffer;
	std::vector<ID3D11Buffer *> m_pCBuffer_Lighting;

	static CMaterial_Lambert const * s_pLatestMaterial;

public:
	CMaterial_Lambert();
	virtual ~CMaterial_Lambert() {}

protected:
	//override AMaterial
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;
	virtual void setOnDX_UpdateBuffer(CRendererState_D3D* pRendererState)const;

public:
	bool createObject(IFactory_Material_Lambert *pFactory);
};

/*
�ȼ� ���̴��� s0, t0~1, b0~2 ���� ���
*/
class CMaterial_Lambert_DiffuseNormal : public CMaterial_Lambert,
										virtual public RIMaterial_Mapping {
private:
	std::vector<ID3D11ShaderResourceView *> m_pMaps_DiffuseNormal;
	ID3D11SamplerState * m_pSamplerState;

public:
	CMaterial_Lambert_DiffuseNormal();
	virtual ~CMaterial_Lambert_DiffuseNormal() {}

private:
	//override AMaterial
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	bool createObject(IFactory_Material_Lambert_DiffuseNormal *pFactory);
};

/*
�ȼ� ���̴��� s0, t0~3, b0~ ���� ���
*/
class CMaterial_Lambert_Splatting: public CMaterial_Lambert,
	virtual public RIMaterial_Mapping {
private:
	std::vector<ID3D11ShaderResourceView *> m_pMaps;	//��ǻ��, �븻, ������, ����
	ID3D11SamplerState * m_pSamplerState;
	ID3D11Buffer *m_pCB_NumberOfSplatting;
	ID3D11Buffer *m_pCB_BlockSizeOfDetail;

public:
	CMaterial_Lambert_Splatting();
	virtual ~CMaterial_Lambert_Splatting() {}

private:
	//override AMaterial
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	bool createObject(IFactory_Material_Lambert_Splatting *pFactory);
};

/*
�ȼ� ���̴��� s0, t0~2, b0~1 ���� ���
*/
class CMaterialSet_Lambert_DiffuseNormal : public AMaterial {
private:
	std::vector<ID3D11ShaderResourceView *> m_pSRVs;	//{ ���׸���, ��ǻ��, �븻 }
	std::vector<ID3D11Buffer *> m_pCBuffer_Lighting;

public:
	CMaterialSet_Lambert_DiffuseNormal(){}
	virtual ~CMaterialSet_Lambert_DiffuseNormal() {}

protected:
	//override AMaterial
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	bool createObject(IFactory_MaterialSet_Lambert_DiffuseNormal *pFactory);
};


#endif