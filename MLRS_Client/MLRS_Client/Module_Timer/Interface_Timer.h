#pragma once

#ifndef HEADER_INTERFACE_TIMER
#define HEADER_INTERFACE_TIMER

class IGameTimer {
public:
	virtual ~IGameTimer() = 0 {}

	virtual void setFrameRate(const int  frameRate) = 0;
	virtual const float getFPS()const = 0;
	virtual void getReady() = 0;
	virtual float tick() = 0;
};

#endif