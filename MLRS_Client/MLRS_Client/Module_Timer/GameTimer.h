#pragma once

#ifndef HEADER_FRAMEWORK
#define HEADER_FRAMEWORK

#define TIMER_NOT_ELAPSED_ONE_FRAME -1

#if defined(USE_TIMER_CHRONO)& !defined(HEADER_STL)
#include<chrono>
#endif

#include"Interface_Timer.h"


class AGameTimer : public IGameTimer {
protected:
	int m_nFrameRate;
	float m_fTimeUnit_OneFrame;
	float m_fTime_Elapsed;
	float m_fTime_FPSCounter;
	int m_nFPSCount;
	float m_fFPS;
	bool m_bTimeDelay;	//1프레임이 지났을 경우 지연 or 0보다 작은거나 같은 값 반환
	float m_fSleepRate;
	bool m_bReady;
public:
	AGameTimer();
	virtual ~AGameTimer() {}

	//override ITimer
	virtual void setFrameRate(const int  frameRate) { m_nFrameRate = frameRate; }
	virtual const float getFPS()const { return m_fFPS; }
	virtual void getReady();
	virtual float tick();
	virtual void setTimeDelay() { m_bReady = true; }	//1프레임이 경과하지 않았을 경우 시간을 지연시키도록 설정
	virtual void setNoTimeDelay() { m_bReady = false; }	//1프레임이 경과하지 않았을 경우 0 이하의 값을 반환하도록 설정

	//virtual function
	virtual void sleepToNextFrame();

	//pure virtual function
	virtual void updateElapsedTime() = 0;
	virtual void setFrameCount_Past() = 0;
};

#ifdef USE_TIMER_QUERYPERFORMANCECOUNTER
class CGameTimer_QueryPerformanceCounter :public AGameTimer {
private:
	__int64 m_nPerformanceFrequency;
	__int64 m_nTimeCount;
	__int64 m_nTimeCount_Past;
	float m_fTimeUnit;

public:
	CGameTimer_QueryPerformanceCounter();
	virtual ~CGameTimer_QueryPerformanceCounter() {  }

	//override IGameTimer
	virtual void getReady();

	//override AGameTimer
	virtual void updateElapsedTime();
	virtual void setFrameCount_Past() { m_nTimeCount_Past = m_nTimeCount; }

};
#endif
#ifdef USE_TIMER_WINMM
class CGameTimer_WinMM :public AGameTimer {
private:
	DWORD m_nTimeCount;
	DWORD m_nTimeCount_Past;
public:
	CGameTimer_WinMM();
	virtual ~CGameTimer_WinMM() {}

	//override IBase
	virtual bool create(Base::ICreator *pCreator = nullptr) { return true; }
	virtual void releaseObject() {};

	//override IGameTimer
	virtual void getReady();

	//override AGameTimer
	virtual void updateElapsedTime();
	virtual void setFrameCount_Past() { m_nTimeCount_Past = m_nTimeCount; }
};
#endif
#ifdef USE_TIMER_CHRONO
class CGameTimer_Chrono :public AGameTimer {
private:
	std::chrono::system_clock::time_point m_TimePoint;
	std::chrono::system_clock::time_point m_TimePoint_Past;
	std::chrono::system_clock::duration m_Duration_Frame;
	std::chrono::system_clock::duration m_Duration_Sleep;
public:
	CGameTimer_Chrono() {}
	virtual ~CGameTimer_Chrono() {};

	//override IGameTimer
	virtual void getReady();

	//override AGameTimer
	virtual void updateElapsedTime();
	virtual void setFrameCount_Past() { m_TimePoint_Past = m_TimePoint; }
	virtual void sleepToNextFrame();
};
#endif

#endif