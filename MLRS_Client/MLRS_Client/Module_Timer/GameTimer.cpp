#include"stdafx.h"
#include "GameTimer.h"

#ifndef HEADER_WINDOWS
#include <Windows.h>
#endif


#if defined(USE_TIMER_CHRONO)& !defined(HEADER_STL)
#include<thread>
#endif

//----------------------------------- AGameTimer -------------------------------------------------------------------------------------

AGameTimer::AGameTimer()
	:m_bReady(false),
	m_fFPS(0.0f),
	m_fTime_FPSCounter(0.0f),
	m_nFPSCount(0),
	m_fTime_Elapsed(0.0f),
	m_bTimeDelay(false),
	m_fSleepRate(0.95)
{
	setFrameRate(60);
}

void AGameTimer::getReady()
{
	m_fFPS = 0.0f;
	m_nFPSCount = 0;
	m_fTime_FPSCounter = 0.0f;
	m_fTimeUnit_OneFrame = 1.0f / m_nFrameRate;
	m_bReady = true;
}

float AGameTimer::tick()
{
	if (!m_bReady)
		return -1.0f;

	if (m_fTime_FPSCounter > 1.0f)
	{
		m_fFPS = static_cast<float>(m_nFPSCount) / m_fTime_FPSCounter;
		m_fTime_FPSCounter = 0.0f;
		m_nFPSCount = 0;
	}

	//각 클래스의 방식대로 경과시간 계산
	if (m_bTimeDelay)
	{
		while (1) {
			this->updateElapsedTime();
			if (m_fTime_Elapsed > m_fTimeUnit_OneFrame)
				break;
			else
				this->sleepToNextFrame();
		}
	}
	else
	{
		this->updateElapsedTime();
		if (m_fTime_Elapsed < m_fTimeUnit_OneFrame)
			return TIMER_NOT_ELAPSED_ONE_FRAME;
	}
	this->setFrameCount_Past();

	m_nFPSCount++;
	m_fTime_FPSCounter += m_fTime_Elapsed;

	return m_fTime_Elapsed;
}

void AGameTimer::sleepToNextFrame()
{
	Sleep(static_cast<DWORD>((m_fTimeUnit_OneFrame - m_fTime_Elapsed)*1000.0f*m_fSleepRate));
}

#ifdef USE_TIMER_QUERYPERFORMANCECOUNTER
//----------------------------------- CGameTimer_QueryPerformanceCounter -------------------------------------------------------------------------------------

CGameTimer_QueryPerformanceCounter::CGameTimer_QueryPerformanceCounter()
	: m_nPerformanceFrequency(0), m_fTimeUnit(0.0f), m_nTimeCount(0), m_nTimeCount_Past(0)
{
}
void CGameTimer_QueryPerformanceCounter::getReady()
{
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER *>(&m_nPerformanceFrequency));
	AGameTimer::getReady();
	m_fTimeUnit = 1.0f / (float)m_nPerformanceFrequency;
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER *>(&m_nTimeCount_Past));
}

void CGameTimer_QueryPerformanceCounter::updateElapsedTime()
{
	QueryPerformanceCounter((LARGE_INTEGER *)&m_nTimeCount);
	m_fTime_Elapsed = static_cast<float>(m_nTimeCount - m_nTimeCount_Past) * m_fTimeUnit;
}
#endif
#ifdef USE_TIMER_WINMN
//----------------------------------- CGameTimer_WinMM -------------------------------------------------------------------------------------
CGameTimer_WinMM::CGameTimer_WinMM()
	:m_nTimeCount(0), m_nTimeCount_Past(0)
{
}
void CGameTimer_WinMM::getReady()
{
	AGameTimer::getReady();
	m_nTimeCount_Past = timeGetTime();
}
void CGameTimer_WinMM::updateElapsedTime()
{
	m_nTimeCount = timeGetTime();
	m_fTime_Elapsed = static_cast<float>(m_nTimeCount - m_nTimeCount_Past) / 1000.0f;
}
#endif
#ifdef USE_TIMER_CHRONO
//----------------------------------- CGameTimer_Chrono -------------------------------------------------------------------------------------

void CGameTimer_Chrono::getReady()
{
	using namespace std::chrono;
	AGameTimer::getReady();
	m_Duration_Frame = duration_cast<system_clock::duration>(std::chrono::duration<float>(m_fTimeUnit_OneFrame));
	m_TimePoint_Past = system_clock::now();
}
void CGameTimer_Chrono::updateElapsedTime()
{
	using namespace std::chrono;
	m_TimePoint = system_clock::now();
	m_fTime_Elapsed = duration_cast<duration<float>>(m_TimePoint - m_TimePoint_Past).count();
}
void CGameTimer_Chrono::sleepToNextFrame()
{
	using namespace std::chrono;
	std::this_thread::sleep_until(m_TimePoint_Past + m_Duration_Frame);
	//	std::this_thread::sleep_for((m_Duration_Frame - (m_TimePoint - m_TimePoint_Past))*9/10);
}
#endif