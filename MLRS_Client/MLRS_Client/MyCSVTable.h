#pragma once

#include"MyUtility.h"

class CMyCSVTable{
private:
	std::vector<std::vector<std::string>> m_Table;

public:
	CMyCSVTable(const std::wstring & rwsFileName, const bool bColumnName = true) { m_Table = std::move(readFile(rwsFileName, bColumnName)); }
	CMyCSVTable(const std::string & rsFileName , const bool bColumnName = true) { m_Table = std::move(readFile(CUtility_String::convertToWString(rsFileName), bColumnName)); }
	~CMyCSVTable(){}

private:
	static std::vector<std::vector<std::string>> readFile(const std::wstring & rwsFileName, bool bColumnName);

public:
	const std::vector<std::vector<std::string>> &getTable()const { return m_Table; }
};

class CMyCSVTable_wstring {
private:
	std::vector<std::vector<std::wstring>> m_Table;

public:
	CMyCSVTable_wstring(const std::wstring & rwsFileName, const bool bColumnName = true) { m_Table = std::move(readFile(rwsFileName, bColumnName)); }
	CMyCSVTable_wstring(const std::string & rsFileName , const bool bColumnName = true) { m_Table = std::move(readFile(CUtility_String::convertToWString(rsFileName), bColumnName)); }
	~CMyCSVTable_wstring() {}

private:
	static std::vector<std::vector<std::wstring>> readFile(const std::wstring & rwsFileName, bool bColumnName);

public:
	const std::vector<std::vector<std::wstring>> &getTable() { return m_Table; }
};
