#pragma once

#ifndef HEADER_STL
#include<map>
#endif // !HEADER_STL

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH


class CMyINISection : private std::map<const std::string, std::string >{
private:
	typedef std::map<const std::string, std::string > SUPER;
public:
	CMyINISection() {}
	~CMyINISection() {}
	
	void setParam(const std::string &rsParamName, const std::string &rsParam);
	const bool isEmpty()const { return SUPER::empty(); }

	const std::string & getParam(const std::string &rsParamName)const;
	const std::wstring getParam_wstring(const std::string &rsParamName)const;
	const DirectX::XMFLOAT4 getParam_XMFLOAT4(const std::string &rsParamName)const;
	const DirectX::XMFLOAT3 getParam_XMFLOAT3(const std::string &rsParamName)const;
	const DirectX::XMFLOAT3 getParam_XMFLOAT3_Normalize(const std::string &rsParamName)const;
	const DirectX::XMFLOAT2 getParam_XMFLOAT2(const std::string &rsParamName)const;
	const DirectX::XMFLOAT2 getParam_XMFLOAT2_ToRadian(const std::string &rsParamName)const;
	const DirectX::XMFLOAT4X4 getParam_XMFLOAT4X4_FromPosAndScale(const std::string &rsParamName)const;
	const float getParam_float(const std::string &rsParamName)const;
	const float getParam_float_ToRadian(const std::string &rsParamName)const;
	const short getParam_short(const std::string &rsParamName)const;
	const unsigned int getParam_uint(const std::string &rsParamName)const;
//	const std::string & getParam(const std::string &rsParamName)const { return SUPER::at(rsParamName); }

	CMyINISection& setDefaultData(const CMyINISection & rSection);
	void clear() { SUPER::clear(); }

	static const std::string convertString(const std::string &rs);
};


class CMyINIData : private std::map<const std::string, CMyINISection> {
private:
	typedef std::map<const std::string, CMyINISection> SUPER;
public:
//	CMyINIData();
	CMyINIData(const std::string &rsFileName);
	~CMyINIData() {}

	bool loadData(const std::string &rsFileName);
	CMyINISection getSection(const std::string &rsSectionName)const;
	CMyINISection & getSection(const std::string &rsSectionName);
	void setSection_DefaultData(const std::string &rsSectionName,const CMyINISection & rParamiters);
	const bool isEmpty()const { return SUPER::empty(); }

	static CMyINIData getDatas(const std::string &rsFileName){	return CMyINIData(rsFileName);}
};

