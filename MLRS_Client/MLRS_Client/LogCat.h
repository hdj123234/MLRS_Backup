
#ifndef HEADER_LOGCAT
#define HEADER_LOGCAT

#ifdef _DEBUG
#define MYLOGCAT(A,B) CLogCat::logCat(A,B)
#define MYLOGCAT_ERROR_NORMAL CLogCat::LogType::Error_Normal
#define MYLOGCAT_ERROR_WINDOWS CLogCat::LogType::Error_Windows
#define MYLOGCAT_ERROR_FILEOPEN CLogCat::LogType::Error_FileOpen
#define MYLOGCAT_ERROR_DIRECTX CLogCat::LogType::Error_DirectX
#define MYLOGCAT_ERROR_WINSOCK CLogCat::LogType::Error_WinSock


#ifndef HEADER_STL
#include<fstream>
#include<string>
#endif

class CLogCat {
private:
	const std::string m_sFilename;
	std::ofstream m_LogFile;

	void error(char const* s) { m_LogFile << "ERROR\t" << s << std::endl; }
	void error_windows(char const* s) { m_LogFile << "ERROR_Windows\t" << s << std::endl; }
	void error_fileopen(char const* s) { m_LogFile << "ERROR_FileOpen\t" << s << std::endl; }
	void error_directx(char const* s) { m_LogFile << "ERROR_DirectX\t" << s << std::endl; }
	void error_winSock(char const* s) { m_LogFile << "ERROR_WinSock\t" << s << std::endl; }
public:
	static CLogCat *g_pLogcat;

	enum LogType { Error_Normal, Error_Windows, Error_FileOpen, Error_DirectX, Error_WinSock};
	CLogCat() :m_sFilename("LogCat.txt") { m_LogFile.open(m_sFilename); }
	~CLogCat() { m_LogFile.close(); }
	void operator() (LogType t, std::string s) { operator()(t, s.c_str()); }
	void operator() (LogType t, char const * s) {
		switch (t)
		{
		case Error_Normal:
			error(s);
			break;
		case Error_Windows:
			error_windows(s);
			break;
		case Error_FileOpen:
			error_fileopen(s);
			break;
		case Error_DirectX:
			error_directx(s);
			break;
		case Error_WinSock:
			error_winSock(s);
			break;
		}
	}
	static void logCat(LogType t, std::string s) { g_pLogcat ->operator()(t, s); }
};


#else
#define MYLOGCAT(A,B) 
#define MYLOGCAT_ERROR_NORMAL 
#define MYLOGCAT_ERROR_WINDOWS 
#define MYLOGCAT_ERROR_FILEOPEN
#define MYLOGCAT_ERROR_DIRECTX 
#define MYLOGCAT_ERROR_WINSOCK 
#endif


#endif