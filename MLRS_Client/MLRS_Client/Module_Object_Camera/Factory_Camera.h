#pragma once
#ifndef HEADER_FACTORY_CAMERA
#define HEADER_FACTORY_CAMERA

#include"Camera.h"
#include"CameraState.h"

//���漱��
struct ID3D11Buffer;

class AFactory_Camera : virtual public IFactory_Camera {
private:
	CameraState_Projection m_CameraState_Projection;
public:
	AFactory_Camera(const float fFarZ,
					const float fFOV = 45,
					const float fNearZ = 1);
	AFactory_Camera(const CameraState_Projection & rCameraState_Projection);
	virtual ~AFactory_Camera() {}

	//override IFactory_Camear
	virtual IProjection *createProjection();
	virtual ID3D11Buffer *createCB_Projection();
	virtual ID3D11Buffer *createCB_ProjectionInverse();
	virtual ID3D11Buffer *createCB_View();
	virtual ID3D11Buffer *createCB_CameraState();
};

class CFactory_Camera_Local : public AFactory_Camera {
private:
	const DirectX::XMFLOAT3 m_vPos;
	const DirectX::XMFLOAT3 m_vDir;
	const float m_fMoveUnit;
	const float m_fRotationUnit;
public:
	CFactory_Camera_Local(const float fMoveUnit,
		const float fRotationUnit,
		const DirectX::XMFLOAT3 &rvPos,
		const DirectX::XMFLOAT3 &rvDir,
		const float fFarZ,
		const float fFOV = 45,
		const float fNearZ = 1);
	virtual ~CFactory_Camera_Local(){}

	//override IFactory_Camear
	virtual IView *createView();
};


#endif