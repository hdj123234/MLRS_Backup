#pragma once
#ifndef HEADER_PROJECTION
#define HEADER_PROJECTION

#include "Interface_Camera.h"


class CProjection_PerspectiveFov : virtual public IProjection{
private:
	float m_fFovY;
	float m_fAspectRatioHByW;
	float m_fNearZ;
	float m_fFarZ;

	DirectX::XMFLOAT4X4A * m_pmReturnValue_Matrix;

	void updateReturnValue();
public:
	CProjection_PerspectiveFov(float m_fFovY, float fAspectRatioHByW, float fNearZ, float fFarZ);
	virtual ~CProjection_PerspectiveFov();

	virtual const DirectX::XMFLOAT4X4A& getMatrix()const { return *m_pmReturnValue_Matrix; }
	virtual DirectX::XMFLOAT2 getFlat() { return DirectX::XMFLOAT2(m_fNearZ, m_fFarZ); }
};

#endif