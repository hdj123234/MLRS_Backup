#include"stdafx.h"
#include "Factory_Camera_ThirdPerson.h"

#include"View.h"
#include"Projection.h"

#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"



CameraState_ThirdPerson::CameraState_ThirdPerson(
	const DirectX::XMFLOAT3 &rvRelativePos,
	const float fInitDistance,
	const float fRotationUnitX,
	const float fRotationUnitY,
	const float fDistanceMoveUnit,
	const float fFarZ,
	const float fNearZ,
	const float fFOV)
	:m_vs_Third(rvRelativePos, fInitDistance,fRotationUnitX,fRotationUnitY,fDistanceMoveUnit),
	m_ps(fFarZ,fNearZ,fFOV)
{
}



CFactory_Camera_ThirdPerson::CFactory_Camera_ThirdPerson(CameraState_ThirdPerson & rCameraState)
	:AFactory_Camera(rCameraState.m_ps),
	m_rCameraState(rCameraState)
{
}

CFactory_Camera_ThirdPerson::~CFactory_Camera_ThirdPerson()
{
}


IView * CFactory_Camera_ThirdPerson::createView()
{
	CView_ThirdPerson *pView = new CView_ThirdPerson(	m_rCameraState.m_vs_Third.m_vRelativePos,
														m_rCameraState.m_vs_Third.m_fDistanceMoveUnit, 
														m_rCameraState.m_vs_Third.m_fRotationUnitX, 
														m_rCameraState.m_vs_Third.m_fRotationUnitY,
														m_rCameraState.m_vs_Third.m_fInitDistance);
	return pView;
}

