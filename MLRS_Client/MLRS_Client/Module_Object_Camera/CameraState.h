#pragma once
#ifndef HEADER_CAMERASTATE
#define HEADER_CAMERASTATE

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

struct CameraState_Projection {
	float m_fFarZ;
	float m_fNearZ;
	float m_fFOV;

	CameraState_Projection(const float fFarZ = 50000,
		const float fNearZ = 1,
		const float fFOV = 45);
};



struct ViewState_ThirdPerson {
	DirectX::XMFLOAT3 m_vRelativePos;
	float m_fInitDistance;
	float m_fRotationUnitX;
	float m_fRotationUnitY;
	float m_fDistanceMoveUnit;
	ViewState_ThirdPerson(const DirectX::XMFLOAT3 &rvRelativePos = DirectX::XMFLOAT3(0, 0, 0),
		const float fInitDistance = 60,
		const float fRotationUnitX = 0.02f,
		const float fRotationUnitY = 0.02f,
		const float fDistanceMoveUnit = 1)
		:m_vRelativePos(rvRelativePos),
		m_fInitDistance(fInitDistance),
		m_fRotationUnitX(fRotationUnitX),
		m_fRotationUnitY(fRotationUnitY),
		m_fDistanceMoveUnit(fDistanceMoveUnit)
	{	}
};

struct ViewState_ThirdPerson_LimitRange {
	ViewState_ThirdPerson m_vs_ThirdPerson;
	DirectX::XMFLOAT2 m_vRange_Distance;
	DirectX::XMFLOAT2 m_vRange_AngleX_Radian;	//Radian

	ViewState_ThirdPerson_LimitRange(	const DirectX::XMFLOAT2 &rvRange_Distance,
										const DirectX::XMFLOAT2 &rvRange_AngleX,	//Degree
										const DirectX::XMFLOAT3 &rvRelativePos = DirectX::XMFLOAT3(0, 0, 0),
										const float fInitDistance = 60,
										const float fRotationUnitX = 0.02f,
										const float fRotationUnitY = 0.02f,
										const float fDistanceMoveUnit = 1)
		:m_vs_ThirdPerson(rvRelativePos ,fInitDistance ,fRotationUnitX ,fRotationUnitY ,fDistanceMoveUnit ),
		m_vRange_Distance(rvRange_Distance),
		m_vRange_AngleX_Radian(	DirectX::XMConvertToRadians( rvRange_AngleX.x),
								DirectX::XMConvertToRadians(rvRange_AngleX.y) )
	{}
	
	ViewState_ThirdPerson_LimitRange(	const DirectX::XMFLOAT3 &rvRelativePos = DirectX::XMFLOAT3(0, 0, 0),
										const float fInitDistance = 60,
										const float fRotationUnitX = 0.02f,
										const float fRotationUnitY = 0.02f,
										const float fDistanceMoveUnit = 1)
		:m_vs_ThirdPerson(rvRelativePos ,fInitDistance ,fRotationUnitX ,fRotationUnitY ,fDistanceMoveUnit )
	{}
};

struct ViewState_TPS{
	DirectX::XMFLOAT3 m_vRelativePos;
	float m_fDefaultAngle_X;
	float m_fRotationUnit_X;
	DirectX::XMFLOAT2 m_vAngleRange_X;

	ViewState_TPS() {}
	ViewState_TPS(	const DirectX::XMFLOAT3 &rvRelativePos,
					const float fDefaultAngle_X,
					const float fRotationUnit_X,
					const DirectX::XMFLOAT2 &rvAngleRange_X)
		:m_vRelativePos(rvRelativePos),
		m_fDefaultAngle_X(fDefaultAngle_X),
		m_fRotationUnit_X(fRotationUnit_X),
		m_vAngleRange_X(rvAngleRange_X)
	{}
};

struct ViewState_FPS_PitchYaw {
	DirectX::XMFLOAT3 m_vRelativePos; 
	DirectX::XMFLOAT2 m_vRotationUnit;
	DirectX::XMFLOAT2 m_vRange_AngleX;
	DirectX::XMFLOAT2 m_vRange_AngleY;

	ViewState_FPS_PitchYaw() {}
	ViewState_FPS_PitchYaw(const DirectX::XMFLOAT3 &rvRelativePos, 
		const DirectX::XMFLOAT2 &rvRotationUnit,
		const DirectX::XMFLOAT2 &rvRange_AngleX,
		const DirectX::XMFLOAT2 &rvRange_AngleY)
		: m_vRelativePos(rvRelativePos), 
		m_vRotationUnit(rvRotationUnit),
		m_vRange_AngleX(rvRange_AngleX),
		m_vRange_AngleY(rvRange_AngleY)
	{}
};

#endif