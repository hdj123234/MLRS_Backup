#pragma once
#ifndef HEADER_VIEW
#define HEADER_VIEW

#include "Interface_Camera.h"
#include"CameraState.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

class AView_LocalBase : virtual public IView {
protected:
	DirectX::XMFLOAT3A * const m_pvPos;
	DirectX::XMFLOAT3A * const m_pvLook;
	DirectX::XMFLOAT3A * const m_pvRight;
	DirectX::XMFLOAT3A * const m_pvUp;

	DirectX::XMFLOAT4X4A * const m_pmReturnValue_Matrix;

	void updateReturnValue();

	virtual bool move_RealProcessing(const IMsg_Movement *pMoveMsg)=0;
	virtual void updateLocalVector() = 0;
public:
	AView_LocalBase(const DirectX::XMFLOAT3 &rvPos = DirectX::XMFLOAT3(0, 0, 0),
				const DirectX::XMFLOAT3 &rvRight = DirectX::XMFLOAT3(1, 0, 0),
				const DirectX::XMFLOAT3 &rvUp = DirectX::XMFLOAT3(0, 1, 0),
				const DirectX::XMFLOAT3 &rvLook = DirectX::XMFLOAT3(0, 0, 1));
	virtual ~AView_LocalBase();

	//override IView
	virtual const DirectX::XMFLOAT4X4A& getMatrix()const{return *m_pmReturnValue_Matrix;}

	virtual const DirectX::XMFLOAT3A *  getPosition()const	{return m_pvPos;}
	virtual const DirectX::XMFLOAT3A * getLocalRight()const {return m_pvRight;}
	virtual const DirectX::XMFLOAT3A * getLocalUp()const	{return m_pvUp;}
	virtual const DirectX::XMFLOAT3A * getLocalLook()const	{return m_pvLook;}

	virtual void move(const IMsg_Movement *pMoveMsg);
};

class AView_Target : public AView_LocalBase {
protected:
	const DirectX::XMFLOAT3A *m_pvTargetPos;
	DirectX::XMFLOAT3A * const m_pvRelativePos;

public:
	AView_Target(const DirectX::XMFLOAT3 &rvRelativePos);
	virtual ~AView_Target();

public:
	virtual void setTarget(const IGameObject *pTarget);
};

class CView_Local : public AView_LocalBase {
private:
	float m_fMoveUnit;
	float m_fRotationUnit;

public:
	CView_Local(const float fMoveUnit,
		const float fRotationUnit,
		const DirectX::XMFLOAT3 &rvPos = DirectX::XMFLOAT3(0, 0, 0),
		const DirectX::XMFLOAT3 &rvRight = DirectX::XMFLOAT3(1, 0, 0),
		const DirectX::XMFLOAT3 &rvUp = DirectX::XMFLOAT3(0, 1, 0),
		const DirectX::XMFLOAT3 &rvLook = DirectX::XMFLOAT3(0, 0, 1));
	virtual ~CView_Local() {}

protected:
	//override AView_LocalBase
	virtual bool move_RealProcessing(const IMsg_Movement *pMoveMsg);
	virtual void updateLocalVector() {}

public:
	//override IView
	virtual void resetView(IGameObject *pObj);
};

class CView_ThirdPerson : public AView_Target {
protected:
	float m_fMoveUnit;
	float m_fRotationUnitX;
	float m_fRotationUnitY;

	float m_fDistance;
	float m_fAngleX;
	float m_fAngleY;

public:
	CView_ThirdPerson(const DirectX::XMFLOAT3 &rvRelativePos,
		const float fMoveUnit,
		const float fRotationUnitX,
		const float fRotationUnitY,
		const float fInitDistance = 500);
	CView_ThirdPerson( ViewState_ThirdPerson &rViewState);
	virtual ~CView_ThirdPerson(){}

protected:
	//override AView_LocalBase
	virtual bool move_RealProcessing(const IMsg_Movement *pMoveMsg);
	virtual void updateLocalVector();

public:
	//override IView
	virtual void resetView(IGameObject *pObj) ;
};

class CView_ThirdPerson_LimitRange : public CView_ThirdPerson {
private:
	const DirectX::XMFLOAT2 m_vRange_Distance;
	const DirectX::XMFLOAT2 m_vRange_AngleX;	//Radian

public:
	CView_ThirdPerson_LimitRange(	const DirectX::XMFLOAT2 &rvRange_Distance,
									const DirectX::XMFLOAT2 &rvRange_AngleX,	//Degree
									const DirectX::XMFLOAT3 &rvRelativePos,
									const float fMoveUnit,
									const float fRotationUnitX,
									const float fRotationUnitY,
									const float fInitDistance = 500);
	CView_ThirdPerson_LimitRange(ViewState_ThirdPerson_LimitRange &rViewState);
	virtual ~CView_ThirdPerson_LimitRange() {}

protected:
	//override AView_LocalBase
	virtual bool move_RealProcessing(const IMsg_Movement *pMoveMsg);

};

class CView_TPS : public AView_Target {
protected:
	const DirectX::XMFLOAT3A *m_pvTargetRight;
	const DirectX::XMFLOAT3A *m_pvTargetUp;
	const DirectX::XMFLOAT3A *m_pvTargetLook;

	const float m_fRotationUnitX;
	const DirectX::XMFLOAT2 m_vRange_AngleX;	//Radian
	const float m_fDefaultAngle_X;

	float m_fAngleX;

public:
	//CView_TPS(	const DirectX::XMFLOAT3 &rvRelativePos,
	//			const float fRotationUnitX,
	//			const DirectX::XMFLOAT2 &rvRange_AngleX);
	CView_TPS(const ViewState_TPS &rvViewState);
	virtual ~CView_TPS(){}

protected:
	//override AView_LocalBase
	virtual bool move_RealProcessing(const IMsg_Movement *pMoveMsg);
	virtual void updateLocalVector();

public:
	//override IView
	virtual void resetView(IGameObject *pObj) {}
	
	virtual void setTarget(const IGameObject *pTarget);

};

class CView_FPS_PitchYaw : public AView_Target {
protected:
	const DirectX::XMFLOAT3A *m_pvTargetRight;
	const DirectX::XMFLOAT3A *m_pvTargetUp;
	const DirectX::XMFLOAT3A *m_pvTargetLook;
	 
	DirectX::XMFLOAT2 m_vRotationAngle_PitchYaw;

	const DirectX::XMFLOAT2 m_vRotationUnit;
	const DirectX::XMFLOAT2 m_vRange_AngleX;	//Radian
	const DirectX::XMFLOAT2 m_vRange_AngleY;	//Radian

public: 
	CView_FPS_PitchYaw(const ViewState_FPS_PitchYaw &rvViewState);
	virtual ~CView_FPS_PitchYaw() {}

protected:
	//override AView_LocalBase
	virtual bool move_RealProcessing(const IMsg_Movement *pMoveMsg);
	virtual void updateLocalVector();

public:
	//override IView
	virtual void resetView(IGameObject *pObj) { m_vRotationAngle_PitchYaw = DirectX::XMFLOAT2(0, 0); }

	virtual void setTarget(const IGameObject *pTarget);

};


#endif