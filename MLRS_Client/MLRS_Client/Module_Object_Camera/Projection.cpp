#include"stdafx.h"
#include "Projection.h"


#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX

using namespace DirectX;

CProjection_PerspectiveFov::CProjection_PerspectiveFov(float fFovY, float fAspectRatioHByW, float fNearZ, float fFarZ)
	:m_fFovY(fFovY),
	m_fAspectRatioHByW(fAspectRatioHByW),
	m_fNearZ(fNearZ),
	m_fFarZ(fFarZ),
	m_pmReturnValue_Matrix(static_cast<XMFLOAT4X4A*>(_aligned_malloc(sizeof(XMFLOAT4X4A), 16)))
{
	updateReturnValue();
}
CProjection_PerspectiveFov::~CProjection_PerspectiveFov()
{
	_aligned_free(m_pmReturnValue_Matrix);
}


void CProjection_PerspectiveFov::updateReturnValue()
{
	XMStoreFloat4x4A(	m_pmReturnValue_Matrix, 
						XMMatrixPerspectiveFovLH(	XMConvertToRadians(m_fFovY),
													m_fAspectRatioHByW,
													m_fNearZ,
													m_fFarZ));
}
