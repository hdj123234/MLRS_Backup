#include"stdafx.h"
#include "Camera.h"

#include "..\Module_DXComponent\Type_InDirectX.h"
#include"..\Module_Renderer\RendererState_D3D.h"

#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_Platform_DirectX11\GlobalDirectXDevice.h"
#include"View.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

#ifndef HEADER_DIRECTX
#include<d3d11.h>
#endif // !HEADER_DIRECTX

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX


using namespace DirectX;

CCamera::CCamera()
	:m_pProjection(nullptr),
	m_pView(nullptr),
	m_pCB_Projection(nullptr),
	m_pCB_ProjectionInverse(nullptr),
	m_pCB_View(nullptr),
	m_pCB_CameraState(nullptr)
{
}

void CCamera::updateOnDX(CRendererState_D3D* pRendererState)const
{
	if (!m_pView || !m_pCB_View || !m_pCB_CameraState)
		return ;

	if (!pRendererState)
		return;

	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();

	if(!CBufferUpdater::update<XMFLOAT4X4A>(pDeviceContext, m_pCB_View, m_pView->getMatrix()))
		return;

	Type_CameraState cameraData;
	auto  pvPos = m_pView->getPosition();
	auto pvDir = m_pView->getLocalLook();
	cameraData.m_vPosition = XMFLOAT4(pvPos->x, pvPos->y, pvPos->z, 0);
	cameraData.m_vDirection = XMFLOAT4(pvDir->x, pvDir->y, pvDir->z, 0);
	cameraData.m_vFlat = m_pProjection->getFlat();

	if (!CBufferUpdater::update<Type_CameraState>(pDeviceContext, m_pCB_CameraState, cameraData))
		return;
}

const bool CCamera::initializeOnDX(CRendererState_D3D *pRendererState)const
{
	if (!m_pProjection || !m_pCB_Projection)
		return false;

	if (!pRendererState)
		return false;

	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();
	
	DirectX::XMFLOAT4X4A mProj = m_pProjection->getMatrix();
	DirectX::XMFLOAT4X4A mProjInverse;
	XMStoreFloat4x4A(&mProjInverse, XMMatrixInverse(NULL, XMLoadFloat4x4A(&mProj)));

	if (!CBufferUpdater::update<XMFLOAT4X4A>(pDeviceContext, m_pCB_Projection, mProj))
		return false;
	if (!CBufferUpdater::update<XMFLOAT4X4A>(pDeviceContext, m_pCB_ProjectionInverse, mProjInverse))
		return false;

	return true;
}

void CCamera::move(const IMsg_Movement * pMoveMsg)
{
	m_pView->move(pMoveMsg);
}

bool CCamera::createObject(IFactory_Camera * pFactory)
{
	if(!pFactory)	return false;
	m_pProjection = pFactory->createProjection();
	m_pView = pFactory->createView();
	
	m_pCB_Projection = pFactory->createCB_Projection();
	m_pCB_ProjectionInverse = pFactory->createCB_ProjectionInverse();
	m_pCB_View = pFactory->createCB_View();
	m_pCB_CameraState = pFactory->createCB_CameraState();
	if (!(m_pProjection && m_pCB_Projection  && m_pCB_View))
		return false;
	CBufferUpdater::update<XMFLOAT4X4A>(CGlobalDirectXDevice::getDeviceContext(), m_pCB_Projection, m_pProjection->getMatrix());
	return true;
}

void CCamera::release()
{
	if (m_pProjection)
		delete m_pProjection;
	m_pProjection = nullptr;

	if (m_pView)
		delete m_pView;
	m_pView = nullptr;
}

void CCamera_ThirdPerson::setTarget(const IGameObject *pTarget)
{
	if (auto * pView = dynamic_cast<AView_Target *>(m_pView))
	{
		pView->setTarget(pTarget);
	}
}
