#include"stdafx.h"
#include "View.h"

#include "..\Module_Object\Msg_Movement.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX


using namespace DirectX;


//----------------------------------- AView_LocalBase -------------------------------------------------------------------------------------

AView_LocalBase::AView_LocalBase(
	const DirectX::XMFLOAT3 & rvPos, 
	const DirectX::XMFLOAT3 & rvRight,
	const DirectX::XMFLOAT3 & rvUp, 
	const DirectX::XMFLOAT3 & rvLook)
	:m_pvPos(static_cast<XMFLOAT3A*>(_aligned_malloc(sizeof(XMFLOAT3A), 16))),
	m_pvRight(static_cast<XMFLOAT3A*>(_aligned_malloc(sizeof(XMFLOAT3A), 16))),
	m_pvUp(static_cast<XMFLOAT3A*>(_aligned_malloc(sizeof(XMFLOAT3A), 16))),
	m_pvLook(static_cast<XMFLOAT3A*>(_aligned_malloc(sizeof(XMFLOAT3A), 16))),
	m_pmReturnValue_Matrix(static_cast<XMFLOAT4X4A*>(_aligned_malloc(sizeof(XMFLOAT4X4A), 16)))
{
	m_pvPos->x = rvPos.x;
	m_pvRight->x = rvRight.x;
	m_pvUp->x = rvUp.x;
	m_pvLook->x = rvLook.x;
	m_pvPos->y = rvPos.y;
	m_pvRight->y = rvRight.y;
	m_pvUp->y = rvUp.y;
	m_pvLook->y = rvLook.y;
	m_pvPos->z = rvPos.z;
	m_pvRight->z = rvRight.z;
	m_pvUp->z = rvUp.z;
	m_pvLook->z = rvLook.z;

	updateReturnValue();
}

AView_LocalBase::~AView_LocalBase()
{
	_aligned_free(m_pvPos);
	_aligned_free(m_pvLook);
	_aligned_free(m_pvRight);
	_aligned_free(m_pvUp);
	_aligned_free(m_pmReturnValue_Matrix);
}

void AView_LocalBase::move(const IMsg_Movement * pMoveMsg)
{
	static UINT nMoveCount = 0;
	if (!pMoveMsg)	return;
	if (this->move_RealProcessing(pMoveMsg))
	{
		this->updateLocalVector();
		AView_LocalBase::updateReturnValue();
		//nMoveCount++;
		//if (nMoveCount > 100)
		//{
		//	nMoveCount = 0;
		//	XMStoreFloat3A(m_pvLook, XMVector3NormalizeEst(XMLoadFloat3A(m_pvLook)));
		//	XMStoreFloat3A(m_pvUp, XMVector3NormalizeEst(XMLoadFloat3A(m_pvUp)));
		//	XMStoreFloat3A(m_pvRight, XMVector3Cross(XMLoadFloat3A(m_pvUp), XMLoadFloat3A(m_pvLook)));
		//}
	}
}

void AView_LocalBase::updateReturnValue()
{

	XMVECTOR vPos = XMLoadFloat3A(m_pvPos);
	m_pmReturnValue_Matrix->_11 = m_pvRight->x;
	m_pmReturnValue_Matrix->_21 = m_pvRight->y;
	m_pmReturnValue_Matrix->_31 = m_pvRight->z;
	m_pmReturnValue_Matrix->_12 = m_pvUp->x;
	m_pmReturnValue_Matrix->_22 = m_pvUp->y;
	m_pmReturnValue_Matrix->_32 = m_pvUp->z;
	m_pmReturnValue_Matrix->_13 = m_pvLook->x;
	m_pmReturnValue_Matrix->_23 = m_pvLook->y;
	m_pmReturnValue_Matrix->_33 = m_pvLook->z;
	m_pmReturnValue_Matrix->_41 = -XMVectorGetX(XMVector3Dot(XMLoadFloat3A(m_pvRight), vPos));
	m_pmReturnValue_Matrix->_42 = -XMVectorGetX(XMVector3Dot(XMLoadFloat3A(m_pvUp), vPos));
	m_pmReturnValue_Matrix->_43 = -XMVectorGetX(XMVector3Dot(XMLoadFloat3A(m_pvLook), vPos));
	m_pmReturnValue_Matrix->_14 = 0;
	m_pmReturnValue_Matrix->_24 = 0;
	m_pmReturnValue_Matrix->_34 = 0;
	m_pmReturnValue_Matrix->_44 = 1;
}


//----------------------------------- AView_Target -------------------------------------------------------------------------------------

AView_Target::AView_Target(const DirectX::XMFLOAT3 & rvRelativePos)
	: m_pvTargetPos(nullptr),
	m_pvRelativePos(static_cast<DirectX::XMFLOAT3A *>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16)))
{
	m_pvRelativePos->x = rvRelativePos.x;
	m_pvRelativePos->y = rvRelativePos.y;
	m_pvRelativePos->z = rvRelativePos.z;
}

AView_Target::~AView_Target()
{
	_aligned_free(m_pvRelativePos);
}

void AView_Target::setTarget(const IGameObject * pTarget)
{
	if (auto p = dynamic_cast<RIGameObject_PositionGetter const *>(pTarget))
	{
		m_pvTargetPos = p->getPositionOrigin();
	}
}


//----------------------------------- CView_Local -------------------------------------------------------------------------------------

CView_Local::CView_Local(
	const float fMoveUnit,
	const float fRotationUnit,
	const DirectX::XMFLOAT3 & rvPos,
	const DirectX::XMFLOAT3 & rvRight,
	const DirectX::XMFLOAT3 & rvUp,
	const DirectX::XMFLOAT3 & rvLook)
	:AView_LocalBase(rvPos, rvRight, rvUp, rvLook),
	m_fMoveUnit(fMoveUnit),
	m_fRotationUnit(fRotationUnit)
{
}


bool CView_Local::move_RealProcessing(const IMsg_Movement * pMoveMsg)
{
	if (!pMoveMsg)
		return false;

	switch (pMoveMsg->getMovementType())
	{
	case EMsgType_Movement::eMove_LookBase:
	{
		const CMsg_Movement_Vector3 *msg = dynamic_cast<const CMsg_Movement_Vector3 *>(pMoveMsg);
		XMVECTOR vLook = XMLoadFloat3A(m_pvLook);
		XMVECTOR vRight = XMLoadFloat3A(m_pvRight);
		XMVECTOR vUp = XMLoadFloat3A(m_pvUp);
		const XMFLOAT3 * pvMovement = msg->getVector();
		XMStoreFloat3A(m_pvPos, XMLoadFloat3A(m_pvPos) + 
			(vRight*(pvMovement->x*m_fMoveUnit)) + 
			(vUp*(pvMovement->y*m_fMoveUnit))+ 
			(vLook*(pvMovement->z*m_fMoveUnit)));
		return true;
	}
	case EMsgType_Movement::eRotate_PitchYaw:
	{
		const CMsg_Movement_Vector2 *msg = dynamic_cast<const CMsg_Movement_Vector2*>(pMoveMsg);
		float fPitch = msg->getVector()->x;
		XMVECTOR vRight = XMLoadFloat3A(m_pvRight);
		float fYaw = msg->getVector()->y;
		XMMATRIX mRotation_Pitch = XMMatrixRotationAxis(vRight, XMConvertToRadians(fPitch*m_fRotationUnit));
		XMMATRIX mRotation_Yaw = XMMatrixRotationRollPitchYaw(0, XMConvertToRadians(fYaw*m_fRotationUnit), 0);
		XMMATRIX mRotation = mRotation_Pitch*mRotation_Yaw;
		XMStoreFloat3A(m_pvLook,	XMVector3TransformCoord(XMLoadFloat3A(m_pvLook), mRotation));
		XMStoreFloat3A(m_pvRight,	XMVector3TransformCoord(XMLoadFloat3A(m_pvRight), mRotation));
		XMStoreFloat3A(m_pvUp,		XMVector3TransformCoord(XMLoadFloat3A(m_pvUp), mRotation));
		return true;
	}
	default:
		return false;
	}
}

void CView_Local::resetView(IGameObject * pObj)
{
	auto pLocalVectorGetter = dynamic_cast<RIGameObject_LocalVectorGetter*>(pObj);
	auto pPosGetter = dynamic_cast<RIGameObject_PositionGetter*>(pObj);

	if (pPosGetter && pLocalVectorGetter )
	{
		auto &rvPos = pPosGetter->getPosition();
		m_pvPos->x = rvPos.x;
		m_pvPos->y = rvPos.y;
		m_pvPos->z = rvPos.z;

		*m_pvRight = *pLocalVectorGetter->getRightOrigin();
		*m_pvUp = *pLocalVectorGetter->getUpOrigin();
		*m_pvLook = *pLocalVectorGetter->getLookOrigin();

	}
}

//----------------------------------- CView_ThirdPerson -------------------------------------------------------------------------------------

CView_ThirdPerson::CView_ThirdPerson(
	const DirectX::XMFLOAT3 &rvRelativePos,
	const float fMoveUnit, 
	const float fRotationUnitX, 
	const float fRotationUnitY,
	const float fInitDistance)
	:AView_Target(rvRelativePos),
	m_fMoveUnit	(fMoveUnit),
	m_fRotationUnitX(fRotationUnitX),
	m_fRotationUnitY(fRotationUnitY),
	m_fDistance		(fInitDistance),
	m_fAngleX		(0),
	m_fAngleY		(0)
{
}

CView_ThirdPerson::CView_ThirdPerson(ViewState_ThirdPerson & rViewState)
	:AView_Target(rViewState.m_vRelativePos),
	m_fMoveUnit(rViewState.m_fDistanceMoveUnit),
	m_fRotationUnitX(rViewState.m_fRotationUnitX),
	m_fRotationUnitY(rViewState.m_fRotationUnitY),
	m_fDistance(rViewState.m_fInitDistance),
	m_fAngleX(0),
	m_fAngleY(0)
{
}


bool CView_ThirdPerson::move_RealProcessing(const IMsg_Movement * pMoveMsg)
{
	if (!pMoveMsg)
		return false;

	switch (pMoveMsg->getMovementType())
	{ 
	case EMsgType_Movement::eRotate_PitchYaw:
	{
		const CMsg_Movement_Vector2 *msg = dynamic_cast<const CMsg_Movement_Vector2*>(pMoveMsg);
		float fPitch = msg->getVector()->x;
		float fYaw = msg->getVector()->y;
		m_fAngleX += XMConvertToRadians(fPitch*m_fRotationUnitX);
		m_fAngleY += XMConvertToRadians(fYaw*m_fRotationUnitY);
		return true;
	}
	case EMsgType_Movement::eControl_Distance:
	{
		const CMsg_Movement_Float *msg = dynamic_cast<const CMsg_Movement_Float*>(pMoveMsg);
		float fMovement = -msg->getData();
		m_fDistance += fMovement * m_fMoveUnit;
		return true;
	}
	case EMsgType_Movement::eAuto:
		return true;

	default:
		return false;
	}
}

void CView_ThirdPerson::updateLocalVector()
{
	XMVECTOR vTargetPos = XMLoadFloat3A(m_pvTargetPos);
	XMMATRIX mWorld = XMMatrixTranslationFromVector(vTargetPos);

	XMMATRIX mCameraFromLocal = XMMatrixTranslationFromVector(XMLoadFloat3A(m_pvRelativePos))
							* XMMatrixTranslation(0, 0, -m_fDistance)
							* XMMatrixRotationRollPitchYaw(m_fAngleX, m_fAngleY, 0);

	XMMATRIX mResult = mCameraFromLocal*mWorld;

	XMStoreFloat3A(m_pvRight, mResult.r[0]);
	XMStoreFloat3A(m_pvUp, mResult.r[1]);
	XMStoreFloat3A(m_pvLook, mResult.r[2]);
	XMStoreFloat3A(m_pvPos, mResult.r[3]);
}

void CView_ThirdPerson::resetView(IGameObject * pObj)
{
	if (auto pObj_DirGetter = dynamic_cast<RIGameObject_DirectionGetter *>(pObj))
	{
		auto &rvDir = pObj_DirGetter->getDirection();
		//üũ
		auto vRight_DefaultAxis = XMVectorSet(1, 0, 0, 0);
		auto vUp_DefaultAxis = XMVectorSet(0, 1, 0, 0);
		auto vLook_DefaultAxis = XMVectorSet(0, 0, 1, 0);
		auto vLook = XMVector3NormalizeEst(XMVectorSet(rvDir.x, 0, rvDir.z,0));
		float fAngle = XMScalarACos( XMVectorGetX( XMVector3Dot(vLook_DefaultAxis, vLook)));
		if (XMVectorGetX(XMVector3Dot(vRight_DefaultAxis, vLook)) < 0)
			fAngle = -fAngle;
		m_fAngleY = fAngle;
		updateLocalVector();
	}
}

//----------------------------------- CView_ThirdPerson_LimitRange -------------------------------------------------------------------------------------

CView_ThirdPerson_LimitRange::CView_ThirdPerson_LimitRange(
	const DirectX::XMFLOAT2 & rvRange_Distance, 
	const DirectX::XMFLOAT2 & rvRange_AngleX, 
	const DirectX::XMFLOAT3 & rvRelativePos,
	const float fMoveUnit,
	const float fRotationUnitX, 
	const float fRotationUnitY, 
	const float fInitDistance)
	:CView_ThirdPerson(rvRelativePos,fMoveUnit,fRotationUnitX,fRotationUnitY,fInitDistance),
	m_vRange_Distance(rvRange_Distance),
	m_vRange_AngleX(XMConvertToRadians(rvRange_AngleX.x), XMConvertToRadians(rvRange_AngleX.y) )
{
}

CView_ThirdPerson_LimitRange::CView_ThirdPerson_LimitRange(ViewState_ThirdPerson_LimitRange & rViewState)
	:CView_ThirdPerson(rViewState.m_vs_ThirdPerson),
	m_vRange_Distance(rViewState.m_vRange_Distance),
	m_vRange_AngleX(rViewState.m_vRange_AngleX_Radian)
{
}

bool CView_ThirdPerson_LimitRange::move_RealProcessing(const IMsg_Movement * pMoveMsg)
{
	if (!CView_ThirdPerson::move_RealProcessing(pMoveMsg))	return false;

	if (m_fAngleX<m_vRange_AngleX.x ) m_fAngleX = m_vRange_AngleX.x;
	else if (m_fAngleX>m_vRange_AngleX.y) m_fAngleX = m_vRange_AngleX.y;
	
	if (m_fDistance<m_vRange_Distance.x) m_fDistance = m_vRange_Distance.x;
	else if (m_fDistance>m_vRange_Distance.y) m_fDistance = m_vRange_Distance.y;
	return true;
}


//----------------------------------- CView_TPS -------------------------------------------------------------------------------------
//
//CView_TPS::CView_TPS(
//	const DirectX::XMFLOAT3 & rvRelativePos,
//	const float fRotationUnitX,
//	const DirectX::XMFLOAT2 &rvRange_AngleX)
//	:AView_Target(rvRelativePos),
//	m_pvTargetRight(nullptr),
//	m_pvTargetUp(nullptr),
//	m_pvTargetLook(nullptr),
//	m_fRotationUnitX(fRotationUnitX),
//	m_vRange_AngleX(rvRange_AngleX),
//	m_fAngleX(0)
//{
//}

CView_TPS::CView_TPS(const ViewState_TPS & rvViewState)
	:AView_Target(rvViewState.m_vRelativePos),
	m_pvTargetRight(nullptr),
	m_pvTargetUp   (nullptr),
	m_pvTargetLook (nullptr),
	m_fRotationUnitX(rvViewState.m_fRotationUnit_X),
	m_vRange_AngleX(rvViewState.m_vAngleRange_X),
	m_fAngleX(rvViewState.m_fDefaultAngle_X),
	m_fDefaultAngle_X(rvViewState.m_fDefaultAngle_X)
{
}

bool CView_TPS::move_RealProcessing(const IMsg_Movement * pMoveMsg)
{
	if (!pMoveMsg)
		return false;

	switch (pMoveMsg->getMovementType())
	{
	case EMsgType_Movement::eRotate_Pitch:
	{
		const CMsg_Movement_Float *msg = dynamic_cast<const CMsg_Movement_Float*>(pMoveMsg);
		float fPitch = msg->getData();
		m_fAngleX += XMConvertToRadians(fPitch*m_fRotationUnitX);
		if (m_fAngleX < m_vRange_AngleX.x)m_fAngleX = m_vRange_AngleX.x;
		else if (m_fAngleX > m_vRange_AngleX.y)m_fAngleX = m_vRange_AngleX.y;
		return true;
	}
	case EMsgType_Movement::eAuto:
		return true;

	default:
		return false;
	}
}

void CView_TPS::updateLocalVector()
{
	XMMATRIX mWorld = XMMatrixSet(
		m_pvTargetRight->x, m_pvTargetRight->y, m_pvTargetRight->z, 0,
		m_pvTargetUp->x, m_pvTargetUp->y, m_pvTargetUp->z, 0,
		m_pvTargetLook->x, m_pvTargetLook->y, m_pvTargetLook->z, 0,
		m_pvTargetPos->x, m_pvTargetPos->y, m_pvTargetPos->z, 1);
//	XMMATRIX mWorld = XMMatrixTranslationFromVector(vTargetPos);

	XMMATRIX mCameraFromLocal = XMMatrixTranslationFromVector(XMLoadFloat3A(m_pvRelativePos)) ;
	XMMATRIX mCameraRotation = XMMatrixRotationRollPitchYaw(m_fAngleX, 0, 0);
	XMMATRIX mResult = mCameraFromLocal*mCameraRotation*mWorld;

	XMStoreFloat3A(m_pvRight, mResult.r[0]);
	XMStoreFloat3A(m_pvUp, mResult.r[1]);
	XMStoreFloat3A(m_pvLook, mResult.r[2]);
	XMStoreFloat3A(m_pvPos, mResult.r[3]);
}

void CView_TPS::setTarget(const IGameObject * pTarget)
{
	AView_Target::setTarget(pTarget);
	if (auto p = dynamic_cast<const RIGameObject_LocalVectorGetter *>(pTarget))
	{
		m_pvTargetRight = p->getRightOrigin();
		m_pvTargetUp = p->getUpOrigin();
		m_pvTargetLook = p->getLookOrigin();
	}
	m_fAngleX = m_fDefaultAngle_X;
}


//----------------------------------- CView_FPS_YawPitch -------------------------------------------------------------------------------------

CView_FPS_PitchYaw::CView_FPS_PitchYaw(const ViewState_FPS_PitchYaw & rvViewState)
	:AView_Target(rvViewState.m_vRelativePos),
	m_pvTargetRight(nullptr),
	m_pvTargetUp(nullptr),
	m_pvTargetLook(nullptr),
	m_vRotationUnit(rvViewState.m_vRotationUnit),
	m_vRange_AngleX(rvViewState.m_vRange_AngleX),
	m_vRange_AngleY(rvViewState.m_vRange_AngleY),
	m_vRotationAngle_PitchYaw(0,0)
{
}

bool CView_FPS_PitchYaw::move_RealProcessing(const IMsg_Movement * pMoveMsg)
{
	if (!pMoveMsg)
		return false;

	switch (pMoveMsg->getMovementType())
	{
	case EMsgType_Movement::eRotate_PitchYaw:
	{
		const CMsg_Movement_Vector2 *msg = dynamic_cast<const CMsg_Movement_Vector2*>(pMoveMsg);
		const XMFLOAT2 *pvPitchYaw = msg->getVector();
		m_vRotationAngle_PitchYaw.x += XMConvertToRadians(pvPitchYaw->x * m_vRotationUnit.x);
		m_vRotationAngle_PitchYaw.y += XMConvertToRadians(pvPitchYaw->y * m_vRotationUnit.y);

		if (m_vRotationAngle_PitchYaw.x < m_vRange_AngleX.x)m_vRotationAngle_PitchYaw.x = m_vRange_AngleX.x;
		else if (m_vRotationAngle_PitchYaw.x > m_vRange_AngleX.y)m_vRotationAngle_PitchYaw.x = m_vRange_AngleX.y;

		if (m_vRotationAngle_PitchYaw.y < m_vRange_AngleY.x)m_vRotationAngle_PitchYaw.y = m_vRange_AngleY.x;
		else if (m_vRotationAngle_PitchYaw.y > m_vRange_AngleY.y)m_vRotationAngle_PitchYaw.y = m_vRange_AngleY.y;
		return true;
	}
	case EMsgType_Movement::eAuto:
		return true;

	default:
		return false;
	}
}

void CView_FPS_PitchYaw::updateLocalVector()
{
	XMMATRIX mWorld = XMMatrixSet(
		m_pvTargetRight->x, m_pvTargetRight->y, m_pvTargetRight->z, 0,
		m_pvTargetUp->x, m_pvTargetUp->y, m_pvTargetUp->z, 0,
		m_pvTargetLook->x, m_pvTargetLook->y, m_pvTargetLook->z, 0,
		m_pvTargetPos->x, m_pvTargetPos->y, m_pvTargetPos->z, 1);
	//	XMMATRIX mWorld = XMMatrixTranslationFromVector(vTargetPos);

	XMMATRIX mCameraFromLocal = XMMatrixTranslationFromVector(XMLoadFloat3A(m_pvRelativePos));
	XMMATRIX mCameraRotation = XMMatrixRotationRollPitchYaw(m_vRotationAngle_PitchYaw.x, m_vRotationAngle_PitchYaw.y, 0);
	XMMATRIX mResult = mCameraFromLocal*mCameraRotation*mWorld;

	XMStoreFloat3A(m_pvRight, mResult.r[0]);
	XMStoreFloat3A(m_pvUp, mResult.r[1]);
	XMStoreFloat3A(m_pvLook, mResult.r[2]);
	XMStoreFloat3A(m_pvPos, mResult.r[3]);
}

void CView_FPS_PitchYaw::setTarget(const IGameObject * pTarget)
{
	AView_Target::setTarget(pTarget);
	if (auto p = dynamic_cast<const RIGameObject_LocalVectorGetter *>(pTarget))
	{
		m_pvTargetRight = p->getRightOrigin();
		m_pvTargetUp = p->getUpOrigin();
		m_pvTargetLook = p->getLookOrigin();
	}
}
