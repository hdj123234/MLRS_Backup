#pragma once
#ifndef HEADER_FACTORY_CAMERA_THIRDPERSON
#define HEADER_FACTORY_CAMERA_THIRDPERSON

#include"Camera.h"
#include"Factory_Camera.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

//���漱��
struct ID3D11Buffer;

struct CameraState_ThirdPerson {
	ViewState_ThirdPerson  m_vs_Third;
	CameraState_Projection m_ps;

	CameraState_ThirdPerson() {}
	CameraState_ThirdPerson(const DirectX::XMFLOAT3 &rvRelativePos ,
							const float fInitDistance = 60,
							const float fRotationUnitX = 0.02f,
							const float fRotationUnitY = 0.02f,
							const float fDistanceMoveUnit = 1,
							const float fFarZ = 50000,
							const float fNearZ = 1,
							const float fFOV = 45);
};


class CFactory_Camera_ThirdPerson : public AFactory_Camera{
public:
private:
	const CameraState_ThirdPerson &m_rCameraState;
public:
	CFactory_Camera_ThirdPerson(CameraState_ThirdPerson &rCameraState);
	virtual ~CFactory_Camera_ThirdPerson();

	//override IFactory_Camera
	virtual IView *createView();
};






#endif