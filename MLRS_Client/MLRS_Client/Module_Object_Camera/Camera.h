#pragma once
#ifndef HEADER_CAMERA
#define HEADER_CAMERA

#include "Interface_Camera.h"

//���漱��
struct ID3D11Buffer;

class IFactory_Camera {
public:
	virtual ~IFactory_Camera() = 0 {}

	virtual IView *createView()=0;
	virtual IProjection *createProjection()=0;
	virtual ID3D11Buffer *createCB_Projection() = 0;
	virtual ID3D11Buffer *createCB_ProjectionInverse() = 0;
	virtual ID3D11Buffer *createCB_View() = 0;
	virtual ID3D11Buffer *createCB_CameraState() = 0;
};

class CCamera : virtual public ICamera{
protected:
	ID3D11Buffer *m_pCB_View;
	ID3D11Buffer *m_pCB_Projection;
	ID3D11Buffer *m_pCB_ProjectionInverse;
	ID3D11Buffer *m_pCB_CameraState;
	IView *m_pView;
	IProjection *m_pProjection;
public:
	CCamera();
	virtual ~CCamera() { CCamera::release(); }

	//override IObject
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW|FLAG_OBJECTTYPE_BEMUST_INIT_FOR_DRAW; }
	
	//override IObject_UpdateOnDX
	virtual void updateOnDX(CRendererState_D3D* pRendererState)const;

	//override RIGameObject_BeMust_InitForDraw
	virtual const bool initializeOnDX(CRendererState_D3D *pRendererState)const;

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg);
	
	//override RIGameObject_DirectionGetter
	virtual DirectX::XMFLOAT3A* getDirectionOrigin() const { return nullptr; }
	virtual const DirectX::XMFLOAT3 getDirection() const { auto v = m_pView->getLocalLook(); return DirectX::XMFLOAT3(v->x, v->y, v->z); }

	//override RIGameObject_PositionGetter
	virtual DirectX::XMFLOAT3A* getPositionOrigin()const { return nullptr; }
	virtual const DirectX::XMFLOAT3 getPosition()const { auto v = m_pView->getPosition(); return DirectX::XMFLOAT3(v->x, v->y, v->z);   }

	bool createObject(IFactory_Camera *pFactory);
	void release();
};

class CCamera_ThirdPerson : public CCamera,
							public RICamera_Target {
public:
	CCamera_ThirdPerson() {}
	virtual ~CCamera_ThirdPerson() { CCamera::release(); }

	//override RICamera_Target
	virtual void setTarget(const IGameObject *pTarget) ;
};


#endif