
#ifndef HEADER_INTERFACE_CAMERA
#define HEADER_INTERFACE_CAMERA

#include"..\Module_Object\Interface_Object.h"

namespace DirectX {
	struct XMFLOAT4X4A;
	struct XMFLOAT3;
	struct XMFLOAT3A;
}

class IProjection {
public:
	virtual ~IProjection() = 0 {}
	virtual const DirectX::XMFLOAT4X4A& getMatrix()const = 0;

	virtual DirectX::XMFLOAT2 getFlat() = 0;
};

class IView {
public:
	virtual ~IView() = 0 {}

	virtual void move(const IMsg_Movement *pMoveMsg) = 0;
	virtual const DirectX::XMFLOAT4X4A & getMatrix()const = 0;
	virtual const DirectX::XMFLOAT3A *  getPosition()const = 0;
	virtual const DirectX::XMFLOAT3A * getLocalRight()const = 0;
	virtual const DirectX::XMFLOAT3A * getLocalUp()const = 0;
	virtual const DirectX::XMFLOAT3A * getLocalLook()const = 0;
	virtual void resetView(IGameObject *pObj) = 0;
};


class ICamera : virtual public IGameObject,
				virtual public RIGameObject_BeMust_RenewalForDraw,
				virtual public RIGameObject_BeMust_InitForDraw,
				virtual public RIGameObject_Movable,
				virtual public RIGameObject_PositionGetter,
				virtual public RIGameObject_DirectionGetter {
public:
	virtual ~ICamera() = 0 {}

	virtual void resetCamera(IGameObject *pObj)=0;

};

class RICamera_Target {
public:
	virtual ~RICamera_Target() = 0 {}

	virtual void setTarget(const IGameObject *pTarget)=0;

};

class RICamera_MultiMode {
public:
	virtual ~RICamera_MultiMode() = 0 {}

	virtual void setCameraMode(const ENUMTYPE_256 eMode, IGameObject *pObj)= 0;
};

#endif