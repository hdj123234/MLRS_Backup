#include"stdafx.h"
#include "Factory_Camera.h"

#include"View.h"
#include"Projection.h"

#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>

#endif // !HEADER_DirectX


using namespace DirectX;


//----------------------------------- CameraState_Projection -------------------------------------------------------------------------------------
CameraState_Projection::CameraState_Projection(
	const float fFarZ,
	const float fNearZ,
	const float fFOV)
	:m_fFarZ(fFarZ),
	m_fNearZ(fNearZ),
	m_fFOV(fFOV)
	{	}

//----------------------------------- AFactory_Camera -------------------------------------------------------------------------------------

IProjection * AFactory_Camera::createProjection()
{
	CProjection_PerspectiveFov *pProjection 
		= new CProjection_PerspectiveFov(	m_CameraState_Projection.m_fFOV, 
											CGlobalWindow::getAspectRatio(),
											m_CameraState_Projection.m_fNearZ, 
											m_CameraState_Projection.m_fFarZ);
	return pProjection;
}


ID3D11Buffer * AFactory_Camera::createCB_CameraState()
{
	return factory_CB_CameraState.createBuffer();
}

AFactory_Camera::AFactory_Camera(
	const float fFarZ, 
	const float fFOV, 
	const float fNearZ)
	:m_CameraState_Projection(fFarZ,fFOV,fNearZ)
{
}

AFactory_Camera::AFactory_Camera(const CameraState_Projection & rCameraState_Projection)
	: m_CameraState_Projection(rCameraState_Projection)
{
}

ID3D11Buffer * AFactory_Camera::createCB_Projection()
{
	return factory_CB_Matrix_Projection.createBuffer();
}
ID3D11Buffer * AFactory_Camera::createCB_ProjectionInverse()
{
	return factory_CB_Matrix_ProjectionInverse.createBuffer();
}
ID3D11Buffer * AFactory_Camera::createCB_View()
{
	return factory_CB_Matrix_View.createBuffer();
}



//----------------------------------- CFactory_Camera -------------------------------------------------------------------------------------

CFactory_Camera_Local::CFactory_Camera_Local(
	const float fMoveUnit,
	const float fRotationUnit,
	const DirectX::XMFLOAT3 & rvPos,
	const DirectX::XMFLOAT3 & rvDir,
	const float fFarZ,
	const float fFOV,
	const float fNearZ)
	:AFactory_Camera(fFarZ, fFOV, fNearZ),
	m_vPos(rvPos),
	m_vDir(rvDir),
	m_fMoveUnit(fMoveUnit),
	m_fRotationUnit(fRotationUnit)
{
}

IView * CFactory_Camera_Local::createView()
{
	CView_Local *pView = new CView_Local(m_fMoveUnit, m_fRotationUnit, m_vPos);
	return pView;
}
