#pragma once
#ifndef HEADER_FACTORY_PAINTER_COMMON
#define HEADER_FACTORY_PAINTER_COMMON

#include"Painter_Common.h"
#include"..\Module_Object_GameModel\Factory_Material.h"

//���漱��
class CMyType_Bones;



//----------------------------------- BoundingBox -------------------------------------------------------------------------------------

class CFactory_Painter_BoundingBox : virtual public IFactory_Painter_BoundingBox,
							virtual private IFactory_Material{
public:
	virtual ~CFactory_Painter_BoundingBox() {}

	//override IFactory_Painter_BoundingBox
	virtual std::vector<ID3D11Buffer *> createGSBuffers() ;
	virtual ID3D11RasterizerState * createRasterizerState() ;
	virtual CMaterial_None getMaterial() ;

	//override IFactory_Mesh_GS
	virtual ID3D11GeometryShader	* createGeometryShader() ;
	virtual ID3D11VertexShader		* createVertexShader() ;
	virtual const unsigned int getMaterialID() { return 0; }

	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader() ;
};

//----------------------------------- Line -------------------------------------------------------------------------------------

class CFactory_Painter_Line  : virtual public IFactory_Painter_Line,
	virtual private IFactory_Material {
public:
	virtual ~CFactory_Painter_Line() {}

	//override IFactory_Painter_BoundingBox
	virtual std::vector<ID3D11Buffer *> createGSBuffers();
	virtual ID3D11RasterizerState * createRasterizerState();
	virtual CMaterial_None getMaterial();

	//override IFactory_Mesh_GS
	virtual ID3D11GeometryShader	* createGeometryShader();
	virtual ID3D11VertexShader		* createVertexShader();
	virtual const unsigned int getMaterialID() { return 0; }

	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader();
};


//----------------------------------- Curve -------------------------------------------------------------------------------------

class CFactory_Painter_Curve : virtual public IFactory_Painter_Curve{
private:
	const std::string m_sName;
	const unsigned int m_nMaxNumberOfVertex;
	CFactory_Material_NoneLighting_ColorOnly m_Factory_Material;

	static const std::string s_sVSName;
	static const std::string s_sPSName;

public:
	CFactory_Painter_Curve(	const std::string &rsName,
							const DirectX::XMFLOAT4 &rvColor,
							const unsigned int nMaxNumberOfVertex);
	virtual ~CFactory_Painter_Curve()  {}

	//override IFactory_Painter_Curve
	virtual ID3D11RasterizerState * createRasterizerState() ;
	virtual std::vector<ID3D11Buffer *> createCBs_VS() ;
	virtual CMaterial_NoneLighting_ColorOnly getMaterial() ;

	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11InputLayout	*	createInputLayout() ;
	virtual ID3D11Buffer		*	createVertexBuffer() ;
	virtual unsigned int			getStride() { return sizeof(DirectX::XMFLOAT3); }
	virtual unsigned int			getOffset() { return 0; }
	virtual unsigned int			getNumberOfVertex() { return 0; }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP; }

	//override IFactory_Mesh
	virtual ID3D11VertexShader		* createVertexShader() ;
	virtual const unsigned int getMaterialID() { return 0; }

//	//override IFactory_Material
//	virtual ID3D11PixelShader *createPixelShader();
//
//	//override IFactory_Material_NoneLighting_ColorOnly
//	virtual ID3D11Buffer * createCB_Color();
};


//----------------------------------- Curve -------------------------------------------------------------------------------------

class CFactory_Painter_CirclePlate : virtual public IFactory_Painter_CirclePlate {
private:
	const std::string m_sName; 
	CFactory_Material_NoneLighting_ColorOnly m_Factory_Material;

	const float m_fRadius;

	static const std::string s_sVSName;
	static const std::string s_sGSName;
	static const std::string s_sPSName;

public:
	CFactory_Painter_CirclePlate(	const std::string &rsName,
									const DirectX::XMFLOAT4 &rvColor,
									const float fRadius);
	virtual ~CFactory_Painter_CirclePlate() {}

	//override IFactory_Painter_CirclePlate
	virtual ID3D11RasterizerState *				createRasterizerState();
	virtual ID3D11BlendState *					createBlendState();
	virtual ID3D11DepthStencilState * createDepthStencilState() ;
	virtual std::vector<ID3D11Buffer *>			createCBs_GS();
	virtual CMaterial_NoneLighting_ColorOnly	getMaterial();
	virtual const float getRadius() { return m_fRadius; }

	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11InputLayout	*	createInputLayout();
	virtual ID3D11Buffer		*	createVertexBuffer();
	virtual unsigned int			getStride() { return sizeof(DirectX::XMFLOAT4); }
	virtual unsigned int			getOffset() { return 0; }
	virtual unsigned int			getNumberOfVertex() { return 1; }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST; }

	//override IFactory_Mesh_GS  
	virtual ID3D11GeometryShader	* createGeometryShader() ;

	//override IFactory_Mesh
	virtual ID3D11VertexShader		* createVertexShader();
	virtual const unsigned int getMaterialID() { return 0; }
	 
};









//----------------------------------- Bone -------------------------------------------------------------------------------------

class CFactory_Mesh_Painter_Bone : public IFactory_Mesh_Bone {
private:
	ID3D11ShaderResourceView	* m_pBoneStructure;
	ID3D11ShaderResourceView	* m_pAnimData;
	ID3D11ShaderResourceView	* m_pAnimIndicator;
	ID3D11Buffer				* m_pAnimUpdater;
	unsigned int m_nNumberOfBone;
	unsigned int m_nNumberOfFrame;

public:
	CFactory_Mesh_Painter_Bone(const unsigned int nNumberOfBone,
		ID3D11ShaderResourceView	* pBoneStructure,
		ID3D11ShaderResourceView	* pAnimData,
		ID3D11ShaderResourceView	* pAnimIndicator);
	CFactory_Mesh_Painter_Bone(CMyType_Bones &rBoneStructure,
		std::vector<std::vector<DirectX::XMFLOAT4X4>> &rmAnimData);
	virtual ~CFactory_Mesh_Painter_Bone() {}

	virtual ID3D11VertexShader		* createVertexShader();
	virtual ID3D11GeometryShader	* createGeometryShader();
	virtual ID3D11PixelShader	* createPixelShader();

	virtual ID3D11ShaderResourceView	* getBoneStructure() { return m_pBoneStructure; }
	virtual ID3D11ShaderResourceView	* getAnimData() { return m_pAnimData; }
	virtual ID3D11ShaderResourceView	* getAnimIndicator() { return m_pAnimIndicator; }
	virtual ID3D11Buffer				*getAnimUpdater() { return m_pAnimUpdater; }
	const unsigned int 	getNumberOfBone() { return m_nNumberOfBone; }
	const unsigned int 	getNumberOfFrame() { return m_nNumberOfFrame; }
	std::vector<ID3D11Buffer *> createVP();
	virtual ID3D11DepthStencilState * createDepthStencilState() ;
	virtual ID3D11BlendState *createBlendState();

	virtual const unsigned int getMaterialID() { return 0; }
};

#endif