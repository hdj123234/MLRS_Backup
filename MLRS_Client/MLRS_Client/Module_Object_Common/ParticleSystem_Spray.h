#pragma once

#ifndef HEADER_PARTICLESYSTEM_SPRAY
#define HEADER_PARTICLESYSTEM_SPRAY

#include "Interface_Object_Common.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"

#include "..\Module_Renderer\RendererState_D3D.h"
#include "..\Module_Platform_DirectX11\DXUtility.h"


#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif 

#ifndef HEADER_STL
#include<set>
#include<array>
#endif // !HEADER_STL

//전방선언
class CRendererState_D3D;

template<class SprayPointType>
class CParticleSprayPoints ;

class CParticleMover ;

class CParticlePainter_Spray ;

//----------------------------------- Creator -------------------------------------------------------------------------------------
class IFactory_ParticleSprayPoints {
public:
	virtual ~IFactory_ParticleSprayPoints() = 0 {};

	virtual ID3D11VertexShader*		createVS_SprayPoints()=0;
	virtual ID3D11InputLayout *		createIL_SprayPoints()=0;
	virtual ID3D11Buffer*			createVB_SprayPoints()=0;
	virtual ID3D11GeometryShader*	createGS_SprayPoints()=0;
	virtual std::vector<ID3D11Buffer *> createCBs_GS_Points() = 0;
	virtual UINT					getStride_SprayPoints() = 0;
};

class IFactory_ParticleMover {
public:
	virtual ~IFactory_ParticleMover() = 0 {};

	virtual ID3D11VertexShader*		createVS_Mover()=0;
	virtual ID3D11InputLayout *		createIL_Mover()=0;
	virtual ID3D11GeometryShader*	createGS_Mover()=0;
	virtual UINT					getStride_Mover() = 0;
};

class IFactory_ParticlePainter {
public:
	virtual ~IFactory_ParticlePainter() = 0 {};

	virtual ID3D11VertexShader *		createVS_Painter()=0;
	virtual ID3D11InputLayout *			createIL_Painter()=0;
	virtual ID3D11GeometryShader *		createGS_Painter()=0;
	virtual std::vector<ID3D11Buffer *> createCBs_GS_Painter()=0;
	virtual UINT						getStride_Painter() = 0;
	virtual RIMaterial_SetterOnDX *		createMaterial()=0;
};

template<class SprayPointType>
class IFactory_ParticleSystem_Spray {
public:
	virtual ~IFactory_ParticleSystem_Spray() = 0 {};

	virtual CParticleSprayPoints<SprayPointType> *	createParticleSprayPoints()=0;
	virtual CParticleMover *						createParticleMover()=0;
	virtual CParticlePainter_Spray *				createParticlePainter()=0;
	virtual std::array<ID3D11Buffer *, 2>			createParticleBuffers()=0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

template<class SprayPointType>
class CParticleSprayPoints {
private: 
	ID3D11VertexShader* m_pVS;
	ID3D11InputLayout * m_pIL;
	UINT m_nStride;
	ID3D11Buffer* m_pVB_SprayPoint;
	std::vector<ID3D11Buffer *> m_pCBs_GS;
	ID3D11GeometryShader* m_pGS;

	std::set<RIGameObject_SprayParticle<SprayPointType> *> m_pSprayObjects;

public:
	CParticleSprayPoints()
		:m_pVS(nullptr),
		m_pIL(nullptr),
		m_nStride(0),
		m_pVB_SprayPoint(nullptr),
		m_pGS(nullptr)
	{}
	virtual ~CParticleSprayPoints() { CParticleSprayPoints::releaseObject(); }

public:
	virtual const unsigned int sprayParticles(	CRendererState_D3D *pRendererState,
												ID3D11Buffer* pSOTarget_Particle, 
												unsigned int nOffset=0)const;	//return spray count

	void addObject(RIGameObject_SprayParticle<SprayPointType> * pObject) { m_pSprayObjects.insert(pObject); }
	void removeObject(RIGameObject_SprayParticle<SprayPointType> * pObject) { m_pSprayObjects.erase(pObject); }

	bool createObject(IFactory_ParticleSprayPoints *pFactory);
	void releaseObject() {}
};

class CParticleMover{
private:
	ID3D11VertexShader*		m_pVS;
	ID3D11InputLayout *		m_pIL;
	UINT					m_nStride;
	ID3D11GeometryShader*	m_pGS;

public:
	CParticleMover();
	virtual ~CParticleMover() { CParticleMover::releaseObject(); }

public:
	virtual void moveParticles(	CRendererState_D3D *pRendererState,
												ID3D11Buffer* pVB_Particle,
												ID3D11Buffer* pSOTarget_Particle,
												unsigned int nOffset = 0)const;	//Return number of particles
 
	bool createObject(IFactory_ParticleMover *pFactory);
	void releaseObject() {}
};

class CParticlePainter_Spray { 
public:
	ID3D11VertexShader * m_pVS;
	ID3D11InputLayout * m_pIL;
	UINT m_nStride;
	ID3D11GeometryShader * m_pGS;
	std::vector<ID3D11Buffer *> m_pCBs_GS;

	RIMaterial_SetterOnDX *m_pMaterial;	//원본

public:
	CParticlePainter_Spray();
	virtual ~CParticlePainter_Spray() { CParticlePainter_Spray::releaseObject(); }
	 
	virtual void drawParticles(	CRendererState_D3D *pRendererState,
								ID3D11Buffer* pVB)const;

	bool createObject(IFactory_ParticlePainter *pFactory);
	void releaseObject();

};

template<class SprayPointType>
class CParticleSystem_Spray :	public IGameObject,
								virtual public RIGameObject_Drawable {
private:
	CParticleSprayPoints<SprayPointType> *m_pSprayPoint;
	CParticleMover * m_pMover;
	CParticlePainter_Spray *m_pPainter;

	std::array<ID3D11Buffer *,2> m_pBuffers;
	
	mutable ID3D11Buffer ** m_ppBuffer_Past;
	mutable ID3D11Buffer ** m_ppBuffer_Now;
public:
	CParticleSystem_Spray()
		:m_pSprayPoint(nullptr),
		m_pMover(nullptr),
		m_pPainter(nullptr),
		m_pBuffers{ nullptr, nullptr },
		m_ppBuffer_Past(&m_pBuffers[0]),
		m_ppBuffer_Now(&m_pBuffers[1])
	{
	}
	virtual ~CParticleSystem_Spray() { CParticleSystem_Spray::releaseObject(); }

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR; }

	//override RIGameObject_Drawable
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const ;
	virtual const  bool isClear() const { return true; }

	void addSprayPoint(RIGameObject_SprayParticle<SprayPointType> * pObject) { m_pSprayPoint->addObject(pObject); }
	void removeSprayPoint(RIGameObject_SprayParticle<SprayPointType> * pObject) { m_pSprayPoint->removeObject(pObject); }

	bool createObject(IFactory_ParticleSystem_Spray<SprayPointType> *pFactory);
	void releaseObject();

};


//----------------------------------- 구현부 -------------------------------------------------------------------------------------

//----------------------------------- CParticleSprayPoints -------------------------------------------------------------------------------------
template<class SprayPointType>
inline const unsigned int CParticleSprayPoints<SprayPointType>::sprayParticles(
	CRendererState_D3D *pRendererState,
	ID3D11Buffer* pSOTarget_Particle, 
	unsigned int nOffset)const	
{
	std::vector<SprayPointType> points;
	for (auto pData : m_pSprayObjects)
	{
		auto &rTmp = pData->getSprayPoints();
		points.insert(points.end(), rTmp.begin(), rTmp.end());
	}

	const unsigned int nNumberOfSprayPoints = static_cast<unsigned int>(points.size());

	CBufferUpdater::update<SprayPointType>(m_pVB_SprayPoint, points);

	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);
	pRendererState->setInputLayout(m_pIL);
	pRendererState->setVertexBuffer(m_pVB_SprayPoint, m_nStride);
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_GS(m_pGS);
	pRendererState->setConstBuffer_GS(m_pCBs_GS);
	pRendererState->setSOTarget(pSOTarget_Particle, nOffset);
	pRendererState->getDeviceContext()->Draw(nNumberOfSprayPoints, 0);

	return  nNumberOfSprayPoints;
}
template<class SprayPointType>
inline bool CParticleSprayPoints<SprayPointType>::createObject(IFactory_ParticleSprayPoints *pFactory)
{
	m_pVS = pFactory->createVS_SprayPoints();
	m_pIL = pFactory->createIL_SprayPoints();
	m_nStride = pFactory->getStride_SprayPoints();
	m_pVB_SprayPoint = pFactory->createVB_SprayPoints();
	m_pGS = pFactory->createGS_SprayPoints();
	m_pCBs_GS = pFactory->createCBs_GS_Points();

	return (m_pVS && m_pIL && m_nStride && m_pVB_SprayPoint && m_pGS);
}

//----------------------------------- CParticleSprayPoints -------------------------------------------------------------------------------------

template<class SprayPointType>
inline void CParticleSystem_Spray<SprayPointType>::drawOnDX(CRendererState_D3D * pRendererState) const
{
	auto nOffset = m_pSprayPoint->sprayParticles(pRendererState, *m_ppBuffer_Now);
	m_pMover->moveParticles(pRendererState, *m_ppBuffer_Past, *m_ppBuffer_Now, nOffset);
	m_pPainter->drawParticles(pRendererState, *m_ppBuffer_Now);

	auto tmp = m_ppBuffer_Now;
	m_ppBuffer_Now = m_ppBuffer_Past;
	m_ppBuffer_Past = tmp;
}

template<class SprayPointType>
inline bool CParticleSystem_Spray<SprayPointType>::createObject(IFactory_ParticleSystem_Spray<SprayPointType> * pFactory)
{
	m_pSprayPoint = pFactory->createParticleSprayPoints();
	m_pMover = pFactory->createParticleMover();
	m_pPainter = pFactory->createParticlePainter();
	m_pBuffers = pFactory->createParticleBuffers();

	return (m_pSprayPoint && m_pMover && m_pPainter && m_pBuffers[0] && m_pBuffers[1]);
}

template<class SprayPointType>
inline void CParticleSystem_Spray<SprayPointType>::releaseObject()
{
	if (m_pSprayPoint)	delete m_pSprayPoint;
	if (m_pMover)	delete m_pMover;
	if (m_pPainter) delete m_pPainter;
}

#endif