#pragma once
#ifndef HEADER_LIGHTMANAGER
#define HEADER_LIGHTMANAGER

#include "Interface_Object_Common.h"
#include"..\Module_DXComponent\Type_InDirectX.h"

#ifndef HEADER_STL
#include<array>
#endif // !HEADER_STL


//���漱��
struct ID3D11Buffer;

class IFactory_LightManager{
public:
	virtual ~IFactory_LightManager() = 0 {}

	virtual ID3D11Buffer * createGlobalLightBuffer() = 0;
	virtual DirectX::XMFLOAT4 getGlobalAmbient() = 0;
};


class CLightManager : virtual public ILightManager {
private:
	std::vector<ILight*> m_pLights;
	DirectX::XMFLOAT4 m_vGlobalAmbient;
	
	ID3D11Buffer *m_pGlobalLightBuffer;
public:
	CLightManager();
	virtual ~CLightManager(){}

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW; }

	//override IObject_UpdateOnDX
	void updateOnDX(CRendererState_D3D *pRendererState)const;
	
	//override ILightManager
	bool addLight(ILight *p);
	void clear() { m_pLights.clear(); }

	bool createObject(IFactory_LightManager *pFactory);
};


#endif