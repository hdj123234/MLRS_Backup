#include"stdafx.h"

#include "Light.h"

ALight::ALight(unsigned int type)
{
	m_Lights.m_nType = type;
}


CLight_Directional::CLight_Directional( )
	:ALight(LIGHTTYPE_DIRECTIONAL),
	m_rvAmbient(m_Lights.m_vAmbient),
	m_rvDiffuse(m_Lights.m_vDiffuse),
	m_rvSpecular(m_Lights.m_vSpecular),
	m_rvDirection(m_Lights.m_vDirection)
{
}


CLight_Point::CLight_Point( )
	:ALight(LIGHTTYPE_POINT),
	m_rvAmbient(m_Lights.m_vAmbient),
	m_rvDiffuse(m_Lights.m_vDiffuse),
	m_rvSpecular(m_Lights.m_vSpecular),
	m_rvPosition(m_Lights.m_vPosition),
	m_rvAttenuation(m_Lights.m_vAttenuation),
	m_rfMaxRange(m_Lights.m_vAttenuation.w)
{
}


void CLight_Point::setAttenuation(const DirectX::XMFLOAT3 & rvAttenuation)
{
	m_rvAttenuation.x = rvAttenuation.x;
	m_rvAttenuation.y = rvAttenuation.y;
	m_rvAttenuation.z = rvAttenuation.z;
}

CLight_Spot::CLight_Spot( )
	:ALight(LIGHTTYPE_SPOT),
	m_rvAmbient(m_Lights.m_vAmbient),
	m_rvDiffuse(m_Lights.m_vDiffuse),
	m_rvSpecular(m_Lights.m_vSpecular),
	m_rvPosition(m_Lights.m_vPosition),
	m_rvDirection(m_Lights.m_vDirection),
	m_rvAttenuation(m_Lights.m_vAttenuation),
	m_rfMaxRange(m_Lights.m_vAttenuation.w),
	m_rfTheta(m_Lights.m_vSpotlightData.x),
	m_rfPhi(m_Lights.m_vSpotlightData.y),
	m_rfFalloff(m_Lights.m_vSpotlightData.z)
{
}

void CLight_Spot::setAttenuation(const DirectX::XMFLOAT3 & rvAttenuation)
{
	m_rvAttenuation.x = rvAttenuation.x;
	m_rvAttenuation.y = rvAttenuation.y;
	m_rvAttenuation.z = rvAttenuation.z;
}
