
#ifndef HEADER_INTERFACE_OBJECT_COMMON
#define HEADER_INTERFACE_OBJECT_COMMON

#include"..\Module_Object\Interface_Object.h"

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL

//���漱��
class IScene;
class IRendererState;
struct Type_Light;
class ICamera;

namespace DirectX {
	struct XMFLOAT3;
}


//
//template<class TYPE>
//class IGameObject_Model_Instanced : public IGameObject,
//									public RIGameObject_Animate{
//public:
//	virtual ~IGameObject_Model_Instanced() = 0 {}
//};
//
//class IGameObject_Common :	public IGameObject,
//							public RIGameObject_Drawable,
//							public RIGameObject_Animate {
//public:
//	virtual ~IGameObject_Common() = 0 {}
//};
//

class IGameObjectDecorator : virtual public IGameObject {
public:
	virtual ~IGameObjectDecorator() = 0 {}

	virtual IGameObject *getObject()=0;
};



class ILight : virtual public IGameObject{
public:
	virtual ~ILight() = 0 {}
	virtual Type_Light const * getLight()const =0;
};

class ITerrain :	virtual public IGameObject,
					virtual public RIGameObject_ModelOrigin,
					virtual public RIGameObject_Drawable {
public:
	virtual ~ITerrain() = 0 {}
	virtual const DirectX::XMFLOAT3 getPos_Min()const = 0;
	virtual const DirectX::XMFLOAT3 getPos_Max()const = 0;
	virtual const float getHeight(float x,float z)const = 0;
};

class ISkyBox : virtual public IGameObject,
				virtual public RIGameObject_ModelOrigin,
				virtual public RIGameObject_Drawable {
public:
	virtual ~ISkyBox() = 0 {}
};

class IGameObject_Common :	virtual public IGameObject,
							virtual public RIGameObject_Drawable,
							virtual public RIGameObject_Movable,
							virtual public RIGameObject_HandleMsg,
							virtual public RIGameObject_Animate,
							virtual public RIGameObject_Collision {
public:
	virtual ~IGameObject_Common() = 0 {}
};


class ILightManager :	virtual public IGameObject,
						virtual public RIGameObject_BeMust_RenewalForDraw {
public:
	virtual ~ILightManager() = 0 {}

	virtual bool addLight(ILight *p)=0;
	virtual void clear()=0;
};

class IEffect : virtual public IGameObject,
				virtual public RIGameObject_Animate{
public:
	virtual ~IEffect() = 0 {}

	virtual const unsigned int getEffectTypeID() = 0;
};

class IEffectManager :	virtual public IGameObject,
						virtual public RIGameObject_BeMust_RenewalForDraw,
						virtual public RIGameObject_HandleMsg,
						virtual public RIGameObject_Drawable,
						virtual public RIGameObject_Animate {
public:
	virtual ~IEffectManager() = 0 {}
};

class IEffectBuilder : virtual public IMultiBuilder<IEffect>{
public:
	virtual ~IEffectBuilder() = 0 {}

	virtual void setPosition(const DirectX::XMFLOAT3 &rvPos) = 0;
};
#endif