#include"stdafx.h"
#include "Factory_Terrain_Tess.h"

#include"..\Module_Object_GameModel\Factory_Material.h"
//#include"..\Module_Object_GameModel\Factory_Mesh_Plane.h"
#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"

#include"..\MyUtility.h"
#include"..\MyINI.h"


using namespace DirectX;

//----------------------------------- CFactory_Mesh_Terrain_Tess -------------------------------------------------------------------------------------

CFactory_Mesh_Terrain_Tess::CFactory_Mesh_Terrain_Tess(
	const std::string & rsName, 
	const std::string & rsVSName,
	const std::string & rsHSName,
	const std::string & rsDSName, 
	const std::string & rsTextureFileName_HeightMap,
	const DirectX::XMFLOAT3 & rvPos, 
	const DirectX::XMFLOAT2 & rvSize, 
	const float fHeightRate)
	:CFactory_Mesh_HeightMap_Tess(rsName,rsVSName,rsHSName,rsDSName,
		static_cast<const unsigned int>(sqrt(CUtility_File::getSize(g_sDataFolder+ rsTextureFileName_HeightMap)) ),
		DirectX::XMFLOAT2(rvPos.x, rvPos.z),	rvSize),
	m_sTextureFileName_HeightMap(rsTextureFileName_HeightMap),
	m_fHeightRate(fHeightRate),
	m_vPos(rvPos)
{
}

std::vector<ID3D11Buffer*> CFactory_Mesh_Terrain_Tess::createBuffers_Transform()
{
	ID3D11Buffer* pView = factory_CB_Matrix_View.createBuffer();
	ID3D11Buffer* pProjection = factory_CB_Matrix_Projection.createBuffer();
	return std::vector<ID3D11Buffer*>({ pView,pProjection });
}

ID3D11ShaderResourceView * CFactory_Mesh_Terrain_Tess::createDisplacementMap()
{
	if (m_storeData_ForTexture.empty())
	{
		if (!createStoreData())
		{
			return nullptr;
		}
	}

	return 	CFactory_SRV_Texture2D_FromData<float>(m_sName+"_HeightMap", m_storeData_ForTexture, DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT).createShaderResourceView();
}

ID3D11SamplerState * CFactory_Mesh_Terrain_Tess::createSamplerState()
{
	return factory_SamplerState_Default.createSamplerState();
}

ID3D11Buffer * CFactory_Mesh_Terrain_Tess::createCameraBuffer()
{
	return factory_CB_CameraState.createBuffer();
}

std::vector<std::vector<float>>&& CFactory_Mesh_Terrain_Tess::getStoreData()
{
	if (m_storeData.empty())
	{
		if (!createStoreData())
		{
			m_storeData.clear();
			return std::move(m_storeData);
		}
	}
	return std::move(m_storeData);
}

unsigned int CFactory_Mesh_Terrain_Tess::getSizeOfMap()
{
	if (m_nSizeOfMap == 0)
	{
		if (!createStoreData())	return 0;
	}
	return m_nSizeOfMap;
}


bool CFactory_Mesh_Terrain_Tess::createStoreData()
{
	std::ifstream inFile(g_sDataFolder + m_sTextureFileName_HeightMap, std::ios::binary);
	if (inFile.fail())
	{
		MYLOGCAT(MYLOGCAT_ERROR_FILEOPEN, "CFactory_Mesh_Terrain_Tess.createStoreData()");
		return false;
	}
	//	inFile.unsetf(std::ios::skipws);
	inFile.seekg(0, std::ios::end);
	unsigned int  nSize_Full = static_cast<unsigned int >(inFile.tellg());
	inFile.seekg(0, std::ios::beg);

	unsigned int nSize = static_cast<unsigned int>(sqrt(nSize_Full));
	auto iter = std::istreambuf_iterator<char>(inFile.rdbuf());
	unsigned char rawData = 0;
	unsigned int indexY;
	unsigned int indexYForTexture;	//1차원 배열에서의 y인덱스값, x인덱스를 더하여 사용

	m_storeData_ForTexture.resize(nSize_Full);
	m_storeData.resize(nSize);
	for (unsigned int i = 0; i < nSize; ++i)
	{
		indexY = nSize - i - 1;	//y인덱스 반전
		indexYForTexture = i*nSize;
		m_storeData[indexY].resize(nSize);
		auto &rTmp = m_storeData[indexY];
		for (unsigned int j = 0; j < nSize; ++j)	//x인덱스 그대로 사용
		{
			rawData = 0x00 | *iter;	//char를 unsigned char로 변환하여 대입
			rTmp[j] =
				m_storeData_ForTexture[indexYForTexture + j]
				= rawData*m_fHeightRate + m_vPos.y;
			++iter;
		}
	}
	inFile.close();

	m_nSizeOfMap = nSize;
	return true;
}



//----------------------------------- AFactory_Terrain_Tess -------------------------------------------------------------------------------------

const DirectX::XMFLOAT4 AFactory_Terrain_Tess::s_vAmbient(0.4, 0.4, 0.4, 1);
const DirectX::XMFLOAT4 AFactory_Terrain_Tess::s_vDiffuse(0.8, 0.8, 0.8, 1);
const DirectX::XMFLOAT4 AFactory_Terrain_Tess::s_vSpecular(0.1, 0.1, 0.1, 1);

AFactory_Terrain_Tess::AFactory_Terrain_Tess(
	const std::string & rsName,
	const std::string &rsVSName,
	const std::string &rsHSName,
	const std::string &rsDSName,
	const std::string &rsTextureFileName_HeightMap,
	const DirectX::XMFLOAT3 & rvPos,
	const DirectX::XMFLOAT2 & rvSize,
	const float fHeightRate)
	:CFactory_Mesh_Terrain_Tess(rsName, rsVSName, rsHSName, rsDSName,
								rsTextureFileName_HeightMap,
								rvPos, rvSize, fHeightRate),
	m_vSize(rvSize)
{
}
AFactory_Terrain_Tess::AFactory_Terrain_Tess(
	const std::string & rsName,
	const std::string &rsVSName,
	const std::string &rsHSName,
	const std::string &rsDSName,
	const CMyINISection & rIniData) 
	:CFactory_Mesh_Terrain_Tess(rsName, rsVSName, rsHSName, rsDSName,
								rIniData.getParam("Map_Height"),
								rIniData.getParam_XMFLOAT3("Pos_Center"), rIniData.getParam_XMFLOAT2("Size"), rIniData.getParam_float("HeightRate")),
	m_vSize(rIniData.getParam_XMFLOAT2("Size"))
{
}
std::vector<IMesh*> AFactory_Terrain_Tess::createMeshs()
{
	CMesh_Terrain_Tess * pMesh = new CMesh_Terrain_Tess();
	if (!pMesh->createObject(this))
	{
		delete pMesh;
		return std::vector<IMesh *>();
	}
	return std::vector<IMesh *>{pMesh};
}

DirectX::XMFLOAT2 AFactory_Terrain_Tess::getUnit()
{
	DirectX::XMFLOAT2 vUnit;
	unsigned int nSizeOfMap = CFactory_Mesh_Terrain_Tess::getSizeOfMap();

	vUnit.x = m_vSize.x / (nSizeOfMap - 1);
	vUnit.y = m_vSize.y / (nSizeOfMap - 1);

	return vUnit;
}

IGameModel * AFactory_Terrain_Tess::createModel()
{
	CGameModel * pModel = new CGameModel();
	if (!pModel->createObject(this))
	{
		delete pModel;
		pModel = nullptr;
	}
	return pModel;
}


ID3D11RasterizerState * AFactory_Terrain_Tess::createRasterizerState()
{
	return factory_RasterizerState_Wire.createRasterizerState();
}

const CMyINISection AFactory_Terrain_Tess::getDefaultINISection()
{
	CMyINISection retval;
	retval.setParam("Map_Height", "Terrain/terrain_heights.raw");
	retval.setParam("Map_Diffuse", "Terrain/terrain_diffuse.jpg");
	retval.setParam("Map_Normal", "Terrain/terrain_normals.jpg");
	retval.setParam("Pos_Center", "0, 0, 0");
	retval.setParam("Size", "10000, 10000");
	retval.setParam("HeightRate", "1");
	retval.setParam("BlockSizeOfDetail", "10,10");
	return retval;
}

//----------------------------------- CFactory_Terrain_Tess -------------------------------------------------------------------------------------
const std::map<bool, std::string> CFactory_Terrain_Tess::s_PSNameMap_DeferredLighting = {std::make_pair(false,"psCommon"),std::make_pair(true,"psCommon_DeferredLighting")};


CFactory_Terrain_Tess::CFactory_Terrain_Tess(
	const std::string & rsName,
	const DirectX::XMFLOAT3 & rvPos,
	const DirectX::XMFLOAT2 & rvSize, 
	const float fHeightRate, 
	const std::string & rsTextureFileName_HeightMap,
	const std::string & rsTextureFileName_Diffuse,
	const std::string & rsTextureFileName_Normal,
	const bool bDeferredLighting )
	:AFactory_Terrain_Tess( rsName, "vsTerrainTess", "hsTerrainTess", "dsTerrainTess",rsTextureFileName_HeightMap, rvPos, rvSize, fHeightRate),
	m_Factory_Material(rsName, s_PSNameMap_DeferredLighting.at(bDeferredLighting),
		rsTextureFileName_Diffuse, rsTextureFileName_Normal,
		s_vAmbient,		s_vDiffuse,		s_vSpecular)
{
}
CFactory_Terrain_Tess::CFactory_Terrain_Tess(
	const std::string & rsName, 
	const CMyINISection & rIniData,
	const bool bDeferredLighting )
	:AFactory_Terrain_Tess(rsName, "vsTerrainTess", "hsTerrainTess", "dsTerrainTess", rIniData),
	m_Factory_Material(rsName, s_PSNameMap_DeferredLighting.at(bDeferredLighting),
		rIniData.getParam("Map_Diffuse"),  rIniData.getParam("Map_Normal"),
		s_vAmbient, s_vDiffuse, s_vSpecular)
{
}
 
std::vector<IMaterial *> CFactory_Terrain_Tess::createMaterials()
{
	CMaterial_Lambert_DiffuseNormal *pMaterial = new CMaterial_Lambert_DiffuseNormal();
	if (!pMaterial->createObject(&m_Factory_Material))
	{
		delete pMaterial;
		return std::vector<IMaterial *>();
	}
	return std::vector<IMaterial *>{pMaterial};
} 

//----------------------------------- CFactory_Terrain_Tess_Splatting -------------------------------------------------------------------------------------

const std::map<bool, std::string> CFactory_Terrain_Tess_Splatting::s_PSNameMap_DeferredLighting = { std::make_pair(true,"psTerrain_DeferredLighting") };

CFactory_Terrain_Tess_Splatting::CFactory_Terrain_Tess_Splatting(
	const std::string & rsName,
	const DirectX::XMFLOAT3 & rvPos, 
	const DirectX::XMFLOAT2 & rvSize,
	const float fHeightRate,
	const std::string & rsTextureFileName_HeightMap, 
	const std::string & rsTextureFileName_Diffuse, 
	const std::string & rsTextureFileName_Normal,
	const DirectX::XMFLOAT2 &rvBlockSizeOfDetail,
	const std::vector<std::string>& rsTextureFileNames_Detail, 
	const std::vector<std::string>& rsTextureFileNames_Alpha, 
	const bool bDeferredLighting)
	:AFactory_Terrain_Tess(rsName, "vsTerrainTess", "hsTerrainTess", "dsTerrainTess", rsTextureFileName_HeightMap, rvPos, rvSize, fHeightRate),
	m_Factory_Material(rsName, s_PSNameMap_DeferredLighting.at(bDeferredLighting),
		rsTextureFileName_Diffuse, rsTextureFileName_Normal,
		rsTextureFileNames_Detail, rsTextureFileNames_Alpha, rvBlockSizeOfDetail,
		s_vAmbient, s_vDiffuse, s_vSpecular)
{
}

CFactory_Terrain_Tess_Splatting::CFactory_Terrain_Tess_Splatting(
	const std::string & rsName,
	const CMyINISection & rIniData, 
	const std::vector<std::string>& rsTextureFileNames_Detail,
	const std::vector<std::string>& rsTextureFileNames_Alpha, 
	const bool bDeferredLighting)
	:AFactory_Terrain_Tess(rsName, "vsTerrainTess", "hsTerrainTess", "dsTerrainTess", rIniData),
	m_Factory_Material(rsName, s_PSNameMap_DeferredLighting.at(bDeferredLighting),
		rIniData.getParam("Map_Diffuse"), rIniData.getParam("Map_Normal"),
		rsTextureFileNames_Detail, rsTextureFileNames_Alpha, rIniData.getParam_XMFLOAT2("BlockSizeOfDetail"),
		s_vAmbient, s_vDiffuse, s_vSpecular) 
{
}

std::vector<IMaterial*> CFactory_Terrain_Tess_Splatting::createMaterials()
{
	CMaterial_Lambert_Splatting *pMaterial = new CMaterial_Lambert_Splatting();
	if (!pMaterial->createObject(&m_Factory_Material))
	{
		delete pMaterial;
		return std::vector<IMaterial *>();
	}
	return std::vector<IMaterial *>{pMaterial};
}
