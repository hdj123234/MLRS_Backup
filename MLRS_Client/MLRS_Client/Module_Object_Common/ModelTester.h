#pragma once

#ifndef HEADER_MODELTESTER
#define HEADER_MODELTESTER

#include"..\Module_Object_GameModel\GameModel.h"
#include"..\Module_Object\GameObject_ModelBase.h"
#include"..\Module_Object\BoundingObject.h"
#include"..\Module_Object\Factory_AnimController.h"
#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"InWorldData.h"

class CFactory_ModelTester :	virtual public IFactory_GameObject_ModelBase_Anim {
private:
	IGameModel *m_pModel;
public:
	CFactory_ModelTester(IGameModel *pModel) :m_pModel(pModel) {}
	~CFactory_ModelTester() {}
	std::vector<ID3D11Buffer *> getVSBuffer_WVP()
	{
		return std::vector<ID3D11Buffer *>{	factory_CB_Matrix_World.createBuffer(), 
											factory_CB_Matrix_View.createBuffer(), 
											factory_CB_Matrix_Projection.createBuffer()};
	}
	virtual IGameModel * getModel() { return m_pModel; }
	virtual	IAnimController * createAnimController() 
	{
		CAnimController *pResult = new CAnimController();
		if (!pResult->createObject(&CFactory_AnimController(m_pModel)))
		{
			delete pResult;
			pResult = nullptr;
		}
		return pResult;
	}

};


//----------------------------------- Class -------------------------------------------------------------------------------------


class CModelTester :	public AGameObject_ModelBase_Anim,
						public RIGameObject_ModelOrigin,
						public RIGameObject_Collision {
private:
	typedef AGameObject_ModelBase_Anim SUPER;
private:
	CGameModel_BoundingBox *m_pModel_BoundingBox;
	CInWorldData_Matrix_FromLocal m_WorldData;
	CBoundingObject_OOBB m_OOBB;
	std::vector<ID3D11Buffer *> m_pVSBuffers;

public:
	CModelTester(){	}
	virtual ~CModelTester()  { }

private:
	virtual void drawReady(CRendererState_D3D *pRendererState)const;

public:

	virtual const FLAG_8 getTypeFlag() const {	return FLAG_OBJECTTYPE_ANIMATE | FLAG_OBJECTTYPE_DRAWALBE ;}

	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const { return &m_OOBB; }

	bool createObject(CFactory_ModelTester *pFactory);
	void releaseModel() { if (m_pModel) delete m_pModel; }

	void setPos(DirectX::XMFLOAT3 &rvPos);
};


//----------------------------------- ������ -------------------------------------------------------------------------------------

inline bool CModelTester::createObject(CFactory_ModelTester *pFactory)
{
	if (!SUPER::createObject(pFactory))	return false;
	m_pVSBuffers = pFactory->getVSBuffer_WVP();
	m_pModel_BoundingBox = dynamic_cast<CGameModel_BoundingBox *>(m_pModel);
	return (m_pVSBuffers.size() == 3) && (m_pModel_BoundingBox);
}
inline void CModelTester::setPos(DirectX::XMFLOAT3 & rvPos)
{
	using namespace DirectX;
	m_WorldData.m_rvPos.x = rvPos.x; 
	m_WorldData.m_rvPos.y = rvPos.y;	
	m_WorldData.m_rvPos.z = rvPos.z;
	m_WorldData.updateWorldMatrix();
	m_OOBB.getOOBB().Center = m_pModel_BoundingBox->getCenter();
	m_OOBB.getOOBB().Center.x += m_WorldData.m_rvPos.x;
	m_OOBB.getOOBB().Center.y += m_WorldData.m_rvPos.y;
	m_OOBB.getOOBB().Center.z += m_WorldData.m_rvPos.z;
	m_OOBB.getOOBB().Extents = m_pModel_BoundingBox->getExtent();
	XMStoreFloat4(&m_OOBB.getOOBB().Orientation, XMQuaternionRotationMatrix(XMLoadFloat4x4A(&m_WorldData.m_rmWorld)));

}
inline void CModelTester::drawReady(CRendererState_D3D *pRendererState)const
{
	CBufferUpdater::update<DirectX::XMFLOAT4X4A>(pRendererState->getDeviceContext(), m_pVSBuffers[0], m_WorldData.m_rmWorld);
	pRendererState->setConstBuffer_VS(m_pVSBuffers);
}

#endif