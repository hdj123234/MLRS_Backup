#pragma once
#ifndef HEADER_FACTORY_TERRAIN_TESS
#define HEADER_FACTORY_TERRAIN_TESS

#include"Terrain.h"
#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"
#include"..\Module_Object_GameModel\Factory_Material.h"

//전방선언
struct ID3D11Buffer;
class CMyINISection;

class CFactory_Mesh_Terrain_Tess : virtual public IFactory_Mesh_Terrain_Tess,
									private CFactory_Mesh_HeightMap_Tess {
private:
	const std::string m_sName;
	const std::string m_sTextureFileName_HeightMap;

	unsigned int m_nSizeOfMap;
	std::vector<std::vector<float>> m_storeData;
	std::vector<float> m_storeData_ForTexture;

	static const std::string s_sName;

protected:
	const DirectX::XMFLOAT3 m_vPos;
	const float m_fHeightRate;

public:
	CFactory_Mesh_Terrain_Tess(const std::string &rsName,
		const std::string &rsVSName,
		const std::string &rsHSName,
		const std::string &rsDSName,
		const std::string &rsTextureFileName_HeightMap,
		const DirectX::XMFLOAT3 &rvPos,
		const DirectX::XMFLOAT2 &rvSize,
		const float fHeightRate);
	virtual ~CFactory_Mesh_Terrain_Tess() {}

protected:
	bool createStoreData();

public:
	//override IFactory_Mesh_Tess
	virtual ID3D11VertexShader	*	createVertexShader() { return CFactory_Mesh_HeightMap_Tess::createVertexShader(); }
	virtual ID3D11InputLayout	*	createInputLayout() { return CFactory_Mesh_HeightMap_Tess::createInputLayout(); }
	virtual ID3D11Buffer		*	createVertexBuffer() { return CFactory_Mesh_HeightMap_Tess::createVertexBuffer(); }
	virtual ID3D11Buffer		*	createIndexBuffer() { return CFactory_Mesh_HeightMap_Tess::createIndexBuffer(); }
	virtual unsigned int			getStride() { return CFactory_Mesh_HeightMap_Tess::getStride(); }
	virtual unsigned int			getOffset() { return CFactory_Mesh_HeightMap_Tess::getOffset(); }
	virtual unsigned int			getNumberOfVertex() { return CFactory_Mesh_HeightMap_Tess::getNumberOfVertex(); }
	virtual unsigned int			getNumberOfIndex() { return CFactory_Mesh_HeightMap_Tess::getNumberOfIndex(); }
	virtual const unsigned int		getMaterialID() { return CFactory_Mesh_HeightMap_Tess::getMaterialID(); }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST; }

	//override IFactory_Mesh_Vertex
	virtual ID3D11HullShader	*	createHullShader() { return CFactory_Mesh_HeightMap_Tess::createHullShader(); }
	virtual ID3D11DomainShader	*	createDomainShader() { return CFactory_Mesh_HeightMap_Tess::createDomainShader(); }
	
	//override IFactory_Mesh_DynamicLOD_DisplacementMapping
	virtual ID3D11ShaderResourceView	*	createDisplacementMap();
	virtual ID3D11SamplerState	*	createSamplerState() ;
	virtual ID3D11Buffer	*	createCameraBuffer() ;
	
	//ovdrride IFactory_Mesh_Terrain_Tess
	virtual std::vector<ID3D11Buffer*> createBuffers_Transform();


	std::vector<std::vector<float>> && getStoreData();	//절대 두번 호출하지 말것
	unsigned int getSizeOfMap();
};

class AFactory_Terrain_Tess :	virtual public IFactory_Terrain,
								virtual public IFactory_GameModel,
								protected CFactory_Mesh_Terrain_Tess {
private:
	const DirectX::XMFLOAT2 m_vSize;

protected:
	static const DirectX::XMFLOAT4 s_vAmbient;
	static const DirectX::XMFLOAT4 s_vDiffuse;
	static const DirectX::XMFLOAT4 s_vSpecular;

public:
	AFactory_Terrain_Tess(	const std::string &rsName,
							const std::string &rsVSName,
							const std::string &rsHSName,
							const std::string &rsDSName,
							const std::string &rsTextureFileName_HeightMap	,
							const DirectX::XMFLOAT3 &rvPos,
							const DirectX::XMFLOAT2 &rvSize,
							const float fHeightRate);
	AFactory_Terrain_Tess(	const std::string & rsName,
							const std::string &rsVSName,
							const std::string &rsHSName,
							const std::string &rsDSName,
							const CMyINISection & rIniData);
	virtual ~AFactory_Terrain_Tess() = 0 {}
	
	//override IFactory_GameModel
	virtual std::vector<IMesh			*> createMeshs();
	virtual std::vector<IMaterial		*> createMaterials()=0;
	virtual IAnimData	* createAnimData() { return nullptr; }

	//override IFactory_Terrain
	virtual DirectX::XMFLOAT3 getCenter() { return m_vPos; }
	virtual DirectX::XMFLOAT2 getRange() { return m_vSize; }
	virtual DirectX::XMFLOAT2 getUnit();
	virtual float getMaxHeight() { return m_vPos.y + m_fHeightRate * 255; }
	virtual std::vector<std::vector<float>>&&  getStoreData() { return std::move(CFactory_Mesh_Terrain_Tess::getStoreData()); }
	virtual IGameModel *  createModel();

	virtual ID3D11RasterizerState *  createRasterizerState();

	static const CMyINISection getDefaultINISection();
};

class CFactory_Terrain_Tess :	public AFactory_Terrain_Tess {
private:
	static const std::map<bool, std::string> s_PSNameMap_DeferredLighting;

private:
	CFactory_Material_Lambert_DiffuseNormal m_Factory_Material; 

public:
	CFactory_Terrain_Tess(	const std::string &rsName,
							const DirectX::XMFLOAT3 &rvPos,
							const DirectX::XMFLOAT2 &rvSize,
							const float fHeightRate,
							const std::string &rsTextureFileName_HeightMap	,
							const std::string &rsTextureFileName_Diffuse	,
							const std::string &rsTextureFileName_Normal		, 
							const bool bDeferredLighting = false);
	
	CFactory_Terrain_Tess(	const std::string & rsName, 
							const CMyINISection & rIniData, 
							const bool bDeferredLighting = false);
	virtual ~CFactory_Terrain_Tess() {}

	//override IFactory_GameModel 
	virtual std::vector<IMaterial		*> createMaterials() ; 
};

class CFactory_Terrain_Tess_Splatting : public AFactory_Terrain_Tess {
private:
	static const std::map<bool, std::string> s_PSNameMap_DeferredLighting;

private:
	CFactory_Material_Lambert_Splatting m_Factory_Material;
public:
	CFactory_Terrain_Tess_Splatting(const std::string &rsName,
									const DirectX::XMFLOAT3 &rvPos,
									const DirectX::XMFLOAT2 &rvSize,
									const float fHeightRate,
									const std::string &rsTextureFileName_HeightMap,
									const std::string &rsTextureFileName_Diffuse,
									const std::string &rsTextureFileName_Normal,
									const DirectX::XMFLOAT2 &rvBlockSizeOfDetail,
									const std::vector<std::string> &rsTextureFileNames_Detail,
									const std::vector<std::string> &rsTextureFileNames_Alpha,
									const bool bDeferredLighting = false);
	CFactory_Terrain_Tess_Splatting(const std::string & rsName,
									const CMyINISection & rIniData,
									const std::vector<std::string> &rsTextureFileNames_Detail,
									const std::vector<std::string> &rsTextureFileNames_Alpha,
									const bool bDeferredLighting = false);
	virtual ~CFactory_Terrain_Tess_Splatting() {}

	//override IFactory_GameModel 
	virtual std::vector<IMaterial		*> createMaterials();
};

#endif