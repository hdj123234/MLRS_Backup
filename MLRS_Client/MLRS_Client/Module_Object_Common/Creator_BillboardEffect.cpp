#include"stdafx.h"
#include "Creator_BillboardEffect.h"
//

#include"..\Module_Object_GameModel\Factory_Mesh_GS.h"
#include"..\Module_Object_GameModel\Factory_Material.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"


//----------------------------------- CFactory_Mesh_BillboardEffect -------------------------------------------------------------------------------------

CFactory_Mesh_BillboardEffect::CFactory_Mesh_BillboardEffect(
	const std::string &rsName,
	const std::string & rsVSName, 
	const std::string & rsGSName,
	const DirectX::XMFLOAT2 &rvSizeOfBillboard,
	const unsigned int nMaxNumberOfInstance)
	:m_sName(rsName),
	m_sVSName(rsVSName),
	m_sGSName(rsGSName),
	m_vSizeOfBillboard(rvSizeOfBillboard),
	m_nMaxNumberOfInstance(nMaxNumberOfInstance)
{
}

ID3D11VertexShader * CFactory_Mesh_BillboardEffect::createVertexShader()
{
	return CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
}

ID3D11GeometryShader * CFactory_Mesh_BillboardEffect::createGeometryShader()
{
	return CFactory_GeometryShader(m_sGSName, m_sGSName).createShader();
}

ID3D11InputLayout * CFactory_Mesh_BillboardEffect::createInputLayout()
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputElementDescs = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXINDEX", 0, DXGI_FORMAT_R32_UINT,		  0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } };


	return CFactory_InputLayout(m_sVSName, m_sVSName,
		"IL_" + m_sName,
		inputElementDescs)
		.createInputLayout();
}

ID3D11Buffer * CFactory_Mesh_BillboardEffect::createVertexBuffer()
{
	return CFactory_VertexBuffer<Type_InstanceData_BillboardEffect>("VB_" + m_sName, m_nMaxNumberOfInstance,D3D11_USAGE_DYNAMIC,D3D11_CPU_ACCESS_WRITE).createBuffer();
}

std::vector<ID3D11Buffer*> CFactory_Mesh_BillboardEffect::createGSBuffers()
{
	ID3D11Buffer* pCBuffer_View = factory_CB_Matrix_View.createBuffer();
	ID3D11Buffer* pCBuffer_Projection = factory_CB_Matrix_Projection.createBuffer();
	ID3D11Buffer* pCBuffer_BillboardSize = CFactory_ConstBuffer_NonRenewal<Type_BillboardSize>("CB_"+m_sName+"_BillboardSize").createBuffer(&m_vSizeOfBillboard);

	return std::vector<ID3D11Buffer*>{pCBuffer_View, pCBuffer_Projection, pCBuffer_BillboardSize};
}


//std::vector<IMesh*> CFactory_Model_BillboardEffect_SoftParticle::createMeshs()
//{
//}

//----------------------------------- CFactory_Model_BillboardEffect -------------------------------------------------------------------------------------

CFactory_Model_BillboardEffect::CFactory_Model_BillboardEffect(
	const std::string & rsName,
	const DirectX::XMFLOAT2 & rvSizeOfBillboard,
	const unsigned int nMaxNumberOfInstance,
	const std::vector<std::string>& rsTextureFileNames,
	const std::string & rsVSName,
	const std::string & rsGSName,
	const std::string & rsPSName)
	:m_sName(rsName),
	m_Factory_Mesh(rsName, rsVSName, rsGSName, rvSizeOfBillboard, nMaxNumberOfInstance),
	m_Factory_Material(rsName, rsPSName, rsTextureFileNames)
{
}
CMesh_Billboard<Type_InstanceData_BillboardEffect>* CFactory_Model_BillboardEffect::createMesh()
{
	CMesh_BillboardEffect *pMesh = new CMesh_BillboardEffect;
	if (!pMesh->createObject(&m_Factory_Mesh))
		return nullptr;

	return pMesh;
}

IMaterial * CFactory_Model_BillboardEffect::createMaterial()
{
	CMaterial_NoneLighting_Diffuse *pMaterial = new CMaterial_NoneLighting_Diffuse;
	if (!pMaterial->createObject(&m_Factory_Material))
	{
		delete pMaterial;
		return nullptr;
	}

	return pMaterial;
}


ID3D11DepthStencilState * CFactory_Model_BillboardEffect::createDepthStencilState()
{
	return factory_DepthStencilState_CheckOnly.createDepthStencilState();
}

ID3D11BlendState * CFactory_Model_BillboardEffect::createBlendState()
{
	return factory_BlendState_Effect.createBlendState();
}

ID3D11RasterizerState * CFactory_Model_BillboardEffect::createRasterizerState()
{
	return factory_RasterizerState_DisableCulling.createRasterizerState();
}

//----------------------------------- CFactory_Model_BillboardEffect_SoftParticle -------------------------------------------------------------------------------------

CFactory_Model_BillboardEffect_SoftParticle::CFactory_Model_BillboardEffect_SoftParticle(
	const std::string & rsName,
	const DirectX::XMFLOAT2 & rvSizeOfBillboard,
	const unsigned int nMaxNumberOfInstance,
	const std::vector<std::string>& rsTextureFileNames,
	const std::string & rsVSName,
	const std::string & rsGSName,
	const std::string & rsPSName)
	:CFactory_Model_BillboardEffect(rsName, rvSizeOfBillboard, nMaxNumberOfInstance, rsTextureFileNames, rsVSName,rsGSName,rsPSName),
	m_Factory_Material(rsName, rsPSName, rsTextureFileNames)
{
}
IMaterial* CFactory_Model_BillboardEffect_SoftParticle::createMaterial()
{
	CMaterial_NoneLighting_Diffuse_DepthBiasBlending *pMaterial = new CMaterial_NoneLighting_Diffuse_DepthBiasBlending;
	if (!pMaterial->createObject(&m_Factory_Material))
	{
		delete pMaterial;
		return nullptr;
	}

	return pMaterial;
}


//----------------------------------- CFactory_BillboardEffect_Instance -------------------------------------------------------------------------------------

CFactory_BillboardEffect_Instance::CFactory_BillboardEffect_Instance(
	CModel_This * pModel,
	const DirectX::XMFLOAT3 & rvPos, 
	const unsigned int nMaxNumberOfAnimation, 
	const float fFPS, 
	const unsigned int nEffectTypeID)
	:m_pModel(pModel),
	m_vPos(rvPos),
	m_nMaxNumberOfAnimation(nMaxNumberOfAnimation),
	m_fFPS(fFPS),
	m_nEffectTypeID(nEffectTypeID)
{
}

CBuilder_BillboaardEffect::CBuilder_BillboaardEffect(
	CModel_This * pModel,
	const unsigned int nMaxNumberOfAnimation,
	const float fFPS,
	const unsigned int nEffectTypeID)
	:m_pModel(pModel),
	m_vPos(0,0,0),
	m_nMaxNumberOfAnimation(nMaxNumberOfAnimation),
	m_fFPS(fFPS),
	m_nEffectTypeID(nEffectTypeID)
{
}

IEffect * CBuilder_BillboaardEffect::build()
{
	CInstance_BillboardEffect *pBillboardEffect = new CInstance_BillboardEffect;
	if (!pBillboardEffect->createObject(&CFactory_BillboardEffect_Instance(m_pModel, m_vPos, m_nMaxNumberOfAnimation, m_fFPS, m_nEffectTypeID)))
	{
		delete pBillboardEffect;
		return nullptr;
	}
	pBillboardEffect->enable();
	return pBillboardEffect;
}
