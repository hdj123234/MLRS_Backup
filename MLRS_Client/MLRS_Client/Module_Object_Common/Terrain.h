#pragma once
#ifndef HEADER_TERRAIN
#define HEADER_TERRAIN

#include "Interface_Object_Common.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_GameModel\Mesh_DynamicLOD.h"
#include "..\Module_Object_GameModel\GameModel.h"


#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif 

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif 

//���漱��
class CRendererState_D3D;
class IMaterial;

//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Mesh_Terrain_Tess : virtual public IFactory_Mesh_DynamicLOD_DisplacementMapping {
public:
	IFactory_Mesh_Terrain_Tess() {}
	virtual ~IFactory_Mesh_Terrain_Tess()=0 {}

	//pure virtual func
	virtual std::vector<ID3D11Buffer*> createBuffers_Transform() = 0;
};

class IFactory_Terrain   {
public:
	virtual ~IFactory_Terrain() = 0 {}

	virtual DirectX::XMFLOAT3 getCenter()=0;
	virtual DirectX::XMFLOAT2 getRange()=0;
	virtual DirectX::XMFLOAT2 getUnit() = 0;
	virtual float getMaxHeight() = 0;
	virtual std::vector<std::vector<float>>&&  getStoreData() = 0;
	virtual IGameModel *  createModel() = 0;
	virtual ID3D11RasterizerState *  createRasterizerState() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

class CMesh_Terrain_Tess :public CMesh_DynamicLOD_DisplacementMapping {
private:
	std::vector<ID3D11Buffer*> m_pBuffers_Transform;

	virtual void setOnDX_InputAssembler(CRendererState_D3D *pRendererState)const;
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;
public:
	CMesh_Terrain_Tess() {}
	virtual ~CMesh_Terrain_Tess() { }

	bool createObject(IFactory_Mesh_Terrain_Tess *pFactory);
};

class CTerrain :	virtual public ITerrain,
					private AGameObject_ModelBase {
private:
	DirectX::XMFLOAT3 m_vCenter;
	DirectX::XMFLOAT2 m_vRange;
	DirectX::XMFLOAT2 m_vUnit;
	float m_fMaxHeight;
	std::vector<std::vector<float>> m_StoreData;
	ID3D11RasterizerState * m_pRasterizerState;

	bool m_bDrawWire;

public:
	CTerrain();
	virtual ~CTerrain() { CTerrain::releaseModel(); }

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_DRAWALBE; }

	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;
	
	//override RIGameObject_Drawable
	virtual const bool isClear() const { return AGameObject_ModelBase::isClear(); }

	//override ITerrain
	virtual const DirectX::XMFLOAT3 getPos_Min()const;
	virtual const DirectX::XMFLOAT3 getPos_Max()const;
	virtual const float getHeight(float x, float z)const;

	bool createObject(IFactory_Terrain *pFactory);
	void releaseModel();

	void setWire(bool bOn) { m_bDrawWire = bOn; }
};

#endif