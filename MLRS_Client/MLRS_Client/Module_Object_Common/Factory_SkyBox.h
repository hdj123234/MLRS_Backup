#pragma once
#ifndef HEADER_FACTORY_SKYBOX
#define HEADER_FACTORY_SKYBOX
#include"SkyBox.h"

#include"..\Module_Object_GameModel\Factory_Mesh_GS.h"
#include"..\Module_Object_GameModel\Factory_Material.h"

class CFactory_Mesh_SkyBox : virtual public IFactory_Mesh_SkyBox,
							protected CFactory_Mesh_GS{
public:
	CFactory_Mesh_SkyBox(const std::string &rsGSName);
	virtual ~CFactory_Mesh_SkyBox() {}

	virtual ID3D11VertexShader		* createVertexShader() { return CFactory_Mesh_GS::createVertexShader(); }
	virtual ID3D11GeometryShader	* createGeometryShader() { return CFactory_Mesh_GS::createGeometryShader(); }
	virtual std::vector<ID3D11Buffer*> createGSBuffers() ;

	virtual const unsigned int getMaterialID() { return CFactory_Mesh_GS::getMaterialID(); }
};

class CFactory_Model_SkyBox : virtual public IFactory_Model_SkyBox,
								protected CFactory_Mesh_SkyBox  {
private:
	const std::string m_sPSName;
	const std::string m_sTextureFileName;
	CFactory_Material_NoneLighting_Diffuse m_Factory_Material;

	static const std::string s_sName;
public:
	CFactory_Model_SkyBox(	const std::string &rsTextureFileName,
							const std::string &rsGSName, 
							const std::string &rsPSName);
	virtual ~CFactory_Model_SkyBox() {}


	//override IFactory_GameModel
	virtual std::vector<IMesh			*> createMeshs() ;
	virtual std::vector<IMaterial		*> createMaterials();
	virtual IAnimData	* createAnimData() { return nullptr; }
};

class CFactory_SkyBox : virtual public IFactory_SkyBox,
						private CFactory_Model_SkyBox {
public:
	CFactory_SkyBox(const std::string &rsTextureFileName,
					const std::string &rsGSName = std::string("gsSkyBox"),
					const std::string &rsPSName = std::string("psSkyBox"));
	virtual ~CFactory_SkyBox() {}

	//override IFactory_SkyBox
	virtual IGameModel *		createSkyBoxModel();
	virtual ID3D11DepthStencilState*	createDepthStencilState();
};

#endif