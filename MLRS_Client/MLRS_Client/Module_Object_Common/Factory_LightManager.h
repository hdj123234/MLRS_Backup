#pragma once
#ifndef HEADER_FACTORY_LIGHTMANAGER
#define HEADER_FACTORY_LIGHTMANAGER
#include"LightManager.h"

//���漱��
class CMyINISection;

class CFactory_LightManager : virtual public IFactory_LightManager {
	DirectX::XMFLOAT4 m_vGlobalAmbient;
public:
	CFactory_LightManager(const DirectX::XMFLOAT4 &vGlobalAmbient);
	virtual ~CFactory_LightManager()  {}

	virtual ID3D11Buffer * createGlobalLightBuffer() ;
	virtual DirectX::XMFLOAT4 getGlobalAmbient() ;

	static const CMyINISection getDefaultINISection_GlobalLight();
};

#endif