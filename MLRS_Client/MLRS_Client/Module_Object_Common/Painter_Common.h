#pragma once
/*디버깅용
*/

#ifndef HEADER_DUMMYPAINTER
#define HEADER_DUMMYPAINTER

#include "Interface_Object_Common.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"
#include "..\Module_Object_GameModel\Material.h"


#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif 

//전방선언
class CRendererState_D3D;
class IMaterial;
struct Type_Bone;





//----------------------------------- BoundingBox -------------------------------------------------------------------------------------

//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Painter_BoundingBox : virtual public IFactory_Mesh_GS {
public:
	virtual ~IFactory_Painter_BoundingBox() = 0 {}

	virtual std::vector<ID3D11Buffer *> createGSBuffers() = 0;
	virtual ID3D11RasterizerState * createRasterizerState() = 0;
	virtual CMaterial_None getMaterial() = 0;
};


//----------------------------------- Class -------------------------------------------------------------------------------------

class CPainter_BoundingBox :	public CMesh_GS,
								virtual public IGameObject,
								virtual public RIGameObject_Drawable {
private:
	struct Type_BoundingBox {
		DirectX::XMFLOAT3 m_vPos; char dummy1[4];
		DirectX::XMFLOAT3 m_vExtent; char dummy2[4];
		DirectX::XMFLOAT4X4 m_mRotation;
	};
private:
	std::vector<ID3D11Buffer*> m_pGSBuffers;
	CMaterial_None m_Material;
	DirectX::BoundingOrientedBox m_BoundingBox;
	ID3D11RasterizerState *m_pRasterizerState;

public:
	CPainter_BoundingBox() {}
	virtual ~CPainter_BoundingBox(){}

private:
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:

	virtual void setBoundingBox(const DirectX::BoundingOrientedBox &rBoundingBox) {m_BoundingBox = rBoundingBox;}

	bool createObject(IFactory_Painter_BoundingBox *pFactory);


	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;
	virtual const  bool isClear() const { return false; }
	
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_DRAWALBE; } 
};

class CPainter_BoundingBoxManager :virtual public IGameObject,
									virtual public RIGameObject_Drawable{
private:
	std::vector<CPainter_BoundingBox> m_BoundingBox;
public:
	void addBoundingBox(const DirectX::BoundingOrientedBox &rBoundingBox);
	void clearBoundingBox() { m_BoundingBox.clear(); }

	virtual void drawOnDX(CRendererState_D3D *pRendererState)const { for (auto &rData : m_BoundingBox) rData.drawOnDX(pRendererState); }
	virtual const  bool isClear() const { return true; }
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_DRAWALBE; }
};

//----------------------------------- Line -------------------------------------------------------------------------------------

//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Painter_Line : virtual public IFactory_Mesh_GS {
public:
	virtual ~IFactory_Painter_Line() = 0 {}

	virtual std::vector<ID3D11Buffer *> createGSBuffers() = 0;
	virtual ID3D11RasterizerState * createRasterizerState() = 0;
	virtual CMaterial_None getMaterial() = 0;
};


//----------------------------------- Class -------------------------------------------------------------------------------------

class CPainter_Line : public CMesh_GS,
	virtual public IGameObject,
	virtual public RIGameObject_Drawable ,
	virtual public RIGameObject_Animate {
private:
	struct Type_Line {
		DirectX::XMFLOAT3 m_vPos; char dummy1[4];
		DirectX::XMFLOAT3 m_vTarget; char dummy2[4];
	};
private:
	std::vector<ID3D11Buffer*> m_pGSBuffers;
	CMaterial_None m_Material;
	DirectX::XMFLOAT3 m_vPos; 
	DirectX::XMFLOAT3 m_vTarget;
	ID3D11RasterizerState *m_pRasterizerState;

	bool m_bEnable;
	float m_fTime;
public:
	CPainter_Line():m_bEnable(false){}
	virtual ~CPainter_Line(){}

private:
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	virtual void setLine(const DirectX::XMFLOAT3 &rvPos, const DirectX::XMFLOAT3 &rvTarget);

	bool createObject(IFactory_Painter_Line *pFactory);
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;
	virtual const  bool isClear() const { return false; }
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_DRAWALBE| FLAG_OBJECTTYPE_ANIMATE; }

	virtual std::vector<IMsg *> animateObject(const float fElapsedTime) ;

	void enable() { m_bEnable = true; }
	void disable() { m_bEnable = false; }
};

//----------------------------------- Curve -------------------------------------------------------------------------------------

//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Painter_Curve : virtual public IFactory_Mesh_Vertex {
public:
	virtual ~IFactory_Painter_Curve() = 0 {}
	 
	virtual ID3D11RasterizerState * createRasterizerState() = 0;
	virtual std::vector<ID3D11Buffer *> createCBs_VS() = 0;
	virtual CMaterial_NoneLighting_ColorOnly getMaterial() = 0;
};


//----------------------------------- Class -------------------------------------------------------------------------------------

class CPainter_Curve : public CMesh_Vertex,
	virtual public IGameObject,
	virtual public RIGameObject_Drawable {

private:
	CMaterial_NoneLighting_ColorOnly m_Material;
	std::vector<ID3D11Buffer*> m_pCBs_VS;
	std::vector<DirectX::XMFLOAT3> m_vVertices;
	ID3D11RasterizerState *m_pRasterizerState;

	bool m_bEnable; 
public:
	CPainter_Curve() :m_bEnable(false) {}
	virtual ~CPainter_Curve() {}

private:
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	virtual void setCurve(std::vector<DirectX::XMFLOAT3> &&rvrvVertices);

	bool createObject(IFactory_Painter_Curve *pFactory);
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;
	virtual const  bool isClear() const { return true; }
	virtual const FLAG_8 getTypeFlag() const { return static_cast<char>(m_bEnable)*( FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_ANIMATE); }
	 
	void enable() { m_bEnable = true; }
	void disable() { m_bEnable = false; }
	const bool isEnable()const { return m_bEnable; }
};

//----------------------------------- CirclePlate -------------------------------------------------------------------------------------

//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Painter_CirclePlate : virtual public IFactory_Mesh_VertexAndGS {
public:
	virtual ~IFactory_Painter_CirclePlate() = 0 {}
	 
	virtual ID3D11BlendState * createBlendState() = 0;
	virtual ID3D11RasterizerState * createRasterizerState() = 0;
	virtual ID3D11DepthStencilState * createDepthStencilState() = 0;
	virtual std::vector<ID3D11Buffer *> createCBs_GS() = 0;
	virtual CMaterial_NoneLighting_ColorOnly getMaterial() = 0;
	virtual const float getRadius() = 0;
};


//----------------------------------- Class -------------------------------------------------------------------------------------

class CPainter_CirclePlate :	public CMesh_VertexAndGS,
								virtual public IGameObject,
								virtual public RIGameObject_Drawable {

private:
	CMaterial_NoneLighting_ColorOnly m_Material;
	DirectX::XMFLOAT4 m_vTargetAndRadius;
	ID3D11RasterizerState *m_pRasterizerState;
	ID3D11BlendState *m_pBlendState;
	ID3D11DepthStencilState *m_pDepthStencilState;
	std::vector<ID3D11Buffer*> m_pCBs_GS;
	float m_fRadius;

	bool m_bEnable;
public:
	CPainter_CirclePlate() :m_bEnable(false) {}
	virtual ~CPainter_CirclePlate() {}

private:
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;

	virtual const  bool isClear() const { return true; }
	virtual const FLAG_8 getTypeFlag() const { return static_cast<char>(m_bEnable)*(FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_ANIMATE); }
	
	bool createObject(IFactory_Painter_CirclePlate *pFactory);

	void enable() { m_bEnable = true; }
	void disable() { m_bEnable = false; }
	const bool isEnable()const { return m_bEnable; }
	 
	void setTarget(const DirectX::XMFLOAT3 &rvTarget);
};




//----------------------------------- Bone -------------------------------------------------------------------------------------


//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Mesh_Bone : public IFactory_Mesh_GS {
public:
	virtual ~IFactory_Mesh_Bone() = 0 {}

	virtual ID3D11PixelShader	* createPixelShader() = 0;

	virtual ID3D11ShaderResourceView	* getBoneStructure() = 0;
	virtual ID3D11ShaderResourceView	* getAnimData() = 0;
	virtual ID3D11ShaderResourceView	* getAnimIndicator() = 0;
	virtual ID3D11Buffer				*getAnimUpdater() = 0;
	virtual const unsigned int 	getNumberOfBone() = 0;
	virtual const unsigned int 	getNumberOfFrame() = 0;
	virtual std::vector<ID3D11Buffer *> createVP() = 0;
	virtual ID3D11DepthStencilState * createDepthStencilState() = 0;
	virtual ID3D11BlendState *createBlendState() = 0;

	virtual const unsigned int getMaterialID() = 0;
};

class CMesh_Painter_Bone : virtual public CMesh_GS {
private:
	unsigned int m_nNumberOfBone;
	ID3D11ShaderResourceView *m_pBoneStructure;
	ID3D11ShaderResourceView *m_pAnimData;
	ID3D11ShaderResourceView *m_pAnimIndicator;
	ID3D11PixelShader	*m_pPixelShader;
	std::vector<ID3D11Buffer *>m_pVP;
	ID3D11Buffer *m_pBuffer_AnimIndicator_Updater;
	ID3D11DepthStencilState * m_pDepthStencilState;
	ID3D11BlendState * m_pBlendStateState;
	std::vector<unsigned int> m_Indicator;
	unsigned int m_nNumberOfFrame;

	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;
public:
	CMesh_Painter_Bone();
	virtual ~CMesh_Painter_Bone() {}

	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D* pRendererState)const;

	bool createObject(IFactory_Mesh_Bone *pFactory);

	void advanceFrame();
};

class CPainter_Bone : virtual public IGameObject,
	virtual public RIGameObject_Drawable,
	virtual public RIGameObject_Animate {
private:
	CMesh_Painter_Bone *m_pMesh_Bone;
	float m_fETime;

public:
	CPainter_Bone() :m_pMesh_Bone(nullptr), m_fETime(0) {}
	virtual ~CPainter_Bone() { if (m_pMesh_Bone)delete m_pMesh_Bone; }

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR | FLAG_OBJECTTYPE_ANIMATE; }

	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const { m_pMesh_Bone->setOnDX(pRendererState); m_pMesh_Bone->drawOnDX(pRendererState); }

	//override RIGameObject_Drawable
	virtual const bool isClear() const { return true; }

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	bool createObject(IFactory_Mesh_Bone *pFactory);

};

#endif