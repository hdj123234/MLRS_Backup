#pragma once
#ifndef HEADER_FACTORY_BILLBOARDEFFECT
#define HEADER_FACTORY_BILLBOARDEFFECT

#include "BillboardEffect.h"

#include"..\Module_Object_GameModel\Factory_Mesh_GS.h"
#include"..\Module_Object_GameModel\Factory_Material.h"

class CFactory_Mesh_BillboardEffect : virtual public IFactory_Mesh_Billboard{
private:
	const std::string m_sName;
	const std::string m_sVSName;
	const std::string m_sGSName;
	const DirectX::XMFLOAT2 m_vSizeOfBillboard;
	const unsigned int m_nMaxNumberOfInstance;
public:
	CFactory_Mesh_BillboardEffect(	const std::string &rsName, 
									const std::string &rsVSName,
									const std::string &rsGSName,
									const DirectX::XMFLOAT2 &rvSizeOfBillboard,
									const unsigned int nMaxNumberOfInstance);
	virtual ~CFactory_Mesh_BillboardEffect() {}

public:
	//override IFactory_Mesh
	virtual ID3D11VertexShader		* createVertexShader()  ;

	//override IFactory_Mesh_GS
	virtual ID3D11GeometryShader	* createGeometryShader();

	//override RIFactory_Mesh_VertexBuffer
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST; }
	virtual ID3D11InputLayout	*	createInputLayout() ;
	virtual ID3D11Buffer		*	createVertexBuffer() ;
	virtual unsigned int			getStride() { return sizeof(Type_InstanceData_BillboardEffect); }

	//override IFactory_Mesh_Billboard
	virtual std::vector<ID3D11Buffer*> createGSBuffers() ;
	virtual const unsigned int getMaxNumberOfInstance() { return m_nMaxNumberOfInstance; }

// ��Ȱ��ȭ
private:
	//override IFactory_Mesh
	virtual const unsigned int getMaterialID() { return 0; }

	//override RIFactory_Mesh_VertexBuffer
	virtual unsigned int			getOffset() { return 0; }
	virtual unsigned int			getNumberOfVertex() { return 0; }
};

class CFactory_Model_BillboardEffect: public IFactory_Model_Billboard<Type_InstanceData_BillboardEffect> {
private:
	const std::string m_sName;
	CFactory_Mesh_BillboardEffect m_Factory_Mesh;
	CFactory_Material_NoneLighting_DiffuseArray m_Factory_Material;
public:
	CFactory_Model_BillboardEffect(const std::string &rsName,
		const DirectX::XMFLOAT2 &rvSizeOfBillboard,
		const unsigned int nMaxNumberOfInstance,
		const std::vector<std::string> &rsTextureFileNames,
		const std::string &rsVSName = "vsBillboard_Effect",
		const std::string &rsGSName = "gsBillboard_Effect",
		const std::string &rsPSName = "psBillboard_Effect");
	virtual ~CFactory_Model_BillboardEffect() {}


	//override IFactory_Model_Billboard
	virtual CMesh_Billboard<Type_InstanceData_BillboardEffect> * createMesh();
	//	virtual std::vector<IMesh			*> createMeshs() ;
	virtual IMaterial		* createMaterial();
	virtual IAnimData	* createAnimData() { return nullptr; }

	virtual ID3D11DepthStencilState		* createDepthStencilState();
	virtual ID3D11BlendState		* createBlendState();
	virtual ID3D11RasterizerState		* createRasterizerState();
	 
};


class CFactory_Model_BillboardEffect_SoftParticle : public CFactory_Model_BillboardEffect {
private: 
	CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending m_Factory_Material;

public:
	CFactory_Model_BillboardEffect_SoftParticle(	const std::string &rsName		,
									const DirectX::XMFLOAT2 &rvSizeOfBillboard	,
									const unsigned int nMaxNumberOfInstance	,
									const std::vector<std::string> &rsTextureFileNames,
									const std::string &rsVSName="vsBillboard_Effect",
									const std::string &rsGSName="gsBillboard_Effect",
									const std::string &rsPSName="psBillboard_Effect_SoftParticle");
	virtual ~CFactory_Model_BillboardEffect_SoftParticle() {}


	//override IFactory_Model_Billboard 
	virtual IMaterial		* createMaterial(); 
	 
};

class CFactory_BillboardEffect_Instance : virtual public IFactory_BillboardEffect {
private:
	typedef Type_InstanceData_BillboardEffect  InstanceData;
	typedef CModel_Billboard<Type_InstanceData_BillboardEffect> CModel_This;

private:
	CModel_This * m_pModel;
	const DirectX::XMFLOAT3 m_vPos;

	const unsigned int m_nMaxNumberOfAnimation;
	const float m_fFPS;
	const unsigned int m_nEffectTypeID;

public:
	CFactory_BillboardEffect_Instance(	CModel_This *pModel,
										const DirectX::XMFLOAT3 &rvPos,
										const unsigned int nMaxNumberOfAnimation,
										const float fFPS,
										const unsigned int nEffectTypeID);
	virtual ~CFactory_BillboardEffect_Instance() {}


	//override  IFactory_Billboard_Instance<Type_InstanceData_BillboardEffect>
	virtual CModel_This *getModel() { return m_pModel; }
	virtual InstanceData getData() { return InstanceData(m_vPos); }

	//override IFactory_BillboardEffect
	virtual const unsigned int getMaxNumberOfAnimation() { return m_nMaxNumberOfAnimation; }
	virtual const float getFPS() { return m_fFPS; }
	virtual const unsigned int getEffectTypeID() { return m_nEffectTypeID; }
};

class CBuilder_BillboaardEffect : virtual public IEffectBuilder {
private:
	typedef CModel_Billboard<Type_InstanceData_BillboardEffect> CModel_This;

private:
	CModel_This *m_pModel;
	DirectX::XMFLOAT3 m_vPos;
	const unsigned int m_nMaxNumberOfAnimation;
	const float m_fFPS;
	const unsigned int m_nEffectTypeID;

public:
	CBuilder_BillboaardEffect(	CModel_This *pModel,
								const unsigned int nMaxNumberOfAnimation,
								const float fFPS,
								const unsigned int nEffectTypeID);
	virtual ~CBuilder_BillboaardEffect() {}

	//override IEffectBuilder
	virtual void setPosition(const DirectX::XMFLOAT3 &rvPos) { m_vPos = rvPos; }

	//override IBuilder<IEffect>
	virtual IEffect * build() ;
};



#endif