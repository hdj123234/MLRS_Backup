#pragma once

#ifndef HEADER_SKYBOX
#define HEADER_SKYBOX

#include "Interface_Object_Common.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"


#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif 

//���漱��
class CRendererState_D3D;

//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Mesh_SkyBox : virtual public IFactory_Mesh_GS {
public:
	virtual ~IFactory_Mesh_SkyBox() = 0 {}

	//IFactory_Mesh_GS
	virtual ID3D11VertexShader		* createVertexShader() = 0;
	virtual ID3D11GeometryShader	* createGeometryShader() = 0;
	virtual const unsigned int getMaterialID() = 0;

	virtual std::vector<ID3D11Buffer*> createGSBuffers()=0;
};

class IFactory_Model_SkyBox : virtual public IFactory_GameModel {
public:
	virtual ~IFactory_Model_SkyBox() = 0 {}

};

class IFactory_SkyBox {
public:
	virtual ~IFactory_SkyBox()=0 {}

	virtual IGameModel *		createSkyBoxModel() = 0;
	virtual ID3D11DepthStencilState *createDepthStencilState() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

class CMesh_SkyBox :public CMesh_GS{
private:
	std::vector<ID3D11Buffer*> m_pGSBuffers;

	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;
public:
	CMesh_SkyBox() {}
	virtual ~CMesh_SkyBox() {  }

	bool createObject(IFactory_Mesh_SkyBox *pFactory);
};

class CModel_SkyBox :public CGameModel {
private:
public:
	CModel_SkyBox();
	virtual ~CModel_SkyBox() {  }

	bool createObject(IFactory_Model_SkyBox *pFactory);
	
	//override IGameModel
	virtual const bool isClear()const { return false; }
};

class CSkyBox : virtual public ISkyBox,
				private AGameObject_ModelBase {
private:
	ID3D11DepthStencilState *m_pDepthStencilState;

public:
	CSkyBox();
	virtual ~CSkyBox() { CSkyBox::releaseModel(); }

private:
	virtual void drawReady(CRendererState_D3D *pRendererState)const;
	virtual void resetForDraw(CRendererState_D3D *pRendererState)const;

public:
	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return FLAG_OBJECTTYPE_DRAWALBE; }
	virtual IMsg* handleMsg(IMsg*) { return nullptr; }

	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const { AGameObject_ModelBase::drawOnDX(pRendererState); }
	
	//override RIGameObject_Drawable
	virtual const bool isClear() const {return  AGameObject_ModelBase::isClear(); }

	bool createObject(IFactory_SkyBox *pFactory);
	void releaseModel();
};
#endif