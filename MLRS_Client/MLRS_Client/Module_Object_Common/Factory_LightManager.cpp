#include"stdafx.h"
#include "Factory_LightManager.h"

#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\MyINI.h"

CFactory_LightManager::CFactory_LightManager(const DirectX::XMFLOAT4 & vGlobalAmbient)
	:m_vGlobalAmbient(vGlobalAmbient)
{
}

ID3D11Buffer * CFactory_LightManager::createGlobalLightBuffer()
{
	ID3D11Buffer *pBuffer = factory_CB_GlobalLighting.createBuffer();
	return pBuffer;
}

DirectX::XMFLOAT4 CFactory_LightManager::getGlobalAmbient()
{
	return m_vGlobalAmbient;
}

const CMyINISection CFactory_LightManager::getDefaultINISection_GlobalLight()
{
	CMyINISection retval;
	retval.setParam("GlobalAmbient", "0.1, 0.1, 0.1, 1");
	return retval;
}
