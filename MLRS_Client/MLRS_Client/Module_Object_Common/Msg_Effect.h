#pragma once
#ifndef HEADER_MSG_EFFECT
#define HEADER_MSG_EFFECT

#include "..\Module_Object\Interface_Object.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif

class IEffect;

enum EMsgType_Effect :ENUMTYPE_256{
	eCreate,
	eRemove
};


class CMsg_Effect : public IMsg_GameObject {
private:
	EMsgType_Effect m_Type;
public:
	CMsg_Effect(const EMsgType_Effect type)
		:m_Type(type){}
	virtual ~CMsg_Effect() {}
	
	virtual const EMsgType_Effect getMsgType()const { return m_Type; }
};

class CMsg_Effect_Create : public CMsg_Effect {
private:
	const unsigned int m_nEffectTypeID;
	const DirectX::XMFLOAT3 m_vPosition;
public:
	CMsg_Effect_Create(const unsigned int nEffectTypeID,
	const DirectX::XMFLOAT3 &rvPosition)
		:CMsg_Effect(eCreate),
		m_nEffectTypeID(nEffectTypeID),
		m_vPosition(rvPosition){}
	virtual ~CMsg_Effect_Create() {}

	unsigned int getTypeID() { return m_nEffectTypeID; }
	const DirectX::XMFLOAT3 getPosition() { return m_vPosition; }
};


class CMsg_Effect_Remove : public CMsg_Effect {
private:
	IEffect * m_pTarget;
public:
	CMsg_Effect_Remove(IEffect *pTarget)
		:CMsg_Effect(eRemove),
		m_pTarget(pTarget){}
	virtual ~CMsg_Effect_Remove() {}

	IEffect *getEvent() { return m_pTarget; }
};



#endif