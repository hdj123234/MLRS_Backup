#include "stdafx.h"
#include "BillboardEffect.h"

#include "Msg_Effect.h"

CBillboard_Instance_TexAnim::CBillboard_Instance_TexAnim()
	:m_fElapsedTime(0),
	m_nMaxNumberOfAnimation(0),
	m_fFPS(0),
	m_nEffectTypeID(0)
{
}

//void CBillboard_Instance_TexAnim::disable()
//{
////	m_fElapsedTime = 0.0f;
////	m_Data.m_nAnimIndex = 0;
//	CBillboard_Instance::disable();
//}

std::vector<IMsg*> CBillboard_Instance_TexAnim::animateObject(const float fElapsedTime)
{
	std::vector<IMsg *> result;
	if (m_bEnable)
	{
		m_fElapsedTime += fElapsedTime;
		m_Data.m_nAnimIndex = m_pInstanceData->m_nAnimIndex = static_cast<unsigned int >(m_fElapsedTime * m_fFPS);
		//if (m_Data.m_nAnimIndex >= m_nMaxNumberOfAnimation)
		//	m_Data.m_nAnimIndex = m_pInstanceData->m_nAnimIndex = m_nMaxNumberOfAnimation ;
		if (m_fElapsedTime >= 1.0f/m_fFPS* m_nMaxNumberOfAnimation)
			result.push_back(new CMsg_Effect_Remove(this));
	}
	return result;

}

bool CBillboard_Instance_TexAnim::createObject(IFactory_BillboardEffect * pFactory)
{
	if (!pFactory)	return false;

	m_fFPS = pFactory->getFPS();
	m_nEffectTypeID = pFactory->getEffectTypeID();
	m_nMaxNumberOfAnimation = pFactory->getMaxNumberOfAnimation();

	return SUPER::createObject(pFactory);
}
