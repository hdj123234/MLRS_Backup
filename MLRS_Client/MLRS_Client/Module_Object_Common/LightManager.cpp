#include "stdafx.h"
#include "LightManager.h"

#include"..\Module_Renderer\RendererState_D3D.h"

#ifndef HEADER_DIRECTX
#include<d3d11.h>
#endif // !HEADER_DIRECTX

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX



CLightManager::CLightManager()
{
	m_pLights.reserve(MAX_LIGHT);
}

bool CLightManager::addLight(ILight * p)
{
	if(m_pLights.size()>=MAX_LIGHT)		return false;
	m_pLights.push_back(p);
	return true;
}

void CLightManager::updateOnDX(CRendererState_D3D *pRendererState)const
{

	if (!pRendererState)
		return;
	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();

	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(m_pGlobalLightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CLightManager.updateLightBuffer(), Map Error");
		return ;
	}
	Type_GlobalLighting *pMappedResource = static_cast<Type_GlobalLighting *>(d3dMappedResource.pData);
	pMappedResource->m_vGlobalAmbient = m_vGlobalAmbient;
	pMappedResource->m_nNumberOfLight = static_cast<unsigned int>(m_pLights.size());
	for (unsigned int i = 0; i < m_pLights.size(); ++i)
		pMappedResource->m_Lights[i] = *m_pLights[i]->getLight();
	pDeviceContext->Unmap(m_pGlobalLightBuffer, 0);
	return ;
}

bool CLightManager::createObject(IFactory_LightManager * pFactory)
{
	if (!pFactory)	return false;
	m_pGlobalLightBuffer = pFactory->createGlobalLightBuffer();
	m_vGlobalAmbient = pFactory->getGlobalAmbient();
	if (!m_pGlobalLightBuffer)	return false;
	return true;
}
