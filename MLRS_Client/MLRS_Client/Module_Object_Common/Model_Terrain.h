#pragma once
#ifndef HEADER_MODEL_TERRAIN
#define HEADER_MODEL_TERRAIN

#include "..\Module_Object_GameModel\GameModel.h"


#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif 

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif 


//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Model_Terrain_Tess : virtual public IFactory_GameModel {
public:
	IFactory_Model_Terrain_Tess() {}
	virtual ~IFactory_Model_Terrain_Tess() {}

	//override pure virtual func
	virtual ID3D11Buffer* createBuffer_CameraPos()=0;
	virtual std::vector<ID3D11Buffer*> createBuffers_Transform() = 0;
	virtual std::vector<ID3D11Buffer*> createBuffers_Lighting() = 0;
	virtual ID3D11SamplerState*			createSamplerState() = 0;

};

//----------------------------------- Class -------------------------------------------------------------------------------------

class CModel_Terrain_Tess :public CGameModel {
private:
	//ID3D11InputLayout *m_pInputLayout;
	//ID3D11VertexShader *m_pVS;
	//ID3D11HullShader *m_pHS;
	//ID3D11DomainShader *m_pDS;
	//ID3D11PixelShader *m_pPS;

	//ID3D11Buffer *m_pVertexBuffer;
	//ID3D11Buffer *m_pIndexBuffer;
	//UINT m_nStride;
	//UINT m_nNumberOfIndex;

	//ID3D11Buffer* m_pBuffer_CameraPos;
	//std::vector<ID3D11Buffer*> m_pBuffers_Transform;
	//std::vector<ID3D11Buffer*> m_pBuffers_Lighting;
	//std::vector<ID3D11ShaderResourceView*> m_pTexture_DiffuseNormal;
	//ID3D11ShaderResourceView* m_pTexture_HeightMap;
	//ID3D11SamplerState* m_pSamplerState;

public:
	CModel_Terrain_Tess();
	virtual ~CModel_Terrain_Tess() { CModel_Terrain_Tess::releaseObject(); }

	bool createObject(IFactory_Model_Terrain_Tess *pFactory);

	//override IGameModel
//	void draw(IPrinterState *pPrinterState)const;
	const	bool isClear()const { return false; }
};



#endif