#include"stdafx.h"
#include "Factory_SkyBox.h"


#include"..\Module_Object_GameModel\Factory_Mesh_GS.h"
#include"..\Module_Object_GameModel\Factory_Material.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"

//----------------------------------- CFactory_Mesh_SkyBox -------------------------------------------------------------------------------------

CFactory_Mesh_SkyBox::CFactory_Mesh_SkyBox(const std::string & rsGSName)
	:CFactory_Mesh_GS(rsGSName, rsGSName)
{
}

std::vector<ID3D11Buffer*> CFactory_Mesh_SkyBox::createGSBuffers()
{
	ID3D11Buffer * pView = factory_CB_Matrix_View.createBuffer();
	ID3D11Buffer * pProjection = factory_CB_Matrix_Projection.createBuffer();

	if (!(pView&&pProjection))return std::vector<ID3D11Buffer*>();

	return std::vector<ID3D11Buffer*>{pView, pProjection};
}


//----------------------------------- CFactory_Model_SkyBox -------------------------------------------------------------------------------------

const std::string CFactory_Model_SkyBox::s_sName("TXC_SKYBOX");

std::vector<IMesh *> CFactory_Model_SkyBox::createMeshs()
{
	CMesh_SkyBox * pMesh = new CMesh_SkyBox();
	if (!pMesh->createObject(this))
	{
		delete pMesh;
		return std::vector<IMesh *>();
	}
	return std::vector<IMesh *>{pMesh};
}

std::vector<IMaterial *> CFactory_Model_SkyBox::createMaterials()
{
	CMaterial_NoneLighting_Diffuse *pMaterial = new CMaterial_NoneLighting_Diffuse();
	if (!pMaterial->createObject(&m_Factory_Material))
	{
		delete pMaterial;
		pMaterial = nullptr;
	}
	return std::vector<IMaterial *>{pMaterial};
}

CFactory_Model_SkyBox::CFactory_Model_SkyBox(
	const std::string & rsTextureFileName, 
	const std::string & rsGSName, 
	const std::string & rsPSName)
	:CFactory_Mesh_SkyBox(rsGSName),
	m_Factory_Material(s_sName,rsPSName, rsTextureFileName),
	m_sTextureFileName(rsTextureFileName),
	m_sPSName(rsPSName)
{
}

//----------------------------------- CFactory_SkyBox -------------------------------------------------------------------------------------

CFactory_SkyBox::CFactory_SkyBox(
	const std::string & rsTextureFileName, 
	const std::string & rsGSName, 
	const std::string &	rsPSName)
	:CFactory_Model_SkyBox(rsTextureFileName, rsGSName, rsPSName )
{
}

IGameModel * CFactory_SkyBox::createSkyBoxModel()
{
	CModel_SkyBox *pModel = new CModel_SkyBox();
	if(!pModel->createObject(this))
	{
		delete pModel;
		pModel = nullptr;
	}

	return pModel;
}

ID3D11DepthStencilState * CFactory_SkyBox::createDepthStencilState()
{

	return factory_DepthStencilState_Disable.createDepthStencilState();

}