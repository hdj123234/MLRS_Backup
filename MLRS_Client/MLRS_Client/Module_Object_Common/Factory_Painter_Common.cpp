#include"stdafx.h"
#include "Factory_Painter_Common.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Type_InDirectX.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\MyINI.h"
#include"..\Module_MLRS_Object\Creator_Model_CustomFBX.h"


//----------------------------------- CFactory_Painter_BoundingBox -------------------------------------------------------------------------------------


std::vector<ID3D11Buffer*> CFactory_Painter_BoundingBox::createGSBuffers()
{

	struct Type_BoundingBox {
		DirectX::XMFLOAT3 m_vPos; char dummy1[4];
		DirectX::XMFLOAT3 m_vExtent; char dummy2[4];
		DirectX::XMFLOAT4X4 m_mRotation;
	};
	auto pView			= factory_CB_Matrix_View.createBuffer();
	auto pProjection	= factory_CB_Matrix_Projection.createBuffer();
	auto pBoundingBox	= CFactory_ConstBuffer<Type_BoundingBox >("CB_BoundingBox",1).createBuffer();

	return std::vector<ID3D11Buffer*>{pView, pProjection, pBoundingBox	};
}

ID3D11RasterizerState * CFactory_Painter_BoundingBox::createRasterizerState()
{
	return factory_RasterizerState_Wire.createRasterizerState();
}

CMaterial_None CFactory_Painter_BoundingBox::getMaterial()
{
	CMaterial_None retval;
	retval.createObject(this);
	return retval;
}

ID3D11GeometryShader * CFactory_Painter_BoundingBox::createGeometryShader()
{
	return CFactory_GeometryShader("gsBoundingBox","gsBoundingBox").createShader();
}

ID3D11VertexShader * CFactory_Painter_BoundingBox::createVertexShader()
{
	return CFactory_VertexShader("vsDummy", "vsDummy").createShader();
}

ID3D11PixelShader * CFactory_Painter_BoundingBox::createPixelShader()
{
	return CFactory_PixelShader("psDummy_Red","psDummy_Red").createShader();
}



//----------------------------------- CFactory_Mesh_Painter_Bone -------------------------------------------------------------------------------------

std::vector<ID3D11Buffer*> CFactory_Painter_Line::createGSBuffers()
{
	struct Type_Line {
		DirectX::XMFLOAT3 m_vPos; char dummy1[4];
		DirectX::XMFLOAT3 m_vTarget; char dummy2[4];
	};
	auto pView = factory_CB_Matrix_View.createBuffer();
	auto pProjection = factory_CB_Matrix_Projection.createBuffer();
	auto pLine = CFactory_ConstBuffer<Type_Line >("CB_Line", 1).createBuffer();

	return std::vector<ID3D11Buffer*>{pView, pProjection, pLine	};
}

ID3D11RasterizerState * CFactory_Painter_Line::createRasterizerState()
{
	return factory_RasterizerState_Wire.createRasterizerState();
}

CMaterial_None CFactory_Painter_Line::getMaterial()
{
	CMaterial_None retval;
	retval.createObject(this);
	return retval;
}

ID3D11GeometryShader * CFactory_Painter_Line::createGeometryShader()
{
	return CFactory_GeometryShader("gsLine", "gsLine").createShader();
}

ID3D11VertexShader * CFactory_Painter_Line::createVertexShader()
{
	return CFactory_VertexShader("vsDummy", "vsDummy").createShader();
}

ID3D11PixelShader * CFactory_Painter_Line::createPixelShader()
{
	return CFactory_PixelShader("psDummy_Red", "psDummy_Red").createShader();
}

//----------------------------------- CFactory_Painter_Curve -------------------------------------------------------------------------------------

const std::string CFactory_Painter_Curve::s_sVSName = "vsCurve";
const std::string CFactory_Painter_Curve::s_sPSName = "psColorOnly";

CFactory_Painter_Curve::CFactory_Painter_Curve(
	const std::string & rsName, 
	const DirectX::XMFLOAT4 &rvColor,
	const unsigned int nMaxNumberOfVertex)
	:m_sName(rsName),
	m_nMaxNumberOfVertex(nMaxNumberOfVertex),
	m_Factory_Material(rsName+"_Material",s_sPSName, rvColor)
{
}

ID3D11RasterizerState * CFactory_Painter_Curve::createRasterizerState()
{
	return factory_RasterizerState_Wire.createRasterizerState();
}

std::vector<ID3D11Buffer*> CFactory_Painter_Curve::createCBs_VS()
{
	auto pView = factory_CB_Matrix_View.createBuffer();
	auto pProjection = factory_CB_Matrix_Projection.createBuffer(); 

	return std::vector<ID3D11Buffer*>{pView, pProjection};
}

CMaterial_NoneLighting_ColorOnly CFactory_Painter_Curve::getMaterial()
{
	CMaterial_NoneLighting_ColorOnly retval;
	retval.createObject(&m_Factory_Material);
	return retval;
}

ID3D11InputLayout * CFactory_Painter_Curve::createInputLayout()
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputElementDesc = { { "POSITION", 0, DXGI_FORMAT::DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } };

	return CFactory_InputLayout(s_sVSName, s_sVSName, "IL_Curve", inputElementDesc).createInputLayout();
}

ID3D11Buffer * CFactory_Painter_Curve::createVertexBuffer()
{
	return CFactory_VertexBuffer<DirectX::XMFLOAT3>("VB_Curve",
													m_nMaxNumberOfVertex,
													D3D11_USAGE_DYNAMIC, 
													D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE)
		.createBuffer();
}

ID3D11VertexShader * CFactory_Painter_Curve::createVertexShader()
{
	return CFactory_VertexShader(s_sVSName, s_sVSName).createShader();
}

//ID3D11PixelShader * CFactory_Painter_Curve::createPixelShader()
//{
//	return CFactory_PixelShader(s_sPSName, s_sPSName).createShader();
//}

//----------------------------------- CFactory_Painter_CirclePlate -------------------------------------------------------------------------------------

const std::string CFactory_Painter_CirclePlate::s_sVSName = "vsCirclePlate";
const std::string CFactory_Painter_CirclePlate::s_sGSName = "gsCirclePlate";
const std::string CFactory_Painter_CirclePlate::s_sPSName = "psColorOnly";

CFactory_Painter_CirclePlate::CFactory_Painter_CirclePlate(
	const std::string & rsName, 
	const DirectX::XMFLOAT4 & rvColor,
	const float fRadius)
	: m_sName(rsName),
	m_Factory_Material(rsName + "_Material", s_sPSName, rvColor) ,
	m_fRadius(fRadius)
{
}

ID3D11RasterizerState * CFactory_Painter_CirclePlate::createRasterizerState()
{
	return factory_RasterizerState_DisableCulling.createRasterizerState();
}

ID3D11BlendState * CFactory_Painter_CirclePlate::createBlendState()
{
	return factory_BlendState_Default.createBlendState();
}

ID3D11DepthStencilState * CFactory_Painter_CirclePlate::createDepthStencilState()
{
	return factory_DepthStencilState_Disable.createDepthStencilState();
}

std::vector<ID3D11Buffer*> CFactory_Painter_CirclePlate::createCBs_GS()
{
	auto pView = factory_CB_Matrix_View.createBuffer();
	auto pProjection = factory_CB_Matrix_Projection.createBuffer();

	return std::vector<ID3D11Buffer*>{pView, pProjection};
}

CMaterial_NoneLighting_ColorOnly CFactory_Painter_CirclePlate::getMaterial()
{
	CMaterial_NoneLighting_ColorOnly retval;
	retval.createObject(&m_Factory_Material);
	return retval;
}

ID3D11InputLayout * CFactory_Painter_CirclePlate::createInputLayout()
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputElementDesc = { { "POSANDRADIUS", 0, DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } };

	return CFactory_InputLayout(s_sVSName, s_sVSName, "IL_CirclePlate", inputElementDesc).createInputLayout();
}

ID3D11Buffer * CFactory_Painter_CirclePlate::createVertexBuffer()
{
	return CFactory_VertexBuffer<DirectX::XMFLOAT4>(m_sName + "_VB", 1, D3D11_USAGE::D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE)
		.createBuffer();
}

ID3D11GeometryShader * CFactory_Painter_CirclePlate::createGeometryShader()
{
	return CFactory_GeometryShader(s_sGSName, s_sGSName).createShader();
}

ID3D11VertexShader * CFactory_Painter_CirclePlate::createVertexShader()
{
	return CFactory_VertexShader(s_sVSName, s_sVSName).createShader();
}

//----------------------------------- CFactory_Mesh_Painter_Bone -------------------------------------------------------------------------------------

CFactory_Mesh_Painter_Bone::CFactory_Mesh_Painter_Bone(
	const unsigned int nNumberOfBone,
	ID3D11ShaderResourceView * pBoneStructure,
	ID3D11ShaderResourceView * pAnimData,
	ID3D11ShaderResourceView * pAnimIndicator)
	:m_nNumberOfBone(nNumberOfBone),
	m_pBoneStructure(pBoneStructure),
	m_pAnimData(pAnimData),
	m_pAnimIndicator(pAnimIndicator),
	m_pAnimUpdater(nullptr),
	m_nNumberOfFrame(0)

{
}

CFactory_Mesh_Painter_Bone::CFactory_Mesh_Painter_Bone(
	CMyType_Bones & rBoneStructure,
	std::vector<std::vector<DirectX::XMFLOAT4X4>>& rmAnimData)
	:m_nNumberOfBone(static_cast<unsigned int>(rBoneStructure.getNumberOfBone())),
	m_pAnimIndicator(nullptr),
	m_nNumberOfFrame(static_cast<unsigned int>(rmAnimData[0].size()))
{
	auto &rContainer = rBoneStructure.getContainer();
	std::vector<Type_Bone> bones(m_nNumberOfBone);
	for (unsigned int i = 0; i < m_nNumberOfBone; ++i)
	{
		bones[i].m_nParentIndex = rContainer[i].second.m_nParentIndex;

		DirectX::XMStoreFloat4x4(&bones[i].m_mTransform_ToLocal, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&rContainer[i].second.m_mTransform_ToLocal)));
	}


	m_pBoneStructure = CFactory_SRV_BufferEX_FromData<Type_Bone>("BONE_BoneStructure", bones)
		.createShaderResourceView();


	unsigned int nHeight = static_cast<unsigned int>(rmAnimData.size());
	unsigned int nWidth = static_cast<unsigned int>(rmAnimData[0].size());
	std::vector<DirectX::XMFLOAT4> data;
	data.resize(nWidth*nHeight * 4);
	for (unsigned int i = 0; i < nHeight; ++i)
	{
		for (unsigned int j = 0; j < nWidth; ++j)
		{
			data[(i * 4 + 0)*nWidth + j] = DirectX::XMFLOAT4(
				rmAnimData[i][j]._11,
				rmAnimData[i][j]._12,
				rmAnimData[i][j]._13,
				rmAnimData[i][j]._14);

			data[(i * 4 + 1)*nWidth + j] = DirectX::XMFLOAT4(
				rmAnimData[i][j]._21,
				rmAnimData[i][j]._22,
				rmAnimData[i][j]._23,
				rmAnimData[i][j]._24);

			data[(i * 4 + 2)*nWidth + j] = DirectX::XMFLOAT4(
				rmAnimData[i][j]._31,
				rmAnimData[i][j]._32,
				rmAnimData[i][j]._33,
				rmAnimData[i][j]._34);

			data[(i * 4 + 3)*nWidth + j] = DirectX::XMFLOAT4(
				rmAnimData[i][j]._41,
				rmAnimData[i][j]._42,
				rmAnimData[i][j]._43,
				rmAnimData[i][j]._44);
		}

	}

	m_pAnimData = CFactory_SRV_Texture2D_FromData<DirectX::XMFLOAT4>("BONE_FrameData",
		data,
		DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT,
		nWidth,
		nHeight * 4)
		.createShaderResourceView();


	std::vector<Type_AnimIndicatorElement> tmp(m_nNumberOfBone);
	m_pAnimUpdater = CFactory_Buffer<Type_AnimIndicatorElement>(
		"BUF_BONE_AnimIndicator",
		m_nNumberOfBone,
		D3D11_USAGE::D3D11_USAGE_DYNAMIC,
		D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
		D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE)
		.createBuffer(tmp.data());

	m_pAnimIndicator = CFactory_ShaderResourceView_FromBuffer("SRV_BONE_AnimIndicator",
		m_pAnimUpdater,
		DXGI_FORMAT::DXGI_FORMAT_R32_UINT,
		m_nNumberOfBone).createShaderResourceView();
}

ID3D11VertexShader * CFactory_Mesh_Painter_Bone::createVertexShader()
{
	return CFactory_VertexShader("vsDummy_Bone", "vsDummy_Bone").createShader();
}

ID3D11GeometryShader * CFactory_Mesh_Painter_Bone::createGeometryShader()
{
	return CFactory_GeometryShader("gsDummy_Bone", "gsDummy_Bone").createShader();
}

ID3D11PixelShader * CFactory_Mesh_Painter_Bone::createPixelShader()
{
	return CFactory_PixelShader("psDummy_Bone", "psDummy_Bone").createShader();
}


std::vector<ID3D11Buffer*> CFactory_Mesh_Painter_Bone::createVP()
{
	ID3D11Buffer *pView = factory_CB_Matrix_View.createBuffer();
	ID3D11Buffer *pProjection = factory_CB_Matrix_Projection.createBuffer();
	return std::vector<ID3D11Buffer*>({ pView ,pProjection });
}

ID3D11DepthStencilState * CFactory_Mesh_Painter_Bone::createDepthStencilState()
{
	return 	factory_DepthStencilState_Disable.createDepthStencilState();

}

ID3D11BlendState * CFactory_Mesh_Painter_Bone::createBlendState()
{
	return 	factory_BlendState_Disable.createBlendState();
}
