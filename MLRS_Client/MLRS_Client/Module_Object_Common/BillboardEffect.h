#pragma once

#ifndef HEADER_BILLBOARDEFFECT
#define HEADER_BILLBOARDEFFECT

#include "Billboard.h"


struct CInstanceData_Billboard_Pos_TexAnim {
	DirectX::XMFLOAT3 m_vPos;
	unsigned int  m_nAnimIndex;

	CInstanceData_Billboard_Pos_TexAnim(const DirectX::XMFLOAT3 &rvPos = DirectX::XMFLOAT3(0,0,0))
		:m_vPos(rvPos),
		m_nAnimIndex(0)	{	}
};


typedef CInstanceData_Billboard_Pos_TexAnim Type_InstanceData_BillboardEffect;
typedef CMesh_Billboard<Type_InstanceData_BillboardEffect> CMesh_BillboardEffect;
typedef CModel_Billboard<Type_InstanceData_BillboardEffect> CModel_BillboardEffect;
typedef CBillboard_Instance<Type_InstanceData_BillboardEffect> CInstance_BillboardAnimation;

class IFactory_BillboardEffect : public  IFactory_Billboard_Instance<Type_InstanceData_BillboardEffect> {
public:
	virtual ~IFactory_BillboardEffect() = 0 {}

	virtual const unsigned int getMaxNumberOfAnimation() = 0;
	virtual const float getFPS() = 0;
	virtual const unsigned int getEffectTypeID() = 0;
};

class CBillboard_Instance_TexAnim :	public CInstance_BillboardAnimation,
									virtual public IEffect{
private:
	typedef CInstance_BillboardAnimation SUPER;
private:
	float m_fElapsedTime;
	
	unsigned int m_nMaxNumberOfAnimation;
	float m_fFPS;
	unsigned int m_nEffectTypeID;

public:
	CBillboard_Instance_TexAnim();
	virtual ~CBillboard_Instance_TexAnim() { }

	//override CBillboard_Instance<CInstanceData_Billboard_Pos_TexAnim>
//	void disable();

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return 0; }

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	//override IEffect
	virtual const unsigned int getEffectTypeID() {return m_nEffectTypeID ;}

	bool createObject(IFactory_BillboardEffect *pFactory);
};

typedef CBillboard_Instance_TexAnim CInstance_BillboardEffect;


#endif
