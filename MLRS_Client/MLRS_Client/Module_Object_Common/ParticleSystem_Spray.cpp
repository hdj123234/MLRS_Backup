#include"stdafx.h"
#include "ParticleSystem_Spray.h"

#include "..\Module_Object_GameModel\Mesh.h"
#include "..\Module_Object_GameModel\Material.h"
#include "..\Module_Renderer\RendererState_D3D.h"
#include "..\Module_Platform_DirectX11\DXUtility.h"

#ifndef HEADER_STL
#include<array>
#endif // !HEADER_STL

//----------------------------------- CParticleSprayPoints -------------------------------------------------------------------------------------




//----------------------------------- CParticleMover -------------------------------------------------------------------------------------

CParticleMover::CParticleMover()
	:m_pVS		(nullptr),
	m_pIL		(nullptr),
	m_nStride	(0),
	m_pGS		(nullptr)
{
}

void CParticleMover::moveParticles(CRendererState_D3D * pRendererState, ID3D11Buffer * pVB_Particle, ID3D11Buffer * pSOTarget_Particle, unsigned int nOffset) const
{
	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);
	pRendererState->setInputLayout(m_pIL);
	pRendererState->setVertexBuffer(pVB_Particle, m_nStride);
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_GS(m_pGS);
	pRendererState->setSOTarget(pSOTarget_Particle, nOffset);
	pRendererState->getDeviceContext()->DrawAuto(); 
}

bool CParticleMover::createObject(IFactory_ParticleMover * pFactory)
{
	m_pVS		=pFactory->createVS_Mover();
	m_pIL		=pFactory->createIL_Mover();
	m_nStride	=pFactory->getStride_Mover();
	m_pGS		=pFactory->createGS_Mover();

	return (m_pVS && m_pIL && m_nStride && m_pGS);
}

//----------------------------------- CParticlePainter_Spray -------------------------------------------------------------------------------------

CParticlePainter_Spray::CParticlePainter_Spray()
	:m_pVS		(nullptr),
	m_pIL		(nullptr),
	m_nStride	(0),
	m_pGS		(nullptr),
	m_pCBs_GS	(),
	m_pMaterial	(nullptr)
{
}

void CParticlePainter_Spray::drawParticles(CRendererState_D3D * pRendererState, ID3D11Buffer * pVB) const
{
	pRendererState->setSOTarget(nullptr);
	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);
	pRendererState->setInputLayout(m_pIL);
	pRendererState->setVertexBuffer(pVB, m_nStride);
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_GS(m_pGS);
	pRendererState->setConstBuffer_GS(m_pCBs_GS);

	m_pMaterial->setOnDX(pRendererState);

	pRendererState->getDeviceContext()->DrawAuto();
}

bool CParticlePainter_Spray::createObject(IFactory_ParticlePainter * pFactory)
{
	m_pVS		= pFactory->createVS_Painter();
	m_pIL		= pFactory->createIL_Painter();
	m_nStride	= pFactory->getStride_Painter();
	m_pGS		= pFactory->createGS_Painter();
	m_pCBs_GS	= pFactory->createCBs_GS_Painter();
	m_pMaterial	= pFactory->createMaterial();

	return (m_pVS && m_pIL && m_nStride && m_pGS && m_pMaterial);
}

void CParticlePainter_Spray::releaseObject()
{
	if (m_pMaterial)	delete m_pMaterial;
}