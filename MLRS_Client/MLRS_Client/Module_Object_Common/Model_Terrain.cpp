#include"stdafx.h"
#include"Terrain.h"

#include"..\Module_Printer\DXRendererState.h"
#include"..\Module_Object_GameModel\Interface_GameModel.h"

#include "..\Module_Object_GameModel\Mesh.h"
#include "..\Module_Object_GameModel\Material.h"
#include "Model_Terrain.h"


CModel_Terrain_Tess::CModel_Terrain_Tess()
{
}

bool CModel_Terrain_Tess::createObject(IFactory_Model_Terrain_Tess * pFactory)
{
	if (!pFactory)	return false;
	m_pBuffers_Lighting = pFactory->createBuffers_Lighting();
	m_pBuffers_Transform = pFactory->createBuffers_Transform();
	m_pBuffer_CameraPos = pFactory->createBuffer_CameraPos();
	m_pSamplerState = pFactory->createSamplerState();
	return AGameModel::createObject(pFactory);
}
//
//void CModel_Terrain_Tess::drawReady(IPrinterState * pPrinterState)const
//{
//	CDXRendererState* pRendererState = dynamic_cast<CDXRendererState *>(pPrinterState);
//	if (!pRendererState)		return;
//
////	m_pMaterial->updateMaterial(pRendererState);
//	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();
//	UINT nOffset = 0;
//
//	pRendererState->setShader_GS(nullptr);
//
//	pRendererState->setInputLayout(m_pInputLayout);
//	pRendererState->setVertexBuffers(0, m_pVertexBuffer, m_nStride, nOffset);
//	pRendererState->setIndexBuffer(m_pIndexBuffer, DXGI_FORMAT::DXGI_FORMAT_R32_UINT, 0);
//	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);
//
//	pRendererState->setShader_VS(m_pVS);
//	pRendererState->setSamplerStates_VS(m_pSamplerState);
//	pRendererState->setShaderResourceViews_VS(m_pTexture_HeightMap);
//
//	pRendererState->setShader_HS(m_pHS);
//	pRendererState->setConstBuffer_HS(m_pBuffer_CameraPos);
//
//	pRendererState->setShader_DS(m_pDS);
//	pRendererState->setConstBuffer_DS(m_pBuffers_Transform);
//	pRendererState->setSamplerStates_DS(m_pSamplerState);
//	pRendererState->setShaderResourceViews_DS(m_pTexture_HeightMap);
//
//	pRendererState->setShader_PS(m_pPS);
//	pRendererState->setConstBuffer_PS(m_pBuffers_Lighting);
//	pRendererState->setShaderResourceViews_PS(m_pTexture_DiffuseNormal);
//	pRendererState->setSamplerStates_PS(m_pSamplerState);
//}

void CModel_Terrain_Tess::draw(IPrinterState * pPrinterState)const
{
	CDXRendererState* pRendererState = dynamic_cast<CDXRendererState *>(pPrinterState);
	if (!pRendererState)		return;

	pRendererState->getDeviceContext()->DrawIndexed(m_nNumberOfIndex, 0, 0);
}
