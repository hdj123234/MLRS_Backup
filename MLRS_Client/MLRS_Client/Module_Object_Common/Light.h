#pragma once
#ifndef HEADER_LIGHT
#define HEADER_LIGHT

#include "Interface_Object_Common.h"
#include"..\Module_DXComponent\Type_InDirectX.h"

class ALight : virtual public ILight {
protected:
	Type_Light m_Lights;
public:
	ALight(unsigned int type);
	virtual ~ALight() =0{}

	Type_Light const * getLight()const { return &m_Lights; }

	//override IGameObject
	const FLAG_8 getTypeFlag() const { return 0; }
};

class CLight_Directional : public ALight {
private:
	DirectX::XMFLOAT4 &m_rvAmbient;
	DirectX::XMFLOAT4 &m_rvDiffuse;
	DirectX::XMFLOAT4 &m_rvSpecular;
	DirectX::XMFLOAT3 &m_rvDirection;
public:
	CLight_Directional();
	virtual ~CLight_Directional() {}

	void setAmbient	(const DirectX::XMFLOAT4 &rvAmbient  ){ m_rvAmbient = rvAmbient;}
	void setDiffuse	(const DirectX::XMFLOAT4 &rvDiffuse  ){ m_rvDiffuse = rvDiffuse;}
	void setSpecular(const DirectX::XMFLOAT4 &rvSpecular ){ m_rvSpecular = rvSpecular;}
	void setDirection(const DirectX::XMFLOAT3 &rvDirection){m_rvDirection = rvDirection;}
};

class CLight_Point : public ALight {
	DirectX::XMFLOAT4 &m_rvAmbient;
	DirectX::XMFLOAT4 &m_rvDiffuse;
	DirectX::XMFLOAT4 &m_rvSpecular;
	DirectX::XMFLOAT3 &m_rvPosition;
	DirectX::XMFLOAT4 &m_rvAttenuation;
	float &m_rfMaxRange;
public:
	CLight_Point( );
	virtual ~CLight_Point()  {}

	void setAmbient	(const DirectX::XMFLOAT4 &rvAmbient) { m_rvAmbient = rvAmbient; }
	void setDiffuse	(const DirectX::XMFLOAT4 &rvDiffuse) { m_rvDiffuse = rvDiffuse; }
	void setSpecular(const DirectX::XMFLOAT4 &rvSpecular) { m_rvSpecular = rvSpecular; }
	void setPosition(const DirectX::XMFLOAT3 &rvPosition){m_rvPosition = rvPosition;}
	void setAttenuation(const DirectX::XMFLOAT3 & rvAttenuation);
	void setMaxRange(float const &rfMaxRange) { m_rfMaxRange = rfMaxRange; }	
};

class CLight_Spot : public ALight {
	DirectX::XMFLOAT4 &m_rvAmbient;
	DirectX::XMFLOAT4 &m_rvDiffuse;
	DirectX::XMFLOAT4 &m_rvSpecular;
	DirectX::XMFLOAT3 &m_rvPosition;
	DirectX::XMFLOAT3 &m_rvDirection;
	DirectX::XMFLOAT4 &m_rvAttenuation;
	float &m_rfMaxRange;
	float &m_rfTheta;
	float &m_rfPhi;
	float &m_rfFalloff;

public:
	CLight_Spot( );
	virtual ~CLight_Spot()  {}

	void setAmbient	(const DirectX::XMFLOAT4 &rvAmbient) { m_rvAmbient = rvAmbient; }
	void setDiffuse	(const DirectX::XMFLOAT4 &rvDiffuse) { m_rvDiffuse = rvDiffuse; }
	void setSpecular(const DirectX::XMFLOAT4 &rvSpecular) { m_rvSpecular = rvSpecular; }
	void setPosition(const DirectX::XMFLOAT3 &rvPosition){m_rvPosition = rvPosition;}
	void setDirection	(const DirectX::XMFLOAT3 &rvDirection){m_rvDirection = rvDirection;}
	void setAttenuation	(const DirectX::XMFLOAT3 &rvAttenuation);
	void setMaxRange(const float fMaxRange) { m_rfMaxRange = fMaxRange; }
	void setTheta(const float fTheta) { m_rfTheta = fTheta; }
	void setPhi(const float fPhi) { m_rfPhi = fPhi; }
	void setFalloff(const float fFalloff) { m_rfFalloff = fFalloff; }
};



#endif