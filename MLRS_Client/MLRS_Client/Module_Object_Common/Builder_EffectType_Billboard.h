#pragma once
#ifndef HEADER_FACTORY_EFFECTMANAGER
#define HEADER_FACTORY_EFFECTMANAGER

#include "EffectManager.h"
#include "Creator_BillboardEffect.h"

//���漱��
class CMyINISection;

class CBuilder_EffectType_Billboard : public  IBuilder_EffectType {
private:
	CFactory_Model_BillboardEffect_SoftParticle m_ModelBuilder;
	const std::vector<std::string> m_sTextureFileNames;
	CEffectType m_Retval;
public:
	CBuilder_EffectType_Billboard(	const std::string &rsName,
							const DirectX::XMFLOAT2 &rvSizeOfBillboard,
							const unsigned int nMaxNumberOfInstance,
							const std::vector<std::string> &rsTextureFileNames,
							const std::string & rsGSName = "gsBillboard_Effect");
	virtual ~CBuilder_EffectType_Billboard()  {}

	virtual bool build(CEffectType * pEffectType) ;
	virtual CEffectType * build() ;

	static const CMyINISection getDefaultINISection();
};




#endif