#pragma once
#ifndef HEADER_EFFECT
#define HEADER_EFFECT

#include "Interface_Object_Common.h"

#ifndef HEADER_STL
#include<array>
#endif // !HEADER_STL

//���漱��
class IGameModel;
class RIGameModel_Instancing_Origin;


struct CEffectType {
	RIGameModel_Instancing_Origin * m_pGameModel;
	IEffectBuilder *m_pEffectBuilder;
	unsigned int m_nNumberOfInstance;

	CEffectType():m_pGameModel(nullptr), m_pEffectBuilder(nullptr), m_nNumberOfInstance(0){}
};

class IBuilder_EffectType : virtual public IBuilder<CEffectType> {
public:
	virtual ~IBuilder_EffectType() = 0 {}

	virtual bool build(CEffectType *) = 0;
	virtual CEffectType * build() = 0;
};

class CEffectManager : virtual public IEffectManager {
private:
	std::vector<CEffectType> m_EffectTypes;
	std::vector<IEffect *> m_pEffects;
	
	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_DRAWALBE| FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW | FLAG_OBJECTTYPE_ANIMATE| FLAG_OBJECTTYPE_DRAWALBE_CLEAR;
public:
	CEffectManager() {}
	virtual ~CEffectManager() { relaeseObject(); }

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_Flag; }

	//override RIGameObject_BeMust_RenewalForDraw
	void updateOnDX(CRendererState_D3D *pRendererState)const;

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject * pMsg);

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return true; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const ;

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	void relaeseObject();

	void addEffectType(RIGameModel_Instancing_Origin * pGameModel, IEffectBuilder *pEffectBuilder);
	void addEffectType(CEffectType &rEffectType) { m_EffectTypes.push_back(rEffectType); }
};


#endif