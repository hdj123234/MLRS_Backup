#include"stdafx.h"
#include"Terrain.h"

#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_Object_GameModel\Interface_GameModel.h"

using namespace DirectX;


//----------------------------------- CMesh_Terrain_Tess -------------------------------------------------------------------------------------

void CMesh_Terrain_Tess::setOnDX_InputAssembler(CRendererState_D3D * pRendererState) const
{
	pRendererState->setVertexBuffer(m_pVBuffer, m_nStride, m_nOffset);
	pRendererState->setIndexBuffer(m_pIBuffer, DXGI_FORMAT::DXGI_FORMAT_R32_UINT, m_nOffset);
	pRendererState->setInputLayout(m_pInputLayout);
	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);
}

void CMesh_Terrain_Tess::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	CMesh_DynamicLOD_DisplacementMapping::setOnDX_ShaderResource(pRendererState);
	pRendererState->setConstBuffer_DS(m_pBuffers_Transform);
}

bool CMesh_Terrain_Tess::createObject(IFactory_Mesh_Terrain_Tess * pFactory)
{
	m_pBuffers_Transform = pFactory->createBuffers_Transform();
	if (m_pBuffers_Transform.size() != 2)	return false;

	return CMesh_DynamicLOD_DisplacementMapping::createObject(pFactory);
}




//----------------------------------- CTerrain -------------------------------------------------------------------------------------

CTerrain::CTerrain()
	:AGameObject_ModelBase(),
	m_bDrawWire(false)
{
}



void CTerrain::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (m_bDrawWire)
	{
		pRendererState->setRasterizerState(m_pRasterizerState);
		AGameObject_ModelBase::drawOnDX(pRendererState);
		pRendererState->revertRasterizerState();
	}
	else
		AGameObject_ModelBase::drawOnDX(pRendererState);

}

const DirectX::XMFLOAT3 CTerrain::getPos_Min()const
{
	DirectX::XMFLOAT3 result;
	result.x = m_vCenter.x - m_vRange.x;
	result.y = m_vCenter.y ;
	result.z = m_vCenter.z - m_vRange.y;
	return result;
}

const DirectX::XMFLOAT3 CTerrain::getPos_Max()const
{
	DirectX::XMFLOAT3 result;
	result.x = m_vCenter.x + m_vRange.x;
	result.y = m_fMaxHeight;
	result.z = m_vCenter.z + m_vRange.y;
	return result;
}

const float CTerrain::getHeight(float x, float z)const
{
	unsigned int nMaxIndex_X = static_cast<unsigned int>(m_StoreData[0].size()) - 1;
	unsigned int nMaxIndex_Z = static_cast<unsigned int>(m_StoreData.size()) - 1;
	DirectX::XMFLOAT3 vCenter = m_vCenter;
	vCenter.x += m_vRange.x / 2;
	vCenter.z += m_vRange.y / 2;
	if( x > vCenter.x + m_vRange.x ||
		x < vCenter.x - m_vRange.x || 
		z > vCenter.z + m_vRange.y || 
		z < vCenter.z - m_vRange.y )	
		return 0.0f;

	//원점을 x,z에 대해 0으로 이동
	x -= vCenter.x;
	z -= vCenter.z;
	x += m_vRange.x;
	z += m_vRange.y;

	//필요한 인자를 계산
	float targetX = x / m_vUnit.x;
	float targetZ = z / m_vUnit.y;
	UINT indexX = static_cast<UINT>(targetX);
	UINT indexZ = static_cast<UINT>(targetZ);
//	indexX = nMaxIndex_X - indexX;
//	indexZ = nMaxIndex_Z - indexZ;
	if (indexX > nMaxIndex_X - 1 || indexX < 0) return 0;
	if (indexZ > nMaxIndex_Z - 1 || indexZ < 0) return 0;
	float perX = targetX - indexX;
	float perZ = targetZ - indexZ;

	//해당 점이 존재하는 사각형을 절단하는 사선의 방향 계산
	bool bBackSlash;
	if ((indexX + indexZ)%2 )
		bBackSlash = true;
	else
		bBackSlash = false;
	//해당 점이 사각형의 왼쪽 삼각형인지 오른쪽 삼각형인지 계산
	bool bLeft;
	if (bBackSlash)
	{
		if (perX + perZ < 1)
			bLeft = true;
		else
			bLeft = false;
	}
	else
	{
		if (perX < perZ )
			bLeft = true;
		else
			bLeft = false;
	}

	//삼각형에 대한 평면의 방정식 계산
	//ax + by + cz - d = 0
	XMFLOAT3A vLeftFront (0,m_StoreData[indexZ][indexX],0);
	XMFLOAT3A vLeftBack (0, m_StoreData[indexZ+1][indexX], 1);
	XMFLOAT3A vRightFront(1, m_StoreData[indexZ ][indexX+1], 0);
	XMFLOAT3A vRightBack (1, m_StoreData[indexZ+1][indexX + 1], 1);
	XMFLOAT3A vPlane;	//a, b, c
	float d;
	if (bBackSlash)
	{
		if (bLeft)	//RightBack 사용 안함
		{
			XMVECTOR vLeftFront_Load	= XMLoadFloat3A(&vLeftFront);
			XMVECTOR vLeftBack_Load	= XMLoadFloat3A(&vLeftBack);
			XMVECTOR vRightFront_Load	= XMLoadFloat3A(&vRightFront);
			XMVECTOR vNormal = XMVector3Cross(vLeftBack_Load - vLeftFront_Load, vRightFront_Load - vLeftFront_Load);
			XMStoreFloat3A(&vPlane, vNormal);
			d = XMVectorGetX(XMVector3Dot(vNormal, vLeftFront_Load));
		}
		else //LeftFront 사용 안함
		{
			XMVECTOR vLeftBack_Load = XMLoadFloat3A(&vLeftBack);
			XMVECTOR vRightFront_Load = XMLoadFloat3A(&vRightFront);
			XMVECTOR vRightBack_Load = XMLoadFloat3A(&vRightBack);
			XMVECTOR vNormal = XMVector3Cross(vRightFront_Load - vRightBack_Load,vLeftBack_Load - vRightBack_Load);
			XMStoreFloat3A(&vPlane, vNormal);
			d = XMVectorGetX(XMVector3Dot(vNormal, vLeftBack_Load));
		}
	}
	else
	{
		if (bLeft)	//RightFront 사용 안함
		{
			XMVECTOR vLeftFront_Load = XMLoadFloat3A(&vLeftFront);
			XMVECTOR vLeftBack_Load = XMLoadFloat3A(&vLeftBack);
			XMVECTOR vRightBack_Load = XMLoadFloat3A(&vRightBack);
			XMVECTOR vNormal = XMVector3Cross(vRightBack_Load - vLeftBack_Load, vLeftFront_Load - vLeftBack_Load);
			XMStoreFloat3A(&vPlane, vNormal);
			d = XMVectorGetX(XMVector3Dot(vNormal, vLeftFront_Load));
		}
		else //LeftBack 사용 안함
		{
			XMVECTOR vLeftFront_Load = XMLoadFloat3A(&vLeftFront);
			XMVECTOR vRightFront_Load = XMLoadFloat3A(&vRightFront);
			XMVECTOR vRightBack_Load = XMLoadFloat3A(&vRightBack);
			XMVECTOR vNormal = XMVector3Cross(vLeftFront_Load - vRightFront_Load, vRightBack_Load - vRightFront_Load);
			XMStoreFloat3A(&vPlane, vNormal);
			d = XMVectorGetX(XMVector3Dot(vNormal, vLeftFront_Load));
		}
	}

	//평면의 방정식에 x,z를 대입하여 높이 계산
	// y = (d - ax - cz)/b
	float result = (d - vPlane.x*perX - vPlane.z*perZ)/ vPlane.y;

	return result;
}

bool CTerrain::createObject(IFactory_Terrain * pFactory)
{
	if(!pFactory)	return false;
	m_pModel = pFactory->createModel();
	m_StoreData = pFactory->getStoreData();
	m_vCenter = pFactory->getCenter();
	m_vRange = pFactory->getRange();
	m_vUnit = pFactory->getUnit();
	m_fMaxHeight = pFactory->getMaxHeight();
	m_pRasterizerState =	pFactory->createRasterizerState();
	return (m_pModel &&!m_StoreData.empty());
}

void CTerrain::releaseModel()
{
	if (m_pModel)	delete m_pModel;
	m_pModel = nullptr;
}
