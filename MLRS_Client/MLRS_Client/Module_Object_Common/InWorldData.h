#pragma once
#ifndef HEADER_INWORLDDATA
#define HEADER_INWORLDDATA

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif

class CInWorldData_FromLocal {
public:
	DirectX::XMFLOAT3A & m_rvPos	;
	DirectX::XMFLOAT3A & m_rvRight	;
	DirectX::XMFLOAT3A & m_rvUp		;
	DirectX::XMFLOAT3A & m_rvLook	;

public:
	CInWorldData_FromLocal()
		:m_rvPos	(*static_cast<DirectX::XMFLOAT3A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16))),
		m_rvRight	(*static_cast<DirectX::XMFLOAT3A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16))),
		m_rvUp		(*static_cast<DirectX::XMFLOAT3A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16))),
		m_rvLook	(*static_cast<DirectX::XMFLOAT3A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16)))
	{
		m_rvPos		.x=0;	m_rvPos		.y=0;	m_rvPos		.z=0;
		m_rvRight	.x=1;	m_rvRight	.y=0;	m_rvRight	.z=0;
		m_rvUp		.x=0;	m_rvUp		.y=1;	m_rvUp		.z=0;
		m_rvLook	.x=0;	m_rvLook	.y=0;	m_rvLook	.z=1;
	}
	~CInWorldData_FromLocal() 
	{
		_aligned_free(&m_rvPos);
		_aligned_free(&m_rvRight);
		_aligned_free(&m_rvUp);
		_aligned_free(&m_rvLook);
	}

	void getWorldMatrix(DirectX::XMFLOAT4X4A *pmMatrix)const;
	void getWorldMatrix(DirectX::XMFLOAT4X4  *pmMatrix)const;
	void getWorldMatrix(DirectX::XMFLOAT4X4A &rmMatrix)const;
	void getWorldMatrix(DirectX::XMFLOAT4X4  &rmMatrix)const;
	DirectX::XMFLOAT4X4 getWorldMatrix()const;
};

class CInWorldData_Matrix_FromLocal : public CInWorldData_FromLocal {
public:
	DirectX::XMFLOAT4X4A & m_rmWorld;

public:
	CInWorldData_Matrix_FromLocal()
		:m_rmWorld(*static_cast<DirectX::XMFLOAT4X4A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT4X4A), 16)))	{}
	~CInWorldData_Matrix_FromLocal()	{		_aligned_free(&m_rmWorld);	}

	void updateWorldMatrix();
};
//----------------------------------- ������ -------------------------------------------------------------------------------------


//----------------------------------- CInWorldData_FromLocal -------------------------------------------------------------------------------------

inline void CInWorldData_FromLocal::getWorldMatrix(DirectX::XMFLOAT4X4A *pmMatrix)const
{
	pmMatrix->_11 = m_rvRight.x;	pmMatrix->_12 = m_rvRight.y;	pmMatrix->_13 = m_rvRight.z;	pmMatrix->_14 = 0;
	pmMatrix->_21 = m_rvUp.x;		pmMatrix->_22 = m_rvUp.y;		pmMatrix->_23 = m_rvUp.z;		pmMatrix->_24 = 0;
	pmMatrix->_31 = m_rvLook.x;		pmMatrix->_32 = m_rvLook.y;		pmMatrix->_33 = m_rvLook.z;		pmMatrix->_34 = 0;
	pmMatrix->_41 = m_rvPos.x;		pmMatrix->_42 = m_rvPos.y;		pmMatrix->_43 = m_rvPos.z;		pmMatrix->_44 = 1;
}

inline void CInWorldData_FromLocal::getWorldMatrix(DirectX::XMFLOAT4X4 * pmMatrix)const
{
	pmMatrix->_11 = m_rvRight.x;	pmMatrix->_12 = m_rvRight.y;	pmMatrix->_13 = m_rvRight.z;	pmMatrix->_14 = 0;
	pmMatrix->_21 = m_rvUp.x;		pmMatrix->_22 = m_rvUp.y;		pmMatrix->_23 = m_rvUp.z;		pmMatrix->_24 = 0;
	pmMatrix->_31 = m_rvLook.x;		pmMatrix->_32 = m_rvLook.y;		pmMatrix->_33 = m_rvLook.z;		pmMatrix->_34 = 0;
	pmMatrix->_41 = m_rvPos.x;		pmMatrix->_42 = m_rvPos.y;		pmMatrix->_43 = m_rvPos.z;		pmMatrix->_44 = 1;
}

inline void CInWorldData_FromLocal::getWorldMatrix(DirectX::XMFLOAT4X4A & rmMatrix)const
{
	rmMatrix._11 = m_rvRight.x;	rmMatrix._12 = m_rvRight.y;	rmMatrix._13 = m_rvRight.z;	rmMatrix._14 = 0;
	rmMatrix._21 = m_rvUp.x;	rmMatrix._22 = m_rvUp.y;	rmMatrix._23 = m_rvUp.z;	rmMatrix._24 = 0;
	rmMatrix._31 = m_rvLook.x;	rmMatrix._32 = m_rvLook.y;	rmMatrix._33 = m_rvLook.z;	rmMatrix._34 = 0;
	rmMatrix._41 = m_rvPos.x;	rmMatrix._42 = m_rvPos.y;	rmMatrix._43 = m_rvPos.z;	rmMatrix._44 = 1;
}

inline void CInWorldData_FromLocal::getWorldMatrix(DirectX::XMFLOAT4X4 & rmMatrix)const
{
	rmMatrix._11 = m_rvRight.x;	rmMatrix._12 = m_rvRight.y;	rmMatrix._13 = m_rvRight.z;	rmMatrix._14 = 0;
	rmMatrix._21 = m_rvUp.x;	rmMatrix._22 = m_rvUp.y;	rmMatrix._23 = m_rvUp.z;	rmMatrix._24 = 0;
	rmMatrix._31 = m_rvLook.x;	rmMatrix._32 = m_rvLook.y;	rmMatrix._33 = m_rvLook.z;	rmMatrix._34 = 0;
	rmMatrix._41 = m_rvPos.x;	rmMatrix._42 = m_rvPos.y;	rmMatrix._43 = m_rvPos.z;	rmMatrix._44 = 1;
}

inline DirectX::XMFLOAT4X4 CInWorldData_FromLocal::getWorldMatrix() const
{
	return DirectX::XMFLOAT4X4(	m_rvRight.x	,	m_rvRight.y	,	m_rvRight.z	,	0,
								m_rvUp.x	,	m_rvUp.y	,	m_rvUp.z	,	0,
								m_rvLook.x	,	m_rvLook.y	,	m_rvLook.z	,	0,
								m_rvPos.x	,	m_rvPos.y	,	m_rvPos.z	,	1);
}


//----------------------------------- CInWorldData_Matrix_FromLocal -------------------------------------------------------------------------------------

inline void CInWorldData_Matrix_FromLocal::updateWorldMatrix()
{
	m_rmWorld._11 = m_rvRight.x;	m_rmWorld._12 = m_rvRight.y;	m_rmWorld._13 = m_rvRight.z;	m_rmWorld._14 = 0;
	m_rmWorld._21 = m_rvUp.x;		m_rmWorld._22 = m_rvUp.y;		m_rmWorld._23 = m_rvUp.z;		m_rmWorld._24 = 0;
	m_rmWorld._31 = m_rvLook.x;		m_rmWorld._32 = m_rvLook.y;		m_rmWorld._33 = m_rvLook.z;		m_rmWorld._34 = 0;
	m_rmWorld._41 = m_rvPos.x;		m_rmWorld._42 = m_rvPos.y;		m_rmWorld._43 = m_rvPos.z;		m_rmWorld._44 = 1;
}
#endif