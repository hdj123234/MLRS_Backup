#include"stdafx.h"
#include "Painter_Common.h"

#include "Factory_Painter_Common.h"
#include"..\Module_Object_GameModel\Interface_GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"
#include "..\Module_Object_GameModel\Material.h"
#include "..\Module_Renderer\RendererState_D3D.h"
#include "..\Module_Platform_DirectX11\DXUtility.h"

#ifndef HEADER_STL
#include<array>
#include<string>
#endif // !HEADER_STL


using namespace DirectX;

//----------------------------------- CPainter_BoundingBox -------------------------------------------------------------------------------------

void CPainter_BoundingBox::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_GS(m_pGSBuffers);
}


bool CPainter_BoundingBox::createObject(IFactory_Painter_BoundingBox * pFactory)
{
	m_Material = pFactory->getMaterial();
	m_pGSBuffers = pFactory->createGSBuffers();
	m_pRasterizerState = pFactory->createRasterizerState();

	return CMesh_GS::createObject(pFactory);
}

void CPainter_BoundingBox::drawOnDX(CRendererState_D3D * pRendererState) const
{
	using namespace DirectX;
	Type_BoundingBox data;
	data.m_vPos = m_BoundingBox.Center;
	data.m_vExtent = m_BoundingBox.Extents;

	XMStoreFloat4x4(&data.m_mRotation ,XMMatrixRotationQuaternion( XMLoadFloat4(&m_BoundingBox.Orientation)));
	CBufferUpdater::update(pRendererState->getDeviceContext(), m_pGSBuffers[2], data);
	
	pRendererState->setRasterizerState(m_pRasterizerState);

	CMesh_GS::setOnDX(pRendererState);
	m_Material.setOnDX(pRendererState);
	CMesh_GS::drawOnDX(pRendererState);

	pRendererState->revertRasterizerState(); 
}



//----------------------------------- CPainter_Line -------------------------------------------------------------------------------------

void CPainter_Line::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_GS(m_pGSBuffers);
}

void CPainter_Line::setLine(const DirectX::XMFLOAT3 & rvPos, const DirectX::XMFLOAT3 & rvTarget)
{
	m_fTime = 0;
	m_vPos = rvPos;
	m_vTarget = rvTarget;	
//	m_bEnable = true;
}

bool CPainter_Line::createObject(IFactory_Painter_Line * pFactory)
{
	m_Material = pFactory->getMaterial();
	m_pGSBuffers = pFactory->createGSBuffers();
	m_pRasterizerState = pFactory->createRasterizerState();

	return CMesh_GS::createObject(pFactory);
}

void CPainter_Line::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (!m_bEnable)	return;
	using namespace DirectX;
	Type_Line data;
	data.m_vPos = m_vPos;
	data.m_vTarget = m_vTarget;

	CBufferUpdater::update(pRendererState->getDeviceContext(), m_pGSBuffers[2], data);

	pRendererState->setRasterizerState(m_pRasterizerState);
	
	CMesh_GS::setOnDX(pRendererState);
	m_Material.setOnDX(pRendererState);
	CMesh_GS::drawOnDX(pRendererState);

	pRendererState->revertRasterizerState();

}

std::vector<IMsg*> CPainter_Line::animateObject(const float fElapsedTime)
{
	if (!m_bEnable)	return std::vector<IMsg*>();
	m_fTime += fElapsedTime;
	if (m_fTime > 5)
		m_bEnable = false;
	return std::vector<IMsg*>();
}


//----------------------------------- CPainter_Curve -------------------------------------------------------------------------------------

void CPainter_Curve::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_VS(m_pCBs_VS);
}

void CPainter_Curve::setCurve(std::vector<DirectX::XMFLOAT3>&& rvrvVertices)
{
	m_vVertices = rvrvVertices; 
	CBufferUpdater::update<DirectX::XMFLOAT3>(m_pVBuffer, m_vVertices);
	m_nNumberOfVertex = static_cast<unsigned int>(m_vVertices.size());
}

bool CPainter_Curve::createObject(IFactory_Painter_Curve * pFactory)
{
	if (!pFactory) return false;
	m_Material =  pFactory->getMaterial();
	m_pRasterizerState = pFactory->createRasterizerState();
	m_pCBs_VS = pFactory->createCBs_VS();
	return CMesh_Vertex::createObject(pFactory);
}

void CPainter_Curve::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (!m_bEnable)	return;
	using namespace DirectX; 

	pRendererState->setRasterizerState(m_pRasterizerState);

	CMesh_Vertex::setOnDX(pRendererState);
	m_Material.setOnDX(pRendererState);
	CMesh_Vertex::drawOnDX(pRendererState);

	pRendererState->revertRasterizerState();
}


//----------------------------------- CPainter_CirclePlate -------------------------------------------------------------------------------------

void CPainter_CirclePlate::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_GS(m_pCBs_GS);
	pRendererState->setConstBuffer_GS(nullptr,2);
}

void CPainter_CirclePlate::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (!m_bEnable)	return;
	using namespace DirectX;

	pRendererState->setRasterizerState(m_pRasterizerState);
	pRendererState->setBlend(m_pBlendState);
	pRendererState->setDepthStencilState(m_pDepthStencilState);

	CMesh_VertexAndGS::setOnDX(pRendererState);
	m_Material.setOnDX(pRendererState);
	CMesh_VertexAndGS::drawOnDX(pRendererState);

	pRendererState->revertRasterizerState();
	pRendererState->revertBlend();
	pRendererState->revertDepthStencilState();
}

bool CPainter_CirclePlate::createObject(IFactory_Painter_CirclePlate * pFactory)
{
	if (!pFactory) return false;
	
	m_Material = pFactory->getMaterial();
	m_pRasterizerState = pFactory->createRasterizerState();
	m_pBlendState = pFactory->createBlendState();
	m_pDepthStencilState = pFactory->createDepthStencilState();
	m_pCBs_GS = pFactory->createCBs_GS();
	m_fRadius = pFactory->getRadius();

	return CMesh_VertexAndGS::createObject(pFactory);
}

void CPainter_CirclePlate::setTarget(const DirectX::XMFLOAT3 & rvTarget)
{
	m_vTargetAndRadius.x = rvTarget.x;
	m_vTargetAndRadius.y = rvTarget.y;
	m_vTargetAndRadius.z = rvTarget.z;
	m_vTargetAndRadius.w = m_fRadius;
	CBufferUpdater::update<DirectX::XMFLOAT4>(m_pVBuffer, m_vTargetAndRadius);
}

//----------------------------------- CMesh_Painter_Bone -------------------------------------------------------------------------------------


void CMesh_Painter_Bone::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_GS(m_pVP);
	pRendererState->setShaderResourceViews_GS(m_pBoneStructure, 0);
	pRendererState->setShaderResourceViews_GS(m_pAnimData, 1);
	pRendererState->setShaderResourceViews_GS(m_pAnimIndicator, 2);
}

CMesh_Painter_Bone::CMesh_Painter_Bone()
	:m_nNumberOfBone(0),
	m_pBoneStructure(nullptr),
	m_pAnimData(nullptr),
	m_pAnimIndicator(nullptr),
	m_nNumberOfFrame(0),
	m_pDepthStencilState(nullptr)
{
}

void CMesh_Painter_Bone::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (!pRendererState)	return;

	if (m_pBuffer_AnimIndicator_Updater)
	{

		ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();

		D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
		if (FAILED(pDeviceContext->Map(m_pBuffer_AnimIndicator_Updater, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CAnimController.updateOnDX(), Map Error");
			return;
		}
		Type_AnimIndicatorElement *pMappedResource = static_cast<Type_AnimIndicatorElement *>(d3dMappedResource.pData);
		for (unsigned int i = 0; i < m_Indicator.size(); ++i)
			pMappedResource[i] = m_Indicator[i];
		pDeviceContext->Unmap(m_pBuffer_AnimIndicator_Updater, 0);
	}

	pRendererState->setDepthStencilState(m_pDepthStencilState, 0);
	pRendererState->setBlend(m_pBlendStateState);
	pRendererState->setShader_PS(m_pPixelShader);
	pRendererState->getDeviceContext()->Draw(m_nNumberOfBone, 0);
	pRendererState->revertDepthStencilState();
}

bool CMesh_Painter_Bone::createObject(IFactory_Mesh_Bone * pFactory)
{
	if (!pFactory)	return false;
	m_nNumberOfBone = pFactory->getNumberOfBone();
	m_pBoneStructure = pFactory->getBoneStructure();
	m_pAnimData = pFactory->getAnimData();
	m_pAnimIndicator = pFactory->getAnimIndicator();
	m_pPixelShader = pFactory->createPixelShader();
	m_pVP = pFactory->createVP();
	m_pBuffer_AnimIndicator_Updater = pFactory->getAnimUpdater();
	m_pDepthStencilState = pFactory->createDepthStencilState();
	m_pBlendStateState = pFactory->createBlendState();
	m_Indicator.resize(m_nNumberOfBone);
	for (auto &rData : m_Indicator)
		rData = 0;
	m_nNumberOfFrame = pFactory->getNumberOfFrame();

	if (m_nNumberOfBone == 0 || !m_pBoneStructure || !m_pAnimData || !m_pPixelShader || m_pVP.size() != 2)	return false;
	return CMesh_GS::createObject(pFactory);
}

void CMesh_Painter_Bone::advanceFrame()
{
	for (auto &rData : m_Indicator)
	{
		rData++;
		if (rData >= m_nNumberOfFrame)
			rData = 0;
	}
}

//----------------------------------- CPainter_Bone -------------------------------------------------------------------------------------

std::vector<IMsg*> CPainter_Bone::animateObject(const float fElapsedTime)
{
	static float fFrame = 1.0 / 30;
	m_fETime += fElapsedTime;
	if (m_fETime > fFrame)
	{
		m_fETime -= fFrame;
		m_pMesh_Bone->advanceFrame();
	}
	return std::vector<IMsg*>();
}

bool CPainter_Bone::createObject(IFactory_Mesh_Bone * pFactory)
{
	if (m_pMesh_Bone)	return false;
	m_pMesh_Bone = new CMesh_Painter_Bone;
	if (!m_pMesh_Bone->createObject(pFactory))
	{
		delete m_pMesh_Bone;
		m_pMesh_Bone = nullptr;
		return false;
	}
	return true;
}

void CPainter_BoundingBoxManager::addBoundingBox(const DirectX::BoundingOrientedBox & rBoundingBox)
{
	m_BoundingBox.push_back(CPainter_BoundingBox());
	m_BoundingBox.back().createObject(&CFactory_Painter_BoundingBox());
	m_BoundingBox.back().setBoundingBox(rBoundingBox);
}
