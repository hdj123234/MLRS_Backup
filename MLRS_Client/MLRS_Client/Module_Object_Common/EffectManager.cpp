#include "stdafx.h"
#include "EffectManager.h"

#include"Msg_Effect.h"
#include"..\Module_Object_GameModel\Interface_GameModel.h"
#include"..\Module_Object_Instancing\Interface_Object_Instancing.h"

//
//#include"..\Module_Renderer\RendererState_D3D.h"
//
//#ifndef HEADER_DIRECTX
//#include<d3d11.h>
//#endif // !HEADER_DIRECTX
//
//#ifndef HEADER_MYLOGCAT
//#include"..\LogCat.h"
//#endif // !HEADER_DIRECTX
//


void CEffectManager::updateOnDX(CRendererState_D3D * pRendererState) const
{
	for (auto &rEffectType : m_EffectTypes)
	{
		if (rEffectType.m_nNumberOfInstance <= 0)	continue;
		rEffectType.m_pGameModel->updateOnDX(pRendererState);
	}
}

void CEffectManager::handleMsg(IMsg_GameObject * pMsg)
{
	switch (static_cast<CMsg_Effect *>(pMsg)->getMsgType())
	{
	case EMsgType_Effect::eCreate:
		m_EffectTypes[static_cast<CMsg_Effect_Create *>(pMsg)->getTypeID()].m_pEffectBuilder->setPosition(static_cast<CMsg_Effect_Create *>(pMsg)->getPosition());
		m_pEffects.push_back( m_EffectTypes[static_cast<CMsg_Effect_Create *>(pMsg)->getTypeID()].m_pEffectBuilder->build());
		m_EffectTypes[static_cast<CMsg_Effect_Create *>(pMsg)->getTypeID()].m_nNumberOfInstance++;
		break;

	case EMsgType_Effect::eRemove:
		{
			auto iter_Begin = m_pEffects.begin();
			auto iter_End = m_pEffects.end();

			auto iter = std::find(iter_Begin, iter_End, static_cast<CMsg_Effect_Remove *>(pMsg)->getEvent());
			if (iter != iter_End)
			{
				const unsigned int nID = static_cast<unsigned int >(iter - iter_Begin);
				m_EffectTypes[static_cast<CMsg_Effect_Remove *>(pMsg)->getEvent()->getEffectTypeID()].m_nNumberOfInstance--;

				delete m_pEffects[nID];
				m_pEffects[nID] = m_pEffects.back();
				m_pEffects.pop_back();

			}
			break;
		}
	}
}

void CEffectManager::drawOnDX(CRendererState_D3D * pRendererState) const
{
	for (auto &rEffectType : m_EffectTypes)
	{
		if (rEffectType.m_nNumberOfInstance <= 0)	continue;
		rEffectType.m_pGameModel->drawOnDX(pRendererState);
	}
}

std::vector<IMsg*> CEffectManager::animateObject(const float fElapsedTime)
{
	std::vector<IMsg*> result;
	for (auto pData : m_pEffects)
	{
		auto &rpMsgs = pData->animateObject(fElapsedTime);
		result.insert(result.end(), rpMsgs.begin(), rpMsgs.end());
	}
	for (auto pData : result)
	{
		CEffectManager::handleMsg(static_cast<CMsg_Effect *>(pData));
		delete pData;
	}
	return std::vector<IMsg*>();
}

void CEffectManager::relaeseObject()
{
	for (auto data : m_pEffects)
		delete data;
	for (auto &rData : m_EffectTypes)
	{
		delete rData.m_pEffectBuilder;
		delete rData.m_pGameModel;
	}
}

void CEffectManager::addEffectType(RIGameModel_Instancing_Origin * pGameModel, IEffectBuilder * pEffectBuilder)
{
	CEffectType effectType;
	effectType.m_pEffectBuilder = pEffectBuilder;
	effectType.m_pGameModel = pGameModel;
	effectType.m_nNumberOfInstance = 0;

	m_EffectTypes.push_back(effectType);
}
