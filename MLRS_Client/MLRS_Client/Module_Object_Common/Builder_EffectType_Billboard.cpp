#include"stdafx.h"
#include "Builder_EffectType_Billboard.h"

#include "..\Module_Object_Common\Creator_BillboardEffect.h"
#include "..\MyINI.h"


CBuilder_EffectType_Billboard::CBuilder_EffectType_Billboard(
	const std::string & rsName, 
	const DirectX::XMFLOAT2 & rvSizeOfBillboard, 
	const unsigned int nMaxNumberOfInstance, 
	const std::vector<std::string>& rsTextureFileNames,
	const std::string & rsGSName)
	:m_ModelBuilder(rsName, rvSizeOfBillboard, nMaxNumberOfInstance, rsTextureFileNames,
		"vsBillboard_Effect", rsGSName),
	m_sTextureFileNames(rsTextureFileNames)
{
	m_Retval.m_pGameModel = nullptr;
	m_Retval.m_pEffectBuilder = nullptr;
	m_Retval.m_nNumberOfInstance = 0;
}


bool CBuilder_EffectType_Billboard::build(CEffectType * pEffectType)
{
	if (!pEffectType)	return false;
	*pEffectType = *build();
	return true;
}

CEffectType * CBuilder_EffectType_Billboard::build()
{
	CModel_BillboardEffect *pModel = new CModel_BillboardEffect;
	if (!pModel->createObject(&m_ModelBuilder))
	{
		delete pModel;
		return &m_Retval;
	}


	m_Retval.m_pGameModel = pModel;
	m_Retval.m_pEffectBuilder = new CBuilder_BillboaardEffect(pModel, static_cast<unsigned int>(m_sTextureFileNames.size()), 30, 0);
	m_Retval.m_nNumberOfInstance = 0;

	return &m_Retval;
}

const CMyINISection CBuilder_EffectType_Billboard::getDefaultINISection()
{
	CMyINISection retval;
	retval.setParam("Filename", "Effect/Effect_Explosion_");
	retval.setParam("NumberLength", "4");
	retval.setParam("IndexStart", "0");
	retval.setParam("IndexEnd", "30");
	retval.setParam("Expension", "tga");
	retval.setParam("Size", "50,50");
	return retval;
}
