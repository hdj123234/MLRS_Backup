#include"stdafx.h"
#include "SkyBox.h"

#include "..\Module_Object_GameModel\Mesh.h"
#include "..\Module_Object_GameModel\Material.h"
#include "..\Module_Renderer\RendererState_D3D.h"

#ifndef HEADER_STL
#include<array>
#endif // !HEADER_STL



//----------------------------------- CModel_SkyBox -------------------------------------------------------------------------------------

CModel_SkyBox::CModel_SkyBox()

{
}

bool CModel_SkyBox::createObject(IFactory_Model_SkyBox * pFactory)
{
	if (!pFactory)	return false;

	return CGameModel::createObject(pFactory);
}


//----------------------------------- CMesh_SkyBox -------------------------------------------------------------------------------------

void CMesh_SkyBox::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_GS(m_pGSBuffers);
}

bool CMesh_SkyBox::createObject(IFactory_Mesh_SkyBox * pFactory)
{
	m_pGSBuffers = pFactory->createGSBuffers();
	if (m_pGSBuffers.empty())	return false;

	return CMesh_GS::createObject(pFactory);
}

//----------------------------------- CSkyBox -------------------------------------------------------------------------------------

void CSkyBox::drawReady(CRendererState_D3D * pRendererState) const
{
	pRendererState->setDepthStencilState(m_pDepthStencilState, NULL);
}

void CSkyBox::resetForDraw(CRendererState_D3D * pRendererState) const
{
	pRendererState->revertDepthStencilState();
}

CSkyBox::CSkyBox()
	:AGameObject_ModelBase(),
	m_pDepthStencilState(nullptr)
{

}

bool CSkyBox::createObject(IFactory_SkyBox * pFactory)
{
	if (!pFactory)	return false;
	m_pModel = pFactory->createSkyBoxModel();
	m_pDepthStencilState = pFactory->createDepthStencilState();
	return (m_pModel!= nullptr);
}

void CSkyBox::releaseModel()
{
	if (m_pModel)	delete m_pModel;
	m_pModel = nullptr;
}
