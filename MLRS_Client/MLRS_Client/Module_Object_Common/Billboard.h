#pragma once
/*	Billboard 클래스
	정점 쉐이더에서 위치를 넘기고 기하쉐이더에서 처리
	인스턴싱이 적용되는것은 아니나 처리가 거의 흡사하므로 인스턴싱 버전의 인터페이스 사용
*/


#ifndef HEADER_BILLBOARD
#define HEADER_BILLBOARD

#include "Interface_Object_Common.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_Instancing\Interface_Object_Instancing.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"
#include "..\Module_Object_GameModel\Material.h"

#include "..\Module_Renderer\RendererState_D3D.h"

#ifndef HEADER_LOGCAT
#include"..\LogCat.h"
#endif // !HEADER_STL

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif 



//전방선언
template<class Type>
class CMesh_Billboard;
template<class Type>
class CModel_Billboard;
template<class Type>
class CBillboard_Instance;


//----------------------------------- Creator -------------------------------------------------------------------------------------
//
class IFactory_Mesh_Billboard : virtual public IFactory_Mesh_GS,
								virtual public RIFactory_Mesh_VertexBuffer{
public:
	virtual ~IFactory_Mesh_Billboard() = 0 {}

	virtual std::vector<ID3D11Buffer*> createGSBuffers()=0;
	virtual const unsigned int getMaxNumberOfInstance() = 0;
};
//
template<class Type>
class IFactory_Model_Billboard  {
public:
	virtual ~IFactory_Model_Billboard() = 0 {}

	virtual CMesh_Billboard<Type> * createMesh() = 0;
	virtual IMaterial		* createMaterial() = 0;
	virtual ID3D11DepthStencilState		* createDepthStencilState() = 0;
	virtual ID3D11BlendState		* createBlendState() = 0;
	virtual ID3D11RasterizerState		* createRasterizerState() = 0;
};

template<class Type>
class IFactory_Billboard_Instance {
public:
	virtual ~IFactory_Billboard_Instance() = 0 {}

	virtual CModel_Billboard<Type> *getModel() = 0;
	virtual Type getData() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

template<class Type>
class CMesh_Billboard :public CMesh_GS,
						virtual public RIMesh_InstancingOnDX<Type> {
private:
	typedef CMesh_GS SUPER;
private:
	ID3D11InputLayout* m_pInputLayout;
	ID3D11Buffer* m_pVBuffer;
	unsigned int m_nStride;
	std::vector<ID3D11Buffer*> m_pGSBuffers;
	std::vector<InstanceData> m_InstancdDatas;
	
	unsigned int m_nNumberOfBillboard;
	unsigned int m_nMaxNumberOfInstance;
public:
	CMesh_Billboard();
	virtual ~CMesh_Billboard() {  }

	virtual void drawOnDX(CRendererState_D3D* pRendererState)const;
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;
	virtual void setOnDX_InputAssembler(CRendererState_D3D *pRendererState)const;

	virtual std::vector<InstanceData> *getInstanceData_Origin() {return &m_InstancdDatas;}
	virtual void addInstance() { ++m_nNumberOfBillboard; }
	virtual void releaseInstance() { --m_nNumberOfBillboard; }

	virtual void updateOnDX(CRendererState_D3D *pRendererState)const ;

	bool createObject(IFactory_Mesh_Billboard *pFactory);
	const unsigned int getNumberOfInstance() { return m_nMaxNumberOfInstance; }
};


template<class Type>
class CModel_Billboard :public IGameModel,
						virtual public RIGameModel_MeshGetter,
						virtual public RIGameModel_Instancing<Type> {
private:
	CMesh_Billboard<Type> * m_pMesh;
	IMaterial * m_pMaterial;
	ID3D11DepthStencilState *m_pDepthStencilState;
	ID3D11BlendState *m_pBlendState;
	ID3D11RasterizerState *m_pRasterizerState;

	InstanceDataContainer* m_pInstanceDataContainers;
	std::vector<RIInstance* > m_pInstances;

	RIMaterial_SetterOnDX * m_pMaterials_DXSetter;

	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR | FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW;

public:
	CModel_Billboard();
	virtual ~CModel_Billboard() { CModel_Billboard::releaseObject(); }

private:
	virtual void drawReady(CRendererState_D3D *pRendererState)const;
	virtual void resetForDraw(CRendererState_D3D *pRendererState)const {}
	virtual void revertRendererSetting(CRendererState_D3D *pRendererState)const {}
	

public:
	//override IGameObejct
	virtual const FLAG_8 getTypeFlag() const { return static_cast<int>(m_pMesh->getNumberOfInstance()!=0)* s_Flag; }

	//override IGameModel
	virtual const bool isClear()const { return true; }

	//override RIGameModel_MeshGetter
	virtual std::vector<IMesh *> getMeshs() { return  std::vector<IMesh *>{m_pMesh}; }

	//override IObejct_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;

	//override IObejct_UpdateOnDX
	virtual void updateOnDX(CRendererState_D3D *pRendererState)const	{ m_pMesh->updateOnDX(pRendererState);}
	
	//override RIGameModel_Instancing<Type_InstanceData>
	virtual void addInstance(RIInstance *pInstance);
	virtual void releaseInstance(RIInstance *pInstance);

	bool createObject(IFactory_Model_Billboard<Type> *pFactory);
	void releaseObject();
};

template<class Type>
class CBillboard_Instance :	public IGameObject,
							virtual public RIGameObject_Instancing<Type>{
private:
	CModel_Billboard<Type> *m_pModel;

protected:
	InstanceData  *m_pInstanceData;
	InstanceData  m_Data;
	bool m_bEnable;

public:
	CBillboard_Instance();
	virtual ~CBillboard_Instance() { if (m_pModel)m_pModel->releaseInstance(this); }

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return 0; }

	//RIGameObject_Instancing
	void setInstanceData(InstanceData *pInstanceData_Origin);	//RIGameModel_Instancing에서 호출
	void enable() ;
	void disable() ;
	void setValue(InstanceData &rValue)
	{
		*m_pInstanceData = m_Data = rValue;	
	}


	bool createObject(IFactory_Billboard_Instance<Type> *pFactory);
};



//----------------------------------- 구현부 -------------------------------------------------------------------------------------


//----------------------------------- CMesh_Billboard -------------------------------------------------------------------------------------
//
template<class Type>
inline CMesh_Billboard<Type>::CMesh_Billboard()
	:m_pVBuffer(nullptr),
	m_nStride(0),
	m_nNumberOfBillboard(0),
	m_nMaxNumberOfInstance(0)
{
}

template<class Type>
inline void CMesh_Billboard<Type>::drawOnDX(CRendererState_D3D * pRendererState) const
{
	pRendererState->getDeviceContext()->Draw(m_nNumberOfBillboard, 0);
}

template<class Type>
inline void CMesh_Billboard<Type>::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
//	pRendererState->setVertexBuffer(m_pVBuffer, m_nStride);
	pRendererState->setConstBuffer_GS(m_pGSBuffers);
}

template<class Type>
inline void CMesh_Billboard<Type>::setOnDX_InputAssembler(CRendererState_D3D * pRendererState) const
{
	pRendererState->setInputLayout(m_pInputLayout);
	pRendererState->setVertexBuffer(m_pVBuffer, m_nStride);
	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
}

template<class Type>
inline void CMesh_Billboard<Type>::updateOnDX(CRendererState_D3D * pRendererState) const
{
	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();

	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(m_pVBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CMesh_Billboard.updateOnDX(), Map Error");
		return;
	}
	Type *pMappedResource = static_cast<Type *>(d3dMappedResource.pData);
	for (unsigned int i = 0; i < m_nNumberOfBillboard; ++i)
		pMappedResource[i] = m_InstancdDatas[i];
	pDeviceContext->Unmap(m_pVBuffer, 0);
}

template<class Type>
inline bool CMesh_Billboard<Type>::createObject(IFactory_Mesh_Billboard * pFactory)
{
	if (!pFactory)	return false;

	m_pGSBuffers = pFactory->createGSBuffers();
	m_pVBuffer = pFactory->createVertexBuffer();
	m_pInputLayout = pFactory->createInputLayout();
	m_nStride = pFactory->getStride();
	m_nMaxNumberOfInstance = pFactory->getMaxNumberOfInstance();
	m_InstancdDatas.resize(m_nMaxNumberOfInstance);

	if (!(m_pVBuffer&&m_nStride > 0))	return false;

	return SUPER::createObject(pFactory);
}


////----------------------------------- CModel_Billboard -------------------------------------------------------------------------------------
//
template<class Type>
inline CModel_Billboard<Type>::CModel_Billboard()
	:m_pMesh(nullptr),
	m_pMaterial(nullptr),
	m_pInstanceDataContainers(nullptr),
	m_pMaterials_DXSetter(nullptr),
	m_pBlendState(nullptr),
	m_pDepthStencilState(nullptr),
	m_pRasterizerState(nullptr)
{
}

template<class Type>
inline void CModel_Billboard<Type>::drawReady(CRendererState_D3D * pRendererState) const
{
	pRendererState->setDepthStencilState(m_pDepthStencilState);
	pRendererState->setBlend(m_pBlendState);
	pRendererState->setRasterizerState(m_pRasterizerState);
}
template<class Type>
inline void CModel_Billboard<Type>::drawOnDX(CRendererState_D3D * pRendererState) const
{
	this->drawReady(pRendererState);
	m_pMaterials_DXSetter->setOnDX(pRendererState);
	m_pMesh->setOnDX(pRendererState);
	m_pMesh->drawOnDX(pRendererState);
	this->revertRendererSetting(pRendererState);
}

template<class Type>
inline void CModel_Billboard<Type>::addInstance(RIInstance * pInstance)
{
	m_pInstances.push_back(pInstance);
	const unsigned int nInstanceID = static_cast<const unsigned int >(m_pInstances.size()) - 1;

	pInstance->setInstanceData(&(*m_pInstanceDataContainers)[nInstanceID]);

	m_pMesh->addInstance();
}

template<class Type>
inline void CModel_Billboard<Type>::releaseInstance(RIInstance * pInstance)
{
	auto iter_Begin = m_pInstances.begin();
	auto iter_End = m_pInstances.end();
	auto iter = std::find(iter_Begin, iter_End, pInstance);
	if (iter == iter_End)	return;
	
	const unsigned int nInstanceID = static_cast<const unsigned int >(iter - m_pInstances.begin());
	auto changedInstance = m_pInstances.back();
	
	m_pInstances[nInstanceID] = changedInstance;
	m_pInstances.pop_back();
	changedInstance->setInstanceData(&(*m_pInstanceDataContainers)[nInstanceID]);
	
	m_pMesh->releaseInstance();
}

template<class Type>
inline bool CModel_Billboard<Type>::createObject(IFactory_Model_Billboard<Type> * pFactory)
{
	if (!pFactory)	return false;
	m_pMesh = pFactory->createMesh();
	m_pMaterial = pFactory->createMaterial();
	m_pMaterials_DXSetter = dynamic_cast<RIMaterial_SetterOnDX *>(m_pMaterial);
	m_pDepthStencilState = pFactory->createDepthStencilState();
	m_pBlendState = pFactory->createBlendState();
	m_pRasterizerState  = pFactory->createRasterizerState();
	if (!(m_pMesh&&m_pMaterial&&m_pMaterials_DXSetter))	return false;
	
	m_pInstanceDataContainers = m_pMesh->getInstanceData_Origin();
	
	return true;
}

template<class Type>
inline void CModel_Billboard<Type>::releaseObject()
{
	if (m_pMesh)	delete m_pMesh;
	if (m_pMaterial)	delete m_pMaterial;
}

//
////----------------------------------- CBillboard_Instance -------------------------------------------------------------------------------------
//

template<class Type>
inline CBillboard_Instance<Type>::CBillboard_Instance()
	:m_pModel(nullptr),
	m_pInstanceData(nullptr)
{
}

template<class Type>
inline void CBillboard_Instance<Type>::setInstanceData(InstanceData * pInstanceData_Origin)
{
	m_pInstanceData = pInstanceData_Origin; 
	*m_pInstanceData = m_Data;
}

template<class Type>
inline void CBillboard_Instance<Type>::enable()
{
	if (!m_bEnable)
	{
		m_bEnable = true;
		m_pModel->addInstance(this);
		*m_pInstanceData = m_Data;
	}
}

template<class Type>
inline void CBillboard_Instance<Type>::disable()
{
	if (m_bEnable)
	{
		m_bEnable = false;
		m_pModel->releaseInstance(this);
	}
}
 

template<class Type>
inline bool CBillboard_Instance<Type>::createObject(IFactory_Billboard_Instance<Type> * pFactory)
{
	if (!pFactory)	return false;
	m_pModel = pFactory->getModel();
	m_Data = pFactory->getData();

	return (m_pModel != nullptr);
}



typedef DirectX::XMFLOAT3 Type_InstanceData_Billboard_Pos;

typedef CMesh_Billboard<Type_InstanceData_Billboard_Pos> CMesh_Billboard_Pos;
typedef CModel_Billboard<Type_InstanceData_Billboard_Pos> CModel_Billboard_Pos;
typedef CBillboard_Instance<Type_InstanceData_Billboard_Pos> CBillboardInstance_Pos;



#endif