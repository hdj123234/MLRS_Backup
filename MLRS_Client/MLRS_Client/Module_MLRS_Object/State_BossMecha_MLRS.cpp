#include "stdafx.h"
#include "State_BossMecha_MLRS.h"

#include "BossMecha_MLRS.h"

#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"

//----------------------------------- AState_EnemyMecha -------------------------------------------------------------------------------------

void AState_BossMecha::setState(CTarget * const pObj, AState * const pState)
{
	if (pState != pObj->m_pState)
	{
		pObj->m_pState_Past = pObj->m_pState;
		pObj->m_pState = pState;
		pObj->m_pAnimContoller->setAction(static_cast<unsigned int>(pState->getActionID()));
	}
}
 
void AState_BossMecha::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop)) 
				AState_BossMecha::setState(pObj, CSingleton<CState_BossMecha_Waiting>::getInstance());
			else  
				AState_BossMecha::setState(pObj, CSingleton<CState_BossMecha_Moving>::getInstance());
		}
		}
	}
	 
}

std::vector<IMsg*> AState_BossMecha::animateObject(CTarget * pObj, const float fElapsedTime)
{
	pObj->CMecha::animateObject(fElapsedTime);
	return std::vector<IMsg*>();
}
 