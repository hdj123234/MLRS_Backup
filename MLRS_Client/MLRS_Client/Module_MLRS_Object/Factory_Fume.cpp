#include"stdafx.h"
#include "Factory_Fume.h" 
//#include"..\Module_Object_Common\UI_Simple_ScreenBase.h"

#include"..\Module_MLRS\GlobalVSInputMap.h"
#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"

#include"..\MyINI.h"


CFactory_Fume::CFactory_Fume(
	const std::string & rsName,
	const DirectX::XMFLOAT2 & rvBillboardSize, 
	const unsigned int nMaxNumberOfSprayPoint, 
	const unsigned int nMaxNumberOfParticle, 
	const float fVaiationAngle,
	const float fInitalSpeed,
	const float fSpeedAttenuation,
	const std::vector<std::string>& rsFileNames_DiffuseMap)
	: m_sName					(rsName),
	m_vBillboardSize			(rvBillboardSize),
	m_nMaxNumberOfSprayPoint	(nMaxNumberOfSprayPoint),
	m_nMaxNumberOfParticle		(nMaxNumberOfParticle),
	m_fVaiationAngle			(fVaiationAngle),
	m_fInitalSpeed				(fInitalSpeed),
	m_fSpeedAttenuation			(fSpeedAttenuation),
	m_Factory_Material			(rsName+"_Material","psBillboard_Effect",rsFileNames_DiffuseMap)
{
}

ID3D11VertexShader * CFactory_Fume::createVS_Particle()
{
	return CFactory_VertexShader("vsFumeSpray","vsFumeSpray").createShader();
}

ID3D11InputLayout * CFactory_Fume::createIL_Particle()
{
	auto &rInputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsFumeSpray");
	return CFactory_InputLayout("vsFumeSpray", "vsFumeSpray", "IL_FumeSpray", rInputElementDesc).createInputLayout();
}

UINT CFactory_Fume::getStride_Particle()
{
	return 40;
}

std::vector<D3D11_SO_DECLARATION_ENTRY> CFactory_Fume::getSODeclarationEntry()
{
	return std::vector<D3D11_SO_DECLARATION_ENTRY> {	{ 0, "POSITION", 0, 0, 3, 0 },
														{ 0,"VELOCITY",0,0,3,0 },
														{ 0,"SPEEDATTENUATION",0,0,1,0 },
														{ 0,"LIFETIME",0,0,1,0 } };
}

ID3D11VertexShader * CFactory_Fume::createVS_SprayPoints()
{
	return CFactory_VertexShader("vsFumeSpray_Point", "vsFumeSpray_Point").createShader();
}

ID3D11InputLayout * CFactory_Fume::createIL_SprayPoints()
{
	auto &rInputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsFumeSpray_Point");
	return CFactory_InputLayout("vsFumeSpray_Point", "vsFumeSpray_Point", "IL_FumeSpray_Point", rInputElementDesc).createInputLayout();
}

ID3D11Buffer * CFactory_Fume::createVB_SprayPoints()
{
	return CFactory_VertexBuffer<Type_Fume_SprayPoint>("VB_"+m_sName+"_SprayPoint", m_nMaxNumberOfSprayPoint,D3D11_USAGE_DYNAMIC,D3D11_CPU_ACCESS_WRITE).createBuffer();
}

ID3D11GeometryShader * CFactory_Fume::createGS_SprayPoints()
{
	std::vector<D3D11_SO_DECLARATION_ENTRY> &rSODeclarationEntry = getSODeclarationEntry();
	std::vector<UINT> nStrides = { getStride_Particle() };
	return CFactory_GeometryShaderForStreamOutput("gsFumeSpray_Spray", "gsFumeSpray_Spray", rSODeclarationEntry, nStrides,D3D11_SO_NO_RASTERIZED_STREAM).createShader();
}

std::vector<ID3D11Buffer*> CFactory_Fume::createCBs_GS_Points()
{
	Type_FumeState fumeState;
	fumeState.m_fInitalSpeed = m_fInitalSpeed;
	fumeState.m_fSpeedAttenuation = m_fSpeedAttenuation;
	fumeState.m_fVaiationAngle = m_fVaiationAngle;
	ID3D11Buffer* pCB_FumeState = CFactory_ConstBuffer_NonRenewal<Type_FumeState>("CB_"+m_sName+"_FumeState").createBuffer(&fumeState);
	return std::vector<ID3D11Buffer*>{pCB_FumeState};
}

ID3D11GeometryShader * CFactory_Fume::createGS_Mover()
{
	std::vector<D3D11_SO_DECLARATION_ENTRY> &rSODeclarationEntry = getSODeclarationEntry();
	std::vector<UINT> nStrides = { getStride_Particle() };
	return CFactory_GeometryShaderForStreamOutput("gsFumeParticle_Move", "gsFumeParticle_Move", rSODeclarationEntry, nStrides,D3D11_SO_NO_RASTERIZED_STREAM).createShader();

}

ID3D11GeometryShader * CFactory_Fume::createGS_Painter()
{
	return CFactory_GeometryShader("gsFumeParticle_Draw","gsFumeParticle_Draw").createShader();
}

std::vector<ID3D11Buffer*> CFactory_Fume::createCBs_GS_Painter()
{
	return std::vector<ID3D11Buffer*>{	factory_CB_Matrix_View.createBuffer(),
										factory_CB_Matrix_Projection.createBuffer(), 
										CFactory_ConstBuffer_NonRenewal<Type_BillboardSize>("CB_" + m_sName + "_BillboardSize").createBuffer(&m_vBillboardSize)};
}

RIMaterial_SetterOnDX * CFactory_Fume::createMaterial()
{
	CMaterial_NoneLighting_Diffuse_DepthBiasBlending *pMaterial = new CMaterial_NoneLighting_Diffuse_DepthBiasBlending();
	if (!pMaterial->createObject(&m_Factory_Material))
	{
		delete pMaterial;
		pMaterial = nullptr;
	}
	return pMaterial;
}

CParticleSprayPoints<CFactory_Fume::PointType>* CFactory_Fume::createParticleSprayPoints()
{
	CParticleSprayPoints<CFactory_Fume::PointType> *pRetval = new CParticleSprayPoints<CFactory_Fume::PointType>();
	if (!pRetval->createObject(this))
	{
		delete pRetval;
		pRetval = nullptr;
	}
	return pRetval;
}

CParticleMover * CFactory_Fume::createParticleMover()
{
	CParticleMover *pRetval = new CParticleMover;
	if (!pRetval->createObject(this))
	{
		delete pRetval;
		pRetval = nullptr;
	}
	return pRetval;
}

CParticlePainter_Spray * CFactory_Fume::createParticlePainter()
{
	CParticlePainter_Spray *pRetval = new CParticlePainter_Spray;
	if (!pRetval->createObject(this))
	{
		delete pRetval;
		pRetval = nullptr;
	}
	return pRetval;
}

std::array<ID3D11Buffer*, 2> CFactory_Fume::createParticleBuffers()
{
	ID3D11Buffer* pBuffer1 = CFactory_Buffer_FromSize(	"Buffer_"+m_sName+"_1",
														getStride_Particle(), 
														m_nMaxNumberOfParticle,
														D3D11_USAGE::D3D11_USAGE_DEFAULT,
														D3D11_BIND_FLAG::D3D11_BIND_VERTEX_BUFFER| D3D11_BIND_FLAG::D3D11_BIND_STREAM_OUTPUT,
														0 ).createBuffer();
	ID3D11Buffer* pBuffer2 = CFactory_Buffer_FromSize(	"Buffer_"+m_sName+"_2",
														getStride_Particle(), 
														m_nMaxNumberOfParticle,
														D3D11_USAGE::D3D11_USAGE_DEFAULT,
														D3D11_BIND_FLAG::D3D11_BIND_VERTEX_BUFFER| D3D11_BIND_FLAG::D3D11_BIND_STREAM_OUTPUT,
														0 ).createBuffer();
	return std::array<ID3D11Buffer*, 2>{pBuffer1, pBuffer2};
}

const CMyINISection CFactory_Fume::getDefaultINISection()
{
	CMyINISection retval;
	retval.setParam("VariationAngle", "1");
	retval.setParam("InitialSpeed", "1");
	retval.setParam("SpeedAttenuation", "1");
	return retval;
}
