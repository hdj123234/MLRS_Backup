#ifndef HEADER_STATE_CLIENTPLAYER_MLRS
#define HEADER_STATE_CLIENTPLAYER_MLRS

#include"State_MainMecha_MLRS.h"

#include"Interface_Object_MLRS.h"
#include"Camera_MLRS.h"
#include"..\MyUtility.h"

class CObjectDecorator_ClientPlayer;
class CSpace;
 

class AState_ClientPlayer  : public IState_Object<CObjectDecorator_ClientPlayer>{
protected:
	typedef CObjectDecorator_ClientPlayer CTarget;
	typedef AState_ClientPlayer AState;

	static CSpace *s_pSpace;

protected:
	void processMsg_KeyFlag(CTarget * const pObj, CMsg_MLRS_KeyFlag *pMsg_KeyFlag);
	void processMsg_Action(CTarget * const pObj, CMsg_MLRS_Action *pMsg_Action);
	void processMsg_MovePacket(CTarget * const pObj, CMsg_SC_Move *pMsg_SC_Move);
	void process_ResetDirection(CTarget * const pObj);
	void setCameraDirection(CTarget * const pObj);
	const bool checkEndOfAnimation(CTarget * const pObj) { return pObj->m_pMainMecha->getAnimController()->isEndOfAction(); }

protected:
	void setState(CTarget * const pObj, AState *const pState);
	virtual const EAnimState_MainMecha getActionID()const = 0;
	virtual const ECameraMode getCameraMode()const = 0;
	void resetAnimation(CTarget * const pObj) { pObj->m_pMainMecha->getAnimController()->setAction(static_cast<unsigned int>(this->getActionID())); }

	FLAG_8& getKeyFlag(CTarget * pObj) { return pObj->m_KeyFlag; }
	void addReturnMsg(CTarget * pObj, IMsg * pMsg) { pObj->m_pReturnMsgs.push_back(pMsg); }


public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)=0;
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);

	static void setSpace(CSpace * pSpace) { s_pSpace = pSpace; }

};


class CState_ClientPlayer_Waiting : public AState_ClientPlayer {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eIdle); }
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Moving : public CState_ClientPlayer_Waiting {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eMoving); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Jump : public CState_ClientPlayer_Waiting {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eJump); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};


class CState_ClientPlayer_Jump_On : public CState_ClientPlayer_Jump {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eJump); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Boost : public CState_ClientPlayer_Waiting {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eBoost_On); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Boost_On : public CState_ClientPlayer_Boost {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eBoost_On); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class AState_ClientPlayer_Attack1 : public AState_ClientPlayer {
protected:
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eWeapon1; }

	CType_Missile1& getMissile1(CTarget * pObj) { return pObj->m_Missile1; }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack1_Ready : public AState_ClientPlayer_Attack1 {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack1_Ready); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eWeapon1; }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack1_On : public AState_ClientPlayer_Attack1 {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack1_On); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eWeapon1; }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack1_Shoot : public AState_ClientPlayer_Attack1 {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack1_Shoot); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eWeapon1; }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack1_Off : public CState_ClientPlayer_Waiting {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack1_Off); }
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	//  virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class AState_ClientPlayer_Attack2 : public AState_ClientPlayer {
protected:
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eWeapon2; }
	
	IMsg * shootMissile(CTarget * pObj);
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack2_On : public AState_ClientPlayer_Attack2 {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack2_On); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};
class CState_ClientPlayer_Attack2_Ready : public AState_ClientPlayer_Attack2 {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack2_Ready); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack2_Shoot : public AState_ClientPlayer_Attack2 {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack2_Shoot); }
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack2_Off : public CState_ClientPlayer_Waiting {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack2_Off); }
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	//  virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class AState_ClientPlayer_Attack3 : public AState_ClientPlayer {
protected:
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eWeapon3_LockOn; }
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack3_Ready); }

	void setState(CTarget * const pObj, AState *const pState);
//	IMsg * shootMissile(CTarget * pObj);
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack3_On : public AState_ClientPlayer_Attack3 {
protected:
public:
	//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_Attack3_Shoot : public AState_ClientPlayer_Attack3 {
protected:
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eWeapon3_Shoot; }
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack3_Shoot); }
public:
	//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};


class CState_ClientPlayer_Attack3_Off : public CState_ClientPlayer_Waiting {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack3_Off); }
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class AState_ClientPlayer_Dead : public AState_ClientPlayer {
protected:
//	virtual const ECameraMode getCameraMode()const { return ECameraMode::eWeapon3_LockOn; }
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eDead); }

	//	IMsg * shootMissile(CTarget * pObj);
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};
class CState_ClientPlayer_Dead : public AState_ClientPlayer_Dead {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eDead); }
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDefault; }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};
class CState_ClientPlayer_GameOver : public AState_ClientPlayer_Dead {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eDummy3); }
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDebug; }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_ClientPlayer_DebugCamera : public AState_ClientPlayer {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eIdle); }
	virtual const ECameraMode getCameraMode()const { return ECameraMode::eDebug; }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};
#endif