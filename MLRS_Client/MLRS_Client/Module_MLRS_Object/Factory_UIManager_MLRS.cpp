#include"stdafx.h"
#include "Factory_UIManager_MLRS.h"

#include"..\Module_Object_UI\Factory_UI_Gauge_Background.h"
#include"..\Module_Object_UI\Factory_UI_Simple_ScreenBase.h"
#include"..\Module_Object_UI\Factory_UI_Gauge.h"
#include"..\Module_Object_UI\UI_LaunchGuide_Parabola.h"
#include"..\Module_Object_UI\Factory_UI_LaunchGuide_Parabola.h"
#include"..\Module_Object_UI\Factory_UI_Switch.h"
#include"..\Module_Object_UI\Factory_UI_Billboards.h"
//#include"..\Module_Object_Common\Factory_UI_ScriptManager.h"
#include"..\Module_Platform_Windows\GlobalWindows.h"
//#include"..\Module_Object_Common\UI_Simple_ScreenBase.h"

#include"..\Module_MLRS\GlobalVSInputMap.h"
#include"..\MyINI.h"
#include"..\MyCSVTable.h"


//void CUIData::resetCenter_FromLeftTop()
//{
//	m_vPos.x = m_vPos.x + (m_fAimSize / 2);
//	m_vPos.y = m_vPos.y + ((m_fAimSize / CGlobalWindow::getAspectRatio()) / 2);
//}

//CFactory_UIManager_MLRS::CFactory_UIManager_MLRS(const UIData_Aim & rUIData_Aim)
//	:m_UIData_Aim(rUIData_Aim)
//{
//}
//
//CFactory_UIManager_MLRS::CFactory_UIManager_MLRS(
//	const CMyINISection & rSection,
//	const std::vector<CScriptData> &rScriptDatas)
//	:m_ScriptDatas(rScriptDatas)
//{
//	m_fScriptEndTime = rSection.getParam_float("Script_EndTime");
//
//	CUIData aim;
//	aim.m_sFileName = rSection.getParam(			"Aim_Weapon1_FileName");
//	aim.m_vPos			= rSection.getParam_XMFLOAT2(	"Aim_Weapon1_Pos");
//	aim.m_fAimSize		= rSection.getParam_float(		"Aim_Weapon1_Size");
//	m_UIDatas["Aim_Weapon1"]=aim;
//
//	aim.m_sFileName = rSection.getParam("Aim_Weapon3_FileName");
//	aim.m_vPos = rSection.getParam_XMFLOAT2("Aim_Weapon3_Pos");
//	aim.m_fAimSize = rSection.getParam_float("Aim_Weapon3_Size");
//	m_UIDatas["Aim_Weapon3"] = aim;
//
//	CUIData weapon;
//	weapon.m_sFileName = rSection.getParam(			"Icon_Weapon1_FileName_Off");
//	weapon.m_sFileName_On = rSection.getParam("Icon_Weapon1_FileName_On");
//	weapon.m_vPos			= rSection.getParam_XMFLOAT2(	"Icon_Weapon1_Pos");
//	weapon.m_fAimSize		= rSection.getParam_float(		"Icon_Weapon1_Size");
//	m_UIDatas["Icon_Weapon1"] = weapon;
//
//	weapon.m_sFileName = rSection.getParam(			"Icon_Weapon2_FileName_Off");
//	weapon.m_sFileName_On = rSection.getParam("Icon_Weapon2_FileName_On");
//	weapon.m_vPos			= rSection.getParam_XMFLOAT2(	"Icon_Weapon2_Pos");
//	weapon.m_fAimSize		= rSection.getParam_float(		"Icon_Weapon2_Size");
//	m_UIDatas["Icon_Weapon2"]=weapon;
//
//	weapon.m_sFileName = rSection.getParam(			"Icon_Weapon3_FileName_Off");
//	weapon.m_sFileName_On = rSection.getParam("Icon_Weapon3_FileName_On");
//	weapon.m_vPos			= rSection.getParam_XMFLOAT2(	"Icon_Weapon3_Pos");
//	weapon.m_fAimSize		= rSection.getParam_float(		"Icon_Weapon3_Size");
//	m_UIDatas["Icon_Weapon3"] = weapon;
///*
//	weapon.m_sFileName = rSection.getParam(			"Icon_Weapon4_FileName_Off");
//	weapon.m_sFileName_On = rSection.getParam("Icon_Weapon4_FileName_On");
//	weapon.m_vPos			= rSection.getParam_XMFLOAT2(	"Icon_Weapon4_Pos");
//	weapon.m_fAimSize		= rSection.getParam_float(		"Icon_Weapon4_Size");
//	m_UIDatas["Icon_Weapon4"] = weapon;
//*/  
//
//	CUIData gauge;
//	gauge.m_sFileName = rSection.getParam( "HP_FileName");
//	gauge.m_sFileName_Background = rSection.getParam("HP_FileName_Background");
//	gauge.m_vPos = rSection.getParam_XMFLOAT2( "HP_Pos");
//	gauge.m_fAimSize = rSection.getParam_float("HP_Size");
//	m_UIDatas["HP"] = gauge;
//
//	gauge.m_sFileName = rSection.getParam("Boost_FileName");
//	gauge.m_sFileName_Background = rSection.getParam("Boost_FileName_Background");
//	gauge.m_vPos = rSection.getParam_XMFLOAT2("Boost_Pos");
//	gauge.m_fAimSize = rSection.getParam_float("Boost_Size");
//	m_UIDatas["Boost"] = gauge;
//
//	m_UIData_LaunchGuide.m_vLineColor	= rSection.getParam_XMFLOAT4("LaunchGuide_LineColor");
//	m_UIData_LaunchGuide.m_vCircleColor = rSection.getParam_XMFLOAT4("LaunchGuide_CircleColor");
//	m_UIData_LaunchGuide.m_fCircleRadius = rSection.getParam_float("LaunchGuide_CircleRadius");
//}

CFactory_UIManager_MLRS::CFactory_UIManager_MLRS(
	const std::string & rsINIFileName,
	const std::string & rsScriptTable)
	:m_INIData(g_sDataFolder + rsINIFileName)
{
	setDefaultINI(m_INIData);
	std::string sScriptTable = g_sDataFolder + rsScriptTable;

	std::string sPath = CUtility_Path::getPathOnly(rsScriptTable);
	CMyCSVTable scriptTable(sScriptTable);
	auto &rTable = scriptTable.getTable();

	for (auto &rData : rTable)
	{
		CScriptData scriptData;
		scriptData.m_sImageFileName = sPath + rData[1];
		scriptData.m_vPos.x = std::stof(rData[2]);
		scriptData.m_vPos.y = std::stof(rData[3]);
		scriptData.m_fScale = std::stof(rData[4]);

		m_ScriptDatas.push_back(scriptData);
	}

	m_fScriptEndTime = m_INIData.getSection("Script").getParam_float("EndTime");
}

IUserInterface * CFactory_UIManager_MLRS::createUI_Aim()
{
	std::string sSectionName;
	auto inputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsPosAndScale");
 
	std::vector<CSwitchData_Simple> switchData;
	CSwitchData_Simple tmp;

	{
		sSectionName = "Aim_Weapon1";
		auto &rSection = m_INIData.getSection(sSectionName);

		tmp.m_sImageFileName = rSection.getParam("FileName");
		tmp.m_vPos = rSection.getParam_XMFLOAT2("Pos");
		tmp.m_fScale = rSection.getParam_float("Size");
		switchData.push_back(tmp);
	}
	{
		sSectionName = "Aim_Weapon3";
		auto &rSection = m_INIData.getSection(sSectionName);

		tmp.m_sImageFileName = rSection.getParam("FileName");
		tmp.m_vPos = rSection.getParam_XMFLOAT2("Pos");
		tmp.m_fScale = rSection.getParam_float("Size");
		switchData.push_back(tmp);
	}
	CUI_Switch<2> *pUI_Switch = new CUI_Switch<2>;
	if (!pUI_Switch->createObject(&CFactory_UI_Switch_Simple<2>("Aim", inputElementDesc, switchData)))
	{
		delete pUI_Switch;
		abort();
		return nullptr;
	}
	return pUI_Switch;
}

IUserInterface * CFactory_UIManager_MLRS::createUI_Icon()
{
	std::string sSectionName;
	auto inputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsPosAndScale");
	 
	std::vector<CSwitchData_Swap> switchData;
	CSwitchData_Swap tmp;
	{
		sSectionName = "Icon_Weapon1";
		auto &rSection = m_INIData.getSection(sSectionName);
		tmp.m_sImageFileName_On = rSection.getParam("FileName_On");
		tmp.m_sImageFileName_Off = rSection.getParam("FileName_Off");
		tmp.m_vPos = rSection.getParam_XMFLOAT2("Pos");
		tmp.m_fScale = rSection.getParam_float("Size");
		switchData.push_back(tmp);
	}
	{
		sSectionName = "Icon_Weapon2";
		auto &rSection = m_INIData.getSection(sSectionName);
		tmp.m_sImageFileName_On = rSection.getParam("FileName_On");
		tmp.m_sImageFileName_Off = rSection.getParam("FileName_Off");
		tmp.m_vPos = rSection.getParam_XMFLOAT2("Pos");
		tmp.m_fScale = rSection.getParam_float("Size");
		switchData.push_back(tmp);
	}
	{
		sSectionName = "Icon_Weapon3";
		auto &rSection = m_INIData.getSection(sSectionName);
		tmp.m_sImageFileName_On = rSection.getParam("FileName_On");
		tmp.m_sImageFileName_Off = rSection.getParam("FileName_Off");
		tmp.m_vPos = rSection.getParam_XMFLOAT2("Pos");
		tmp.m_fScale = rSection.getParam_float("Size");
		switchData.push_back(tmp);
	}
	CUI_Switch<3> *pUI_Switch = new CUI_Switch<3>; 
	if (!pUI_Switch->createObject(&CFactory_UI_Switch_Swap<3>("Icon_Weapon", inputElementDesc, switchData)))
	{
		delete pUI_Switch;
		abort();
		return nullptr;
	} 
	return pUI_Switch;
}

IUserInterface * CFactory_UIManager_MLRS::createUI_HP()
{
	std::string sSectionName;
	auto inputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsPosAndScale");

	sSectionName = "HP";
	auto &rSection = m_INIData.getSection(sSectionName);
	CUI_Gauge_Background * pGauge =  new CUI_Gauge_Background;
	if (!pGauge->createObject(&CFactory_UI_Gauge_Background(
		sSectionName, rSection.getParam_XMFLOAT2("Pos"), rSection.getParam_float("Size"),
		rSection.getParam("FileName"), rSection.getParam("FileName_Background"), inputElementDesc)))
	{
		delete pGauge;
		abort();
		return nullptr;
	}
	return pGauge;
}

IUserInterface * CFactory_UIManager_MLRS::createUI_Boost()
{
	std::string sSectionName;
	auto inputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsPosAndScale");

	sSectionName = "Boost";
	auto &rSection = m_INIData.getSection(sSectionName);
	CUI_Gauge_Background *pGauge = new CUI_Gauge_Background;
	if (!pGauge->createObject(&CFactory_UI_Gauge_Background(
		sSectionName, rSection.getParam_XMFLOAT2("Pos"), rSection.getParam_float("Size"),
		rSection.getParam("FileName"), rSection.getParam("FileName_Background"), inputElementDesc)))
	{
		delete pGauge;
		abort();
		return nullptr;
	}
	return pGauge;
}

IUserInterface * CFactory_UIManager_MLRS::createUI_LaunchGuide()
{
	std::string sSectionName;
	auto inputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsPosAndScale");

	sSectionName = "LaunchGuide";
	auto &rSection = m_INIData.getSection(sSectionName);
	CUI_LaunchGuide_Parabola *pLaunchGuide_Parabola = new CUI_LaunchGuide_Parabola();
	CFactory_UI_LaunchGuide_Parabola factory_LaunchGuide("LaunchGuide",
		rSection.getParam_XMFLOAT4("LineColor"),
		rSection.getParam_XMFLOAT4("CircleColor"),
		rSection.getParam_float("CircleRadius"),
		1024);
	if (!pLaunchGuide_Parabola->createObjects(&factory_LaunchGuide))
	{
		delete pLaunchGuide_Parabola;
		abort();
		return nullptr;
	} 
	return pLaunchGuide_Parabola;
}

IUserInterface * CFactory_UIManager_MLRS::createUI_Script()
{
	auto inputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsPosAndScale");

	CUI_ScriptManager *pUI_ScriptManager = new CUI_ScriptManager(); 
	if (!pUI_ScriptManager->createObject(&CFactory_UI_ScriptManager("Scripts", m_ScriptDatas, m_fScriptEndTime, inputElementDesc)))
	{
		delete pUI_ScriptManager;
		abort();
		return nullptr;
	}

	return pUI_ScriptManager;
}

IUserInterface * CFactory_UIManager_MLRS::createUI_LockOn()
{
	std::string sSectionName;
	auto inputElementDesc = CGlobalVSInputMap::getInputElementDesc("vsPosAndScale");

	sSectionName = "LockOn";
	auto &rSection = m_INIData.getSection(sSectionName);
	std::vector<std::string> sFileNames;
	unsigned int nStart = rSection.getParam_uint("IndexStart");
	unsigned int nEnd = rSection.getParam_uint("IndexEnd")+1; 
	std::string sName = rSection.getParam("Filename");
	std::string sExpension = rSection.getParam("Expension");
	unsigned int nNumberLength = rSection.getParam_uint("NumberLength");  
	for (unsigned int i = nStart; i < nEnd; ++i)
	{
		std::string sTmp;
		std::stringstream ss;
		ss << sName << std::setw(nNumberLength) << std::setfill('0') << i << '.' << sExpension;
		ss >> sTmp;
		sFileNames.push_back(sTmp);
	} 
	CUI_Billboards *pUI = new CUI_Billboards;
	if (!pUI->createObject(&CFactory_UI_Billboards("UI_LockOn", rSection.getParam_XMFLOAT2("Size"), rSection.getParam_float("FPS"),20, sFileNames)))
	{
		delete pUI;
		abort();
		return nullptr;
	}
	return pUI;
}

std::vector<IUserInterface*> CFactory_UIManager_MLRS::failCreation(std::vector<IUserInterface*>& rRetval)
{
	for (auto pData : rRetval)
		delete pData;
	rRetval.clear();
	return rRetval;
}

std::vector<IUserInterface*> CFactory_UIManager_MLRS::createUIs()
{
	std::vector<IUserInterface*> retval;

	if (auto p = createUI_Aim()) retval.push_back(p);
	else return failCreation(retval);

	if (auto p = createUI_Icon()) retval.push_back(p);
	else return failCreation(retval);

	if (auto p = createUI_LockOn()) retval.push_back(p);
	else return failCreation(retval);

	if (auto p = createUI_HP()) retval.push_back(p);
	else return failCreation(retval);

	if (auto p = createUI_Boost()) retval.push_back(p);
	else return failCreation(retval);

	if (auto p = createUI_LaunchGuide()) retval.push_back(p);
	else return failCreation(retval);

	if (auto p = createUI_Script()) retval.push_back(p);
	else return failCreation(retval);

	retval[0]->disable();

	return retval; 
}  

const CMyINISection CFactory_UIManager_MLRS::getDefaultINISection()
{
	CMyINISection retval;
	retval.setParam("INIFile", "UI/MLRS_Client_UI.ini");
	retval.setParam("Script", "Script/ScriptTable.csv");
	return retval;
}

void CFactory_UIManager_MLRS::setDefaultINI(CMyINIData & rINIData)
{
	CMyINISection section;

	section.setParam("FileName", "UI/Aim.tga");
	section.setParam("Pos", "0.5, 0.5");
	section.setParam("Size", "0.06, 0.08");
	rINIData.setSection_DefaultData("Aim_Weapon1", section);
	section.clear();

	section.setParam("FileName", "UI/Aim.tga");
	section.setParam("Pos", "0.5, 0.5");
	section.setParam("Size", "0.06, 0.08");
	rINIData.setSection_DefaultData("Aim_Weapon3", section);
	section.clear();
	
	section.setParam("FileName_On"," UI/Enable1.tga ");
	section.setParam("FileName_Off", " UI/Disable1.tga ");
	section.setParam("Pos"," 0, 0.110677083 ");
	section.setParam("Size "," 0.078125	 ");
	rINIData.setSection_DefaultData("Icon_Weapon1", section);
	section.clear();
	
	section.setParam("FileName_On "," UI/Enable2.tga ");
	section.setParam("FileName_Off ", " UI/Disable2.tga ");
	section.setParam("Pos "," 0.0830078125, 0.110677083	");
	section.setParam("Size "," 0.078125 ");
	rINIData.setSection_DefaultData("Icon_Weapon2", section);
	section.clear();
	
	section.setParam("FileName_On "," UI/Enable3.tga ");
	section.setParam("FileName_Off ", " UI/Disable3.tga ");
	section.setParam("Pos "," 0.166015625, 0.110677083	");
	section.setParam("Size "," 0.078125 ");
	rINIData.setSection_DefaultData("Icon_Weapon3", section);
	section.clear();
	
	section.setParam("FileName ", " UI/HP.tga ");
	section.setParam("FileName_Background ", " UI/HP.tga ");
	section.setParam("Pos ", " 0,0");
	section.setParam("Size ", "0.333984375");
	rINIData.setSection_DefaultData("HP", section);
	section.clear();
	
	section.setParam("FileName ", " UI/Boost.tga ");
	section.setParam("FileName_Background ", " UI/Boost.tga ");
	section.setParam("Pos ", " 0,0.0625");
	section.setParam("Size ", "0.25");
	rINIData.setSection_DefaultData("Boost", section);
	section.clear();
	
	section.setParam("LineColor", "1,0,0,1");
	section.setParam("CircleColor", "1,0,0,0.5");
	section.setParam("CircleRadius", "50");
	rINIData.setSection_DefaultData("LaunchGuide", section);
	section.clear();

	section.setParam("EndTime", "5");
	rINIData.setSection_DefaultData("Script", section);
	section.clear();
	 
	section.setParam("Filename", "UI/LockOn/LockOn_");
	section.setParam("NumberLength", "1");
	section.setParam("IndexStart", "0");
	section.setParam("IndexEnd", "0");
	section.setParam("Expension", "tga");
	section.setParam("Size", "50,50");
	section.setParam("FPS", "30");
	rINIData.setSection_DefaultData("LockOn", section);
	section.clear();
}
//
//const CMyINISection CFactory_UIManager_MLRS::getDefaultINISection()
//{
//	CMyINISection retval;
//	retval.setParam("Aim_Wepaon1_FileName", "UI/Aim.tga");
//	retval.setParam("Aim_Wepaon1_Pos", "0.5, 0.5");
//	retval.setParam("Aim_Wepaon1_Size", "0.06, 0.08");
//	retval.setParam("Aim_Wepaon3_FileName", "UI/Aim.tga");
//	retval.setParam("Aim_Wepaon3_Pos", "0.5, 0.5");
//	retval.setParam("Aim_Wepaon3_Size", "0.06, 0.08");
//	retval.setParam("Icon_Weapon1_FileName_On"," UI/Enable1.tga ");
//	retval.setParam("Icon_Weapon1_FileName_Off", " UI/Disable1.tga ");
//	retval.setParam("Icon_Weapon1_Pos"," 0, 0.110677083 ");
//	retval.setParam("Icon_Weapon1_Size "," 0.078125	 ");
//	retval.setParam("Icon_Weapon2_FileName_On "," UI/Enable2.tga ");
//	retval.setParam("Icon_Weapon2_FileName_Off ", " UI/Disable2.tga ");
//	retval.setParam("Icon_Weapon2_Pos "," 0.0830078125, 0.110677083	");
//	retval.setParam("Icon_Weapon2_Size "," 0.078125 ");
//	retval.setParam("Icon_Weapon3_FileName_On "," UI/Enable3.tga ");
//	retval.setParam("Icon_Weapon3_FileName_Off ", " UI/Disable3.tga ");
//	retval.setParam("Icon_Weapon3_Pos "," 0.166015625, 0.110677083	");
//	retval.setParam("Icon_Weapon3_Size "," 0.078125 ");
////	retval.setParam("Icon_Weapon4_FileName_On "," UI/Enable4.tga ");
////	retval.setParam("Icon_Weapon4_FileName_Off ", " UI/Disable4.tga ");
////	retval.setParam("Icon_Weapon4_Pos "," 0.2490234375, 0.110677083	");
////	retval.setParam("Icon_Weapon4_Size "," 0.078125 ");
//	retval.setParam("HP_FileName ", " UI/HP.tga ");
//	retval.setParam("HP_FileName_Background ", " UI/HP.tga ");
//	retval.setParam("HP_Pos ", " 0,0");
//	retval.setParam("HP_Size ", "0.333984375");
//	retval.setParam("Boost_FileName ", " UI/Boost.tga ");
//	retval.setParam("Boost_FileName_Background ", " UI/Boost.tga ");
//	retval.setParam("Boost_Pos ", " 0,0.0625");
//	retval.setParam("Boost_Size ", "0.25");
//	retval.setParam("LaunchGuide_LineColor", "1,0,0,1");
//	retval.setParam("LaunchGuide_CircleColor", "1,0,0,0.5");
//	retval.setParam("LaunchGuide_CircleRadius", "50");
//	retval.setParam("Script_EndTime", "5");
//	return retval;
//}
