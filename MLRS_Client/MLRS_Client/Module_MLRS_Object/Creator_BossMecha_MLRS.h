#ifndef HEADER_CREATOR_BOSSMECHA_MLRS
#define HEADER_CREATOR_BOSSMECHA_MLRS

#include"BossMecha_MLRS.h"
#include"GlobalDataMap_MLRS.h"
#include"Mecha_MLRS.h"
#include"Type_MLRS.h"
#include"Interface_Object_MLRS.h"

//���漱��
struct CameraState_ThirdPerson;
class CMyINISection;

enum EActionType : ENUMTYPE_256;

class CFactory_BossMecha : public AFactory_Mecha,
							virtual public IFactory_Boss {
private:
	typedef AFactory_Mecha SUPER;
private:
	IGameModel *m_pModel;
public:
	CFactory_BossMecha(const std::string &rsName, IGameModel *pGameModel);
	virtual ~CFactory_BossMecha() {}

	//override IFactory_GameObject_ModelBase
	virtual IGameModel * getModel() { return m_pModel; }

	//override IFactory_GameObject_ModelBase_Anim
	virtual	IAnimController * createAnimController();
	

	virtual ID3D11Buffer *  createConstBuffer_World()	{return SUPER::createConstBuffer_World();}
	void setName(const std::string &rsName)				{SUPER::setName(rsName);}
	virtual DirectX::XMFLOAT3  getPosition(){ return SUPER::getPosition(); }
	virtual DirectX::XMFLOAT3  getRight()	{ return SUPER::getRight(); }
	virtual DirectX::XMFLOAT3  getUp()		{ return SUPER::getUp(); }
	virtual DirectX::XMFLOAT3  getLook()	{ return SUPER::getLook(); }

	virtual EBossType getBossType() { return EBossType::eBoss1; }

	static std::map<unsigned int, EActionType> getStartPointList();
	static const CMyINISection getDefaultINISection();
};
//
//class CBuilder_BossMecha : virtual public IBuilder<IBoss>,
//							private CFactory_BossMecha {
//
//public:
//	CBuilder_BossMecha(const std::string &rsName, IGameModel *pGameModel);
//	virtual ~CBuilder_BossMecha() {}
//
//public:
//	//CMultiBuilder<IMainMecha>
//	virtual IBoss * build();
//	virtual bool build(IBoss *);
//
//};

#endif