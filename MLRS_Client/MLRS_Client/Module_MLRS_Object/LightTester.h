#ifndef HEADER_LIGHTTESTER
#define HEADER_LIGHTTESTER

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif // !HEADER_DIRECTX


#include"Type_MLRS.h"
#include"..\Module_Object_Instancing\GameObject_Instanced.h"
#include"..\Module_Object\GameObject_ModelBase.h"

//----------------------------------- Class -------------------------------------------------------------------------------------

class CLightTester :	virtual public IGameObject,
						virtual public RIGameObject_Drawable{
private:
	ID3D11VertexShader * m_pVS;
	ID3D11GeometryShader * m_pGS;
	ID3D11PixelShader * m_pPS;
	std::vector<ID3D11Buffer *>m_pCBuffer_VP;

	static const FLAG_8 s_ObjectFlag = FLAG_OBJECTTYPE_DRAWALBE;
public:
	CLightTester();
	virtual ~CLightTester() {}

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_ObjectFlag; }

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return false; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const ;

};


#endif