#include "stdafx.h"
#include"BossMecha_MLRS.h"

#include"..\Module_MLRS\Msg_InGame_MLRS.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include"..\Module_Object\Msg_Movement.h"

#include"..\Module_Object\AnimController.h"
#include"..\Module_Object_GameModel\GameModel.h"
#include"..\Module_Object_GameModel\AnimData.h"


#include"State_BossMecha_MLRS.h"


//----------------------------------- CEnemyMecha -------------------------------------------------------------------------------------

//
//void CBossMecha::handleMsg(IMsg_GameObject * pMsg)
//{
//	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
//	{
//		switch (pMsg_SC->getType())
//		{
//		case EMsgType_SC::eMove:
//		{
//			SUPER::move(static_cast<CMsg_SC_Move*>(pMsg_SC)->getMovementMsg());
//			if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
//				m_eAnimState = static_cast<ENUMTYPE_256>(EAnimState_BossMecha::eIdle);
////			else
////				m_eAnimState = static_cast<ENUMTYPE_256>(EAnimState_BossMecha::eMoving);
//		}
//		}
//	}
//
//}

CBossMecha::CBossMecha()
	:m_pState(CSingleton< CState_BossMecha_Waiting>::getInstance()),
	m_pState_Past(nullptr)
{
}

bool CBossMecha::createObject(IFactory_Boss * pFactory)
{
	m_eBossType = pFactory->getBossType();
	return SUPER::createObject(pFactory);
}

ID3D11ShaderResourceView * CBossMecha::getBoneStructure()
{
	return dynamic_cast<CAnimData_NoInstancing *>(dynamic_cast<CGameModel *>(m_pModel)->getAnimData())->getBoneStructure();
}

ID3D11ShaderResourceView * CBossMecha::getAnimData()
{
	DirectX::BoundingSphere s;
	s.Center = DirectX::XMFLOAT3(1, 1, 1);
	s.Radius = 1;
	
	DirectX::BoundingOrientedBox b;
	b.Center = DirectX::XMFLOAT3(1, 1, 1);
	b.Extents = DirectX::XMFLOAT3(1, 1, 1);
	DirectX::XMFLOAT4 q;
	XMStoreFloat4(&q,DirectX::XMQuaternionRotationRollPitchYaw(0, 0, 0));
	b.Orientation = q;

	if (b.Intersects(s)) {
		//�浹
	}
	return dynamic_cast<CAnimData_NoInstancing *>(dynamic_cast<CGameModel *>(m_pModel)->getAnimData())->getFrameData();
}

