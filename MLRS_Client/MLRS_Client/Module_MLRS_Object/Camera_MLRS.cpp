#include"stdafx.h"
#include "Camera_MLRS.h"

#include"..\Module_Object\Msg_Movement.h"
//


//----------------------------------- CView_Morph -------------------------------------------------------------------------------------

CView_Morph::CView_Morph()
	: m_pvPos	(static_cast<DirectX::XMFLOAT3A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16))),
	m_pvRight	(static_cast<DirectX::XMFLOAT3A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16))),
	m_pvUp		(static_cast<DirectX::XMFLOAT3A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16))),
	m_pvLook	(static_cast<DirectX::XMFLOAT3A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT3A), 16))),
	m_pmReturnValue(static_cast<DirectX::XMFLOAT4X4A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT4X4A), 16)))
{
}

CView_Morph::~CView_Morph()
{
	_aligned_free(m_pvPos);
	_aligned_free(m_pvRight);
	_aligned_free(m_pvUp);
	_aligned_free(m_pvLook);
	_aligned_free(m_pmReturnValue);
}

const DirectX::XMFLOAT4X4A & CView_Morph::getMatrix() const
{
	using namespace DirectX;
	CView_Morph::getLocalLook();
	CView_Morph::getLocalUp();
	CView_Morph::getPosition();

	XMVECTOR vPos = XMLoadFloat3A(m_pvPos);

	XMVECTOR vLook = XMVector3NormalizeEst( XMLoadFloat3A(m_pvLook));
	XMVECTOR vUp = XMLoadFloat3A(m_pvUp);
	//XMVECTOR vRight = XMLoadFloat3A(m_pvRight);
	XMVECTOR vRight = XMVector3NormalizeEst(XMVector3Cross(vUp, vLook));
	//XMVECTOR vUp = XMVector3NormalizeEst(XMVector3Cross(vLook,vRight));
	vUp = XMVector3Cross(vLook,vRight);

	XMStoreFloat3A(m_pvRight, vRight);
	XMStoreFloat3A(m_pvUp, vUp);

	m_pmReturnValue->_11 = m_pvRight->x;	
	m_pmReturnValue->_12 = m_pvUp->x;	
	m_pmReturnValue->_13 = m_pvLook->x;	
	m_pmReturnValue->_14 = 0;
	m_pmReturnValue->_21 = m_pvRight->y;
	m_pmReturnValue->_22 = m_pvUp->y;
	m_pmReturnValue->_23 = m_pvLook->y;
	m_pmReturnValue->_24 = 0;
	m_pmReturnValue->_31 = m_pvRight->z;
	m_pmReturnValue->_32 = m_pvUp->z;	
	m_pmReturnValue->_33 = m_pvLook->z;	
	m_pmReturnValue->_34 = 0;
	m_pmReturnValue->_41 = -XMVectorGetX(XMVector3Dot(vPos, vRight));	
	m_pmReturnValue->_42 = -XMVectorGetX(XMVector3Dot(vPos, vUp));
	m_pmReturnValue->_43 = -XMVectorGetX(XMVector3Dot(vPos, vLook));
	m_pmReturnValue->_44 = 1;


	return *m_pmReturnValue;
}
const DirectX::XMFLOAT3A * CView_Morph::getPosition() const
{
	using namespace DirectX;
	
	XMStoreFloat3A(m_pvPos,XMVectorLerp(XMLoadFloat3A(m_pView1->getPosition()),
										XMLoadFloat3A(m_pView2->getPosition()), 
										m_fParameter_t/m_fMorpTime));
	return m_pvPos;
}
const DirectX::XMFLOAT3A * CView_Morph::getLocalRight() const
{
	using namespace DirectX;

	XMStoreFloat3A(m_pvRight, XMVectorLerp(	XMLoadFloat3A(m_pView1->getLocalRight()),
											XMLoadFloat3A(m_pView2->getLocalRight()),
											m_fParameter_t/m_fMorpTime));
	return m_pvRight;
}
const DirectX::XMFLOAT3A * CView_Morph::getLocalUp() const
{
	using namespace DirectX;

	XMStoreFloat3A(m_pvUp, XMVectorLerp(	XMLoadFloat3A(m_pView1->getLocalUp()),
											XMLoadFloat3A(m_pView2->getLocalUp()),
											m_fParameter_t/m_fMorpTime));
	return m_pvPos;
}
const DirectX::XMFLOAT3A * CView_Morph::getLocalLook() const
{
	using namespace DirectX;
	
	XMStoreFloat3A(m_pvLook,XMVectorLerp(XMLoadFloat3A(m_pView1->getLocalLook()),
										XMLoadFloat3A(m_pView2->getLocalLook()),
										m_fParameter_t/m_fMorpTime));
	return m_pvLook;
}
//
//const DirectX::XMFLOAT3 & CView_Morph::getPosition() const
//{
//	using namespace DirectX;
//
//	XMStoreFloat3 (&m_vReturnValue,XMVectorLerp(XMLoadFloat3(&m_pView1->getPosition()),
//												XMLoadFloat3(&m_pView2->getPosition()), 
//												m_fParameter_t));
//	return m_vReturnValue;
//}
//
//const DirectX::XMFLOAT3 & CView_Morph::getLocalRight() const
//{
//	using namespace DirectX;
//
//	XMStoreFloat3(&m_vReturnValue,
//		XMVector3NormalizeEst(XMVectorLerp(	XMLoadFloat3(&m_pView1->getLocalRight()),
//											XMLoadFloat3(&m_pView2->getLocalRight()),
//											m_fParameter_t)));
//	return m_vReturnValue;
//}
//
//const DirectX::XMFLOAT3 & CView_Morph::getLocalUp() const
//{
//	using namespace DirectX;
//
//	XMStoreFloat3(&m_vReturnValue,
//		XMVector3NormalizeEst(XMVectorLerp(	XMLoadFloat3(&m_pView1->getLocalUp()),
//											XMLoadFloat3(&m_pView2->getLocalUp()),
//											m_fParameter_t)));
//	return m_vReturnValue;
//}
//
//const DirectX::XMFLOAT3 & CView_Morph::getLocalLook() const
//{
//	using namespace DirectX;
//
//	XMStoreFloat3(&m_vReturnValue,
//		XMVector3NormalizeEst(XMVectorLerp(	XMLoadFloat3(&m_pView1->getLocalLook()),
//											XMLoadFloat3(&m_pView2->getLocalLook()),
//											m_fParameter_t)));
//	return m_vReturnValue;
//}
//
//const DirectX::XMFLOAT3 & CView_Morph::getDirection() const
//{
//	using namespace DirectX;
//
//	XMStoreFloat3(&m_vReturnValue,
//		XMVector3NormalizeEst(XMVectorLerp(	XMLoadFloat3(&m_pView1->getDirection()),
//											XMLoadFloat3(&m_pView2->getDirection()),
//											m_fParameter_t)));
//	return m_vReturnValue;
//}

//----------------------------------- CCameraModeManager -------------------------------------------------------------------------------------

CCameraModeManager::CCameraModeManager()
	:m_eMode(ECameraMode::eDefault),
	m_bMorph(false)
{
}

IView * CCameraModeManager::setCameraMode(ECameraMode eMode, IGameObject *pObj)
{
	if (eMode == m_eMode)return m_ViewMap[m_eMode].first;
	auto iter = m_ViewMap.find(eMode);
	if(iter == m_ViewMap.end())
		return nullptr;
	if (iter->second.second <= 0)
	{
		m_eMode = eMode;
		iter->second.first->resetView(pObj);
		iter->second.first->move(&CMsg_Movement(EMsgType_Movement::eAuto));
		return iter->second.first;
	}
	else
	{
		iter->second.first->resetView(pObj);
		iter->second.first->move(&CMsg_Movement(EMsgType_Movement::eAuto));
		m_View_Morph.setView(m_ViewMap[m_eMode].first, iter->second.first, iter->second.second);
		m_eMode = eMode;
		m_bMorph = true;
		return &m_View_Morph;
	}

}

IView * CCameraModeManager::animateObject(const float fElapsedTime)
{
	if (!m_bMorph || !m_View_Morph.addT(fElapsedTime))
		return nullptr;
	else
	{
		m_bMorph = false;
		return m_ViewMap[m_eMode].first;
	}
}

void CCameraModeManager::setTarget_AllCamera(const IGameObject *pTarget)
{
	for (auto &rData : m_ViewMap)
		if (auto p = dynamic_cast<AView_Target*>(rData.second.first)) p->setTarget(pTarget);
}

void CCameraModeManager::move(const IMsg_Movement * pMoveMsg)
{
	m_ViewMap[m_eMode].first->move(pMoveMsg);
}

bool CCameraModeManager::createObject(IFactory_CameraModeManager * pFactory)
{
	m_ViewMap = pFactory->getViewMap();

	return !(m_ViewMap.empty());
}

void CCameraModeManager::releaseObject()
{
	for (auto &rData : m_ViewMap)
		delete rData.second.first;
	m_ViewMap.clear();
}



std::vector<IMsg*> CCamera_MLRS::animateObject(const float fElapsedTime)
{
	if (auto *pView = m_ModeManager.animateObject(fElapsedTime))
		m_pView = pView;
	return std::vector<IMsg*>();
}

bool CCamera_MLRS::createObject(IFactory_Camera_MLRS * pFactory)
{
	if (!m_ModeManager.createObject(pFactory))	return false;

	if (!SUPER::createObject(pFactory))	return false;
	return true;
}