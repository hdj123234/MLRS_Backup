#ifndef HEADER_CREATOR_ENEMYMECHA_MLRS
#define HEADER_CREATOR_ENEMYMECHA_MLRS

#include"EnemyMecha_MLRS.h"
#include"GlobalDataMap_MLRS.h"
#include"Mecha_MLRS.h"
#include"Type_MLRS.h"
#include"Interface_Object_MLRS.h"

//전방선언
struct CameraState_ThirdPerson;
class CMyINISection;

enum EActionType : ENUMTYPE_256;

class CFactory_EnemyMecha : public AFactory_Mecha,
							virtual public IFactory_Enemy {
private:
	typedef AFactory_Mecha SUPER;
private:
	const unsigned int  m_nNumberOfBone;
	const unsigned int  m_nNumberOfFrame;
	IGameModel *m_pModel;
public:
	CFactory_EnemyMecha(const std::string &rsName,
								const unsigned int  nNumberOfBone,
								const unsigned int  nNumberOfFrame,
								IGameModel *pGameModel);
	virtual ~CFactory_EnemyMecha() {}

	//override IFactory_GameObject_ModelBase
	virtual IGameModel * getModel() { return m_pModel; }

	//override IFactory_GameObject_ModelBase_Anim
	virtual	IAnimController * createAnimController();
	

	virtual ID3D11Buffer *  createConstBuffer_World()	{return SUPER::createConstBuffer_World();}
	void setName(const std::string &rsName)				{SUPER::setName(rsName);}
	virtual DirectX::XMFLOAT3  getPosition(){ return SUPER::getPosition(); }
	virtual DirectX::XMFLOAT3  getRight()	{ return SUPER::getRight(); }
	virtual DirectX::XMFLOAT3  getUp()		{ return SUPER::getUp(); }
	virtual DirectX::XMFLOAT3  getLook()	{ return SUPER::getLook(); }

//	static std::map<unsigned int, EActionType> getStartPointList();
//	static const CMyINISection getDefaultINISection();
};

class AMultiBuilder_EnemyMecha : virtual public IMultiBuilder<IEnemy>{
protected:
	IGameModel *m_pModel;

public:
	AMultiBuilder_EnemyMecha(IGameModel *pModel);
	virtual ~AMultiBuilder_EnemyMecha() {}

//private:
//	virtual IGameObject * build_ObjectCore() = 0;

public:
	//CMultiBuilder<IMainMecha>
//	virtual IEnemy * build();

};

//----------------------------------- 인스턴싱용 -------------------------------------------------------------------------------------
class CFactory_EnemyMecha_Zephyros_Instanced : public AFactory_Mecha_Instanced,
												virtual public IFactory_Enemy_Instanced{
private:
	IGameModel *m_pModel;

public:
	CFactory_EnemyMecha_Zephyros_Instanced(IGameModel *pGameModel) :AFactory_Mecha_Instanced(m_pModel), m_pModel(pGameModel) {}
	virtual ~CFactory_EnemyMecha_Zephyros_Instanced() {}

	//override IFactory_GameObject_ModelBase
	virtual IGameModel * getModel() { return m_pModel; }

	//override AFactory_Mecha_Instanced
	virtual DirectX::XMFLOAT3  getPosition()	{ return AFactory_Mecha_Instanced::getPosition(); }
	virtual DirectX::XMFLOAT3  getRight()		{ return AFactory_Mecha_Instanced::getRight(); }
	virtual DirectX::XMFLOAT3  getUp()			{ return AFactory_Mecha_Instanced::getUp(); }
	virtual DirectX::XMFLOAT3  getLook()		{ return AFactory_Mecha_Instanced::getLook(); }

	//override IFactory_Enemy_Instanced
	virtual EEnemyType getEnemyType() { return EEnemyType::eZephyros; }
};


class CMultiBuilder_EnemyMecha_Instanced : public AMultiBuilder_EnemyMecha {
private:
	RIGameModel_Instancing<Type_InstanceData_Mecha> *m_pModel_Instance;
	IFactory_Enemy_Instanced *m_pFactory;

public:
	CMultiBuilder_EnemyMecha_Instanced(IGameModel *pModel,
										IFactory_Enemy_Instanced *pFactory);
	virtual ~CMultiBuilder_EnemyMecha_Instanced();

public:
	virtual IEnemy * build();

//private:
//	virtual IGameObject * build_ObjectCore();
};
#endif