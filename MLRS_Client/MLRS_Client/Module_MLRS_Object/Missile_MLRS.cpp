#include "stdafx.h"
#include"Missile_MLRS.h"

#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

using namespace DirectX;

CMissile::CMissile()
	:	m_bEnable(true)
{
}

void CMissile::move(const IMsg_Movement * pMoveMsg)
{
	if (!m_bEnable)	return;
	if (!pMoveMsg)		return;

	switch (pMoveMsg->getMovementType())
	{
	case EMsgType_Movement::eReplace_PosAndDir:
	{
		const CMsg_Movement_PosAndDir *msg = static_cast<const CMsg_Movement_PosAndDir *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getPos();
		const XMFLOAT3 *pvDir = msg->getDir();
		XMVECTOR vLook = XMLoadFloat3(pvDir);
		XMVECTOR vUp = XMVectorSet(0,1,0,0);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		XMFLOAT3 vCheckEqual;
		XMStoreFloat3(&vCheckEqual, XMVectorEqual(vRight, XMVectorZero()));
		if (vCheckEqual.x&&vCheckEqual.y&&vCheckEqual.z)
		{
			vUp = XMVectorSet(0, 0, 1, 0);
			vRight = XMVector3Cross(vUp, vLook);
		}
		vRight = XMVector3NormalizeEst(vRight);
		vUp = XMVector3Cross(vLook, vRight);

		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
	}
	break;
	case EMsgType_Movement::eReplace_Position:
	{
		const RIMsg_Movement_GetVector3 *msg = dynamic_cast<const RIMsg_Movement_GetVector3 *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getVector();
		XMVECTOR vLook;
		if (pvPos->x == m_WorldData.m_rvPos.x && pvPos->y == m_WorldData.m_rvPos.y&& pvPos->z == m_WorldData.m_rvPos.z)
			vLook = XMLoadFloat3A(&m_WorldData.m_rvLook);
		else
			vLook = XMVector3NormalizeEst(XMVectorSet(pvPos->x - m_WorldData.m_rvPos.x, pvPos->y - m_WorldData.m_rvPos.y, pvPos->z - m_WorldData.m_rvPos.z, 1));
		XMVECTOR vRight = (XMVector3TransformCoord(vLook,XMMatrixRotationY(XMConvertToRadians(90))));
		XMFLOAT3 vCheckEqual;
		XMStoreFloat3(&vCheckEqual, XMVectorEqual(vLook, vRight));
		if (vCheckEqual.x&&vCheckEqual.y&&vCheckEqual.z)
			vRight = (XMVector3TransformCoord(vLook, XMMatrixRotationX(XMConvertToRadians(90))));
		
		XMVECTOR vUp = XMVector3NormalizeEst(XMVector3Cross(vLook, vRight));
		vRight = XMVector3Cross(vLook, vRight);
		vUp = XMVector3Cross(vUp,vLook);
		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
	}
	break;
	}
}

void CMissile::enable()
{
	m_bEnable = true;
	AGameObject_Instanced<Type_InstanceData_Mecha>::enable();
}

void CMissile::disable()
{
	m_bEnable = false;
	AGameObject_Instanced<Type_InstanceData_Mecha>::disable();
}

std::vector<Type_Fume_SprayPoint> CMissile::getSprayPoints() const
{
	Type_Fume_SprayPoint sprayPoint;
	sprayPoint.m_vDirection = DirectX::XMFLOAT3(-m_WorldData.m_rvLook.x, -m_WorldData.m_rvLook.y, -m_WorldData.m_rvLook.z);
	sprayPoint.m_vPosition = DirectX::XMFLOAT3(m_WorldData.m_rvPos.x, m_WorldData.m_rvPos.y, m_WorldData.m_rvPos.z);
	return std::vector<Type_Fume_SprayPoint>{sprayPoint};
}

void CMissile::updateMatrix()
{
	m_WorldData.getWorldMatrix(m_pInstanceData);

	m_BoundingObject.setOOBB(*m_pInstanceData);
}

bool CMissile::createObject(AFactory_Missile_Instanced * pFactory)
{
	if (!Super::createObject(pFactory))return false;
	auto &vPos = pFactory->getPosition();
	m_WorldData.m_rvPos.x = vPos.x;
	m_WorldData.m_rvPos.y = vPos.y;
	m_WorldData.m_rvPos.z = vPos.z;
	auto &vRight = pFactory->getRight();
	m_WorldData.m_rvRight.x = vRight.x;
	m_WorldData.m_rvRight.y = vRight.y;
	m_WorldData.m_rvRight.z = vRight.z;
	auto &vUp = pFactory->getUp();
	m_WorldData.m_rvUp.x = vUp.x;
	m_WorldData.m_rvUp.y = vUp.y;
	m_WorldData.m_rvUp.z = vUp.z;
	auto &vLook = pFactory->getLook();
	m_WorldData.m_rvLook.x = vLook.x;
	m_WorldData.m_rvLook.y = vLook.y;
	m_WorldData.m_rvLook.z = vLook.z;

	updateMatrix();

	if (auto p = dynamic_cast<RIGameModel_BoundingBoxGetter *>(m_pModel))
		m_BoundingObject.setData(p->getCenter(), p->getExtent());
	else
	{
		abort();
		return false;
	}

	return true;
}
