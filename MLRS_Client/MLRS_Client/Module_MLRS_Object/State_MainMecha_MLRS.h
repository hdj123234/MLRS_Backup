#ifndef HEADER_STATE_MAINMECHA_MLRS
#define HEADER_STATE_MAINMECHA_MLRS

#include"Interface_Object_MLRS.h"
#include"..\MyUtility.h"

class CMainMecha_Instanced;


enum class  EAnimState_MainMecha : ENUMTYPE_256 {
	eIdle = 0,
	eDance ,
	eMoving,
	eJump,
	eJump_Off,
	eBoost_On,
	eBoost_Off,
	eAttack1_On,
	eAttack1_Ready,
	eAttack1_Shoot,
	eAttack1_Off,
	eAttack2_On,
	eAttack2_Ready,
	eAttack2_Shoot,
	eAttack2_Off,
	eDummy2,
	eAttack3_Ready,
	eAttack3_Shoot,
	eAttack3_Off,
	eDead,
	eDummy3,
	eDummy4,
	eInterceptor,
	eRebound,
	eBoost_Back,
	eBoost_Left,
	eBoost_Right,
};

class AState_MainMecha  : public IState_Object<CMainMecha_Instanced>{
protected:
	typedef CMainMecha_Instanced CTarget;
	typedef AState_MainMecha AState;

protected:
	void setState(CTarget * const pObj, AState *const pState);
	virtual const EAnimState_MainMecha getActionID()const = 0;

public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)=0;
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};


class CState_MainMecha_Waiting : public AState_MainMecha {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eIdle); }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_MainMecha_Moving : public AState_MainMecha {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eMoving); }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);

};

class CState_MainMecha_BoostOn : public AState_MainMecha {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eBoost_On); }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime); 

};
class CState_MainMecha_BoostOff : public AState_MainMecha {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eBoost_Off); }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_MainMecha_JumpOn : public AState_MainMecha {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eJump); }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime); 

};

class CState_MainMecha_Jump : public AState_MainMecha {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eJump_Off); }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};

class CState_MainMecha_Attack1_On : public AState_MainMecha {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack1_On); }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime); 

};


class CState_MainMecha_Attack1 : public AState_MainMecha {
protected:
	virtual const EAnimState_MainMecha getActionID()const { return  (EAnimState_MainMecha::eAttack1_Ready); }
public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
	//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);
};


#endif