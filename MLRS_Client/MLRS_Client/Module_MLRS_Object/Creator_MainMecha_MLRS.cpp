#include "stdafx.h"
#include"Creator_MainMecha_MLRS.h"

#include"GlobalDataMap_MLRS.h"
#include"MainMecha_MLRS.h"

#include"..\Module_Object\Factory_AnimController.h"
#include"..\Module_Object_Camera\Factory_Camera_ThirdPerson.h"
#include"..\Module_Object_GameModel\GameModel.h"
#include"..\MyINI.h"


//----------------------------------- CFactory_MainMecha_Abattre_Instanced -------------------------------------------------------------------------------------

std::map<unsigned int, EActionType> CFactory_MainMecha_Abattre_Instanced::getStartPointList()
{
	std::map<unsigned int, EActionType> startPointList;

	startPointList.insert(std::make_pair(0,  EActionType::eActionType_Repeat));
	startPointList.insert(std::make_pair(1,  EActionType::eActionType_Repeat));
	startPointList.insert(std::make_pair(60, EActionType::eActionType_Repeat));
	startPointList.insert(std::make_pair(120, EActionType::eActionType_Keep));
	startPointList.insert(std::make_pair(150, EActionType::eActionType_Keep));
	startPointList.insert(std::make_pair(180, EActionType::eActionType_Keep));
	startPointList.insert(std::make_pair(210, EActionType::eActionType_Keep));
	startPointList.insert(std::make_pair(240, EActionType::eActionType_Default));
	startPointList.insert(std::make_pair(269, EActionType::eActionType_Keep));
	startPointList.insert(std::make_pair(270, EActionType::eActionType_Default));
	startPointList.insert(std::make_pair(300, EActionType::eActionType_Default));
	startPointList.insert(std::make_pair(330, EActionType::eActionType_Keep));
	startPointList.insert(std::make_pair(360, EActionType::eActionType_Default));
	startPointList.insert(std::make_pair(389, EActionType::eActionType_Keep));
	startPointList.insert(std::make_pair(390, EActionType::eActionType_Default));
	startPointList.insert(std::make_pair(420, EActionType::eActionType_Default));
	return startPointList;
}

const CMyINISection CFactory_MainMecha_Abattre_Instanced::getDefaultINISection()
{
	CMyINISection retval;
	retval.setParam("Filename", "Abattre/Abattre_Animation.fbx");
	retval.setParam("PosAndScale", "0,0,0, 1");
	return retval;
}

const CMyINISection CFactory_MainMecha_Abattre_Instanced::getDefaultINISection_Weapon1()
{
	CMyINISection retval; 
	retval.setParam("ShootDelay", "0.3");
	retval.setParam("RotationUnit_Y", "0.06");
	return retval;

}

const CMyINISection CFactory_MainMecha_Abattre_Instanced::getDefaultINISection_Weapon2()
{
	CMyINISection retval;
	retval.setParam("LaunchTime", "1");
	retval.setParam("MaxSpeed", "1125");
	retval.setParam("Acceleration", "800");
	retval.setParam("RotationUnit_Y", "0.06");
	return retval;
}

const CMyINISection CFactory_MainMecha_Abattre_Instanced::getDefaultINISection_Weapon3()
{
	CMyINISection retval;
	retval.setParam("LaunchTime", "3");
	retval.setParam("LockOnRange", "1000");
	
	return retval;
}


//----------------------------------- AMultiBuilder_MainMecha -------------------------------------------------------------------------------------
//
//AMultiBuilder_MainMecha::AMultiBuilder_MainMecha(
//	IGameModel * pModel, 
//	CameraState_MLRS & rCameraState,
//	CMyType_Guides &rGuides,
//	const float fShootDelay_Weapon1,
//	const float fLaunchTime_Weapon2,
//	const float fMaxSpeed_Weapon2,
//	const float fAcceleration_Weapon2,
//	const float fLaunchTime_Weapon3)
//	:m_pModel(pModel),
//	m_rCameraState(rCameraState),
//	m_Guides(rGuides),
//	m_fShootDelay_Weapon1(fShootDelay_Weapon1),
//	m_fLaunchTime_Weapon2(fLaunchTime_Weapon2),
//	m_fMaxSpeed_Weapon2(fMaxSpeed_Weapon2),
//	m_fAcceleration_Weapon2(fAcceleration_Weapon2),
//	m_fLaunchTime_Weapon3(fLaunchTime_Weapon3)
//{
//}

AMultiBuilder_MainMecha::AMultiBuilder_MainMecha(
	IGameModel * pModel,
	CameraState_MLRS & rCameraState,
	CMyType_Guides & rGuides,
	CMyINISection & rSection_Weapon1,
	CMyINISection & rSection_Weapon2,
	CMyINISection & rSection_Weapon3) 
	:m_pModel(pModel),
	m_rCameraState(rCameraState),
	m_Guides(rGuides)
{

	m_Retval_Weapon1.m_fShootDelay = rSection_Weapon1.getParam_float("ShootDelay");
	m_Retval_Weapon1.m_fRotationUnitY = rSection_Weapon1.getParam_float("RotationUnit_Y");

	m_Retval_Weapon1.m_fShootPressTime = 0;
	m_Retval_Weapon1.m_vPos = m_Guides.m_GuideMap[EMyType_Guide::eGuide_Weapon1].m_vPos;
	 

	m_Retval_Weapon2.m_fLaunchTime = rSection_Weapon2.getParam_float("LaunchTime");
	m_Retval_Weapon2.m_fMaxSpeed = rSection_Weapon2.getParam_float("MaxSpeed");
	m_Retval_Weapon2.m_fAcceleration = rSection_Weapon2.getParam_float("Acceleration");
	m_Retval_Weapon2.m_fRotationUnitY = rSection_Weapon2.getParam_float("RotationUnit_Y");

	m_Retval_Weapon2.m_fElapsedTime = 0;
	m_Retval_Weapon2.m_vPos = m_Guides.m_GuideMap[EMyType_Guide::eGuide_Weapon2].m_vPos;
 

	m_Retval_Weapon3.m_fLaunchTime = rSection_Weapon3.getParam_float("LaunchTime");
	m_Retval_Weapon3.m_fLockOnRange = rSection_Weapon3.getParam_float("LockOnRange");
	m_Retval_Weapon3.m_bShoot = false;
	m_Retval_Weapon3.m_fElapsedTime = 0;

}

EPlayerType AMultiBuilder_MainMecha::getType()
{
	return CGlobalDataMap_MLRS::getPlayerType(m_pModel);
}


IMainMecha * AMultiBuilder_MainMecha::build_ClientPlayer()
{
	CMainMecha_ClientPlayer * pPlayer = new CMainMecha_ClientPlayer;
	if (!pPlayer->createObject(this))
	{
		delete pPlayer;
		pPlayer = nullptr;
	}
	return pPlayer;
}

CCamera_MLRS * AMultiBuilder_MainMecha::createCamera()
{
	CCamera_MLRS *pCamera = new CCamera_MLRS();
	if (!pCamera->createObject(&CFactory_Camera_MLRS(m_rCameraState)))
	{
		delete pCamera;
		pCamera = nullptr;
	}
	return pCamera;
}
//
//const CType_Missile1 AMultiBuilder_MainMecha::getMissile1()
//{
//	CType_Missile1 retval;
//	retval.m_fShootDelay = m_fShootDelay_Weapon1;
//	retval.m_fRotationUnitY = m_rCameraState.m_fRotationUnitY_TPS;
//
//	retval.m_fShootPressTime = 0;
//	retval.m_vPos = m_Guides.m_GuideMap[EMyType_Guide::eGuide_Weapon1].m_vPos;
//	return retval;
//}
//
//const CType_Missile2 AMultiBuilder_MainMecha::getMissile2()
//{
//	CType_Missile2 retval;
//	retval.m_fLaunchTime = m_fLaunchTime_Weapon2;
//	retval.m_fMaxSpeed = m_fMaxSpeed_Weapon2;
//	retval.m_fAcceleration = m_fAcceleration_Weapon2 ;
//	retval.m_fRotationUnitY = m_rCameraState.m_fRotationUnitY_TPS;
//
//	retval.m_fElapsedTime = 0;
//	retval.m_vPos = m_Guides.m_GuideMap[EMyType_Guide::eGuide_Weapon2].m_vPos;
//	return retval;
//}
//
//const CType_Missile3 AMultiBuilder_MainMecha::getMissile3()
//{
//	CType_Missile3 retval;
//
//	retval.m_fLaunchTime = m_fLaunchTime_Weapon3;
//
//	retval.m_bShoot = false;
//	retval.m_fElapsedTime = 0;
//
//	return retval;
//}

 
//----------------------------------- CMultiBuilder_MainMecha_Instanced -------------------------------------------------------------------------------------
//
//CMultiBuilder_MainMecha_Instanced::CMultiBuilder_MainMecha_Instanced(
//	IGameModel * pModel, 
//	CameraState_MLRS & rCameraState,
//	CMyType_Guides &rGuides,
//	const float fShootDelay,
//	const float fLaunchTime)
//	:AMultiBuilder_MainMecha(pModel, rCameraState, rGuides, fShootDelay, fLaunchTime)
//{
//}

CMultiBuilder_MainMecha_Instanced::CMultiBuilder_MainMecha_Instanced(
	IGameModel * pModel, 
	CameraState_MLRS & rCameraState,
	CMyType_Guides & rGuides,
	CMyINISection & rSection_Weapon1,
	CMyINISection & rSection_Weapon2,
	CMyINISection & rSection_Weapon3)
	: AMultiBuilder_MainMecha(pModel, rCameraState, rGuides, 
		rSection_Weapon1,rSection_Weapon2,rSection_Weapon3)
{
}

IMainMecha * CMultiBuilder_MainMecha_Instanced::build()
{
	CMainMecha_Instanced *pObject = new CMainMecha_Instanced;
	if (!pObject->createObject(&CFactory_MainMecha_Abattre_Instanced(m_pModel)))
	{
		delete pObject;
		return nullptr;
	}
	return pObject;
}
