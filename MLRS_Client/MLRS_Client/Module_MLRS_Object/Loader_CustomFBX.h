#pragma once
#ifndef HEADER_LOADER_CUSTOMFBX
#define HEADER_LOADER_CUSTOMFBX

#include"..\..\FBXConverter\Type_MyGameModel.h"

class CLoader_CustomFBX{
private:
	CMyType_GameModel m_Model;
	DirectX::XMFLOAT3 m_vCenter;
	DirectX::XMFLOAT3 m_vExtents;
	std::string m_sPath;
public:
	CLoader_CustomFBX();
	~CLoader_CustomFBX();

private:
	bool loadMeshs(std::ifstream &rifs);
	bool loadBones(std::ifstream &rifs);
	bool loadAnimTransforms(std::ifstream &rifs);
	bool loadMaterials(std::ifstream &rifs);
	bool loadGuides(std::ifstream &rifs);

public:
	CMyType_GameModel & getModel() { return m_Model; }
	bool loadCustomFBX(const std::string &rsFileName);
	DirectX::XMFLOAT3 getCenter() { return m_vCenter; }
	DirectX::XMFLOAT3 getExtents() { return m_vExtents; }
	CMyType_Guides getGuides() { return m_Model.m_Guides; }
};

#endif