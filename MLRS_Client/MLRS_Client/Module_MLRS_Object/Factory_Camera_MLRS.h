#pragma once
#ifndef HEADER_FACTORY_CAMERA_MLRS
#define HEADER_FACTORY_CAMERA_MLRS

#include"Camera_MLRS.h"
#include"..\Module_Object_Camera\Factory_Camera.h"

//���漱��
class CMyINISection;
class CMyINIData;

struct CameraState_MLRS{
	CameraState_Projection m_cs_Projection;
	ViewState_ThirdPerson m_vs_Debug;
	ViewState_ThirdPerson_LimitRange m_vs_Default;
	ViewState_TPS m_vs_TPS;
	ViewState_TPS m_vs_Multiple;
	ViewState_FPS_PitchYaw m_vs_LockOn;
	ViewState_TPS m_vs_LockOn_Shoot;
//	float m_fRotationUnitY_TPS;
//	float m_fRotationUnitY_Multiple;
	float m_fMorpTime_Default;
	float m_fMorpTime_TPS;
	float m_fMorpTime_Multiple;
	float m_fMorpTime_LockOn;
	float m_fMorpTime_LockOn_Shoot;


	CameraState_MLRS() {}
	CameraState_MLRS(const CMyINIData& CMyINIData);

	static const CMyINISection getDefaultINISection_Perspective();
	static const CMyINISection getDefaultINISection_Default();
	static const CMyINISection getDefaultINISection_TPS();
	static const CMyINISection getDefaultINISection_Multiple();
	static const CMyINISection getDefaultINISection_LockOn();
	static const CMyINISection getDefaultINISection_LockOn_Shoot();
};

class CFactory_Camera_MLRS :	private AFactory_Camera,
								virtual public IFactory_Camera_MLRS {
private:
	CameraState_MLRS m_CameraState;
public:
	CFactory_Camera_MLRS(CameraState_MLRS &rCameraState);
	virtual ~CFactory_Camera_MLRS();

	virtual IView *createView() { return nullptr; }
	virtual IProjection *createProjection()				{return AFactory_Camera::createProjection(); }
	virtual ID3D11Buffer *createCB_Projection()		{return AFactory_Camera::createCB_Projection(); }
	virtual ID3D11Buffer *createCB_ProjectionInverse() { return AFactory_Camera::createCB_ProjectionInverse(); }
	virtual ID3D11Buffer *createCB_View()			{return AFactory_Camera::createCB_View(); }
	virtual ID3D11Buffer *createCB_CameraState()	{return AFactory_Camera::createCB_CameraState(); }

	virtual ViewMap getViewMap() ;
};

#endif