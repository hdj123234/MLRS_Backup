#include "stdafx.h"
#include"State_MainMecha_MLRS.h"

#include"ClientPlayer.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include "State_ClientPlayer_MLRS.h"

//DEBUG
#include"..\Module_Object\Space.h"

//----------------------------------- AState_ClientPlayer -------------------------------------------------------------------------------------
 
CSpace * AState_ClientPlayer::s_pSpace = nullptr;
 
void AState_ClientPlayer::processMsg_KeyFlag(CTarget * const pObj, CMsg_MLRS_KeyFlag * pMsg_KeyFlag)
{ 
	const FLAG_8 flag = pMsg_KeyFlag->getFlag();
	auto flag_old = pObj->m_KeyFlag;
	if (pMsg_KeyFlag->isOnFlag())
	{
		pObj->m_KeyFlag |= flag;
		if ((pObj->m_KeyFlag&FLAGMASK_KEY_MOVE) != (flag_old&FLAGMASK_KEY_MOVE))
		{
			pObj->m_bChangeDirection = true;
			if (flag&FLAGMASK_KEY_MOVE_DIR)
			{
				//이동에 대한 상대 비트 해제 
				if (pObj->m_KeyFlag&(FLAG_MOVEMENT_FRONT | FLAG_MOVEMENT_LEFT))
					pObj->m_KeyFlag &= (~((pObj->m_KeyFlag&(FLAG_MOVEMENT_FRONT | FLAG_MOVEMENT_LEFT)) << 1));
				else if (pObj->m_KeyFlag&(FLAG_MOVEMENT_BACK | FLAG_MOVEMENT_RIGHT))
					pObj->m_KeyFlag &= (~((pObj->m_KeyFlag&(FLAG_MOVEMENT_BACK | FLAG_MOVEMENT_RIGHT)) >> 1));
 			} 
		}

		
	}
	else
	{
		pObj->m_KeyFlag &= (~flag);
		if ((pObj->m_KeyFlag&FLAGMASK_KEY_MOVE_DIR) != (flag_old&FLAGMASK_KEY_MOVE_DIR))
		{
			pObj->m_bChangeDirection = true;
			pObj->m_bStopNow = (pObj->m_KeyFlag&FLAGMASK_KEY_MOVE) == 0;
 
		}

	}
}

void AState_ClientPlayer::processMsg_Action(CTarget * const pObj, CMsg_MLRS_Action * pMsg_Action)
{
	switch (pMsg_Action->getAction()) {
	case EAction_MLRS::eAction_DebugCamera:
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0));
		AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_DebugCamera>::getInstance());
		break;
	case EAction_MLRS::eAction_Weapon1:
		setCameraDirection(pObj);
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x01));
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0x01));
		pObj->m_pReturnMsgs.push_back(new CMsg_CS_ChangeWeapon(EWeaponType::eWeapon1));
		AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack1_On>::getInstance());
		break;
	case EAction_MLRS::eAction_Weapon2:
		setCameraDirection(pObj);
		pObj->m_bChangeDirection = true;
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0x02));
		pObj->m_pReturnMsgs.push_back(new CMsg_CS_ChangeWeapon(EWeaponType::eWeapon2));
		pObj->m_Missile2.m_fElapsedTime = 0;
		pObj->m_Missile2.m_bShoot = false;
		AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack2_On>::getInstance());
		break;
	case EAction_MLRS::eAction_Weapon3:
		setCameraDirection(pObj);
		pObj->m_bChangeDirection = true;
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x02));
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0x04));
		pObj->m_pReturnMsgs.push_back(new CMsg_CS_ChangeWeapon(EWeaponType::eWeapon3));
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_ControlPlayer(EMsgType_MLRS_ControlPlayer::eHideClient));
		//Check
		pObj->m_Missile3.m_Targets.clear();
		pObj->m_Missile3.m_bShoot = false;
		AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack3_On>::getInstance());
		break;
	default:
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0));
		break;
	}
}

void AState_ClientPlayer::processMsg_MovePacket(CTarget * const pObj, CMsg_SC_Move * pMsg_SC_Move)
{
	auto pMsg_Move = pMsg_SC_Move->getMovementMsg();
	// 이동 패킷 처리
	//	1. 객체 위치만 갱신
	//	2. 카메라 위치 조정
	//	3. 방향 조정


	//위치갱신
	DirectX::XMFLOAT3 vPos;
	switch (pMsg_Move->getMovementType()) {
	case eReplace_PosAndDir:
		vPos = *static_cast<CMsg_Movement_PosAndDir *>(pMsg_SC_Move->getMovementMsg())->getPos();
		break;
	case eReplace_Position:
		vPos = *static_cast<CMsg_Movement_Vector3 *>(pMsg_SC_Move->getMovementMsg())->getVector();
		break;
	}
	pObj->m_pMainMecha->move(&CMsg_Movement_Vector3(eReplace_PositionOnly, vPos));

	//카메라 위치 조정
	pObj->m_pCamera->move(&CMsg_Movement(EMsgType_Movement::eAuto));

	//방향 조정
	process_ResetDirection(pObj);
}

void AState_ClientPlayer::process_ResetDirection(CTarget * const pObj)
{
	if (pObj->m_KeyFlag&FLAGMASK_KEY_MOVE_DIR)
	{
		using namespace DirectX;

		XMFLOAT3 vDirection = pObj->m_pCamera->getDirection();
		vDirection.y = 0;
		XMVECTOR vFront = XMVector3NormalizeEst(XMLoadFloat3(&vDirection));
		XMVECTOR vRight = XMVector3TransformCoord(vFront, XMMatrixRotationY(XMConvertToRadians(90)));
		XMVECTOR vResult = DirectX::XMVectorSet(0, 0, 0, 0);
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_FRONT) != 0)*vFront;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_BACK) != 0)*-vFront;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_RIGHT) != 0)*vRight;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_LEFT) != 0)*-vRight;
		DirectX::XMStoreFloat3(&vDirection, DirectX::XMVector3NormalizeEst(vResult));
		pObj->m_pMainMecha->move(&CMsg_Movement_Vector3(EMsgType_Movement::eSetDirection, vDirection));
	}
}

void AState_ClientPlayer::setCameraDirection(CTarget * const pObj)
{
	using namespace DirectX;
	XMFLOAT3 vDir = pObj->getCamera()->getDirection();
	vDir.y = 0;
	XMStoreFloat3(&vDir, XMVector3NormalizeEst(XMLoadFloat3(&vDir)));
	pObj->move(&CMsg_Movement_Vector3(EMsgType_Movement::eSetDirection, vDir));
}
 
void AState_ClientPlayer::setState(CTarget * const pObj, AState * const pState)
{
	if (pState != pObj->m_pState)
	{
		pObj->m_pState_Past = pObj->m_pState;
		pObj->m_pState = pState;
		pObj->m_pMainMecha->getAnimController()->setAction(static_cast<unsigned int>(pState->getActionID()));
		pObj->m_pCamera->setCameraMode(pState->getCameraMode(), pObj);
//		pObj->m_pCamera->resetCamera(pObj);
	}
}

std::vector<IMsg*> AState_ClientPlayer::animateObject(CTarget * pObj, const float fElapsedTime)
{
	pObj->m_pMainMecha->CMecha_Instanced::animateObject(fElapsedTime);
	pObj->m_pCamera->animateObject(fElapsedTime);

	std::vector<IMsg*>retval;
	retval.swap(pObj->m_pReturnMsgs);
	return retval;
}

//----------------------------------- CState_ClientPlayer_Waiting -------------------------------------------------------------------------------------

void CState_ClientPlayer_Waiting::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	switch (pMsg_MLRS->getMsgType())	{
	case EMsgType_MLRS::eReceivedFromServer:
	{
		CMsg_SC *pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg);
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::ePlayerDown:
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Dead>::getInstance());
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			CMsg_SC_Move * pMsg_SC_Move = static_cast<CMsg_SC_Move *>(pMsg_SC);
			IMsg_Movement * pMsg_Move = pMsg_SC_Move->getMovementMsg();
 
			if (pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
			{
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Jump_On>::getInstance());
				CSingleton<CState_ClientPlayer_Jump_On>::getInstance()->handleMsg(pObj, pMsg);
			}
			else if (pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
			{
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Boost_On>::getInstance());
				CSingleton<CState_ClientPlayer_Boost_On>::getInstance()->handleMsg(pObj, pMsg);
			}
			else if (!pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
			{
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Moving>::getInstance());
				CSingleton<CState_ClientPlayer_Moving>::getInstance()->handleMsg(pObj, pMsg);
			}
			else
			{
				pObj->m_pMainMecha->move(pMsg_SC_Move->getMovementMsg());
				pObj->m_pCamera->move(&CMsg_Movement(EMsgType_Movement::eAuto));
			} 
			break;
		}
		break;
		}
	}
	case EMsgType_MLRS::eControlPlayer:
	{
		CMsg_MLRS_ControlPlayer *pMsg_CtlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtlPlayer->getMsgType_CtlPlayer()) {
		case EMsgType_MLRS_ControlPlayer::eKey_Flag:
			AState_ClientPlayer::processMsg_KeyFlag(pObj, static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eAction:
			AState_ClientPlayer::processMsg_Action(pObj, static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eCamera_Distance:
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Distance*>(pMsg_CtlPlayer)->getMovementMsg());
			break;
		case EMsgType_MLRS_ControlPlayer::eCamera_Rotate:
//			pObj->m_bChangeDirection = true;
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Rotate*>(pMsg_CtlPlayer)->getMovementMsg());
			process_ResetDirection(pObj); 
			break;
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		}
		break;
	}

	}
}

std::vector<IMsg*> CState_ClientPlayer_Waiting::animateObject(CTarget * pObj, const float fElapsedTime)
{
	pObj->m_pMainMecha->CMecha_Instanced::animateObject(fElapsedTime);
	pObj->m_pCamera->animateObject(fElapsedTime);


	std::vector<IMsg*> pMsgs;
	pMsgs.swap(pObj->m_pReturnMsgs);


	pObj->m_fBoostGauge += 60 * fElapsedTime;
	if (pObj->m_fBoostGauge > 180)pObj->m_fBoostGauge = 180;
	pMsgs.push_back(new CMsg_MLRS_UI_BoostGauge(pObj->m_fBoostGauge / 180));


	if (pObj->m_bStopNow)
	{
		pObj->m_bStopNow = false;
		pObj->m_bChangeDirection = false;
		CMsg_CS_Stop *pMsg = new CMsg_CS_Stop();
		pMsgs.push_back(pMsg);
	}
	else if (pObj->m_bChangeDirection)
	{
		pObj->m_bChangeDirection = false;
		using namespace DirectX;
		FLAG_8 moveFlag = 0;
		//MT_MOVE
		moveFlag |= static_cast<FLAG_8>((pObj->m_KeyFlag&FLAGMASK_KEY_MOVE) != 0);
		//MT_JUMP
		moveFlag |= static_cast<FLAG_8>((pObj->m_KeyFlag & FLAG_JUMP) != 0) << 1;
		//MT_BOOST
		moveFlag |= static_cast<FLAG_8>((pObj->m_KeyFlag & FLAG_BOOST) != 0) << 2;

		CMsg_CS_Move *pMsg = new CMsg_CS_Move(moveFlag);
		auto data = pObj->m_pCamera->getDirection();
		data.y = 0;
		XMFLOAT3 vDirection = XMFLOAT3(0, 0, 0);
		XMVECTOR vFront = XMVector3NormalizeEst(DirectX::XMLoadFloat3(&data));
		XMVECTOR vRight = DirectX::XMVector3TransformCoord(vFront, XMMatrixRotationY(XMConvertToRadians(90)));
		XMVECTOR vResult = DirectX::XMVectorSet(0, 0, 0, 0);
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_FRONT) != 0)*vFront;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_BACK) != 0)*-vFront;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_RIGHT) != 0)*vRight;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_LEFT) != 0)*-vRight;
		if (!XMVector3Equal(vResult, XMVectorZero()))
			DirectX::XMStoreFloat3(&vDirection, DirectX::XMVector3NormalizeEst(vResult));
		pMsg->m_vDirection = vDirection;
		pMsgs.push_back(pMsg);
	}
	return pMsgs;
}

//----------------------------------- CState_ClientPlayer_Moving -------------------------------------------------------------------------------------

void CState_ClientPlayer_Moving::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	switch (pMsg_MLRS->getMsgType()) {
	case EMsgType_MLRS::eReceivedFromServer:
	{
		CMsg_SC *pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg);
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::ePlayerDown:
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Dead>::getInstance());
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			CMsg_SC_Move * pMsg_SC_Move = static_cast<CMsg_SC_Move *>(pMsg_SC);
			IMsg_Movement * pMsg_Move = pMsg_SC_Move->getMovementMsg();
			if (pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
			{
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Jump_On>::getInstance());
				CSingleton<CState_ClientPlayer_Jump_On>::getInstance()->handleMsg(pObj, pMsg);
			}
			else if (pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
			{
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Boost_On>::getInstance());
				CSingleton<CState_ClientPlayer_Boost_On>::getInstance()->handleMsg(pObj, pMsg);
			}
			else if (pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
			{
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Waiting>::getInstance());
				CSingleton<CState_ClientPlayer_Waiting>::getInstance()->handleMsg(pObj, pMsg);
			}
			else
				AState_ClientPlayer::processMsg_MovePacket(pObj, pMsg_SC_Move);
			break;
		}
		break;
		}
	}
	case EMsgType_MLRS::eControlPlayer:
	{
		CMsg_MLRS_ControlPlayer *pMsg_CtlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtlPlayer->getMsgType_CtlPlayer()) {
		case EMsgType_MLRS_ControlPlayer::eCamera_Distance:
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Distance*>(pMsg_CtlPlayer)->getMovementMsg());
		case EMsgType_MLRS_ControlPlayer::eCamera_Rotate:
			pObj->m_bChangeDirection = true;
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Rotate*>(pMsg_CtlPlayer)->getMovementMsg());
			process_ResetDirection(pObj); 
		break; 
		case EMsgType_MLRS_ControlPlayer::eKey_Flag:
			AState_ClientPlayer::processMsg_KeyFlag(pObj, static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eAction:
			AState_ClientPlayer::processMsg_Action(pObj, static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		}
		break;
	}

	}
}
 

//----------------------------------- CState_ClientPlayer_Jump_On -------------------------------------------------------------------------------------

void CState_ClientPlayer_Jump::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	switch (pMsg_MLRS->getMsgType()) {
	case EMsgType_MLRS::eReceivedFromServer:
	{
		CMsg_SC *pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg);
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::ePlayerDown:
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Dead>::getInstance());
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			CMsg_SC_Move * pMsg_SC_Move = static_cast<CMsg_SC_Move *>(pMsg_SC);
			IMsg_Movement * pMsg_Move = pMsg_SC_Move->getMovementMsg();
 			if (pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
			{
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Boost_On>::getInstance());
				CSingleton<CState_ClientPlayer_Boost_On>::getInstance()->handleMsg(pObj, pMsg);
			}
			else if (!pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
			{
				if (pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
				{
					AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Waiting>::getInstance());
					CSingleton<CState_ClientPlayer_Waiting>::getInstance()->handleMsg(pObj, pMsg);
				}
				else
				{
					AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Moving>::getInstance());
					CSingleton<CState_ClientPlayer_Moving>::getInstance()->handleMsg(pObj, pMsg);
				}
			}
			else
				AState_ClientPlayer::processMsg_MovePacket(pObj, pMsg_SC_Move);
			break;
		}
		break;
		}
	}
	case EMsgType_MLRS::eControlPlayer:
	{
		CMsg_MLRS_ControlPlayer *pMsg_CtlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtlPlayer->getMsgType_CtlPlayer()) {
		case EMsgType_MLRS_ControlPlayer::eCamera_Distance:
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Distance*>(pMsg_CtlPlayer)->getMovementMsg());
			break;
		case EMsgType_MLRS_ControlPlayer::eCamera_Rotate:
			pObj->m_bChangeDirection = true;
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Rotate*>(pMsg_CtlPlayer)->getMovementMsg());
			process_ResetDirection(pObj); 
			break;
		case EMsgType_MLRS_ControlPlayer::eKey_Flag:
			AState_ClientPlayer::processMsg_KeyFlag(pObj, static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eAction:
			AState_ClientPlayer::processMsg_Action(pObj, static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		}
		break;
	}

	}
}
 

//----------------------------------- CState_ClientPlayer_Boost_On -------------------------------------------------------------------------------------

//----------------------------------- CState_ClientPlayer_Boost -------------------------------------------------------------------------------------

void CState_ClientPlayer_Boost::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	switch (pMsg_MLRS->getMsgType()) {
	case EMsgType_MLRS::eReceivedFromServer:
	{
		CMsg_SC *pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg);
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::ePlayerDown:
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Dead>::getInstance());
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			CMsg_SC_Move * pMsg_SC_Move = static_cast<CMsg_SC_Move *>(pMsg_SC);
			IMsg_Movement * pMsg_Move = pMsg_SC_Move->getMovementMsg();
 			if (!pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
			{
				if (pMsg_SC_Move->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
				{
					AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Waiting>::getInstance());
					CSingleton<CState_ClientPlayer_Waiting>::getInstance()->handleMsg(pObj, pMsg);
				}
				else
				{
					AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Moving>::getInstance());
					CSingleton<CState_ClientPlayer_Moving>::getInstance()->handleMsg(pObj, pMsg);
				}
			}
			else
				AState_ClientPlayer::processMsg_MovePacket(pObj, pMsg_SC_Move);
			break;
		}
		break;
		}
	}
	case EMsgType_MLRS::eControlPlayer:
	{
		CMsg_MLRS_ControlPlayer *pMsg_CtlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtlPlayer->getMsgType_CtlPlayer()) {
		case EMsgType_MLRS_ControlPlayer::eCamera_Distance:
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Distance*>(pMsg_CtlPlayer)->getMovementMsg());
			break;
		case EMsgType_MLRS_ControlPlayer::eCamera_Rotate:
			pObj->m_bChangeDirection = true;
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Rotate*>(pMsg_CtlPlayer)->getMovementMsg());
			process_ResetDirection(pObj);
			break;
		case EMsgType_MLRS_ControlPlayer::eKey_Flag:
			AState_ClientPlayer::processMsg_KeyFlag(pObj, static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eAction:
			AState_ClientPlayer::processMsg_Action(pObj, static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		}
		break;
	}

	}
}

std::vector<IMsg*> CState_ClientPlayer_Boost::animateObject(CTarget * pObj, const float fElapsedTime)
{ 
	auto retval = CState_ClientPlayer_Waiting::animateObject(pObj, fElapsedTime);
	pObj->m_fBoostGauge -= 180.0f * fElapsedTime;
	if (pObj->m_fBoostGauge < 0)pObj->m_fBoostGauge = 0;
//	retval.push_back(new CMsg_MLRS_UI_BoostGauge(pObj->m_fBoostGauge));
	retval.push_back(new CMsg_MLRS_UI_BoostGauge(pObj->m_fBoostGauge/180));
	return retval;
}

//----------------------------------- CState_ClientPlayer_Attack1_On -------------------------------------------------------------------------------------

void AState_ClientPlayer_Attack1::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	auto &rMissile1 = pObj->m_Missile1;
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	switch (pMsg_MLRS->getMsgType()) {
	case EMsgType_MLRS::eReceivedFromServer:
	{
		CMsg_SC *pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg);
		switch (pMsg_SC->getType()){
		case EMsgType_SC::ePlayerDown:
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Dead>::getInstance());
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			CMsg_SC_Move * pMsg_SC_Move = static_cast<CMsg_SC_Move *>(pMsg_SC);
			IMsg_Movement * pMsg_Move = pMsg_SC_Move->getMovementMsg();
			pObj->m_pMainMecha->move(&CMsg_Movement_Vector3(EMsgType_Movement::eReplace_PositionOnly, *static_cast<CMsg_Movement_PosAndDir *>(pMsg_Move)->getPos()));
			pObj->m_pCamera->move(&CMsg_Movement(EMsgType_Movement::eAuto));
			 
			break;
		}
		break;
		}
	}
	case EMsgType_MLRS::eControlPlayer:
	{
		CMsg_MLRS_ControlPlayer *pMsg_CtlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtlPlayer->getMsgType_CtlPlayer()) {
		case EMsgType_MLRS_ControlPlayer::eCamera_Rotate:
		{
			pObj->m_bChangeDirection = true;
			auto pVector2 = static_cast<CMsg_Movement_Vector2 *>(static_cast<CMsg_MLRS_CtlCamera_Rotate*>(pMsg_CtlPlayer)->getMovementMsg())->getVector();

			pObj->m_pMainMecha->move(&CMsg_Movement_Float(EMsgType_Movement::eRotate_Yaw, pVector2->y*rMissile1.m_fRotationUnitY));
			pObj->m_pCamera->move(&CMsg_Movement_Float(EMsgType_Movement::eRotate_Pitch, pVector2->x));
 			break;
		}
		case EMsgType_MLRS_ControlPlayer::eKey_Flag:
			AState_ClientPlayer::processMsg_KeyFlag(pObj, static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer));

			if (static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer)->getFlag()&FLAG_ATTACK)
			{
				if(static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer)->isOnFlag())
					pObj->m_Missile1.m_fShootPressTime = pObj->m_Missile1.m_fShootDelay;
				else
					pObj->m_Missile1.m_fShootPressTime = 0;
			}
			break;
		case EMsgType_MLRS_ControlPlayer::eAction:
			if (static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer)->getAction() == EAction_MLRS::eAction_Weapon1)
			{
				pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0));
				pObj->m_pReturnMsgs.push_back(new CMsg_CS_ChangeWeapon(EWeaponType::eNoWeapon));
				pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack1_Off>::getInstance());
				
 			}
			else
				AState_ClientPlayer::processMsg_Action(pObj, static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		}
		break;
	}

	}
}

std::vector<IMsg*> AState_ClientPlayer_Attack1::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto &rMissile1 = pObj->m_Missile1;
	pObj->m_pMainMecha->CMecha_Instanced::animateObject(fElapsedTime);
	pObj->m_pCamera->animateObject(fElapsedTime);

	std::vector<IMsg *> pMsgs;
	pMsgs.swap(pObj->m_pReturnMsgs); 
	if (pObj->m_bChangeDirection)
	{
		pObj->m_bChangeDirection = false;

		using namespace DirectX;

		auto data = pObj->m_pMainMecha->getDirection();
		XMFLOAT2 vLook;
		XMStoreFloat2(&vLook, XMVector2NormalizeEst(XMVectorSet(data.x, data.z, 0, 0)));
		XMFLOAT3 vMoveDirection = XMFLOAT3(0, 0, 0);
		XMVECTOR vFront = XMVectorSet(vLook.x, 0, vLook.y, 0);
		XMVECTOR vRight = DirectX::XMVector3TransformCoord(vFront, XMMatrixRotationY(XMConvertToRadians(90)));
		XMVECTOR vResult = DirectX::XMVectorSet(0, 0, 0, 0);
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_FRONT) != 0)*vFront;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_BACK) != 0)*-vFront;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_RIGHT) != 0)*vRight;
		vResult += static_cast<float>((pObj->m_KeyFlag&FLAG_MOVEMENT_LEFT) != 0)*-vRight;
		if (!XMVector3Equal(vResult, XMVectorZero()))
			DirectX::XMStoreFloat3(&vMoveDirection, DirectX::XMVector3NormalizeEst(vResult));

		FLAG_8 moveFlag = 0;
		//MT_MOVE
		moveFlag |= static_cast<FLAG_8>(((pObj->m_KeyFlag&(FLAG_MOVEMENT_FRONT | FLAG_MOVEMENT_BACK | FLAG_MOVEMENT_LEFT | FLAG_MOVEMENT_RIGHT)) != 0));
		//MT_JUMP
		moveFlag |= static_cast<FLAG_8>((pObj->m_KeyFlag & FLAG_JUMP) != 0) << 1;
		//MT_BOOST
		moveFlag |= static_cast<FLAG_8>((pObj->m_KeyFlag & FLAG_BOOST) != 0) << 2;

		pMsgs.push_back(new CMsg_CS_MoveAndAim_Weapon1(XMFLOAT2(vMoveDirection.x, vMoveDirection.z), vLook, XMConvertToRadians(90), moveFlag));
	}
	if (pObj->m_KeyFlag&FLAG_KEY::FLAG_ATTACK)
	{
		rMissile1.m_fShootPressTime += fElapsedTime;
		if (rMissile1.m_fShootPressTime >= rMissile1.m_fShootDelay)
		{
			using namespace DirectX;
			rMissile1.m_fShootPressTime -= rMissile1.m_fShootDelay;

			XMFLOAT3 pos;
			XMFLOAT3 dir;

			XMVECTOR vLook = DirectX::XMLoadFloat3(pObj->m_pMainMecha->getLookOrigin());
			XMVECTOR vUp = DirectX::XMLoadFloat3(pObj->m_pMainMecha->getUpOrigin());
			XMVECTOR vRight = DirectX::XMLoadFloat3(pObj->m_pMainMecha->getRightOrigin());

			XMVECTOR vMissilePos = XMLoadFloat3A(pObj->m_pMainMecha->getPositionOrigin());
			vMissilePos += vRight * rMissile1.m_vPos.x;
			vMissilePos += vUp * rMissile1.m_vPos.y;
			vMissilePos += vLook * rMissile1.m_vPos.z;
			XMStoreFloat3(&pos, vMissilePos);



			auto vDirection = pObj->m_pCamera->getDirection();
			XMVECTOR vPos = XMLoadFloat3(&pObj->m_pCamera->getPosition());

			//체크
			dynamic_cast<CSpace *>(pObj->s_pSpace)->setShootPos(&pos);

			XMFLOAT3 pickResult;
			if (pObj->s_pSpace->pick(CBoundingObject_Ray(pObj->m_pCamera->getPosition(), vDirection, 10000), pickResult))
				XMStoreFloat3(&dir, XMVector3NormalizeEst(XMLoadFloat3(&pickResult) - vMissilePos));

			else
				XMStoreFloat3(&dir, XMVector3NormalizeEst(vPos + (XMLoadFloat3(&vDirection) * 10000) - vMissilePos));

			pMsgs.push_back(new CMsg_CS_Attack1(pos, dir));
			pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_Sound(new CMsg_Sound_RunSE(3)));
 		}
	}
	return pMsgs;
}

//----------------------------------- CState_ClientPlayer_Attack1_Ready -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack1_Ready::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto retval = AState_ClientPlayer_Attack1::animateObject(pObj, fElapsedTime);

	if(AState_ClientPlayer::getKeyFlag(pObj)&FLAG_KEY::FLAG_ATTACK)
		setState(pObj, CSingleton<CState_ClientPlayer_Attack1_Shoot>::getInstance());

	return retval;
}

//----------------------------------- CState_ClientPlayer_Attack1_On -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack1_On::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto retval = AState_ClientPlayer_Attack1::animateObject(pObj, fElapsedTime);

	if (AState_ClientPlayer::getKeyFlag(pObj)&FLAG_KEY::FLAG_ATTACK)
		setState(pObj, CSingleton<CState_ClientPlayer_Attack1_Shoot>::getInstance());
	else if (AState_ClientPlayer::checkEndOfAnimation(pObj) )
		setState(pObj, CSingleton<CState_ClientPlayer_Attack1_Ready>::getInstance());
	return retval;
}

//----------------------------------- CState_ClientPlayer_Attack1_Shoot -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack1_Shoot::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto retval = AState_ClientPlayer_Attack1::animateObject(pObj, fElapsedTime);

	if (!(AState_ClientPlayer::getKeyFlag(pObj)&FLAG_KEY::FLAG_ATTACK))
		setState(pObj, CSingleton<CState_ClientPlayer_Attack1_Ready>::getInstance());
	else if (AState_ClientPlayer::checkEndOfAnimation(pObj))
		resetAnimation(pObj);

	return retval;
}

//----------------------------------- CState_ClientPlayer_Attack1_Off -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack1_Off::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto retval = CState_ClientPlayer_Waiting::animateObject(pObj, fElapsedTime);
	
	if (AState_ClientPlayer::checkEndOfAnimation(pObj))
	{
		AState_ClientPlayer::addReturnMsg(pObj, new CMsg_MLRS_UI_AimFlag(0x00));
		AState_ClientPlayer::addReturnMsg(pObj, new CMsg_MLRS_UI_WeaponFlag(0));
//		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
//		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0));

		//체크
		if (AState_ClientPlayer::getKeyFlag(pObj)&FLAG_KEY::FLAG_BOOST)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Boost>::getInstance());
		else if (AState_ClientPlayer::getKeyFlag(pObj)&FLAG_KEY::FLAG_JUMP)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Jump>::getInstance());
		else if (AState_ClientPlayer::getKeyFlag(pObj)&FLAGMASK_KEY_MOVE)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Moving>::getInstance());
		else
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Waiting>::getInstance());
	}
	return retval;
}

//----------------------------------- AState_ClientPlayer_Attack2 -------------------------------------------------------------------------------------

IMsg * AState_ClientPlayer_Attack2::shootMissile(CTarget * pObj)
{
	auto &rMissile2 = pObj->m_Missile2;
	if (rMissile2.m_bShoot && rMissile2.m_fElapsedTime == 0)
	{
		using namespace DirectX;

		XMFLOAT3 vPos;
		XMFLOAT3X3 mMissileLocal;
		//			XMFLOAT3 vRight;
		//			XMFLOAT3 vUp;
		//			XMFLOAT3 vLook;

		auto pvRight = pObj->m_pMainMecha->getRightOrigin();
		auto pvUp = pObj->m_pMainMecha->getUpOrigin();
		auto pvLook = pObj->m_pMainMecha->getLookOrigin();
		auto pvPos = pObj->m_pMainMecha->getPositionOrigin();
		XMMATRIX mLocal = XMMatrixSet(pvRight->x, pvRight->y, pvRight->z, 0,
			pvUp->x, pvUp->y, pvUp->z, 0,
			pvLook->x, pvLook->y, pvLook->z, 0,
			pvPos->x, pvPos->y, pvPos->z, 1);
		XMVECTOR vMissilePos = XMVector3TransformCoord(XMLoadFloat3(&rMissile2.m_vPos), mLocal);
		XMStoreFloat3(&vPos, vMissilePos);
		XMVECTOR vRight = XMLoadFloat3A(pvRight);
		XMVECTOR vLook = XMLoadFloat3(&pObj->getCamera()->getDirection());
		vLook = XMVector3Transform(vLook, XMMatrixRotationAxis(vRight, XMConvertToRadians(-30)));
		XMVECTOR vUp = XMVector3Cross(vLook, vRight);

		XMStoreFloat3x3(&mMissileLocal, XMMATRIX(vRight,vUp,vLook,XMVectorZero()));

		pObj->m_pReturnMsgs.push_back(CMsg_MLRS_UI_LaunchGuide_Parabola::createOffMsg());
		return 	new CMsg_CS_Attack2(vPos, mMissileLocal);

	}
	return nullptr;
}

void AState_ClientPlayer_Attack2::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{ 
	auto &rMissile2 = pObj->m_Missile2;
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	switch (pMsg_MLRS->getMsgType()) {
	case EMsgType_MLRS::eReceivedFromServer:
	{
		CMsg_SC *pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg);
		switch (pMsg_SC->getType()) {
		case EMsgType_SC::ePlayerDown:
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Dead>::getInstance());
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			CMsg_SC_Move * pMsg_SC_Move = static_cast<CMsg_SC_Move *>(pMsg_SC);
			IMsg_Movement * pMsg_Move = pMsg_SC_Move->getMovementMsg();
			pObj->m_pMainMecha->move(&CMsg_Movement_Vector3(EMsgType_Movement::eReplace_PositionOnly, *static_cast<CMsg_Movement_PosAndDir *>(pMsg_Move)->getPos()));
			pObj->m_pCamera->move(&CMsg_Movement(EMsgType_Movement::eAuto));

			break;
		}
		break;
		}
	}
	case EMsgType_MLRS::eControlPlayer:
	{
		CMsg_MLRS_ControlPlayer *pMsg_CtlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtlPlayer->getMsgType_CtlPlayer()) {
		case EMsgType_MLRS_ControlPlayer::eCamera_Rotate:
		{
			if (this->getActionID() == EAnimState_MainMecha::eAttack2_Shoot)
				return;
			pObj->m_bChangeDirection = true;
			auto pVector2 = static_cast<CMsg_Movement_Vector2 *>(static_cast<CMsg_MLRS_CtlCamera_Rotate*>(pMsg_CtlPlayer)->getMovementMsg())->getVector();
			pObj->m_pMainMecha->move(&CMsg_Movement_Float(EMsgType_Movement::eRotate_Yaw, pVector2->y*rMissile2.m_fRotationUnitY));
			pObj->m_pCamera->move(&CMsg_Movement_Float(EMsgType_Movement::eRotate_Pitch, pVector2->x));
			break;
		}
		case EMsgType_MLRS_ControlPlayer::eKey_Flag:
			if (static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer)->getFlag()&FLAG_ATTACK)
			{
				pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_Sound(new CMsg_Sound_RunSE(4)));
				rMissile2.m_bShoot = true;
				rMissile2.m_fElapsedTime = 0;
			}
			break;
		case EMsgType_MLRS_ControlPlayer::eAction:

			pObj->m_pReturnMsgs.push_back(CMsg_MLRS_UI_LaunchGuide_Parabola::createOffMsg());

			if (static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer)->getAction() == EAction_MLRS::eAction_Weapon2)
			{
				rMissile2.m_bShoot = false;
				rMissile2.m_fElapsedTime = 0;
				pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0));
				pObj->m_pReturnMsgs.push_back(new CMsg_CS_ChangeWeapon(EWeaponType::eNoWeapon));
				pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
				AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack2_Off>::getInstance());

			}
			else
			{
				rMissile2.m_bShoot = false;
				AState_ClientPlayer::processMsg_Action(pObj, static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer));
			}
			break;
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		}
		break;
	}

	}
}

std::vector<IMsg*> AState_ClientPlayer_Attack2::animateObject(CTarget * pObj, const float fElapsedTime)
{
	using namespace DirectX;

	auto &rMissile2 = pObj->m_Missile2;
	pObj->m_pMainMecha->CMecha_Instanced::animateObject(fElapsedTime);
	pObj->m_pCamera->animateObject(fElapsedTime);

	std::vector<IMsg *> pMsgs;
	pMsgs.swap(pObj->m_pReturnMsgs); 
	if (pObj->m_bChangeDirection)
	{ 
		pObj->m_bChangeDirection = false; 

		auto data = pObj->m_pMainMecha->getDirection();
		XMFLOAT2 vLook_Mecha;

		XMFLOAT3 vPos;
		XMFLOAT3X3 mMissileLocal;
		//			XMFLOAT3 vRight;
		//			XMFLOAT3 vUp;
		//			XMFLOAT3 vLook;

		auto pvRight = pObj->m_pMainMecha->getRightOrigin();
		auto pvUp = pObj->m_pMainMecha->getUpOrigin();
		auto pvLook = pObj->m_pMainMecha->getLookOrigin();
		auto pvPos = pObj->m_pMainMecha->getPositionOrigin();
		XMMATRIX mLocal = XMMatrixSet(pvRight->x, pvRight->y, pvRight->z, 0,
			pvUp->x, pvUp->y, pvUp->z, 0,
			pvLook->x, pvLook->y, pvLook->z, 0,
			pvPos->x, pvPos->y, pvPos->z, 1);
		XMVECTOR vMissilePos = XMVector3TransformCoord(XMLoadFloat3(&rMissile2.m_vPos), mLocal);
		XMStoreFloat3(&vPos, vMissilePos);
		XMVECTOR vRight = XMLoadFloat3A(pvRight);
		XMVECTOR vLook = XMLoadFloat3(&pObj->getCamera()->getDirection());
		vLook = XMVector3Transform(vLook, XMMatrixRotationAxis(vRight, XMConvertToRadians(-30)));
		XMVECTOR vUp = XMVector3Cross(vLook, vRight);

		XMStoreFloat3x3(&mMissileLocal, XMMATRIX(vRight, vUp, vLook, XMVectorZero()));

		std::vector<DirectX::XMFLOAT3> m_vVertices;

		const float fTimeUnit_Prediction = 1.0f / 60;
		XMVECTOR vDir = vLook;
		XMVECTOR vPos_Tmp = vMissilePos;

		XMVECTOR vDir_G = XMVectorSet(0, -1, 0, 0);
		XMFLOAT3 vPos_Store_Tmp;
		const float fMaxSpeed = rMissile2.m_fMaxSpeed*fTimeUnit_Prediction;
		const float fAcc = rMissile2.m_fAcceleration * fTimeUnit_Prediction* fTimeUnit_Prediction;
		const float fAcc_G = 9.8*fTimeUnit_Prediction;

		float fSpeed = 3;
		float fSpeed_G = 0;
		for (int i = 0; i < 1000; ++i)
		{
			//acc
			fSpeed = fSpeed + fAcc;
			if (fSpeed > fMaxSpeed)fSpeed = fMaxSpeed;

			//on_g
			if (i > 5)
			{
				fSpeed_G += fAcc_G;
				vPos_Tmp += vDir_G*fSpeed_G;

			}

			//move
			vPos_Tmp += vDir*fSpeed;



			XMStoreFloat3(&vPos_Store_Tmp, vPos_Tmp);
			m_vVertices.push_back(vPos_Store_Tmp);
			if (vPos_Store_Tmp.y < s_pSpace->getHeight_Terrain(vPos_Store_Tmp.x, vPos_Store_Tmp.z))
				break;
		}

		pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_LaunchGuide_Parabola(m_vVertices));

		XMStoreFloat2(&vLook_Mecha, XMVector2NormalizeEst(XMVectorSet(data.x, data.z, 0, 0)));

		pMsgs.push_back(new CMsg_CS_Aim_Weapon2(vLook_Mecha, XMConvertToRadians(90)));
	}
	if (rMissile2.m_bShoot)
	{
		rMissile2.m_fElapsedTime += fElapsedTime;
		if (rMissile2.m_fElapsedTime >= rMissile2.m_fLaunchTime)
		{
			pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0));
			pObj->m_pReturnMsgs.push_back(CMsg_MLRS_UI_LaunchGuide_Parabola::createOffMsg());
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack2_Off>::getInstance());


 		}
	}
	return pMsgs;
}

//----------------------------------- CState_ClientPlayer_Attack2_Off -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack2_On::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto pMsg = AState_ClientPlayer_Attack2::shootMissile(pObj);
	auto retval = AState_ClientPlayer_Attack2::animateObject(pObj, fElapsedTime);
	if(pMsg)
	{
		retval.push_back(pMsg);

		AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack2_Shoot>::getInstance());
	}
	if (AState_ClientPlayer::checkEndOfAnimation(pObj))
		AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack2_Ready>::getInstance());


	return retval;
}

//----------------------------------- CState_ClientPlayer_Attack2_Off -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack2_Ready::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto pMsg = AState_ClientPlayer_Attack2::shootMissile(pObj);
	auto retval = AState_ClientPlayer_Attack2::animateObject(pObj, fElapsedTime);
	if (pMsg)
	{
		retval.push_back(pMsg);

		AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack2_Shoot>::getInstance());
	}
	return retval;
}

//----------------------------------- CState_ClientPlayer_Attack2_Off -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack2_Off::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto retval = CState_ClientPlayer_Waiting::animateObject(pObj, fElapsedTime);
	
	if (AState_ClientPlayer::checkEndOfAnimation(pObj))
	{
		auto keyFlag = AState_ClientPlayer::getKeyFlag(pObj);
		//체크
		if (keyFlag&FLAG_KEY::FLAG_BOOST)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Boost>::getInstance());
		else if (keyFlag&FLAG_KEY::FLAG_JUMP)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Jump>::getInstance());
		else if (keyFlag&FLAGMASK_KEY_MOVE)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Moving>::getInstance());
		else
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Waiting>::getInstance());

	}
	return retval;
}
 

//----------------------------------- AState_ClientPlayer_Attack3 -------------------------------------------------------------------------------------

void AState_ClientPlayer_Attack3::setState(CTarget * const pObj, AState * const pState)
{
	pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI(EMsgType_MLRS_UI::eUI_ReleaseLockOn));
	AState_ClientPlayer::setState(pObj, pState);
}

void AState_ClientPlayer_Attack3::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	auto &rMissile3 = pObj->m_Missile3;
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	switch (pMsg_MLRS->getMsgType()) {
	case EMsgType_MLRS::eReceivedFromServer:
	{
		CMsg_SC *pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg);
		switch (pMsg_SC->getType()) {
		case EMsgType_SC::ePlayerDown:
			pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_ControlPlayer(EMsgType_MLRS_ControlPlayer::eShowClient));
			setState(pObj, CSingleton<CState_ClientPlayer_Dead>::getInstance());
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			CMsg_SC_Move * pMsg_SC_Move = static_cast<CMsg_SC_Move *>(pMsg_SC);
			IMsg_Movement * pMsg_Move = pMsg_SC_Move->getMovementMsg();
			pObj->m_pMainMecha->move(&CMsg_Movement_Vector3(EMsgType_Movement::eReplace_PositionOnly, *static_cast<CMsg_Movement_PosAndDir *>(pMsg_Move)->getPos()));
			pObj->m_pCamera->move(&CMsg_Movement(EMsgType_Movement::eAuto));

			break;
		}
		break;
		}
	}
	case EMsgType_MLRS::eControlPlayer:
	{
		CMsg_MLRS_ControlPlayer *pMsg_CtlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtlPlayer->getMsgType_CtlPlayer()) {
		case EMsgType_MLRS_ControlPlayer::eCamera_Rotate:
		{
			pObj->m_bChangeDirection = true; 
			auto pVector2 = static_cast<CMsg_Movement_Vector2 *>(static_cast<CMsg_MLRS_CtlCamera_Rotate*>(pMsg_CtlPlayer)->getMovementMsg())->getVector();
			 
			pObj->m_pCamera->move(&CMsg_Movement_Vector2(EMsgType_Movement::eRotate_PitchYaw, *pVector2));
			break;
		}
		case EMsgType_MLRS_ControlPlayer::eKey_Flag:
			if (static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer)->getFlag()&FLAG_ATTACK)
			{
				rMissile3.m_bShoot = true;

			}
			break;
		case EMsgType_MLRS_ControlPlayer::eAction:
			rMissile3.m_Targets.clear();
			pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_ControlPlayer(EMsgType_MLRS_ControlPlayer::eShowClient)); 
			if (static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer)->getAction() == EAction_MLRS::eAction_Weapon3)
			{
				pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0));
				pObj->m_pReturnMsgs.push_back(new CMsg_CS_ChangeWeapon(EWeaponType::eNoWeapon));
				pObj->m_pReturnMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
				setState(pObj, CSingleton<CState_ClientPlayer_Attack3_Off>::getInstance());

			}
			else
				AState_ClientPlayer::processMsg_Action(pObj, static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		}
		break;
	}

	}
}

std::vector<IMsg*> AState_ClientPlayer_Attack3::animateObject(CTarget * pObj, const float fElapsedTime)
{
	using namespace DirectX;

	auto &rMissile3 = pObj->m_Missile3;
	pObj->m_pMainMecha->CMecha_Instanced::animateObject(fElapsedTime);
	pObj->m_pCamera->animateObject(fElapsedTime);

	std::vector<IMsg *> pMsgs;
	pMsgs.swap(pObj->m_pReturnMsgs);
	if (rMissile3.m_bShoot)
	{

//		rMissile3.m_Targets.push_back(1024);
		//Check
		rMissile3.m_fElapsedTime = 0;
		if (rMissile3.m_Targets.empty())
		{
			pMsgs.push_back(new CMsg_MLRS_ControlPlayer(EMsgType_MLRS_ControlPlayer::eShowClient));
			pMsgs.push_back(new CMsg_CS_ChangeWeapon(EWeaponType::eNoWeapon));
			pMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
			pMsgs.push_back(new CMsg_MLRS_UI_WeaponFlag(0x00));
			setState(pObj, CSingleton<CState_ClientPlayer_Attack3_Off>::getInstance());
		}
		else
		{
			pMsgs.push_back(new CMsg_CS_Attack3(rMissile3.m_Targets));
			pMsgs.push_back(new CMsg_MLRS_ControlPlayer(EMsgType_MLRS_ControlPlayer::eShowClient));
			pMsgs.push_back(new CMsg_MLRS_UI_AimFlag(0x00));
			rMissile3.m_Targets.clear();
			setState(pObj, CSingleton<CState_ClientPlayer_Attack3_Shoot>::getInstance());
			pMsgs.push_back(new CMsg_MLRS_Sound(new CMsg_Sound_RunSE(5)));

		}
//		pObj->m_pReturnMsgs.push_back(new CMsg_CS_Attack3(vPos, mMissileLocal));
//		pObj->m_pReturnMsgs.push_back(CMsg_MLRS_UI_LaunchGuide_Parabola::createOffMsg());
//		rMissile2.m_fElapsedTime += fElapsedTime;
//		if (rMissile2.m_fElapsedTime >= rMissile2.m_fLaunchTime)
//		{
//		}
	}
	else
	{
		//Check
		//Lock-On
		if(rMissile3.m_Targets.size()>rMissile3.s_nMaxNumberOfTarget)
			return pMsgs;

		auto pTmp = s_pSpace->pickObject(	CBoundingObject_Ray(pObj->getCamera()->getPosition(),
										pObj->getCamera()->getDirection(),
										rMissile3.m_fLockOnRange), ECollisionObjType::eCollisionObjType_Enemy);
		if(auto p = dynamic_cast<RIGameObject_IDGetter *>(pTmp))
		{
			auto nID = p->getID();
			bool bCheck = false;
			for (auto data : rMissile3.m_Targets)
			{
				if (data == nID)
				{
					bCheck = true;
					break;
				}
			}
			if (!bCheck)
			{
				CProxy<RIGameObject_PositionGetter> *pProxy = nullptr;
				if (auto pMecha = dynamic_cast<CMecha_Instanced *>(p))
					pMsgs.push_back(new CMsg_MLRS_UI_LockOn(pMecha->getPositionProxy()));
				rMissile3.m_Targets.push_back(nID);

				//Check 조준시 소리
				pMsgs.push_back(new CMsg_MLRS_Sound(new CMsg_Sound_RunSE(3)));
			}
		}
		

	}

	return pMsgs;
}

//----------------------------------- CState_ClientPlayer_Attack3_On -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack3_On::animateObject(CTarget * pObj, const float fElapsedTime)
{
//	auto pMsg = AState_ClientPlayer_Attack3::shootMissile(pObj);
	auto retval = AState_ClientPlayer_Attack3::animateObject(pObj, fElapsedTime);
	//if (pMsg)
	//{
	//	retval.push_back(pMsg);
	//
	//	AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack2_Shoot>::getInstance());
	//}
	//if (AState_ClientPlayer::checkEndOfAnimation(pObj))
	//	AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Attack3_Ready>::getInstance());


	return retval;
}

//----------------------------------- CState_ClientPlayer_Attack3_Shoot -------------------------------------------------------------------------------------

std::vector<IMsg*> CState_ClientPlayer_Attack3_Shoot::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto &rMissile3 = pObj->m_Missile3;
	pObj->m_pMainMecha->CMecha_Instanced::animateObject(fElapsedTime);
	pObj->m_pCamera->animateObject(fElapsedTime);

	std::vector<IMsg *> pMsgs;
	pMsgs.swap(pObj->m_pReturnMsgs);

	rMissile3.m_fElapsedTime += fElapsedTime;
	if (rMissile3.m_fElapsedTime > rMissile3.m_fLaunchTime)
	{ 
		auto keyFlag = AState_ClientPlayer::getKeyFlag(pObj);
		pMsgs.push_back(new CMsg_CS_ChangeWeapon(EWeaponType::eNoWeapon));
		//체크
		if (keyFlag&FLAG_KEY::FLAG_BOOST)
			setState(pObj, CSingleton<CState_ClientPlayer_Boost>::getInstance());
		else if (keyFlag&FLAG_KEY::FLAG_JUMP)
			setState(pObj, CSingleton<CState_ClientPlayer_Jump>::getInstance());
		else if (keyFlag&FLAGMASK_KEY_MOVE)
			setState(pObj, CSingleton<CState_ClientPlayer_Moving>::getInstance());
		else
			setState(pObj, CSingleton<CState_ClientPlayer_Waiting>::getInstance());
	}

	return pMsgs;
}

//----------------------------------- CState_ClientPlayer_Attack3_Off -------------------------------------------------------------------------------------

void CState_ClientPlayer_Attack3_Off::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	if (pMsg_MLRS->getMsgType() == EMsgType_MLRS::eControlPlayer)
	{
		auto pMsg_CtrlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtrlPlayer->getMsgType_CtlPlayer())
		{
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		default:
			CState_ClientPlayer_Waiting::handleMsg(pObj, pMsg);
			break;
		}
	} 
	else
		CState_ClientPlayer_Waiting::handleMsg(pObj, pMsg);
}

std::vector<IMsg*> CState_ClientPlayer_Attack3_Off::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto retval = CState_ClientPlayer_Waiting::animateObject(pObj, fElapsedTime);

	if (AState_ClientPlayer::checkEndOfAnimation(pObj))
	{
		auto keyFlag = AState_ClientPlayer::getKeyFlag(pObj);
		//체크
		if (keyFlag&FLAG_KEY::FLAG_BOOST)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Boost>::getInstance());
		else if (keyFlag&FLAG_KEY::FLAG_JUMP)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Jump>::getInstance());
		else if (keyFlag&FLAGMASK_KEY_MOVE)
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Moving>::getInstance());
		else
			AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_Waiting>::getInstance());

	}
	return retval;
}

//----------------------------------- AState_ClientPlayer_Dead -------------------------------------------------------------------------------------

void AState_ClientPlayer_Dead::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	switch (pMsg_MLRS->getMsgType()) {
	case EMsgType_MLRS::eControlPlayer:
	{
		CMsg_MLRS_ControlPlayer *pMsg_CtlPlayer = static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS);
		switch (pMsg_CtlPlayer->getMsgType_CtlPlayer()) {
		case EMsgType_MLRS_ControlPlayer::eCamera_Distance:
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Distance*>(pMsg_CtlPlayer)->getMovementMsg());
		case EMsgType_MLRS_ControlPlayer::eCamera_Rotate:
			pObj->m_pCamera->move(static_cast<CMsg_MLRS_CtlCamera_Rotate*>(pMsg_CtlPlayer)->getMovementMsg());
			break;
		case EMsgType_MLRS_ControlPlayer::eKey_Flag:
			AState_ClientPlayer::processMsg_KeyFlag(pObj, static_cast<CMsg_MLRS_KeyFlag *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eAction:
			AState_ClientPlayer::processMsg_Action(pObj, static_cast<CMsg_MLRS_Action *>(pMsg_CtlPlayer));
			break;
		case EMsgType_MLRS_ControlPlayer::eHideClient:
			pObj->hideThis();
			break;
		case EMsgType_MLRS_ControlPlayer::eShowClient:
			pObj->showThis();
			break;
		}
		break;
	}

	}
}

std::vector<IMsg*> AState_ClientPlayer_Dead::animateObject(CTarget * pObj, const float fElapsedTime)
{
	pObj->m_pMainMecha->CMecha_Instanced::animateObject(fElapsedTime);
	pObj->m_pCamera->animateObject(fElapsedTime);

	std::vector<IMsg *> pMsgs;
	pMsgs.swap(pObj->m_pReturnMsgs);
	return pMsgs;
}

//----------------------------------- CState_ClientPlayer_Dead -------------------------------------------------------------------------------------
 
std::vector<IMsg*> CState_ClientPlayer_Dead::animateObject(CTarget * pObj, const float fElapsedTime)
{
	auto pMsgs = AState_ClientPlayer_Dead::animateObject(pObj, fElapsedTime);
	if (AState_ClientPlayer::checkEndOfAnimation(pObj))
	{

		pMsgs.push_back(new CMsg_MLRS_ControlPlayer(EMsgType_MLRS_ControlPlayer::eHideClient));
		AState_ClientPlayer::setState(pObj, CSingleton<CState_ClientPlayer_GameOver>::getInstance());
	}
	return pMsgs;
}


//----------------------------------- CState_ClientPlayer_DebugCamera -------------------------------------------------------------------------------------

void CState_ClientPlayer_DebugCamera::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	CMsg_MLRS *pMsg_MLRS = static_cast<CMsg_MLRS *>(pMsg);
	if (pMsg_MLRS->getMsgType() == EMsgType_MLRS::eControlPlayer
		&& static_cast<CMsg_MLRS_ControlPlayer *>(pMsg_MLRS)->getMsgType_CtlPlayer() == EMsgType_MLRS_ControlPlayer::eAction
		&& static_cast<CMsg_MLRS_Action *>(pMsg_MLRS)->getAction() == EAction_MLRS::eAction_DebugCamera)
		setState(pObj, CSingleton<CState_ClientPlayer_Waiting>::getInstance());
	
	else
		CSingleton<CState_ClientPlayer_Moving>::getInstance()->handleMsg(pObj,pMsg);
}
