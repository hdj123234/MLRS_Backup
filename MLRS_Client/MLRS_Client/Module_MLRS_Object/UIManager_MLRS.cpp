#include "stdafx.h"
#include "UIManager_MLRS.h"

#include "..\Module_MLRS\Msg_InGame_MLRS.h"
#include "..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include "..\Module_Object_UI\UI_Gauge_Background.h"
#include "..\Module_Object_UI\UI_Simple_ScreenBase.h"
#include "..\Module_Object_UI\UI_LaunchGuide_Parabola.h"
#include "..\Module_Object_UI\UI_Switch.h"
#include "..\Module_Object_UI\UI_ScriptManager.h"
#include "..\Module_Object_UI\UI_Billboards.h"

void CUIManager_MLRS::handleMsg(IMsg_GameObject * pMsg)
{
	if (auto pMsg_MLRS = dynamic_cast<CMsg_MLRS *>(pMsg))
	{

		switch (pMsg_MLRS->getMsgType())
		{
		case EMsgType_MLRS::eReceivedFromServer:
		{
			auto pMsg_SC = static_cast<CMsg_SC *>(pMsg_MLRS);
			switch (pMsg_SC->getType())
			{
			case EMsgType_SC::eDrawScript:
				static_cast<CUI_ScriptManager *>(getUI(EUIType::eScript))->setScript(static_cast<CMsg_SC_DrawScript*>(pMsg_SC)->getScriptID());
				break;
			case EMsgType_SC::eHP:
				static_cast<CUI_Gauge_Background *>(getUI(EUIType::eGuage_HP))->setGauge(static_cast<CMsg_SC_HP*>(pMsg_SC)->getHP());
				break;
			}

			break;
		}
		case EMsgType_MLRS::eUI:
		{ 
			auto pMsg_UI = static_cast<CMsg_MLRS_UI *>(pMsg_MLRS);
			switch (pMsg_UI->getMsgType_UI())
			{

			case EMsgType_MLRS_UI::eUI_AimFlag:
				static_cast<CUI_Switch<2> *>(getUI(EUIType::eAim))->setFlag(static_cast<CMsg_MLRS_UI_AimFlag *>(pMsg_MLRS)->getFlag());

				break;
			case EMsgType_MLRS_UI::eUI_WeaponFlag:
				static_cast<CUI_Switch<3> *>(getUI(EUIType::eSwitch_Weapon))->setFlag(static_cast<CMsg_MLRS_UI_WeaponFlag *>(pMsg_MLRS)->getFlag());
				break;

			case EMsgType_MLRS_UI::eUI_LaunchGuide_Parabola:
				if (static_cast<CMsg_MLRS_UI_LaunchGuide_Parabola *>(pMsg_MLRS)->isOn())
				{
					static_cast<CUI_LaunchGuide_Parabola *>(getUI(EUIType::eLaunchGuide))->setLaunchGuide(static_cast<CMsg_MLRS_UI_LaunchGuide_Parabola *>(pMsg_MLRS)->getVertices_RVR());
					static_cast<CUI_LaunchGuide_Parabola *>(getUI(EUIType::eLaunchGuide))->enable();
				}
				else
					static_cast<CUI_LaunchGuide_Parabola *>(getUI(EUIType::eLaunchGuide))->disable();
			
				break;
			case EMsgType_MLRS_UI::eUI_LockOn:
				static_cast<CUI_Billboards*>(getUI(EUIType::eLockOn))->enable();
				static_cast<CUI_Billboards*>(getUI(EUIType::eLockOn))->addInstance(static_cast<CMsg_MLRS_UI_LockOn *>(pMsg_MLRS)->getProxy());
				break;
			case EMsgType_MLRS_UI::eUI_ReleaseLockOn:
				static_cast<CUI_Billboards*>(getUI(EUIType::eLockOn))->disable();
				break;
			case EMsgType_MLRS_UI::eUI_BoostGauge:
				static_cast<CUI_Gauge_Background*>(getUI(EUIType::eGuage_Boost))->setGauge(static_cast<CMsg_MLRS_UI_BoostGauge*>(pMsg_MLRS)->getGauge());
				break; 
			}
			break;
		}
		}
	}
}
