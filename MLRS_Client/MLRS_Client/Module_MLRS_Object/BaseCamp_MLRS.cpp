#include "stdafx.h"
#include "BaseCamp_MLRS.h"

#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

using namespace DirectX;

//----------------------------------- AFactory_Mecha -------------------------------------------------------------------------------------



//----------------------------------- CBaseCamp -------------------------------------------------------------------------------------

CBaseCamp::CBaseCamp()
	:m_bEnable(true)
{
}

void CBaseCamp::drawReady(CRendererState_D3D * pRendererState) const
{
	CBufferUpdater::update(pRendererState->getDeviceContext(), m_pCBuffer_World, m_WorldData.m_rmWorld);
}


void CBaseCamp::move(const IMsg_Movement * pMoveMsg)
{
	if (!m_bEnable)	return;
	if (!pMoveMsg)		return;

	switch (pMoveMsg->getMovementType())
	{
//	case EMsgType_Movement::eRotate_Yaw:
//	{
//		const CMsg_Movement_Float *msg = dynamic_cast<const CMsg_Movement_Float *>(pMoveMsg);
//		const float fAngle = msg->getData();
//
//		XMVECTOR vRight = XMLoadFloat3(&m_WorldData.m_rvRight);
//		XMVECTOR vUp = XMLoadFloat3(&m_WorldData.m_rvUp);
//		XMVECTOR vLook = XMLoadFloat3(&m_WorldData.m_rvLook);
//
//		XMMATRIX mRotation = XMMatrixRotationAxis(vUp, fAngle / 100);
//		XMStoreFloat3A(&m_WorldData.m_rvRight, XMVector3Transform(vRight, mRotation));
//		XMStoreFloat3A(&m_WorldData.m_rvUp, XMVector3Transform(vUp, mRotation));
//		XMStoreFloat3A(&m_WorldData.m_rvLook, XMVector3Transform(vLook, mRotation));
//		updateMatrix();
//		break;
//	}
//	case EMsgType_Movement::eReplace_PositionOnly:
//	{
//		const RIMsg_Movement_GetVector3 *msg = dynamic_cast<const RIMsg_Movement_GetVector3 *>(pMoveMsg);
//		const XMFLOAT3 *pvPos = msg->getVector();
//
//		m_WorldData.m_rvPos.x = pvPos->x;
//		m_WorldData.m_rvPos.y = pvPos->y;
//		m_WorldData.m_rvPos.z = pvPos->z;
//		updateMatrix();
//		break;
//	}
//	case EMsgType_Movement::eReplace_Position:
//	{
//		const RIMsg_Movement_GetVector3 *msg = dynamic_cast<const RIMsg_Movement_GetVector3 *>(pMoveMsg);
//		const XMFLOAT3 *pvPos = msg->getVector();
//		XMVECTOR vLook;
//		if (pvPos->x == m_WorldData.m_rvPos.x && pvPos->z == m_WorldData.m_rvPos.z)
//			vLook = XMLoadFloat3A(&m_WorldData.m_rvLook);
//		else
//			vLook = XMVector3NormalizeEst(XMVectorSet(pvPos->x - m_WorldData.m_rvPos.x, 0, pvPos->z - m_WorldData.m_rvPos.z, 1));
//		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
//		XMVECTOR vRight = (XMVector3Cross(vUp, vLook));
//		m_WorldData.m_rvPos.x = pvPos->x;
//		m_WorldData.m_rvPos.y = pvPos->y;
//		m_WorldData.m_rvPos.z = pvPos->z;
//		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
//		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
//		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
//		updateMatrix();
//		break;
//	}
	case EMsgType_Movement::eReplace_PosAndDir:
	{
		const RIMsg_Movement_GetPosAndDir *msg = dynamic_cast<const RIMsg_Movement_GetPosAndDir *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getPos();
		const XMFLOAT3 *pvDir = msg->getDir();
		XMFLOAT3 vDir = XMFLOAT3(pvDir->x, 0, pvDir->z);
		XMVECTOR vLook = XMVector3NormalizeEst(XMLoadFloat3(&vDir));
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
		break;
	}
//	case EMsgType_Movement::eSetDirection:
//	{
//		const CMsg_Movement_Vector3 *msg = dynamic_cast<const CMsg_Movement_Vector3 *>(pMoveMsg);
//		const XMFLOAT3 *pDirection = msg->getVector();
//
//		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
//		XMVECTOR vLook = XMVector3Normalize(XMLoadFloat3(pDirection));
//		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
//		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
//		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
//		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
//		updateMatrix();
//		break;
//	}
	}
}

std::vector<IMsg *> CBaseCamp::animateObject(const float fElapsedTime)
{
	if (m_eAnimState != m_eAnimState_Past)
	{
		m_eAnimState_Past = m_eAnimState;
		Super::setAction(m_eAnimState);
	}
	return  Super::animateObject(fElapsedTime);
}

void CBaseCamp::updateMatrix()
{
	//	if (!m_pInstanceData)return;
	m_WorldData.updateWorldMatrix();

	m_BoundingObject.setOOBB(m_WorldData.m_rmWorld);
}

bool CBaseCamp::createObject(IFactory_BaseCamp * pFactory)
{
	if (!Super::createObject(pFactory))return false;
	m_WorldData.m_rvPos.x =		0;
	m_WorldData.m_rvPos.y =		0;
	m_WorldData.m_rvPos.z =		0;
	m_WorldData.m_rvRight.x =	1;
	m_WorldData.m_rvRight.y =	0;
	m_WorldData.m_rvRight.z =	0;
	m_WorldData.m_rvUp.x =		0;
	m_WorldData.m_rvUp.y =		1;
	m_WorldData.m_rvUp.z =		0;
	m_WorldData.m_rvLook.x =	0;
	m_WorldData.m_rvLook.y =	0;
	m_WorldData.m_rvLook.z =	1;

	m_pCBuffer_World = pFactory->createConstBuffer_World();

	updateMatrix();

	if (auto p = dynamic_cast<RIGameModel_BoundingBoxGetter *>(m_pModel))
	{
		//Check 바운딩박스 확대
//		m_BoundingObject.setData(p->getCenter(), p->getExtent());
		auto vExtents = p->getExtent();
		vExtents.x *= 1.5;
		vExtents.y *= 1.5;
		vExtents.z *= 1.5;
		m_BoundingObject.setData(p->getCenter(), vExtents);
	}
	else
	{
		abort();
		return false;
	}

	return true;
}