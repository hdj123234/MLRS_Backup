#include "stdafx.h"
#include"Creator_EnemyMecha_MLRS.h"

#include"GlobalDataMap_MLRS.h"

#include"EnemyMecha_MLRS.h"

#include"..\Module_Object\Factory_AnimController.h"
#include"..\Module_Object_GameModel\GameModel.h"
#include"..\MyINI.h"

CFactory_EnemyMecha::CFactory_EnemyMecha(
	const std::string &rsName,
	const unsigned int  nNumberOfBone,
	const unsigned int  nNumberOfFrame,
	IGameModel * pModel)
	:AFactory_Mecha(rsName),
	m_nNumberOfBone(nNumberOfBone),
	m_nNumberOfFrame(nNumberOfFrame),
	m_pModel(pModel)
{
}

IAnimController * CFactory_EnemyMecha::createAnimController()
{
//	std::map<unsigned int, EActionType> startPointList = CGlobalDataMap_MLRS::getStartPointList(CGlobalDataMap_MLRS::getEnemyType(m_pModel)) ;


	CAnimController *pAnimController = new CAnimController;
	if (!pAnimController->createObject(&CFactory_AnimController(m_pModel)))
	{
		delete pAnimController;
		pAnimController = nullptr;
	}
	return pAnimController;
}



//----------------------------------- AMultiBuilder_EnemyMecha -------------------------------------------------------------------------------------
AMultiBuilder_EnemyMecha::AMultiBuilder_EnemyMecha(IGameModel * pModel)
	:m_pModel(pModel)
{
}

//----------------------------------- CMultiBuilder_EnemyMecha_Instanced -------------------------------------------------------------------------------------

CMultiBuilder_EnemyMecha_Instanced::CMultiBuilder_EnemyMecha_Instanced(IGameModel * pModel,
	IFactory_Enemy_Instanced *pFactory)
	:AMultiBuilder_EnemyMecha(pModel),
	m_pModel_Instance(dynamic_cast<RIGameModel_Instancing<Type_InstanceData_Mecha> *>(pModel)),
	m_pFactory(pFactory)
{
}

CMultiBuilder_EnemyMecha_Instanced::~CMultiBuilder_EnemyMecha_Instanced()
{ 
	delete m_pFactory; 
}

IEnemy * CMultiBuilder_EnemyMecha_Instanced::build()
{
	if (!m_pModel_Instance)	return nullptr;
	CEnemyMecha_Instanced *pObject = new CEnemyMecha_Instanced;
	//	m_pModel_Instance->addInstance(pObject);
	if (!pObject->createObject(m_pFactory))
	{
		delete pObject;
		return nullptr;
	}
	return pObject;
}

