#ifndef HEADER_MAINMECHA_MLRS
#define HEADER_MAINMECHA_MLRS

#include"Mecha_MLRS.h"
#include"Interface_Object_MLRS.h"

//전방선언
class AState_MainMecha;

//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------
 

class IFactory_MainMecha_Instanced : public IFactory_Mecha_Instanced {
public:
	virtual ~IFactory_MainMecha_Instanced() {};

	virtual EPlayerType getPlayerType() = 0;
};

 

//----------------------------------- 인스턴싱용 -------------------------------------------------------------------------------------

class CMainMecha_Instanced : public CMecha_Instanced,
								virtual public IMainMecha{
private:
	typedef CMecha_Instanced SUPER;

private:
	EPlayerType m_ePlayerType;
	unsigned int m_nID;

	IState_Object<CMainMecha_Instanced> *m_pState;
	IState_Object<CMainMecha_Instanced> *m_pState_Past;

public:
	CMainMecha_Instanced();
	virtual ~CMainMecha_Instanced() {}

private:
	//override RIGameObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const { }
	virtual const  bool isClear() const { return false; }

public:
	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return SUPER::getTypeFlag(); }

	//override RIGameObject_IDGetter
	virtual const unsigned int getID()const { return m_nID; }

	//override RIGameObject_TypeGetter
	virtual const EGameObjType getObjType()const { return EGameObjType::eAllied; }

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg){ SUPER::move(pMoveMsg); }

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject *pMsg){ m_pState->handleMsg(this, pMsg); }

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime){ return m_pState->animateObject(this, fElapsedTime); }

	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const { if (!m_bEnable)	return nullptr; return SUPER::getBoundingObject(); }

	//override RIGameObject_PositionGetter
	virtual DirectX::XMFLOAT3A* getPositionOrigin()const { return SUPER::getPositionOrigin(); }
	virtual const DirectX::XMFLOAT3 getPosition() const { return SUPER::getPosition(); }

	//override RIGameObject_DirectionGetter
	virtual DirectX::XMFLOAT3A* getDirectionOrigin() const { return SUPER::getDirectionOrigin(); }
	virtual const DirectX::XMFLOAT3 getDirection() const { return SUPER::getDirection(); }

	//override RIGameObject_LocalVectorGetter
	virtual DirectX::XMFLOAT3A* getRightOrigin()const { return SUPER::getRightOrigin(); }
	virtual DirectX::XMFLOAT3A* getUpOrigin()	const { return SUPER::getUpOrigin(); }
	virtual DirectX::XMFLOAT3A* getLookOrigin() const { return SUPER::getLookOrigin(); }
	 
	//override IMainMecha
	virtual const EPlayerType getType()const { return m_ePlayerType; }
	virtual void enable() { SUPER::enable(); }
	virtual void disable() { SUPER::disable(); }
	virtual void setID(unsigned int nID) { m_nID = nID; }
	 
	bool createObject(IFactory_MainMecha_Instanced *pFactory);


	//상태 클래스 friend 선언
	friend AState_MainMecha; 
};


#endif