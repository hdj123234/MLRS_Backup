#include "stdafx.h"

#include "Creator_Mesh_CustomFBX.h"

#include"..\Module_Object_Instancing\Factory_AnimData_Instancing.h"
#include"..\Module_Object_GameModel\Factory_Material.h"
//#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"
//#include"..\Module_Object_GameModel\Factory_AnimData.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Type_InDirectX.h"
//#include"..\Module_Platform_DirectX11\DXUtility.h"
//#include"..\Module_Object\Msg_Movement.h"
//#include"..\Module_Object_Camera\Factory_Camera_ThirdPerson.h"
//#include"..\Module_Object\Factory_AnimController.h"
//#include"..\Module_MLRS_Object\Decorator_Player.h"
//#include"..\Module_MLRS_Object\Decorator_Enemy.h"

//#include"..\Module_Renderer\RendererState_D3D.h"
//#include "..\Module_MLRS_Object\MainMecha_MLRS.h"
#include "..\MyINI.h"

using namespace DirectX;


//----------------------------------- CFactory_Mesh_CustomFBX -------------------------------------------------------------------------------------

CFactory_Mesh_CustomFBX::CFactory_Mesh_CustomFBX(
	const std::string &rsName,
	const CMyType_Mesh & rMesh,
	const std::string &rsVSName)
	: m_sName(rsName),
	m_rMesh(rMesh),
	m_sVSName(rsVSName)
{
}

ID3D11VertexShader * CFactory_Mesh_CustomFBX::createVertexShader()
{
	return CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
}

ID3D11InputLayout * CFactory_Mesh_CustomFBX::createInputLayout()
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputElementDescs = { 
		{ "VERTEXINDEX", 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UVINDEX"	, 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "NORMAL"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "TANGENT"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } };
	

	return CFactory_InputLayout(m_sVSName, m_sVSName,
		m_sName+"_IL",
		inputElementDescs)
		.createInputLayout();
}

ID3D11Buffer * CFactory_Mesh_CustomFBX::createVertexBuffer()
{
	const std::vector<CMyType_VertexIndex> &rIndices = m_rMesh.m_Indices;
	std::vector<CVertex_Mesh_MLRS> vertices;
	vertices.resize(rIndices.size());
	unsigned int nSize = static_cast<unsigned int>(rIndices.size());

	for (unsigned int i = 0; i < nSize; ++i)
	{
		vertices[i].m_nVertexIndex = rIndices[i].m_nVertexIndex;
		vertices[i].m_nUVIndex = rIndices[i].m_nUVIndex;
		vertices[i].m_vNormal = rIndices[i].m_vNormal;
		vertices[i].m_vTangent = rIndices[i].m_vTangent;
	}

	return CFactory_VertexBuffer<CVertex_Mesh_MLRS>(m_sName + "_VB", nSize).createBuffer(vertices.data());
}
const unsigned int CFactory_Mesh_CustomFBX::getMaterialID()
{
	return 0;
}

std::vector<ID3D11ShaderResourceView*> CFactory_Mesh_CustomFBX::createTBuffers()
{
	const std::vector<CMyType_Vertex> &rvPositions = m_rMesh.m_Vertices;

	ID3D11ShaderResourceView *pSRV_Pos = CFactory_SRV_BufferEX_FromData<CMyType_Vertex>(m_sName+"_Buf_Pos",rvPositions)
		.createShaderResourceView();

	const std::vector<XMFLOAT2> &rvUVs = m_rMesh.m_vUVs;

	ID3D11ShaderResourceView *pSRV_UVs = CFactory_SRV_Texture1D_FromData<XMFLOAT2>(m_sName+"_Buf_UV" ,
		rvUVs,
		DXGI_FORMAT::DXGI_FORMAT_R32G32_FLOAT)
		.createShaderResourceView();

	const std::vector<UINT> &rnMaterialIndices = m_rMesh.m_nMaterialIndices;

	ID3D11ShaderResourceView *pSRV_MaterialIndices = CFactory_SRV_Buffer_FromData<UINT>(m_sName + "_Buf_MaterialIndices",
		rnMaterialIndices,
		DXGI_FORMAT::DXGI_FORMAT_R32_UINT)
		.createShaderResourceView();
	if (!(pSRV_Pos&&pSRV_UVs&&pSRV_MaterialIndices)) return std::vector<ID3D11ShaderResourceView*>();

	return std::vector<ID3D11ShaderResourceView*>{pSRV_Pos, pSRV_UVs, pSRV_MaterialIndices};
}

std::vector<ID3D11Buffer*> CFactory_Mesh_CustomFBX::createConstBuffer_Transform()
{
	ID3D11Buffer *pBuffer_World = factory_CB_Matrix_World.createBuffer();
	ID3D11Buffer *pBuffer_View = factory_CB_Matrix_View.createBuffer();
	ID3D11Buffer *pBuffer_Projection = factory_CB_Matrix_Projection.createBuffer();

	if(!(pBuffer_World&&pBuffer_View&&pBuffer_Projection))	return std::vector<ID3D11Buffer*>();

	return std::vector<ID3D11Buffer*>{pBuffer_World, pBuffer_View, pBuffer_Projection};
}


unsigned int CFactory_Mesh_CustomFBX::getNumberOfVertex()
{
	return static_cast<unsigned int>(m_rMesh.m_Indices.size());
}


//----------------------------------- CFactory_FBXMesh_Dummy_Instancing -------------------------------------------------------------------------------------

CFactory_FBXMesh_Dummy_Instancing::CFactory_FBXMesh_Dummy_Instancing(
	const std::string &rsName,
	const CMyType_Mesh & rMesh ,
	const unsigned int nMaxNumberOfInstance,
	const std::string &rsVSName)
	:m_sName(rsName),
	m_rMesh(rMesh) ,
	m_nMaxNumberOfInstance(nMaxNumberOfInstance),
	m_sVSName(rsVSName)
{
}

ID3D11VertexShader * CFactory_FBXMesh_Dummy_Instancing::createVertexShader()
{
	return CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
}

ID3D11InputLayout * CFactory_FBXMesh_Dummy_Instancing::createInputLayout()
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputElementDescs = {
		{ "VERTEXINDEX", 0, DXGI_FORMAT_R32_UINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UVINDEX"	, 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "NORMAL"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "TANGENT"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 } ,
		{ "WORLDMATRIX"	, 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "WORLDMATRIX"	, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 } ,
		{ "WORLDMATRIX"	, 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 } ,
		{ "WORLDMATRIX"	, 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 } };
	
	
	return CFactory_InputLayout("vsDummy_FBX_Instancing", "vsDummy_FBX_Instancing",
		m_sName+"_IL",
		inputElementDescs)
		.createInputLayout();
}

ID3D11Buffer * CFactory_FBXMesh_Dummy_Instancing::createVertexBuffer()
{
	const std::vector<CMyType_VertexIndex> &rIndices = m_rMesh.m_Indices;
	std::vector<CVertex_Mesh_MLRS> vertices;
	vertices.resize(rIndices.size());
	unsigned int nSize = static_cast<unsigned int>(rIndices.size());

	for (unsigned int i = 0; i < nSize; ++i)
	{
		vertices[i].m_nVertexIndex = rIndices[i].m_nVertexIndex;
		vertices[i].m_nUVIndex = rIndices[i].m_nUVIndex;
		vertices[i].m_vNormal = rIndices[i].m_vNormal;
		vertices[i].m_vTangent = rIndices[i].m_vTangent;
	}

	return CFactory_VertexBuffer<CVertex_Mesh_MLRS>(m_sName+"_VB", nSize).createBuffer(vertices.data());
}

unsigned int CFactory_FBXMesh_Dummy_Instancing::getNumberOfVertex()
{
	return static_cast<unsigned int>(m_rMesh.m_Indices.size());
}

const unsigned int CFactory_FBXMesh_Dummy_Instancing::getMaterialID()
{
	return 0;
}

std::vector<ID3D11ShaderResourceView*> CFactory_FBXMesh_Dummy_Instancing::createTBuffers()
{
	const std::vector<CMyType_Vertex> &rvPositions = m_rMesh.m_Vertices;
	
	ID3D11ShaderResourceView *pSRV_Pos = CFactory_SRV_BufferEX_FromData<CMyType_Vertex>(m_sName+"_Buf_Pos" , rvPositions)
		.createShaderResourceView();
	
	const std::vector<XMFLOAT2> &rvUVs = m_rMesh.m_vUVs;
	
	ID3D11ShaderResourceView *pSRV_UVs = CFactory_SRV_Texture1D_FromData<XMFLOAT2>(m_sName+"_Buf_UV" ,
		rvUVs,
		DXGI_FORMAT::DXGI_FORMAT_R32G32_FLOAT)
		.createShaderResourceView();
	if (!(pSRV_Pos&&pSRV_UVs)) return std::vector<ID3D11ShaderResourceView*>();

	const std::vector<UINT> &rnMaterialIndices = m_rMesh.m_nMaterialIndices;

	ID3D11ShaderResourceView *pSRV_MaterialIndices = CFactory_SRV_Buffer_FromData<UINT>(m_sName + "_Buf_MaterialIndices",
		rnMaterialIndices,
		DXGI_FORMAT::DXGI_FORMAT_R32_UINT)
		.createShaderResourceView();
	if (!(pSRV_Pos&&pSRV_UVs&&pSRV_MaterialIndices)) return std::vector<ID3D11ShaderResourceView*>();

	return std::vector<ID3D11ShaderResourceView*>{pSRV_Pos, pSRV_UVs, pSRV_MaterialIndices};

}

std::vector<ID3D11Buffer*> CFactory_FBXMesh_Dummy_Instancing::createConstBuffer_Transform()
{
	ID3D11Buffer *pBuffer_View = factory_CB_Matrix_View.createBuffer();
	ID3D11Buffer *pBuffer_Projection = factory_CB_Matrix_Projection.createBuffer();
	
	if (!(pBuffer_View&&pBuffer_Projection))	return std::vector<ID3D11Buffer*>();
	
	return std::vector<ID3D11Buffer*>{pBuffer_View, pBuffer_Projection};
}
ID3D11Buffer * CFactory_FBXMesh_Dummy_Instancing::createInstanceBuffer()
{
	return CFactory_InstanceBuffer<Type_InstanceData_Mecha>(m_sName+"_Instance" , m_nMaxNumberOfInstance).createBuffer();
}
//