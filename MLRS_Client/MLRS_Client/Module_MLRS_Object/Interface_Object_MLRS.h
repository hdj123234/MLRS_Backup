#pragma once
#ifndef HEADER_INTERFACE_OBJECT_MLRS
#define HEADER_INTERFACE_OBJECT_MLRS

#include "..\Module_Object_Common\Interface_Object_Common.h"
#include "..\Module_Object\Msg_Movement.h"

//전방선언
enum EBossType : ENUMTYPE_256;
enum EPlayerType	: ENUMTYPE_256;
enum EEnemyType		: ENUMTYPE_256;
enum EMissileType	: ENUMTYPE_256;

class IMainMecha :	virtual public IGameObject_Common,
				virtual public RIGameObject_OnOff, 
				virtual public RIGameObject_PositionGetter ,
				virtual public RIGameObject_DirectionGetter,
				virtual public RIGameObject_LocalVectorGetter,
				virtual public RIGameObject_TypeGetter, 
				virtual public RIGameObject_IDGetter  {
public:
	virtual ~IMainMecha() = 0 {}

	virtual const EPlayerType getType()const = 0;
	virtual void setID(unsigned int nID) = 0;
//	//체크
//	virtual const ENUMTYPE_256 getAnimState()const { return 0; }
//	virtual void setAnimState(const ENUMTYPE_256 eState) {}
};

class IEnemy :  virtual public IGameObject_Common,
				virtual public RIGameObject_OnOff,
				virtual public RIGameObject_TypeGetter, 
				virtual public RIGameObject_IDGetter  {
public:
	virtual ~IEnemy() = 0 {}

	virtual const EEnemyType getType()const = 0;
	virtual void setID(unsigned int nID) = 0;
};

class IBoss :	virtual public IGameObject_Common,
				virtual public RIGameObject_OnOff,
				virtual public RIGameObject_TypeGetter, 
				virtual public RIGameObject_IDGetter {
public:
	virtual ~IBoss() = 0 {}

	virtual const EBossType getType()const = 0;
};

class IMissile :	virtual public RIGameObject_Movable,
					virtual public RIGameObject_HandleMsg,
					virtual public RIGameObject_PositionGetter,
					virtual public RIGameObject_Collision,
					virtual public RIGameObject_OnOff , 
					virtual public RIGameObject_IDGetter {
public:
	virtual ~IMissile() = 0 {}

	virtual const EMissileType getType() const = 0;
	virtual void setID(unsigned int nID) = 0;
};


class IPlayer_CameraEmbedded : virtual public IMainMecha {
public:
	virtual ~IPlayer_CameraEmbedded() = 0 {}

	virtual const ICamera * getCamera() = 0;
};

template<class TYPE>
class IState_Object  {
protected:
	virtual ~IState_Object() = 0 {}

//protected:
//	virtual void setState(TYPE * const pObj, IState_Object<TYPE> *const pState) = 0;
//	virtual const ENUMTYPE_256 getActionID()const = 0;

public:
	virtual void  handleMsg		(TYPE * pObj ,IMsg_GameObject * pMsg) = 0;
	virtual std::vector<IMsg *> animateObject	(TYPE * pObj, const float fElapsedTime) = 0;
};

#endif