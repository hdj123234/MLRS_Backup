#ifndef HEADER_STRATEGY_CREATEMODEL_CUSTOMFBX
#define HEADER_STRATEGY_CREATEMODEL_CUSTOMFBX

#include"Creator_Mesh_CustomFBX.h"

#include"..\Module_MLRS_Object\Mesh_MLRS.h"

#include "..\Module_Object_Instancing\ModelDecorator_Instancing.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"

#include "..\Module_Object\Interface_Object.h"
//#include "..\Module_Object_Common\Bone.h"
//#include "Model_FBX_Dummy.h"
//#include"..\Module_MLRS_Object\Mesh_MLRS.h"

#include"..\..\FBXConverter\FBXLoader.h"

//���漱��
struct Type_Bone;
class CMyINISection;
enum EActionType : ENUMTYPE_256;

class CShaderNameGetter {
public:
	static const std::string VS_Anim() { return "vsDummy_FBX"; }
	static const std::string VS_NoAnim() { return "vsDummy_FBX"; }
	static const std::string VS_Instancing_Anim() { return "vsDummy_FBX_Instancing"; }
	static const std::string VS_Instancing_NoAnim() { return "vsDummy_FBX_Instancing_NoAnim"; }
	static const std::string PS() { return "psDummy_FBX"; }
	static const std::string PS_DeferredLighting() { return "psDummy_FBX_DeferredLighting"; }
};
 

//--------------------------------------- Mesh --------------------------------------- 

class AStrategy_CreateModel_CustomFBX_Mesh {
protected:
	const std::string m_sVSName;
public:
	AStrategy_CreateModel_CustomFBX_Mesh(const std::string &rsVSName) :m_sVSName(rsVSName) {}
	virtual ~AStrategy_CreateModel_CustomFBX_Mesh() {}

	virtual std::vector<IMesh *> createMeshs(const std::string &rsName, const std::vector<CMyType_Mesh> &rMeshs);
	virtual IAnimData	* createAnimData(const std::string &rsName, CMyType_Bones &rBoneStructure, std::vector<std::vector<DirectX::XMFLOAT4X4>> &rFrameData) = 0;
};

class AStrategy_CreateModel_CustomFBX_Mesh_Instancing :public AStrategy_CreateModel_CustomFBX_Mesh {
protected:
	const unsigned int m_nNumberOfInstance;
public:
	AStrategy_CreateModel_CustomFBX_Mesh_Instancing(	const unsigned int nNumberOfInstance,
												const std::string &rsVSName) 
		:AStrategy_CreateModel_CustomFBX_Mesh(rsVSName) ,
		m_nNumberOfInstance(nNumberOfInstance){}
	virtual ~AStrategy_CreateModel_CustomFBX_Mesh_Instancing() {}

	virtual std::vector<IMesh *> createMeshs(const std::string &rsName, const std::vector<CMyType_Mesh> &rMeshs);

};

class CStrategy_CreateModel_CustomFBX_Mesh_NoAnim : public AStrategy_CreateModel_CustomFBX_Mesh {
public:
	CStrategy_CreateModel_CustomFBX_Mesh_NoAnim()
		:AStrategy_CreateModel_CustomFBX_Mesh("vsDummy_FBX_NoAnim") {}
	virtual ~CStrategy_CreateModel_CustomFBX_Mesh_NoAnim() {}

	virtual IAnimData	* createAnimData(const std::string &rsName, CMyType_Bones &rBoneStructure, std::vector<std::vector<DirectX::XMFLOAT4X4>> &rFrameData) { return nullptr; }
};

class CStrategy_CreateModel_CustomFBX_Mesh_Anim : public AStrategy_CreateModel_CustomFBX_Mesh {
private:
	typedef std::map<unsigned int, EActionType> StartPointList;
private:
	const StartPointList m_AnimList;
	const float m_fAnimFPS;
public:
	CStrategy_CreateModel_CustomFBX_Mesh_Anim(	const StartPointList &rAnimList,
										const float fAnimFPS = 60.0f)
		:AStrategy_CreateModel_CustomFBX_Mesh("vsDummy_FBX"),
		m_AnimList(rAnimList),
		m_fAnimFPS(fAnimFPS)		{}
	virtual ~CStrategy_CreateModel_CustomFBX_Mesh_Anim() {}

	virtual IAnimData	* createAnimData(const std::string &rsName, CMyType_Bones &rBoneStructure, std::vector<std::vector<DirectX::XMFLOAT4X4>> &rFrameData) ;
};

class CStrategy_CreateModel_CustomFBX_Mesh_Instancing_NoAnim : public AStrategy_CreateModel_CustomFBX_Mesh_Instancing {
public:
	CStrategy_CreateModel_CustomFBX_Mesh_Instancing_NoAnim(const unsigned int nNumberOfInstance)
		:AStrategy_CreateModel_CustomFBX_Mesh_Instancing(nNumberOfInstance,"vsDummy_FBX_Instancing_NoAnim") {}
	virtual ~CStrategy_CreateModel_CustomFBX_Mesh_Instancing_NoAnim() {}

	virtual IAnimData	* createAnimData(const std::string &rsName, CMyType_Bones &rBoneStructure, std::vector<std::vector<DirectX::XMFLOAT4X4>> &rFrameData) { return nullptr; }
};

class CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim : public AStrategy_CreateModel_CustomFBX_Mesh_Instancing {
private:
	typedef std::map<unsigned int, EActionType> StartPointList;
private:
	const StartPointList m_AnimList;
	const float m_fAnimFPS;
public:
	CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim(const unsigned int nNumberOfInstance,
		const StartPointList &rAnimList,
		const float fAnimFPS = 60.0f)
		:AStrategy_CreateModel_CustomFBX_Mesh_Instancing(nNumberOfInstance, "vsDummy_FBX_Instancing"),
		m_AnimList(rAnimList),
		m_fAnimFPS(fAnimFPS) {}
	virtual ~CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim() {}

	virtual IAnimData	* createAnimData(const std::string &rsName, CMyType_Bones &rBoneStructure, std::vector<std::vector<DirectX::XMFLOAT4X4>> &rFrameData);
};


//--------------------------------------- Material --------------------------------------- 
class AStrategy_CreateModel_CustomFBX_Material {
private:
	const std::string m_sPSName;
	bool m_bDeferredLighting;
public:
	AStrategy_CreateModel_CustomFBX_Material(const std::string &rsPSName, const bool bDeferredLighting):m_sPSName(rsPSName), m_bDeferredLighting(bDeferredLighting){}
	virtual ~AStrategy_CreateModel_CustomFBX_Material() {}

	std::vector<IMaterial *> createMaterials(const std::string &rsName, const std::vector<CMyType_Material> &rMaterials);
};

class CStrategy_CreateModel_CustomFBX_Material : public AStrategy_CreateModel_CustomFBX_Material {
private:
	static const std::map<bool, std::string> s_PSNameMap_DeferredLighting;
	static bool s_bDeferredLighting;
public:
	CStrategy_CreateModel_CustomFBX_Material(const bool bDeferredLighting = s_bDeferredLighting)
		:AStrategy_CreateModel_CustomFBX_Material(s_PSNameMap_DeferredLighting.at(bDeferredLighting), bDeferredLighting){}
	virtual ~CStrategy_CreateModel_CustomFBX_Material() {}

	static void setDeferredLighting(bool b) { s_bDeferredLighting = b; }
};

//class CStrategy_CreateFBXModel_Material_DeferredLighting : public AStrategy_CreateModel_CustomFBX_Material {
//public:
//	CStrategy_CreateFBXModel_Material_DeferredLighting() :AStrategy_CreateModel_CustomFBX_Material("psDummy_FBX_DeferredLighting") {}
//	virtual ~CStrategy_CreateFBXModel_Material_DeferredLighting() {}
//};


#endif