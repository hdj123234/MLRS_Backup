#ifndef HEADER_MISSILE_MLRS
#define HEADER_MISSILE_MLRS

#include"Interface_Object_MLRS.h"
#include"Type_MLRS.h"
#include"..\Module_Object_Instancing\GameObject_Instanced.h"
#include"..\Module_Object\GameObject_ModelBase.h"
#include"..\Module_Object\BoundingObject.h"
#include"..\Module_Object_Common\InWorldData.h"
#include"..\Module_DXComponent\Type_InDirectX.h"


//----------------------------------- Creator Interface -------------------------------------------------------------------------------------

class RIFactory_Missile_PosAndLocal {
public:
	virtual ~RIFactory_Missile_PosAndLocal() = 0 {}

	virtual DirectX::XMFLOAT3  getPosition() = 0;
	virtual DirectX::XMFLOAT3  getRight() = 0;
	virtual DirectX::XMFLOAT3  getUp() = 0;
	virtual DirectX::XMFLOAT3  getLook() = 0;
};



//----------------------------------- �ν��Ͻ̿� -------------------------------------------------------------------------------------


//----------------------------------- Creator Interface -------------------------------------------------------------------------------------

class IFactory_Missile : public IFactory_GameObject_Instanced,
	virtual public RIFactory_Missile_PosAndLocal {
public:
	virtual ~IFactory_Missile() = 0 {}

	virtual EMissileType getType()=0;
};

class AFactory_Missile_Instanced : public IFactory_Missile {
public:
	AFactory_Missile_Instanced() {}
	virtual ~AFactory_Missile_Instanced() = 0 {}

	virtual DirectX::XMFLOAT3  getPosition() { return DirectX::XMFLOAT3(0, 0, 0); }
	virtual DirectX::XMFLOAT3  getRight() { return DirectX::XMFLOAT3(1, 0, 0); }
	virtual DirectX::XMFLOAT3  getUp() { return DirectX::XMFLOAT3(0, 1, 0); }
	virtual DirectX::XMFLOAT3  getLook() { return DirectX::XMFLOAT3(0, 0, 1); }
};
//----------------------------------- Class -------------------------------------------------------------------------------------

class CMissile :	public AGameObject_Instanced<Type_InstanceData_Mecha>,
					virtual public IMissile,
					virtual public RIGameObject_SprayParticle<Type_Fume_SprayPoint>{
private:
	typedef AGameObject_Instanced<Type_InstanceData_Mecha> Super;
private: 
	unsigned int m_nID;
	CInWorldData_FromLocal m_WorldData;

	EMissileType m_eMissileType;

	CBoundingObject_OOBB m_BoundingObject;

	static const FLAG_8 s_ObjectFlag =  FLAG_OBJECTTYPE_MOVABLE;
protected:
	bool m_bEnable;

public:
	CMissile();
	virtual ~CMissile() {   }

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_ObjectFlag; }

	//override RIGameObject_IDGetter
	virtual const unsigned int getID()const { return m_nID; }

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg);

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime) { return std::vector<IMsg *>(); }

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject *pMsg) {}

	//override RIGameObject_PositionGetter
	virtual DirectX::XMFLOAT3A* getPositionOrigin()const { return &m_WorldData.m_rvPos; }
	virtual const DirectX::XMFLOAT3 getPosition() const { return DirectX::XMFLOAT3(m_WorldData.m_rvPos.x, m_WorldData.m_rvPos.y, m_WorldData.m_rvPos.z); }

	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const {if (!m_bEnable)	return nullptr; return &m_BoundingObject;}

	//override IMissile
	virtual const EMissileType getType() const { return m_eMissileType;}
	virtual void enable() ;
	virtual void disable() ;
	virtual void setID(unsigned int nID) { m_nID = nID; }

	//override IMissile
	virtual std::vector<Type_Fume_SprayPoint> getSprayPoints()const ;

	void updateMatrix();

	bool createObject(AFactory_Missile_Instanced *pFactory);
};

#endif