#include "stdafx.h"
#include"EnemyMecha_MLRS.h"

#include"..\Module_MLRS\Msg_InGame_MLRS.h"
#include"..\Module_Object\Msg_Movement.h"

#include"..\Module_Object\AnimController.h"
#include"..\Module_Object_GameModel\GameModel.h"
#include"..\Module_Object_GameModel\AnimData.h"
#include"State_EnemyMecha_MLRS.h"


//
//

//----------------------------------- CEnemyMecha_Instanced -------------------------------------------------------------------------------------

CEnemyMecha_Instanced::CEnemyMecha_Instanced() 
	:m_pState(CSingleton<CState_EnemyMecha_Waiting>::getInstance()),
	m_pState_Past(CSingleton<CState_EnemyMecha_Waiting>::getInstance())
{
}

bool CEnemyMecha_Instanced::createObject(IFactory_Enemy_Instanced * pFactory)
{
	m_eEnemyType = pFactory->getEnemyType();
	return SUPER::createObject(pFactory);
}
