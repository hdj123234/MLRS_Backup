#include "stdafx.h"
#include "Creator_Model_CustomFBX.h"

#include "Creator_Mesh_CustomFBX.h"

#include"..\Module_Object_Instancing\Factory_AnimData_Instancing.h"
#include"..\Module_Object_Instancing\ModelDecorator_Instancing.h"
#include"..\Module_Object_GameModel\Factory_Material.h"
#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"
#include"..\Module_Object_GameModel\Factory_AnimData.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Type_InDirectX.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_Object_Camera\Factory_Camera_ThirdPerson.h"
#include"..\Module_Object\Factory_AnimController.h"
#include"..\Module_Object_Common\ModelTester.h"

#include"..\Module_Renderer\RendererState_D3D.h"
#include "..\Module_MLRS_Object\MainMecha_MLRS.h"
#include "..\MyINI.h"

using namespace DirectX;


//--------------------------------------- CFactory_FBXModel --------------------------------------- 

CFactory_FBXModel::CFactory_FBXModel(
	const std::string & rsFileName, 
	AStrategy_CreateModel_CustomFBX_Mesh * pStrategy_Mesh,
	AStrategy_CreateModel_CustomFBX_Material * pStrategy_Material,
	const DirectX::XMFLOAT4X4 & rmGlobalTransform)
	:m_sFileName(rsFileName),
	m_pStrategy_Mesh(pStrategy_Mesh),
	m_pStrategy_Material(pStrategy_Material)
{
	m_Loader.loadCustomFBX(g_sDataFolder + m_sFileName);
}

CFactory_FBXModel::CFactory_FBXModel(
	const CMyINISection & rSection,
	AStrategy_CreateModel_CustomFBX_Mesh * pStrategy_Mesh,
	AStrategy_CreateModel_CustomFBX_Material * pStrategy_Material)
	: m_sFileName(rSection.getParam("FileName")),
	m_pStrategy_Mesh(pStrategy_Mesh),
	m_pStrategy_Material(pStrategy_Material)
{
	m_Loader.loadCustomFBX(g_sDataFolder +m_sFileName); 
}

void CFactory_FBXModel::releaseObject()
{
	if (m_pStrategy_Mesh) delete m_pStrategy_Mesh;
	if (m_pStrategy_Material) delete m_pStrategy_Material;
}

IGameModel * CFactory_FBXModel::createModelOrigin()
{
	CGameModel_BoundingBox *pGameModel = new CGameModel_BoundingBox;
	if (!pGameModel->createObject(this))
	{
		delete pGameModel;
		pGameModel = nullptr;

	}

	return pGameModel;
}

//--------------------------------------- CBuilder_FBXModel --------------------------------------- 

bool CBuilder_FBXModel::build(IGameModel * pModel)
{
	if (auto p = dynamic_cast<CGameModel_BoundingBox*>(pModel))
		return (!p->createObject(&m_rFactory));
	else return false;
}

IGameModel * CBuilder_FBXModel::build()
{
	return m_rFactory.createModelOrigin();
}

//--------------------------------------- CBuilder_FBXModel_Instancing --------------------------------------- 

bool CBuilder_FBXModel_Instancing::build(IGameModel * pModel)
{
	if(auto p = dynamic_cast<CGameModelDecorator_Instancing<Type_InstanceData_Mecha> *>(pModel))
		return p->createObject(dynamic_cast<IFactory_InstancingModel_Decorator *>(&m_rFactory));
	else return false;
}

IGameModel * CBuilder_FBXModel_Instancing::build()
{
	auto p = new CGameModelDecorator_Instancing<Type_InstanceData_Mecha>;
	if (!p->createObject(dynamic_cast<IFactory_InstancingModel_Decorator *>(&m_rFactory)))
	{
		delete p;
		p = nullptr;
	}
	return p; 
}

