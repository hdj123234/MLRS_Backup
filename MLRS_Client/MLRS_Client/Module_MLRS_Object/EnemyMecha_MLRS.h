#ifndef HEADER_ENEMYMECHA_MLRS
#define HEADER_ENEMYMECHA_MLRS

#include"Mecha_MLRS.h"
#include"Interface_Object_MLRS.h"

//전방선언
//class CState_EnemyMecha_Waiting;
//class AState_EnemyMecha;


//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------

class IFactory_Enemy : public IFactory_Mecha {
public:
	virtual ~IFactory_Enemy() {};

	virtual void setName(const std::string &rsName) = 0;
	virtual EEnemyType getEnemyType() = 0;
};

class IFactory_Enemy_Instanced : public IFactory_Mecha_Instanced {
public:
	virtual ~IFactory_Enemy_Instanced() {};

	virtual EEnemyType getEnemyType() = 0;
};


//----------------------------------- Class -------------------------------------------------------------------------------------
//
//

//----------------------------------- 인스턴싱용 -------------------------------------------------------------------------------------


class CEnemyMecha_Instanced : public CMecha_Instanced ,
								virtual public IEnemy {
private:
	typedef CMecha_Instanced SUPER;
private:
	EEnemyType m_eEnemyType;
	unsigned int m_nID;

	IState_Object<CEnemyMecha_Instanced> *m_pState;
	IState_Object<CEnemyMecha_Instanced> *m_pState_Past;

public:
	CEnemyMecha_Instanced();
	virtual ~CEnemyMecha_Instanced() {}

private:
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const {}
	virtual const bool isClear() const { return false; }

public:
	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return SUPER::getTypeFlag(); }

	//override RIGameObject_IDGetter
	virtual const unsigned int getID()const { return m_nID; }

	//override RIGameObject_TypeGetter
	virtual const EGameObjType getObjType()const { return EGameObjType::eEnemy; }

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg){ SUPER::move(pMoveMsg);}

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject *pMsg) {	m_pState->handleMsg(this, pMsg);}
  
	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime) { return m_pState->animateObject(this, fElapsedTime); }
	
	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const { if (!m_bEnable)	return nullptr; return SUPER::getBoundingObject(); }
	
	//override IEnemy
	virtual const EEnemyType getType() const { return m_eEnemyType; }
	virtual void enable()	{ SUPER::enable(); }
	virtual void disable()	{ SUPER::disable(); }
	virtual void setID(unsigned int nID) { m_nID = nID; }

	bool createObject(IFactory_Enemy_Instanced *pFactory);


	//상태 클래스 friend 선언
	friend class AState_EnemyMecha;
	friend class CState_EnemyMecha_Waiting;
};
#endif