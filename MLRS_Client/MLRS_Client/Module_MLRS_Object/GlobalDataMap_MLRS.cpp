#include"stdafx.h"
#include "GlobalDataMap_MLRS.h"

#include "..\Module_Object\Action.h"
#include "..\MyINI.h" 

#ifndef HEADER_STL
#include<fstream>
#endif // !HEADER_STL

std::map<IGameModel*, EPlayerType>	CGlobalDataMap_MLRS::s_PlayerMap;
std::map<IGameModel*, EEnemyType>	CGlobalDataMap_MLRS::s_EnemyMap;
std::map<IGameModel*, EMissileType> CGlobalDataMap_MLRS::s_MissileMap;

EModelType CGlobalDataMap_MLRS::getModelType(IGameModel * pModel)
{
	if (s_PlayerMap.count(pModel))	return ePlayer;
	if (s_EnemyMap.count(pModel))	return eEnemy;
	if (s_MissileMap.count(pModel))	return eMissile;
	return eUnknown;
}
//
//std::map<unsigned int, EActionType> CGlobalDataMap_MLRS::getStartPointList(EEnemyType eType)
//{
//	std::map<unsigned int, EActionType> startPointList;
//
//	switch (eType)
//	{
//	case EEnemyType::eZephyros:
//		startPointList.insert(std::make_pair(0, EActionType::eActionType_Keep));
//		startPointList.insert(std::make_pair(1, EActionType::eActionType_Keep));
//		startPointList.insert(std::make_pair(70, EActionType::eActionType_Repeat));
//		return startPointList;
//	case EEnemyType::eCombat:
//		startPointList.insert(std::make_pair(0, EActionType::eActionType_Repeat));
//		startPointList.insert(std::make_pair(1, EActionType::eActionType_Repeat));
////		startPointList.insert(std::make_pair(2, EActionType::eActionType_Repeat));
//		startPointList.insert(std::make_pair(100, EActionType::eActionType_Repeat));
//		return startPointList;
//	case EEnemyType::eTrollBatRider:
//		startPointList.insert(std::make_pair(0, EActionType::eActionType_Repeat));
//		startPointList.insert(std::make_pair(1, EActionType::eActionType_Repeat));
//		return startPointList;
//	}
//	return startPointList;
//}

std::map<unsigned int, EActionType> CGlobalDataMap_MLRS::getStartPointList(const std::string & rsFileName)
{
	using namespace std;
	map<string, EActionType> ActionTypeMap{
		make_pair("Default",EActionType::eActionType_Default),
		make_pair("Repeat",EActionType::eActionType_Repeat),
		make_pair("Keep",EActionType::eActionType_Keep)
	};

	map<unsigned int, EActionType> retval;
	pair<unsigned int, EActionType> pair_Tmp;

	std::string sTmp;
	ifstream ifs(g_sDataFolder+ rsFileName);
	if (!ifs.is_open())	return retval;
	getline(ifs, sTmp, ',');
	getline(ifs, sTmp, ',');
	ifs >> sTmp ;
	while (!ifs.eof())
	{
		getline(ifs, sTmp, ',');
		if (sTmp == "") break;
		getline(ifs, sTmp, ',');
		if (sTmp == "") break;
		pair_Tmp.first = stoi(sTmp);

		getline(ifs, sTmp);
		if (sTmp == "") break;
		pair_Tmp.second = ActionTypeMap[sTmp];
		retval.insert(pair_Tmp);
	}
	return retval;
}

const CMyINISection CGlobalDataMap_MLRS::getDefaultINISection_Model(EEnemyType eType)
{
	CMyINISection retval;
	switch (eType)
	{
	case EEnemyType::eZephyros:
		retval.setParam("Filename", "Zephyros/Blackwing_Zephyros_0.301_160322_OneObject.mgm");
		retval.setParam("AnimationData", "Zephyros/Zephyros_Animation.csv");
		return retval;
	case EEnemyType::eCombat:
		retval.setParam("Filename", "Combat/GreyRoach_Combat_0.1.00_160226.mgm");
		retval.setParam("AnimationData", "Combat/Combat_Animation.csv");
		return retval;
	case EEnemyType::eTrollBatRider:
		retval.setParam("Filename", "TrollBatRider/GoldenBat_TrollBatRider_0.2.01_160510.mgm");
		retval.setParam("AnimationData", "TrollBat/TrollBat_Animation.csv");
		return retval;
	}
	return retval;
}

const CMyINISection CGlobalDataMap_MLRS::getDefaultINISection_Model_Boss()
{
	CMyINISection retval; 
	retval.setParam("Filename", "abattre/aaa.mgm");
	retval.setParam("AnimationData", "Boss/Boss_Animation.csv");
	return retval;
}

const CMyINISection CGlobalDataMap_MLRS::getDefaultINISection_Model_Abattre()
{
	CMyINISection retval;
	retval.setParam("Filename", "Boss/Delta(Pre).mgm");
	retval.setParam("AnimationData", "abattre/avattre_Animation.csv");
	return retval;
}

const CMyINISection CGlobalDataMap_MLRS::getDefaultINISection_Model_BaseCamp()
{
	CMyINISection retval;
	retval.setParam("Filename", "BaseCamp/fbx.mgm");
	retval.setParam("AnimationData", "BaseCamp/BaseCamp_Animation.csv");
	return retval;
}
