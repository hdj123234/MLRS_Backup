#pragma once
#ifndef HEADER_GLOBALDATAMAP_MLRS
#define HEADER_GLOBALDATAMAP_MLRS

#include"Interface_Object_MLRS.h"

#ifndef HEADER_STL
#include<map>
#endif // !HEADER_STL

//���漱��
class IGameModel;
enum EActionType : ENUMTYPE_256;
class CMyINISection;

enum EModelType : ENUMTYPE_256
{
	ePlayer ,
	eEnemy ,
	eMissile ,
	eUnknown = 0xff,
};
enum EPlayerType : ENUMTYPE_256
{
	eAbattre = 0
};

enum EEnemyType : ENUMTYPE_256
{
	eZephyros=0,
	eCombat=1,
	eTrollBatRider = 2,
};

enum EBossType : ENUMTYPE_256
{
	eBoss1 = 0,
};


enum EMissileType : ENUMTYPE_256
{
	eEnemy1 = 1,
	eEnemy2 = 2,
	eMain1 = 3,
	eMain2 = 4,
	eMain3 = 5,
};

class CGlobalDataMap_MLRS {
private:
	static std::map<IGameModel*, EPlayerType>	s_PlayerMap;
	static std::map<IGameModel*, EEnemyType>	s_EnemyMap;
	static std::map<IGameModel*, EMissileType>	s_MissileMap;

public:
	static void setPlayerModel(EPlayerType eType, IGameModel * pModel) { s_PlayerMap.insert(std::make_pair(pModel, eType)); }
	static void setEnemyModel(EEnemyType eType, IGameModel * pModel) { s_EnemyMap.insert(std::make_pair(pModel, eType)); }
	static void setMissileModel(EMissileType eType, IGameModel * pModel) { s_MissileMap.insert(std::make_pair(pModel, eType)); }

	static EPlayerType getPlayerType(	 IGameModel * pModel) { return s_PlayerMap	[pModel]; }
	static EEnemyType getEnemyType(		 IGameModel * pModel) { return s_EnemyMap	[pModel]; }
	static EMissileType getMissileType(	 IGameModel * pModel) { return s_MissileMap	[pModel]; }
	static EModelType getModelType(IGameModel * pModel);

	static std::map<unsigned int, EActionType> getStartPointList(const std::string &rsFileName);
//	static std::map<unsigned int, EActionType> getStartPointList(EEnemyType eType);
	static const CMyINISection getDefaultINISection_Model(EEnemyType eType);
	static const CMyINISection getDefaultINISection_Model_Boss();
	static const CMyINISection getDefaultINISection_Model_Abattre();
	static const CMyINISection getDefaultINISection_Model_BaseCamp();
};

#endif