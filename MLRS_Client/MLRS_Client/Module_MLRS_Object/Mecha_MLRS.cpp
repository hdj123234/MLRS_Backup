#include "stdafx.h"
#include "Mecha_MLRS.h"

#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif // !HEADER_DIRECTXMATH

using namespace DirectX;

//----------------------------------- AFactory_Mecha -------------------------------------------------------------------------------------

ID3D11Buffer * AFactory_Mecha::createConstBuffer_World()
{
	return factory_CB_Matrix_World.createBuffer();
}


//----------------------------------- CMecha -------------------------------------------------------------------------------------

CMecha::CMecha()
	:	m_bEnable(true)
{
}


void CMecha::drawReady(CRendererState_D3D * pRendererState) const
{
	if (!pRendererState)		return;
	CBufferUpdater::update<XMFLOAT4X4A>(pRendererState->getDeviceContext(), m_pCBuffer_World, m_WorldData.m_rmWorld);

	SUPER::drawReady(pRendererState);
}
void CMecha::move(const IMsg_Movement * pMoveMsg)
{
	if (!m_bEnable)	return;
	if (!pMoveMsg)		return;

	switch (pMoveMsg->getMovementType()){
	case EMsgType_Movement::eReplace_PositionOnly:
	{
		const RIMsg_Movement_GetVector3 *msg = dynamic_cast<const RIMsg_Movement_GetVector3 *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getVector();

		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		updateMatrix();
		break;
	}
	case EMsgType_Movement::eReplace_Position:
	{
		const RIMsg_Movement_GetVector3 *msg = dynamic_cast<const RIMsg_Movement_GetVector3 *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getVector();
		XMVECTOR vLook;
		if (pvPos->x == m_WorldData.m_rvPos.x && pvPos->z == m_WorldData.m_rvPos.z)
			vLook = XMLoadFloat3A(&m_WorldData.m_rvLook);
		else
			vLook = XMVector3NormalizeEst(XMVectorSet(pvPos->x - m_WorldData.m_rvPos.x, 0, pvPos->z - m_WorldData.m_rvPos.z, 1));
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vRight = (XMVector3Cross(vUp, vLook));
		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
		break;
	}
	case EMsgType_Movement::eReplace_PosAndDir:
	{
		const RIMsg_Movement_GetPosAndDir *msg = dynamic_cast<const RIMsg_Movement_GetPosAndDir *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getPos();
		const XMFLOAT3 *pvDir = msg->getDir();
		XMFLOAT3 vDir = XMFLOAT3(pvDir->x, 0, pvDir->z);
		XMVECTOR vLook = XMVector3NormalizeEst(XMLoadFloat3(&vDir));
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
		break;
	}
	case EMsgType_Movement::eSetDirection:
	{
		const CMsg_Movement_Vector3 *msg = dynamic_cast<const CMsg_Movement_Vector3 *>(pMoveMsg);
		const XMFLOAT3 *pDirection = msg->getVector();

		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vLook = XMVector3Normalize(XMLoadFloat3(pDirection));
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
		break;
	}
	}
}

std::vector<IMsg *> CMecha::animateObject(const float fElapsedTime)
{
	if (m_eAnimState != m_eAnimState_Past)
	{
		m_eAnimState_Past = m_eAnimState;
		SUPER::setAction(m_eAnimState);
	}
	return  SUPER::animateObject(fElapsedTime);
}

bool CMecha::createObject(IFactory_Mecha * pFactory)
{	
	m_pCBuffer_World = pFactory->createConstBuffer_World();
	auto &vPos = pFactory->getPosition();
	m_WorldData.m_rvPos.x = vPos.x;
	m_WorldData.m_rvPos.y = vPos.y;
	m_WorldData.m_rvPos.z = vPos.z;
	auto &vRight = pFactory->getRight();
	m_WorldData.m_rvRight.x = vRight.x;
	m_WorldData.m_rvRight.y = vRight.y;
	m_WorldData.m_rvRight.z = vRight.z;
	auto &vUp = pFactory->getUp();
	m_WorldData.m_rvUp.x = vUp.x;
	m_WorldData.m_rvUp.y = vUp.y;
	m_WorldData.m_rvUp.z = vUp.z;
	auto &vLook = pFactory->getLook();
	m_WorldData.m_rvLook.x = vLook.x;
	m_WorldData.m_rvLook.y = vLook.y;
	m_WorldData.m_rvLook.z = vLook.z;

	updateMatrix();
	return SUPER::createObject(pFactory);
}

void CMecha::releaseObject()
{
}

void CMecha::updateMatrix()
{
	m_WorldData.updateWorldMatrix();
	m_BoundingObject.setOOBB(m_WorldData.m_rmWorld);
}




//----------------------------------- CMecha_Instanced -------------------------------------------------------------------------------------


CMecha_Instanced::CMecha_Instanced()
	:m_bEnable(true),
	m_ProxyManager_Pos(this)
{
}

void CMecha_Instanced::move(const IMsg_Movement * pMoveMsg)
{
	if (!m_bEnable)	return;
	if (!pMoveMsg)		return;

	switch (pMoveMsg->getMovementType())
	{
	case EMsgType_Movement::eRotate_Yaw:
	{
		const CMsg_Movement_Float *msg = dynamic_cast<const CMsg_Movement_Float *>(pMoveMsg);
		const float fAngle = msg->getData();

		XMVECTOR vRight = XMLoadFloat3(&m_WorldData.m_rvRight);
		XMVECTOR vUp = XMLoadFloat3(&m_WorldData.m_rvUp);
		XMVECTOR vLook = XMLoadFloat3(&m_WorldData.m_rvLook);

		XMMATRIX mRotation = XMMatrixRotationAxis(vUp, fAngle/100);
		XMStoreFloat3A(&m_WorldData.m_rvRight, XMVector3Transform( vRight,mRotation));
		XMStoreFloat3A(&m_WorldData.m_rvUp, XMVector3Transform(vUp, mRotation));
		XMStoreFloat3A(&m_WorldData.m_rvLook, XMVector3Transform(vLook, mRotation));
		updateMatrix();
		break;
	}
	case EMsgType_Movement::eReplace_PositionOnly:
	{
		const RIMsg_Movement_GetVector3 *msg = dynamic_cast<const RIMsg_Movement_GetVector3 *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getVector();

		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		updateMatrix();
		break;
	}
	case EMsgType_Movement::eReplace_Position:
	{
		const RIMsg_Movement_GetVector3 *msg = dynamic_cast<const RIMsg_Movement_GetVector3 *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getVector();
		XMVECTOR vLook;
		if ( pvPos->x == m_WorldData.m_rvPos.x && pvPos->z == m_WorldData.m_rvPos.z)
			vLook = XMLoadFloat3A(&m_WorldData.m_rvLook);
		else
			vLook = XMVector3NormalizeEst(XMVectorSet(pvPos->x - m_WorldData.m_rvPos.x, 0, pvPos->z - m_WorldData.m_rvPos.z, 1));
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vRight = (XMVector3Cross(vUp, vLook));
		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
		break;
	}
	case EMsgType_Movement::eReplace_PosAndDir:
	{
		const RIMsg_Movement_GetPosAndDir *msg = dynamic_cast<const RIMsg_Movement_GetPosAndDir *>(pMoveMsg);
		const XMFLOAT3 *pvPos = msg->getPos();
		const XMFLOAT3 *pvDir = msg->getDir();
		XMFLOAT3 vDir = XMFLOAT3(pvDir->x, 0, pvDir->z);
		XMVECTOR vLook = XMVector3NormalizeEst(XMLoadFloat3(&vDir));
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		m_WorldData.m_rvPos.x = pvPos->x;
		m_WorldData.m_rvPos.y = pvPos->y;
		m_WorldData.m_rvPos.z = pvPos->z;
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
		break;
	}
	case EMsgType_Movement::eSetDirection:
	{
		const CMsg_Movement_Vector3 *msg = dynamic_cast<const CMsg_Movement_Vector3 *>(pMoveMsg);
		const XMFLOAT3 *pDirection = msg->getVector();

		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vLook = XMVector3Normalize(XMLoadFloat3(pDirection));
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		XMStoreFloat3A(&m_WorldData.m_rvRight, vRight);
		XMStoreFloat3A(&m_WorldData.m_rvUp, vUp);
		XMStoreFloat3A(&m_WorldData.m_rvLook, vLook);
		updateMatrix();
		break;
	}
	}
}

//std::vector<IMsg *> CMecha_Instanced::animateObject(const float fElapsedTime)
//{
//	if (m_eAnimState != m_eAnimState_Past)
//	{
//		m_eAnimState_Past = m_eAnimState;
//		SUPER::setAction(m_eAnimState);
//	}
//	
//	//if (m_pAnimContoller->isEndOfAction())
//	//	this->revertState();
//
//	return  SUPER::animateObject(fElapsedTime);
//}

void CMecha_Instanced::setInstanceData(Type_InstanceData_Mecha * pInstanceData_Origin)
{ 
	m_pInstanceData = pInstanceData_Origin; 
	updateMatrix();
}

void CMecha_Instanced::updateMatrix()
{
//	if (!m_pInstanceData)return;
	m_WorldData.getWorldMatrix(m_pInstanceData);

	m_BoundingObject.setOOBB(*m_pInstanceData);
}

bool CMecha_Instanced::createObject(IFactory_Mecha_Instanced * pFactory)
{
	if(!SUPER::createObject(pFactory))return false;
	auto &vPos = pFactory->getPosition();
	m_WorldData.m_rvPos.x = vPos.x;
	m_WorldData.m_rvPos.y = vPos.y;
	m_WorldData.m_rvPos.z = vPos.z;
	auto &vRight = pFactory->getRight();
	m_WorldData.m_rvRight.x = vRight.x;
	m_WorldData.m_rvRight.y = vRight.y;
	m_WorldData.m_rvRight.z = vRight.z;
	auto &vUp = pFactory->getUp();
	m_WorldData.m_rvUp.x = vUp.x;
	m_WorldData.m_rvUp.y = vUp.y;
	m_WorldData.m_rvUp.z = vUp.z;
	auto &vLook = pFactory->getLook();
	m_WorldData.m_rvLook.x = vLook.x;
	m_WorldData.m_rvLook.y = vLook.y;
	m_WorldData.m_rvLook.z = vLook.z;

	updateMatrix();

	if (auto p = dynamic_cast<RIGameModel_BoundingBoxGetter *>(m_pModel))
		m_BoundingObject.setData(p->getCenter(), p->getExtent());
	else
	{
		abort();
		return false;
	}

	return true;
}

void CMecha_Instanced::releaseObject()
{
}

AFactory_Mecha_Instanced::AFactory_Mecha_Instanced(IGameModel * pModel)
{
}
