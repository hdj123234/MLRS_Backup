#pragma once
#ifndef HEADER_CLIENTPLAYER
#define HEADER_CLIENTPLAYER

#include"Camera_MLRS.h"
#include"MainMecha_MLRS.h"
#include "Interface_Object_MLRS.h"
#include "..\Module_Object_Instancing\Interface_Object_Instancing.h"
#include "Type_MLRS.h"
#include "..\Module_Sound\Interface_Sound.h"

#ifndef HEADER_XAUDIO2
#include<X3DAudio.h>
#endif // !HEADER_XAUDIO2


//전방선언
class CCamera_MLRS;
class CMainMecha_Instanced;

namespace DirectX {
	struct XMFLOAT3A;
}

struct CType_Missile1 {
	DirectX::XMFLOAT3 m_vPos;
	float m_fShootPressTime;
	float m_fShootDelay;
	float m_fRotationUnitY;
	CType_Missile1();
};

struct CType_Missile2 {
	DirectX::XMFLOAT3 m_vPos;
	float m_fLaunchTime;
	float m_fElapsedTime;
	float m_fRotationUnitY;
	bool m_bShoot;
	float m_fMaxSpeed;
	float m_fAcceleration;
	CType_Missile2();
};

struct CType_Missile3 {
	bool m_bShoot;
	float m_fElapsedTime;
	float m_fLaunchTime;
	float m_fLockOnRange;
	std::vector<unsigned short> m_Targets;

	static const unsigned int s_nMaxNumberOfTarget = 12;

	CType_Missile3();
};
//----------------------------------- Creator -------------------------------------------------------------------------------------

class IFactory_Player_Decorator_MLRS {
public:
	virtual ~IFactory_Player_Decorator_MLRS() = 0 {};

	virtual CMainMecha_Instanced * getObjectOrigin() = 0;
	virtual CCamera_MLRS * createCamera() = 0;

	virtual const CType_Missile1 getMissile1() = 0;
	virtual const CType_Missile2 getMissile2() = 0;
	virtual const CType_Missile3 getMissile3() = 0;
//	virtual const float getRotationUnitY() = 0;
//	virtual const float getShootDelay() = 0;
//	virtual const DirectX::XMFLOAT3 getMissilePos() = 0;
};
//----------------------------------- Class -------------------------------------------------------------------------------------
//

/*	CObjectDecorator_ClientPlayer 클래스
*/
class CObjectDecorator_ClientPlayer :	virtual public IMainMecha ,
										virtual public IPlayer_CameraEmbedded,
										virtual public IGameObjectDecorator,
										virtual public RIGameObject_SoundListener{
private:
	CMainMecha_Instanced * m_pMainMecha;
	CCamera_MLRS *m_pCamera;

	IState_Object<CObjectDecorator_ClientPlayer> *m_pState;
	IState_Object<CObjectDecorator_ClientPlayer> *m_pState_Past;
//	ICamera * m_pCamera;

	FLAG_8 m_KeyFlag;
	bool m_bChangeDirection;
	bool m_bStopNow; 

	CType_Missile1 m_Missile1;
	CType_Missile2 m_Missile2;
	CType_Missile3 m_Missile3;
//	DirectX::XMFLOAT3 m_vMissilePos;
	
	std::vector<IMsg *> m_pReturnMsgs;

	X3DAUDIO_LISTENER m_SoundListener;
	float m_fBoostGauge;

	static ISpace * s_pSpace;
public:
	CObjectDecorator_ClientPlayer();
	virtual ~CObjectDecorator_ClientPlayer() { CObjectDecorator_ClientPlayer::releaseObject(); }

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return m_pMainMecha->getTypeFlag(); }

	//override RIGameObject_IDGetter
	virtual const unsigned int getID()const { return m_pMainMecha->getID(); }

	//override IMainMecha
	virtual void enable() { m_pMainMecha->enable(); }
	virtual void disable(){ m_pMainMecha->disable(); }

	//override RIGameObject_TypeGetter
	virtual const EGameObjType getObjType()const { return EGameObjType::ePlayer; }

	//override RIGameObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const{}
	
	//override RIGameObject_DrawableOnDX
	virtual const  bool isClear() const { return false; }

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg) { m_pMainMecha->move(pMoveMsg); }

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime) {return m_pState->animateObject(this, fElapsedTime); }

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject * pMsg) { m_pState->handleMsg(this, pMsg); }
	
	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const { return m_pMainMecha->getBoundingObject(); }

	//override IMainMecha
	virtual const EPlayerType getType() const { return m_pMainMecha->getType(); }
	virtual void setID(unsigned int nID) { m_pMainMecha->setID(nID); }

	//override IGameObjectDecorator
	virtual IGameObject *getObject() { return m_pMainMecha; }

	//override IPlayer_CameraEmbedded
	virtual const ICamera * getCamera() { return m_pCamera; }

	bool createObject(IFactory_Player_Decorator_MLRS *pFactory);
	void releaseObject();

	static void setSpace(ISpace * pSpace) { s_pSpace = pSpace; }
	 
	//override RIGameObject_PositionGetter
	virtual DirectX::XMFLOAT3A* getPositionOrigin()const { return m_pMainMecha->getPositionOrigin(); }
	virtual const DirectX::XMFLOAT3 getPosition() const { return m_pMainMecha->getPosition(); }

	//override RIGameObject_DirectionGetter
	virtual DirectX::XMFLOAT3A* getDirectionOrigin() const { return m_pMainMecha->getDirectionOrigin(); }
	virtual const DirectX::XMFLOAT3 getDirection() const { return m_pMainMecha->getDirection(); }

	//override RIGameObject_LocalVectorGetter
	virtual DirectX::XMFLOAT3A* getRightOrigin()const { return m_pMainMecha->getRightOrigin(); }
	virtual DirectX::XMFLOAT3A* getUpOrigin()	const { return m_pMainMecha->getUpOrigin(); }
	virtual DirectX::XMFLOAT3A* getLookOrigin() const { return m_pMainMecha->getLookOrigin(); }

	//override RIGameObject_SoundListener
	virtual X3DAUDIO_LISTENER * getListenerPtr();

	void hideThis();
	void showThis();

	friend class AState_ClientPlayer;
	friend class CState_ClientPlayer_Waiting;
	friend class CState_ClientPlayer_Moving;
	friend class CState_ClientPlayer_Jump;
	friend class CState_ClientPlayer_Boost;
	friend class AState_ClientPlayer_Attack1;
	friend class AState_ClientPlayer_Attack2;
	friend class AState_ClientPlayer_Attack3;
	friend class CState_ClientPlayer_Attack3_Shoot;
	friend class AState_ClientPlayer_Dead; 
};




typedef CObjectDecorator_ClientPlayer CMainMecha_ClientPlayer;



#endif