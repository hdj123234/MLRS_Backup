#include "stdafx.h"
#include"LightTester.h"
#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Factory_DXShader.h"

CLightTester::CLightTester()
{
	m_pVS = CFactory_VertexShader("vsDummy", "vsDummy").createShader();
	m_pGS = CFactory_GeometryShader("gsLightTester", "gsLightTester").createShader();
	m_pPS = CFactory_PixelShader("psLightTester", "psLightTester").createShader();
	m_pCBuffer_VP = std::vector<ID3D11Buffer *>{ factory_CB_Matrix_View.createBuffer(),factory_CB_Matrix_Projection.createBuffer() };
}

void CLightTester::drawOnDX(CRendererState_D3D * pRendererState) const
{
	pRendererState->setShader_VS(m_pVS);
	pRendererState->setShader_GS(m_pGS);
	pRendererState->setShader_PS(m_pPS);
	pRendererState->setConstBuffer_GS(m_pCBuffer_VP);
	pRendererState->setInputLayout(nullptr);
	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	pRendererState->getDeviceContext()->Draw(1, 0);
}
