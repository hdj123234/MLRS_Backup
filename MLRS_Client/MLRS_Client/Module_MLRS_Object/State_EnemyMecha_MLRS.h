#ifndef HEADER_STATE_ENEMYMECHA_MLRS
#define HEADER_STATE_ENEMYMECHA_MLRS

#include"Interface_Object_MLRS.h"
#include"..\MyUtility.h"

class CEnemyMecha_Instanced;

enum class EAnimState_EnemyMecha : ENUMTYPE_256 {
	eMoving,
	eIdle,
	eAttack,
	eTmp,
};

class AState_EnemyMecha  : public IState_Object<CEnemyMecha_Instanced>{
protected:
	typedef CEnemyMecha_Instanced CTarget;
	typedef AState_EnemyMecha AState;

protected:
	void setState(CTarget * const pObj, AState *const pState);
	virtual const EAnimState_EnemyMecha getActionID()const = 0;

public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg) ;
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime) ;
};


class CState_EnemyMecha_Waiting : public AState_EnemyMecha {
protected:
	virtual const EAnimState_EnemyMecha getActionID()const { return  (EAnimState_EnemyMecha::eIdle); }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);

};

class CState_EnemyMecha_Moving : public AState_EnemyMecha {
protected:
	virtual const EAnimState_EnemyMecha getActionID()const { return  (EAnimState_EnemyMecha::eMoving); }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);

};




#endif