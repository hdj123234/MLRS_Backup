#ifndef HEADER_BASECAMP_MLRS
#define HEADER_BASECAMP_MLRS

#include"Type_MLRS.h"
#include"..\Module_Object_Instancing\GameObject_Instanced.h"
#include"..\Module_Object\GameObject_ModelBase.h"
#include"..\Module_Object\BoundingObject.h"
#include"..\Module_Object_Common\InWorldData.h"

//���漱��
struct ID3D11Buffer;


enum class  EState_BaseCamp : ENUMTYPE_256 {
	eIdle = 0,
};
//----------------------------------- Creator Interface -------------------------------------------------------------------------------------


class IFactory_BaseCamp : public IFactory_GameObject_ModelBase_Anim  {
public:
	virtual ~IFactory_BaseCamp() = 0 {}

	virtual ID3D11Buffer *  createConstBuffer_World() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

class CBaseCamp :	public AGameObject_ModelBase_Anim,
				virtual public RIGameObject_Movable,
				virtual public RIGameObject_HandleMsg,
				virtual public RIGameObject_PositionGetter,
				virtual public RIGameObject_LocalVectorGetter,
//				virtual public RIGameObject_StateGetter,
				virtual public RIGameObject_DirectionGetter,
				virtual public RIGameObject_Collision {
private:
	typedef AGameObject_ModelBase_Anim Super;

private:
	CInWorldData_Matrix_FromLocal m_WorldData;
	CBoundingObject_OOBB m_BoundingObject;

	ID3D11Buffer *m_pCBuffer_World;

	static const FLAG_8 s_ObjectFlag = FLAG_OBJECTTYPE_DRAWALBE;
protected:
	ENUMTYPE_256 m_eAnimState;
	ENUMTYPE_256 m_eAnimState_Past;

	bool m_bEnable;

public:
	CBaseCamp();
	virtual ~CBaseCamp() {  }

private:
	virtual void drawReady(CRendererState_D3D *pRendererState)const;

public:
	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_ObjectFlag; }

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg);

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject *pMsg) {}

	//override RIGameObject_PositionGetter
	virtual DirectX::XMFLOAT3A* getPositionOrigin()const { return &m_WorldData.m_rvPos; }
	virtual const DirectX::XMFLOAT3 getPosition() const { return DirectX::XMFLOAT3(m_WorldData.m_rvPos.x, m_WorldData.m_rvPos.y, m_WorldData.m_rvPos.z); } 

	//override RIGameObject_DirectionGetter
	virtual DirectX::XMFLOAT3A* getDirectionOrigin() const { return &m_WorldData.m_rvLook; };
	virtual const DirectX::XMFLOAT3 getDirection() const { return DirectX::XMFLOAT3(m_WorldData.m_rvLook.x, m_WorldData.m_rvLook.y, m_WorldData.m_rvLook.z); }

	//override RIGameObject_LocalVectorGetter
	virtual DirectX::XMFLOAT3A* getRightOrigin()const {return &m_WorldData.m_rvRight;}
	virtual DirectX::XMFLOAT3A* getUpOrigin()	const {return &m_WorldData.m_rvUp;}
	virtual DirectX::XMFLOAT3A* getLookOrigin() const {return &m_WorldData.m_rvLook;}

//	//override RIGameObject_StateGetter
//	virtual const ENUMTYPE_256 getState() const { return m_eAnimState; }
//	virtual void setState(const ENUMTYPE_256 eState) { m_eAnimState_Past = m_eAnimState; m_eAnimState = eState; }

	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const { if (!m_bEnable)	return nullptr; return &m_BoundingObject; }

	void updateMatrix();

	bool createObject(IFactory_BaseCamp *pFactory);

	void setAnimState(ENUMTYPE_256 eAnimState) { m_eAnimState = eAnimState; }

};




#endif