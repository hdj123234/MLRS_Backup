#include "stdafx.h"
#include "Loader_CustomFBX.h"

#include"..\MyUtility.h"

#ifndef HEADER_STL
#include<fstream>
#endif // !HEADER_STL


CLoader_CustomFBX::CLoader_CustomFBX()
	: m_vCenter (0,0,0),
	  m_vExtents(0,0,0)
{
}

CLoader_CustomFBX::~CLoader_CustomFBX()
{
}

bool CLoader_CustomFBX::loadMeshs(std::ifstream & rifs)
{
	using namespace std;

	unsigned int n;

	rifs >> n;
	m_Model.m_Meshs.resize(n);
	for (auto &rMesh : m_Model.m_Meshs)
	{
		rifs >> rMesh.m_sName;

		rifs	>> rMesh.m_vAABB[0].x >> rMesh.m_vAABB[0].y >> rMesh.m_vAABB[0].z 
				>> rMesh.m_vAABB[1].x >> rMesh.m_vAABB[1].y >> rMesh.m_vAABB[1].z ;

		rifs >> n;
		rMesh.m_Vertices.resize(n);
		for (auto &rVertex : rMesh.m_Vertices)
			rifs	>> rVertex.m_vPosition.x >> rVertex.m_vPosition.y >> rVertex.m_vPosition.z 
					>> rVertex.m_vBoneIndex.x >> rVertex.m_vBoneIndex.y >> rVertex.m_vBoneIndex.z >> rVertex.m_vBoneIndex.w 
					>> rVertex.m_vWeights.x >> rVertex.m_vWeights.y >> rVertex.m_vWeights.z >> rVertex.m_vWeights.w ;

		rifs >> n;
		rMesh.m_vUVs.resize(n);
		for (auto &rUV : rMesh.m_vUVs)
			rifs >> rUV.x >> rUV.y ;

		rifs >> n;
		rMesh.m_nMaterialIndices.resize(n) ;
		for (auto &rMaterialIndex : rMesh.m_nMaterialIndices)
			rifs >> rMaterialIndex ;

		rifs >> n;
		rMesh.m_Indices.resize(n);
		for (auto &rIndex : rMesh.m_Indices)
			rifs	>> rIndex.m_nVertexIndex >> rIndex.m_nUVIndex 
					>> rIndex.m_vNormal.x >> rIndex.m_vNormal.y >> rIndex.m_vNormal.z 
					>> rIndex.m_vTangent.x >> rIndex.m_vTangent.y >> rIndex.m_vTangent.z ;
	}
	return true;
}

bool CLoader_CustomFBX::loadBones(std::ifstream & rifs)
{
	using namespace std;

	unsigned int n;
	std::string s;

	rifs >> n;
	auto &rContainer = m_Model.m_Bones.getContainer() ;
	rContainer.resize(n);
	for (auto &rBone : rContainer)
	{
		std::getline(rifs, s);
		std::getline(rifs, s);
		rBone.first = s;
		rifs >> rBone.second.m_nParentIndex ;
		auto element = rBone.second.m_mTransform_ToLocal.m;
		rifs	>> element[0][0] >> element[0][1] >> element[0][2] >> element[0][3] 
				>> element[1][0] >> element[1][1] >> element[1][2] >> element[1][3] 
				>> element[2][0] >> element[2][1] >> element[2][2] >> element[2][3] 
				>> element[3][0] >> element[3][1] >> element[3][2] >> element[3][3] ;
	}

	return true;
}

bool CLoader_CustomFBX::loadAnimTransforms(std::ifstream & rifs)
{
	using namespace std;

	unsigned int n;

	rifs >> n;
	if (n == 0)	return true;

	m_Model.m_AnimTransform.resize(n);
	rifs >> n;

	for (auto &rFrame : m_Model.m_AnimTransform)
	{
		rFrame.resize(n);
		for (auto &rMatrix_Bone : rFrame)
		{
			auto element = rMatrix_Bone.m;
			rifs	>> element[0][0] >> element[0][1] >> element[0][2] >> element[0][3] 
					>> element[1][0] >> element[1][1] >> element[1][2] >> element[1][3] 
					>> element[2][0] >> element[2][1] >> element[2][2] >> element[2][3] 
					>> element[3][0] >> element[3][1] >> element[3][2] >> element[3][3] ;
		}
	}
	return true;
}

bool CLoader_CustomFBX::loadMaterials(std::ifstream & rifs)
{
	using namespace std;

	unsigned int nNumberOfMap;
	unsigned int n;
//	char c;
	std::string s;

	rifs >> n;
	m_Model.m_Materials.resize(n) ;
	for (auto &rMaterial : m_Model.m_Materials)
	{
		std::getline(rifs, s);
		std::getline(rifs ,rMaterial.m_sName );
		rifs >> rMaterial.m_bPhong;
		rifs >> rMaterial.m_vAmbient.x		>> rMaterial.m_vAmbient.y		>> rMaterial.m_vAmbient.z		>> rMaterial.m_vAmbient.w		;
		rifs >> rMaterial.m_vDiffuse.x		>> rMaterial.m_vDiffuse.y		>> rMaterial.m_vDiffuse.z		>> rMaterial.m_vDiffuse.w		;
		rifs >> rMaterial.m_vSpecular.x		>> rMaterial.m_vSpecular.y		>> rMaterial.m_vSpecular.z		>> rMaterial.m_vSpecular.w		;
		rifs >> rMaterial.m_vEmissive.x		>> rMaterial.m_vEmissive.y		>> rMaterial.m_vEmissive.z		>> rMaterial.m_vEmissive.w		;
		rifs >> rMaterial.m_vReflection.x	>> rMaterial.m_vReflection.y	>> rMaterial.m_vReflection.z	>> rMaterial.m_vReflection.w	;
		rifs >> rMaterial.m_fShininess ;
		rifs >> nNumberOfMap;
		for (unsigned int i = 0; i < nNumberOfMap; ++i)
		{
			rifs >> n ;
			std::getline(rifs, s);
			std::getline(rifs, s);
			rMaterial.m_Maps[static_cast<EMyType_Map>(n)] = m_sPath+s;
		}
	}
	return true;
}

bool CLoader_CustomFBX::loadGuides(std::ifstream & rifs)
{
	using namespace std;
	 
	unsigned int n;
	unsigned int nMapSize;
	rifs >> n;
	m_Model.m_Guides.m_eModelType = static_cast<EMyType_ModelType>(n);
	switch (m_Model.m_Guides.m_eModelType)
	{
	case EMyType_ModelType::eModelType_MainMecha: 
		rifs >> nMapSize;
		for (auto i = 0U; i < nMapSize; ++i)
		{
			rifs >> n;
			auto &rvTmp = m_Model.m_Guides.m_GuideMap[static_cast<EMyType_Guide>(n)].m_vPos;
			rifs >> rvTmp.x >> rvTmp.y >> rvTmp.z;
		}
		break; 
	}

	return false;
}

bool CLoader_CustomFBX::loadCustomFBX(const std::string & rsFileName)
{
	m_sPath = CUtility_Path::dividePath_FromDataFolder( CUtility_Path::getPathOnly(rsFileName));
	std::ifstream ifs(rsFileName, std::ios::binary);
	if (ifs.fail())return false;

	loadMeshs(ifs);
	loadBones(ifs);
	loadAnimTransforms(ifs);
	loadMaterials(ifs);
	loadGuides(ifs);

	ifs.close();

	DirectX::XMFLOAT3 vMin = m_Model.m_Meshs[0].m_vAABB[0];
	DirectX::XMFLOAT3 vMax = m_Model.m_Meshs[0].m_vAABB[1];
	for (auto &rMesh : m_Model.m_Meshs)
	{
		if (vMin.x < rMesh.m_vAABB[0].x)vMin.x = rMesh.m_vAABB[0].x;
		if (vMin.y < rMesh.m_vAABB[0].y)vMin.y = rMesh.m_vAABB[0].y;
		if (vMin.z < rMesh.m_vAABB[0].z)vMin.z = rMesh.m_vAABB[0].z;
		if (vMax.x > rMesh.m_vAABB[1].x)vMax.x = rMesh.m_vAABB[1].x;
		if (vMax.y > rMesh.m_vAABB[1].y)vMax.y = rMesh.m_vAABB[1].y;
		if (vMax.z > rMesh.m_vAABB[1].z)vMax.z = rMesh.m_vAABB[1].z;
	}
	m_vCenter.x = (vMin.x + vMax.x) / 2;
	m_vCenter.y = (vMin.y + vMax.y) / 2;
	m_vCenter.z = (vMin.z + vMax.z) / 2;
	m_vExtents.x = (vMax.x - vMin.x) / 2;
	m_vExtents.y = (vMax.y - vMin.y) / 2;
	m_vExtents.z = (vMax.z - vMin.z) / 2;
	return true;
}
