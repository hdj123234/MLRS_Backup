#pragma once

#include"..\Module_Object_UI\UIManager.h"

#ifndef HEADER_UIMANAGER_MLRS
#define HEADER_UIMANAGER_MLRS

enum class EUIType : unsigned char{		//UI ������ �ε����� ����
	eAim=0,
	eSwitch_Weapon,
	eLockOn,
	eGuage_HP,
	eGuage_Boost,
	eLaunchGuide,
	eScript,
};

class CUIManager_MLRS :	public AUIManager {
public:
	CUIManager_MLRS() {}
	virtual ~CUIManager_MLRS() {}

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject * pMsg);

	inline IUserInterface * getUI(const EUIType eUIType) 
	{
		return m_pUIs[static_cast<unsigned char>(eUIType)];
	}

};

#endif