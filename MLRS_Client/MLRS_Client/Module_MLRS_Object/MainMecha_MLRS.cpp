#include "stdafx.h"
#include"MainMecha_MLRS.h"

#include"Camera_MLRS.h "
#include"..\Module_MLRS\Msg_InGame_MLRS.h"
#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_Object_Camera\Interface_Camera.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include"..\Module_Object\BoundingObject.h"
#include"State_MainMecha_MLRS.h"

//DEBUG
#include"..\Module_Object\Space.h"
// 

//----------------------------------- CMainMecha_Instanced -------------------------------------------------------------------------------------


CMainMecha_Instanced::CMainMecha_Instanced()
	:m_pState(CSingleton<CState_MainMecha_Waiting>::getInstance()),
	m_pState_Past(CSingleton<CState_MainMecha_Waiting>::getInstance())
{
}

bool CMainMecha_Instanced::createObject(IFactory_MainMecha_Instanced * pFactory)
{
	m_ePlayerType = pFactory->getPlayerType();
	return SUPER::createObject(pFactory);
}
