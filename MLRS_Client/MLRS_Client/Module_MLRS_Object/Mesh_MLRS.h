#ifndef HEADER_MESH_MLRS
#define HEADER_MESH_MLRS

#include"Type_MLRS.h"
#include"..\Module_Object_Instancing\Mesh_Instancing.h"
#include"..\Module_Object_GameModel\Mesh.h"

struct CVertex_Mesh_MLRS {
	unsigned int m_nVertexIndex;
	unsigned int m_nUVIndex;
	DirectX::XMFLOAT3 m_vNormal;
	DirectX::XMFLOAT3 m_vTangent;
	CVertex_Mesh_MLRS() {}
	CVertex_Mesh_MLRS(const unsigned int nVERTEXINDEX,
		const unsigned int nUVIndex,
		const DirectX::XMFLOAT3 &rvNormal,
		const DirectX::XMFLOAT3 &rvTangent);
};

//----------------------------------- Creator -------------------------------------------------------------------------------------

class RIFactory_Mesh_MLRS_Buffer {
public:
	virtual ~RIFactory_Mesh_MLRS_Buffer() = 0 {}

	virtual std::vector<ID3D11ShaderResourceView *> createTBuffers() = 0;
	virtual std::vector<ID3D11Buffer*> createConstBuffer_Transform() = 0;
};

class IFactory_Mesh_MLRS : virtual public IFactory_Mesh_Vertex ,
							virtual public RIFactory_Mesh_MLRS_Buffer {
public:
	virtual ~IFactory_Mesh_MLRS() = 0 {}
};


//----------------------------------- Class -------------------------------------------------------------------------------------

class CMesh_MLRS : public CMesh_Vertex {
private:
	std::vector<ID3D11ShaderResourceView *> m_pTBuffers;
	std::vector<ID3D11Buffer*> m_pBuffers_WVP;

private:
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	CMesh_MLRS() {}
	virtual ~CMesh_MLRS() {}

	bool createObject(IFactory_Mesh_MLRS *pFactory);
};

//----------------------------------- �ν��Ͻ̿� -------------------------------------------------------------------------------------

class IFactory_Mesh_MLRS_Instancing : virtual public IFactory_Mesh_Instancing<Type_InstanceData_Mecha>,
										virtual public RIFactory_Mesh_MLRS_Buffer {
public:
	virtual ~IFactory_Mesh_MLRS_Instancing() = 0 {}
};


class CMesh_MLRS_Instancing : public CMesh_Instancing<Type_InstanceData_Mecha> {
private:
	typedef CMesh_Instancing<Type_InstanceData_Mecha> Super;
private:
	std::vector<ID3D11ShaderResourceView *> m_pTBuffers;
	std::vector<ID3D11Buffer*> m_pBuffers_VP;

private:
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	CMesh_MLRS_Instancing() {}
	virtual ~CMesh_MLRS_Instancing() {}

	bool createObject(IFactory_Mesh_MLRS_Instancing *pFactory);
};


#endif