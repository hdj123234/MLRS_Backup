#ifndef HEADER_CREATOR_BASECAMP_MLRS
#define HEADER_CREATOR_BASECAMP_MLRS

#include"BaseCamp_MLRS.h"
#include"Type_MLRS.h"
#include"Interface_Object_MLRS.h"

//���漱��
enum EActionType : ENUMTYPE_256;
class CMyINISection;

class CFactory_BaseCamp : public IFactory_BaseCamp {
private:
	IGameModel *m_pModel;
public:
	CFactory_BaseCamp(IGameModel *pGameModel);
	virtual ~CFactory_BaseCamp() {}

	//override IFactory_GameObject_ModelBase
	virtual IGameModel * getModel() { return m_pModel; }

	//override IFactory_GameObject_ModelBase_Anim
	virtual	IAnimController * createAnimController();
	
	//IFactory_BaseCamp
	virtual ID3D11Buffer *  createConstBuffer_World() ;
	
	static std::map<unsigned int, EActionType> getStartPointList();
	static const CMyINISection getDefaultINISection();
};


#endif