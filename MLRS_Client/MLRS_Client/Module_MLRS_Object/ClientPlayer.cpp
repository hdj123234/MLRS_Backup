#include"stdafx.h"
#include"ClientPlayer.h"

#include"..\Module_Object_Camera\Camera.h"
#include"..\Module_Object\Msg_Movement.h"

#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"
#include"Camera_MLRS.h"
#include"State_ClientPlayer_MLRS.h"

//DEBUG
#include"..\Module_Object\Space.h"

#ifndef HEADER_STL
#include<fstream>
#endif // !HEADER_STL

CType_Missile1::CType_Missile1()
	:m_vPos(0, 0, 0),
	m_fShootPressTime(0),
	m_fShootDelay(0),
	m_fRotationUnitY(0)
{
}

CType_Missile2::CType_Missile2()
	:m_vPos(0, 0, 0),
	m_fLaunchTime(0),
	m_fElapsedTime(0),
	m_fRotationUnitY(0),
	m_bShoot(false)
{
}

CType_Missile3::CType_Missile3()
	:m_bShoot(false),
	m_fElapsedTime(0)
{
}



//----------------------------------- CObjectDecorator_ClientPlayer -------------------------------------------------------------------------------------

ISpace * CObjectDecorator_ClientPlayer::s_pSpace = nullptr;

CObjectDecorator_ClientPlayer::CObjectDecorator_ClientPlayer()
	: m_pMainMecha(nullptr),
	m_pCamera(nullptr),
	m_KeyFlag(0),
	m_bChangeDirection(false),
	m_bStopNow(false), 
	m_pState(CSingleton<CState_ClientPlayer_Waiting>::getInstance()),
	m_pState_Past(nullptr),
	m_fBoostGauge(180)
{
	ZeroMemory(&m_SoundListener, sizeof(m_SoundListener));
} 

bool CObjectDecorator_ClientPlayer::createObject(IFactory_Player_Decorator_MLRS * pFactory)
{ 
	m_pMainMecha = pFactory->getObjectOrigin();
	m_pCamera = pFactory->createCamera();

	if (!(m_pMainMecha&&m_pCamera))	return false;
	m_Missile1 = pFactory->getMissile1();
	m_Missile2 = pFactory->getMissile2();
	m_Missile3 = pFactory->getMissile3();
	 
	m_pCamera->setTarget(m_pMainMecha);
	m_pCamera->setCameraMode(ECameraMode::eDefault, m_pMainMecha);

	return true;
}



void CObjectDecorator_ClientPlayer::releaseObject()
{
	if (m_pMainMecha)	delete m_pMainMecha;
	if (m_pCamera)	delete m_pCamera;
	for (auto data : m_pReturnMsgs)
		delete data;
}

X3DAUDIO_LISTENER * CObjectDecorator_ClientPlayer::getListenerPtr()
{
	using namespace DirectX;
	XMFLOAT3 vTmp;
	XMFLOAT3 vLook = m_pCamera->getDirection();
	XMFLOAT3 vUp;
	XMVECTOR vLook_Loaded = XMLoadFloat3(&vLook);
	XMStoreFloat3(&vUp, XMVector3Cross(vLook_Loaded, XMVector3NormalizeEst(XMVector3Cross(XMVectorSet(0, 1, 0, 0), vLook_Loaded))));

	m_SoundListener.OrientFront.x = vLook.x;
	m_SoundListener.OrientFront.y = vLook.y;
	m_SoundListener.OrientFront.z = vLook.z;
	m_SoundListener.OrientTop.x = vUp.x;
	m_SoundListener.OrientTop.y = vUp.y;
	m_SoundListener.OrientTop.z = vUp.z;


	vTmp = DirectX::XMFLOAT3(0, 0, 0);
	m_SoundListener.Velocity.x = vTmp.x;
	m_SoundListener.Velocity.y = vTmp.y;
	m_SoundListener.Velocity.z = vTmp.z;
	vTmp = m_pCamera->getPosition();
	m_SoundListener.Position.x = vTmp.x;
	m_SoundListener.Position.y = vTmp.y;
	m_SoundListener.Position.z = vTmp.z;

	return &m_SoundListener; 
}

void CObjectDecorator_ClientPlayer::hideThis()
{
	m_pMainMecha->disable();
}

void CObjectDecorator_ClientPlayer::showThis()
{
	m_pMainMecha->enable();
}




 