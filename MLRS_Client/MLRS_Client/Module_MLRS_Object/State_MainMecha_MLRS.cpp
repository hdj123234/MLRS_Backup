#include "stdafx.h"
#include"State_MainMecha_MLRS.h"

#include"MainMecha_MLRS.h"
#include"..\Module_MLRS_Network\Msg_Network_MLRS.h"

//----------------------------------- AState_MainMecha -------------------------------------------------------------------------------------


void AState_MainMecha::setState(CTarget * const pObj, AState * const pState)
{
	if (pState != pObj->m_pState)
	{
		pObj->m_pState_Past = pObj->m_pState;
		pObj->m_pState = pState;
		pObj->m_pAnimContoller->setAction(static_cast<unsigned int>(pState->getActionID()));
	}
}

std::vector<IMsg*> AState_MainMecha::animateObject(CTarget * pObj, const float fElapsedTime)
{
	pObj->CMecha_Instanced::animateObject(fElapsedTime);
	return std::vector<IMsg*>();
}
 
//----------------------------------- CState_MainMecha_Waiting -------------------------------------------------------------------------------------

void CState_MainMecha_Waiting::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eChangeWeapon:
			switch (static_cast<CMsg_SC_ChangeWeapon *>(pMsg_SC)->m_eWeaponType)
			{
			case EWeaponType::eWeapon1:
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
				break;
			//case EWeaponType::eWeapon2:
			//	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack2_On>::getInstance());
			//	break;
			}
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eAttack1))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance()); 
			else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_BoostOn>::getInstance()); 
			else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_JumpOn>::getInstance()); 
			else if (!static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Moving>::getInstance()); 
			break;
		}
//		case EMsgType_SC::eMove:
		}
	}
}

//----------------------------------- CState_MainMecha_Moving -------------------------------------------------------------------------------------

void CState_MainMecha_Moving::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eChangeWeapon:
			switch (static_cast<CMsg_SC_ChangeWeapon *>(pMsg_SC)->m_eWeaponType)
			{
			case EWeaponType::eWeapon1:
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
				break;
				//case EWeaponType::eWeapon2:
				//	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack2_On>::getInstance());
				//	break;
			}
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eAttack1))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
			else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_BoostOn>::getInstance());
			else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_JumpOn>::getInstance());
			else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Waiting>::getInstance());

			break;
		}
//		case EMsgType_SC::eMove:
		}
	}
	 
}

//----------------------------------- CState_MainMecha_BoostOn -------------------------------------------------------------------------------------

void CState_MainMecha_BoostOn::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eChangeWeapon:
			switch (static_cast<CMsg_SC_ChangeWeapon *>(pMsg_SC)->m_eWeaponType)
			{
			case EWeaponType::eWeapon1:
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
				break;
				//case EWeaponType::eWeapon2:
				//	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack2_On>::getInstance());
				//	break;
			}
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eAttack1))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
			else if (!static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
			{
				if(static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_JumpOn>::getInstance());
				else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Waiting>::getInstance());
				else   
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Moving>::getInstance());
			}
			break;
		}
	} 
}

std::vector<IMsg*> CState_MainMecha_BoostOn::animateObject(CTarget * pObj, const float fElapsedTime)
{
	std::vector<IMsg*> &rRetval = pObj->CMecha_Instanced::animateObject(fElapsedTime);
	if (pObj->isAnimEnd())	AState_MainMecha::setState(pObj,CSingleton<CState_MainMecha_BoostOff>::getInstance());

	return rRetval;
}

//----------------------------------- CState_MainMecha_BoostOff -------------------------------------------------------------------------------------

void CState_MainMecha_BoostOff::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eChangeWeapon:
			switch (static_cast<CMsg_SC_ChangeWeapon *>(pMsg_SC)->m_eWeaponType)
			{
			case EWeaponType::eWeapon1:
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
				break;
				//case EWeaponType::eWeapon2:
				//	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack2_On>::getInstance());
				//	break;
			}
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eAttack1))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
			else if (!static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost)
				&&static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_JumpOn>::getInstance());
			else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Waiting>::getInstance());
			break;
		}
		}
	} 
}

//----------------------------------- CState_MainMecha_JumpOn -------------------------------------------------------------------------------------

void CState_MainMecha_JumpOn::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eChangeWeapon:
			switch (static_cast<CMsg_SC_ChangeWeapon *>(pMsg_SC)->m_eWeaponType)
			{
			case EWeaponType::eWeapon1:
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
				break;
				//case EWeaponType::eWeapon2:
				//	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack2_On>::getInstance());
				//	break;
			}
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eAttack1))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
			else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_BoostOn>::getInstance());
			else if (!static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
			{
				if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Waiting>::getInstance());
				else
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Moving>::getInstance());
			}
			break;
		}
		}
	} 
}

std::vector<IMsg*> CState_MainMecha_JumpOn::animateObject(CTarget * pObj, const float fElapsedTime)
{
	std::vector<IMsg*> &rRetval = pObj->CMecha_Instanced::animateObject(fElapsedTime);
	if (pObj->isAnimEnd())	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Jump>::getInstance());

	return rRetval;
}

//----------------------------------- CState_MainMecha_Jump -------------------------------------------------------------------------------------

void CState_MainMecha_Jump::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eChangeWeapon:
			switch (static_cast<CMsg_SC_ChangeWeapon *>(pMsg_SC)->m_eWeaponType)
			{
			case EWeaponType::eWeapon1:
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
				break;
				//case EWeaponType::eWeapon2:
				//	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack2_On>::getInstance());
				//	break;
			}
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eAttack1))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1_On>::getInstance());
			else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_BoostOn>::getInstance());
			break;
		}
		}
	} 
}

//----------------------------------- CState_MainMecha_Attack1_On -------------------------------------------------------------------------------------

void CState_MainMecha_Attack1_On::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eChangeWeapon:
			switch (static_cast<CMsg_SC_ChangeWeapon *>(pMsg_SC)->m_eWeaponType)
			{
			case EWeaponType::eNoWeapon:
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Moving>::getInstance());
				break;
				//case EWeaponType::eWeapon2:
				//	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack2_On>::getInstance());
				//	break;
			}
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (!static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eAttack1))
			{
				if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_BoostOn>::getInstance());
				else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_JumpOn>::getInstance());
				else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Waiting>::getInstance());
				else 
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Moving>::getInstance());
				 
			} 
			break;
		} 
		}
	}
	 
}

std::vector<IMsg*> CState_MainMecha_Attack1_On::animateObject(CTarget * pObj, const float fElapsedTime )
{
	std::vector<IMsg*> &rRetval = pObj->CMecha_Instanced::animateObject(fElapsedTime);
	if (pObj->isAnimEnd())	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack1>::getInstance());
	
	return rRetval;
}


//----------------------------------- CState_MainMecha_Attack1 -------------------------------------------------------------------------------------

void CState_MainMecha_Attack1::handleMsg(CTarget * pObj, IMsg_GameObject * pMsg)
{
	if (auto pMsg_SC = dynamic_cast<CMsg_SC *>(pMsg))
	{
		switch (pMsg_SC->getType())
		{
		case EMsgType_SC::eChangeWeapon:
			switch (static_cast<CMsg_SC_ChangeWeapon *>(pMsg_SC)->m_eWeaponType)
			{
			case EWeaponType::eNoWeapon:
				AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Moving>::getInstance());
				break;
				//case EWeaponType::eWeapon2:
				//	AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Attack2_On>::getInstance());
				//	break;
			}
			break;
		case EMsgType_SC::eCreateObj:
		case EMsgType_SC::eMove:
		{
			pObj->move(static_cast<CMsg_SC_Move *>(pMsg_SC)->getMovementMsg());
			if (!static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eAttack1))
			{
				if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eBoost))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_BoostOn>::getInstance());
				else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eJump))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_JumpOn>::getInstance());
				else if (static_cast <CMsg_SC_Move*>(pMsg_SC)->checkFlag(CMsg_SC_Move::EMoveFlag::eStop))
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Waiting>::getInstance());
				else 
					AState_MainMecha::setState(pObj, CSingleton<CState_MainMecha_Moving>::getInstance());
			 
			}
			break;
		}
		}
	} 
}
