#ifndef HEADER_CREATOR_MISSILE_MLRS
#define HEADER_CREATOR_MISSILE_MLRS

#include"Missile_MLRS.h"

//���漱��
class CMyINISection;


class CFactory_Missile : public AFactory_Missile_Instanced {
protected:
	IGameModel *m_pModel;

public:
	CFactory_Missile(IGameModel *pGameModel) : m_pModel(pGameModel) {}
	virtual ~CFactory_Missile() {}

	//override IFactory_GameObject_ModelBase
	virtual IGameModel * getModel() { return m_pModel; }

};

class CMultiBuilder_Missile : virtual public IMultiBuilder<IGameObject>,
								private CFactory_Missile {
public:
	CMultiBuilder_Missile(IGameModel *pModel) :CFactory_Missile(pModel){}
	virtual ~CMultiBuilder_Missile() {}

public:
	//CMultiBuilder<IGameObject>
	virtual IGameObject * build();
	virtual EMissileType getType();

	static const CMyINISection getDefaultINISection();
};





#endif