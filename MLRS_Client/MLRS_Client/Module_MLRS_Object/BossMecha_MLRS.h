#ifndef HEADER_BOSSMECHA_MLRS
#define HEADER_BOSSMECHA_MLRS

#include"Mecha_MLRS.h"
#include"Interface_Object_MLRS.h"



//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------

class IFactory_Boss : public IFactory_Mecha {
public:
	virtual ~IFactory_Boss() {};

	virtual EBossType getBossType() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------


class CBossMecha : public CMecha,
					virtual public IBoss,
					virtual public RIGameObject_Collision  {
private:
	typedef CMecha SUPER;
private:  
	EBossType m_eBossType;

	IState_Object<CBossMecha> *m_pState;
	IState_Object<CBossMecha> *m_pState_Past;

public:
	CBossMecha();
	virtual ~CBossMecha() {}

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return SUPER::getTypeFlag(); }

	//override RIGameObject_IDGetter
	virtual const unsigned int getID()const { return 1024; }

	//override RIGameObject_TypeGetter
	virtual const EGameObjType getObjType()const { return EGameObjType::eEnemy; }

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg){ SUPER::move(pMoveMsg); }

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject *pMsg) { m_pState->handleMsg(this, pMsg); }

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime) { return m_pState->animateObject(this,fElapsedTime); }

	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const { if (!m_bEnable)	return nullptr; return SUPER::getBoundingObject(); }

	//override RIGameObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const { if(m_bEnable)SUPER::drawOnDX(pRendererState); }
	virtual const  bool isClear() const { return SUPER::isClear(); }

	//override IEnemy
	virtual const EBossType getType() const { return m_eBossType; }
	virtual void enable() {SUPER::enable();}
	virtual void disable(){SUPER::disable();}

	bool createObject(IFactory_Boss *pFactory);

	ID3D11ShaderResourceView *	getBoneStructure();
	ID3D11ShaderResourceView *	getAnimData();

	friend class AState_BossMecha;
	friend class CState_BossMecha_Waiting;
	friend class CState_BossMecha_Moving;
};

#endif