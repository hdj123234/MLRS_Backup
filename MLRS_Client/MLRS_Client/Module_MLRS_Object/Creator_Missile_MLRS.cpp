#include "stdafx.h"
#include "Creator_Missile_MLRS.h"

#include"GlobalDataMap_MLRS.h"
#include"..\MyINI.h"


IGameObject * CMultiBuilder_Missile::build()
{
	CMissile *pObject = new CMissile;
	if (!pObject->createObject(this))
	{
		delete pObject;
		return nullptr;
	}
	return pObject;
}

EMissileType CMultiBuilder_Missile::getType()
{
	return 	CGlobalDataMap_MLRS::getMissileType(m_pModel);
}

const CMyINISection CMultiBuilder_Missile::getDefaultINISection()
{
	CMyINISection retval;
	retval.setParam("Filename", "Missile/Enemy_Missile_20160322.fbx");
	retval.setParam("PosAndScale", "0,0,0,1");
	return retval;
}
