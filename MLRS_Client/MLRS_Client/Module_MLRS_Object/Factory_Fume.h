#pragma once
#ifndef HEADER_FACTORY_FUME
#define HEADER_FACTORY_FUME

#include"..\Module_Object_Common\ParticleSystem_Spray.h"
#include"..\Module_DXComponent\Type_InDirectX.h"
#include"..\Module_Object_GameModel\Factory_Material.h"

//���漱��
class CMyINISection;

typedef CParticleSystem_Spray<Type_Fume_SprayPoint> CFume;
typedef CParticleSprayPoints<Type_Fume_SprayPoint> CParticleSprayPoints_Fume;
typedef IFactory_ParticleSystem_Spray<Type_Fume_SprayPoint> IFactory_ParticleSpray_Fume;

class CFactory_Fume :	public IFactory_ParticleSpray_Fume,
						virtual public IFactory_ParticleSprayPoints,
						virtual public IFactory_ParticleMover,
						virtual public IFactory_ParticlePainter{
private:
	typedef Type_Fume_SprayPoint PointType;

private:
	const std::string m_sName;
	const DirectX::XMFLOAT2 m_vBillboardSize;
	const unsigned int m_nMaxNumberOfSprayPoint;
	const unsigned int m_nMaxNumberOfParticle;
	const float m_fVaiationAngle;
	const float m_fInitalSpeed;
	const float m_fSpeedAttenuation;
	CFactory_Material_NoneLighting_DiffuseArray_DepthBiasBlending m_Factory_Material;

public:
	CFactory_Fume(	const std::string &rsName,
					const DirectX::XMFLOAT2 &rvBillboardSize,
					const unsigned int nMaxNumberOfSprayPoint,
					const unsigned int nMaxNumberOfParticle,
					const float fVaiationAngle	 ,
					const float fInitalSpeed	 ,
					const float fSpeedAttenuation,
					const std::vector<std::string> &rsFileNames_DiffuseMap);
	virtual ~CFactory_Fume() {}

private:
	ID3D11VertexShader*		createVS_Particle() ;
	ID3D11InputLayout *		createIL_Particle() ;
	UINT					getStride_Particle();
	std::vector<D3D11_SO_DECLARATION_ENTRY> getSODeclarationEntry();
public:
	//override IFactory_ParticleSprayPoints
	virtual ID3D11VertexShader*		createVS_SprayPoints() ;
	virtual ID3D11InputLayout *		createIL_SprayPoints() ;
	virtual ID3D11Buffer*			createVB_SprayPoints() ;
	virtual ID3D11GeometryShader*	createGS_SprayPoints() ;
	virtual std::vector<ID3D11Buffer *> createCBs_GS_Points();
	virtual UINT					getStride_SprayPoints() { return sizeof(PointType); }

	//override IFactory_ParticleMover
	virtual ID3D11VertexShader*		createVS_Mover() { return CFactory_Fume::createVS_Particle(); }
	virtual ID3D11InputLayout *		createIL_Mover() { return CFactory_Fume::createIL_Particle(); }
	virtual ID3D11GeometryShader*	createGS_Mover() ;
	virtual UINT					getStride_Mover() { return CFactory_Fume::getStride_Particle(); }
	
	//override IFactory_ParticlePainter
	virtual ID3D11VertexShader *		createVS_Painter() { return CFactory_Fume::createVS_Particle(); }
	virtual ID3D11InputLayout *			createIL_Painter() { return CFactory_Fume::createIL_Particle(); }
	virtual ID3D11GeometryShader *		createGS_Painter() ;
	virtual std::vector<ID3D11Buffer *> createCBs_GS_Painter() ;
	virtual UINT						getStride_Painter() { return CFactory_Fume::getStride_Particle(); }
	virtual RIMaterial_SetterOnDX *		createMaterial() ;
	
	//override IFactory_ParticleSystem_Spray
	virtual CParticleSprayPoints<PointType> *	createParticleSprayPoints() ;
	virtual CParticleMover *					createParticleMover() ;
	virtual CParticlePainter_Spray *			createParticlePainter() ;
	virtual std::array<ID3D11Buffer *, 2>		createParticleBuffers() ;


	static const CMyINISection getDefaultINISection();

};

#endif