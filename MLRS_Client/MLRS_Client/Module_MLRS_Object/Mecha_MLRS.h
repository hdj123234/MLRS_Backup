#ifndef HEADER_MECHA_MLRS
#define HEADER_MECHA_MLRS

#include"Type_MLRS.h"
#include"..\Module_Object_Instancing\GameObject_Instanced.h"
#include"..\Module_Object\GameObject_ModelBase.h"
#include"..\Module_Object\BoundingObject.h"
#include"..\Module_Object_Common\InWorldData.h"
#include"..\Module_Object\Proxy.h"

//----------------------------------- Creator Interface -------------------------------------------------------------------------------------

class RIFactory_Mecha_PosAndLocal  {
public:
	virtual ~RIFactory_Mecha_PosAndLocal() = 0 {}

	virtual DirectX::XMFLOAT3  getPosition() = 0;
	virtual DirectX::XMFLOAT3  getRight() = 0;
	virtual DirectX::XMFLOAT3  getUp() = 0;
	virtual DirectX::XMFLOAT3  getLook() = 0;
};


class IFactory_Mecha : public IFactory_GameObject_ModelBase_Anim ,
						virtual public RIFactory_Mecha_PosAndLocal {
public:
	virtual ~IFactory_Mecha() = 0 {}

	virtual ID3D11Buffer *  createConstBuffer_World() = 0;
};

class AFactory_Mecha : public IFactory_Mecha {
protected:
	std::string m_sName;
public:
	AFactory_Mecha(const std::string &rsName) : m_sName (rsName){ }
	virtual ~AFactory_Mecha() = 0 {}

	virtual ID3D11Buffer *  createConstBuffer_World();
	virtual DirectX::XMFLOAT3  getPosition(){return DirectX::XMFLOAT3(0,0,0);}
	virtual DirectX::XMFLOAT3  getRight()	{return DirectX::XMFLOAT3(1,0,0);}
	virtual DirectX::XMFLOAT3  getUp()		{return DirectX::XMFLOAT3(0,1,0);}
	virtual DirectX::XMFLOAT3  getLook()	{return DirectX::XMFLOAT3(0,0,1);}
	void setName(const std::string &rsName) { m_sName = rsName; }
};
//----------------------------------- Class -------------------------------------------------------------------------------------

class CMecha :	public AGameObject_ModelBase_Anim,
				virtual public RIGameObject_Movable,
				virtual public RIGameObject_HandleMsg,
				virtual public RIGameObject_PositionGetter,
				virtual public RIGameObject_LocalVectorGetter,
				virtual public RIGameObject_Collision {
private:
	typedef AGameObject_ModelBase_Anim SUPER;
	
private:
	CInWorldData_Matrix_FromLocal m_WorldData;
	CBoundingObject_OOBB m_BoundingObject;

	ID3D11Buffer *m_pCBuffer_World;

	static const FLAG_8 s_ObjectFlag = FLAG_OBJECTTYPE_DRAWALBE;
protected:
	ENUMTYPE_256 m_eAnimState;
	ENUMTYPE_256 m_eAnimState_Past;

	bool m_bEnable;

public:
	CMecha();
	virtual ~CMecha() { CMecha::releaseObject(); }

private:
	virtual void drawReady(CRendererState_D3D *pRendererState)const;

public:
	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_ObjectFlag; }

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg);

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject *pMsg) {}

	//override RIGameObject_PositionGetter
	virtual DirectX::XMFLOAT3A* getPositionOrigin()const { return &m_WorldData.m_rvPos; }
	virtual const DirectX::XMFLOAT3 getPosition() const { return DirectX::XMFLOAT3(m_WorldData.m_rvPos.x, m_WorldData.m_rvPos.y, m_WorldData.m_rvPos.z); } 

	//override RIGameObject_DirectionGetter
	virtual DirectX::XMFLOAT3A* getDirectionOrigin() const { return &m_WorldData.m_rvLook; };
	virtual const DirectX::XMFLOAT3 getDirection() const { return DirectX::XMFLOAT3(m_WorldData.m_rvLook.x, m_WorldData.m_rvLook.y, m_WorldData.m_rvLook.z); }

	//override RIGameObject_LocalVectorGetter
	virtual DirectX::XMFLOAT3A* getRightOrigin()const {return &m_WorldData.m_rvRight;}
	virtual DirectX::XMFLOAT3A* getUpOrigin()	const {return &m_WorldData.m_rvUp;}
	virtual DirectX::XMFLOAT3A* getLookOrigin() const {return &m_WorldData.m_rvLook;}

	//override RIGameObject_StateGetter
	virtual const ENUMTYPE_256 getState() const { return m_eAnimState; }
	virtual void setState(const ENUMTYPE_256 eState) { m_eAnimState_Past = m_eAnimState; m_eAnimState = eState; }

	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const { if (!m_bEnable)	return nullptr; return &m_BoundingObject; }
	
	const bool isAnimEnd()const { return SUPER::isAnimEnd(); }
	virtual void enable() {  m_bEnable = true; }
	virtual void disable() { m_bEnable = false; }

	void updateMatrix();

	bool createObject(IFactory_Mecha *pFactory);
	void releaseObject();

//	void setAnimState(ENUMTYPE_256 eAnimState) { m_eAnimState = eAnimState; }
};



//----------------------------------- 인스턴싱용 -------------------------------------------------------------------------------------


//----------------------------------- Creator Interface -------------------------------------------------------------------------------------

class IFactory_Mecha_Instanced : public IFactory_GameObject_Instanced,
								virtual public RIFactory_Mecha_PosAndLocal {
public:
	virtual ~IFactory_Mecha_Instanced() = 0 {}
};

class AFactory_Mecha_Instanced : public IFactory_Mecha_Instanced {
public:
	AFactory_Mecha_Instanced(IGameModel *pModel);
	virtual ~AFactory_Mecha_Instanced() = 0 {}

	virtual DirectX::XMFLOAT3  getPosition() { return DirectX::XMFLOAT3(0, 0, 0); }
	virtual DirectX::XMFLOAT3  getRight() { return DirectX::XMFLOAT3(1, 0, 0); }
	virtual DirectX::XMFLOAT3  getUp() { return DirectX::XMFLOAT3(0, 1, 0); }
	virtual DirectX::XMFLOAT3  getLook() { return DirectX::XMFLOAT3(0, 0, 1); }
};
//----------------------------------- Class -------------------------------------------------------------------------------------

class CMecha_Instanced : public AGameObject_Instanced_Anim<Type_InstanceData_Mecha>,
								virtual public RIGameObject_Movable,
								virtual public RIGameObject_HandleMsg,
								virtual public RIGameObject_PositionGetter,
								virtual public RIGameObject_LocalVectorGetter,
//								virtual public RIGameObject_StateGetter,
								virtual public RIGameObject_DirectionGetter,
								virtual public RIGameObject_Collision{
private:
	typedef AGameObject_Instanced_Anim<Type_InstanceData_Mecha> SUPER;
private:
	CInWorldData_FromLocal m_WorldData;
	CBoundingObject_OOBB m_BoundingObject;

	CProxyManager<RIGameObject_PositionGetter> m_ProxyManager_Pos;

	static const FLAG_8 s_ObjectFlag = FLAG_OBJECTTYPE_ANIMATE | FLAG_OBJECTTYPE_MOVABLE;
protected:
//	ENUMTYPE_256 m_eAnimState;
//	ENUMTYPE_256 m_eAnimState_Past;

	bool m_bEnable;

public:
	CMecha_Instanced();
	virtual ~CMecha_Instanced() { CMecha_Instanced::releaseObject(); }

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_ObjectFlag; }

	//override RIGameObject_Movable
	virtual void move(const IMsg_Movement *pMoveMsg);

	//override RIGameObject_PositionGetter
	virtual DirectX::XMFLOAT3A* getPositionOrigin()const { return &m_WorldData.m_rvPos; }
	virtual const DirectX::XMFLOAT3 getPosition()const { return DirectX::XMFLOAT3(m_WorldData.m_rvPos.x, m_WorldData.m_rvPos.y, m_WorldData.m_rvPos.z); }

	//override RIGameObject_DirectionGetter
	virtual DirectX::XMFLOAT3A* getDirectionOrigin() const	{return &m_WorldData.m_rvLook;};
	virtual const DirectX::XMFLOAT3 getDirection() const	{return DirectX::XMFLOAT3(m_WorldData.m_rvLook.x, m_WorldData.m_rvLook.y, m_WorldData.m_rvLook.z);}

	//override RIGameObject_LocalVectorGetter
	virtual DirectX::XMFLOAT3A* getRightOrigin()const { return &m_WorldData.m_rvRight; }
	virtual DirectX::XMFLOAT3A* getUpOrigin()	const { return &m_WorldData.m_rvUp; }
	virtual DirectX::XMFLOAT3A* getLookOrigin() const { return &m_WorldData.m_rvLook; }

	//override RIGameObject_StateGetter
//	virtual const ENUMTYPE_256 getState() const { return m_eAnimState; }
//	virtual void setState(const ENUMTYPE_256 eState) { m_eAnimState_Past = m_eAnimState; m_eAnimState = eState; }

	//override RIGameObject_Collision
	virtual const IBoundingObject * getBoundingObject()const { if (!m_bEnable)	return nullptr; return &m_BoundingObject; }
	
	//override AGameObject_Instanced_Anim<Type_InstanceData_Mecha>
	virtual void setInstanceData(Type_InstanceData_Mecha *pInstanceData_Origin);//RIGameModel_Instancing에서 호출

	const bool isAnimEnd()const { return SUPER::isAnimEnd(); }

	virtual void enable() { if (!m_bEnable) SUPER::enable(); m_bEnable = true; }
	virtual void disable() { if (m_bEnable) SUPER::disable();m_bEnable = false; }

//	const ENUMTYPE_256 getAnimState()const { return m_eAnimState; }
//	void setAnimState(const ENUMTYPE_256 eState) { m_eAnimState_Past = m_eAnimState; m_eAnimState = eState; }

	//virtual void revertState() { m_eAnimState = m_eAnimState_Past ;	SUPER::setAction(m_eAnimState);}

	CProxy<RIGameObject_PositionGetter> * getPositionProxy() { return m_ProxyManager_Pos.getNewProxy(); }

	void updateMatrix();

	bool createObject(IFactory_Mecha_Instanced *pFactory);
	void releaseObject();



//	void setAnimState(ENUMTYPE_256 eAnimState) { m_eAnimState = eAnimState; }
};

#endif