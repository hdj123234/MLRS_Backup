#include "stdafx.h"
#include"Mesh_MLRS.h"

#include"..\Module_Renderer\RendererState_D3D.h"


//----------------------------------- CVertex_Mesh_MLRS -------------------------------------------------------------------------------------

CVertex_Mesh_MLRS::CVertex_Mesh_MLRS(
	const unsigned int nVERTEXINDEX,
	const unsigned int nUVIndex,
	const DirectX::XMFLOAT3 & rvNormal,
	const DirectX::XMFLOAT3 & rvTangent)
	:m_nVertexIndex(nVERTEXINDEX),
	m_nUVIndex(nUVIndex),
	m_vNormal(rvNormal),
	m_vTangent(rvTangent)
{
}
//----------------------------------- CMesh_FBXDummy -------------------------------------------------------------------------------------


void CMesh_MLRS::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_VS(m_pBuffers_WVP);
	pRendererState->setShaderResourceViews_VS(m_pTBuffers);
}


bool CMesh_MLRS::createObject(IFactory_Mesh_MLRS * pFactory)
{
	m_pTBuffers = pFactory->createTBuffers();
	m_pBuffers_WVP = pFactory->createConstBuffer_Transform();
	if (m_pTBuffers.size() != 3 || m_pBuffers_WVP.size() != 3)	return false;

	return CMesh_Vertex::createObject(pFactory);
}

//----------------------------------- CMesh_MLRS_Instancing -------------------------------------------------------------------------------------


void CMesh_MLRS_Instancing::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_VS(m_pBuffers_VP);
	pRendererState->setShaderResourceViews_VS(m_pTBuffers);
}

bool CMesh_MLRS_Instancing::createObject(IFactory_Mesh_MLRS_Instancing * pFactory)
{
	m_pTBuffers = pFactory->createTBuffers();
	m_pBuffers_VP = pFactory->createConstBuffer_Transform();
	if (!(m_pTBuffers.size() == 3 && m_pBuffers_VP.size() == 2))	return false;

	return Super::createObject(pFactory);
}
