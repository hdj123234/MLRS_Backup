#ifndef HEADER_CREATOR_MAINMECHA_MLRS
#define HEADER_CREATOR_MAINMECHA_MLRS

#include"ClientPlayer.h"
#include"MainMecha_MLRS.h"
#include"Factory_Camera_MLRS.h"
#include"..\..\FBXConverter\Type_MyGameModel.h"

//전방선언
class CFactory_Camera_ThirdPerson;
struct CameraState_ThirdPerson;
enum EActionType : ENUMTYPE_256;
class CMyINISection;

 
class AMultiBuilder_MainMecha : virtual public IMultiBuilder<IMainMecha>,
	virtual public IFactory_Player_Decorator_MLRS {
protected:
	IGameModel *m_pModel;
	CameraState_MLRS &m_rCameraState;
	CMyType_Guides m_Guides;
	//const float m_fShootDelay_Weapon1;
	//const float m_fLaunchTime_Weapon2;
	//const float m_fMaxSpeed_Weapon2;
	//const float m_fAcceleration_Weapon2;
	//const float m_fLaunchTime_Weapon3;
	CType_Missile1 m_Retval_Weapon1;
	CType_Missile2 m_Retval_Weapon2;
	CType_Missile3 m_Retval_Weapon3;
public:
	AMultiBuilder_MainMecha(IGameModel *pModel,
		CameraState_MLRS &rCameraState,
		CMyType_Guides &rGuides,
		CMyINISection &rSection_Weapon1,
		CMyINISection &rSection_Weapon2,
		CMyINISection &rSection_Weapon3
	//	const float fShootDelay_Weapon1,
	//	const float fLaunchTime_Weapon2,
	//	const float fMaxSpeed_Weapon2,
	//	const float fAcceleration_Weapon2,
	//	const float fLaunchTime_Weapon3
	);
	virtual ~AMultiBuilder_MainMecha() {}

private:
//	virtual IGameObject * build_ObjectCore()=0;
	virtual EPlayerType getType() ;

public:
	//CMultiBuilder<IMainMecha>
	virtual IMainMecha * build_ClientPlayer();

	virtual CCamera_MLRS * createCamera();
	virtual CMainMecha_Instanced * getObjectOrigin() { return dynamic_cast<CMainMecha_Instanced *>(this->build()); }


	virtual const CType_Missile1 getMissile1(){return m_Retval_Weapon1;}
	virtual const CType_Missile2 getMissile2(){return m_Retval_Weapon2;}
	virtual const CType_Missile3 getMissile3(){return m_Retval_Weapon3;}
//	virtual const float getRotationUnitY() { return m_rCameraState.m_fRotationUnitY_TPS; }
//	virtual const DirectX::XMFLOAT3 getMissilePos() { return m_Guides.m_GuideMap[EMyType_Guide::eGuide_Weapon1].m_vPos; }
//	virtual const float getShootDelay() { return m_fShootDelay; }
}; 




//----------------------------------- 인스턴싱용 -------------------------------------------------------------------------------------


class CFactory_MainMecha_Abattre_Instanced : public AFactory_Mecha_Instanced,
											virtual public IFactory_MainMecha_Instanced {
private:
	IGameModel *m_pModel;
public:
	CFactory_MainMecha_Abattre_Instanced(IGameModel *pGameModel) :AFactory_Mecha_Instanced(m_pModel),m_pModel(pGameModel) {}
	virtual ~CFactory_MainMecha_Abattre_Instanced() {}

	//override IFactory_GameObject_ModelBase
	virtual IGameModel * getModel() { return m_pModel; }

	virtual DirectX::XMFLOAT3  getPosition() { return DirectX::XMFLOAT3(0, 0, 0); }
	virtual DirectX::XMFLOAT3  getRight() { return DirectX::XMFLOAT3(1, 0, 0); }
	virtual DirectX::XMFLOAT3  getUp() { return DirectX::XMFLOAT3(0, 1, 0); }
	virtual DirectX::XMFLOAT3  getLook() { return DirectX::XMFLOAT3(0, 0, 1); }

	virtual EPlayerType getPlayerType() { return EPlayerType(0); }

	static std::map<unsigned int, EActionType> getStartPointList();
	static const CMyINISection getDefaultINISection();
	static const CMyINISection getDefaultINISection_Weapon1();
	static const CMyINISection getDefaultINISection_Weapon2();
	static const CMyINISection getDefaultINISection_Weapon3();
};


class CMultiBuilder_MainMecha_Instanced : public AMultiBuilder_MainMecha {
public:
	//CMultiBuilder_MainMecha_Instanced(IGameModel *pModel,
	//	CameraState_MLRS &rCameraState,
	//	CMyType_Guides &rGuides,
	//	const float fShootDelay,
	//	const float fLaunchTime);

	CMultiBuilder_MainMecha_Instanced(IGameModel *pModel,
		CameraState_MLRS &rCameraState,
		CMyType_Guides &rGuides,
		CMyINISection &rSection_Weapon1,
		CMyINISection &rSection_Weapon2,
		CMyINISection &rSection_Weapon3);
	virtual ~CMultiBuilder_MainMecha_Instanced() {}

public:
	virtual IMainMecha * build();
//	virtual IGameObject * build_ObjectCore();
};



#endif