#include "stdafx.h"
#include"Factory_BaseCamp_MLRS.h"

#include"GlobalDataMap_MLRS.h"

#include"..\Module_DXComponent\Factory_DXComponent_Common.h"

#include"..\Module_Object\Factory_AnimController.h"
#include"..\MyINI.h"


CFactory_BaseCamp::CFactory_BaseCamp(IGameModel * pGameModel)
	:m_pModel(pGameModel)
{

}

IAnimController * CFactory_BaseCamp::createAnimController()
{
	CAnimController *pAnimController = new CAnimController;
	if (!pAnimController->createObject(&CFactory_AnimController(m_pModel)))
	{
		delete pAnimController;
		pAnimController = nullptr;
	}
	return pAnimController;
}

ID3D11Buffer * CFactory_BaseCamp::createConstBuffer_World()
{
	return factory_CB_Matrix_World.createBuffer();
}

std::map<unsigned int, EActionType> CFactory_BaseCamp::getStartPointList()
{
	return std::map<unsigned int, EActionType>({std::make_pair(0,EActionType::eActionType_Default)});
}

const CMyINISection CFactory_BaseCamp::getDefaultINISection()
{

	CMyINISection retval;
	retval.setParam("Filename", "BaseCamp/BaseCapn_Low_Mapping_160508.fbx");
	retval.setParam("PosAndScale", "0,0,0, 1");

	return retval;
}
