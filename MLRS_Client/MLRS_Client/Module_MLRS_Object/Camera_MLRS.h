#pragma once
#ifndef HEADER_CAMERA_MLRS
#define HEADER_CAMERA_MLRS

#include "..\Module_Object_Camera\View.h"
#include "..\Module_Object_Camera\Camera.h"

#ifndef HEADER_STL
#include<map>
#endif // !HEADER_STL

//
////���漱��
//struct ID3D11Buffer;

enum ECameraMode : ENUMTYPE_256
{
	eDebug = 0xff,
	eDefault = 0,
	eWeapon1 = 1,
	eWeapon2 = 2,
	eWeapon3_LockOn = 3,
	eWeapon3_Shoot = 4,
};

class IFactory_CameraModeManager {
protected:
	typedef float ModeChangeTimeMap;
	typedef std::pair<IView *, ModeChangeTimeMap> ViewData;
	typedef std::map<ECameraMode, ViewData> ViewMap;
public:
	virtual ~IFactory_CameraModeManager() = 0 {}

	virtual ViewMap getViewMap()=0;
};

class IFactory_Camera_MLRS  :	virtual public IFactory_Camera,
								virtual public IFactory_CameraModeManager {
public:
	virtual ~IFactory_Camera_MLRS() = 0 {}

};

class CView_Morph : virtual public IView{
private:
	DirectX::XMFLOAT3A * const m_pvPos;
	DirectX::XMFLOAT3A * const m_pvRight;
	DirectX::XMFLOAT3A * const m_pvUp;
	DirectX::XMFLOAT3A * const m_pvLook;

	IView *m_pView1;
	IView *m_pView2;
	float m_fParameter_t;
	float m_fMorpTime;

	mutable DirectX::XMFLOAT4X4A *m_pmReturnValue;
public:
	CView_Morph();
	virtual ~CView_Morph();

	//override IView
	virtual void move(const IMsg_Movement *pMoveMsg) { m_pView2->move(pMoveMsg); }
	virtual void resetView(IGameObject *pObj) { m_pView2->resetView(pObj); }

	virtual const DirectX::XMFLOAT4X4A & getMatrix()const ;
	virtual const DirectX::XMFLOAT3A * getPosition()const	;
	virtual const DirectX::XMFLOAT3A * getLocalRight()const ;
	virtual const DirectX::XMFLOAT3A * getLocalUp()const	;
	virtual const DirectX::XMFLOAT3A * getLocalLook()const	;

	void setView(IView *pView1, IView *pView2, const float fMorpTime) { m_pView1 = pView1; m_pView2 = pView2; m_fParameter_t = 0;m_fMorpTime = fMorpTime;  }
	bool addT(float fParameter_t) { m_fParameter_t += fParameter_t; return (m_fParameter_t >= m_fMorpTime); }

};


class CCameraModeManager {
private:
	typedef float ModeChangeTimeMap;
	typedef std::pair<IView *, ModeChangeTimeMap> ViewData;
	typedef std::map<ECameraMode, ViewData> ViewMap;

private:
	ViewMap m_ViewMap;
	ECameraMode m_eMode;

	CView_Morph m_View_Morph;
	bool m_bMorph;

public:
	CCameraModeManager();
	~CCameraModeManager() { CCameraModeManager::releaseObject(); }

	IView * getCurrentView() { return m_ViewMap[m_eMode].first; }
	const IView * getCurrentView()const { 
		auto iter = m_ViewMap.find(m_eMode); 
		if (iter == m_ViewMap.end()) return nullptr;
		else  return iter->second.first; }
	IView * setCameraMode(ECameraMode eMode, IGameObject *pObj);
	IView * animateObject(const float fElapsedTime);

	void setTarget_AllCamera(const IGameObject *pTarget);

	const ECameraMode getCameraMode() { return m_eMode; }
	void move(const IMsg_Movement *pMoveMsg);

	bool createObject(IFactory_CameraModeManager *pFactory);
	void releaseObject();
};

class CCamera_MLRS :	public CCamera,
						virtual public RICamera_Target,
						virtual public RICamera_MultiMode,
						virtual public RIGameObject_Animate {
private:
	typedef CCamera SUPER;

private:
	CCameraModeManager m_ModeManager;

public:
	CCamera_MLRS() {}
	virtual ~CCamera_MLRS() { m_pView = nullptr; }

	//override ICamera
	virtual void resetCamera(IGameObject *pObj) { m_ModeManager.getCurrentView()->resetView(pObj); }

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime) ;

	//override RICamera_MultiMode
	virtual void setCameraMode(const ENUMTYPE_256 eMode, IGameObject *pObj) { m_pView= m_ModeManager.setCameraMode(ECameraMode(eMode), pObj); }

	//override CCamera
	void move(const IMsg_Movement *pMoveMsg) { m_ModeManager.move(pMoveMsg); }

	//override RICamera_Target
	virtual void setTarget(const IGameObject *pTarget) { m_ModeManager.setTarget_AllCamera(pTarget); }

	//override RIGameObject_PositionGetter
	virtual DirectX::XMFLOAT3A* getPositionOrigin()const  { return nullptr; }
	virtual const DirectX::XMFLOAT3 getPosition() const { auto v = m_ModeManager.getCurrentView()->getPosition(); return DirectX::XMFLOAT3(v->x, v->y, v->z); }

	//override RIGameObject_DirectionGetter
	virtual DirectX::XMFLOAT3A* getDirectionOrigin() { return nullptr; }
	virtual DirectX::XMFLOAT3 getDirection() {auto v=  m_ModeManager.getCurrentView()->getLocalLook();return DirectX::XMFLOAT3(v->x, v->y, v->z); }

	bool createObject(IFactory_Camera_MLRS *pFactory);
};


#endif