#include "stdafx.h"
#include"Creator_BossMecha_MLRS.h"

#include"GlobalDataMap_MLRS.h"

#include"EnemyMecha_MLRS.h"

#include"..\Module_Object\Factory_AnimController.h"
#include"..\Module_Object_GameModel\GameModel.h"
#include"..\MyINI.h"


CFactory_BossMecha::CFactory_BossMecha(const std::string & rsName, IGameModel * pGameModel)
	:AFactory_Mecha(rsName),
	m_pModel(pGameModel)
{
}

IAnimController * CFactory_BossMecha::createAnimController()
{


	CAnimController *pAnimController = new CAnimController;
	if (!pAnimController->createObject(&CFactory_AnimController(m_pModel)))
	{
		delete pAnimController;
		pAnimController = nullptr;
	}
	return pAnimController;
}

std::map<unsigned int, EActionType> CFactory_BossMecha::getStartPointList()
{
	return std::map<unsigned int, EActionType>({ std::make_pair(0,EActionType::eActionType_Default) });

}

const CMyINISection CFactory_BossMecha::getDefaultINISection()
{
	CMyINISection retval;
	retval.setParam("Filename", "Abattre/Abattre_Animation.fbx");
	retval.setParam("PosAndScale", "0,0,0, 1");
	return retval;
}

//
//
////----------------------------------- CBuilder_BossMecha -------------------------------------------------------------------------------------
//
//CBuilder_BossMecha::CBuilder_BossMecha(
//	const std::string & rsName, 
//	IGameModel * pGameModel)
//	:CFactory_BossMecha(rsName,pGameModel)
//{
//}
//
//IBoss * CBuilder_BossMecha::build()
//{
//	CBossMecha * pBoss = new CBossMecha;
//	if (!pBoss->createObject(this))
//	{
//		delete pBoss;
//		pBoss = nullptr;
//	}
//	return  pBoss;
//}
//
//bool CBuilder_BossMecha::build(IBoss *pBoss)
//{
//	if (auto p = dynamic_cast<CBossMecha*>(pBoss))
//		return p->createObject(this);
//	else return false;
//}
