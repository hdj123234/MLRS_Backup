#include "stdafx.h"
#include "Strategy_CreateModel_CustomFBX.h"

#include"..\Module_Object_Instancing\Factory_AnimData_Instancing.h"
#include"..\Module_Object_Instancing\ModelDecorator_Instancing.h"
#include"..\Module_Object_GameModel\Factory_Material.h"
#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"
#include"..\Module_Object_GameModel\Factory_AnimData.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_DXComponent\Type_InDirectX.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"..\Module_Object\Msg_Movement.h"
#include"..\Module_Object_Camera\Factory_Camera_ThirdPerson.h"
#include"..\Module_Object\Factory_AnimController.h"
#include"..\Module_Object_Common\ModelTester.h"

#include"..\Module_Renderer\RendererState_D3D.h"
#include "..\Module_MLRS_Object\MainMecha_MLRS.h"
#include "..\MyINI.h"

using namespace DirectX;



//--------------------------------------- AStrategy_CreateModel_CustomFBX_Mesh --------------------------------------- 

std::vector<IMesh*> AStrategy_CreateModel_CustomFBX_Mesh::createMeshs(const std::string &rsName, const std::vector<CMyType_Mesh> &rMeshs)
{
	std::vector<IMesh *> result;

	for (auto &data : rMeshs)
	{
		CFactory_Mesh_CustomFBX factory(rsName+"_"+data.m_sName,data, m_sVSName);

		CMesh_MLRS *pMesh = new CMesh_MLRS;
		if (!pMesh->createObject(&factory))
		{
			delete pMesh;
			for (auto data2 : result)
				delete data2;
			return result;
		}
		result.push_back(pMesh);
	}
	return  result;
}

//--------------------------------------- AStrategy_CreateModel_CustomFBX_Mesh_Instancing --------------------------------------- 

std::vector<IMesh*> AStrategy_CreateModel_CustomFBX_Mesh_Instancing::createMeshs(const std::string & rsName, const std::vector<CMyType_Mesh>& rMeshs)
{
	std::vector<IMesh *> result;

	for (auto &data : rMeshs)
	{
		CFactory_FBXMesh_Dummy_Instancing factory(rsName + "_" + data.m_sName, data, m_nNumberOfInstance, m_sVSName);

		CMesh_MLRS_Instancing *pMesh = new CMesh_MLRS_Instancing;
		if (!pMesh->createObject(&factory))
		{
			delete pMesh;
			for (auto data2 : result)
				delete data2;
			return result;
		}
		result.push_back(pMesh);
	}
	return  result;
}

//--------------------------------------- CStrategy_CreateModel_CustomFBX_Mesh_Anim --------------------------------------- 

IAnimData * CStrategy_CreateModel_CustomFBX_Mesh_Anim::createAnimData(const std::string &rsName, CMyType_Bones& rBoneStructure, std::vector<std::vector<DirectX::XMFLOAT4X4>>& rFrameData)
{
	auto &rData = rBoneStructure.getContainer();
	const unsigned int nNumberOfBone = static_cast<unsigned int>(rData.size());
	std::vector<Type_Bone> bones(nNumberOfBone);
	for (unsigned int i = 0; i < nNumberOfBone; ++i)
	{
		bones[i].m_nParentIndex = rData[i].second.m_nParentIndex;

		DirectX::XMStoreFloat4x4(&bones[i].m_mTransform_ToLocal, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&rData[i].second.m_mTransform_ToLocal)));
	}

	CAnimData_NoInstancing * pAnimData = new CAnimData_NoInstancing();
	if (!pAnimData->createObject(&CFactory_AnimData_NoInstancing(rsName,
		bones,
		rFrameData,
		m_AnimList,
		m_fAnimFPS)))
	{
		delete pAnimData;
		pAnimData = nullptr;
	}
	return pAnimData;
}

//--------------------------------------- CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim --------------------------------------- 

IAnimData * CStrategy_CreateModel_CustomFBX_Mesh_Instancing_Anim::createAnimData(const std::string &rsName, CMyType_Bones& rBoneStructure, std::vector<std::vector<DirectX::XMFLOAT4X4>>& rFrameData)
{
	auto &rData = rBoneStructure.getContainer();
	const unsigned int nNumberOfBone = static_cast<unsigned int>(rData.size());
	std::vector<Type_Bone> bones(nNumberOfBone);
	for (unsigned int i = 0; i < nNumberOfBone; ++i)
	{
		bones[i].m_nParentIndex = rData[i].second.m_nParentIndex;

		DirectX::XMStoreFloat4x4(&bones[i].m_mTransform_ToLocal, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&rData[i].second.m_mTransform_ToLocal)));
	}

	CAnimData_Instancing * pAnimData = new CAnimData_Instancing();
	if (!pAnimData->createObject(&CFactory_AnimData_Instancing(rsName,
		std::move(bones),
		rFrameData,
		m_nNumberOfInstance,
		m_AnimList,
		m_fAnimFPS)))
	{
		delete pAnimData;
		pAnimData = nullptr;
	}
	return pAnimData;
}


//--------------------------------------- AStrategy_CreateModel_CustomFBX_Material --------------------------------------- 

std::vector<IMaterial*> AStrategy_CreateModel_CustomFBX_Material::createMaterials(const std::string &rsName, const std::vector<CMyType_Material> &rMaterials)
{
	std::vector<Type_Material> materials;
	std::vector<std::string> names_DiffuseMap;
	std::vector<std::string> names_NormalMap;
	for (auto &rData : rMaterials)
	{
		Type_Material material;
		material.m_vAmbient = rData.m_vAmbient;
		material.m_vDiffuse = rData.m_vDiffuse;
		material.m_vSpecular = rData.m_vSpecular;

		materials.push_back(material);
		names_DiffuseMap.push_back(rData.m_Maps.find(EMyType_Map::eDiffuse)->second);
		names_NormalMap.push_back(rData.m_Maps.find(EMyType_Map::eNormal)->second);
	}


	CFactory_MaterialSet_Lambert_DiffuseNormal factory_Light(	rsName,
																m_sPSName,
																materials,
																names_DiffuseMap,
																names_NormalMap);
	CMaterialSet_Lambert_DiffuseNormal *pMaterial = new CMaterialSet_Lambert_DiffuseNormal();
	if (!pMaterial->createObject(&factory_Light))
	{
		delete pMaterial;
		return std::vector<IMaterial *>();
	}

	return std::vector<IMaterial *>{pMaterial};

}

//--------------------------------------- CStrategy_CreateModel_CustomFBX_Material --------------------------------------- 

const std::map<bool, std::string> CStrategy_CreateModel_CustomFBX_Material::s_PSNameMap_DeferredLighting = { std::make_pair(false,"psDummy_FBX"), std::make_pair(true,"psDummy_FBX_DeferredLighting")};
bool CStrategy_CreateModel_CustomFBX_Material::s_bDeferredLighting = false;