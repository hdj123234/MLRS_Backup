#pragma once
#ifndef HEADER_FACTORY_UIMANAGER_MLRS
#define HEADER_FACTORY_UIMANAGER_MLRS

#include"..\MyINI.h"
#include"..\Module_Object_UI\UIManager.h"
#include"..\Module_Object_UI\Factory_UI_ScriptManager.h"

//���漱��
//class CMyINIData;
class CMyCSVTable;
//class CMyINISection; 

class CFactory_UIManager_MLRS : public AFactory_UIManager {
private:
	struct CUIData {
		std::string m_sFileName;
		DirectX::XMFLOAT2 m_vPos;
		float m_fAimSize;

		std::string m_sFileName_On;
		std::string m_sFileName_Background;
		//	void resetCenter_FromLeftTop();
	};

	struct CUIData_LaunchGuide {
		DirectX::XMFLOAT4 m_vLineColor;
		DirectX::XMFLOAT4 m_vCircleColor;
		float m_fCircleRadius;

	};

private:
	CMyINIData m_INIData;
//	std::map<std::string,CUIData> m_UIDatas;
	std::vector<CScriptData> m_ScriptDatas; 
	float m_fScriptEndTime;

public:
//	CFactory_UIManager_MLRS(const UIData_Aim &rUIData_Aim);
	CFactory_UIManager_MLRS(const std::string & rsINIFileName,
							const std::string & rsScriptTable);
	virtual ~CFactory_UIManager_MLRS() {}

private:
	IUserInterface * createUI_Aim();
	IUserInterface * createUI_Icon();
	IUserInterface * createUI_HP();
	IUserInterface * createUI_Boost();
	IUserInterface * createUI_LaunchGuide();
	IUserInterface * createUI_Script();
	IUserInterface * createUI_LockOn();

	std::vector<IUserInterface*> failCreation(std::vector<IUserInterface *> &rRetval);

public:
	virtual std::vector<IUserInterface *> createUIs() ;

	static const CMyINISection getDefaultINISection();
	static void setDefaultINI(CMyINIData &rINIData);

};
#endif