#ifndef HEADER_STATE_BOSSMECHA_MLRS
#define HEADER_STATE_BOSSMECHA_MLRS

#include"Interface_Object_MLRS.h"
#include"..\MyUtility.h"

class CBossMecha;

enum class EAnimState_BossMecha : ENUMTYPE_256 {
	eIdle,
	eMoving
};

class AState_BossMecha  : public IState_Object<CBossMecha>{
protected:
	typedef CBossMecha CTarget;
	typedef AState_BossMecha AState;

protected:
	void setState(CTarget * const pObj, AState *const pState);
	virtual const EAnimState_BossMecha getActionID()const = 0;

public:
	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg) ;
	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime) ;
};


class CState_BossMecha_Waiting : public AState_BossMecha {
protected:
	virtual const EAnimState_BossMecha getActionID()const { return  (EAnimState_BossMecha::eIdle); }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);

};

class CState_BossMecha_Moving : public AState_BossMecha {
protected:
	virtual const EAnimState_BossMecha getActionID()const { return  (EAnimState_BossMecha::eMoving); }
public:
//	virtual void  handleMsg(CTarget * pObj, IMsg_GameObject * pMsg);
//	virtual std::vector<IMsg *> animateObject(CTarget * pObj, const float fElapsedTime);

};




#endif