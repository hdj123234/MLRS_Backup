#include"stdafx.h"
#include "Factory_Camera_MLRS.h"


#include"Camera_MLRS.h"
#include"..\MyINI.h"
//

CFactory_Camera_MLRS::CFactory_Camera_MLRS(CameraState_MLRS & rCameraState)
	:AFactory_Camera(rCameraState.m_cs_Projection),
	m_CameraState(rCameraState)
{
}

CFactory_Camera_MLRS::~CFactory_Camera_MLRS()
{
}

IFactory_CameraModeManager::ViewMap CFactory_Camera_MLRS::getViewMap()
{
	using std::make_pair;
	ViewMap viewMap;
	viewMap.insert(make_pair(ECameraMode::eDebug, make_pair(new CView_ThirdPerson(m_CameraState.m_vs_Debug), 0.0f)));
	viewMap.insert(make_pair(ECameraMode::eDefault, make_pair(new CView_ThirdPerson_LimitRange(m_CameraState.m_vs_Default), m_CameraState.m_fMorpTime_Default )));
	viewMap.insert(make_pair(ECameraMode::eWeapon1, make_pair(new CView_TPS(m_CameraState.m_vs_TPS), m_CameraState.m_fMorpTime_TPS)));
	viewMap.insert(make_pair(ECameraMode::eWeapon2, make_pair(new CView_TPS(m_CameraState.m_vs_Multiple), m_CameraState.m_fMorpTime_Multiple)));
	viewMap.insert(make_pair(ECameraMode::eWeapon3_LockOn, make_pair(new CView_FPS_PitchYaw(m_CameraState.m_vs_LockOn), m_CameraState.m_fMorpTime_LockOn)));
	viewMap.insert(make_pair(ECameraMode::eWeapon3_Shoot, make_pair(new CView_TPS(m_CameraState.m_vs_LockOn_Shoot), m_CameraState.m_fMorpTime_LockOn_Shoot)));

	return viewMap;
}

CameraState_MLRS::CameraState_MLRS(const CMyINIData & rData)
{
	using namespace DirectX;
	m_vs_Debug.m_vRelativePos = XMFLOAT3(10, 20, -20);
	m_vs_Debug.m_fInitDistance = 40;
	m_vs_Debug.m_fDistanceMoveUnit = 5;

	auto section_CameraPerspective = rData.getSection("CameraPerspective").setDefaultData(CameraState_MLRS::getDefaultINISection_Perspective());
	m_cs_Projection.m_fFOV = section_CameraPerspective.getParam_float("FOV");
	m_cs_Projection.m_fNearZ = section_CameraPerspective.getParam_float("NearZ");
	m_cs_Projection.m_fFarZ = section_CameraPerspective.getParam_float("FarZ");

	auto section_Camera_Default = rData.getSection("Camera_Default").setDefaultData(CameraState_MLRS::getDefaultINISection_Default());
	m_vs_Default.m_vs_ThirdPerson.m_vRelativePos = section_Camera_Default.getParam_XMFLOAT3("DefaultRelativePos");
	m_vs_Default.m_vs_ThirdPerson.m_fInitDistance = section_Camera_Default.getParam_float("DefaultDistance");
	m_vs_Default.m_vs_ThirdPerson.m_fDistanceMoveUnit = section_Camera_Default.getParam_float("DistanceMoveUnit");
	m_vs_Default.m_vRange_Distance = section_Camera_Default.getParam_XMFLOAT2("DistanceRange");
	m_vs_Default.m_vRange_AngleX_Radian = section_Camera_Default.getParam_XMFLOAT2_ToRadian("AngleRange_X");
	m_fMorpTime_Default = section_Camera_Default.getParam_float("MorpTime");

	auto section_Camera_TPS = rData.getSection("Camera_TPS").setDefaultData(CameraState_MLRS::getDefaultINISection_TPS());
	m_vs_TPS.m_vRelativePos = section_Camera_TPS.getParam_XMFLOAT3("DefaultRelativePos");
	m_vs_TPS.m_fDefaultAngle_X = section_Camera_TPS.getParam_float_ToRadian("DefaultAngle_X");
	m_vs_TPS.m_vAngleRange_X = section_Camera_TPS.getParam_XMFLOAT2_ToRadian("AngleRange_X");
	m_vs_TPS.m_fRotationUnit_X = section_Camera_TPS.getParam_float("RotationUnit_X");
//	m_fRotationUnitY_TPS = section_Camera_TPS.getParam_float("YRotationUnit");
	m_fMorpTime_TPS = section_Camera_TPS.getParam_float("MorpTime");

	auto section_Camera_Multiple = rData.getSection("Camera_Multiple").setDefaultData(CameraState_MLRS::getDefaultINISection_Multiple());
	m_vs_Multiple.m_vRelativePos = section_Camera_Multiple.getParam_XMFLOAT3("DefaultRelativePos");
	m_vs_Multiple.m_fDefaultAngle_X = section_Camera_Multiple.getParam_float_ToRadian("DefaultAngle_X");
	m_vs_Multiple.m_vAngleRange_X = section_Camera_Multiple.getParam_XMFLOAT2_ToRadian("AngleRange_X");
	m_vs_Multiple.m_fRotationUnit_X = section_Camera_Multiple.getParam_float("RotationUnit_X");
//	m_fRotationUnitY_Multiple = section_Camera_Multiple.getParam_float("YRotationUnit");
	m_fMorpTime_Multiple = section_Camera_Multiple.getParam_float("MorpTime");

	auto section_Camera_LockOn = rData.getSection("Camera_LockOn").setDefaultData(CameraState_MLRS::getDefaultINISection_LockOn());
	m_vs_LockOn.m_vRelativePos = section_Camera_LockOn.getParam_XMFLOAT3("RelativePos");
	m_vs_LockOn.m_vRotationUnit = section_Camera_LockOn.getParam_XMFLOAT2("RotationUnit");
	m_vs_LockOn.m_vRange_AngleX = section_Camera_LockOn.getParam_XMFLOAT2_ToRadian("AngleRange_X");
	m_vs_LockOn.m_vRange_AngleY = section_Camera_LockOn.getParam_XMFLOAT2_ToRadian("AngleRange_Y");
	m_fMorpTime_LockOn = section_Camera_LockOn.getParam_float("MorpTime");

	auto section_Camera_LockOn_Shoot = rData.getSection("Camera_LockOn_Shoot").setDefaultData(CameraState_MLRS::getDefaultINISection_LockOn_Shoot());
	m_vs_LockOn_Shoot.m_vRelativePos = section_Camera_LockOn_Shoot.getParam_XMFLOAT3("DefaultRelativePos");
	m_vs_LockOn_Shoot.m_fDefaultAngle_X = section_Camera_LockOn_Shoot.getParam_float_ToRadian("DefaultAngle_X");
	m_vs_LockOn_Shoot.m_vAngleRange_X = section_Camera_LockOn_Shoot.getParam_XMFLOAT2_ToRadian("AngleRange_X");
	m_vs_LockOn_Shoot.m_fRotationUnit_X = section_Camera_LockOn_Shoot.getParam_float("RotationUnit_X");
	m_fMorpTime_LockOn_Shoot = section_Camera_LockOn_Shoot.getParam_float("MorpTime");
}

const CMyINISection CameraState_MLRS::getDefaultINISection_Perspective()
{
	CMyINISection retval;
	retval.setParam("FOV", "45");
	retval.setParam("NearZ", "1");
	retval.setParam("FarZ", "5000");
	return retval;
}

const CMyINISection CameraState_MLRS::getDefaultINISection_Default()
{
	CMyINISection retval;
	retval.setParam("DefaultRelativePos", "10, 20, -20");
	retval.setParam("DefaultDistance", "40");
	retval.setParam("DistanceMoveUnit", "0.1");
	retval.setParam("DistanceRange", "10, 100");
	retval.setParam("AngleRange_X", "-60, 90");
	retval.setParam("MorpTime", "1");
	return retval;
}

const CMyINISection CameraState_MLRS::getDefaultINISection_TPS()
{
	CMyINISection retval;
	retval.setParam("DefaultRelativePos", "15, 40, -60");
	retval.setParam("DefaultAngle", "0, 95");
	retval.setParam("AngleRange_X", "-60, 90");
	retval.setParam("RotationUnit_X", "0.02");
	retval.setParam("MorpTime", "1");
	return retval;
}

const CMyINISection CameraState_MLRS::getDefaultINISection_Multiple()
{
	CMyINISection retval;
	retval.setParam("DefaultRelativePos", "15, 40, -60");
	retval.setParam("DefaultAngle", "0, 95");
	retval.setParam("AngleRange_X", "-60, 90");
	retval.setParam("RotationUnit_X", "0.02");
	retval.setParam("MorpTime", "1");
	return retval;
}

const CMyINISection CameraState_MLRS::getDefaultINISection_LockOn()
{ 
	CMyINISection retval;
	retval.setParam("RelativePos", "0, 20, 0");
	retval.setParam("RotationUnit", "0.01, 0.03");
	retval.setParam("AngleRange_X", "-180, 180");
	retval.setParam("AngleRange_Y", "-180, 180"); 
	retval.setParam("MorpTime", "1");
	return retval;
}

const CMyINISection CameraState_MLRS::getDefaultINISection_LockOn_Shoot()
{
	CMyINISection retval;
	retval.setParam("DefaultRelativePos", "15, 140, -180");
	retval.setParam("DefaultAngle", "0, 95");
	retval.setParam("AngleRange_X", "-60, 90");
	retval.setParam("RotationUnit_X", "0.02");
	retval.setParam("YRotationUnit", "0.06");
	retval.setParam("MorpTime", "1");
	return retval;
}
