#ifndef HEADER_CREATOR_MODEL_CUSTOMFBX
#define HEADER_CREATOR_MODEL_CUSTOMFBX

#include"Strategy_CreateModel_CustomFBX.h"
#include"Loader_CustomFBX.h" 
//
#include "..\Module_Object_Instancing\ModelDecorator_Instancing.h" 
#include"..\..\FBXConverter\Type_MyGameModel.h"


class CFactory_FBXModel :	virtual public IFactory_GameModel_BoundingBox,
							virtual public IFactory_InstancingModel_Decorator{
protected:
	typedef std::map<unsigned int, EActionType> StartPointList;

protected:
	CLoader_CustomFBX m_Loader;
	const std::string m_sFileName;
	AStrategy_CreateModel_CustomFBX_Mesh * m_pStrategy_Mesh;
	AStrategy_CreateModel_CustomFBX_Material *m_pStrategy_Material;

public:
	CFactory_FBXModel(const std::string &rsFileName,
		AStrategy_CreateModel_CustomFBX_Mesh * pStrategy_Mesh,
		AStrategy_CreateModel_CustomFBX_Material *pStrategy_Material,
		const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1));
	CFactory_FBXModel(const CMyINISection &rSection,
		AStrategy_CreateModel_CustomFBX_Mesh * pStrategy_Mesh,
		AStrategy_CreateModel_CustomFBX_Material *pStrategy_Material);
	virtual ~CFactory_FBXModel() { releaseObject(); }

	//override IFactory_GameModel
	virtual std::vector<IMesh			*> createMeshs() { return m_pStrategy_Mesh->createMeshs(m_sFileName + "_Mesh", m_Loader.getModel().m_Meshs); }
	virtual std::vector<IMaterial		*> createMaterials() { return m_pStrategy_Material->createMaterials(m_sFileName + "_Material", m_Loader.getModel().m_Materials); }
	virtual IAnimData	* createAnimData() { return m_pStrategy_Mesh->createAnimData(m_sFileName + "_Material", m_Loader.getModel().m_Bones, m_Loader.getModel().m_AnimTransform); }

	//override IFactory_GameModel_BoundingBox
	virtual const DirectX::XMFLOAT3 getCenter() { return m_Loader.getCenter();}//CFBXLoader::getCenter(); }
	virtual const DirectX::XMFLOAT3 getExtent() { return m_Loader.getExtents();}//CFBXLoader::getExtent(); }

	//override IFactory_InstancingModel_Decorator
	virtual IGameModel * createModelOrigin();


	const unsigned int getNumberOfBone() { return static_cast<const unsigned int>(m_Loader.getModel().m_Bones.getNumberOfBone()); }
	const unsigned int getNumberOfFrameData() { return static_cast<const unsigned int>(m_Loader.getModel().m_AnimTransform[0].size()); }

	CMyType_Bones &getBoneStructure() { return m_Loader.getModel().m_Bones; }
	std::vector<std::vector<DirectX::XMFLOAT4X4>> &getFrameData() { return m_Loader.getModel().m_AnimTransform; }

	CMyType_Guides getGuides() { return m_Loader.getGuides(); }


	void releaseObject();
};

class ABuilder_FBXModel : virtual public IBuilder<IGameModel> {
protected:
	CFactory_FBXModel & m_rFactory;

public:
	ABuilder_FBXModel(CFactory_FBXModel & rFactory) :m_rFactory(rFactory) {}
	virtual ~ABuilder_FBXModel() {}

	const unsigned int getNumberOfBone() { return m_rFactory.getNumberOfBone(); }
	const unsigned int getNumberOfFrameData() { return m_rFactory.getNumberOfFrameData(); }

	CMyType_Bones &getBoneStructure() { return m_rFactory.getBoneStructure(); }
	std::vector<std::vector<DirectX::XMFLOAT4X4>> &getFrameData() { return m_rFactory.getFrameData(); }
};

class CBuilder_FBXModel : public ABuilder_FBXModel { 
public:
	CBuilder_FBXModel(CFactory_FBXModel & rFactory) :ABuilder_FBXModel(rFactory) {}
	virtual ~CBuilder_FBXModel() {}

	//override IBuilder<IGameModel>
	virtual bool build(IGameModel *pModel);
	virtual IGameModel * build();
};

class CBuilder_FBXModel_Instancing : public ABuilder_FBXModel { 
public:
	CBuilder_FBXModel_Instancing(CFactory_FBXModel & rFactory) :ABuilder_FBXModel(rFactory) {}
	virtual ~CBuilder_FBXModel_Instancing() {}

	//override IBuilder<IGameModel>
	virtual bool build(IGameModel *pModel);
	virtual IGameModel * build();
};
 
#endif