#pragma once
#include "stdafx.h"
#include"Module_Network\Decorator_Network.h"
#include"Module_Platform_Windows\Platform_Window.h"
#include"Module_Platform_DirectX11\DirectXDevice.h"
#include"Module_Platform_DirectX11\Builder_DirectXDevice.h"
#include"Module_Platform_Windows\Factory_Window.h"
#include"Module_DXComponent\GlobalDirectXStorage.h"
#include"Module_MLRS\Factory_Framework_MLRS.h"
#include"Module_MLRS\Initializer_MLRS.h"

#include <tchar.h>


//�׽�Ʈ
#include"Module_Object\Proxy.h"

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	//AllocConsole();
	//FILE *fpStreamOut;
	//freopen_s(&fpStreamOut, "CONOUT$", "w", stdout);


	int result;
	CInitializer_MLRS initializer(hInstance, 
		&CFactory_Window(WindowClass_Default(), WindowData_Default(std::wstring(L"Title"), 1366, 768)),
		&CBuilder_DirectXDevice()	);
//	CWindow win((hInstance));
//	CDirectXDevice directxDevice;
//	if (!win.createObject(&CFactory_Window( WindowClass_Default(), WindowData_Default(std::wstring(L"Title"),1366,768))))		return FALSE;
//	if (!win.createWindow())		return FALSE;
//
//	CGlobalWindow::setInstance(hInstance);
//	CGlobalWindow::setInfo();
//	CGlobalWindow::setTitle(win.getTitle());
//
//
//	if (!directxDevice.createObject(&CBuilder_DirectXDevice()))		return FALSE;

	CWindow * pWin = initializer.getWindow();

	IFramework *pFramework = nullptr;
	CDecorator_GameFramework_Network framework_Net;
	pFramework = &framework_Net;
	if (!framework_Net.createObject(&CFactory_Framework_Network_MLRS("MLRS_Client.ini")))
	{
			return FALSE;
	}
	pWin->setFramework(pFramework); 
	framework_Net.setWindow(pWin);
	

//	if (!pFramework->getReady())	return FALSE;
	if (!pWin->getReady())	return FALSE;
//	pWin->hideCursor();
//	pWin->lockFocus();
	pWin->unhideCursor();
	pWin->unlockFocus();
	result= pWin->run();

	//FreeConsole();
	return result;
}


 