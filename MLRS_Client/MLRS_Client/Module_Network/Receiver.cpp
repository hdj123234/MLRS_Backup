#pragma once
#include "stdafx.h"
#include "Receiver.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif

CReceiver::CReceiver()
	:m_Sock(NULL),
	m_pMsgConverter(nullptr),
	m_bRun(true)
{ 
	m_pMsgs.reserve(100); 
}

CReceiver::~CReceiver()
{
	std::lock_guard<std::mutex> lock(m_Mutex_Container);
	for(auto data : m_pMsgs)
		delete data;
	m_pMsgs.clear();
}

std::vector<IMsg_SC*> CReceiver::getReceivedMsg()
{
	std::vector<IMsg_SC*> result;
	{
		std::lock_guard<std::mutex> lock(m_Mutex_Container);
		result = m_pMsgs;
		m_pMsgs.clear();
	}
	return result;
}

int CReceiver::run()
{
	char buf[s_nSizeOfBuf_Max];
	std::vector<char> bytes;
	while (m_bRun)
	{
		bytes.clear();
		recv(m_Sock, buf, 1, NULL);
		if (recvn_Block(m_Sock, buf + 1, buf[0] - 1, NULL) == SOCKET_ERROR)
			break;

		bytes.insert(bytes.end(), buf, buf + buf[0]);
		IMsg_SC *pMsg = m_pMsgConverter->getMsg(bytes);
		if (!pMsg)	continue;
		{
			std::lock_guard<std::mutex> lock(m_Mutex_Container); 
			m_pMsgs.push_back(pMsg);
		}
	}
	return 0;
}

int CReceiver::recvn_Block(SOCKET s, char * buf, int len, int flags)
{

	int received;
	char *ptr = buf;
	int left = len;

	while (left > 0) {
		received = recv(s, ptr, left, flags);
		if (received == SOCKET_ERROR)
		{
			//if (WSAGetLastError() == WSAETIMEDOUT)
			//	continue;
			//else
				return SOCKET_ERROR;
		}
		else if (received == 0)
			break;
		left -= received;
		ptr += received;
	}

	return (len - left);

}
