#pragma once
#include "stdafx.h"
#include "Decorator_Network.h"
#include"..\Module_Network\NetworkManager.h"

#include"..\Module_Framework\GameFramework.h"
#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif

#ifndef HEADER_STL
#include<iomanip>
#endif
//----------------------------------- CGameFramework -------------------------------------------------------------------------------------

std::vector<IMsg_CS*> CDecorator_GameFramework_Network::getSendMessages()
{
	if(!m_pFramework_ObjManagerGetter)
		return std::vector<IMsg_CS*>();
	
	std::vector<IMsg_CS*> result;
	auto &rMsgs = m_pFramework_ObjManagerGetter->getObjectManager()->getReturnMsgs();

	for (auto data : rMsgs)
	{
		if (IMsg_CS * pMsg_CS = dynamic_cast<IMsg_CS *>(data))
			result.push_back(pMsg_CS);
		else
			delete data;
	}
	return result;
}

CDecorator_GameFramework_Network::CDecorator_GameFramework_Network()
	:m_bNoneNetwork(nullptr),
	m_pFramework(nullptr),
	m_pNetworkManager(nullptr),
	m_pFramework_Divide(nullptr),
	m_pFramework_ObjManagerGetter(nullptr),
	m_bReady(false)
{
}

bool CDecorator_GameFramework_Network::getReady()
{
	if (m_pFramework->getReady() )
	{
		m_bReady = true;
		if (!m_pNetworkManager->initialize(m_sServerIP ,m_nPort))
			m_bNoneNetwork = true;
		return true;
	}
	else
		return false;
}

bool CDecorator_GameFramework_Network::advanceFrame()
{
	m_bReady = (m_bReady&&m_pFramework->isReady());
	if(!m_bReady)
		return false;
	if (m_bNoneNetwork)
		return m_pFramework->advanceFrame();
	
	//받은 메시지 처리
	std::vector<IMsg_SC *> v= m_pNetworkManager->getReceivedMsg() ;
	for (auto data : v)
		m_pFramework->processMessage(data);

	float fElapsedTime;
	if ((fElapsedTime = m_pFramework_Divide->advanceFrameUnit_Tick()) < 0)
		return true;

	m_pFramework_Divide->advanceFrameUnit_Calculate(fElapsedTime);
	
	//메시지 Send
	m_pNetworkManager->sendMsg(this->getSendMessages());

	m_pFramework_Divide->advanceFrameUnit_Render();

	return m_pFramework_Divide->advanceFrameUnit_PostProcessing();
}

bool CDecorator_GameFramework_Network::createObject(IFactory_GameFramework_NetworkDecorator * pFactory)
{
	m_pFramework = pFactory->createFramework();
	if (RIFramework_DivideFrameUnit * pFramework_Divide = dynamic_cast<RIFramework_DivideFrameUnit *>(m_pFramework))
		m_pFramework_Divide = pFramework_Divide;
	if (RIFramework_ObjectManagerGetter * pFramework_ObjManagerGetter = dynamic_cast<RIFramework_ObjectManagerGetter *>(m_pFramework))
		m_pFramework_ObjManagerGetter = pFramework_ObjManagerGetter;


	m_pNetworkManager = pFactory->createNetworkManager();
	if (!m_pFramework_ObjManagerGetter || !m_pFramework_Divide || !m_pNetworkManager)
		return false;

	m_sServerIP = pFactory->getServerIP();
	m_nPort = pFactory->getPort();

	return true;
}

void CDecorator_GameFramework_Network::releaseObject()
{
	if (m_pFramework)	delete m_pFramework;
	m_pFramework = nullptr;
	m_pFramework_Divide = nullptr;

	if (m_pNetworkManager)
	{
		m_pNetworkManager->cleanup();
		delete m_pNetworkManager;
		m_pNetworkManager = nullptr;
	}
	m_bReady = false;
}

void CDecorator_GameFramework_Network::setWindow(CWindow * pWindow)
{
	dynamic_cast<CGameFramework *>(m_pFramework)->setWindow(pWindow);
}
