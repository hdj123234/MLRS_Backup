
#pragma once
#ifndef HEADER_SENDER
#define HEADER_SENDER

#include"Interface_Network.h"
#include"..\Module_MultyThread\Interface_MultiThread.h"

#ifndef HEADER_STL
#include<mutex>
#endif // !HEADER_STL

#ifndef HEADER_WINDOWS
#include<WinSock2.h>
#endif // !HEADER_STL

class CSender : virtual public ISender,
				virtual public IThreadUnit{
private:
	SOCKET m_Sock;
	INetworkMsgConverter *m_pMsgConverter;

	std::vector<IMsg_CS *> m_pMsgs;
	std::mutex m_Mutex_Container;

	std::condition_variable m_Event;
	std::mutex m_Mutex_Event;

	volatile bool m_bRun;
public:
	CSender();
	virtual ~CSender();

	//override ISender
	virtual void setSocket(SOCKET sock) { m_Sock = sock; }
	virtual void sendMsg(std::vector<IMsg_CS *> &rpMsgs) ;

	//override IThreadUnit
	virtual int run() ;
	virtual void releaseThread() { m_bRun = false; m_Event.notify_one(); }

	void setMsgConverter(INetworkMsgConverter * pConverter) { m_pMsgConverter = pConverter; }
};

#endif