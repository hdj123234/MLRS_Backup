#pragma once
#ifndef HEADER_NETWORKMANAGER
#define HEADER_NETWORKMANAGER

#include "Interface_Network.h"
#ifndef HEADER_WINDOWS
#include<Ws2tcpip.h>
#include<winsock2.h>
#endif // !HEADER_WINDOWS

//���漱��
class IThreadManager_Single;

class CNetworkInitializer {
private:
	CNetworkInitializer() {}
	~CNetworkInitializer() {}

	static bool s_bInit;

public:
	static bool initialize();
	static void releaseNetwork();
};

class CNetworkManager : virtual public INetworkManager{
private:
	SOCKET m_Socket;
	ISender *m_pSender;
	IReceiver *m_pReceiver;
	std::vector<IThreadManager_Single *>m_pThreadManagers;
	INetworkMsgConverter *m_pNetworkMsgConverter;

public:
	CNetworkManager(INetworkMsgConverter *pMsgConverter);
	virtual ~CNetworkManager() { CNetworkManager::releaseObject(); }

	void releaseObject();

	//override INetworkManager
	virtual bool initialize(const std::wstring &rsServerIP , const short nPort );
	virtual void cleanup() { CNetworkInitializer::releaseNetwork(); }
	virtual void sendMsg(std::vector<IMsg_CS *> &rpMsgs) { if (m_pSender) m_pSender->sendMsg(rpMsgs); }
	virtual std::vector<IMsg_SC *> getReceivedMsg() 
	{
		if (m_pReceiver)	return m_pReceiver->getReceivedMsg();
		else				return std::vector<IMsg_SC *>();
	}
};




#endif