
#pragma once
#ifndef HEADER_DECORATOR_NETWORK
#define HEADER_DECORATOR_NETWORK

#include"..\Module_Network\Interface_Network.h"
#include"..\Module_Framework\Interface_Framework.h"
#include"..\Module_World\Interface_World.h"


//전방선언
class INetworkManager;
class IAdapter_Msg_Network;
class CWindow;


//----------------------------------- Creator -------------------------------------------------------------------------------------
class IFactory_GameFramework_NetworkDecorator {
public:
	virtual ~IFactory_GameFramework_NetworkDecorator() = 0 {}
	//virtual function
	virtual IFramework * createFramework() = 0;
	virtual INetworkManager * createNetworkManager() = 0;
	virtual const std::wstring getServerIP() = 0;
	virtual const short getPort() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

/*
	이하의 인터페이스를 상속받은 프레임워크에 대해서만 정상동작
	RIFramework_DivideFrameUnit
	RIFramework_ObjectManagerGetter
*/
class CDecorator_GameFramework_Network : virtual public IFramework,
										virtual public RIFramework_Network{
private:
	virtual std::vector<IMsg_CS *> getSendMessages();

	std::wstring m_sServerIP;
	int m_nPort;

	bool m_bNoneNetwork;
protected:
	IFramework *m_pFramework;
	INetworkManager *m_pNetworkManager;
	RIFramework_DivideFrameUnit *m_pFramework_Divide;
	RIFramework_ObjectManagerGetter *m_pFramework_ObjManagerGetter;
	bool m_bReady;

public:
	CDecorator_GameFramework_Network();
	virtual ~CDecorator_GameFramework_Network() { CDecorator_GameFramework_Network::releaseObject(); }
	
	//override IFramework
	virtual bool getReady();
	virtual bool advanceFrame();
	virtual void processMessage(IMsg *pMsg) { if (m_bReady)m_pFramework->processMessage(pMsg); else delete pMsg; }
	virtual const bool isReady()const { return m_bReady&&m_pFramework->isReady(); }

	IFramework * getFramework() { return m_pFramework; }
	bool createObject(IFactory_GameFramework_NetworkDecorator *pFactory);
	void releaseObject();

	void setWindow(CWindow *pWindow);
};

#endif