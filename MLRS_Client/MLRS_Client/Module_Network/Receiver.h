
#pragma once
#ifndef HEADER_RECEIVER
#define HEADER_RECEIVER

#include"Interface_Network.h"
#include"..\Module_MultyThread\Interface_MultiThread.h"

#ifndef HEADER_STL
#include<mutex>
#endif // !HEADER_STL

#ifndef HEADER_WINDOWS
#include<WinSock2.h>
#endif // !HEADER_STL

class CReceiver : virtual public IReceiver,
				virtual public IThreadUnit{
private:
	SOCKET m_Sock;
	INetworkMsgConverter *m_pMsgConverter;

	std::vector<IMsg_SC *> m_pMsgs;
	std::mutex m_Mutex_Container;

	volatile bool m_bRun;

	static const unsigned int s_nSizeOfBuf_Max=255;
public:
	CReceiver();
	virtual ~CReceiver();

	//override ISender
	virtual void setSocket(SOCKET sock) { m_Sock = sock; }
	virtual std::vector<IMsg_SC *> getReceivedMsg() ;

	//override IThreadUnit
	virtual int run() ;
	virtual void releaseThread() { m_bRun = false; }

	void setMsgConverter(INetworkMsgConverter * pConverter) { m_pMsgConverter = pConverter; }

	static int recvn_Block(SOCKET s, char *buf, int len, int flags);
	
};

#endif