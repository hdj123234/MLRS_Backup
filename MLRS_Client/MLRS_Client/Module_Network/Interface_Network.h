
#ifndef HEADER_INTERFACE_NETWORK
#define HEADER_INTERFACE_NETWORK

#include"..\Interface_Global.h"

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL

//���漱��
enum class EMsgType_SC :ENUMTYPE_256;
enum class EMsgType_CS :ENUMTYPE_256;


class IMsg_Network:virtual public IMsg {
public:
	virtual ~IMsg_Network() = 0 {}
};

//Server to Client (S->C)
class IMsg_SC :virtual public IMsg_Network {
public:
	virtual ~IMsg_SC() = 0 {}

	virtual EMsgType_SC getType() = 0;
};

//Client to Server (C->S)
class IMsg_CS :virtual public IMsg_Network {
public:
	virtual ~IMsg_CS() = 0 {}

	virtual EMsgType_CS getType() = 0;
};

#include"..\Module_Network\Interface_Network.h"

class INetworkMsgConverter {
public:
	virtual ~INetworkMsgConverter() = 0 {}

	virtual std::vector<char> getBytes(IMsg_CS *pMsg) =0;
	virtual std::vector<char> getBytes(std::vector<IMsg_CS *>&rpMsgs) = 0;

	virtual std::vector<IMsg_SC *> getMsgs(std::vector<char> &rBytes) = 0;
	virtual IMsg_SC * getMsg(std::vector<char> &rBytes) = 0;
};

class RIFramework_Network {
private:
	virtual std::vector<IMsg_CS *> getSendMessages() = 0;
public:
	virtual ~RIFramework_Network() = 0 {}

};

class RIGameWorld_Network {
public:
	virtual ~RIGameWorld_Network() = 0 {}

	virtual std::vector<IMsg_CS *> getSendMessages() = 0;
};


class INetworkManager {
public:
	virtual ~INetworkManager() = 0 {};

	virtual bool initialize(const std::wstring &rsServerIP , const short nPort ) = 0;
	virtual void cleanup() = 0;
	virtual void sendMsg(std::vector<IMsg_CS *> &rpMsgs) = 0;
	virtual std::vector<IMsg_SC *> getReceivedMsg() = 0;
};

class ISender {
public:
	virtual ~ISender() = 0 {};

	virtual void setSocket(SOCKET sock)=0;
	virtual void sendMsg(std::vector<IMsg_CS *> &rpMsgs) = 0;
};

class IReceiver {
public:
	virtual ~IReceiver() = 0 {};

	virtual void setSocket(SOCKET sock) = 0;
	virtual std::vector<IMsg_SC *> getReceivedMsg() = 0;
};
#endif