#pragma once
#include "stdafx.h"
#include "Sender.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif


CSender::CSender()
	:m_Sock(NULL),
	m_pMsgConverter(nullptr),
	m_bRun(true)
{
	m_pMsgs.reserve(100);
}

CSender::~CSender()
{
	std::lock_guard<std::mutex> lock(m_Mutex_Container);
	for (auto data : m_pMsgs)
		delete data;
	m_pMsgs.clear();
}

void CSender::sendMsg(std::vector<IMsg_CS*>& rpMsgs)
{
	{
		std::lock_guard<std::mutex> lock(m_Mutex_Container);
		m_pMsgs.insert(m_pMsgs.end(), rpMsgs.begin(), rpMsgs.end());
		if (m_pMsgs.empty())
			return;
	}
	m_Event.notify_one();
}

int CSender::run()
{
	std::vector<char> bytes;
	std::vector<char> bytes_tmp;
	std::vector<IMsg_CS *> msgsTmp;
	while (m_bRun)
	{
		std::unique_lock<std::mutex> mtx(m_Mutex_Event);
		m_Event.wait(mtx);
		{
			std::lock_guard<std::mutex> lock(m_Mutex_Container);
			bytes_tmp = m_pMsgConverter->getBytes(m_pMsgs);
			msgsTmp = m_pMsgs;
			m_pMsgs.clear();
		}
		for (auto data : msgsTmp)
			delete data;
		msgsTmp.clear();
		bytes.insert(bytes.end(), bytes_tmp.begin(), bytes_tmp.end());
		if (bytes.empty()) continue;

		send(m_Sock, bytes.data(), static_cast<UINT>(bytes.size()), NULL);
		bytes.clear();

	}
	return 0;
}
