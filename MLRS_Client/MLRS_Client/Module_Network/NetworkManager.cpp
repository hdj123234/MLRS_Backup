#include"stdafx.h"
#include "NetworkManager.h"

#include "Sender.h"
#include "Receiver.h"

#include "..\Module_MultyThread\ThreadManager_STL.h"

#ifndef HEADER_STL
#include<iostream>
#endif
#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX



//----------------------------------- CNetworkInitializer -------------------------------------------------------------------------------------

bool CNetworkInitializer::s_bInit = false;

bool CNetworkInitializer::initialize()
{
	if (s_bInit)	return true;
	else
	{

		WSADATA wsa;
		if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
			return false;
		
		s_bInit = true;
		return true;
	}
}

void CNetworkInitializer::releaseNetwork()
{
	if (s_bInit)
	{
		WSACleanup();
		s_bInit = false;
	}
}

//----------------------------------- CFactory_Mesh_CustomFBX -------------------------------------------------------------------------------------

//std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> CNetworkManager::s_wstring_converter;
//std::string CNetworkManager::s_sFileName_ServerIP = "ip.ini" ;



bool CNetworkManager::initialize(const std::wstring &rsServerIP, const short nPort)
{
	if (!CNetworkInitializer::initialize()) return false;

	IN_ADDR tmp_addr;
	if(InetPton(AF_INET, rsServerIP.data(), &tmp_addr)!=1)
		return false;
	
	m_Socket = socket(AF_INET, SOCK_STREAM, NULL);
	if (m_Socket == INVALID_SOCKET)		return false;
	
	SOCKADDR_IN serverAddress;
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr = tmp_addr;
	serverAddress.sin_port = htons(nPort);
	
	if (connect(m_Socket, (SOCKADDR*)&serverAddress, sizeof(serverAddress)) == SOCKET_ERROR) {
		MYLOGCAT(MYLOGCAT_ERROR_WINSOCK, "CNetworkManager.initialize(), connect() Error");
		std::cout << "connect() Time Out!"<<std::endl;
		return false;
	}
	CSender *pSender = new CSender;
	CReceiver *pReceiver = new CReceiver;
	m_pSender = pSender;
	m_pReceiver = pReceiver;
	pSender->setSocket(m_Socket);
	pSender->setMsgConverter(m_pNetworkMsgConverter);
	pReceiver->setSocket(m_Socket);
	pReceiver->setMsgConverter(m_pNetworkMsgConverter);
	m_pThreadManagers.resize(2);
	m_pThreadManagers[0] = (new CThreadManager_Single_STL());
	m_pThreadManagers[0]->setTask(pSender);
	m_pThreadManagers[0]->run();
	m_pThreadManagers[1] = (new CThreadManager_Single_STL());
	m_pThreadManagers[1]->setTask(pReceiver);
	m_pThreadManagers[1]->run();

	//네이글 알고리즘
	//	BOOL opt = TRUE;
	//	setsockopt(m_Sock, IPPROTO_TCP, TCP_NODELAY, (const char*)&opt, sizeof(opt));

	return true;
}



CNetworkManager::CNetworkManager(INetworkMsgConverter * pMsgConverter)
	:m_pSender(nullptr),
	m_pReceiver(nullptr),
	m_pNetworkMsgConverter(pMsgConverter)
{
}

void CNetworkManager::releaseObject()
{
	for (auto data : m_pThreadManagers)
	{
		data->releaseThread();
		delete data;
	}

	if (m_pSender)
		delete m_pSender;
	
	if (m_pReceiver)
		delete m_pReceiver;

	if (m_pNetworkMsgConverter)
		delete m_pNetworkMsgConverter;

	closesocket(m_Socket);
}
