#pragma once
#ifndef HEADER_INTERFACE_DIRECTX11
#define HEADER_INTERFACE_DIRECTX11

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL

struct ID3D11ShaderResourceView;
class CRendererState_D3D;

/*	DirectX 상태기계에 세팅하기 위한 모든 정보를 가지는 클래스는 이하의 클래스를 상속
*/
class IObject_SetOnDX {
public:
	virtual ~IObject_SetOnDX() = 0 {}

	virtual void setOnDX(CRendererState_D3D *pRendererState)const=0;
};
class IObject_UpdateOnDX {
public:
	virtual ~IObject_UpdateOnDX() = 0 {}

	virtual void updateOnDX(CRendererState_D3D *pRendererState)const = 0;
};
class IObject_InitializeOnDX {
public:
	virtual ~IObject_InitializeOnDX() = 0 {}

	virtual const bool initializeOnDX(CRendererState_D3D *pRendererState)const = 0;
};
class IObject_DrawOnDX {
public:
	virtual ~IObject_DrawOnDX() = 0 {}

	virtual void drawOnDX(CRendererState_D3D *pRendererState)const = 0;
};

/*
*/
class IDXTextureMaps {
public:
	virtual ~IDXTextureMaps() = 0 {}

	virtual std::vector<ID3D11ShaderResourceView *>  getSRVs()const=0;
};

#endif