#pragma once

#ifndef HEADER_DXUTILITY
#define HEADER_DXUTILITY

#include"GlobalDirectXDevice.h"

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif

class CBufferUpdater {
public:
	CBufferUpdater() {}
	~CBufferUpdater() {}

	//행렬은 쉐이더에서 열/행 우선 문제로 인해 전치행렬로 저장되도록 특수화됨	
	template<class TYPE>
	static bool update(	ID3D11DeviceContext *pDeviceContext, 
						ID3D11Buffer *pBuffer,
						const TYPE &rData);
	template<class TYPE>
	static bool update(	ID3D11DeviceContext *pDeviceContext, 
						ID3D11Buffer *pBuffer,
						const std::vector<TYPE> &rDatas);
	template<class TYPE>
	static bool update(ID3D11Buffer *pBuffer, const TYPE &rData)
	{
		return update(CGlobalDirectXDevice::getDeviceContext(), pBuffer, rData);
	}	
	template<class TYPE>
	static bool update(ID3D11Buffer *pBuffer, const std::vector<TYPE> &rDatas)
	{
		return update(CGlobalDirectXDevice::getDeviceContext(), pBuffer, rDatas);
	}

//	//템플릿 특수화
//	template<>
//	static bool update(	ID3D11DeviceContext *pDeviceContext, 
//						ID3D11Buffer *pBuffer, 
//						const DirectX::XMFLOAT4X4 &rData);
//	template<>
//	static bool update(	ID3D11DeviceContext *pDeviceContext, 
//						ID3D11Buffer *pBuffer, 
//						const  DirectX::XMFLOAT4X4A &rData);
//	template<>
//	static bool update(	ID3D11DeviceContext *pDeviceContext, 
//						ID3D11Buffer *pBuffer, 
//						const  DirectX::XMMATRIX &rData);
};



//함수 본문

template<class TYPE>
inline bool CBufferUpdater::update(ID3D11DeviceContext *pDeviceContext, ID3D11Buffer *pBuffer, const TYPE &rData)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CBufferUpdater.update(), Map Error");
		return false;
	}
	TYPE *pMappedResource = static_cast<TYPE *>(d3dMappedResource.pData);
	*pMappedResource = rData;
	pDeviceContext->Unmap(pBuffer, 0);
	return true;
}

template<class TYPE>
inline bool CBufferUpdater::update(ID3D11DeviceContext * pDeviceContext, ID3D11Buffer * pBuffer, const std::vector<TYPE>& rDatas)
{
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CBufferUpdater.update(), Map Error");
		return false;
	}
	TYPE *pMappedResource = static_cast<TYPE *>(d3dMappedResource.pData);
	for (auto &rData : rDatas)
	{
		*pMappedResource = rData;
		++pMappedResource;
	}
	pDeviceContext->Unmap(pBuffer, 0);
	return true;
}

//템플릿 특수화
template<>
inline bool CBufferUpdater::update(ID3D11DeviceContext *pDeviceContext, ID3D11Buffer *pBuffer, const DirectX::XMFLOAT4X4 &rData)
{
	using namespace DirectX;
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CBufferUpdater.update(), Map Error");
		return false;
	}
	XMMATRIX *pMappedResource = static_cast<XMMATRIX *>(d3dMappedResource.pData);
	*pMappedResource = XMMatrixTranspose(XMLoadFloat4x4(&rData));
	pDeviceContext->Unmap(pBuffer, 0);
	return true;
}

template<>
inline bool CBufferUpdater::update(ID3D11DeviceContext *pDeviceContext, ID3D11Buffer *pBuffer, const  DirectX::XMFLOAT4X4A &rData)
{
	using namespace DirectX;
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CBufferUpdater.update(), Map Error");
		return false;
	}
	XMMATRIX *pMappedResource = static_cast<XMMATRIX *>(d3dMappedResource.pData);
	*pMappedResource = XMMatrixTranspose(XMLoadFloat4x4A(&rData));
	pDeviceContext->Unmap(pBuffer, 0);
	return true;
}

template<>
inline bool CBufferUpdater::update(ID3D11DeviceContext * pDeviceContext, ID3D11Buffer * pBuffer, const DirectX::XMMATRIX & rData)
{
	using namespace DirectX;
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CBufferUpdater.update(), Map Error");
		return false;
	}
	XMMATRIX *pMappedResource = static_cast<XMMATRIX *>(d3dMappedResource.pData);
	*pMappedResource = XMMatrixTranspose(rData);
	pDeviceContext->Unmap(pBuffer, 0);
	return true;
}





#endif