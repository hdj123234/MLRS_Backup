#pragma once
#ifndef HEADER_DIRECTXDEVICE
#define HEADER_DIRECTXDEVICE

#include"..\Interface_Global.h"
#include"Interface_DirectX11.h"

#ifndef HEADER_GLOBAL
#include"GlobalDirectXDevice.h"
#endif

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#include<DXGI.h>
#include<D3DX11core.h>
#include<D3Dcompiler.h>
#endif

//���漱��
class CDirectXDevice;

class IBuilder_DirectXDevice : public IBuilder<CDirectXDevice> {
public:
	virtual bool build(CDirectXDevice *pTarget) = 0;
	virtual CDirectXDevice * build() = 0;
};


class CDirectXDevice {
private:
	ID3D11Device *m_pDevice;
	ID3D11DeviceContext *m_pDeviceContext;
	IDXGISwapChain *m_pSwapChain;
public:
	CDirectXDevice()
		:m_pDevice(nullptr),
		m_pDeviceContext(nullptr),
		m_pSwapChain(nullptr)
	{
	}
	virtual ~CDirectXDevice() { releaseObject(); }

	bool createObject(IBuilder_DirectXDevice *pBuilder)
	{
		if (!pBuilder)	return false;
		if (!pBuilder->build(this))	return false;
		return true;
	}
	void releaseObject()
	{
		if (m_pDevice)	m_pDevice->Release();
		if (m_pDeviceContext)	m_pDeviceContext->Release();
		if (m_pSwapChain)	m_pSwapChain->Release();
	}

	ID3D11Device		*getDevice()		{return m_pDevice;}
	ID3D11DeviceContext *getDeviceContext()	{return m_pDeviceContext;}
	IDXGISwapChain		*getSwapChain()		{return m_pSwapChain;}

	void setObject(ID3D11Device * pDevice, ID3D11DeviceContext * pDeviceContext, IDXGISwapChain * pSwapChain) 
	{
		m_pDevice = pDevice;
		m_pDeviceContext = pDeviceContext;
		m_pSwapChain = pSwapChain;
	}
};
 

#endif