#include"stdafx.h"
#include "Builder_DirectXDevice.h"
#include "DirectXDevice.h"

#include"..\Module_Platform_Windows\GlobalWindows.h"

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#include<DXGI.h>
#include<D3DX11core.h>
#include<D3Dcompiler.h>
#endif

//----------------------------------- CBuilder_DirectXDevice -------------------------------------------------------------------------------------
bool CBuilder_DirectXDevice::build(CDirectXDevice * pTarget)
{
	ID3D11Device *pDevice=nullptr;
	ID3D11DeviceContext *pDeviceContext = nullptr;
	IDXGISwapChain *pSwapChain = nullptr;

	RECT rect;
	//드라이버 타입 설정(3종류)
	D3D_DRIVER_TYPE ad3dDriverType[3]{ D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE };
	D3D_DRIVER_TYPE d3dDriverType;
	UINT nDriverTypes = 3;
	//피처레벨 설정(3종류)
	D3D_FEATURE_LEVEL aFeatureLevel[3] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_10_0 };
	D3D_FEATURE_LEVEL d3dFeatureLevel = D3D_FEATURE_LEVEL_11_0;
	UINT nFeatureLevels = 3;
	UINT nFlag = 0;
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	HWND hWnd = CGlobalWindow::getHWnd();

#ifdef _DEBUG
	nFlag |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	GetClientRect(hWnd, &rect);
	ZeroMemory(&dxgiSwapChainDesc, sizeof(dxgiSwapChainDesc));
	//SwapChainDesc 초기화
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = rect.right - rect.left;
	dxgiSwapChainDesc.BufferDesc.Height = rect.bottom - rect.top;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = hWnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	//드라이버 타입 및 피처레벨 설정
	for (UINT i = 0; i < nDriverTypes; i++)
	{

		d3dDriverType = ad3dDriverType[i];
		if (SUCCEEDED(D3D11CreateDeviceAndSwapChain(NULL, d3dDriverType, NULL, nFlag, aFeatureLevel, nFeatureLevels, D3D11_SDK_VERSION, &dxgiSwapChainDesc, &pSwapChain, &pDevice, &d3dFeatureLevel, &pDeviceContext)))
			break;
	}
	if (!pDevice || !pDeviceContext || !pSwapChain)
		return false;


	pTarget->setObject(pDevice, pDeviceContext, pSwapChain);
	return true;
}