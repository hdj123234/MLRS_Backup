#include"stdafx.h"
#include "GlobalDirectXDevice.h"

ID3D11Device *			CGlobalDirectXDevice::s_pDevice			=nullptr;
ID3D11DeviceContext *	CGlobalDirectXDevice::s_pDeviceContext	=nullptr;
IDXGISwapChain *		CGlobalDirectXDevice::s_pSwapChain		=nullptr;


void CGlobalDirectXDevice::clear()
{
	if (s_pSwapChain)
		s_pSwapChain->Release();
	s_pSwapChain = nullptr;
	if (s_pDeviceContext)
		s_pDeviceContext->Release();
	s_pDeviceContext = nullptr;
	if (s_pDevice)
		s_pDevice->Release();
	s_pDevice = nullptr;
}
 