#pragma once
#ifndef HEADER_GLOBALDIRECTXDEVICE
#define HEADER_GLOBALDIRECTXDEVICE

#include"..\Interface_Global.h"
#include"Interface_DirectX11.h"

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif

class CGlobalDirectXDevice  {
private:
	static ID3D11Device *s_pDevice;
	static ID3D11DeviceContext *s_pDeviceContext;
	static IDXGISwapChain *s_pSwapChain;

public:
	virtual ~CGlobalDirectXDevice() { clear(); }
	static void setDeviceInstance(ID3D11Device * pDevice, ID3D11DeviceContext * pDeviceContext, IDXGISwapChain * pSwapChain)
	{
		s_pDevice = pDevice;
		s_pDeviceContext = pDeviceContext;
		s_pSwapChain = pSwapChain;
	}
	static ID3D11Device *getDevice() { return s_pDevice; }
	static ID3D11DeviceContext *getDeviceContext() { return s_pDeviceContext; }
	static IDXGISwapChain *getSwapChain() { return s_pSwapChain; }
	static ID3D11DeviceContext *createDeferredContext() 
	{
		ID3D11DeviceContext *pDeferredContext;
		s_pDevice->CreateDeferredContext(0, &pDeferredContext);
		return pDeferredContext;
	}

	static void clear();

};
#endif