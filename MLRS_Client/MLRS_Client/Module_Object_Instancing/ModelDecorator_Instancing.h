#pragma once

#ifndef HEADER_MODELDECORATOR_INSTANCING
#define HEADER_MODELDECORATOR_INSTANCING


#include"Interface_Object_Instancing.h"
#include"AnimController_Instancing.h"
#include"GameObject_Instanced.h"
#include"..\Module_Renderer\RendererState_D3D.h"

#ifndef HEADER_STL
#include<vector>
#endif


//template<class Type_InstanceData>
class IFactory_InstancingModel_Decorator {
public:
	virtual ~IFactory_InstancingModel_Decorator() = 0 {}

	virtual IGameModel * createModelOrigin() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

template<class Type_InstanceData>
class CGameModelDecorator_Instancing :	virtual public IGameModel,
										virtual public RIGameModel_BoundingBoxGetter,
										virtual public RIGameModel_Instancing<Type_InstanceData>{
private:
	IGameModel *m_pGameModel_Origin;
	CAnimData_Instancing *m_pAnimInstanceManager;

	std::vector<RIMesh_Instancing *> m_pMeshs_Instancing;
	std::vector<InstanceDataContainer*> m_pInstanceDataContainers;
	std::vector<RIInstance *> m_pInstances;

	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW;

public:
	CGameModelDecorator_Instancing() : m_pGameModel_Origin(nullptr),
		m_pAnimInstanceManager(nullptr) {}
	virtual ~CGameModelDecorator_Instancing() { CGameModelDecorator_Instancing::releaseObject(); }

private:
	virtual void drawReady(CRendererState_D3D *pRendererState)const {if(m_pAnimInstanceManager) m_pAnimInstanceManager->setOnDX(pRendererState); }
	virtual void resetForDraw(CRendererState_D3D *pRendererState)const {}

public:
	//override IGameObejct
	virtual const FLAG_8 getTypeFlag() const { return m_pGameModel_Origin->getTypeFlag()| s_Flag; }

	//override IGameModel
	virtual const bool isClear()const { return m_pGameModel_Origin->isClear(); }

	//override IObejct_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;

	//override IObejct_UpdateOnDX
	virtual void updateOnDX(CRendererState_D3D *pRendererState)const ;

	//override RIGameModel_BoundingBoxGetter
	virtual DirectX::XMFLOAT3 getCenter() {return dynamic_cast<RIGameModel_BoundingBoxGetter*>(m_pGameModel_Origin)->getCenter();}
	virtual DirectX::XMFLOAT3 getExtent() {return dynamic_cast<RIGameModel_BoundingBoxGetter*>(m_pGameModel_Origin)->getExtent();}

	//override RIGameModel_Instancing<Type_InstanceData>
	virtual void addInstance(RIInstance *pInstance) ;
	virtual void releaseInstance(RIInstance *pInstance) ;

	bool createObject(IFactory_InstancingModel_Decorator *pFactory);
	void releaseObject() { if (m_pGameModel_Origin) delete m_pGameModel_Origin; }

	IGameModel * getModelOrigin() { return m_pGameModel_Origin; }
};
//





//----------------------------------- ������ -------------------------------------------------------------------------------------

//----------------------------------- CGameModelDecorator_Instancing -------------------------------------------------------------------------------------

template<class Type_InstanceData>
inline void CGameModelDecorator_Instancing<Type_InstanceData>::drawOnDX(CRendererState_D3D * pRendererState) const
{ 
	this->drawReady(pRendererState);
	m_pGameModel_Origin->drawOnDX(pRendererState);
	this->resetForDraw(pRendererState);
}

template<class Type_InstanceData>
inline void CGameModelDecorator_Instancing<Type_InstanceData>::updateOnDX(CRendererState_D3D * pRendererState) const
{
	const unsigned int nNumberOfMesh = static_cast<const unsigned int >(m_pInstanceDataContainers.size());
	if (nNumberOfMesh > 1)
	{
		auto iter_Begin = m_pInstanceDataContainers[0]->begin();
		auto iter_End = iter_Begin+ m_pInstances.size();

		for (unsigned int i = 1; i < nNumberOfMesh; ++i)
			std::copy(iter_Begin, iter_End, m_pInstanceDataContainers[i]->begin());
	}
	for (auto pData : m_pMeshs_Instancing)
		pData->updateOnDX(pRendererState);

	if(m_pAnimInstanceManager)
		m_pAnimInstanceManager->updateOnDX(pRendererState);
}

template<class Type_InstanceData>
inline void CGameModelDecorator_Instancing<Type_InstanceData>::addInstance(RIInstance * pInstance)
{
	m_pInstances.push_back(pInstance);

	const unsigned int nInstanceID = static_cast<const unsigned int >(m_pInstances.size()) - 1;

	pInstance->setInstanceData(&(*m_pInstanceDataContainers[0])[nInstanceID]);

	for (auto pMesh : m_pMeshs_Instancing)
		pMesh->addInstance();

	if(m_pAnimInstanceManager)
		if (auto p = dynamic_cast<AGameObject_Instanced_Anim<Type_InstanceData> *>(pInstance))
			p->setAnimController(m_pAnimInstanceManager->getNewInstance());
}

template<class Type_InstanceData>
inline void CGameModelDecorator_Instancing<Type_InstanceData>::releaseInstance(RIInstance * pInstance)
{
	auto iter_Begin = m_pInstances.begin();
	auto iter_End = m_pInstances.end();
	auto iter = std::find(iter_Begin, iter_End, pInstance);
	if (iter == iter_End)	return;

	const unsigned int nInstanceID = static_cast<const unsigned int >(iter - m_pInstances.begin());
	auto changedInstance = m_pInstances.back();
	
	m_pInstances[nInstanceID] = changedInstance;
	m_pInstances.pop_back();
	changedInstance->setInstanceData(&(*m_pInstanceDataContainers[0])[nInstanceID]);

	for (auto pMesh : m_pMeshs_Instancing)
		pMesh->releaseInstance();

	if (m_pAnimInstanceManager)
		if (auto p = dynamic_cast<AGameObject_Instanced_Anim<Type_InstanceData> *>(pInstance))
			m_pAnimInstanceManager->releaseInstance(dynamic_cast<CAnimController_Instance *>(p->getAnimController()));
			//		p->setAnimController(nullptr);
		
}

template<class Type_InstanceData>
inline bool CGameModelDecorator_Instancing<Type_InstanceData>::createObject(IFactory_InstancingModel_Decorator * pFactory)
{
	m_pGameModel_Origin = pFactory->createModelOrigin();
	if (auto pModel_MeshGetter = dynamic_cast<RIGameModel_MeshGetter *>(m_pGameModel_Origin))
	{
		auto pMeshs = pModel_MeshGetter->getMeshs();
		for (auto pMesh : pMeshs)
		{
			if (auto pMesh_Instancing = dynamic_cast<RIMesh_Instancing *>(pMesh))
			{
				m_pMeshs_Instancing.push_back(pMesh_Instancing);
				m_pInstanceDataContainers.push_back(pMesh_Instancing->getInstanceData_Origin());
			}
			else
				return false;
		}
	}
	else
		return false;
	m_pAnimInstanceManager
		= dynamic_cast<CAnimData_Instancing *>(dynamic_cast<CGameModel*>(m_pGameModel_Origin)->getAnimData());
	m_pInstances.reserve(m_pInstanceDataContainers[0]->size());
	return true;
}

#endif
