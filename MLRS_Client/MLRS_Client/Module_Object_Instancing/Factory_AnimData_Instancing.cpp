#include"stdafx.h"
#include"Factory_AnimData_Instancing.h"

#include"..\Module_Object\Action.h"

//#include"..\Module_DXComponent\Type_InDirectX.h"
#include"..\Module_DXComponent\Factory_DXTexture_FromData.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView.h"


#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX

////----------------------------------- CFactory_AnimData_Instancing -------------------------------------------------------------------------------------

CFactory_AnimData_Instancing::CFactory_AnimData_Instancing(
	const std::string & rsName,
	const std::vector<Type_Bone> &rnBoneStructure,
	const std::vector<std::vector<DirectX::XMFLOAT4X4>> &rmFrameData,
	const unsigned int nMaxNumberOfInstance,
	const StartPointList & rList,
	const float fFPS)
	:AFactory_AnimData(rsName, rnBoneStructure, rmFrameData,rList, fFPS),
	m_nMaxNumberOfInstance(nMaxNumberOfInstance),
	m_pTexture2D(nullptr)
{
}

ID3D11Texture2D * CFactory_AnimData_Instancing::createUpdater_Indicator()
{
	if (!m_pTexture2D)
		if (!createTexture2D_RealProcessing())
			return nullptr;

	return m_pTexture2D;
}

ID3D11ShaderResourceView * CFactory_AnimData_Instancing::createTBuffer_AnimIndicator()
{
	if (!m_pTexture2D)
		if (!createTexture2D_RealProcessing())
			return nullptr;

	return CFactory_ShaderResourceView_FromTexture2D(std::string("SRV") + m_sName + std::string("AnimationIndicator"),
		m_pTexture2D,
		DXGI_FORMAT::DXGI_FORMAT_R32_UINT)
		.createShaderResourceView();
}

bool CFactory_AnimData_Instancing::createTexture2D_RealProcessing()
{
	std::vector<Type_AnimIndicatorElement> tmp(AFactory_AnimData::m_nBoneStructure.size() *m_nMaxNumberOfInstance);

	if (m_pTexture2D)	return false;
	m_pTexture2D = CFactory_Texture2D_FromData<Type_AnimIndicatorElement>(
		std::string("TX2D") + m_sName + std::string("AnimInstanceManager"),
		tmp,
		DXGI_FORMAT::DXGI_FORMAT_R32_UINT,
		D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE,
		static_cast<unsigned int >(m_nBoneStructure.size()),
		m_nMaxNumberOfInstance,
		D3D11_USAGE_DYNAMIC,
		D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE)
		.createTexture2D();
	return (m_pTexture2D != nullptr);
}
 


