#pragma once
#ifndef HEADER_INTERFACE_INSTANCING
#define HEADER_INTERFACE_INSTANCING

#include"..\Interface_Global.h"
#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"
#include"..\Module_Object_GameModel\Interface_GameModel.h"
#include"..\Module_Object\Interface_Object.h"

#ifndef HEADER_STL
#include<vector>
#endif

//전방선언
template<class Type_InstanceData>
class RIGameModel_Instancing;


template<class Type_InstanceData>
class RIGameObject_Instancing {
protected:
	typedef Type_InstanceData InstanceData;
	typedef RIGameModel_Instancing<InstanceData> RIGameModel_Instancing;
public:
	virtual ~RIGameObject_Instancing() = 0 {}

	virtual void setInstanceData(InstanceData *pInstanceData_Origin) = 0;	//RIGameModel_Instancing에서 호출
	virtual void enable()=0;
	virtual void disable() = 0;
};

template<class Type_InstanceData>
class RIMesh_InstancingOnDX : virtual public IObject_UpdateOnDX{
protected:
	typedef Type_InstanceData InstanceData;
	typedef std::vector<InstanceData> InstanceDataContainer;
public:
	virtual ~RIMesh_InstancingOnDX() = 0 {}
	
	virtual std::vector<InstanceData> *getInstanceData_Origin()=0;
	virtual void addInstance() = 0;
	virtual void releaseInstance() = 0;
};

class RIGameModel_Instancing_Origin :	virtual public RIGameObject_BeMust_RenewalForDraw,
										virtual public RIGameObject_Drawable{
public:
	virtual ~RIGameModel_Instancing_Origin() = 0 {}
};

template<class Type_InstanceData>
class RIGameModel_Instancing :	virtual public RIGameModel_Instancing_Origin {
protected:
	typedef Type_InstanceData InstanceData;
	typedef std::vector<InstanceData> InstanceDataContainer;
	typedef RIMesh_InstancingOnDX <InstanceData>  RIMesh_Instancing;
	typedef RIGameObject_Instancing<InstanceData> RIInstance;
public:
	virtual ~RIGameModel_Instancing() = 0 {}

	virtual void addInstance(RIInstance *pInstance) = 0;
	virtual void releaseInstance(RIInstance *pInstance) = 0;
};

#endif