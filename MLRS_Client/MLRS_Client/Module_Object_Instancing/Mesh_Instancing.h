#pragma once

#ifndef HEADER_MESH_INSTANCING
#define HEADER_MESH_INSTANCING

#include"Interface_Object_Instancing.h"
#include"..\Module_Object_GameModel\Mesh.h"
#include"..\Module_Renderer\RendererState_D3D.h"

template<class Type_InstanceData>
class IFactory_Mesh_Instancing : virtual public IFactory_Mesh_Vertex{
public:
	virtual ~IFactory_Mesh_Instancing() = 0 {}
	
	virtual const unsigned int getMaxNumberOfInstance() = 0;
	virtual ID3D11Buffer * createInstanceBuffer() = 0;

	static const UINT getStride() { return sizeof(Type_InstanceData); }
};

//----------------------------------- Class -------------------------------------------------------------------------------------
/*	CMesh_Instancing 클래스
실제 인스턴스 관리는 모델에서 실시
*/
template<class Type_InstanceData>
class CMesh_Instancing :  public CMesh_Vertex,
							virtual public RIMesh_InstancingOnDX<Type_InstanceData> {
private:
	typedef std::vector<Type_InstanceData> InstanceDataContainer;
private:
	ID3D11Buffer *m_pInstanceBuffer;

	std::vector<ID3D11Buffer *>m_pVertexBuffers;	//인스턴스 버퍼 포함
	std::vector<UINT> m_nStrides;
	std::vector<UINT> m_nOffsets;

	InstanceDataContainer m_InstanceContainer;
	unsigned int m_nNumberOfInstance;

public:
	CMesh_Instancing()		:	m_pInstanceBuffer(nullptr),
								m_nNumberOfInstance(0){}
	virtual ~CMesh_Instancing() {}

public:
	virtual void setOnDX_InputAssembler(CRendererState_D3D *pRendererState)const;

	//override IObject_DrawOnDX
	virtual void drawOnDX(CRendererState_D3D* pRendererState)const ;

	//override IObject_UpdateOnDX
	virtual void updateOnDX(CRendererState_D3D *pRendererState)const ;

	//override RIMesh_InstancingOnDX<Type_InstanceData>
	virtual std::vector<Type_InstanceData> *getInstanceData_Origin() { return &m_InstanceContainer; }
	virtual void addInstance() {	m_nNumberOfInstance++;}
	virtual void releaseInstance() { m_nNumberOfInstance--; }

	bool createObject(IFactory_Mesh_Instancing<Type_InstanceData> *pFactory);
};


//----------------------------------- 구현부 -------------------------------------------------------------------------------------



template<class Type_InstanceData>
inline void CMesh_Instancing<Type_InstanceData>::setOnDX_InputAssembler(CRendererState_D3D * pRendererState) const
{
	pRendererState->setInputLayout(m_pInputLayout);
	pRendererState->setVertexBuffers(0, m_pVertexBuffers, m_nStrides, m_nOffsets);
	pRendererState->setPrimitive(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

template<class Type_InstanceData>
inline void CMesh_Instancing<Type_InstanceData>::drawOnDX(CRendererState_D3D * pRendererState) const
{
//	if (!pRendererState) return;
	pRendererState->getDeviceContext()->DrawInstanced(m_nNumberOfVertex, m_nNumberOfInstance, 0, 0);
}

template<class Type_InstanceData>
inline void CMesh_Instancing<Type_InstanceData>::updateOnDX(CRendererState_D3D * pRendererState) const
{
	if (!pRendererState)	return;

	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();

	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(m_pInstanceBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CMeshDecorator_Instancing.updateOnDX(), Map Error");
		return;
	}
	Type_InstanceData *pMappedResource = static_cast<Type_InstanceData *>(d3dMappedResource.pData);
	for (unsigned int i = 0; i < m_nNumberOfInstance; ++i)
		pMappedResource[i] = m_InstanceContainer[i];
	pDeviceContext->Unmap(m_pInstanceBuffer, 0);

}

template<class Type_InstanceData>
inline bool CMesh_Instancing<Type_InstanceData>::createObject(IFactory_Mesh_Instancing<Type_InstanceData> * pFactory)
{
	if(!CMesh_Vertex::createObject(pFactory))	return false;
	m_pInstanceBuffer = pFactory->createInstanceBuffer();
	m_InstanceContainer.resize(pFactory->getMaxNumberOfInstance());
	if (!m_pInstanceBuffer)	return false;

	m_pVertexBuffers.clear();
	m_nStrides.clear();
	m_nOffsets.clear();

	m_pVertexBuffers.push_back(CMesh_Vertex::m_pVBuffer);
	m_nStrides.push_back(CMesh_Vertex::m_nStride);
	m_nOffsets.push_back(CMesh_Vertex::m_nOffset);
	
	m_nNumberOfInstance = 0;
	m_pVertexBuffers.push_back(m_pInstanceBuffer);
	m_nStrides.push_back(pFactory->getStride());
	m_nOffsets.push_back(0);

	
	return true;
}

#endif
