#pragma once
#ifndef HEADER_ANIMCOLTROLLER_INSTANCING
#define HEADER_ANIMCOLTROLLER_INSTANCING

#include"..\Module_Object_GameModel\AnimData.h"
#include"..\Module_Object\Action.h"
#include "..\Module_Object\Interface_Object.h"
#include "..\Module_DXComponent\Type_InDirectX.h"

//���漱��
struct ID3D11Texture2D;

//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------

class IFactory_AnimData_Instancing : public IFactory_AnimData{
public:
	virtual ~IFactory_AnimData_Instancing() = 0 {}

	virtual const unsigned int getMaxNumberOfInstance() = 0;
	virtual ID3D11Texture2D * createUpdater_Indicator()=0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------

class CAnimData_Instancing;

class CAnimController_Instance : virtual public IAnimController {
private:
	CAnimData_Instancing * m_pAnimManager;

	unsigned int m_nActionIndex;
	float m_fElapsedTime_Action;

	std::vector<unsigned int> *m_pInstanceData;
public:
	CAnimController_Instance(CAnimData_Instancing * m_pAnimManager);
	virtual ~CAnimController_Instance();

	//override IAnimController
	void setAction(unsigned int nIndex);
	void run(const float fElapsedTime);
	virtual const bool isEndOfAction()const ;

	void setInstanceData(std::vector<unsigned int> *pInstanceData) { m_pInstanceData = pInstanceData; }
	void releaseController() { m_pAnimManager = nullptr; m_pInstanceData = nullptr; }
};


class CAnimData_Instancing: public AAnimData,
							virtual public RIAnimContoller_OnDX{
private:
	typedef AAnimData SUPER;
private:
	std::vector<CAnimController_Instance *> m_pInstances;

	ID3D11Texture2D *m_pTexture2D_Updater_AnimIndicator;
	
	std::vector<std::vector<unsigned int>> m_Indicator;
	unsigned int m_nNumberOfInstance;
public:
	CAnimData_Instancing();
	virtual ~CAnimData_Instancing() { CAnimData_Instancing::releaseObject(); }

	//override  IObject_SetOnDX
	virtual void setOnDX(CRendererState_D3D *pRendererState)const { AAnimData::setOnDX(pRendererState); }

	//override  IObject_UpdateOnDX
	virtual void updateOnDX(CRendererState_D3D *pRendererState)const;

	bool createObject(IFactory_AnimData_Instancing *pFactory);
	void releaseObject();

//	ID3D11ShaderResourceView *getIndicator() { return m_pSRV_AnimIndicator; }
	const std::vector<CAction>& getActions() const { return AAnimData::getActions(); }

	CAnimController_Instance* getNewInstance();
	void releaseInstance(CAnimController_Instance* const pInstance);
};


#endif