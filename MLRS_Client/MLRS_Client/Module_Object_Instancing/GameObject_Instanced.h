#pragma once

#ifndef HEADER_GAMEOBJECT_MODELINSTANCE
#define HEADER_GAMEOBJECT_MODELINSTANCE

#include"Interface_Object_Instancing.h"




//----------------------------------- Creator -------------------------------------------------------------------------------------
class IFactory_GameObject_Instanced {
public:
	virtual ~IFactory_GameObject_Instanced()=0{}

	virtual IGameModel * getModel() = 0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------


template<class Type_InstanceData>
class AGameObject_Instanced :	virtual public IGameObject,
								virtual public RIGameObject_Instancing<Type_InstanceData>{
protected:
	IGameModel *m_pModel;

private:
	RIGameModel_Instancing *m_pModel_Instancing;

protected:
	bool m_bEnable;
	Type_InstanceData * m_pInstanceData; 

public:
	AGameObject_Instanced():m_pModel(nullptr),
								m_pInstanceData(nullptr),
								m_bEnable(true){	}
	virtual ~AGameObject_Instanced() = 0 { }

	//override RIGameObject_Instancing<Type_InstanceData>
	virtual void setInstanceData(Type_InstanceData *pInstanceData_Origin) {		m_pInstanceData = pInstanceData_Origin; }	//RIGameModel_Instancing에서 호출
	virtual void enable() ;
	virtual void disable();

	bool createObject(IFactory_GameObject_Instanced *pFactory);
	void releaseObject();

	void releaseInstanceOrigin();
};

template<class Type_InstanceData>
class AGameObject_Instanced_Anim : public AGameObject_Instanced<Type_InstanceData>,
										virtual public RIGameObject_Animate{
private:
	typedef AGameObject_Instanced<Type_InstanceData> SUPER;

protected:
	IAnimController *m_pAnimContoller;

public:
	AGameObject_Instanced_Anim() : SUPER(),
		m_pAnimContoller(nullptr) {	}
	virtual ~AGameObject_Instanced_Anim() = 0 { }

protected:

public:
	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);
	void setAction(unsigned int n){	if(m_bEnable) m_pAnimContoller->setAction(n);}

	const bool isAnimEnd()const { return m_pAnimContoller->isEndOfAction(); }

	void setAnimController(IAnimController *pAnimContoller) { m_pAnimContoller = pAnimContoller; }
	IAnimController *getAnimController() { return m_pAnimContoller; }
	//	IAnimController * getAnimController() {	return	m_pAnimContoller;	}


};


//----------------------------------- 구현부 -------------------------------------------------------------------------------------

//----------------------------------- AGameObject_Instanced -------------------------------------------------------------------------------------

template<class Type_InstanceData>
inline void AGameObject_Instanced<Type_InstanceData>::releaseInstanceOrigin()
{
	m_pModel_Instancing = nullptr;
	m_pInstanceData = nullptr;
}

template<class Type_InstanceData>
inline void AGameObject_Instanced<Type_InstanceData>::enable()
{
	if(!m_bEnable)
	{
		m_bEnable = true;
		m_pModel_Instancing->addInstance(this);
	}
}

template<class Type_InstanceData>
inline void AGameObject_Instanced<Type_InstanceData>::disable()
{
	if (m_bEnable)
	{
		m_pModel_Instancing->releaseInstance(this);
		m_bEnable = false;
	}
}

template<class Type_InstanceData>
inline bool AGameObject_Instanced<Type_InstanceData>::createObject(IFactory_GameObject_Instanced * pFactory)
{
	m_pModel = pFactory->getModel();
	if (m_pModel_Instancing = dynamic_cast<RIGameModel_Instancing *>(m_pModel))
		m_pModel_Instancing->addInstance(this);
	else
		return false;
	return true;
}

template<class Type_InstanceData>
inline void AGameObject_Instanced<Type_InstanceData>::releaseObject()
{
	if (m_pModel_Instancing) 
		m_pModel_Instancing->releaseInstance(this);
}

template<class Type_InstanceData>
inline std::vector<IMsg *> AGameObject_Instanced_Anim<Type_InstanceData>::animateObject(const float fElapsedTime)
{
	if (m_bEnable)
		m_pAnimContoller->run(fElapsedTime);

	return std::vector<IMsg *>();
}


#endif
