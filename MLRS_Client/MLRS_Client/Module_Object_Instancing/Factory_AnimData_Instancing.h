#pragma once
#ifndef HEADER_FACTORY_ANIMINSTANCEMANAGER
#define HEADER_FACTORY_ANIMINSTANCEMANAGER

#include "AnimController_Instancing.h"
#include "..\Module_Object_GameModel\Factory_AnimData.h"


#ifndef HEADER_STL
#include<map>
#endif // !HEADER_STL


class CFactory_AnimData_Instancing : virtual public IFactory_AnimData_Instancing,
									public AFactory_AnimData{
private:
	const unsigned int m_nMaxNumberOfInstance;
	ID3D11Texture2D *m_pTexture2D;

public:
	CFactory_AnimData_Instancing(const std::string &rsName,
		const std::vector<Type_Bone> &rnBoneStructure,
		const std::vector<std::vector<DirectX::XMFLOAT4X4>> &rmFrameData,
		const unsigned int nMaxNumberOfInstance,
		const StartPointList &rList,
		const float fFPS = 30);
	virtual ~CFactory_AnimData_Instancing() {}

private:
	bool createTexture2D_RealProcessing();

public:
	//override IFactory_AnimData
	virtual const unsigned int getMaxNumberOfInstance() { return m_nMaxNumberOfInstance; }
	virtual ID3D11Texture2D * createUpdater_Indicator() ;

	//override IFactory_AnimData
	virtual ID3D11ShaderResourceView * createTBuffer_AnimIndicator();
	virtual ID3D11ShaderResourceView * createTBuffer_BoneStructure() { return AFactory_AnimData::createTBuffer_BoneStructure(); }
	virtual ID3D11ShaderResourceView *createTexture_FrameData() { return AFactory_AnimData::createTexture_FrameData(); }
	virtual std::vector<CAction> createActions() { return AFactory_AnimData::createActions(); }
	virtual const unsigned int getNumberOfBone() { return AFactory_AnimData::getNumberOfBone(); }


};

#endif