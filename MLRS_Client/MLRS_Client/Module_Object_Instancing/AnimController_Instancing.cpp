#include"stdafx.h"
#include"AnimController_Instancing.h"

#include"..\Module_Object\Action.h"
#include"..\Module_Renderer\RendererState_D3D.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX

////----------------------------------- CAnimController_Instance -------------------------------------------------------------------------------------
//
CAnimController_Instance::CAnimController_Instance( CAnimData_Instancing * const m_pAnimManager)
	:m_pAnimManager(m_pAnimManager),
	m_nActionIndex(0),
	m_fElapsedTime_Action(0),
	m_pInstanceData(nullptr)
{
}

CAnimController_Instance::~CAnimController_Instance()
{
}

void CAnimController_Instance::setAction(unsigned int nIndex)
{
	if ((m_pAnimManager->getActions()).size() <= nIndex)	return;
	m_fElapsedTime_Action = 0;
	m_nActionIndex = nIndex;

	unsigned int nFrame = (m_pAnimManager->getActions())[m_nActionIndex].getFrame(0);
	for (auto &data : *m_pInstanceData)
		data = nFrame;
}

void CAnimController_Instance::run(const float fElapsedTime)
{
	m_fElapsedTime_Action += fElapsedTime;

	
	int nFrame = (m_pAnimManager->getActions())[m_nActionIndex].getFrame(m_fElapsedTime_Action);
	for (auto &data : *m_pInstanceData)
		data = nFrame;

	const CAction &rAction = (m_pAnimManager->getActions())[m_nActionIndex];
	if (rAction.isEnd(m_fElapsedTime_Action))
	{
		switch (rAction.getType()) {
		//case EActionType::eActionType_Default:
		//	m_nActionIndex = 0;
		//	m_fElapsedTime_Action = 0;
		//	break;
		case EActionType::eActionType_Repeat:
			m_fElapsedTime_Action -= rAction.getActionTime();
			break;
		case EActionType::eActionType_Keep:
			m_fElapsedTime_Action = rAction.getActionTime();
			break;
		}
	}
	
}

const bool CAnimController_Instance::isEndOfAction() const
{
	const CAction &rAction = (m_pAnimManager->getActions())[m_nActionIndex];

	return(rAction.getType()== EActionType::eActionType_Default&& rAction.isEnd(m_fElapsedTime_Action));
}



CAnimData_Instancing::CAnimData_Instancing()
	:	m_nNumberOfInstance(0),
	m_pTexture2D_Updater_AnimIndicator(nullptr)
{
}

void CAnimData_Instancing::updateOnDX(CRendererState_D3D * pRendererState) const
{
	if (!pRendererState)	return;
	
	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();
	
	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(m_pTexture2D_Updater_AnimIndicator, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CAnimController.updateOnDX(), Map Error");
		return;
	}
	Type_AnimIndicatorElement (*pMappedResource) = static_cast<Type_AnimIndicatorElement *>(d3dMappedResource.pData);
	unsigned int nWidth = d3dMappedResource.RowPitch / sizeof(unsigned int);
	for (unsigned int i = 0; i < m_nNumberOfInstance; ++i)
	{
		for (unsigned int j = 0; j < m_nNumberOfBone; ++j)
		{
			pMappedResource[i*nWidth +j] = m_Indicator[i][j];
		}
	}
	pDeviceContext->Unmap(m_pTexture2D_Updater_AnimIndicator, 0);
}

bool CAnimData_Instancing::createObject(IFactory_AnimData_Instancing * pFactory)
{
	if(!SUPER::createObject(pFactory))	return false;
	m_pTexture2D_Updater_AnimIndicator = pFactory->createUpdater_Indicator();
	unsigned int nMaxNumberOfInstance = pFactory->getMaxNumberOfInstance();
	
	m_Indicator.resize(nMaxNumberOfInstance);
	for (auto &rData : m_Indicator)
		rData.resize(m_nNumberOfBone);
	
	m_pInstances.resize(nMaxNumberOfInstance);
	for (unsigned int i = 0; i < nMaxNumberOfInstance; ++i)
	{
		m_pInstances[i] = new CAnimController_Instance(this);
		m_pInstances[i]->setInstanceData(&m_Indicator[i]);
	}

	return true;
}

void CAnimData_Instancing::releaseObject()
{
	for (auto data : m_pInstances)
		delete data;
}

CAnimController_Instance * CAnimData_Instancing::getNewInstance()
{
	CAnimController_Instance* pInstance = m_pInstances[m_nNumberOfInstance];

	m_nNumberOfInstance++;
	return pInstance;
}

void CAnimData_Instancing::releaseInstance(CAnimController_Instance * const pInstance)
{
	auto iter_Begin = m_pInstances.begin();
	auto iter_End = m_pInstances.end();
	auto iter = std::find(iter_Begin, iter_End, pInstance);
	if (iter == iter_End)	return;

	m_nNumberOfInstance--;
	unsigned int nInstanceID = static_cast<unsigned int>(iter - iter_Begin);
	auto tmp = m_pInstances[nInstanceID];
	m_pInstances[nInstanceID] = m_pInstances[m_nNumberOfInstance];
	m_pInstances[m_nNumberOfInstance] = tmp;

	m_pInstances[nInstanceID]->setInstanceData(&m_Indicator[nInstanceID]);
	m_pInstances[m_nNumberOfInstance]->setInstanceData(&m_Indicator[m_nNumberOfInstance]);
}
