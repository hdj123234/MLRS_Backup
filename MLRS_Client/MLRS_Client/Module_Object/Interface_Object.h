
#ifndef HEADER_INTERFACE_OBJECT
#define HEADER_INTERFACE_OBJECT

#include"..\Interface_Global.h"
#include"..\Module_Platform_DirectX11\Interface_DirectX11.h"

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL
#ifndef HEADER_DIRECTXMATH
#include<DirectXCollision.h>
#endif // !HEADER_STL


static const FLAG_8 FLAG_OBJECTTYPE_ANIMATE					= 0x01;
static const FLAG_8 FLAG_OBJECTTYPE_DRAWALBE					= 0x02;
static const FLAG_8 FLAG_OBJECTTYPE_DRAWALBE_CLEAR			= 0x04;
static const FLAG_8 FLAG_OBJECTTYPE_BEMUST_INIT_FOR_DRAW		= 0x08;
static const FLAG_8 FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW	= 0x10;
static const FLAG_8 FLAG_OBJECTTYPE_MOVABLE					= 0x20;


namespace DirectX {
	struct XMFLOAT3A;
}
class CRendererState_D3D;

//enum class EMsgType_MLRS : ENUMTYPE_256; 

enum class EGameObjType : ENUMTYPE_256 {
	ePlayer,
	eAllied,
	eEnemy,
	eNeutral,
//	eETC,
};

class IMsg_GameObject : public IMsg {
public:
	virtual ~IMsg_GameObject() = 0 {}
};

enum EMsgType_Movement : ENUMTYPE_256 ; 

class IAdapter_Msg_ToGame : virtual public IAdapter<IMsg_GameObject, IMsg> {
public:
	virtual ~IAdapter_Msg_ToGame() = 0 {}
};

class IMsg_Movement  : virtual public IMsg_GameObject {
public:
	virtual const EMsgType_Movement getMovementType() const = 0;
	virtual ~IMsg_Movement() = 0 {}
}; 

class IGameObject {
public:
	virtual ~IGameObject() = 0 {}

	virtual const FLAG_8 getTypeFlag() const = 0;
};

class RIGameObject_TypeGetter {
public:
	virtual ~RIGameObject_TypeGetter() = 0 {}

	virtual const EGameObjType getObjType()const = 0;
};

class RIGameObject_IDGetter {
public:
	virtual ~RIGameObject_IDGetter() = 0 {}

	virtual const unsigned int getID()const = 0;
};

class RIGameObject_OnOff {
public:
	virtual ~RIGameObject_OnOff() = 0 {}

	virtual void enable() = 0;
	virtual void disable() = 0;
};


class RIGameObject_HandleMsg {
public:
	virtual ~RIGameObject_HandleMsg() = 0 {}

	virtual void handleMsg(IMsg_GameObject * ) = 0;
};

class RIGameObject_Animate {
public:
	virtual ~RIGameObject_Animate() = 0 {}

	virtual std::vector<IMsg *> animateObject(const float fElapsedTime) = 0;
};

class RIGameObject_Drawable  : virtual public IObject_DrawOnDX {
public:
	virtual ~RIGameObject_Drawable() = 0 {}

	virtual void drawOnDX(CRendererState_D3D *pRendererState)const = 0;
	virtual const  bool isClear() const = 0;
};

class RIGameObject_BeMust_RenewalForDraw : virtual public IObject_UpdateOnDX {
public:
	virtual ~RIGameObject_BeMust_RenewalForDraw() = 0 {}
};

class RIGameObject_BeMust_InitForDraw : virtual public IObject_InitializeOnDX {
public:
	virtual ~RIGameObject_BeMust_InitForDraw() = 0 {}
};

class RIGameObject_Movable {
public:
	virtual ~RIGameObject_Movable() = 0 {}

	virtual void move(const IMsg_Movement *pMoveMsg) = 0;
};

class RIGameObject_ModelOrigin {
public:
	virtual ~RIGameObject_ModelOrigin() = 0 {}

	virtual void releaseModel() = 0;
};

class RIGameObject_PositionGetter {
public:
	virtual ~RIGameObject_PositionGetter() = 0 {}

	virtual DirectX::XMFLOAT3A* getPositionOrigin()const =0;
	virtual const DirectX::XMFLOAT3 getPosition()const = 0;
};

class RIGameObject_DirectionGetter {
public:
	virtual ~RIGameObject_DirectionGetter() = 0 {}

	virtual DirectX::XMFLOAT3A* getDirectionOrigin() const = 0;
	virtual const DirectX::XMFLOAT3 getDirection() const= 0;
};


class RIGameObject_LocalVectorGetter {
public:
	virtual ~RIGameObject_LocalVectorGetter() = 0 {}

	virtual DirectX::XMFLOAT3A* getRightOrigin()const  = 0;
	virtual DirectX::XMFLOAT3A* getUpOrigin()   const = 0;
	virtual DirectX::XMFLOAT3A* getLookOrigin() const = 0;
};

template<class SprayPointType>
class RIGameObject_SprayParticle {
public:
	virtual ~RIGameObject_SprayParticle() = 0 {}

	virtual std::vector<SprayPointType> getSprayPoints()const = 0;
};

//class RIGameObject_StateGetter {
//public:
//	virtual ~RIGameObject_StateGetter() = 0 {}
//
//	virtual const ENUMTYPE_256 getState() const = 0;
//	virtual void setState(const ENUMTYPE_256 eState) = 0;
//};

enum ECollisionObjectType : ENUMTYPE_256;

class IBoundingObject {
public:
	virtual ~IBoundingObject() = 0 {}

public:
	virtual const bool checkCollision(const IBoundingObject * pOther)const = 0;
	virtual const ECollisionObjectType getCollisionObjectType()const = 0;
	virtual const DirectX::XMFLOAT3 getCenter()const = 0;

//	static const bool checkCollision(IBoundingObject * p1, IBoundingObject * p2) { return p1->checkCollision(p2); }
};

class RIGameObject_Collision {
public:
	virtual ~RIGameObject_Collision() = 0 {}

	virtual const IBoundingObject * getBoundingObject()const = 0;
};

class IAnimController {
public:
	virtual ~IAnimController() = 0 {}

	virtual void setAction(unsigned int nIndex)=0;
	virtual void run(const float fElapsedTime) =0;
	virtual const bool isEndOfAction()const = 0;
};

class RIAnimContoller_OnDX :	virtual public IObject_UpdateOnDX {
public:
	virtual ~RIAnimContoller_OnDX() = 0 {}

};

//���漱��
class CBoundingObject_Ray;

class ISpace {
public:
	virtual ~ISpace() = 0 {}

	virtual void addObject(RIGameObject_Collision *pCollision)		=0;
	virtual void releaseObject(RIGameObject_Collision *pCollision)	=0;

	virtual const bool pick(const CBoundingObject_Ray & rBoundingRay, DirectX::XMFLOAT3 &out_rCollisionPoint)const=0;

	//DEBUG
	virtual const std::set<RIGameObject_Collision *>& getObjects() = 0;
};


#endif