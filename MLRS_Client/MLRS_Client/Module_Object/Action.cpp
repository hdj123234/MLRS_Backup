#include"stdafx.h"
#include"Action.h"


//----------------------------------- CAction -------------------------------------------------------------------------------------

float CAction::s_fFPS_Default = 30.0f;

CAction::CAction(
	const unsigned int nStartFrame,
	const unsigned int nEndFrame,
	const EActionType eType,
	const float fFPS)
	:m_nStartFrame(nStartFrame),
	m_nFrameRange(nEndFrame - nStartFrame),
	m_fFPS(fFPS),
	m_eType(eType)
{
}
