#include"stdafx.h"
#include"AnimController.h"

#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_Object_GameModel\AnimData.h"

#ifndef HEADER_MYLOGCAT
#include"..\LogCat.h"
#endif // !HEADER_DIRECTX




//----------------------------------- CAnimController -------------------------------------------------------------------------------------

CAnimController::CAnimController()
	:m_nActionIndex(0),
	m_fElapsedTime_Action(0),
	m_pBuffer_Updater_AnimIndicator(nullptr)
{
}

void CAnimController::updateOnDX(CRendererState_D3D * pRendererState) const
{
	if (!pRendererState)	return;

	ID3D11DeviceContext *pDeviceContext = pRendererState->getDeviceContext();

	D3D11_MAPPED_SUBRESOURCE d3dMappedResource;
	if (FAILED(pDeviceContext->Map(m_pBuffer_Updater_AnimIndicator, 0, D3D11_MAP_WRITE_DISCARD, 0, &d3dMappedResource)))
	{
		MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CAnimController.updateOnDX(), Map Error");
		return;
	}
	Type_AnimIndicatorElement *pMappedResource = static_cast<Type_AnimIndicatorElement *>(d3dMappedResource.pData);
	for (unsigned int i = 0; i < m_Indicator.size(); ++i)
		pMappedResource[i] = m_Indicator[i];
	pDeviceContext->Unmap(m_pBuffer_Updater_AnimIndicator, 0);

}

void CAnimController::setAction(unsigned int nIndex)
{
	if (m_pAnimData->getActions().size() <= nIndex)	return;
	m_fElapsedTime_Action = 0;
	m_nActionIndex = nIndex;

	unsigned int nFrame = m_pAnimData->getActions()[m_nActionIndex].getFrame(0);
	for (auto &data : m_Indicator)
		data = nFrame;
}

void CAnimController::run(const float fElapsedTime)
{
	m_fElapsedTime_Action += fElapsedTime;
	
	int nFrame = m_pAnimData->getActions()[m_nActionIndex].getFrame(m_fElapsedTime_Action);
	for (auto &data : m_Indicator)
		data = nFrame;

	if (m_pAnimData->getActions()[m_nActionIndex].isEnd(m_fElapsedTime_Action))
	{
		switch (m_pAnimData->getActions()[m_nActionIndex].getType()) {
		case EActionType::eActionType_Repeat:
			m_fElapsedTime_Action -= m_pAnimData->getActions()[m_nActionIndex].getActionTime();
			break;
		}
	}
}

const bool CAnimController::isEndOfAction() const
{  
	return((m_pAnimData->getActions())[m_nActionIndex].isEnd(m_fElapsedTime_Action));
}

bool CAnimController::createObject(IFactory_AnimController * pFactory)
{
	if (!pFactory)	return false;
	m_pAnimData = pFactory->getAnimData();
	m_pBuffer_Updater_AnimIndicator = pFactory->getBuffer_Updater_AnimIndicator();

	m_Indicator.resize(m_pAnimData->getNumberOfBone()) ;

	return (m_pAnimData&& m_pBuffer_Updater_AnimIndicator);
}
