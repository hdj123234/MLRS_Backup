#pragma once
#ifndef HEADER_ANIMCOLTROLLER
#define HEADER_ANIMCOLTROLLER

#include "Interface_Object.h"
#include"Action.h"
#include "..\Module_DXComponent\Type_InDirectX.h"

//���漱��
struct ID3D11Buffer;
class CAnimData_NoInstancing;

//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------

class IFactory_AnimController {
public:
	virtual ~IFactory_AnimController() = 0 {}

	virtual const CAnimData_NoInstancing * getAnimData()=0;
	virtual ID3D11Buffer * getBuffer_Updater_AnimIndicator() = 0;
};


//----------------------------------- Class -------------------------------------------------------------------------------------

class CAnimController : virtual public IAnimController,
							 virtual public RIAnimContoller_OnDX{
private:
	const CAnimData_NoInstancing * m_pAnimData;
	std::vector<unsigned int> m_Indicator;
	unsigned int m_nActionIndex;
	float m_fElapsedTime_Action;
	ID3D11Buffer *m_pBuffer_Updater_AnimIndicator;

public:
	CAnimController();
	virtual ~CAnimController() {}

	//override  IObject_UpdateOnDX
	virtual void updateOnDX(CRendererState_D3D *pRendererState)const;

	//override IAnimController
	void setAction(unsigned int nIndex);
	void run(const float fElapsedTime);
	virtual const bool isEndOfAction()const ;

	bool createObject(IFactory_AnimController *pFactory);

//	ID3D11ShaderResourceView *getIndicator() { return m_pTBuffer_AnimIndicator; }
};

#endif