#pragma once

#ifndef HEADER_GAMEOBJECT_MODELBASE
#define HEADER_GAMEOBJECT_MODELBASE

#include"Interface_Object.h"
#include"..\Module_Object_GameModel\Interface_GameModel.h"



//----------------------------------- Creator -------------------------------------------------------------------------------------
class IFactory_GameObject_ModelBase {
public:
	virtual ~IFactory_GameObject_ModelBase()=0{}

	virtual IGameModel * getModel() = 0;
};
class IFactory_GameObject_ModelBase_Anim : virtual public IFactory_GameObject_ModelBase{
public:
	virtual ~IFactory_GameObject_ModelBase_Anim() = 0 {}

	virtual	IAnimController * createAnimController()=0;
};

//----------------------------------- Class -------------------------------------------------------------------------------------


class AGameObject_ModelBase :	virtual public IGameObject,
								virtual public RIGameObject_Drawable {
protected:
	IGameModel *m_pModel;

private:
	virtual void drawReady(CRendererState_D3D *pRendererState)const {}
	virtual void resetForDraw(CRendererState_D3D *pRendererState)const {}

public:
	AGameObject_ModelBase(IGameModel *pModel = nullptr)
		:m_pModel(pModel)	{	}
	virtual ~AGameObject_ModelBase() = 0 { }

	//override RIGameObject_Drawable
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;
	virtual const bool isClear() const { return m_pModel->isClear(); }

	bool createObject(IFactory_GameObject_ModelBase *pFactory);

//	void setModel(IGameModel *pModel) { m_pModel = pModel; }
};

class AGameObject_ModelBase_Anim : public AGameObject_ModelBase,
										virtual public RIGameObject_Animate{
protected:
	IAnimController *m_pAnimContoller;
private:
	RIAnimContoller_OnDX *m_pAnimContoller_OnDX;

public:
	AGameObject_ModelBase_Anim():	m_pAnimContoller(nullptr),
										m_pAnimContoller_OnDX(nullptr)
	{	}
	virtual ~AGameObject_ModelBase_Anim() { AGameObject_ModelBase_Anim::releaseObject(); }


protected:
	virtual void drawReady(CRendererState_D3D *pRendererState)const; 
	IAnimController *getAnimController() { return m_pAnimContoller; }

public:
	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);
	void setAction(unsigned int n){	m_pAnimContoller->setAction(n);}

	const bool isAnimEnd()const { return m_pAnimContoller->isEndOfAction(); }

	bool createObject(IFactory_GameObject_ModelBase_Anim *pFactory);
	void releaseObject();

//	void setAnimController(IAnimController *pAnimContoller);
//	IAnimController * getAnimController() {	return	m_pAnimContoller;	}
};

class AGameObject_ModelOrigin : public AGameObject_ModelBase,
								virtual public RIGameObject_ModelOrigin {
public:
	virtual ~AGameObject_ModelOrigin() { AGameObject_ModelOrigin::releaseModel(); }

	virtual void releaseModel() { if (m_pModel) delete m_pModel; }
};

class AGameObject_ModelOrigin_Anim : public AGameObject_ModelBase_Anim,
											virtual public RIGameObject_ModelOrigin {
public:
	virtual ~AGameObject_ModelOrigin_Anim() { AGameObject_ModelOrigin_Anim::releaseModel(); }

	virtual void releaseModel() { if (m_pModel) delete m_pModel; }
};



//----------------------------------- ������ -------------------------------------------------------------------------------------

//----------------------------------- AGameObject_ModelBase -------------------------------------------------------------------------------------

inline void AGameObject_ModelBase::drawOnDX(CRendererState_D3D *pRendererState)const
{
	if (!m_pModel)	return;
	this->drawReady(pRendererState);
	m_pModel->drawOnDX(pRendererState);
	this->resetForDraw(pRendererState);
}

inline bool AGameObject_ModelBase::createObject(IFactory_GameObject_ModelBase *pFactory)
{
	if (!pFactory)	return false;
	m_pModel = pFactory->getModel();
	return (m_pModel != nullptr);
}

//----------------------------------- AGameObject_ModelBase_Anim -------------------------------------------------------------------------------------


inline void AGameObject_ModelBase_Anim::drawReady(CRendererState_D3D *pRendererState)const
{
	if (!m_pAnimContoller_OnDX)	return;

	m_pAnimContoller_OnDX->updateOnDX(pRendererState);
}
inline std::vector<IMsg *> AGameObject_ModelBase_Anim::animateObject(const float fElapsedTime)
{
	if (m_pAnimContoller)	
		m_pAnimContoller->run(fElapsedTime);

	return std::vector<IMsg *>();
}

inline bool AGameObject_ModelBase_Anim::createObject(IFactory_GameObject_ModelBase_Anim *pFactory)
{
	if (!pFactory)return false;
	m_pAnimContoller = pFactory->createAnimController();
	if (auto p = dynamic_cast<RIAnimContoller_OnDX *>(m_pAnimContoller))
		m_pAnimContoller_OnDX = p;
//	if(!(m_pAnimContoller &&m_pAnimContoller_OnDX))	return false;

	return AGameObject_ModelBase::createObject(pFactory);
}

inline void AGameObject_ModelBase_Anim::releaseObject() 
{
	if (m_pAnimContoller)
		delete m_pAnimContoller;
}
//inline void AGameObject_ModelBase_Anim::setAnimController(IAnimController *pAnimContoller)
//{
//	if (m_pAnimContoller_OnDX = dynamic_cast<RIAnimContoller_OnDX *>(pAnimContoller))
//		m_pAnimContoller = pAnimContoller;
//}

#endif