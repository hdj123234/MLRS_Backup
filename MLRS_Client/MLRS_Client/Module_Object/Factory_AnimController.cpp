#include "stdafx.h"
#include "Factory_AnimController.h"


#include "..\Module_Object_GameModel\AnimData.h"
#include "..\Module_Object_GameModel\GameModel.h"

CFactory_AnimController::CFactory_AnimController(IGameModel * pModel)
	:m_pAnimData(dynamic_cast<CAnimData_NoInstancing *>(dynamic_cast<CGameModel * >(pModel)->getAnimData()))
{
}

ID3D11Buffer * CFactory_AnimController::getBuffer_Updater_AnimIndicator()
{ 
	if (!m_pAnimData)	return nullptr;
	else return m_pAnimData->getUpdater_AnimIndicator(); 
}
