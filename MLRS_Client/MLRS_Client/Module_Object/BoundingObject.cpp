#include"stdafx.h"
#include"BoundingObject.h"

#ifndef HEADER_STL
#include<array>
#endif // !HEADER_STL
using namespace DirectX;


BoundingRay::BoundingRay(
	const DirectX::XMFLOAT3 &rvStartPos,
	const DirectX::XMFLOAT3 &rvDir,
	const float m_fMaxLength)
	:m_vStartPos(rvStartPos),
	m_vDir(rvDir),
	m_fMaxLength(m_fMaxLength)
{
}

BoundingFace::BoundingFace(
	const std::vector<DirectX::XMFLOAT3>& rvVertices,
	const DirectX::XMFLOAT4 & rvPlane,
	bool bFrontFaceCheck)
	:m_vVertices(rvVertices),
	m_vPlane(rvPlane),
	m_bFrontFaceCheck(bFrontFaceCheck)
{
}

BoundingFace::BoundingFace(const std::vector<DirectX::XMFLOAT3>& rvVertices,
	bool bFrontFaceCheck)
	:m_vVertices(rvVertices),
	m_bFrontFaceCheck(bFrontFaceCheck)
{
	XMStoreFloat4(&m_vPlane, XMPlaneFromPoints(XMLoadFloat3(&rvVertices[0]), XMLoadFloat3(&rvVertices[1]), XMLoadFloat3(&rvVertices[2])));

}

bool CGlobalCollisionCalculator::checkCollision(const DirectX::BoundingOrientedBox & rOOBB, const BoundingRay & rRay)
{
	float fDistance;
	bool bRetval =   rOOBB.Intersects(XMLoadFloat3(&rRay.m_vStartPos), XMVector3Normalize(XMLoadFloat3(&rRay.m_vDir)), fDistance);
	return bRetval && (rRay.m_fMaxLength > fDistance);
}

bool CGlobalCollisionCalculator::getIntersection(const BoundingRay & rRay, const BoundingFace & rFace, DirectX::XMFLOAT3 & out_rIntersection)
{


	XMVECTOR vPlane = XMLoadFloat4(&rFace.m_vPlane);
	XMVECTOR vDir = XMLoadFloat3(&rRay.m_vDir);
	if (rFace.m_bFrontFaceCheck)
	{
		if (XMVectorGetX(XMVector3Dot(-vDir, vPlane)) < 0)
			return false;
	}
	XMVECTOR vLinePoint1 = XMLoadFloat3(&rRay.m_vStartPos);
	XMVECTOR vLinePoint2 = vLinePoint1 + rRay.m_fMaxLength * vDir;
	DirectX::XMFLOAT3 bNan;
	XMVECTOR vIntersectionPoint = XMPlaneIntersectLine(vPlane, vLinePoint1, vLinePoint2);
	XMStoreFloat3(&bNan, XMVectorIsNaN(vIntersectionPoint));

	if (reinterpret_cast<int&>(bNan.x)|| reinterpret_cast<int&>(bNan.y)|| reinterpret_cast<int&>(bNan.z))
		return false;
	float fDegree = 0;
	auto nNumberOfVertex = rFace.m_vVertices.size();
	XMVECTOR v0;
	XMVECTOR v1 = v0= XMLoadFloat3(&rFace.m_vVertices[0]) - vIntersectionPoint;
	XMVECTOR v2;
	XMVECTOR *pv1 = &v1;
	XMVECTOR *pv2 = &v2;
	XMVECTOR *pvTmp;
	for (auto i=1;i<nNumberOfVertex;++i)
	{
		*pv2 = XMLoadFloat3(&rFace.m_vVertices[i]) - vIntersectionPoint;
		fDegree += XMVectorGetX(XMVector3AngleBetweenVectors(v1 , v2 ));
		pvTmp = pv1;		pv1 = pv2;		pv2 = pvTmp;
	}
	v1 = XMLoadFloat3(&rFace.m_vVertices.back()) - vIntersectionPoint;
	fDegree += XMVectorGetX(XMVector3AngleBetweenVectors(v0, v1));

	fDegree -= (g_XMPi.f[0] * 2); 
	auto tmp = (*reinterpret_cast<int32_t *>(&fDegree))&g_XMAbsMask.i[0];
	fDegree = *reinterpret_cast<float*>(&tmp);

	if (fDegree <= 0.00000086)
	{
		XMStoreFloat3(&out_rIntersection, vIntersectionPoint);
		return true;
	}
	else
		return false;


}

//bool CBoundingObject_Sphere::checkCollision(IBoundingObject * pOther)
//{
//	CGlobalCollisionCalculator::checkCollision(m_)
//	return false;
//}


CBoundingObject_OOBB::CBoundingObject_OOBB()
	:m_vCenter(0,0,0),
	m_OOBB(XMFLOAT3(0,0,0), XMFLOAT3(1, 1, 1), XMFLOAT4(0, 0, 0,1))
{
}

const bool CBoundingObject_OOBB::checkCollision(const IBoundingObject * pOther)const
{
	switch (pOther->getCollisionObjectType()) {
	case ECollisionObjectType::eRay:
		return CGlobalCollisionCalculator::checkCollision(m_OOBB, static_cast<const CBoundingObject_Ray*>(pOther)->getRay());

	}
	return false;
}

void CBoundingObject_OOBB::setOOBB(const DirectX::XMFLOAT4X4 & rWorldMatrix)
{
	XMFLOAT4 vQuartenion;
	XMStoreFloat4(&vQuartenion, XMQuaternionRotationMatrix(XMLoadFloat4x4(&rWorldMatrix)));
	m_OOBB.Orientation = vQuartenion;
	m_OOBB.Center = XMFLOAT3(	m_vCenter.x + rWorldMatrix._41, 
								m_vCenter.y + rWorldMatrix._42, 
								m_vCenter.z + rWorldMatrix._43);
}

std::array<BoundingFace, 6> CBoundingObject_OOBB::getFaces()const
{
	std::array<XMFLOAT3, 8> vVertices;

//	for(auto &rData : vVertices)		rData = m_OOBB.Center;

	//- - -	
	vVertices[0].x =- m_OOBB.Extents.x;	vVertices[0].y =- m_OOBB.Extents.y;	vVertices[0].z =- m_OOBB.Extents.z;
	//- - +	
	vVertices[1].x =- m_OOBB.Extents.x;	vVertices[1].y =- m_OOBB.Extents.y;	vVertices[1].z =+ m_OOBB.Extents.z;
	//- + -	
	vVertices[2].x =- m_OOBB.Extents.x;	vVertices[2].y =+ m_OOBB.Extents.y;	vVertices[2].z =- m_OOBB.Extents.z;
	//- + +	
	vVertices[3].x =- m_OOBB.Extents.x;	vVertices[3].y =+ m_OOBB.Extents.y;	vVertices[3].z =+ m_OOBB.Extents.z;
	//+ - -	
	vVertices[4].x =+ m_OOBB.Extents.x;	vVertices[4].y =- m_OOBB.Extents.y;	vVertices[4].z =- m_OOBB.Extents.z;
	//+ - +	
	vVertices[5].x =+ m_OOBB.Extents.x;	vVertices[5].y =- m_OOBB.Extents.y;	vVertices[5].z =+ m_OOBB.Extents.z;
	//+ + -	
	vVertices[6].x =+ m_OOBB.Extents.x;	vVertices[6].y =+ m_OOBB.Extents.y;	vVertices[6].z =- m_OOBB.Extents.z;
	//+ + +	
	vVertices[7].x =+ m_OOBB.Extents.x;	vVertices[7].y =+ m_OOBB.Extents.y;	vVertices[7].z =+ m_OOBB.Extents.z;
	
	XMMATRIX mWorld = XMMatrixRotationQuaternion(XMLoadFloat4(&m_OOBB.Orientation)) 
					* XMMatrixTranslation(m_OOBB.Center.x, m_OOBB.Center.y, m_OOBB.Center.z);
	for (auto &rData : vVertices)
		XMStoreFloat3(&rData, XMVector3TransformCoord(XMLoadFloat3(&rData), mWorld));

	BoundingFace top	(std::vector<XMFLOAT3>{vVertices[2], vVertices[3], vVertices[7], vVertices[6]},true);
	BoundingFace bottom	(std::vector<XMFLOAT3>{vVertices[0], vVertices[4], vVertices[5], vVertices[1]},true);
	BoundingFace left	(std::vector<XMFLOAT3>{vVertices[0], vVertices[1], vVertices[3], vVertices[2]},true);
	BoundingFace right	(std::vector<XMFLOAT3>{vVertices[4], vVertices[6], vVertices[7], vVertices[5]},true);
	BoundingFace front	(std::vector<XMFLOAT3>{vVertices[1], vVertices[5], vVertices[7], vVertices[3]},true);
	BoundingFace back	(std::vector<XMFLOAT3>{vVertices[0], vVertices[2], vVertices[6], vVertices[4]},true);

	return std::array<BoundingFace, 6>({top, bottom, left, right, front, back });
}

CBoundingObject_Ray::CBoundingObject_Ray(const DirectX::XMFLOAT3 & rvStartPos, const DirectX::XMFLOAT3 & rvDir, const float fMaxLength)
	:m_BoundingRay(rvStartPos,rvDir,fMaxLength)
{
}

const bool CBoundingObject_Ray::checkCollision(const IBoundingObject * pOther)const
{
	if (!pOther)	return false;
	switch (pOther->getCollisionObjectType())	{
	case ECollisionObjectType::eOOBB:
		return CGlobalCollisionCalculator::checkCollision(static_cast<const CBoundingObject_OOBB*>(pOther)->getOOBB(), m_BoundingRay);


	}
	return false;
}
