#pragma once
#ifndef HEADER_ACTION
#define HEADER_ACTION

#include "Interface_Object.h"
#include "..\Module_DXComponent\Type_InDirectX.h"

//���漱��
struct ID3D11Buffer;

enum EActionType : ENUMTYPE_256 {
	eActionType_Default = 0,
	eActionType_Repeat = 1,
	eActionType_Keep = 2
};
//----------------------------------- Struct -------------------------------------------------------------------------------------

class CAction {
private:
	const unsigned int m_nStartFrame;
	const unsigned int m_nFrameRange;
	const float m_fFPS;
	EActionType m_eType;
//	const bool m_bRepeat;

	static float s_fFPS_Default;
public:
	CAction(const unsigned int nStartFrame, const unsigned int nEndFrame, const EActionType eType = eActionType_Default, const float fFPS = s_fFPS_Default);
	~CAction() {}

	const unsigned int getFrame(const float fElapsedTime)const { auto retval = m_nStartFrame + static_cast<unsigned int>(m_fFPS *fElapsedTime); if (retval > m_nStartFrame + m_nFrameRange) retval = m_nStartFrame + m_nFrameRange; return retval; }
	const bool isEnd(const float fElapsedTime)const { return m_fFPS*fElapsedTime > m_nFrameRange; }
	const EActionType getType()const { return m_eType; }
	const float getActionTime()const { return static_cast<float>(m_nFrameRange) / m_fFPS; }

	static void setFPS(const float fFPS) { s_fFPS_Default = fFPS; }
};


#endif

