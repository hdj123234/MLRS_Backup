#include"stdafx.h"
#include"Space.h"

#include"..\Module_Object_Common\Painter_Common.h"

#include"..\Module_Object_Common\Terrain.h"

#ifndef HEADER_STL
#include<queue>
#include<functional>
#include<array>
#endif // !HEADER

CSpace::CSpace()
	:m_pTerrain(nullptr),
	m_pLine(new CPainter_Line),
	m_pShootPos(new DirectX::XMFLOAT3)
{
}

const bool CSpace::pick(const CBoundingObject_Ray & rBoundingRay, DirectX::XMFLOAT3 & out_rCollisionPoint) const
{
	using namespace DirectX;


	std::vector<const IBoundingObject *> pBoundingObjects;
//	std::vector<XMFLOAT3> vPoss;
	for (auto data : m_CollisionObjects)
	{
		auto pBoundingObject = data->getBoundingObject();
		if (rBoundingRay.checkCollision(pBoundingObject))
			pBoundingObjects.push_back(pBoundingObject);
			
	}
	if(pBoundingObjects.empty())
		return false;


	XMFLOAT3 vStartPoint = rBoundingRay.getRay().m_vStartPos;
	std::nth_element(pBoundingObjects.begin(), pBoundingObjects.begin() + 1, pBoundingObjects.end(), 
		[&rvStartPoint = vStartPoint]
		(const IBoundingObject * p1, const IBoundingObject * p2)->bool		{
			XMVECTOR vSP = XMLoadFloat3(&rvStartPoint);
			XMVECTOR v1 = XMLoadFloat3(&p1->getCenter()) - vSP;
			XMVECTOR v2 = XMLoadFloat3(&p2->getCenter()) - vSP;
			float fDistance1 = XMVectorGetX(XMVector3Dot(v1, v1));
			float fDistance2 = XMVectorGetX(XMVector3Dot(v2, v2));
			return fDistance1 < fDistance2;
		});

	const IBoundingObject * pBoundingObject = pBoundingObjects[0];
	XMVECTOR vSP = XMLoadFloat3(&(rBoundingRay.getRay().m_vStartPos));

	out_rCollisionPoint = pBoundingObject->getCenter();
	if (auto p = dynamic_cast<const CBoundingObject_OOBB *>(pBoundingObject))
	{
		int i = 0;
		auto &rFaces = p->getFaces();
		for (auto &rFace : rFaces)
		{
			if (CGlobalCollisionCalculator::getIntersection(rBoundingRay.getRay(), rFace, out_rCollisionPoint))
				break;
			i++;
			if (i == 6)
				int a = 0;
		}

	}
//	else
//		out_rCollisionPoint = pBoundingObject->getCenter();

	m_pLine->setLine(*m_pShootPos, out_rCollisionPoint);
	return true;

	//const auto nNumberOfVertex = vPoss.size();
	//XMVECTOR vDisplacement = XMLoadFloat3(&vPoss[0]) - vStartPoint;
	//float fMinDistance = XMVectorGetX(XMVector3Dot(vDisplacement, vDisplacement));
	//for (auto i=1;i<nNumberOfVertex;++i)
	//{
	//	XMVECTOR vDisplacement = XMLoadFloat3(&vPoss[i]) - vStartPoint;
	//	float fDistance = XMVectorGetX(XMVector3Dot(vDisplacement, vDisplacement));
	//	if (fDistance < fMinDistance)
	//	{
	//		fMinDistance = fDistance;
	//		out_rCollisionPoint = vPoss[i];
	//	}
	//}

	//if (auto p = dynamic_cast<const CBoundingObject_OOBB *>(pBoundingObject))
	//{
	//	auto &rFaces = p->getFaces();
	//	for (auto &rFace : rFaces)
	//		if (CGlobalCollisionCalculator::getIntersection(rBoundingRay.getRay(), rFace, tmp))
	//			break;
	//	vPoss.push_back(tmp);
	//	out_rCollisionPoint = tmp;

	//}
	//else
	//	vPoss.push_back(pBoundingObject->getCenter());
} 

RIGameObject_Collision * CSpace::pickObject(const CBoundingObject_Ray & rBoundingRay, FLAG_8 flag_ObjType) const
{
	using namespace DirectX;

	typedef std::pair<float, RIGameObject_Collision *> TmpNode;
	std::vector<TmpNode> pBoundingObjects;

	XMVECTOR vRayCenter = XMLoadFloat3(&rBoundingRay.getCenter());
	XMVECTOR vObjCenter;
	float fDistance;
	//	std::vector<XMFLOAT3> vPoss;
	for (auto pData : m_CollisionObjects)
	{
		auto pBoundingObject = pData->getBoundingObject();
		if (rBoundingRay.checkCollision(pBoundingObject))
		{
			vObjCenter = XMLoadFloat3(&pBoundingObject->getCenter());
			fDistance = XMVectorGetX(XMVector3Dot(vRayCenter, vObjCenter));
			pBoundingObjects.push_back(std::make_pair(fDistance, pData));
		}

	}
	if (pBoundingObjects.empty())
		return nullptr;

	std::sort(pBoundingObjects.begin(), pBoundingObjects.end(), 
		[](const TmpNode &r1, const TmpNode &r2) 
		{
			return r1.first < r2.first;
		}
	);

	for (auto data : pBoundingObjects)
	{
		FLAG_8 flag_tmp = 0;
		if(auto p = dynamic_cast<const RIGameObject_TypeGetter *>(data.second))
			flag_tmp = convertType_ToFlag(p->getObjType());
		if (flag_tmp&flag_ObjType)
			return data.second;
	}

	return nullptr;
}

void CSpace::setLine(CPainter_Line * pLine)
{ 
	*m_pLine = *pLine; 
}

float CSpace::getHeight_Terrain(float x, float z)
{
	return m_pTerrain->getHeight(x, z);
}

void CSpace::enable_Line()
{
	m_pLine->enable();
}

void CSpace::disable_Line()
{
	m_pLine->disable();
}

void CSpace::releaseObject()
{
	delete m_pLine;
	delete	m_pShootPos;
}
