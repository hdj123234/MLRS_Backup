#pragma once
#ifndef HEADER_BOUNDINGOBJECT
#define HEADER_BOUNDINGOBJECT

#include"Interface_Object.h"


#ifndef HEADER_DIRECTX
#include<DirectXCollision.h>
#endif // !HEADER_DIRECTX

enum ECollisionObjectType : ENUMTYPE_256 {
	eOOBB,
	eSphere,
	eRay

};

struct BoundingRay {
	DirectX::XMFLOAT3 m_vStartPos;
	DirectX::XMFLOAT3 m_vDir;
	float m_fMaxLength;

	BoundingRay (	const DirectX::XMFLOAT3 &rvStartPos,
					const DirectX::XMFLOAT3 &rvDir,
					const float fMaxLength);
};

struct BoundingFace {
	std::vector<DirectX::XMFLOAT3> m_vVertices;
	DirectX::XMFLOAT4 m_vPlane;
	bool m_bFrontFaceCheck;
	
	BoundingFace(	const std::vector<DirectX::XMFLOAT3> &rvVertices,
					const DirectX::XMFLOAT4 &rvPlane,
					bool bFrontFaceCheck =false);
	BoundingFace(const std::vector<DirectX::XMFLOAT3> &rvVertices,
					bool bFrontFaceCheck =false);
};


class CGlobalCollisionCalculator {
public:
//	static void checkCollision(const DirectX::BoundingOrientedBox &rOOBB1, const  DirectX::BoundingOrientedBox &rOOBB2);
//	static void checkCollision(const DirectX::BoundingOrientedBox &rOOBB, const DirectX::BoundingSphere &rSphere);
	static bool checkCollision(const DirectX::BoundingOrientedBox &rOOBB, const BoundingRay &rRay);
	
	static bool getIntersection(const BoundingRay &rRay, const BoundingFace &rFace, DirectX::XMFLOAT3 &out_rIntersection);

};


class CBoundingObject_OOBB : public IBoundingObject{
private:
	DirectX::XMFLOAT3 m_vCenter;
	DirectX::BoundingOrientedBox m_OOBB;

public:
	CBoundingObject_OOBB();
	virtual ~CBoundingObject_OOBB() {}

public:
	virtual const bool checkCollision (const IBoundingObject * pOther)const ;
	virtual const ECollisionObjectType getCollisionObjectType()const { return ECollisionObjectType::eOOBB; }
	virtual const DirectX::XMFLOAT3 getCenter()const { return m_OOBB.Center; }

	void setData(const DirectX::XMFLOAT3 &rvCenter, const DirectX::XMFLOAT3 &rvExtents) { m_vCenter = rvCenter; m_OOBB.Extents = rvExtents; }
	void setOOBB(const DirectX::XMFLOAT4X4 &rWorldMatrix) ;
	const DirectX::BoundingOrientedBox& getOOBB() const { return m_OOBB; }
	DirectX::BoundingOrientedBox& getOOBB() { return m_OOBB; }
	std::array<BoundingFace,6> getFaces()const;
};

//
//class CBoundingObject_Sphere : public IBoundingObject {
//private:
//	DirectX::BoundingSphere m_Sphere;
//
//public:
//	virtual ~CBoundingObject_Sphere()  {}
//
//protected:
//	virtual const ECollisionObjectType getCollisionObjectType() { return ECollisionObjectType::eSphere; }
//
//public:
//	virtual bool checkCollision(IBoundingObject * pOther);
//	const DirectX::BoundingSphere& getSphere() const { return m_Sphere; }
//};

class CBoundingObject_Ray : public IBoundingObject {
private:
	BoundingRay m_BoundingRay;

public:
	CBoundingObject_Ray(const DirectX::XMFLOAT3 &rvStartPos,
						const DirectX::XMFLOAT3 &rvDir,
						const float fMaxLength);
	virtual ~CBoundingObject_Ray() {}

public:
	virtual const bool checkCollision (const IBoundingObject * pOther)const;
	virtual const ECollisionObjectType getCollisionObjectType()const { return ECollisionObjectType::eRay; }
	virtual const DirectX::XMFLOAT3 getCenter()const { return m_BoundingRay.m_vStartPos; }

	const BoundingRay& getRay() const { return m_BoundingRay; }
};


#endif