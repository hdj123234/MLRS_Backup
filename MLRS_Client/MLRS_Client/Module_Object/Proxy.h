#pragma once
#ifndef HEADER_PROXY
#define HEADER_PROXY
 
#ifndef HEADER_STL
#include<set>
#endif // !HEADER_STL


//전방선언 

template<class Type> class CProxyManager;


template<class Type>
class CProxy {
	friend CProxyManager<Type>;

private:
	Type * m_pOrigin;
	CProxyManager<Type> *m_pProxyManager;
	bool m_bAvailable;

private:
	CProxy(Type *  pOrigin, CProxyManager<Type> * pProxyManager)
		:m_pOrigin(pOrigin),
		m_pProxyManager(pProxyManager),
		m_bAvailable(true)
	{	}

public:
	~CProxy();

	const bool isAvailable()const { return m_bAvailable; }
	Type * getData()
	{ 
		if (m_bAvailable) return m_pOrigin; 
		else return nullptr; 
	} 
	
private:
	void disableProxy()
	{
		m_bAvailable = false;
		m_pOrigin = nullptr;
		m_pProxyManager = nullptr;
	}
};


template<class Type>
class CProxyManager {
	friend CProxy<Type>;
private:
	Type * m_pOrigin;
	std::set<CProxy<Type> *> m_pProxys;

public:
	CProxyManager(Type * pOrigin)
		:m_pOrigin(pOrigin) {}
	~CProxyManager() { releaseAllProxy(); }

private:
	void releaseAllProxy()
	{
		for (auto pData : m_pProxys)
			pData->disableProxy();
		m_pProxys.clear();
	}
	void releaseProxy(CProxy<Type> *pProxy){ m_pProxys.erase(pProxy); }

public:
	const bool isAvailable()const { return m_bAvailable; }
	inline CProxy<Type> * getNewProxy()
	{
		CProxy<Type> * pNewProxy = new CProxy<Type>(m_pOrigin,this);
		m_pProxys.insert(pNewProxy);
		return pNewProxy;
	}

};

//----------------------------------------- 구현부 ------------------------------------------------------


template<class Type>
CProxy<Type>::~CProxy()
{
	m_pProxyManager->releaseProxy(this);
}


#endif