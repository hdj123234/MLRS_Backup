#pragma once
#ifndef HEADER_FACTORY_ANIMCOLTROLLER
#define HEADER_FACTORY_ANIMCOLTROLLER

#include "AnimController.h"

#ifndef HEADER_STL
#include<map>
#endif // !HEADER_STL

//���漱��
class CAnimData_NoInstancing;
class IGameModel;

class CFactory_AnimController : virtual public IFactory_AnimController {
private:
	CAnimData_NoInstancing *m_pAnimData;

public:
	CFactory_AnimController( CAnimData_NoInstancing *pAnimData)
		:m_pAnimData(pAnimData)
		{}
	CFactory_AnimController(IGameModel *pModel);
	virtual ~CFactory_AnimController()  {}

	virtual const CAnimData_NoInstancing * getAnimData() { return m_pAnimData; }
	virtual ID3D11Buffer * getBuffer_Updater_AnimIndicator();
};


#endif