#pragma once
#ifndef HEADER_MSG_MOVEMENT
#define HEADER_MSG_MOVEMENT
#include "..\Module_Object\Interface_Object.h"

#ifndef HEADER_DIRECTX
#include<D3D11.h>
#endif 

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif 

enum EMsgType_Movement :ENUMTYPE_256{
	eAuto,
	eIndex,
	eMove_LookBase,
	eMove_Direction,
	eReplace_Position,
	eReplace_PositionOnly,
	eReplace_PosAndDir,
	eSetDirection,
	eControl_Distance,
	eRotate_PitchYaw,
	eRotate_Pitch,
	eRotate_Yaw,
	eSetWorldTransform

};

class RIMsg_Movement_GetVector2 {
public:
	virtual ~RIMsg_Movement_GetVector2() = 0 {}

	virtual const DirectX::XMFLOAT2 * getVector()const = 0;
};
class RIMsg_Movement_GetVector3 {
public:
	virtual ~RIMsg_Movement_GetVector3() = 0 {}

	virtual const DirectX::XMFLOAT3 * getVector()const = 0;
};
class RIMsg_Movement_GetPosAndDir {
public:
	virtual ~RIMsg_Movement_GetPosAndDir() = 0 {}

	virtual const DirectX::XMFLOAT3 * getPos()const = 0;
	virtual const DirectX::XMFLOAT3 * getDir()const = 0;
};

class CMsg_Movement : public IMsg_Movement {
private:
	EMsgType_Movement m_Type;
public:
	CMsg_Movement(const EMsgType_Movement type)
		:m_Type(type){}
	virtual ~CMsg_Movement() {}
	
	virtual const EMsgType_Movement getMovementType()const { return m_Type; }
};

class CMsg_Movement_Float :  public CMsg_Movement {
private:
	float m_fData;
public:
	CMsg_Movement_Float(const EMsgType_Movement type, const float f)
		:CMsg_Movement(type),
		m_fData(f) {}
	virtual ~CMsg_Movement_Float() {}


	const float getData()const { return m_fData; }
};

class CMsg_Movement_Vector2 : public CMsg_Movement, 
								virtual public RIMsg_Movement_GetVector2 {
private:
	DirectX::XMFLOAT2 m_vVector;
public:
	CMsg_Movement_Vector2(const EMsgType_Movement type, const DirectX::XMFLOAT2 &rvVector)
		:CMsg_Movement(type),
		m_vVector(rvVector) {}
	CMsg_Movement_Vector2(const EMsgType_Movement type, const float fx, const float fy)
		:CMsg_Movement(type),
		m_vVector(fx, fy) {}
	virtual ~CMsg_Movement_Vector2() {}


	const DirectX::XMFLOAT2 * getVector()const { return &m_vVector; }
};

class CMsg_Movement_Vector3 :	public CMsg_Movement,
								virtual public RIMsg_Movement_GetVector3 {
private:
	DirectX::XMFLOAT3 m_vVector;
public:
	CMsg_Movement_Vector3(const EMsgType_Movement type, const DirectX::XMFLOAT3 &rvVector)
		:CMsg_Movement(type),
		m_vVector(rvVector){}
	CMsg_Movement_Vector3(const EMsgType_Movement type, const float fx, const float fy, const float fz)
		:CMsg_Movement(type),
		m_vVector(fx,fy,fz) {}
	virtual ~CMsg_Movement_Vector3(){}

	const DirectX::XMFLOAT3 * getVector()const { return &m_vVector; }
};


class CMsg_Movement_PosAndDir : public CMsg_Movement,
								virtual public RIMsg_Movement_GetPosAndDir {
private:
	DirectX::XMFLOAT3 m_vPos;
	DirectX::XMFLOAT3 m_vDir;
public:
	CMsg_Movement_PosAndDir(	const DirectX::XMFLOAT3 &rvPos, 
								const DirectX::XMFLOAT3 &rvDir )
		:CMsg_Movement(EMsgType_Movement::eReplace_PosAndDir)
		, m_vPos(rvPos),
		m_vDir(rvDir) {}
	virtual ~CMsg_Movement_PosAndDir() {}

	virtual const DirectX::XMFLOAT3 * getPos()const {return &m_vPos;};
	virtual const DirectX::XMFLOAT3 * getDir()const {return &m_vDir;};
};


class CMsg_Movement_SetWorldTransform: public CMsg_Movement {
private:
	DirectX::XMFLOAT3 m_vPos;
	DirectX::XMFLOAT3 m_vRight;
	DirectX::XMFLOAT3 m_vUp;
	DirectX::XMFLOAT3 m_vLook;

public:
	CMsg_Movement_SetWorldTransform(	const DirectX::XMFLOAT3 &rvPos	,
										const DirectX::XMFLOAT3 &rvRight	,
										const DirectX::XMFLOAT3 &rvUp ,
										const DirectX::XMFLOAT3 &rvLook	)
		:CMsg_Movement(EMsgType_Movement::eSetWorldTransform)
		, m_vPos(rvPos),
		m_vRight(rvRight),
		m_vUp(rvUp),
		m_vLook(rvLook) {}
	virtual ~CMsg_Movement_SetWorldTransform() {}

	const DirectX::XMFLOAT3 & getPos()const { return m_vPos; }
	const DirectX::XMFLOAT3 & getRight()const { return m_vRight; }
	const DirectX::XMFLOAT3 & getUp()const { return m_vUp; }
	const DirectX::XMFLOAT3 & getLook()const { return m_vLook; }

};

#endif