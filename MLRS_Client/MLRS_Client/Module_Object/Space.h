#pragma once
#ifndef HEADER_SPACE
#define HEADER_SPACE

#include"BoundingObject.h"
#include"Interface_Object.h"

#ifndef HEADER_STL
#include<set>
#endif // !HEADER_STL

#ifndef HEADER_DIRECTX
#include<DirectXCollision.h>
#endif // !HEADER_DIRECTX

//���漱��
class CPainter_Line;
class CTerrain;

enum ECollisionObjType : FLAG_8 {
	eCollisionObjType_Player = 1,
	eCollisionObjType_Enemy = 2
};

class CSpace : public ISpace {
private:
	std::set<RIGameObject_Collision *> m_CollisionObjects;
	CTerrain *m_pTerrain;
	CPainter_Line * m_pLine;
	DirectX::XMFLOAT3 * m_pShootPos;
public:
	CSpace();
	virtual ~CSpace() { CSpace::releaseObject(); }

	virtual void addObject(RIGameObject_Collision *pCollision) {if(pCollision) m_CollisionObjects.insert(pCollision); }
	virtual void releaseObject(RIGameObject_Collision *pCollision) { m_CollisionObjects.erase(pCollision); }

	virtual const bool pick(const CBoundingObject_Ray & rBoundingRay, DirectX::XMFLOAT3 &out_rCollisionPoint)const;
	virtual const std::set<RIGameObject_Collision *>& getObjects() { return m_CollisionObjects; }

	virtual RIGameObject_Collision * pickObject(const CBoundingObject_Ray & rBoundingRay, FLAG_8 flag_ObjType = FLAG_8_ALL)const;
	
	void setLine(CPainter_Line *pLine);
	void setShootPos(DirectX::XMFLOAT3 * pShootPos){ *m_pShootPos = *pShootPos; }

	void setTerrain(CTerrain * pTerrain) { m_pTerrain = pTerrain; }
	float getHeight_Terrain(float x, float z);

	void enable_Line() ;
	void disable_Line();

	void releaseObject();

	const FLAG_8 convertType_ToFlag(const EGameObjType eObjType)const
	{
		switch (eObjType)
		{
		case EGameObjType::eAllied:
		case EGameObjType::ePlayer:
			return static_cast<FLAG_8>(ECollisionObjType::eCollisionObjType_Player);
		case EGameObjType::eEnemy:
			return static_cast<FLAG_8>(ECollisionObjType::eCollisionObjType_Enemy);
		case EGameObjType::eNeutral:
		default:
			return 0;
		}
	}
};


#endif