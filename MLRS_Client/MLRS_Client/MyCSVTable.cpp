#include "stdafx.h"
#include "MyCSVTable.h"
 

std::vector<std::vector<std::string>> CMyCSVTable::readFile(const std::wstring & rwsFileName, bool bColumnName)
{
	using namespace std;

	vector<vector<string>> retval;
	std::string sTmp;
	unsigned int nColumnCount = 0;
	
	ifstream ifs(rwsFileName);
	if (!ifs.is_open())	return retval;

	//columnCount ���
	{
		getline(ifs, sTmp);
		stringstream ss(sTmp);
		while (!ss.eof())
		{
			getline(ss, sTmp, ',');
			++nColumnCount;
		}
	}
	ifs.seekg(0, std::ios::beg);
	
	if(bColumnName)		getline(ifs, sTmp);  
	
	while (!ifs.eof())
	{
		getline(ifs, sTmp);
		if (sTmp == "")	break;
		stringstream ss(sTmp);

		vector<string> v;
		v.resize(nColumnCount);
		for(auto i=0U;i<nColumnCount;++i)
			getline(ss, v[i], ',');

		retval.push_back(std::move(v));
	} 


	return std::move(retval); 
}

std::vector<std::vector<std::wstring>> CMyCSVTable_wstring::readFile(const std::wstring & rwsFileName, bool bColumnName)
{
	using namespace std;

	vector<vector<wstring>> retval;
	std::wstring sTmp;
	unsigned int nColumnCount = 0;

	wifstream ifs(rwsFileName);
	if (!ifs.is_open())	return retval;

	//columnCount ���
	getline(ifs, sTmp);
	wstringstream ss(sTmp);
	while (!ss.eof())
	{
		getline(ss, sTmp, L',');
		++nColumnCount;
	}
	ifs.seekg(0, std::ios::beg);

	if (bColumnName)		getline(ifs, sTmp);

	while (!ifs.eof())
	{
		vector<wstring> v;
		v.resize(nColumnCount);
		for (auto i = 0U; i<nColumnCount; ++i)
			getline(ifs, v[i], L',');

		retval.push_back(std::move(v));
	}
	return retval;


	return std::move(retval);
}
