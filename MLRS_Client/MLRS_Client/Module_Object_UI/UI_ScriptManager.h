#pragma once

#ifndef HEADER_UI_SCRIPTMANAGER
#define HEADER_UI_SCRIPTMANAGER

#include"Interface_UI.h"
#include"UI_Simple_ScreenBase.h"
 


//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------
class IFactory_UI_ScriptManager{
public:
	virtual ~IFactory_UI_ScriptManager() = 0 {}

	virtual const float getEndTime()=0;
	virtual std::vector<CUI_Simple_ScreenBase *>&& getScripts()=0;
}; 

//----------------------------------- Class -------------------------------------------------------------------------------------
class CUI_ScriptManager :	public IUserInterface,
							virtual public RIGameObject_Animate{
private:
	static const int s_nScriptDisable = -1 ;

private: 
	int m_nEnableScript;
	float m_fElapsedTime;
	float m_fEndTime;
	std::vector<CUI_Simple_ScreenBase *> m_pScripts;

	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_ANIMATE |FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR;

public:
	CUI_ScriptManager()
		: m_nEnableScript(s_nScriptDisable),
		m_fElapsedTime(0),
		m_fEndTime(0)
	{}
	virtual ~CUI_ScriptManager() {}

private:
	inline void endScript()
	{
		if (CUI_ScriptManager::isEnable())
			m_pScripts[m_nEnableScript]->disable();
		m_fElapsedTime = 0;
		m_nEnableScript = s_nScriptDisable;
	}

public:
	//override IUserInterface
	virtual void enable() {  }
	virtual void disable() 	{  endScript(); }
	virtual const bool isEnable()const{ return (m_nEnableScript != s_nScriptDisable);}

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const 	{return static_cast<unsigned char>(CUI_ScriptManager::isEnable()) * s_Flag;	}

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject * pMsg) {  }

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return true; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const
	{ 
		if (CUI_ScriptManager::isEnable())  
			m_pScripts[m_nEnableScript]->drawOnDX(pRendererState);
	} 

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime)
	{
		if (m_nEnableScript == s_nScriptDisable)	return std::vector<IMsg *>();

		m_fElapsedTime += fElapsedTime;

		if (m_fElapsedTime > m_fEndTime)
			endScript();

		return std::vector<IMsg *>();
	}
	 
	bool createObject(IFactory_UI_ScriptManager  *pFactory)
	{
		m_fEndTime = pFactory->getEndTime();
		m_pScripts = pFactory->getScripts();
		return true;
	}

	void setScript(unsigned int nScriptID)
	{
		endScript();
		m_nEnableScript = nScriptID;
		m_pScripts[m_nEnableScript]->enable();
	}
};
 

#endif