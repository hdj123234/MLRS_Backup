#pragma once
#ifndef HEADER_FACTORY_UI_LAUNCHGUIDE_PARABOLA
#define HEADER_FACTORY_UI_LAUNCHGUIDE_PARABOLA

#include"UI_LaunchGuide_Parabola.h"
#include"..\Module_Object_Common\Factory_Painter_Common.h"

class CFactory_UI_LaunchGuide_Parabola : virtual public IFactory_UI_LaunchGuide_Parabola{
private:
	CFactory_Painter_Curve m_Factory_CurvePainter;
	CFactory_Painter_CirclePlate m_Factory_CirclePlatePainter;

public:
	CFactory_UI_LaunchGuide_Parabola(
		const std::string &rsName,
		const DirectX::XMFLOAT4 &rvCurveColor,
		const DirectX::XMFLOAT4 &rvCirclePlateColor,
		const float fCirclePlateRadius,
		const unsigned int nMaxNumberOfVertices_Curve)
		:m_Factory_CurvePainter(rsName+"_Curve", rvCurveColor, nMaxNumberOfVertices_Curve),
		m_Factory_CirclePlatePainter(rsName + "_CirclePlate", rvCirclePlateColor,fCirclePlateRadius)
	{
	}
	virtual ~CFactory_UI_LaunchGuide_Parabola(){}

	virtual IFactory_Painter_Curve * getFactory_Curve() { return &m_Factory_CurvePainter; }
	virtual IFactory_Painter_CirclePlate * getFactory_CirclePlate() { return &m_Factory_CirclePlatePainter; }
	
};


#endif