#pragma once

#ifndef HEADER_UI_SIMPLE_SCREENBASE
#define HEADER_UI_SIMPLE_SCREENBASE

#include"Interface_UI.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"
#include "..\Module_Object_GameModel\Material.h"

#include "..\Module_Renderer\RendererState_D3D.h"

typedef CMesh_VertexAndGS CMesh_UI_ScreenBase;
typedef CMaterial_NoneLighting_Diffuse CMaterial_UI_ScreenBase;


//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------


class IFactory_UI_Simple_ScreenBase {
public:
	virtual ~IFactory_UI_Simple_ScreenBase() = 0 {}

	virtual CMesh_VertexAndGS * createMesh() = 0;
	virtual CMaterial_NoneLighting_Diffuse * createMaterial() = 0;

};
class IFactory_UI_Simple_ScreenBase_Swap_OnOff: virtual public IFactory_UI_Simple_ScreenBase {
public:
	virtual ~IFactory_UI_Simple_ScreenBase_Swap_OnOff() = 0 {}

	virtual CMaterial_NoneLighting_Diffuse * createMaterial_On() = 0;
};

class IFactory_Mesh_UI_Simple : virtual public IFactory_Mesh_VertexAndGS {
public:
	virtual ~IFactory_Mesh_UI_Simple() = 0 {}

	virtual ID3D11Buffer * createCBuffer_Projection() = 0;
	virtual ID3D11ShaderResourceView * createDiffuseMap() = 0;
};
//----------------------------------- Mesh -------------------------------------------------------------------------------------



class CMesh_UI_Simple : public CMesh_VertexAndGS {
private:
	ID3D11Buffer * m_pCBuffer_Projection;
	ID3D11ShaderResourceView * m_pDiffuseMap;

public:
	CMesh_UI_Simple() 
		:m_pCBuffer_Projection(nullptr),
		m_pDiffuseMap(nullptr) {}
	virtual ~CMesh_UI_Simple() {}

private:
	//override AMaterial
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const
	{
		pRendererState->setConstBuffer_GS(m_pCBuffer_Projection);
		pRendererState->setShaderResourceViews_GS(m_pDiffuseMap);
	}

public:
	bool createObject(IFactory_Mesh_UI_Simple *pFactory)
	{
		m_pCBuffer_Projection = pFactory->createCBuffer_Projection();
		m_pDiffuseMap = pFactory->createDiffuseMap();
		if (!(m_pCBuffer_Projection&&m_pDiffuseMap))	return false;
		return CMesh_VertexAndGS::createObject(pFactory);
	}

};


//----------------------------------- UI -------------------------------------------------------------------------------------

class CUI_Simple_ScreenBase : public IUserInterface {
protected:
	CMaterial_NoneLighting_Diffuse * m_pMaterial;
private:
	CMesh_VertexAndGS * m_pMesh;

	bool m_bEnable;
public:
	CUI_Simple_ScreenBase()
		:m_bEnable(true) ,
		m_pMesh(nullptr),
		m_pMaterial(nullptr){}
	virtual ~CUI_Simple_ScreenBase() { releaseObject(); }

	//	void disable();

	//override IGameObject
	virtual const FLAG_8 getTypeFlag() const { return static_cast<int>(m_bEnable)*(FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR); }

	//override RIGameObject_Drawable
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const
	{
		if (m_bEnable)
		{
			m_pMaterial->setOnDX(pRendererState);
			m_pMesh->setOnDX(pRendererState);
			m_pMesh->drawOnDX(pRendererState);
		}
	}
	virtual const  bool isClear() const { return true; }

	//override IUserInterface
	virtual void enable() { m_bEnable = true; }
	virtual void disable() { m_bEnable = false; }
	virtual const bool isEnable()const { return m_bEnable; }

	bool createObject(IFactory_UI_Simple_ScreenBase *pFactory)
	{
		m_pMaterial = pFactory->createMaterial();
		m_pMesh = pFactory->createMesh();
		return  (m_pMaterial&&m_pMesh);
	}
	void releaseObject() 
	{
		if (m_pMesh)		delete m_pMesh;
		if (m_pMaterial)	delete m_pMaterial;
	}
};


class CUI_Simple_ScreenBase_OnOff : public CUI_Simple_ScreenBase,
									virtual public RIUserInterface_OnOff{
public:
	CUI_Simple_ScreenBase_OnOff() {}
	virtual ~CUI_Simple_ScreenBase_OnOff() { releaseObject(); }

	virtual void setMode(bool bOn)
	{
		if (bOn)	enable();
		else disable();
	}
};


class CUI_Simple_ScreenBase_Swap_OnOff : public CUI_Simple_ScreenBase,
										virtual public RIUserInterface_OnOff {
private:
	CMaterial_NoneLighting_Diffuse * m_pMaterial_On;
	CMaterial_NoneLighting_Diffuse * m_pMaterial_Off;

public:
	CUI_Simple_ScreenBase_Swap_OnOff():m_pMaterial_On(nullptr), m_pMaterial_Off(nullptr){}
	virtual ~CUI_Simple_ScreenBase_Swap_OnOff() { CUI_Simple_ScreenBase_Swap_OnOff::releaseObject(); }

	//	void disable();

	bool createObject(IFactory_UI_Simple_ScreenBase_Swap_OnOff *pFactory)
	{
		m_pMaterial_On = pFactory->createMaterial_On();
		if (!CUI_Simple_ScreenBase::createObject(pFactory))	return false;
		m_pMaterial_Off = m_pMaterial;
		return  (m_pMaterial_On&&m_pMaterial_Off);
	}
	void releaseObject()
	{
		if (m_pMaterial_On)		delete m_pMaterial_On;
		if (m_pMaterial_Off)	delete m_pMaterial_Off;
		m_pMaterial = nullptr;
	}

	//override RIUserInterface_OnOff
	void setMode(bool bOn) { 
		if(bOn)m_pMaterial = m_pMaterial_On;
		else m_pMaterial = m_pMaterial_Off;
	}
};

#endif