#pragma once


#ifndef HEADER_UIMANAGER
#define HEADER_UIMANAGER

#include"Interface_UI.h"

//���漱��
struct ID3D11DepthStencilState;
struct ID3D11BlendState;

class IFactory_UIManager {
public:
	virtual ~IFactory_UIManager() = 0 {}
	virtual ID3D11DepthStencilState * createDepthStencilState()=0;
	virtual ID3D11BlendState *createBlendState()				=0;
	virtual std::vector<IUserInterface *> createUIs() = 0;
};

class AFactory_UIManager : public IFactory_UIManager {
public:
	virtual ~AFactory_UIManager()=0 {}

	virtual ID3D11DepthStencilState * createDepthStencilState() ;
	virtual ID3D11BlendState *createBlendState() ;
};

class AUIManager :	public IUIManager {
private:
	ID3D11DepthStencilState *m_pDepthStencilState;
	ID3D11BlendState *m_pBlendState;

	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW | FLAG_OBJECTTYPE_ANIMATE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR;

protected:
	std::vector<IUserInterface *> m_pUIs;

public:
	AUIManager() {}
	virtual ~AUIManager() { relaeseObject(); }

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_Flag; }

	//override RIGameObject_BeMust_RenewalForDraw
	void updateOnDX(CRendererState_D3D *pRendererState)const;

	//override RIGameObject_HandleMsg
//	virtual void handleMsg(IMsg_GameObject * pMsg);

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return true; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	bool createObject(IFactory_UIManager *pFactory);
	void relaeseObject();
};

#endif