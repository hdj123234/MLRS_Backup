#pragma once
#ifndef HEADER_FACTORY_UI_GAUGE
#define HEADER_FACTORY_UI_GAUGE

#include"UI_Gauge.h"

class CFactory_UI_Gauge : virtual public IFactory_UI_Gauge,
							virtual public IFactory_Mesh_UI_Gauge,
							virtual public IFactory_Material_NoneLighting_Diffuse{
private:
	const std::string m_sName;
	DirectX::XMFLOAT2 m_vPos;
	float m_fScale;

	const std::string m_sImageFileName;
	const std::string m_sVSName;
	const std::string m_sGSName;
	const std::string m_sPSName;

	ID3D11Texture2D *m_pStencilBuffer;
	ID3D11DepthStencilView * m_pStencilView;
public:
	CFactory_UI_Gauge(	const std::string &rsName, 
						const DirectX::XMFLOAT2 &rvPos,
						const float fScale,
						const std::string &rsImageFileName,
						const std::string &rsVSName = "vsDummy",
						const std::string &rsGSName = "gsUI_ScreenBase_Gauge",
						const std::string &rsPSName = "psTexOnly");
	virtual ~CFactory_UI_Gauge(){}

private:
	bool createStencilView_RealProcessing();
	void initializeStencilBuffer();

public:
	//override IFactory_UI_Gauge
	virtual CMesh_UI_Gauge * createMesh() ;
	virtual CMaterial_NoneLighting_Diffuse * createMaterial() ;
	virtual ID3D11DepthStencilState *	createStencilState()	;
	virtual ID3D11DepthStencilView *	createStencilView()		;
	virtual ID3D11Texture2D *			createStencilBuffer()	;
	virtual Type_UI_Gauge getGauge() ;

	//override IFactory_Mesh
	virtual ID3D11VertexShader		* createVertexShader() ;
	virtual const unsigned int getMaterialID() { return 0; }

	//override IFactory_Mesh_GS
	virtual ID3D11GeometryShader	* createGeometryShader() ;

	//override IFactory_Mesh_UI_Gauge
	virtual std::vector<ID3D11Buffer *> createCBuffers()   ;

	//override IFactory_Material 
	virtual ID3D11PixelShader *createPixelShader() ;
	
	//override IFactory_Material_NoneLighting_Diffuse 
	virtual ID3D11ShaderResourceView * createDiffuseMap() ;
	virtual ID3D11SamplerState * createSamplerState() ;
	
};


#endif