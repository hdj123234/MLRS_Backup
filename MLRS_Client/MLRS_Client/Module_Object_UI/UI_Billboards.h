#pragma once

#ifndef HEADER_UI_BILLBOARD
#define HEADER_UI_BILLBOARD

#include"Interface_UI.h"
#include"..\Module_Object_Common\BillboardEffect.h"
#include"..\Module_Object\Proxy.h"

//���漱��
struct ID3D11DepthStencilState;
struct ID3D11DepthStencilView;
struct ID3D11BlendState;
struct ID3D11Buffer; 
class CFactory_UI_Billboards_Instance;

//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------
 
class IFactory_UI_Billboards {
public:
	virtual ~IFactory_UI_Billboards() = 0 {}


	virtual CModel_BillboardEffect * createModel() = 0;
	virtual const float getFPS() = 0;
	virtual unsigned int getNumberOfTexture()=0;

};

struct UI_Billboards_InstanceData{
	CInstance_BillboardAnimation *m_pInstance;
	Type_InstanceData_BillboardEffect m_Value;
	CProxy<RIGameObject_PositionGetter> *m_pProxy;
	float m_fElapsedTime;
	
	UI_Billboards_InstanceData(	CInstance_BillboardAnimation *pInstance,
								CProxy<RIGameObject_PositionGetter> * pPosProxy)
		:m_pInstance(pInstance),
		m_Value(),
		m_pProxy(pPosProxy),
		m_fElapsedTime(0)
	{	}
	virtual ~UI_Billboards_InstanceData()
	{
		if (m_pInstance)	delete m_pInstance; 
		if (m_pProxy)	delete m_pProxy; 
	}
};

//----------------------------------- Class -------------------------------------------------------------------------------------
 
class CUI_Billboards :	public IUserInterface,
						virtual public RIGameObject_Animate {
private:
	CModel_BillboardEffect *m_pModel;
	CFactory_UI_Billboards_Instance *m_pFactory_Instance;
	std::vector<UI_Billboards_InstanceData *> m_pInstances;

	float m_fFPS;
	unsigned int m_nNumberOfTexture;
	 
	bool m_bEnable;
//	ID3D11BlendState *m_pBlendState;

	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR;

public:
	CUI_Billboards();
	virtual ~CUI_Billboards() { releaseObjects(); }

public:
	//override IUserInterface
	virtual void enable() { m_bEnable = true; }
	virtual void disable() { m_bEnable = false; releaseAllInstance(); }
	virtual const bool isEnable()const { return m_bEnable; }

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_Flag | (static_cast<FLAG_8>(m_bEnable==true)*FLAG_OBJECTTYPE_ANIMATE ); }

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return true; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const 
	{
		if (m_bEnable && !m_pInstances.empty())
		{
			m_pModel->updateOnDX(pRendererState);
			m_pModel->drawOnDX(pRendererState); 
		}
	}

	//override RIGameObject_Animate
	virtual std::vector<IMsg *> animateObject(const float fElapsedTime);

	bool createObject(IFactory_UI_Billboards *pFactory);
	void releaseObjects();
	 
	void addInstance(CProxy<RIGameObject_PositionGetter> *pProxy);
	void releaseAllInstance();
};

#endif