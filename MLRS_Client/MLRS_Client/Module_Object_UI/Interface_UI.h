
#ifndef HEADER_INTERFACE_UI
#define HEADER_INTERFACE_UI

#include"..\Module_Object\Interface_Object.h"

#ifndef HEADER_STL
#include<vector>
#endif // !HEADER_STL
 


class IUserInterface :	virtual public IGameObject,
						virtual public RIGameObject_Drawable {
public:
	virtual ~IUserInterface() = 0 {}

	virtual void enable() = 0;
	virtual void disable() = 0;

	virtual const bool isEnable()const = 0;
};
class RIUserInterface_OnOff {
public:
	virtual ~RIUserInterface_OnOff() = 0 {}

	virtual void setMode(bool bOn) = 0;
};

class IUIManager :	virtual public IGameObject,
					virtual public RIGameObject_BeMust_RenewalForDraw,
					virtual public RIGameObject_Animate,
					virtual public RIGameObject_Drawable,
					virtual public RIGameObject_HandleMsg {
public:
	virtual ~IUIManager() = 0 {}
};

#endif