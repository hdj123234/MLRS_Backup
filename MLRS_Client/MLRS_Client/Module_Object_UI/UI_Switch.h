#pragma once

#ifndef HEADER_UI_SWITCH
#define HEADER_UI_SWITCH

#include"Interface_UI.h"
#include"UI_Simple_ScreenBase.h"

#ifndef HEADER_STL
#include<array>
#endif // !HEADER_STL



//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------
template<unsigned char NumberOfSwitch>
class IFactory_UI_Switch{
public:
	virtual ~IFactory_UI_Switch() = 0 {}

	virtual std::array<RIUserInterface_OnOff *, NumberOfSwitch>&& createUIs_OnOff() = 0;
	virtual std::array<IUserInterface *, NumberOfSwitch>&& createUIs() = 0;
}; 

//----------------------------------- Class -------------------------------------------------------------------------------------
template<unsigned char NumberOfSwitch>
class CUI_Switch :	public IUserInterface {
private: 
	std::array<RIUserInterface_OnOff *, NumberOfSwitch> m_pUIs_OnOff;
	std::array<IUserInterface*, NumberOfSwitch> m_pUIs;

	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR;

public:
	CUI_Switch()  {}
	virtual ~CUI_Switch() { CUI_Switch::releaseObject(); }

public:
	//override IUserInterface
	virtual void enable() {  }
	virtual void disable() { setFlag(0); }
	virtual const bool isEnable()const
	{
		bool bOn = false;
		for (auto pData : m_pUIs) bOn = bOn && pData->isEnable();
		return bOn;
	}

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const 
	{
		bool bOn = false; 
		for (auto pData : m_pUIs) bOn = bOn && pData->isEnable();
		return static_cast<unsigned char>(bOn)*s_Flag;
	}

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject * pMsg) {  }

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return true; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const
	{ 
		for (auto pData : m_pUIs)
			pData->drawOnDX(pRendererState);
	} 

	void setFlag(const FLAG_8 _flag)
	{
		FLAG_8  flag = _flag;
		for (auto i = 0U; i < NumberOfSwitch; ++i, flag=flag>>1)
			m_pUIs_OnOff[i]->setMode((flag & 0x01)!=0);
	}

	bool createObject(IFactory_UI_Switch<NumberOfSwitch> *pFactory)
	{
		m_pUIs_OnOff = pFactory->createUIs_OnOff();
		m_pUIs = pFactory->createUIs();
		return true;
	}
	void releaseObject()
	{
	//	for (auto data : m_pUIs)
	//		if (data) delete data;
	}
};
 

#endif