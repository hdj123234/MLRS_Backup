#include "stdafx.h" 
#include "UI_Billboards.h"

#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include"Factory_UI_Billboards.h"
//
//CUI_Billboards::CUI_Billboards()
//	:m_pModel(nullptr),
//	m_bEnable(false)
//{
//}
//
//void CUI_Billboards::drawOnDX(CRendererState_D3D * pRendererState) const
//{
//	if (!m_pInstances.empty())	m_pModel->drawOnDX(pRendererState);
//}
//
//std::vector<IMsg*> CUI_Billboards::animateObject(const float fElapsedTime)
//{
//	for (auto pData : m_pInstances)
//	{
//		pData->m_fElapsedTime += fElapsedTime;
//		pData->m_pInstance->setInstanceData
//	}
//	return std::vector<IMsg*>();
//}

CUI_Billboards::CUI_Billboards()
	:m_pModel(nullptr),
	m_pFactory_Instance(nullptr),
	m_fFPS(30),
	m_nNumberOfTexture(0),
	m_bEnable(false)
{
}
 

std::vector<IMsg*> CUI_Billboards::animateObject(const float fElapsedTime)
{
	float fAnimTime =  static_cast<float>(m_nNumberOfTexture) / m_fFPS;
	for (auto i=0U;i< m_pInstances.size();++i)
	{
		auto pData = m_pInstances[i];
		if (!pData->m_pProxy->isAvailable())
		{ 
			delete pData; 
			m_pInstances[i] = m_pInstances.back();
			--i;
		}
		else
		{
			pData->m_fElapsedTime += fElapsedTime;
			while (pData->m_fElapsedTime > fAnimTime)
				pData->m_fElapsedTime -= fAnimTime;
			pData->m_Value.m_nAnimIndex = static_cast<unsigned int>(pData->m_fElapsedTime*m_fFPS);
			pData->m_Value.m_vPos = pData->m_pProxy->getData()->getPosition();
			pData->m_pInstance->setValue(pData->m_Value);
		}
	}
	return std::vector<IMsg*>();
}

bool CUI_Billboards::createObject(IFactory_UI_Billboards * pFactory)
{
	if (!pFactory)	return false;

	m_pModel = pFactory->createModel();
	m_fFPS = pFactory->getFPS();
	m_nNumberOfTexture = pFactory->getNumberOfTexture();

	if(m_pModel==nullptr ||m_nNumberOfTexture<=0) return false;
	m_pFactory_Instance = new CFactory_UI_Billboards_Instance(m_pModel);
	return true;
}

void CUI_Billboards::releaseObjects()
{
	releaseAllInstance();

	if (m_pModel) delete m_pModel; 
	if (m_pFactory_Instance) delete m_pFactory_Instance;
	m_pFactory_Instance = nullptr;
}

void CUI_Billboards::addInstance(CProxy<RIGameObject_PositionGetter>* pProxy)
{
	if (!pProxy->isAvailable())	return;
	CInstance_BillboardAnimation * pTmp = new CInstance_BillboardAnimation;
	if (!pTmp->createObject(m_pFactory_Instance))
	{
		delete pTmp;
		return;
	}
	UI_Billboards_InstanceData *pInstance = new UI_Billboards_InstanceData(pTmp, pProxy); 
	pInstance->m_pInstance->enable();
	pInstance->m_Value.m_vPos = pProxy->getData()->getPosition();
	pInstance->m_pInstance->setValue(pInstance->m_Value);
	m_pInstances.push_back(pInstance);
}

void CUI_Billboards::releaseAllInstance()
{
	for (auto pData : m_pInstances)		delete pData;
	m_pInstances.clear();
}
