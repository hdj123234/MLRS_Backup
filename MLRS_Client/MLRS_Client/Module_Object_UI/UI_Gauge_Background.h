#pragma once

#ifndef HEADER_UI_GAUGE_BACKGROUND
#define HEADER_UI_GAUGE_BACKGROUND

#include"UI_Gauge.h"
#include"UI_Simple_ScreenBase.h"





//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------


class IFactory_UI_Gauge_Background {
public:
	virtual ~IFactory_UI_Gauge_Background() = 0 {}


	virtual CUI_Gauge *				createUI_Gauge()=0;
	virtual CUI_Simple_ScreenBase * createUI_Background()=0;

};


//----------------------------------- Class -------------------------------------------------------------------------------------


class CUI_Gauge_Background :	public IUserInterface {

private:
	CUI_Gauge * m_pGauge;
	CUI_Simple_ScreenBase * m_pBackground;

public:
	CUI_Gauge_Background():m_pGauge(nullptr), m_pBackground(nullptr){	}
	virtual ~CUI_Gauge_Background() {  }

public:
	//override IUserInterface
	virtual void enable() { m_pGauge->enable(); m_pBackground->enable(); }
	virtual void disable() { m_pGauge->disable(); m_pBackground->disable(); }
	virtual const bool isEnable()const { return m_pGauge->isEnable()&& m_pBackground->isEnable(); }

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const { return m_pGauge->getTypeFlag()| m_pBackground->getTypeFlag(); }

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject * pMsg) { m_pGauge->handleMsg(pMsg); }

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return true; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const
	{
		m_pBackground->drawOnDX(pRendererState);
		m_pGauge->drawOnDX(pRendererState);
	}

	bool createObject(IFactory_UI_Gauge_Background *pFactory)
	{
		m_pBackground	=pFactory->createUI_Background();
		m_pGauge		=pFactory->createUI_Gauge();
		return (m_pBackground&&m_pGauge);
	}
	void releaseObject()
	{
		if (m_pGauge)delete m_pGauge;
		if (m_pBackground)delete m_pBackground;
	}
	void setGauge(const float fGauge) { m_pGauge->setGauge(fGauge); }
};

#endif