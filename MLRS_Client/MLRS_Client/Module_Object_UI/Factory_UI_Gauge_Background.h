#pragma once
#ifndef HEADER_FACTORY_UI_GAUGE_BACKGROUND
#define HEADER_FACTORY_UI_GAUGE_BACKGROUND

#include"UI_Gauge_Background.h"
#include"Factory_UI_Gauge.h"
#include"Factory_UI_Simple_ScreenBase.h"

class CFactory_UI_Gauge_Background : virtual public IFactory_UI_Gauge_Background{
private:
	CFactory_UI_Gauge m_Factory_Gauge;
	CFactory_UI_Simple_PosAndScale m_Factory_Background;

public:
	CFactory_UI_Gauge_Background(	const std::string &rsName,
						const DirectX::XMFLOAT2 &rvPos,
						const float fScale,
						const std::string &rsImageFileName_Gauge,
						const std::string &rsImageFileName_Background,
						std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs)
		:m_Factory_Gauge(rsName+"_Gauge", rvPos, fScale, rsImageFileName_Gauge),
		m_Factory_Background(rsName + "_Background", rsImageFileName_Background,rvPos,fScale, rInputElementDescs)
	{	}
	virtual ~CFactory_UI_Gauge_Background(){}

public:
	virtual CUI_Gauge *				createUI_Gauge()
	{
		CUI_Gauge * pGauge = new CUI_Gauge;
		if (!pGauge->createObject(&m_Factory_Gauge))
		{
			delete pGauge;
			pGauge = nullptr;
		}
		return pGauge;
	}
	virtual CUI_Simple_ScreenBase * createUI_Background()
	{
		CUI_Simple_ScreenBase * pBackground = new CUI_Simple_ScreenBase;
		if (!pBackground->createObject(&m_Factory_Background))
		{
			delete pBackground;
			pBackground = nullptr;
		}
		return pBackground;

	}
};


#endif