#include "stdafx.h"
#include "UIManager.h"

#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"
#include "UI_Gauge.h"
//
////----------------------------------- CMesh_UI_Gauge -------------------------------------------------------------------------------------
//
CMesh_UI_Gauge::CMesh_UI_Gauge()
	:m_pDiffuseMap(nullptr) 
{
}

void CMesh_UI_Gauge::setOnDX_ShaderResource(CRendererState_D3D * pRendererState) const
{
	pRendererState->setConstBuffer_GS(m_pCBuffers);
	pRendererState->setShaderResourceViews_GS(m_pDiffuseMap);
}

void CMesh_UI_Gauge::updateOnDX(CRendererState_D3D * pRendererState) const
{
	CBufferUpdater::update(pRendererState->getDeviceContext(), m_pCBuffers[1], m_Guage);
}

bool CMesh_UI_Gauge::createObject(IFactory_Mesh_UI_Gauge * pFactory)
{
	m_pCBuffers = pFactory->createCBuffers();
	m_pDiffuseMap = pFactory->createDiffuseMap();
	m_Guage = pFactory->getGauge();
	if (!(m_pCBuffers.size() == 2 && m_pDiffuseMap))	return false;
	return CMesh_GS::createObject(pFactory);
}

//
//
////----------------------------------- CUI_Gauge -------------------------------------------------------------------------------------

CUI_Gauge::CUI_Gauge()
	:
	m_pMesh		(nullptr),
	m_pMaterial	(nullptr),
	m_fGauge	(1),
	m_pStencilState	(nullptr),
	m_pStencilView	(nullptr),
	m_pStencilBuffer(nullptr),
	m_bEnable	(true),
	m_bUpdate(true)
{
}


void CUI_Gauge::handleMsg(IMsg_GameObject * pMsg)
{
}

void CUI_Gauge::drawOnDX(CRendererState_D3D * pRendererState) const
{
	if (m_bUpdate) 
	{
		m_pMesh->updateOnDX(pRendererState);
		m_bUpdate = false;
	}
	pRendererState->setDepthStencilState(m_pStencilState);
	pRendererState->setDepthStencilView(m_pStencilView);
	m_pMesh->setOnDX(pRendererState);
	m_pMaterial->setOnDX(pRendererState);
	m_pMesh->drawOnDX(pRendererState);
	pRendererState->revertDepthStencilState();
	pRendererState->revertDepthStencilView();
}

bool CUI_Gauge::createObject(IFactory_UI_Gauge * pFactory)
{
	if(!pFactory)	return false;
	m_pMesh = pFactory->createMesh();
	m_pMaterial = pFactory->createMaterial();
//	m_pStencilBuffer = pFactory->createStencilBuffer();
	m_pStencilState = pFactory->createStencilState();
	m_pStencilView = pFactory->createStencilView();
	return (m_pMesh&&m_pMaterial&&m_pStencilState&&m_pStencilView);
}