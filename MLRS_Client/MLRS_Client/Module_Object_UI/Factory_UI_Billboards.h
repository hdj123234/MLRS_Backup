#pragma once
#ifndef HEADER_FACTORY_UI_BILLBOARDS
#define HEADER_FACTORY_UI_BILLBOARDS

#include"UI_Billboards.h"
#include"..\Module_Object_Common\Creator_BillboardEffect.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
 
class CFactory_UI_Billboards :	public CFactory_Model_BillboardEffect,
								virtual public IFactory_UI_Billboards {
private:
	const float m_fFPS;
	const unsigned int m_nNumberOfTexture;

public:
	CFactory_UI_Billboards(const std::string &rsName,
							const DirectX::XMFLOAT2 &rvSizeOfBillboard,
							const float fFPS,
							const unsigned int nMaxNumberOfInstance,
							const std::vector<std::string> &rsTextureFileNames,
							const std::string &rsVSName = "vsBillboard_Effect",
							const std::string &rsGSName = "gsBillboard_Effect",
							const std::string &rsPSName = "psBillboard_Effect")
		:CFactory_Model_BillboardEffect(rsName, rvSizeOfBillboard, nMaxNumberOfInstance, rsTextureFileNames, rsVSName, rsGSName, rsPSName),
		m_fFPS(fFPS),
		m_nNumberOfTexture(static_cast<unsigned int>(rsTextureFileNames.size()))
	{}
	virtual ~CFactory_UI_Billboards() {}

	virtual CModel_BillboardEffect * createModel()
	{
		CModel_BillboardEffect *pRetval = new CModel_BillboardEffect;
		if (!pRetval->createObject(this))
		{
			delete pRetval;
			return nullptr;
		}
		return pRetval;
	}
	virtual const float getFPS() { return m_fFPS; }
	virtual unsigned int getNumberOfTexture() { return m_nNumberOfTexture; }

	//override CFactory_Model_BillboardEffect
	virtual ID3D11DepthStencilState		* createDepthStencilState()	{ return factory_DepthStencilState_Disable.createDepthStencilState();	}
};

class CFactory_UI_Billboards_Instance : public IFactory_Billboard_Instance<Type_InstanceData_BillboardEffect>{
private: 
	CModel_Billboard<Type_InstanceData_BillboardEffect> * m_pModel;

public:
	CFactory_UI_Billboards_Instance(CModel_Billboard<Type_InstanceData_BillboardEffect> *pModel)
		:m_pModel(pModel)
	{}
	virtual ~CFactory_UI_Billboards_Instance() {}

	virtual CModel_Billboard<Type_InstanceData_BillboardEffect> *getModel() { return m_pModel; }
	virtual Type_InstanceData_BillboardEffect getData() { return Type_InstanceData_BillboardEffect(); }

};

#endif