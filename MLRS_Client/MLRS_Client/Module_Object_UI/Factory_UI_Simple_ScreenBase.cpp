#include"stdafx.h"
#include "Factory_UI_Simple_ScreenBase.h"


#include"..\Module_Object_GameModel\Factory_Material.h"
//#include"..\Module_Object_GameModel\Factory_Mesh_Plane.h"
#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"

#include"..\MyUtility.h"
#include"..\MyINI.h"


using namespace DirectX;


//----------------------------------- CFactory_UI_Simple_ScreenBase -------------------------------------------------------------------------------------


ID3D11VertexShader * AFactory_UI_Image::createVertexShader()
{
	return CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
}

AFactory_UI_Image::AFactory_UI_Image(
	const std::string & rsName,
	const std::string & rsImageFileName,
	const std::vector<D3D11_INPUT_ELEMENT_DESC>& rInputElementDescs,
	const std::string & rsVSName,
	const std::string & rsGSName, 
	const std::string & rsPSName)
	:m_sName(rsName),
	m_sImageFileName(rsImageFileName),
	m_sVSName(rsVSName),
	m_sGSName(rsGSName),
	m_sPSName(rsPSName),
	m_InputElementDescs(rInputElementDescs)
{
}

ID3D11InputLayout * AFactory_UI_Image::createInputLayout()
{
	return CFactory_InputLayout(m_sVSName, m_sVSName, "IL_" + m_sName, m_InputElementDescs).createInputLayout();

}

ID3D11GeometryShader * AFactory_UI_Image::createGeometryShader()
{
	return CFactory_GeometryShader(m_sGSName, m_sGSName).createShader();
}

ID3D11PixelShader * AFactory_UI_Image::createPixelShader()
{
	return CFactory_PixelShader(m_sPSName, m_sPSName).createShader();

}

ID3D11ShaderResourceView * AFactory_UI_Image::createDiffuseMap()
{
	return CFactory_ShaderResourceView_FromTextureFile(m_sName+"_DiffuseMap",m_sImageFileName).createShaderResourceView();
}

ID3D11SamplerState * AFactory_UI_Image::createSamplerState()
{
	return factory_SamplerState_Default.createSamplerState();

}


//----------------------------------- CFactory_UI_Simple_ScreenBase -------------------------------------------------------------------------------------

CFactory_UI_Simple_PosAndSize::CFactory_UI_Simple_PosAndSize(
	const std::string & rsName,
	const std::string & rsImageFileName, 
	const DirectX::XMFLOAT2 & rvPos, 
	const DirectX::XMFLOAT2 & rvSize,
	const std::vector<D3D11_INPUT_ELEMENT_DESC>& rInputElementDescs,
	const std::string & rsVSName, 
	const std::string & rsGSName, 
	const std::string & rsPSName)
	:AFactory_UI_Image(rsName, rsImageFileName, rInputElementDescs, rsVSName, rsGSName, rsPSName),
	m_vPos				(rvPos),
	m_vSize				(rvSize)
{
}


ID3D11Buffer * CFactory_UI_Simple_PosAndSize::createVertexBuffer()
{
	VertexData_UI_Simple_PosAndSize vertexData;
	vertexData.m_vPos = m_vPos;
	vertexData.m_vSize = m_vSize;
	return CFactory_VertexBuffer<VertexData_UI_Simple_PosAndSize>("VB_" + m_sName, 1).createBuffer(vertexData);
}



//----------------------------------- CFactory_UI_Simple_ScreenBase_Scale -------------------------------------------------------------------------------------


CFactory_UI_Simple_PosAndScale::CFactory_UI_Simple_PosAndScale(
	const std::string & rsName,
	const std::string & rsImageFileName,
	const DirectX::XMFLOAT2 & rvPos,
	const float fScale,
	const std::vector<D3D11_INPUT_ELEMENT_DESC>& rInputElementDescs, 
	const std::string & rsGSName, 
	const std::string & rsVSName, 
	const std::string & rsPSName)
	:AFactory_UI_Image(rsName, rsImageFileName, rInputElementDescs, rsVSName, rsGSName, rsPSName),
	m_vPos(rvPos),
	m_fSize(fScale)
{
}

ID3D11Buffer * CFactory_UI_Simple_PosAndScale::createVertexBuffer()
{
	VertexData_UI_Simple_PosAndScale vertexData;
	vertexData.m_vPos = m_vPos;
	vertexData.m_fSize = m_fSize;
	return CFactory_VertexBuffer<VertexData_UI_Simple_PosAndScale>("VB_" + m_sName, 1).createBuffer(vertexData);

}

ID3D11Buffer * CFactory_UI_Simple_PosAndScale::createCBuffer_Projection()
{
	return factory_CB_Matrix_Projection.createBuffer();
}

ID3D11ShaderResourceView * CFactory_UI_Simple_PosAndScale::createDiffuseMap()
{
	return AFactory_UI_Image::createDiffuseMap();
}

CMesh_VertexAndGS * CFactory_UI_Simple_PosAndScale::createMesh()
{
	CMesh_UI_Simple * pMesh = new CMesh_UI_Simple;
	if (!pMesh->createObject(this))
	{
		delete pMesh;
		pMesh = nullptr;
	}
	return pMesh;
}

CMaterial_NoneLighting_Diffuse * CFactory_UI_Simple_PosAndScale::createMaterial()
{
	CMaterial_NoneLighting_Diffuse * pMaterial= new CMaterial_NoneLighting_Diffuse;
	if (!pMaterial->createObject(this))
	{
		delete pMaterial;
		pMaterial = nullptr;
	}
	return pMaterial;
}

CFactory_UI_Simple_PosAndScale_OnOff::CFactory_UI_Simple_PosAndScale_OnOff(
	const std::string & rsName, 
	const std::string & rsImageFileName_On,
	const std::string & rsImageFileName_Off,
	const DirectX::XMFLOAT2 & rvPos,
	const float fScale, 
	const std::vector<D3D11_INPUT_ELEMENT_DESC>& rInputElementDescs,
	const std::string & rsGSName, 
	const std::string & rsVSName, 
	const std::string & rsPSName)
	:CFactory_UI_Simple_PosAndScale(rsName, rsImageFileName_Off, rvPos, fScale, rInputElementDescs, rsGSName, rsVSName, rsPSName),
	m_sImageFileName_On(rsImageFileName_On),
	m_sPSName(rsPSName)
{
}

CMaterial_NoneLighting_Diffuse * CFactory_UI_Simple_PosAndScale_OnOff::createMaterial_On()
{
	CMaterial_NoneLighting_Diffuse * pMaterial = new CMaterial_NoneLighting_Diffuse;
	if (!pMaterial->createObject(&CFactory_Material_NoneLighting_Diffuse(m_sName+"_Material_On",m_sPSName,m_sImageFileName_On)))
	{
		delete pMaterial;
		pMaterial = nullptr;
	}
	return pMaterial;
}
