#pragma once

#ifndef HEADER_UI_LAUNCHGUIDE_PARABOLA
#define HEADER_UI_LAUNCHGUIDE_PARABOLA

#include"Interface_UI.h"
#include"..\Module_Object_Common\Painter_Common.h"



//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------
class IFactory_UI_LaunchGuide_Parabola {
public:
	virtual ~IFactory_UI_LaunchGuide_Parabola() = 0 {}

	virtual IFactory_Painter_Curve * getFactory_Curve() = 0;
	virtual IFactory_Painter_CirclePlate * getFactory_CirclePlate() = 0;
};
// IFactory_Painter_Curve ���

//----------------------------------- Class -------------------------------------------------------------------------------------

class CUI_LaunchGuide_Parabola :	public IUserInterface {
private: 
	CPainter_Curve m_CurvePainter; 
	CPainter_CirclePlate m_CirclePlatePainter;

	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR;

public:
	CUI_LaunchGuide_Parabola()  {}
	virtual ~CUI_LaunchGuide_Parabola() {} 

public:
	//override IUserInterface
	virtual void enable() { m_CurvePainter.enable(); m_CirclePlatePainter.enable(); }
	virtual void disable() { m_CurvePainter.disable(); m_CirclePlatePainter.disable(); }
	virtual const bool isEnable()const { return m_CurvePainter.isEnable() && m_CirclePlatePainter.isEnable(); }

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_Flag; }

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject * pMsg) {  }

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return true; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const
	{ 
		m_CurvePainter.drawOnDX(pRendererState);
		m_CirclePlatePainter.drawOnDX(pRendererState);
	}

	void setLaunchGuide(std::vector<DirectX::XMFLOAT3> &&rvrvVertices) 
	{
		m_CirclePlatePainter.setTarget(rvrvVertices.back());
		m_CurvePainter.setCurve(std::move(rvrvVertices)); 
	}

	bool createObjects(IFactory_UI_LaunchGuide_Parabola *pFactory)
	{
		if (!m_CurvePainter.createObject(pFactory->getFactory_Curve()))	return false;
		if (!m_CirclePlatePainter.createObject(pFactory->getFactory_CirclePlate()))	return false;
		return true;
	}
};
 

#endif