#pragma once

#ifndef HEADER_UI_GAUGE
#define HEADER_UI_GAUGE

#include"Interface_UI.h"
#include "..\Module_Object\GameObject_ModelBase.h"
#include "..\Module_Object_GameModel\GameModel.h"
#include "..\Module_Object_GameModel\Mesh.h"
#include "..\Module_Object_GameModel\Material.h"


//���漱��
struct ID3D11DepthStencilState;
struct ID3D11DepthStencilView;
struct ID3D11BlendState;
struct ID3D11Buffer;
class CMesh_UI_Gauge;

struct Type_UI_Gauge 
{
	DirectX::XMFLOAT2 m_vPos; char dummy1[8];
	float m_fScale; char dummy2[12];
	float m_fGauge; char dummy3[12];
};


//----------------------------------- CreatorInterface -------------------------------------------------------------------------------------

class IFactory_Mesh_UI_Gauge : public IFactory_Mesh_GS{
public:
	virtual ~IFactory_Mesh_UI_Gauge()=0{}

	virtual std::vector<ID3D11Buffer *> createCBuffers()=0;
	virtual ID3D11ShaderResourceView *	createDiffuseMap()=0;
	virtual Type_UI_Gauge getGauge() = 0;

};
class IFactory_UI_Gauge {
public:
	virtual ~IFactory_UI_Gauge() = 0 {}


	virtual CMesh_UI_Gauge * createMesh() = 0;
	virtual CMaterial_NoneLighting_Diffuse * createMaterial()=0;
	virtual ID3D11DepthStencilState *	createStencilState()=0;
	virtual ID3D11DepthStencilView *	createStencilView()=0;
	virtual ID3D11Texture2D *				createStencilBuffer()=0;

};


//----------------------------------- Class -------------------------------------------------------------------------------------

class CMesh_UI_Gauge : public CMesh_GS {
private:
	std::vector<ID3D11Buffer *> m_pCBuffers;
	ID3D11ShaderResourceView * m_pDiffuseMap;

	Type_UI_Gauge m_Guage;

public:
	CMesh_UI_Gauge();
	virtual ~CMesh_UI_Gauge() {}

private:
	//override AMaterial
	virtual void setOnDX_ShaderResource(CRendererState_D3D *pRendererState)const;

public:
	void updateOnDX(CRendererState_D3D *pRendererState)const;

	bool createObject(IFactory_Mesh_UI_Gauge *pFactory);

	void setGauge(const float fGauge) { m_Guage.m_fGauge = fGauge; }
};

class CUI_Gauge :	public IUserInterface {

private:
	CMesh_UI_Gauge * m_pMesh;
	CMaterial_NoneLighting_Diffuse * m_pMaterial;
	float m_fGauge;
	ID3D11DepthStencilState *	m_pStencilState;
	ID3D11DepthStencilView *	m_pStencilView;
	ID3D11Texture2D * m_pStencilBuffer;
	bool m_bEnable;
	
	mutable bool m_bUpdate;
//	ID3D11BlendState *m_pBlendState;

	static const FLAG_8 s_Flag = FLAG_OBJECTTYPE_DRAWALBE | FLAG_OBJECTTYPE_DRAWALBE_CLEAR;

public:
	CUI_Gauge();
	virtual ~CUI_Gauge() {  }

public:
	//override IUserInterface
	virtual void enable() { m_bEnable = true; }
	virtual void disable() { m_bEnable = false; }
	virtual const bool isEnable()const { return m_bEnable; }

	//ovdrride IGameObject
	virtual const FLAG_8 getTypeFlag() const { return s_Flag; }

	//override RIGameObject_HandleMsg
	virtual void handleMsg(IMsg_GameObject * pMsg);

	//override RIGameObject_Drawable
	virtual const  bool isClear() const { return true; }
	virtual void drawOnDX(CRendererState_D3D *pRendererState)const;

	bool createObject(IFactory_UI_Gauge *pFactory);

	void setGauge(const float fGauge) { m_pMesh->setGauge(fGauge); m_bUpdate = true; }
};

#endif