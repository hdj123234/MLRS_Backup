#pragma once
#ifndef HEADER_FACTORY_UI_SCRIPTMANAGER
#define HEADER_FACTORY_UI_SCRIPTMANAGER

#include"UI_ScriptManager.h"
#include"Factory_UI_Simple_ScreenBase.h"

struct CScriptData{
	std::string m_sImageFileName;
	DirectX::XMFLOAT2 m_vPos;
	float m_fScale;
};

class CFactory_UI_ScriptManager : virtual public IFactory_UI_ScriptManager{  
private:
	const std::string m_sName;
	const std::vector<CScriptData> m_ScriptDatas;
	const float m_fEndTime;
	const std::vector<D3D11_INPUT_ELEMENT_DESC> m_InputElementDescs;
	std::vector<CUI_Simple_ScreenBase *> m_Retval;
public:
	CFactory_UI_ScriptManager(	const std::string &rsName,
								const std::vector<CScriptData>&rScriptDatas,
								const float fEndTime,
								const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs)
		:m_sName(rsName),
		m_ScriptDatas(rScriptDatas),
		m_fEndTime(fEndTime),
		m_InputElementDescs(rInputElementDescs)
	{	}
	virtual ~CFactory_UI_ScriptManager(){}


	virtual const float getEndTime() { return m_fEndTime; }
	virtual std::vector<CUI_Simple_ScreenBase *>&& getScripts() 
	{
		const unsigned int nNumberOfScript = static_cast<unsigned int>(m_ScriptDatas.size());

		for (auto i = 0U; i < nNumberOfScript; ++i)
		{
			CUI_Simple_ScreenBase *pScript = new CUI_Simple_ScreenBase();
			if (!pScript->createObject(&CFactory_UI_Simple_PosAndScale(m_sName+"_"+std::to_string(i+1),
				m_ScriptDatas[i].m_sImageFileName,
				m_ScriptDatas[i].m_vPos,
				m_ScriptDatas[i].m_fScale,
				m_InputElementDescs)))
				abort();
			m_Retval.push_back(pScript);
		}

		return std::move(m_Retval);
	}
};


#endif