#include"stdafx.h"
#include "Factory_UI_Gauge.h"


#include"..\Module_Object_GameModel\Factory_Material.h"

#include"..\Module_DXComponent\Factory_DXShader.h"
#include"..\Module_DXComponent\Factory_DXComponent.h"
#include"..\Module_DXComponent\Factory_DXShaderResourceView_FromData.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"
#include"..\Module_Platform_DirectX11\DXUtility.h"

#include"..\MyUtility.h"
#include"..\MyINI.h"


using namespace DirectX;


//----------------------------------- CFactory_UI_Gauge -------------------------------------------------------------------------------------


CFactory_UI_Gauge::CFactory_UI_Gauge(
	const std::string & rsName,
	const DirectX::XMFLOAT2 &rvPos,
	const float fScale,
	const std::string &rsImageFileName,
	const std::string &rsVSName,
	const std::string &rsGSName, 
	const std::string &rsPSName)
	: m_sName(rsName),
	m_vPos(rvPos),
	m_fScale(fScale),
	m_sImageFileName	(rsImageFileName),
	m_sVSName			(rsVSName),
	m_sGSName			(rsGSName),
	m_sPSName			(rsPSName),
	m_pStencilBuffer(nullptr),
	m_pStencilView(nullptr)
{
}

bool CFactory_UI_Gauge::createStencilView_RealProcessing()
{
	ID3D11Device *pDevice = CGlobalDirectXDevice::getDevice();

	const std::string sName = "DSB_" + m_sName;

	//DepthStencilBuffer ����
	if (CGlobalDirectXStorage::checkTexture2D(sName))
		m_pStencilBuffer = CGlobalDirectXStorage::getTexture2D(sName);
	else
	{
		if (FAILED(pDevice->CreateTexture2D(&DepthStencilBufferDesc_Default(), NULL, &m_pStencilBuffer)))
		{
			MYLOGCAT(MYLOGCAT_ERROR_DIRECTX, "CFactory_DepthStencilView.build(), CreateTexture2D()");
			return false;
		}
		CGlobalDirectXStorage::setTexture2D(sName, m_pStencilBuffer);
	}
	m_pStencilView =  CFactory_DepthStencilView_FromTexture2D("DSV_" + m_sName, m_pStencilBuffer, DXGI_FORMAT::DXGI_FORMAT_D24_UNORM_S8_UINT).createDepthStencilView();
	if (!m_pStencilView)	return false;

	initializeStencilBuffer();
	return true;
}

void CFactory_UI_Gauge::initializeStencilBuffer()
{
	auto pDC = CGlobalDirectXDevice::getDeviceContext();
	D3D11_DEPTH_STENCILOP_DESC depthStencilOPDesc;
	ZeroMemory(&depthStencilOPDesc, sizeof(depthStencilOPDesc));
	depthStencilOPDesc.StencilDepthFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
	depthStencilOPDesc.StencilFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_REPLACE;
	depthStencilOPDesc.StencilPassOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_REPLACE;
	depthStencilOPDesc.StencilFunc = D3D11_COMPARISON_ALWAYS;
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.DepthEnable = false;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilWriteMask = 0xff;
	depthStencilDesc.StencilReadMask = 0xff;
	depthStencilDesc.FrontFace = depthStencilOPDesc;
	depthStencilDesc.BackFace = StencilOPDesc_Disable();
	auto pStencilState = CFactory_DepthStencilState("DSS_Temp", depthStencilDesc).createDepthStencilState();


	auto pVS =  CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
	auto pGS =  CFactory_GeometryShader(m_sGSName, m_sGSName).createShader();
	auto pPS =  CFactory_PixelShader(m_sPSName, m_sPSName).createShader();
	auto pRS = factory_RasterizerState_Default.createRasterizerState();
	auto pSRV =  CFactory_ShaderResourceView_FromTextureFile("SRV_" + m_sName, m_sImageFileName).createShaderResourceView();
	auto pSS =  factory_SamplerState_Default.createSamplerState();
	auto pCB1 = factory_CB_Matrix_Projection.createBuffer();
	auto pCB2 =  CFactory_ConstBuffer< Type_UI_Gauge>("CB_" + m_sName, 1).createBuffer();
	auto pRTV = factory_RTV_BackBuffer.createRenderTargetView();

	Type_UI_Gauge data;
	data.m_fGauge = 1;
	data.m_vPos = m_vPos;
	data.m_fScale = m_fScale;

	CBufferUpdater::update(pDC, pCB2, data);
	FLOAT color[4] = { 1,1,1,1 };
	pDC->ClearRenderTargetView(pRTV, color);
	pDC->ClearDepthStencilView(m_pStencilView, D3D11_CLEAR_FLAG::D3D11_CLEAR_STENCIL, 0, 0x00);
	pDC->VSSetShader(pVS, nullptr, 0);
	pDC->HSSetShader(nullptr, nullptr, 0);
	pDC->DSSetShader(nullptr, nullptr, 0);
	pDC->GSSetShader(pGS, nullptr, 0);
	pDC->GSSetConstantBuffers(0, 1, &pCB1);
	pDC->GSSetConstantBuffers(1, 1, &pCB2);
	pDC->GSSetShaderResources(0, 1, &pSRV);
	pDC->RSSetState(pRS);
	pDC->RSSetViewports(1, &DXViewport_Default());
	pDC->PSSetShader(pPS, nullptr, 0);
	pDC->PSSetSamplers(0, 1, &pSS);
	pDC->PSSetShaderResources(0, 1, &pSRV);
	pDC->OMSetRenderTargets(1, &pRTV, m_pStencilView);
	pDC->OMSetDepthStencilState(pStencilState, 0xffffffff);
	pDC->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	pDC->Draw(1, 0);


	CGlobalDirectXStorage::deleteDepthStencilState("DSS_Temp");

}

CMesh_UI_Gauge * CFactory_UI_Gauge::createMesh()
{
	CMesh_UI_Gauge *pMesh = new CMesh_UI_Gauge;
	if (!pMesh->createObject(this))
	{
		delete pMesh;
		pMesh = nullptr;
	}
	return pMesh;
}

CMaterial_NoneLighting_Diffuse * CFactory_UI_Gauge::createMaterial()
{
	CMaterial_NoneLighting_Diffuse *pMaterial= new CMaterial_NoneLighting_Diffuse;
	if (!pMaterial->createObject(this))
	{
		delete pMaterial;
		pMaterial = nullptr;
	}
	return pMaterial;
}

ID3D11DepthStencilState * CFactory_UI_Gauge::createStencilState()
{
	D3D11_DEPTH_STENCILOP_DESC depthStencilOPDesc;
	ZeroMemory(&depthStencilOPDesc, sizeof(depthStencilOPDesc));
	depthStencilOPDesc.StencilDepthFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
	depthStencilOPDesc.StencilFailOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
	depthStencilOPDesc.StencilPassOp = D3D11_STENCIL_OP::D3D11_STENCIL_OP_KEEP;
	depthStencilOPDesc.StencilFunc = D3D11_COMPARISON_EQUAL;
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.DepthEnable = false;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.StencilReadMask = 0xff;
	depthStencilDesc.FrontFace = depthStencilOPDesc;
	depthStencilDesc.BackFace = StencilOPDesc_Disable();
	return CFactory_DepthStencilState("DSS_"+ m_sName, depthStencilDesc).createDepthStencilState();
}

ID3D11DepthStencilView * CFactory_UI_Gauge::createStencilView()
{
	if (!m_pStencilBuffer)
		if (!createStencilView_RealProcessing())
			return nullptr;
	return CFactory_DepthStencilView_FromTexture2D("DSV_" + m_sName, m_pStencilBuffer, DXGI_FORMAT_D24_UNORM_S8_UINT).createDepthStencilView();
}

ID3D11Texture2D * CFactory_UI_Gauge::createStencilBuffer()
{
	if (!m_pStencilBuffer)
		if (!createStencilView_RealProcessing())
			return nullptr;
	return m_pStencilBuffer;
}

Type_UI_Gauge CFactory_UI_Gauge::getGauge()
{
	Type_UI_Gauge retval;
	retval.m_vPos = m_vPos;
	retval.m_fScale = m_fScale;
	retval.m_fGauge = 1;
	return retval;
}

ID3D11VertexShader * CFactory_UI_Gauge::createVertexShader()
{
	return CFactory_VertexShader(m_sVSName, m_sVSName).createShader();
}

ID3D11GeometryShader * CFactory_UI_Gauge::createGeometryShader()
{
	return CFactory_GeometryShader(m_sGSName, m_sGSName).createShader();
}

std::vector<ID3D11Buffer*> CFactory_UI_Gauge::createCBuffers()
{
	auto pCBuffer_Projection = factory_CB_Matrix_Projection.createBuffer();
	auto pCBuffer_Gauge = CFactory_ConstBuffer< Type_UI_Gauge>("CB_" + m_sName, 1).createBuffer();
	return std::vector<ID3D11Buffer*>{pCBuffer_Projection , pCBuffer_Gauge};
}

ID3D11PixelShader * CFactory_UI_Gauge::createPixelShader()
{
	return CFactory_PixelShader(m_sPSName, m_sPSName).createShader();
}

ID3D11ShaderResourceView * CFactory_UI_Gauge::createDiffuseMap()
{
	return CFactory_ShaderResourceView_FromTextureFile("SRV_"+m_sName,m_sImageFileName).createShaderResourceView();
}

ID3D11SamplerState * CFactory_UI_Gauge::createSamplerState()
{
	return factory_SamplerState_Default.createSamplerState();
}
