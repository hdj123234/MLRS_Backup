#include "stdafx.h"
#include "UIManager.h"

#include"..\Module_Renderer\RendererState_D3D.h"
#include"..\Module_DXComponent\Factory_DXComponent_Common.h"

//----------------------------------- AFactory_UIManager -------------------------------------------------------------------------------------

ID3D11DepthStencilState * AFactory_UIManager::createDepthStencilState()
{
	return factory_DepthStencilState_Disable.createDepthStencilState();
}

ID3D11BlendState * AFactory_UIManager::createBlendState()
{
	return factory_BlendState_Default.createBlendState();
}


//----------------------------------- AUIManager -------------------------------------------------------------------------------------

void AUIManager::updateOnDX(CRendererState_D3D * pRendererState) const
{
	pRendererState->setDepthStencilState(m_pDepthStencilState);
	pRendererState->setBlend(m_pBlendState);
	for (auto pData : m_pUIs)
		if (pData->getTypeFlag()&FLAG_OBJECTTYPE_BEMUST_RENEWAL_FOR_DRAW)
			dynamic_cast<RIGameObject_BeMust_RenewalForDraw *>(pData)->updateOnDX(pRendererState);
}

void AUIManager::drawOnDX(CRendererState_D3D * pRendererState) const
{
	for (auto pData : m_pUIs)
		pData->drawOnDX(pRendererState);
}

std::vector<IMsg*> AUIManager::animateObject(const float fElapsedTime)
{
	std::vector<IMsg*>	retval;
	for (auto pData : m_pUIs)
	{
		if (pData->getTypeFlag()&FLAG_OBJECTTYPE_ANIMATE)
		{
			auto pMsgs = dynamic_cast<RIGameObject_Animate *>(pData)->animateObject(fElapsedTime);
			if(!pMsgs.empty())
				retval.insert(retval.end(), pMsgs.begin(), pMsgs.end());
		}
	}
	return retval;
}

bool AUIManager::createObject(IFactory_UIManager * pFactory)
{
	m_pDepthStencilState = pFactory->createDepthStencilState();
	m_pBlendState = pFactory->createBlendState();
	m_pUIs = pFactory->createUIs();
	return (m_pDepthStencilState&&m_pBlendState&&!m_pUIs.empty());
}

void AUIManager::relaeseObject()
{
	for (auto pData : m_pUIs)
		delete pData;
}
