#pragma once
#ifndef HEADER_FACTORY_UI_SIMPLE_SCREENBASE
#define HEADER_FACTORY_UI_SIMPLE_SCREENBASE

#include"UI_Simple_ScreenBase.h"
#include"..\Module_Object_GameModel\Factory_Mesh_HeightMap.h"
#include"..\Module_Object_GameModel\Factory_Material.h"

class AFactory_UI_Image : virtual public IFactory_UI_Simple_ScreenBase,
							virtual public IFactory_Mesh_VertexAndGS,
							virtual public IFactory_Material_NoneLighting_Diffuse{
protected:
	const std::string m_sName;
private:
	const std::string m_sImageFileName;
	const std::string m_sVSName;
	const std::string m_sGSName;
	const std::string m_sPSName;
	std::vector<D3D11_INPUT_ELEMENT_DESC> m_InputElementDescs;

public:
	AFactory_UI_Image(	const std::string &rsName,
									const std::string &rsImageFileName	,
									const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs,
									const std::string &rsVSName ,
									const std::string &rsGSName ,
									const std::string &rsPSName );
	virtual ~AFactory_UI_Image()=0{}
	//override IFactory_Mesh
	virtual ID3D11VertexShader		* createVertexShader();
	virtual const unsigned int getMaterialID() { return 0; }

	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11InputLayout	*	createInputLayout();
	virtual unsigned int			getOffset() { return 0; }
	virtual unsigned int			getNumberOfVertex() { return 1; }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST; }

	//override IFactory_Mesh_GS
	virtual ID3D11GeometryShader	* createGeometryShader();

	//override IFactory_Material
	virtual ID3D11PixelShader *createPixelShader();

	//override IFactory_Material_NoneLighting_Diffuse
	virtual ID3D11ShaderResourceView * createDiffuseMap();
	virtual ID3D11SamplerState * createSamplerState();
};


class CFactory_UI_Simple_PosAndSize : public AFactory_UI_Image {
private:
	struct VertexData_UI_Simple_PosAndSize {
		DirectX::XMFLOAT2 m_vPos;
		DirectX::XMFLOAT2 m_vSize;
	};
private:
	const DirectX::XMFLOAT2 m_vPos;
	const DirectX::XMFLOAT2 m_vSize;

public:
	CFactory_UI_Simple_PosAndSize(	const std::string &rsName,
								const std::string &rsImageFileName	,
								const DirectX::XMFLOAT2 &rvPos		,
								const DirectX::XMFLOAT2 &rvSize		,
								const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs,
								const std::string &rsVSName = "vsPosAndSizeXY",
								const std::string &rsGSName = "gsUI_ScreenBase_Size",
								const std::string &rsPSName = "psTexOnly"	);
	virtual ~CFactory_UI_Simple_PosAndSize() {}


	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11Buffer		*	createVertexBuffer();
	virtual unsigned int			getStride() { return sizeof(VertexData_UI_Simple_PosAndSize); }

};


class CFactory_UI_Simple_PosAndScale :   public AFactory_UI_Image,
										virtual public IFactory_Mesh_UI_Simple {
private:
	typedef AFactory_UI_Image SUPER;
private:
	struct VertexData_UI_Simple_PosAndScale {
		DirectX::XMFLOAT2 m_vPos;
		float m_fSize;
	};
private:
	const DirectX::XMFLOAT2 m_vPos;
	const float m_fSize;

public:
	CFactory_UI_Simple_PosAndScale(	const std::string &rsName,
									const std::string &rsImageFileName,
									const DirectX::XMFLOAT2 &rvPos,
									const float fScale,
									const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs,
									const std::string &rsGSName = "gsUI_ScreenBase_Scale_LT",
									const std::string &rsVSName = "vsPosAndScale",
									const std::string &rsPSName = "psTexOnly");
	virtual ~CFactory_UI_Simple_PosAndScale() {}

	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11Buffer		*	createVertexBuffer();
	virtual unsigned int			getStride() { return sizeof(VertexData_UI_Simple_PosAndScale); }

	//override IFactory_Mesh
	virtual ID3D11VertexShader		* createVertexShader() { return SUPER::createVertexShader(); }
	virtual const unsigned int getMaterialID() { return SUPER::getMaterialID(); }

	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11InputLayout	*	createInputLayout() { return SUPER::createInputLayout(); }
	virtual unsigned int			getOffset() { return SUPER::getOffset(); }
	virtual unsigned int			getNumberOfVertex() { return SUPER::getNumberOfVertex(); }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return SUPER::getPrimitiveTopology(); }

	//override IFactory_Mesh_GS
	virtual ID3D11GeometryShader	* createGeometryShader() { return SUPER::createGeometryShader(); }


	//override IFactory_Mesh_UI_Simple
	virtual ID3D11Buffer * createCBuffer_Projection() ;
	virtual ID3D11ShaderResourceView * createDiffuseMap() ;

	//override IFactory_UI_Simple_ScreenBase
	virtual CMesh_VertexAndGS * createMesh() ;
	virtual CMaterial_NoneLighting_Diffuse * createMaterial() ;
};

class CFactory_UI_Simple_PosAndScale_OnOff : public CFactory_UI_Simple_PosAndScale,
											virtual public IFactory_UI_Simple_ScreenBase_Swap_OnOff{
private:
	typedef CFactory_UI_Simple_PosAndScale SUPER;

private:
	const std::string m_sImageFileName_On;
	const std::string m_sPSName;


public:
	CFactory_UI_Simple_PosAndScale_OnOff(const std::string &rsName,
		const std::string &rsImageFileName_On,
		const std::string &rsImageFileName_Off,
		const DirectX::XMFLOAT2 &rvPos,
		const float fScale,
		const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs,
		const std::string &rsGSName = "gsUI_ScreenBase_Scale_LT",
		const std::string &rsVSName = "vsPosAndScale",
		const std::string &rsPSName = "psTexOnly");
	virtual ~CFactory_UI_Simple_PosAndScale_OnOff() {}

	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11Buffer		*	createVertexBuffer() { return SUPER::createVertexBuffer(); }
	virtual unsigned int			getStride() { return SUPER::getStride(); }

	//override IFactory_Mesh
	virtual ID3D11VertexShader		* createVertexShader() { return SUPER::createVertexShader(); }
	virtual const unsigned int getMaterialID() { return SUPER::getMaterialID(); }

	//override RIFactory_Mesh_VertexBuffer
	virtual ID3D11InputLayout	*	createInputLayout() { return SUPER::createInputLayout(); }
	virtual unsigned int			getOffset() { return SUPER::getOffset(); }
	virtual unsigned int			getNumberOfVertex() { return SUPER::getNumberOfVertex(); }
	virtual D3D11_PRIMITIVE_TOPOLOGY getPrimitiveTopology() { return SUPER::getPrimitiveTopology(); }

	//override IFactory_Mesh_GS
	virtual ID3D11GeometryShader	* createGeometryShader() { return SUPER::createGeometryShader(); }
	 
	//override IFactory_Mesh_UI_Simple
	virtual ID3D11Buffer * createCBuffer_Projection() { return SUPER::createCBuffer_Projection(); }
	virtual ID3D11ShaderResourceView * createDiffuseMap() { return SUPER::createDiffuseMap(); }

	//override IFactory_UI_Simple_ScreenBase
	virtual CMesh_VertexAndGS * createMesh() { return SUPER::createMesh(); }
	virtual CMaterial_NoneLighting_Diffuse * createMaterial() { return SUPER::createMaterial(); }

	//override IFactory_UI_Simple_ScreenBase_Swap_OnOff
	virtual CMaterial_NoneLighting_Diffuse * createMaterial_On() ;
};


#endif