#pragma once
#ifndef HEADER_FACTORY_UI_SWITCH
#define HEADER_FACTORY_UI_SWITCH

#include"UI_Switch.h"
#include"Factory_UI_Simple_ScreenBase.h"

struct CSwitchData_Swap{
	std::string m_sImageFileName_On;
	std::string m_sImageFileName_Off;
	DirectX::XMFLOAT2 m_vPos;
	float m_fScale;
};
struct CSwitchData_Simple {
	std::string m_sImageFileName;
	DirectX::XMFLOAT2 m_vPos;
	float m_fScale;
};
//
//template<unsigned char NumberOfSwitch>
//class CFactory_UI_Switch_Swap : virtual public IFactory_UI_Switch<NumberOfSwitch>{  
//private:
//	const std::string m_sName;
//	const std::vector<D3D11_INPUT_ELEMENT_DESC> m_InputElementDescs;
//	const std::vector<CSwitchData_Simple> m_SwitchData;
//
//	std::array<CUI_Simple_ScreenBase_Swap_OnOff *, NumberOfSwitch> m_Datas;
//	std::array<RIUserInterface_OnOff *, NumberOfSwitch> m_Retval_RI;
//	std::array<IUserInterface *, NumberOfSwitch> m_Retval;
//public:
//	CFactory_UI_Switch_Swap(	const std::string &rsName,
//						const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs,
//						const std::vector<CSwitchData_Simple>&rSwitchData)
//		:m_sName(rsName),
//		m_InputElementDescs(rInputElementDescs),
//		m_SwitchData(rSwitchData)
//	{	
//		createUIs_RealProcessing();
//	}
//	virtual ~CFactory_UI_Switch_Swap(){}
//
//	void createUIs_RealProcessing()
//	{ 
//		for (auto i = 0U; i < NumberOfSwitch; ++i)
//		{
//			m_Datas[i] = new CUI_Simple_ScreenBase_Swap_OnOff();
//			if (!m_Datas[i]->createObject(&CFactory_UI_Simple_PosAndScale_OnOff(m_sName + "_" + std::to_string(i + 1),
//				m_SwitchData[i].m_sImageFileName_On,
//				m_SwitchData[i].m_sImageFileName_Off,
//				m_SwitchData[i].m_vPos,
//				m_SwitchData[i].m_fScale,
//				m_InputElementDescs)))
//				abort();
//		}
//		for (auto i=0U;i<NumberOfSwitch;++i)
//		{
//			m_Retval_RI[i] = m_Datas[i];
//			m_Retval[i] = m_Datas[i];
//		} 
//	}
//
//	virtual std::array<RIUserInterface_OnOff *, NumberOfSwitch>&& createUIs_OnOff()
//	{
//		return std::move(m_Retval_RI);
//	}
//	virtual std::array<IUserInterface *, NumberOfSwitch>&& createUIs() 
//	{
//		return std::move(m_Retval);
//
//	}
//};

template<unsigned char NumberOfSwitch>
class AFactory_UI_Switch : virtual public IFactory_UI_Switch<NumberOfSwitch>{  
protected:
	const std::string m_sName;
	const std::vector<D3D11_INPUT_ELEMENT_DESC> m_InputElementDescs;

	std::array<RIUserInterface_OnOff *, NumberOfSwitch> m_Retval_RI;
	std::array<IUserInterface *, NumberOfSwitch> m_Retval;
public:
	AFactory_UI_Switch(	const std::string &rsName,
						const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs)
		:m_sName(rsName),
		m_InputElementDescs(rInputElementDescs) 
	{	
	}
	virtual ~AFactory_UI_Switch()=0{}

	virtual void createUIs_RealProcessing() = 0;

	virtual std::array<RIUserInterface_OnOff *, NumberOfSwitch>&& createUIs_OnOff()
	{
		return std::move(m_Retval_RI);
	}
	virtual std::array<IUserInterface *, NumberOfSwitch>&& createUIs() 
	{
		return std::move(m_Retval); 
	}
};

template<unsigned char NumberOfSwitch>
class CFactory_UI_Switch_Simple : public AFactory_UI_Switch<NumberOfSwitch> { 
private:
	const std::vector<CSwitchData_Simple> m_SwitchData;
	std::array<CUI_Simple_ScreenBase_OnOff *, NumberOfSwitch> m_Datas;
public:
	CFactory_UI_Switch_Simple(const std::string &rsName,
		const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs,
		const std::vector<CSwitchData_Simple>&rSwitchData)
		:AFactory_UI_Switch<NumberOfSwitch>(rsName, rInputElementDescs),
		m_SwitchData(rSwitchData)
	{
		this->createUIs_RealProcessing();
	}
	virtual ~CFactory_UI_Switch_Simple()  {}

	virtual void createUIs_RealProcessing()
	{
		for (auto i = 0U; i < NumberOfSwitch; ++i)
		{
			m_Datas[i] = new CUI_Simple_ScreenBase_OnOff();
			if (!m_Datas[i]->createObject(&CFactory_UI_Simple_PosAndScale(m_sName + "_" + std::to_string(i + 1),
				m_SwitchData[i].m_sImageFileName, 
				m_SwitchData[i].m_vPos,
				m_SwitchData[i].m_fScale,
				m_InputElementDescs)))
				abort();
		}
		for (auto i = 0U; i<NumberOfSwitch; ++i)
		{
			m_Retval_RI[i] = m_Datas[i];
			m_Retval[i] = m_Datas[i];
		}
	}
	 
};

template<unsigned char NumberOfSwitch>
class CFactory_UI_Switch_Swap : public AFactory_UI_Switch<NumberOfSwitch> {
private:
	const std::vector<CSwitchData_Swap> m_SwitchData;
	std::array<CUI_Simple_ScreenBase_Swap_OnOff *, NumberOfSwitch> m_Datas;
public:
	CFactory_UI_Switch_Swap(const std::string &rsName,
		const std::vector<D3D11_INPUT_ELEMENT_DESC> &rInputElementDescs,
		const std::vector<CSwitchData_Swap>&rSwitchData)
		:AFactory_UI_Switch<NumberOfSwitch>(rsName, rInputElementDescs),
		m_SwitchData(rSwitchData)
	{
		this->createUIs_RealProcessing();
	}
	virtual ~CFactory_UI_Switch_Swap() {}

	virtual void createUIs_RealProcessing()
	{
		for (auto i = 0U; i < NumberOfSwitch; ++i)
		{
			m_Datas[i] = new CUI_Simple_ScreenBase_Swap_OnOff();
			if (!m_Datas[i]->createObject(&CFactory_UI_Simple_PosAndScale_OnOff(m_sName + "_" + std::to_string(i + 1),
				m_SwitchData[i].m_sImageFileName_On,
				m_SwitchData[i].m_sImageFileName_Off,
				m_SwitchData[i].m_vPos,
				m_SwitchData[i].m_fScale,
				m_InputElementDescs)))
				abort();
		}
		for (auto i = 0U; i<NumberOfSwitch; ++i)
		{
			m_Retval_RI[i] = m_Datas[i];
			m_Retval[i] = m_Datas[i];
		}
	}

};
#endif