// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once
#define USE_DIRECTX
#define USE_DIRECTXMATH
#define USE_MODEL_OBJ
#define USE_TIMER_QUERYPERFORMANCECOUNTER
#define USE_DIRECTXTEX
 
//#define USE_TIMER_CHRONO

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.

// Windows 헤더 파일:
#define HEADER_WINDOWS
#include<Ws2tcpip.h>
#include<winsock2.h>
#include <Windows.h>
#include <mmsystem.h>
#include <windowsx.h>
#pragma comment(lib,"winmm.lib")


// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <cctype>

//STL
#define HEADER_STL
#include<iostream>
#include<vector>
#include<array>
#include<set>
#include<map>
#include<iomanip>
#include<iterator>
#include<fstream>
#include<sstream>
#include<string>
#include<algorithm>
#include <random>
#include<chrono>
#include<thread>
#include<locale>
#include<codecvt>
#include<queue>
#include<future>
#include<mutex>
#include<functional>

#define HEADER_DIRECTXTEX
#include <dxgi1_2.h>
//#include <DXGItype.h>
#include <DirectXTex\DirectXTex.h>

#define HEADER_DIRECTX
#include <DXGI.h>
#include <D3D11.h>
#include <D3DX11core.h>
#include <D3Dcompiler.h>


//#pragma comment(lib,"d3d11.lib")
//#pragma comment(lib,"d3dx11.lib")
//#pragma comment(lib,"dxgi.lib")
//#pragma comment(lib,"d3dcompiler.lib")
//#pragma comment(lib,"dxguid.lib")
//
//#ifdef _DEBUG
//#pragma comment(lib,"d3dx11d.lib")
//#pragma comment(lib,"dxerr.lib")
//#endif

// XAUDIO2
#define HEADER_XAUDIO2
 
#include<xaudio2.h>
#include<X3daudio.h> 


// OGG
#define HEADER_OGG
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>


#ifdef USE_DIRECTXMATH
#define HEADER_DIRECTXMATH
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXCollision.h>
#include <DirectXColors.h>
#endif

#define HEADER_MYLOGCAT
#include"LogCat.h"

// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.

//#pragma comment(lib,"lib/d3d11.lib")
//#pragma comment(lib,"lib/d3dcompiler.lib")
//#pragma comment(lib,"lib/dxguid.lib")
//#pragma comment(lib,"lib/dxgi.lib")
//
//#pragma comment(lib,"lib/D3DX11.lib")
//
//#ifdef _DEBUG
//#pragma comment(lib,"lib/d3dx11d.lib")
//#pragma comment(lib,"lib/dxerr.lib")
//#endif
