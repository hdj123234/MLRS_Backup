#pragma once
#ifndef HEADER_PARTLOADER_BONE
#define HEADER_PARTLOADER_BONE

#include"Type_MyGameModel.h"

#ifndef HEADER_FBXSDK
#include<fbxsdk.h>
#endif

struct CFBX_TreeNode_Skeleton {
	std::string m_sName;
	FbxSkeleton::EType m_eType;
	float m_fSize;
	std::vector<DirectX::XMFLOAT4X4> m_mTransforms_Anim;
	CFBX_TreeNode_Skeleton *m_pParent;
	std::vector<CFBX_TreeNode_Skeleton *> m_pChildren;

	CFBX_TreeNode_Skeleton()
		:m_pParent(nullptr)
	{}
	~CFBX_TreeNode_Skeleton()
	{
		for (auto data : m_pChildren)
			if (data)delete data;
	}
	void addChild(CFBX_TreeNode_Skeleton * pNode)
	{
		if (pNode) {
			m_pChildren.push_back(pNode);
			pNode->m_pParent = this;
		}
	}
	void setSize(const unsigned int n)
	{
		m_mTransforms_Anim.resize(n);
	}
};

#endif


class CPartLoader_Bone{
private:
	std::vector<CFBX_TreeNode_Skeleton*> m_pTree_Skeletons;
	CMyType_Bones m_Retval_Bones;
	std::vector<std::vector<DirectX::XMFLOAT4X4>> m_Retval_AnimTransform;

	DirectX::XMFLOAT4X4A *m_pmGlobalTransform;	//mAxis * mMatrix_FBXGlobal
	unsigned int m_nEndFrame;

public:
	CPartLoader_Bone();
	~CPartLoader_Bone();

private:
	void setAnimStack(FbxAnimStack * pAnimStack);
	void visitNode(FbxNode * pNode);
//	void addNewTree_Skeletons(FbxSkeleton *pSkeleton);
	void visitSkeleton(CFBX_TreeNode_Skeleton *pParent,FbxSkeleton *pSkeleton);
	void convertBones(std::map<std::string, DirectX::XMFLOAT4X4> &rmSkins);

public:
	const bool loadBoneData(	FbxScene *pScene,
								std::map<std::string, DirectX::XMFLOAT4X4> &rmSkins,
								const DirectX::XMFLOAT4X4 &rmGlobalTransform );
	CMyType_Bones getBones() { return m_Retval_Bones; }
	std::vector<std::vector<DirectX::XMFLOAT4X4>> getAnimTransform() { return m_Retval_AnimTransform; }
	void clear();
};

