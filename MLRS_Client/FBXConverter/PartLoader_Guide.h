#pragma once
#ifndef HEADER_PARTLOADER_GUIDE
#define HEADER_PARTLOADER_GUIDE

#include"Type_MyGameModel.h"

class CPartLoader_Guide{
private:
	std::map<std::string, EMyType_Guide> m_GuideMap;
	const std::map<std::string, DirectX::XMFLOAT4X4> *m_pmPoses;
	DirectX::XMFLOAT4X4 m_mGlobalTransform;
	CMyType_Guides m_Retval;

public:
	CPartLoader_Guide();
	~CPartLoader_Guide();

private:
	void setGuideMap(const EMyType_ModelType eModelType);

	void CPartLoader_Guide::visitNode(FbxNode * pNode);
	void CPartLoader_Guide::visitMesh(FbxMesh * pMesh);

public:
	CMyType_Guides loadGuide(	const EMyType_ModelType eModelType,
								FbxScene *pScene,
								const std::map<std::string, DirectX::XMFLOAT4X4>& rmPoses,
								const DirectX::XMFLOAT4X4 &rmGlobalTransform);
	void clear();
};

#endif