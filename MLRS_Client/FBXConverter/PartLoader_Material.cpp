#include "stdafx.h"
#include "PartLoader_Material.h"

#include"..\MLRS_Client\MyUtility.h"


CMyType_Material CPartLoader_Material::getMaterial(FbxSurfaceMaterial * pMaterial)
{
	using namespace DirectX;
	if (!pMaterial)	return CMyType_Material();

	CMyType_Material material;

	if (pMaterial->ClassId.Is(FbxSurfacePhong::ClassId))
		getMaterialInfo_Phong(material,static_cast<FbxSurfacePhong *>(pMaterial));
	if (pMaterial->GetClassId().Is(FbxSurfaceLambert::ClassId))
		getMaterialInfo_Lambert(material, static_cast<FbxSurfaceLambert *>(pMaterial));
	getMaterialInfo_Maps(material, pMaterial);

	return material;
}

void CPartLoader_Material::getMaterialInfo_Lambert(CMyType_Material & out_rMaterial, FbxSurfaceLambert * pMaterial_Lambert)
{

	FbxDouble3 ambient = pMaterial_Lambert->Ambient.Get();
	FbxDouble  ambientFactor = pMaterial_Lambert->AmbientFactor.Get();
	FbxDouble3 diffuse = pMaterial_Lambert->Diffuse.Get();
	FbxDouble  diffuseFactor = pMaterial_Lambert->DiffuseFactor.Get();
	FbxDouble3 emissive = pMaterial_Lambert->Emissive.Get();
	FbxDouble  emissiveFactor = pMaterial_Lambert->EmissiveFactor.Get();

	out_rMaterial.m_sName = pMaterial_Lambert->GetName();
	out_rMaterial.m_vAmbient = { static_cast<float>(ambient[0]), static_cast<float>(ambient[1]), static_cast<float>(ambient[2]), static_cast<float>(ambientFactor) };
	out_rMaterial.m_vDiffuse = { static_cast<float>(diffuse[0]), static_cast<float>(diffuse[1]), static_cast<float>(diffuse[2]), static_cast<float>(diffuseFactor) };
	out_rMaterial.m_vEmissive = { static_cast<float>(emissive[0]), static_cast<float>(emissive[1]), static_cast<float>(emissive[2]), static_cast<float>(emissiveFactor) };

}

void CPartLoader_Material::getMaterialInfo_Phong(CMyType_Material & out_rMaterial, FbxSurfacePhong * pMaterial_Phong)
{

	FbxDouble3 ambient = pMaterial_Phong->Ambient.Get();
	FbxDouble ambientFactor = pMaterial_Phong->AmbientFactor.Get();
	FbxDouble3 diffuse = pMaterial_Phong->Diffuse.Get();
	FbxDouble diffuseFactor = pMaterial_Phong->DiffuseFactor.Get();
	FbxDouble3 emissive = pMaterial_Phong->Emissive.Get();
	FbxDouble emissiveFactor = pMaterial_Phong->EmissiveFactor.Get();
	FbxDouble3 specular = pMaterial_Phong->Specular.Get();
	FbxDouble specularFactor = pMaterial_Phong->SpecularFactor.Get();
	FbxDouble shininess = pMaterial_Phong->Shininess.Get();
	FbxDouble3 reflection = pMaterial_Phong->Reflection.Get();
	FbxDouble reflectionFactor = pMaterial_Phong->ReflectionFactor.Get();

	out_rMaterial.m_sName = pMaterial_Phong->GetName();
	out_rMaterial.m_vAmbient = { static_cast<float>(ambient[0]), static_cast<float>(ambient[1]), static_cast<float>(ambient[2]), static_cast<float>(ambientFactor) };
	out_rMaterial.m_vDiffuse = { static_cast<float>(diffuse[0]), static_cast<float>(diffuse[1]), static_cast<float>(diffuse[2]), static_cast<float>(diffuseFactor) };
	out_rMaterial.m_vSpecular = { static_cast<float>(specular[0]), static_cast<float>(specular[1]), static_cast<float>(specular[2]), static_cast<float>(specularFactor) };
	out_rMaterial.m_vEmissive = { static_cast<float>(emissive[0]), static_cast<float>(emissive[1]), static_cast<float>(emissive[2]), static_cast<float>(emissiveFactor) };
	out_rMaterial.m_vReflection = { static_cast<float>(reflection[0]), static_cast<float>(reflection[1]), static_cast<float>(reflection[2]), static_cast<float>(reflectionFactor) };
	out_rMaterial.m_fShininess = static_cast<float>(shininess);
}

void CPartLoader_Material::getMaterialInfo_Maps(CMyType_Material & out_rMaterial, FbxSurfaceMaterial * pMaterial)
{
	FbxProperty prop;
	EMyType_Map mapType;

	prop = pMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);
	mapType = EMyType_Map::eDiffuse;
	if (unsigned int nNumberOfSrcObject
		= prop.GetSrcObjectCount<FbxFileTexture>())
	{
		FbxFileTexture* pTexture
			= static_cast<FbxFileTexture *>(prop.GetSrcObject<FbxFileTexture>());
		out_rMaterial.m_Maps[mapType] = CUtility_Path::getFileNameOnly( pTexture->GetFileName());
	}

	prop = pMaterial->FindProperty(FbxSurfaceMaterial::sNormalMap);
	mapType = EMyType_Map::eNormal;
	if (unsigned int nNumberOfSrcObject
		= prop.GetSrcObjectCount<FbxFileTexture>())
	{
		FbxFileTexture* pTexture
			= static_cast<FbxFileTexture *>(prop.GetSrcObject<FbxFileTexture>());
		out_rMaterial.m_Maps[mapType] = CUtility_Path::getFileNameOnly(pTexture->GetFileName());
	}
}
 
std::vector<CMyType_Material> CPartLoader_Material::getMaterials(FbxScene * pScene)
{
	std::vector<CMyType_Material> retval;
	const unsigned  int nNumberOfMaterial = pScene->GetMaterialCount();
	retval.reserve(nNumberOfMaterial);
	for (unsigned int i = 0; i < nNumberOfMaterial; i++)
		retval.push_back(getMaterial(pScene->GetMaterial(i)));
	return retval;
}
