#pragma once
#ifndef HEADER_PARTLOADER_MESH
#define HEADER_PARTLOADER_MESH

#include"Type_MyGameModel.h"

#ifndef HEADER_FBXSDK
#include<fbxsdk.h>
#endif

class CPartLoader_Mesh{
private:
	struct CFBX_Vertex {
		DirectX::XMFLOAT3 m_vPos;
		std::map<std::string,float> m_BoneWeight;
	};
	struct CFBX_Mesh {
		std::string m_sName;
		std::vector<CFBX_Vertex> m_Vertices;
		std::vector<DirectX::XMFLOAT2> m_vUVs;
		std::vector<CMyType_VertexIndex> m_Indices;
		std::vector<std::string> m_sMaterials;
		std::set<std::string> m_sMaterialNames;
		std::array<DirectX::XMFLOAT3, 2> m_vAABB;
	};
private:
	std::map<std::string, DirectX::XMFLOAT4X4> m_mPoses;
	std::map<std::string, DirectX::XMFLOAT4X4> m_mSkins;
	std::vector<CMyType_Mesh> m_Meshs;
	std::vector<CFBX_Mesh> m_FBXMeshs;
	DirectX::XMFLOAT4X4 m_mGlobalTransform;	//axis * global

	std::vector<CMyType_Mesh> m_Retval;

public:
	CPartLoader_Mesh();
	~CPartLoader_Mesh();

private:
	void addPose(FbxPose * pPose);
	void visitNode(FbxNode * pNode);
	void visitMesh(FbxMesh *pMesh);
	bool checkFilter(const std::string &rsName)const;

public:
	bool loadMesh(FbxScene *pScene, const DirectX::XMFLOAT4X4 &rmGlobalTransform);
	void convertMeshs(const CMyType_Bones &rBones, std::vector<CMyType_Material> &inout_rMaterials);
	std::vector<CMyType_Mesh> getMeshs();
	std::map<std::string, DirectX::XMFLOAT4X4> getSkin();
	const std::map<std::string, DirectX::XMFLOAT4X4>& getPoses() { return m_mPoses; }

	void clear();
};

#endif