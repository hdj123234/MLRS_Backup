#pragma once
#ifndef HEADER_PARTLOADER_MATERIAL
#define HEADER_PARTLOADER_MATERIAL

#include"Type_MyGameModel.h"

#ifndef HEADER_FBXSDK
#include<fbxsdk.h>
#endif

class CPartLoader_Material{ 
public:
	CPartLoader_Material() {}
	~CPartLoader_Material() {}

private:
	CMyType_Material getMaterial(FbxSurfaceMaterial *pMaterial);
	void getMaterialInfo_Lambert(CMyType_Material &out_rMaterial, FbxSurfaceLambert *pMaterial_Lambert);
	void getMaterialInfo_Phong(CMyType_Material &out_rMaterial, FbxSurfacePhong *pMaterial_Phong);
	void getMaterialInfo_Maps(CMyType_Material &out_rMaterial, FbxSurfaceMaterial *pMaterial);

public: 
	std::vector<CMyType_Material> getMaterials(FbxScene *pScene);
	void clear() {  }
};

#endif