#include "stdafx.h"
#include "Writer_GameModel.h"


CWriter_GameModel::CWriter_GameModel()
{
}


CWriter_GameModel::~CWriter_GameModel()
{
}

const bool CWriter_GameModel::writeMeshs(std::ofstream & rofs, const std::vector<CMyType_Mesh>& rMeshs)
{
	using namespace std;

	rofs << rMeshs.size() << endl;
	for (auto &rMesh : rMeshs)
	{
		rofs << rMesh.m_sName << endl;
		rofs << rMesh.m_vAABB[0].x << " " << rMesh.m_vAABB[0].y << " " << rMesh.m_vAABB[0].z << " "
			 << rMesh.m_vAABB[1].x << " " << rMesh.m_vAABB[1].y << " " << rMesh.m_vAABB[1].z << endl;

		rofs << rMesh.m_Vertices.size() << endl;
		for (auto &rVertex : rMesh.m_Vertices)
			rofs	<< rVertex.m_vPosition.x << " " << rVertex.m_vPosition.y << " " << rVertex.m_vPosition.z << " "
					<< rVertex.m_vBoneIndex.x << " " << rVertex.m_vBoneIndex.y << " " << rVertex.m_vBoneIndex.z << " " << rVertex.m_vBoneIndex.w << " "
					<< rVertex.m_vWeights.x << " " << rVertex.m_vWeights.y << " " << rVertex.m_vWeights.z << " " << rVertex.m_vWeights.w << endl;
		
		rofs << rMesh.m_vUVs.size() << endl;
		for (auto &rUV : rMesh.m_vUVs)
			rofs << rUV.x << " " << rUV.y << endl;

		rofs << rMesh.m_nMaterialIndices.size() << endl;
		for (auto &rMaterialIndex : rMesh.m_nMaterialIndices)
			rofs << rMaterialIndex << endl;

		rofs << rMesh.m_Indices.size() << endl;
		for (auto &rIndex : rMesh.m_Indices)
			rofs << rIndex.m_nVertexIndex<<" " << rIndex.m_nUVIndex << " " 
			<< rIndex.m_vNormal.x << " " << rIndex.m_vNormal.y << " " << rIndex.m_vNormal.z << " " 
			<< rIndex.m_vTangent.x << " " << rIndex.m_vTangent.y << " " << rIndex.m_vTangent.z << " " << endl;
	}
	return true;
}

const bool CWriter_GameModel::writeBone(std::ofstream & rofs, const CMyType_Bones & rBones)
{
	using namespace std;

	rofs << rBones.getNumberOfBone() << endl;
	for (auto &rBone : rBones.getContainer())
	{
		rofs << rBone.first << endl<<rBone.second.m_nParentIndex << endl;
		auto element = rBone.second.m_mTransform_ToLocal.m;
		rofs	<< element[0][0] << " " << element[0][1] << " " << element[0][2] << " " << element[0][3] << " " 
				<< element[1][0] << " " << element[1][1] << " " << element[1][2] << " " << element[1][3] << " " 
				<< element[2][0] << " " << element[2][1] << " " << element[2][2] << " " << element[2][3] << " " 
				<< element[3][0] << " " << element[3][1] << " " << element[3][2] << " " << element[3][3] << endl;
	}
	return true;
}

const bool CWriter_GameModel::writeAnimTransforms(std::ofstream & rofs, const std::vector<std::vector<DirectX::XMFLOAT4X4>>& rAnimTransforms)
{
	using namespace std;

	if (rAnimTransforms.empty())
	{
		rofs << 0 << endl;
		return true;
	}
	
	rofs << rAnimTransforms.size() << " " << rAnimTransforms[0].size() << endl;
	for (auto &rFrame : rAnimTransforms)
	{
		for (auto &rMatrix_Bone : rFrame)
		{
			auto element = rMatrix_Bone.m;
			rofs	<< element[0][0] << " " << element[0][1] << " " << element[0][2] << " " << element[0][3] << " "
					<< element[1][0] << " " << element[1][1] << " " << element[1][2] << " " << element[1][3] << " "
					<< element[2][0] << " " << element[2][1] << " " << element[2][2] << " " << element[2][3] << " "
					<< element[3][0] << " " << element[3][1] << " " << element[3][2] << " " << element[3][3] << endl;
		}
	}
	return true;
}

const bool CWriter_GameModel::writeMaterials(std::ofstream & rofs, const std::vector<CMyType_Material>& rMaterials)
{
	using namespace std;
	
	rofs << rMaterials.size() <<  endl;
	for (auto &rMaterial : rMaterials)
	{
		rofs << rMaterial.m_sName << endl;
		rofs <<  rMaterial.m_bPhong << endl;
		rofs << rMaterial.m_vAmbient.x << " " << rMaterial.m_vAmbient.y << " " << rMaterial.m_vAmbient.z << " " << rMaterial.m_vAmbient.w << endl;
		rofs << rMaterial.m_vDiffuse.x << " " << rMaterial.m_vDiffuse.y << " " << rMaterial.m_vDiffuse.z << " " << rMaterial.m_vDiffuse.w << endl;
		rofs << rMaterial.m_vSpecular.x << " " << rMaterial.m_vSpecular.y << " " << rMaterial.m_vSpecular.z << " " << rMaterial.m_vSpecular.w << endl;
		rofs << rMaterial.m_vEmissive.x << " " << rMaterial.m_vEmissive.y << " " << rMaterial.m_vEmissive.z << " " << rMaterial.m_vEmissive.w << endl;
		rofs << rMaterial.m_vReflection.x << " " << rMaterial.m_vReflection.y << " " << rMaterial.m_vReflection.z << " " << rMaterial.m_vReflection.w << endl;
		rofs << rMaterial.m_fShininess << endl;
		rofs << rMaterial.m_Maps.size() << endl;
		for (auto &rMap : rMaterial.m_Maps)
			rofs << rMap.first << endl << rMap.second << endl;
	}
	return true;
}

const bool CWriter_GameModel::writeGuides(std::ofstream & rofs, const CMyType_Guides & rGuides)
{
	using namespace std;

	rofs << rGuides.m_eModelType << endl;
	rofs << rGuides.m_GuideMap.size() << endl;
	for (auto &rData : rGuides.m_GuideMap)
	{
		rofs << rData.first << endl;
		rofs << rData.second.m_vPos.x<<" " << rData.second.m_vPos.y << " " << rData.second.m_vPos.z << endl;
	} 
	return true;
}

const bool CWriter_GameModel::write(const std::string & rsFileName, CMyType_GameModel & rGameModel, HWND hProgressBar)
{
	std::ofstream ofs(rsFileName,std::ios::binary);

	if (ofs.fail())return false;

	writeMeshs			(ofs, rGameModel.m_Meshs);
	SendMessage(hProgressBar, PBM_SETPOS, 70, 0);
	writeBone			(ofs, rGameModel.m_Bones);
	SendMessage(hProgressBar, PBM_SETPOS, 80, 0);
	writeAnimTransforms	(ofs, rGameModel.m_AnimTransform);
	SendMessage(hProgressBar, PBM_SETPOS, 90, 0);
	writeMaterials		(ofs, rGameModel.m_Materials);
	SendMessage(hProgressBar, PBM_SETPOS, 99, 0);
	writeGuides			(ofs, rGameModel.m_Guides);
	SendMessage(hProgressBar, PBM_SETPOS, 100, 0);

	ofs.close();

	return true;
}
