// FBXConverter.cpp : 응용 프로그램에 대한 진입점을 정의합니다.
//


#include "stdafx.h"
#include "resource.h"

#include"FBXLoader.h"

wchar_t sPath[MAX_PATH + 1];
CFBXLoader g_FbxLoader;
std::wstring g_sFileName;
float g_fScale;
std::vector<std::wstring> g_sTexts_ComboBox = {L"ETC",L"MainMecha", L"Missile"};

HWND g_hProgressBar;
HWND g_hComboBox;
HWND g_hDlg;

INT_PTR  CALLBACK DlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

static void convert()
{
	auto nIndex = SendMessage(g_hComboBox, CB_GETCURSEL, 0, 0);
	EMyType_ModelType eModelType = EMyType_ModelType( static_cast<char>(nIndex));
	DirectX::XMFLOAT4X4 mGlobal = 
		DirectX::XMFLOAT4X4(g_fScale,0.0f, 0.0f, 0.0f,
							0.0f,g_fScale , 0.0f, 0.0f,
							0.0f, 0.0f,g_fScale , 0.0f,
							0.0f, 0.0f, 0.0f,1.0f);
	if(!g_FbxLoader.loadFile(eModelType,g_sFileName, mGlobal, g_hProgressBar))return;
	g_FbxLoader.exportGameModel_CustomType(g_hProgressBar);
	MessageBox(g_hDlg, L"컨버트 종료", L"완료", MB_OK);

	SendMessage(g_hProgressBar, PBM_SETPOS, 0, 0);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	AllocConsole(); 
	FILE *fpStreamOut;
	freopen_s(&fpStreamOut,"CONOUT$", "w", stdout);

	::GetCurrentDirectory(MAX_PATH, sPath);
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, DlgProc);
	
	FreeConsole();
}

INT_PTR  CALLBACK DlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	wchar_t lpstrFile[256] = L"";
	g_hDlg = hDlg;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		g_FbxLoader.setRoodHandle(hDlg);
		g_hProgressBar = GetDlgItem(hDlg, IDC_PROGRESS1);
		g_hComboBox = GetDlgItem(hDlg, IDC_COMBO1);
		
		for(auto &rData : g_sTexts_ComboBox)
			SendMessage(g_hComboBox, CB_ADDSTRING, 0, reinterpret_cast<LPARAM>(rData.c_str()));

		SendMessage(g_hComboBox, CB_SETCURSEL, 0, 0);

		SendMessage(g_hProgressBar, PBM_SETRANGE, 0, WORD(100)<<16|WORD(0));
		SetDlgItemText(hDlg, IDC_EDIT2, L"1.0");
		return TRUE;
	case WM_DROPFILES:
	{
		DragQueryFile(reinterpret_cast<HDROP>(wParam), 0, lpstrFile, 256);
		g_sFileName = lpstrFile;
		SetDlgItemText(hDlg, IDC_EDIT1, lpstrFile);
		
		return TRUE;
	}
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		
		case IDOK:
			GetDlgItemText(hDlg, IDC_EDIT2, lpstrFile,255);
			g_fScale = std::stof(lpstrFile);
			convert();

			return TRUE;
		case IDCANCEL:
		case ID_40001:
			EndDialog(hDlg, IDCANCEL);
			return TRUE;
		case ID_40002:
		{
			OPENFILENAME OFN;

			memset(&OFN, 0, sizeof(OPENFILENAME));
			OFN.lStructSize = sizeof(OPENFILENAME);
			OFN.hwndOwner = hDlg;
			OFN.lpstrFilter = L"FBX File\0*.fbx\0";
			OFN.lpstrFile = lpstrFile;
			OFN.lpstrInitialDir = sPath;
			OFN.nMaxFile = 256;
			if (GetOpenFileName(&OFN) != 0) {
				g_sFileName = lpstrFile;
				SetDlgItemText(hDlg, IDC_EDIT1, lpstrFile);
//				convert(std::wstring(lpstrFile));
			}
			return TRUE;
		}
		default:
			return DefWindowProc(hDlg, uMsg, wParam, lParam);
		}
		break;
	} 
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	//default:
	//	return DefWindowProc(hDlg, uMsg, wParam, lParam);
	}
	return 0;
}