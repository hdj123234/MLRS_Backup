#include "stdafx.h"
#include "PartLoader_Bone.h"


CPartLoader_Bone::CPartLoader_Bone()
	:m_pmGlobalTransform(static_cast<DirectX::XMFLOAT4X4A*>(_aligned_malloc(sizeof(DirectX::XMFLOAT4X4A), 16))),
	m_nEndFrame(0)
{
}

CPartLoader_Bone::~CPartLoader_Bone()
{
	_aligned_free(m_pmGlobalTransform);
}

void CPartLoader_Bone::setAnimStack(FbxAnimStack * pAnimStack)
{
	std::string sName = pAnimStack->GetName();

	unsigned int nEndFrame = static_cast<unsigned int>(pAnimStack->LocalStop.Get().GetFrameCount() + 1);
	if (nEndFrame > m_nEndFrame) m_nEndFrame = nEndFrame;
	 
}

void CPartLoader_Bone::visitNode(FbxNode * pNode)
{ 
	const unsigned int nNumberOfAttribute = pNode->GetNodeAttributeCount();
	for (unsigned int i = 0; i < nNumberOfAttribute; i++)
	{
		FbxNodeAttribute * pAttribute = pNode->GetNodeAttributeByIndex(i);
		auto type = pAttribute->GetAttributeType();
		switch (pAttribute->GetAttributeType())
		{
		case FbxNodeAttribute::EType::eSkeleton:
			visitSkeleton(nullptr,static_cast<FbxSkeleton *>(pAttribute));
			return;
		}
	}
	//재귀호출
	const unsigned int nNumberOfChild = pNode->GetChildCount();
	for (unsigned int i = 0; i < nNumberOfChild; ++i)
		visitNode(pNode->GetChild(i));
}
 
void CPartLoader_Bone::visitSkeleton(CFBX_TreeNode_Skeleton * pParent, FbxSkeleton * pSkeleton)
{
	using namespace DirectX;

	if (!pSkeleton)	return;

	CFBX_TreeNode_Skeleton * pTreeNode = new CFBX_TreeNode_Skeleton();
	if (!pParent)		m_pTree_Skeletons.push_back(pTreeNode);
	else	pParent->addChild(pTreeNode);

	FbxNode * pNode = pSkeleton->GetNode();

	pTreeNode->m_sName = pNode->GetName();
	XMMATRIX mMatrix_FBXGlobal = XMLoadFloat4x4(m_pmGlobalTransform);
	 
	//애니메이션 정보
	unsigned int nNumberOfKey = m_nEndFrame;
	pTreeNode->setSize(nNumberOfKey);
	FbxTime time;
	for (unsigned int i = 0; i < nNumberOfKey; ++i)
	{
		time.SetFrame(i);

		XMMATRIX mAnim;

		auto rmAnimLocal = pNode->EvaluateLocalTransform(time);
		auto pElements = rmAnimLocal.Double44();
		mAnim = (XMMATRIX(	static_cast<float>(pElements[0][0]), static_cast<float>(pElements[0][1]), static_cast<float>(pElements[0][2]), static_cast<float>(pElements[0][3]),
							static_cast<float>(pElements[1][0]), static_cast<float>(pElements[1][1]), static_cast<float>(pElements[1][2]), static_cast<float>(pElements[1][3]),
							static_cast<float>(pElements[2][0]), static_cast<float>(pElements[2][1]), static_cast<float>(pElements[2][2]), static_cast<float>(pElements[2][3]),
							static_cast<float>(pElements[3][0]), static_cast<float>(pElements[3][1]), static_cast<float>(pElements[3][2]), static_cast<float>(pElements[3][3])));
		if (!pTreeNode->m_pParent)
			mAnim = mAnim* mMatrix_FBXGlobal;
		XMStoreFloat4x4(&pTreeNode->m_mTransforms_Anim[i], mAnim);

	}
	pTreeNode->m_eType = pSkeleton->GetSkeletonType();

	if (pSkeleton->GetSkeletonType() == FbxSkeleton::eLimb)
		pTreeNode->m_fSize = static_cast<float>(pSkeleton->LimbLength.Get());
	else if (pSkeleton->GetSkeletonType() == FbxSkeleton::eLimbNode)
		pTreeNode->m_fSize = static_cast<float>(pSkeleton->Size.Get());
	else if (pSkeleton->GetSkeletonType() == FbxSkeleton::eRoot)
		pTreeNode->m_fSize = static_cast<float>(pSkeleton->Size.Get());


	//자식노드
	const unsigned int nNumberOfChild = pNode->GetChildCount();
	for (unsigned int i = 0; i < nNumberOfChild; ++i)
	{
		FbxNode *pChildNode = pNode->GetChild(i);
		std::string s = pChildNode->GetName();
		const unsigned int nNumberOfAttribute = pChildNode->GetNodeAttributeCount();
		for (unsigned int j = 0; j < nNumberOfAttribute; j++)
		{
			FbxNodeAttribute * pAttribute = pChildNode->GetNodeAttributeByIndex(j);

			if (pAttribute->GetAttributeType() == FbxNodeAttribute::EType::eSkeleton)
			{
				CFBX_TreeNode_Skeleton * pTreeNode_Child = new CFBX_TreeNode_Skeleton();
//				pTreeNode->addChild(pTreeNode_Child);
				visitSkeleton(pTreeNode, static_cast<FbxSkeleton *>(pAttribute));
			}
		}
	}
}

void CPartLoader_Bone::convertBones(std::map<std::string, DirectX::XMFLOAT4X4> &rmSkins)
{
	if (m_pTree_Skeletons.empty())
		return;
	else if (m_pTree_Skeletons.size() > 1)
	{
		CFBX_TreeNode_Skeleton *pRoot = new CFBX_TreeNode_Skeleton();
		for (auto data : m_pTree_Skeletons)
			pRoot->addChild(data);
		pRoot->m_sName = "_Root";
		const unsigned int nFrameSize = static_cast<unsigned int>(m_pTree_Skeletons[0]->m_mTransforms_Anim.size());
		pRoot->m_mTransforms_Anim.resize(nFrameSize);
		for (unsigned int i = 0; i < nFrameSize; ++i)
			pRoot->m_mTransforms_Anim[i] = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

		m_pTree_Skeletons.resize(1);
		m_pTree_Skeletons[0] = pRoot;
	}

//	if (m_pSkeletons.size() > 1)	//루트노드가 없을 시 생성하고 연결
//	{
//		CTreeNode_Skeleton *pRoot = new CTreeNode_Skeleton;
//
//		pRoot->m_eType = FbxSkeleton::EType::eRoot;
//		pRoot->m_sName = "Root";
//
//		pRoot->m_mTransform_ToLocal = pRoot->m_mTransform_ToLocal = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
//		const unsigned int nFrameSize = static_cast<unsigned int>(m_pSkeletons[0]->m_mTransforms_Anim.size());
//		pRoot->m_mTransforms_Anim.resize(nFrameSize);
//		for (unsigned int i = 0; i < nFrameSize; ++i)
//			pRoot->m_mTransforms_Anim[i] = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
//
//
//		for (auto data : m_pSkeletons)
//			pRoot->addChild(data);
//
//		m_pSkeletons.clear();
//		m_pSkeletons.push_back(pRoot);
//	}

	std::queue<CFBX_TreeNode_Skeleton *> nodeQueue;

	nodeQueue.push(m_pTree_Skeletons[0]);
	CFBX_TreeNode_Skeleton *pBone = nullptr;
	m_Retval_Bones.reserve(52);
	m_Retval_AnimTransform.reserve(52);

	auto skinIter_End = rmSkins.end();
	DirectX::XMFLOAT4X4 mToLocal;
	while (!nodeQueue.empty())
	{
		bool bSucc;
		pBone = nodeQueue.front();
		nodeQueue.pop();
		
		auto iter = rmSkins.find(pBone->m_sName);
		if (iter == skinIter_End) mToLocal = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
		else mToLocal = iter->second;
		if (auto pParent = pBone->m_pParent)bSucc = m_Retval_Bones.addBone(pBone->m_sName, mToLocal, pParent->m_sName);
		else bSucc = m_Retval_Bones.addBone(pBone->m_sName, mToLocal);
		if (!bSucc) continue;
	
		m_Retval_AnimTransform.push_back(pBone->m_mTransforms_Anim);

		for (auto data : pBone->m_pChildren)
			nodeQueue.push(data);
	}

//	//본 실제 데이터
//	auto &rBoneContainer = m_Bones_Converted.getContainer();
//	m_BoneStructureData.reserve(rBoneContainer.size());
//
//	auto iter_End = m_Skin.end();
//	for (auto &rData : rBoneContainer)
//	{
//		m_BoneStructureData.push_back(rData.second);
//		auto iter = m_Skin.find(rData.first);
//		if (iter == iter_End)
//			m_BoneStructureData.back().m_mTransform_ToLocal = XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
//		else
//			m_BoneStructureData.back().m_mTransform_ToLocal = m_Skin.find(rData.first)->second.m_mTransform_ToLocal;
//
//	}
//
//	// 애니메이션(프레임별 변환행렬)
//	m_Retval_AnimTransform.resize(m_Retval_Bones.getNumberOfBone());
//	 
//	nodeQueue.push(m_pSkeletons[0]);
//	while (!nodeQueue.empty())
//	{
//		pBone = nodeQueue.front();
//		nodeQueue.pop();
//
//		unsigned int nFrameSize = static_cast<unsigned int>(pBone->m_mTransforms_Anim.size());
//		unsigned int  nIndex = m_Bones_Converted.findBoneIndex(pBone->m_sName);
//
//
//		m_mAnimFrameData[nIndex].reserve(nFrameSize);
//
//		std::copy(pBone->m_mTransforms_Anim.begin(), pBone->m_mTransforms_Anim.end(), std::back_inserter(m_mAnimFrameData[nIndex]));
//		for (auto data : pBone->m_pChildren)
//			nodeQueue.push(data);
//
//	}
//	return true;
}

const bool  CPartLoader_Bone::loadBoneData(
	FbxScene * pScene,
	std::map<std::string, DirectX::XMFLOAT4X4> &rmSkins,
	const DirectX::XMFLOAT4X4 &rmGlobalTransform)
{
	if (!pScene)return false;

	*m_pmGlobalTransform = DirectX::XMFLOAT4X4A(reinterpret_cast<const float *>(rmGlobalTransform.m));

	const unsigned int nNumberOfAnimStack = pScene->GetSrcObjectCount<FbxAnimStack>();
	for (unsigned int i = 0; i < nNumberOfAnimStack; i++)
		setAnimStack(pScene->GetSrcObject<FbxAnimStack>(i));
	 
	for (auto pRoot : m_pTree_Skeletons)
		if (pRoot)delete pRoot;
	m_pTree_Skeletons.clear();
	
	FbxNode* pRootNode = pScene->GetRootNode();
	if(!pRootNode)	return false;

	visitNode(pRootNode);

	convertBones(rmSkins);
	 
	return true;
}

void CPartLoader_Bone::clear()
{
	for(auto data : m_pTree_Skeletons)	delete data;
	m_pTree_Skeletons.clear();
	m_Retval_Bones.clear();
	m_Retval_AnimTransform.clear();
	*m_pmGlobalTransform = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};	//mAxis * mMatrix_FBXGlobal
	m_nEndFrame=0;
}

