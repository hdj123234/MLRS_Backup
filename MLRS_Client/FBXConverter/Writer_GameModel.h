#pragma once
#ifndef HEADER_WRITER_GAMEMODEL
#define HEADER_WRITER_GAMEMODEL

#include"Type_MyGameModel.h"


class CWriter_GameModel{
public:
	CWriter_GameModel();
	~CWriter_GameModel(); 

private:
	const bool writeMeshs(std::ofstream &rofs, const std::vector<CMyType_Mesh> &rMeshs);
	const bool writeBone(std::ofstream &rofs, const CMyType_Bones &rBones);
	const bool writeAnimTransforms(std::ofstream &rofs, const std::vector<std::vector<DirectX::XMFLOAT4X4>> &rAnimTransforms);
	const bool writeMaterials(std::ofstream &rofs, const std::vector<CMyType_Material> &rMaterials);
	const bool writeGuides(std::ofstream &rofs, const CMyType_Guides &rGuides);

public:
	const bool write(const std::string &rsFileName,CMyType_GameModel &rGameModel, HWND hProgressBar);
};

#endif