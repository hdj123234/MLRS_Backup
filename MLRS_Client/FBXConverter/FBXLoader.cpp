#include "stdafx.h"
#include "FBXLoader.h"
#include"..\MLRS_Client\MyUtility.h"

using namespace DirectX;

std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> CFBXLoader::s_wstring_converter;

CFBXLoader::CFBXLoader(const DirectX::XMFLOAT4X4 &rmGlobalTransform)
	:m_hWnd(NULL),
	m_pFBXManager(FbxManager::Create()),
	m_pFBXIOSettings(FbxIOSettings::Create(m_pFBXManager, IOSROOT)),
	m_pFBXImporter(FbxImporter::Create(m_pFBXManager, "")),
//	m_pRoot(nullptr),
	m_nEndFrame(0),
	m_mConvertAxis(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
	m_mGlobalTransform(rmGlobalTransform)
{
	m_pFBXManager->SetIOSettings(m_pFBXIOSettings);
}

CFBXLoader::~CFBXLoader()
{
	m_pFBXImporter->Destroy();
	m_pFBXIOSettings->Destroy();
	m_pFBXManager->Destroy();

	//if (m_pRoot)
	//	delete m_pRoot;
}

void CFBXLoader::setName(const std::string & sFileName)
{
	m_sFullPath = sFileName;
	CUtility_Path::dividePath(m_sFullPath, m_sPath, m_sFileName);
	m_sPath = CUtility_Path::dividePath_FromDataFolder(m_sPath);
}
 
const std::string  CFBXLoader::getNewPath(const std::string & sPath)
{
	return m_sPath+ CUtility_Path::getFileNameOnly(sPath);
}

bool CFBXLoader::loadFile(const EMyType_ModelType eModelType, const std::wstring & sFileName, const DirectX::XMFLOAT4X4 &rmGlobal, HWND hProgressBar)
{
	//파일 열기(Scene 생성)
	return loadFile(eModelType,s_wstring_converter.to_bytes(sFileName), rmGlobal, hProgressBar);
}

bool CFBXLoader::loadFile(const EMyType_ModelType eModelType, const std::string & sFileName,const DirectX::XMFLOAT4X4 &rmGlobal, HWND hProgressBar)
{
	SendMessage(hProgressBar, PBM_SETPOS, 0, 0);
	clear();
	setName(sFileName);
	m_mGlobalTransform = rmGlobal;
	FbxIOSettings *pIOSettings = FbxIOSettings::Create(m_pFBXManager, "IOSettings");
	if (!m_pFBXImporter->Initialize(m_sFullPath.c_str()))
	{
		::MessageBox(m_hWnd, (s_wstring_converter.from_bytes(sFileName)  + L" 파일을 읽을수 없습니다").c_str(), L"ERROR", MB_OK);
		return false;
	}
	FbxScene *pScene = FbxScene::Create(m_pFBXManager, "LoadScene");
	if (!m_pFBXImporter->Import(pScene))	return false;
	
	

	//컨버트
	FbxGeometryConverter converter(m_pFBXManager);
	converter.Triangulate(pScene, true);
	/* ConvertScene 함수를 사용한 경우 
	애니메이션 행렬이 변환되지 않는 문제 발생
	수동으로 좌표축 변환행렬 적용	*/
	//파일은 3DSMAX에서 제작되었다고 가정, Y-UP/Z-UP을 판별하여 좌표축 변환행렬 생성
	int nTmp = 0;
	switch (pScene->GetGlobalSettings().GetAxisSystem().GetUpVector(nTmp))
	{
	case FbxAxisSystem::EUpVector::eYAxis:
		XMStoreFloat4x4(&m_mConvertAxis, XMMatrixSet(-1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1));
		break;
	case FbxAxisSystem::EUpVector::eZAxis:
		XMStoreFloat4x4(&m_mConvertAxis, XMMatrixSet(1, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 1));
		break;

	}

	SendMessage(hProgressBar, PBM_SETPOS, 10, 0);
	 
	DirectX::XMFLOAT4X4 mMatrix;
	DirectX::XMStoreFloat4x4(&mMatrix, (DirectX::XMLoadFloat4x4(&m_mConvertAxis) * DirectX::XMLoadFloat4x4(&m_mGlobalTransform)));
	 
	m_GameModel.m_Materials = std::move(m_Loader_Material.getMaterials(pScene));
	SendMessage(hProgressBar, PBM_SETPOS, 20, 0);

	m_Loader_Mesh.loadMesh(pScene,  mMatrix);
	SendMessage(hProgressBar, PBM_SETPOS, 35, 0);

	m_Loader_Bone.loadBoneData(pScene, m_Loader_Mesh.getSkin(), mMatrix);
	m_GameModel.m_Bones = std::move(m_Loader_Bone.getBones());
	m_GameModel.m_AnimTransform = std::move(m_Loader_Bone.getAnimTransform());
	SendMessage(hProgressBar, PBM_SETPOS, 50, 0);
	 
	m_Loader_Mesh.convertMeshs(m_GameModel.m_Bones,m_GameModel.m_Materials);
	m_GameModel.m_Meshs = std::move(m_Loader_Mesh.getMeshs());
	SendMessage(hProgressBar, PBM_SETPOS, 60, 0);

	m_GameModel.m_Guides=std::move( m_Loader_Guide.loadGuide(eModelType, pScene, m_Loader_Mesh.getPoses(), mMatrix));
	//if (FbxNode* pRootNode = pScene->GetRootNode())
	//{
	//	//노드 순회
	//	CFBX_TreeNode *pRoot = new CFBX_TreeNode();
	//	m_pRoot = pRoot;
	//	addNode(pRoot,pRootNode);
	//}

	//소멸
	pScene->Destroy();

	return true;
//	return convertData();
}

bool CFBXLoader::exportGameModel_CustomType(HWND hProgressBar)
{
	return m_Writer_Model.write(
		CUtility_Path::clipException(m_sFullPath)+".mgm",
		m_GameModel,
		hProgressBar);
}

bool CFBXLoader::exportBoundingObject()
{
	return false;
}

void CFBXLoader::clear()
{
	m_Loader_Bone.clear();
	m_Loader_Material.clear();
	m_Loader_Mesh.clear();
	m_Loader_Guide.clear();
	m_nEndFrame = 0;
}

