#ifndef HEADER_FBXLOADER
#define HEADER_FBXLOADER

#include"Type_MyGameModel.h" 
#include"PartLoader_Material.h"
#include"PartLoader_Mesh.h"
#include"PartLoader_Bone.h"
#include"PartLoader_Guide.h"
#include"Writer_GameModel.h"

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif

#ifndef HEADER_WINDOWS
#include<Windows.h>
#endif // !HEADER_WINDOWS

#ifndef HEADER_FBXSDK
#include<fbxsdk.h>
#endif

#ifndef HEADER_STL
#include<locale>
#include<codecvt>
#include<vector>
#include<string>
#include<map>
#endif


class CFBXLoader{
private:
	CPartLoader_Mesh m_Loader_Mesh;
	CPartLoader_Bone m_Loader_Bone;
	CPartLoader_Material m_Loader_Material;
	CPartLoader_Guide m_Loader_Guide;

	CWriter_GameModel m_Writer_Model;

	CMyType_GameModel m_GameModel;


private:
	std::string m_sFileName;
	std::string m_sPath;
	std::string m_sFullPath;

	HWND m_hWnd;
	FbxManager *m_pFBXManager;
	FbxIOSettings *m_pFBXIOSettings;
	FbxImporter *m_pFBXImporter;
	std::vector<FbxAnimLayer *> m_pFBXAnimLayers;
	unsigned int m_nEndFrame;
	 
	std::map<std::string,DirectX::XMFLOAT4X4> m_mPoses;
	DirectX::XMFLOAT4X4 m_mConvertAxis;
	DirectX::XMFLOAT4X4 m_mGlobalTransform;

	static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> s_wstring_converter;

public:
	CFBXLoader(const DirectX::XMFLOAT4X4 &rmGlobalTransform = DirectX::XMFLOAT4X4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1));
	~CFBXLoader();

	void setName(const std::string &sFileName); 

	const std::string getNewPath(const std::string &sPath); 

public:
	void setRoodHandle(HWND hWnd) { m_hWnd = hWnd; }

	bool loadFile(const EMyType_ModelType eModelType, const std::wstring &sFileName, const DirectX::XMFLOAT4X4 &rmGlobal,HWND hProgressBar);
	bool loadFile(const EMyType_ModelType eModelType, const std::string &sFileName, const DirectX::XMFLOAT4X4 &rmGlobal, HWND hProgressBar);
	bool exportGameModel_CustomType(HWND hProgressBar);
	bool exportBoundingObject();
	void clear();
};

#endif