#include "stdafx.h"
#include "PartLoader_Mesh.h"

#include"..\MLRS_Client\MyUtility.h"


CPartLoader_Mesh::CPartLoader_Mesh()
{
}

CPartLoader_Mesh::~CPartLoader_Mesh()
{
}

void CPartLoader_Mesh::addPose(FbxPose * pPose)
{
	using DirectX::XMFLOAT4X4;
	std::string s = pPose->GetName();

	const unsigned int nNumber = pPose->GetCount();
	for (unsigned int i = 0; i < nNumber; ++i)
	{
		std::string sCurrentName = pPose->GetNodeName(i).GetCurrentName();
		auto matrix = pPose->GetMatrix(i);
		m_mPoses[sCurrentName] = XMFLOAT4X4(
			static_cast<float>(matrix[0][0]), static_cast<float>(matrix[0][1]), static_cast<float>(matrix[0][2]), static_cast<float>(matrix[0][3]),
			static_cast<float>(matrix[1][0]), static_cast<float>(matrix[1][1]), static_cast<float>(matrix[1][2]), static_cast<float>(matrix[1][3]),
			static_cast<float>(matrix[2][0]), static_cast<float>(matrix[2][1]), static_cast<float>(matrix[2][2]), static_cast<float>(matrix[2][3]),
			static_cast<float>(matrix[3][0]), static_cast<float>(matrix[3][1]), static_cast<float>(matrix[3][2]), static_cast<float>(matrix[3][3]));
	}
}

void CPartLoader_Mesh::visitNode(FbxNode * pNode)
{
	const unsigned int nNumberOfAttribute = pNode->GetNodeAttributeCount();
	for (unsigned int i = 0; i < nNumberOfAttribute; i++)
	{
		FbxNodeAttribute * pAttribute = pNode->GetNodeAttributeByIndex(i);
		auto type = pAttribute->GetAttributeType();
		switch (pAttribute->GetAttributeType())
		{
		case FbxNodeAttribute::EType::eMesh:
			visitMesh(static_cast<FbxMesh *>(pAttribute));
		}
	}
	//재귀호출
	const unsigned int nNumberOfChild = pNode->GetChildCount();
	for (unsigned int i = 0; i < nNumberOfChild; ++i)
		visitNode(pNode->GetChild(i));
}

void CPartLoader_Mesh::visitMesh(FbxMesh * pMesh)
{
	using namespace DirectX;
	if (!pMesh)	return;
	
	FbxNode * pNode = pMesh->GetNode();
	std::string sName = pNode->GetName();
	if (checkFilter(sName))return;
	
	m_FBXMeshs.push_back(CFBX_Mesh());
	auto &rMesh = m_FBXMeshs.back();
	 
	rMesh.m_sName = sName;
	// it is not inherited by the children.   
	const FbxVector4 lT = pNode->GetGeometricTranslation(FbxNode::eSourcePivot);
	const FbxVector4 lR = pNode->GetGeometricRotation(FbxNode::eSourcePivot);
	const FbxVector4 lS = pNode->GetGeometricScaling(FbxNode::eSourcePivot);
	
	FbxAMatrix mGeometryOffset = FbxAMatrix(lT, lR, lS);
	XMMATRIX mMatrix_Offset = (XMMATRIX(
		static_cast<float>(mGeometryOffset[0][0]), static_cast<float>(mGeometryOffset[0][1]), static_cast<float>(mGeometryOffset[0][2]), static_cast<float>(mGeometryOffset[0][3]),
		static_cast<float>(mGeometryOffset[1][0]), static_cast<float>(mGeometryOffset[1][1]), static_cast<float>(mGeometryOffset[1][2]), static_cast<float>(mGeometryOffset[1][3]),
		static_cast<float>(mGeometryOffset[2][0]), static_cast<float>(mGeometryOffset[2][1]), static_cast<float>(mGeometryOffset[2][2]), static_cast<float>(mGeometryOffset[2][3]),
		static_cast<float>(mGeometryOffset[3][0]), static_cast<float>(mGeometryOffset[3][1]), static_cast<float>(mGeometryOffset[3][2]), static_cast<float>(mGeometryOffset[3][3])));
	
	FbxAMatrix &mGlobal = pNode->EvaluateGlobalTransform();
	
	XMMATRIX mMatrix_FBXGlobal = XMLoadFloat4x4(&m_mGlobalTransform);
//	XMMATRIX mMatrix_Axis = XMLoadFloat4x4(&m_mConvertAxis);
	
	XMMATRIX mMatrix_Pose;
	if (m_mPoses.find(sName) == m_mPoses.end())
	{
		mMatrix_Pose = XMMATRIX(
			static_cast<float>(mGlobal[0][0]), static_cast<float>(mGlobal[0][1]), static_cast<float>(mGlobal[0][2]), static_cast<float>(mGlobal[0][3]),
			static_cast<float>(mGlobal[1][0]), static_cast<float>(mGlobal[1][1]), static_cast<float>(mGlobal[1][2]), static_cast<float>(mGlobal[1][3]),
			static_cast<float>(mGlobal[2][0]), static_cast<float>(mGlobal[2][1]), static_cast<float>(mGlobal[2][2]), static_cast<float>(mGlobal[2][3]),
			static_cast<float>(mGlobal[3][0]), static_cast<float>(mGlobal[3][1]), static_cast<float>(mGlobal[3][2]), static_cast<float>(mGlobal[3][3]));
	}
	else
	mMatrix_Pose = XMLoadFloat4x4(&m_mPoses[sName]);
	
	XMMATRIX mMatrix_OPG = mMatrix_Offset*mMatrix_Pose*mMatrix_FBXGlobal;
	XMMATRIX mMatrix_OG = mMatrix_Offset*mMatrix_FBXGlobal;

	//	XMMATRIX mConvertAxis = XMLoadFloat4x4(&m_mConvertAxis);
	/*
	한 파일에 존재하는 메쉬는 모두 하나의 객체라고 가정하고
	또한 모든 객체는 Skined Anim이 적용되어있다고 가정하여
	계층구조 및 순서를 무시하고 로딩
	*/
	
	//정점
	FbxVector4 * pControlPoints = pMesh->GetControlPoints();
	const unsigned int nNumberOfControlPoint = pMesh->GetControlPointsCount();
//	rMesh.m_Vertices.reserve(nNumberOfControlPoint);
//	std::vector<DirectX::XMFLOAT3> vPoss;
	rMesh.m_Vertices.reserve(nNumberOfControlPoint);
	for (unsigned int i = 0; i < nNumberOfControlPoint; ++i)
	{
		XMFLOAT3 vPosition;
		XMVECTOR vVector = XMVectorSet(
			static_cast<float>(pControlPoints[i][0]),
			static_cast<float>(pControlPoints[i][1]),
			static_cast<float>(pControlPoints[i][2]), 1);
	
	
		//		vVector = XMVector3TransformCoord(vVector, mMatrix_Global);
		//		vVector = XMVector3TransformCoord(vVector, mMatrix_Local);
//		vVector = XMVector3TransformCoord(vVector, mMatrix_Offset);
//		vVector = XMVector3TransformCoord(vVector, mMatrix_Pose);
//		vVector = XMVector3TransformCoord(vVector, mMatrix_Axis);
		vVector = XMVector3TransformCoord(vVector, mMatrix_OPG);
	 
		XMStoreFloat3(&vPosition, vVector);
		rMesh.m_Vertices.push_back(CFBX_Vertex());
		rMesh.m_Vertices.back().m_vPos = vPosition;
	
		if (i == 0)
			rMesh.m_vAABB[0] = rMesh.m_vAABB[1] = vPosition;
		else
		{
			if		(vPosition.x < rMesh.m_vAABB[0].x)			rMesh.m_vAABB[0].x = vPosition.x;
			else if (vPosition.x > rMesh.m_vAABB[1].x)			rMesh.m_vAABB[1].x = vPosition.x;
			if		(vPosition.y < rMesh.m_vAABB[0].y)			rMesh.m_vAABB[0].y = vPosition.y;
			else if (vPosition.y > rMesh.m_vAABB[1].y)			rMesh.m_vAABB[1].y = vPosition.y;
			if		(vPosition.z < rMesh.m_vAABB[0].z)			rMesh.m_vAABB[0].z = vPosition.z;
			else if (vPosition.z > rMesh.m_vAABB[1].z)			rMesh.m_vAABB[1].z = vPosition.z;
		}
	}
	
	//uv
	FbxGeometryElementUV *pUV;
	FbxStringList uvSetNameList;
	pMesh->GetUVSetNames(uvSetNameList);
	if (!(pUV = pMesh->GetElementUV(uvSetNameList[0]))) return;
	FbxLayerElementArrayTemplate<FbxVector2> &rvUVs = pUV->GetDirectArray();
	const unsigned int nNumberOfUV = rvUVs.GetCount();
	
	rMesh.m_vUVs.reserve(nNumberOfUV);
	for (unsigned int i = 0; i < nNumberOfUV; ++i)
		rMesh.m_vUVs.push_back(XMFLOAT2(
			static_cast<float>(rvUVs[i][0]),
			1.0f - static_cast<float>(rvUVs[i][1])));
	//재질
	bool bByPolygon;
	std::vector<std::string> sMaterialIndices;
	std::set<std::string> sMaterialNames;
	switch (pMesh->GetElementMaterial()->GetMappingMode())	{
	case FbxLayerElement::EMappingMode::eByPolygon:
	{
		bByPolygon = true;
		FbxLayerElementArrayTemplate<int>& rMaterialIndices = pMesh->GetElementMaterial()->GetIndexArray();
		const unsigned int nNumberOfMaterialIndex = rMaterialIndices.GetCount();
		sMaterialIndices.resize(nNumberOfMaterialIndex);
		for (unsigned int i = 0; i < nNumberOfMaterialIndex; ++i)
		{
			std::string sMaterialName = pNode->GetMaterial(rMaterialIndices[i])->GetName();
			sMaterialNames.insert(sMaterialName);
			sMaterialIndices[nNumberOfMaterialIndex-(i+1)]=(sMaterialName);
		}
		break;
	}
	default:
	{
		bByPolygon = false;
		FbxLayerElementArrayTemplate<int>& rMaterialIndices = pMesh->GetElementMaterial()->GetIndexArray();
		sMaterialIndices.resize(1);
		std::string sMaterialName = pNode->GetMaterial(0)->GetName();
		sMaterialNames.insert(sMaterialName);
		sMaterialIndices[0] = sMaterialName;
		
		break;
	}
	}
	rMesh.m_sMaterialNames = std::move(sMaterialNames);
	rMesh.m_sMaterials = std::move(sMaterialIndices);

	/*폴리곤
	삼각형으로 변환(Trangulate)했기 때문에 모든 폴리곤은 삼각형으로 가정
	*/
	int * pVertexIndices = pMesh->GetPolygonVertices();
	const unsigned int nNumberOfPolygonVertex = pMesh->GetPolygonVertexCount();
	FbxLayerElementArrayTemplate<FbxVector4> *pvNormals;
	FbxLayerElementArrayTemplate<FbxVector4> *pvTangents;
	if (!pMesh->GetNormals(&pvNormals))	return;
	if (!pMesh->GetTangents(&pvTangents))	return;
	FbxLayerElementArrayTemplate<int> &pUVsIndices = pUV->GetIndexArray();
	
	rMesh.m_Indices.resize(nNumberOfPolygonVertex);
	for (unsigned int i = 0; i < nNumberOfPolygonVertex; ++i)
	{
		FbxVector4 vNormal = pvNormals->GetAt(i);
		FbxVector4 vTangent = pvTangents->GetAt(i);
		XMVECTOR vNormal_Converted = XMVector3NormalizeEst(
				XMVector3Transform(XMVectorSet(static_cast<float>(vNormal[0]),
													static_cast<float>(vNormal[1]),
													static_cast<float>(vNormal[2]), 0), mMatrix_FBXGlobal));
		XMVECTOR vTangent_Converted = XMVector3NormalizeEst(
				XMVector3Transform(XMVectorSet(static_cast<float>(vTangent[0]),
													static_cast<float>(vTangent[1]),
													static_cast<float>(vTangent[2]), 0), mMatrix_FBXGlobal));

		//FBX와 DirectX는 좌표계가 반대이므로 와인딩 오더를 역순으로 입력
		rMesh.m_Indices[nNumberOfPolygonVertex-(i+1)].m_nVertexIndex = pVertexIndices[i]; 
		rMesh.m_Indices[nNumberOfPolygonVertex-(i+1)].m_nUVIndex = pUVsIndices[i];
		XMStoreFloat3(&rMesh.m_Indices[nNumberOfPolygonVertex - (i + 1)].m_vNormal, vNormal_Converted);
		XMStoreFloat3(&rMesh.m_Indices[nNumberOfPolygonVertex - (i + 1)].m_vTangent, vTangent_Converted);
		//rMesh.m_Indices.push_back(CMyType_VertexIndex(pVertexIndices[i],
		//	pUVsIndices[i],
		//	XMFLOAT3(static_cast<float>(vNormal[0]),
		//		static_cast<float>(vNormal[1]),
		//		static_cast<float>(vNormal[2])),
		//	XMFLOAT3(static_cast<float>(vTangent[0]),
		//		static_cast<float>(vTangent[1]),
		//		static_cast<float>(vTangent[2]))));
	}
	
	// Skin 정보
	const unsigned int nNumberOfDeformer = pMesh->GetDeformerCount();
	for (unsigned int i = 0; i < nNumberOfDeformer; ++i)
	{
		if (!pMesh->GetDeformer(i)->GetDeformerType() == FbxDeformer::EDeformerType::eSkin)	continue;
		FbxSkin *pSkinDeformer = (FbxSkin *)pMesh->GetDeformer(i);
		std::string name = pSkinDeformer->GetGeometry()->GetNode()->GetName();
		if (!pSkinDeformer->GetSkinningType() == FbxSkin::EType::eRigid) continue;
		const unsigned int nNumberOfCluster = pSkinDeformer->GetClusterCount();
		for (unsigned int j = 0; j<nNumberOfCluster; ++j)
		{
			FbxCluster * pCluster = pSkinDeformer->GetCluster(j);
			std::string sBoneName = pCluster->GetLink()->GetName();
	
			// 가중치
			const unsigned int nNumberOfControlPoint_Cluster = pCluster->GetControlPointIndicesCount();
			int *pControlPoints = pCluster->GetControlPointIndices();
			double * pWeights = pCluster->GetControlPointWeights();
			for (unsigned int k = 0; k < nNumberOfControlPoint_Cluster; ++k)
			{
				rMesh.m_Vertices[pControlPoints[k]].m_BoneWeight[sBoneName] = static_cast<float>(pWeights[k]);
			}
	
			FbxAMatrix mTransform_Link;
			pCluster->GetTransformLinkMatrix(mTransform_Link);
			XMMATRIX  mLink = XMMATRIX(
				static_cast<float>(mTransform_Link[0][0]),static_cast<float>( mTransform_Link[0][1]), static_cast<float>(mTransform_Link[0][2]), static_cast<float>(mTransform_Link[0][3]),
				static_cast<float>(mTransform_Link[1][0]),static_cast<float>( mTransform_Link[1][1]), static_cast<float>(mTransform_Link[1][2]), static_cast<float>(mTransform_Link[1][3]),
				static_cast<float>(mTransform_Link[2][0]),static_cast<float>( mTransform_Link[2][1]), static_cast<float>(mTransform_Link[2][2]), static_cast<float>(mTransform_Link[2][3]),
				static_cast<float>(mTransform_Link[3][0]),static_cast<float>( mTransform_Link[3][1]), static_cast<float>(mTransform_Link[3][2]), static_cast<float>(mTransform_Link[3][3]))
				* mMatrix_FBXGlobal;
	
			XMMATRIX  mPose = XMMatrixInverse(NULL, mLink);// *XMMatrixInverse(NULL, mMatrix_Offset);
	
			XMStoreFloat4x4(&m_mSkins[sBoneName], mPose);
		}
	}
	
}

bool CPartLoader_Mesh::checkFilter(const std::string & rsName)const
{
	if(rsName.size()>6&&
		CUtility_String::convertToLower( std::string(rsName.begin(), rsName.begin()+6))=="guide_")
		return true;
	else return false;
}

void CPartLoader_Mesh::convertMeshs(const CMyType_Bones &rBones, std::vector<CMyType_Material>& inout_rMaterials)
{
	//불필요 재질 제거
	std::set<std::string> sMaterials;
	std::map<std::string,unsigned int > nMaterialIndexMap;
	for (auto &rData : m_FBXMeshs)
	{
		for (auto &sMaterialName : rData.m_sMaterialNames)
			sMaterials.insert(sMaterialName);
	}
	for (int i = 0; i < inout_rMaterials.size(); ++i)
	{
		if(sMaterials.count(inout_rMaterials[i].m_sName)==0)
		{
			inout_rMaterials[i] = inout_rMaterials.back();
			inout_rMaterials.pop_back();
			--i;
		}
		else
		{
			nMaterialIndexMap[inout_rMaterials[i].m_sName] = i;
		}
	}
	

	float fWeights[4];
	unsigned int nBoneIndices[4];
//	CMyVertex_Converted vertexData;
	for (auto &rMesh : m_FBXMeshs)
	{
		m_Retval.push_back(CMyType_Mesh());
		CMyType_Mesh &rTarget = m_Retval.back();
	
		rTarget.m_sName = rMesh.m_sName;

		rTarget.m_vAABB = rMesh.m_vAABB;
		//정점
		std::vector<CMyType_Vertex> &rVector = rTarget.m_Vertices;
		std::map<std::string, unsigned int> boneIndexMap;
		for (int i = 0; i<rBones.getContainer().size(); ++i)
			boneIndexMap[rBones.getContainer()[i].first] = i;
		auto iter_BoneMap_End = boneIndexMap.end();
		for (auto &rVertex : rMesh.m_Vertices)
		{
			//4개 이상의 본을 참조하고 있을 경우
			//			if (rVertex.m_vBoneWeight.size() > 4);

			auto iter = rVertex.m_BoneWeight.begin();
			auto iter_End = rVertex.m_BoneWeight.end();
			for (unsigned int i = 0; i < 4; ++i)
			{
				if (iter != iter_End)
				{
					auto iter_BoneMap = boneIndexMap.find(iter->first);
					if (iter_BoneMap == iter_BoneMap_End)
					{
						++iter;
						continue;
					}
					nBoneIndices[i] = iter_BoneMap->second;
					fWeights[i] = iter->second;
					++iter;
					//tmp = m_Bones_Converted.findBoneIndex(iter->first);
					////본 인덱스 검색 실패(해당 이름의 본이 없을경우)
					//if (tmp == -1)
					//{
					//	iter++;
					//	continue;
					//}
					//nBoneIndices[i] = tmp;
					//iter++;
				}
				else
				{
					fWeights[i] = 0;
					nBoneIndices[i] = 0;
				}
			}
			rVector.push_back(CMyType_Vertex());

			rVector.back().m_vPosition = rVertex.m_vPos;
			rVector.back().m_vBoneIndex = DirectX::XMUINT4(nBoneIndices);
			rVector.back().m_vWeights = DirectX::XMFLOAT4(fWeights);
		}

		//uv
		rTarget.m_vUVs = std::move(rMesh.m_vUVs);

		//polygon
		rTarget.m_Indices = std::move(rMesh.m_Indices);

		//재질정보
		const unsigned int nNumberOfMaterial = static_cast<unsigned int>(rTarget.m_Indices.size() / 3);
		rTarget.m_nMaterialIndices.resize(rTarget.m_Indices.size() / 3);
		if (rMesh.m_sMaterials.size()>1) 
			for (unsigned int i = 0; i < nNumberOfMaterial; ++i) 
				rTarget.m_nMaterialIndices[i] = nMaterialIndexMap[rMesh.m_sMaterials[i]];   
		else 
			for (unsigned int i = 0; i < nNumberOfMaterial; ++i)
				rTarget.m_nMaterialIndices[i] = nMaterialIndexMap[rMesh.m_sMaterials[0]]; 
//		rTarget.m_Indices.reserve(rMesh.m_Indices.size());
//		std::copy(rMesh.m_Indices.rbegin(), rMesh.m_Indices.rend(), std::back_inserter(rTarget.m_Indices));
	
	
	} 
}

bool CPartLoader_Mesh::loadMesh(FbxScene * pScene, const DirectX::XMFLOAT4X4 & rmGlobalTransform)
{
	const unsigned  int nNumberOfPose = pScene->GetPoseCount();
	for (unsigned int i = 0; i < nNumberOfPose; i++)
		addPose(pScene->GetPose(i));

	m_mGlobalTransform = rmGlobalTransform;

	visitNode(pScene->GetRootNode());
	return true;
}
 
std::vector<CMyType_Mesh> CPartLoader_Mesh::getMeshs()
{ 
	return std::move(m_Retval);
}

std::map<std::string, DirectX::XMFLOAT4X4> CPartLoader_Mesh::getSkin()
{
	return std::move(m_mSkins);
}

void CPartLoader_Mesh::clear()
{
	m_mPoses.clear();
	m_mSkins.clear();
	m_Meshs.clear();
	m_FBXMeshs.clear();
	m_mGlobalTransform = {1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};	//axis * global

	m_Retval.clear();
}
 

  
//
//DirectX::XMFLOAT3 CFBXLoader::getCenter()
//{
//	using namespace DirectX;
//	XMFLOAT3 vMin = m_Meshs[0].m_BoundingBox[0];
//	XMFLOAT3 vMax = m_Meshs[0].m_BoundingBox[1];
//	for (auto &rData : m_Meshs)
//	{
//		if (vMin.x >rData.m_BoundingBox[0].x)vMin.x = rData.m_BoundingBox[0].x;
//		if (vMin.y >rData.m_BoundingBox[0].y)vMin.y = rData.m_BoundingBox[0].y;
//		if (vMin.z >rData.m_BoundingBox[0].z)vMin.z = rData.m_BoundingBox[0].z;
//		if (vMax.x <rData.m_BoundingBox[1].x)vMax.x = rData.m_BoundingBox[1].x;
//		if (vMax.y <rData.m_BoundingBox[1].y)vMax.y = rData.m_BoundingBox[1].y;
//		if (vMax.z <rData.m_BoundingBox[1].z)vMax.z = rData.m_BoundingBox[1].z;
//	}
//	return DirectX::XMFLOAT3((vMax.x + vMin.x) / 2,
//		(vMax.y + vMin.y) / 2,
//		(vMax.z + vMin.z) / 2);
//}
//
//DirectX::XMFLOAT3 CFBXLoader::getExtent()
//{
//	XMFLOAT3 vMin = m_Meshs[0].m_BoundingBox[0];
//	XMFLOAT3 vMax = m_Meshs[0].m_BoundingBox[1];
//	for (auto &rData : m_Meshs)
//	{
//		if (vMin.x >rData.m_BoundingBox[0].x)vMin.x = rData.m_BoundingBox[0].x;
//		if (vMin.y >rData.m_BoundingBox[0].y)vMin.y = rData.m_BoundingBox[0].y;
//		if (vMin.z >rData.m_BoundingBox[0].z)vMin.z = rData.m_BoundingBox[0].z;
//		if (vMax.x <rData.m_BoundingBox[1].x)vMax.x = rData.m_BoundingBox[1].x;
//		if (vMax.y <rData.m_BoundingBox[1].y)vMax.y = rData.m_BoundingBox[1].y;
//		if (vMax.z <rData.m_BoundingBox[1].z)vMax.z = rData.m_BoundingBox[1].z;
//	}
//	return DirectX::XMFLOAT3((vMax.x - vMin.x) / 2,
//		(vMax.y - vMin.y) / 2,
//		(vMax.z - vMin.z) / 2);
//}

