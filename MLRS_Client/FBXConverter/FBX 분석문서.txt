현재 FBX 포맷 임포트 상태
	모델
		메쉬
			저장된 데이터
				정점
					위치
					가중치(각 본에 대해)
				UV좌표
			정점 데이터
				정점 인덱스
				UV 인덱스
				normal/tangent : 각 폴리곤의 점 별로 값 입력됨
		재질
			램버트 조명모델 혹은 퐁 조명모델에 대해서 값을 읽어옴
			읽어온 데이터는 램버트 조명모델로 처리
			ambient, diffuse, specular
			emissive 무시
			퐁모델의 경우 필요하면 추가 예정
		뼈대(Skeleton, 본)
			트리 구조 사용
			부모 좌표계에서의 평행이동, 회전, 크기변환 값을 가짐
			DirectX에 넣기 위한 형태로 구조 수정 예정
	애니메이션



기본 개념
	Node
		=Object
	Attribute
		Node의 구체적 자료
	Layer
		Geometry의 자료를 기술하기 위한 메카니즘		
		Layer 는 다음 layer element 들 중 하나 이상을 포함할 수 있다.
			Normals
			Binormals
			Tangents
			Materials
			Polygon Groups
			UVs
			Vertex Colors
			Smoothing informations
			Vertex Creases
			Edge Creases
			Custom User Data
			Visibilities
			Textures( diffuse, ambient, specular, etc. )
			[출처] FBX file 에서 Mesh loading 하기 - 1. 개념|작성자 라이푸
	Property
		Object의 설정값을 읽고 쓰기 위함
	Connection
		Property와 Object의 관계 규정

FBXManager
	메모리 관리 클래스
	FBXManager::Create()로 생성
	반드시 Destroy() 함수로 명시적으로 소멸시킬것

FBXImport 순서
	1. 메모리 관리 클래스(FbxManager) 생성
	2. FbxIOSettings 설정
	3. FbxImporter 생성
	4. FbxScene 생성 -> FbxImporter.Import함수로 읽어오기

FbxScene
	일종의 컨테이너
	하나의 FbxScene객체만 관찰될 수 있음(오역일지도)
	가질수 있는 요소
		메시	(*)
		조명
		카메라
		스켈톤	(*)
		NURBS
	FbxNodes로 구성됨(계층적 트리구조로 정렬됨)
	GetRootNode() 함수로 루트노드 엑세스 가능
	기본적으로 Y-UP 오른손 좌표계

FbxNode
	개념적으로 다른 Scene원소들(하나 또는 여러)의 컨테이너의 역할을 함
	원소들은 FbxNodeAttribute에 정의되어 있음
	루트 노드
		오직 자식을 통해서만 저장되며 export시 저장되지 않음
	이하 속성에 대한 접근을 제공
		로컬변환(FbxNode::LclTranslation), 
		회전(FbxNode::LclRotation), 
		크기조절(FbxNode::LclScaling) 
		이러한 속성은 부모노드 위치에서 적용됨
	
	FbxTexture : 텍스처 매핑 정보
	FbxSurfaceMaterial : 재질정보
	FbxCharacter : 미리 등록된 사람/동물 등의 스켈톤 시스템
		FbxCharacterLink, FbxControlSet 로 조작
	FbxCharacterPose : 캐릭터의 포즈, 계층구조
		노드의 기본 위치만 고려, 애니메이션 데이터 무시됨
	FbxGeometry -> FbxMesh
		용어
			제어점(Control Point) : 정점(Vertex)
			다각형 정점(Polygon Vertex) : 제어점에 대한 인덱스
			다각형(Polygon) : 다각형 정점 집합(최소 3개)
	
애니메이션
	FbxAnimStack(애니메이션 스택)
	FbxAnimLayer(애니메이션 레이어)
	FbxAnimCurvNode(애니메이션 커브노드)
	FbxAnimCurv(애니메이션 커브)
	FbxAnimCurvKey(애니메이션 커브 키)
