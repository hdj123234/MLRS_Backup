#include "stdafx.h"
#include "PartLoader_Guide.h"

#include"..\MLRS_Client\MyUtility.h"

CPartLoader_Guide::CPartLoader_Guide()
{
}


CPartLoader_Guide::~CPartLoader_Guide()
{
}

void CPartLoader_Guide::setGuideMap(const EMyType_ModelType eModelType)
{
	m_GuideMap.clear();
	switch (eModelType)
	{
	case EMyType_ModelType::eModelType_MainMecha:
		m_GuideMap["guide_shoot1"] = EMyType_Guide::eGuide_Weapon1;
		m_GuideMap["guide_shoot2"] = EMyType_Guide::eGuide_Weapon2;
		break;

	}
}

void CPartLoader_Guide::visitNode(FbxNode * pNode)
{
	const unsigned int nNumberOfAttribute = pNode->GetNodeAttributeCount();
	for (unsigned int i = 0; i < nNumberOfAttribute; i++)
	{
		FbxNodeAttribute * pAttribute = pNode->GetNodeAttributeByIndex(i);
		auto type = pAttribute->GetAttributeType();
		switch (pAttribute->GetAttributeType())
		{
		case FbxNodeAttribute::EType::eMesh:
			visitMesh(static_cast<FbxMesh *>(pAttribute));
		}
	}
	//���ȣ��
	const unsigned int nNumberOfChild = pNode->GetChildCount();
	for (unsigned int i = 0; i < nNumberOfChild; ++i)
		visitNode(pNode->GetChild(i));
}

void CPartLoader_Guide::visitMesh(FbxMesh * pMesh)
{
	using namespace DirectX;
	if (!pMesh)	return;

	FbxNode * pNode = pMesh->GetNode();

	std::string sName = pNode->GetName();
	auto iter = m_GuideMap.find(CUtility_String::convertToLower(sName));
	if (iter == m_GuideMap.end())	return;

	// it is not inherited by the children.   
	const FbxVector4 lT = pNode->GetGeometricTranslation(FbxNode::eSourcePivot);
	const FbxVector4 lR = pNode->GetGeometricRotation(FbxNode::eSourcePivot);
	const FbxVector4 lS = pNode->GetGeometricScaling(FbxNode::eSourcePivot);

	FbxAMatrix mGeometryOffset = FbxAMatrix(lT, lR, lS);
	XMMATRIX mMatrix_Offset = (XMMATRIX(
		static_cast<float>(mGeometryOffset[0][0]), static_cast<float>(mGeometryOffset[0][1]), static_cast<float>(mGeometryOffset[0][2]), static_cast<float>(mGeometryOffset[0][3]),
		static_cast<float>(mGeometryOffset[1][0]), static_cast<float>(mGeometryOffset[1][1]), static_cast<float>(mGeometryOffset[1][2]), static_cast<float>(mGeometryOffset[1][3]),
		static_cast<float>(mGeometryOffset[2][0]), static_cast<float>(mGeometryOffset[2][1]), static_cast<float>(mGeometryOffset[2][2]), static_cast<float>(mGeometryOffset[2][3]),
		static_cast<float>(mGeometryOffset[3][0]), static_cast<float>(mGeometryOffset[3][1]), static_cast<float>(mGeometryOffset[3][2]), static_cast<float>(mGeometryOffset[3][3])));

	FbxAMatrix &mGlobal = pNode->EvaluateGlobalTransform();

	XMMATRIX mMatrix_FBXGlobal = XMLoadFloat4x4(&m_mGlobalTransform);
	//	XMMATRIX mMatrix_Axis = XMLoadFloat4x4(&m_mConvertAxis);

	XMMATRIX mMatrix_Pose;
	if (m_pmPoses->find(sName) == m_pmPoses->end())
	{
		mMatrix_Pose = XMMATRIX(
			static_cast<float>(mGlobal[0][0]), static_cast<float>(mGlobal[0][1]), static_cast<float>(mGlobal[0][2]), static_cast<float>(mGlobal[0][3]),
			static_cast<float>(mGlobal[1][0]), static_cast<float>(mGlobal[1][1]), static_cast<float>(mGlobal[1][2]), static_cast<float>(mGlobal[1][3]),
			static_cast<float>(mGlobal[2][0]), static_cast<float>(mGlobal[2][1]), static_cast<float>(mGlobal[2][2]), static_cast<float>(mGlobal[2][3]),
			static_cast<float>(mGlobal[3][0]), static_cast<float>(mGlobal[3][1]), static_cast<float>(mGlobal[3][2]), static_cast<float>(mGlobal[3][3]));
	}
	else
		mMatrix_Pose = XMLoadFloat4x4(&m_pmPoses->at(sName));

	XMMATRIX mMatrix_OPG = mMatrix_Offset*mMatrix_Pose*mMatrix_FBXGlobal;
	DirectX::XMFLOAT4X4 mResult;
	XMStoreFloat4x4(&mResult, mMatrix_OPG);

	m_Retval.m_GuideMap[iter->second].m_vPos = XMFLOAT3(mResult.m[3][0], mResult.m[3][1], mResult.m[3][2]);
//	//����
//	FbxVector4 * pControlPoints = pMesh->GetControlPoints();
//	const unsigned int nNumberOfControlPoint = pMesh->GetControlPointsCount(); 
//	for (unsigned int i = 0; i < nNumberOfControlPoint; ++i)
//	{
//		XMFLOAT3 vPosition;
//		XMVECTOR vVector = XMVectorSet(
//			static_cast<float>(pControlPoints[i][0]),
//			static_cast<float>(pControlPoints[i][1]),
//			static_cast<float>(pControlPoints[i][2]), 1);
//
//		 
//		vVector = XMVector3TransformCoord(vVector, mMatrix_OPG);
//
//		XMStoreFloat3(&vPosition, vVector);  
//	} 
}



CMyType_Guides CPartLoader_Guide::loadGuide(
	const EMyType_ModelType eModelType, 
	FbxScene * pScene, 
	const std::map<std::string, DirectX::XMFLOAT4X4>& rmPoses, 
	const DirectX::XMFLOAT4X4 & rmGlobalTransform)
{
	m_Retval.m_eModelType = eModelType;

	m_mGlobalTransform = rmGlobalTransform;
	setGuideMap(eModelType);
	m_pmPoses = &rmPoses;

	visitNode(pScene->GetRootNode());

	return m_Retval;
}

void CPartLoader_Guide::clear()
{
	m_GuideMap.clear();
	m_pmPoses = nullptr;
	m_Retval.m_eModelType = EMyType_ModelType::eModelType_ETC;
	m_Retval.m_GuideMap.clear();
}
