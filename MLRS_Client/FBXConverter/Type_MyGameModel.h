#pragma once
#ifndef HEADER_TYPE_MYGAMEMODEL
#define HEADER_TYPE_MYGAMEMODEL

#ifndef HEADER_DIRECTXMATH
#include<DirectXMath.h>
#endif

#ifndef HEADER_STL
#include<string>
#include<vector>
#include<array>
#include<set>
#include<map>
#include<algorithm>
#endif

enum EMyType_ModelType : char {
	eModelType_ETC=0,
	eModelType_MainMecha = 1,
	eModelType_Missile = 2,
};
enum EMyType_Guide : char{
	eGuide_Weapon1,
	eGuide_Weapon2,
};

struct CMyType_GuideValue {
	DirectX::XMFLOAT3 m_vPos;
};

struct CMyType_Guides {
	EMyType_ModelType m_eModelType;
	std::map<EMyType_Guide, CMyType_GuideValue > m_GuideMap;
};

struct CMyType_Vertex {
	DirectX::XMFLOAT3 m_vPosition;
	DirectX::XMFLOAT4 m_vWeights;
	DirectX::XMUINT4 m_vBoneIndex;
	CMyType_Vertex()
		:m_vPosition(0,0,0),
		m_vWeights(0, 0, 0, 0),
		m_vBoneIndex(0,0,0,0)
	{}
};

struct CMyType_VertexIndex {
	unsigned int m_nVertexIndex;
	unsigned int m_nUVIndex; 
	DirectX::XMFLOAT3 m_vNormal;
	DirectX::XMFLOAT3 m_vTangent;

	CMyType_VertexIndex()
		:m_nVertexIndex(0),
		m_nUVIndex(0),
		m_vNormal(DirectX::XMFLOAT3(0,0,0)),
		m_vTangent(DirectX::XMFLOAT3(0, 0, 0)) 
	{}
	CMyType_VertexIndex(const unsigned int nVertexIndex,
		const unsigned int nUVIndex, 
		const DirectX::XMFLOAT3 &rvNormal,
		const DirectX::XMFLOAT3 &rvTangent)
		:m_nVertexIndex(nVertexIndex),
		m_nUVIndex(nUVIndex), 
		m_vNormal(rvNormal),
		m_vTangent(rvTangent)
	{	}
};

struct CMyType_Mesh {
	std::string m_sName;
	std::vector<CMyType_Vertex> m_Vertices;
	std::vector<DirectX::XMFLOAT2> m_vUVs;
	std::vector<unsigned int> m_nMaterialIndices;	//byPolygon
	std::vector<CMyType_VertexIndex> m_Indices;
	std::array<DirectX::XMFLOAT3,2> m_vAABB;
};

struct CMyType_Bone {
	int m_nParentIndex;
	DirectX::XMFLOAT4X4 m_mTransform_ToLocal;

	CMyType_Bone()
		:m_nParentIndex(-1), 
		m_mTransform_ToLocal(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1)
	{}
	CMyType_Bone(const int nParentIndex,
		const DirectX::XMFLOAT4X4 &rmTransform_Local)
		:m_nParentIndex(nParentIndex),
		m_mTransform_ToLocal(rmTransform_Local)
	{}
};

class CMyType_Bones : private std::vector<std::pair<std::string, CMyType_Bone>> {
private:
	typedef std::pair<std::string, CMyType_Bone> ThisNode;
	typedef std::vector<ThisNode> ThisVector;
public:

	//없을 경우 -1 반환
	const int findBoneIndex(const std::string &rsBoneName)
	{
		auto iter = std::find_if(ThisVector::begin(), ThisVector::end(),
			[&rsName = rsBoneName](const ThisNode &rNode) {
			return rNode.first == rsName;
		});
		if (iter == ThisVector::end())	return -1;
		return static_cast<const int>(iter - ThisVector::begin());
	}
	const bool addBone(	const std::string &rsName,
						const DirectX::XMFLOAT4X4 &rmTransform_ToLocal, 
						const std::string &rsParentName)
	{

		int nParentIndex = findBoneIndex(rsParentName);
		if (nParentIndex == -1)	return false;

		ThisVector::push_back(std::make_pair(rsName, CMyType_Bone(nParentIndex, rmTransform_ToLocal)));
		return true;
	}
	const bool addBone(	const std::string &rsName,
						const DirectX::XMFLOAT4X4 &rmTransform_ToLocal)	//root
	{
		ThisVector::push_back(std::make_pair(rsName, CMyType_Bone(-1, rmTransform_ToLocal)));
		return true;
	}

	//const bool addBone(CTreeNode_Skeleton *pBone)
	//{
	//	if (!pBone->m_pParent)	//루트노드일 경우
	//		ThisVector::push_back(std::make_pair(pBone->m_sName, CMyType_Bone(-1, pBone->m_mTransform_ToLocal)));
	//	else
	//	{
	//		int nParentIndex = findBoneIndex(pBone->m_pParent->m_sName);
	//		if (nParentIndex == -1)	return false;
	//
	//		ThisVector::push_back(std::make_pair(pBone->m_sName, CMyType_Bone(nParentIndex, pBone->m_mTransform_ToLocal)));
	//	}
	//	return true;
	//}
	
	const unsigned int getNumberOfBone()const { return static_cast<unsigned int>(ThisVector::size()); }

	ThisVector & getContainer() { return *this; }
	const ThisVector & getContainer() const { return *this; }

	void reserve(size_t nSize) { ThisVector::reserve(nSize); }
	void clear() { ThisVector::clear(); }

};

enum EMyType_Map : char {
	eDiffuse,
	eNormal
};

struct CMyType_Material {
	std::string m_sName;
	bool				m_bPhong;
	DirectX::XMFLOAT4	m_vAmbient;
	DirectX::XMFLOAT4	m_vDiffuse;
	DirectX::XMFLOAT4	m_vSpecular;
	DirectX::XMFLOAT4	m_vEmissive;
	DirectX::XMFLOAT4	m_vReflection;
	float				m_fShininess;
	std::map<EMyType_Map, std::string> m_Maps;

	CMyType_Material()
		:m_bPhong(false),
		m_vAmbient		({0,0,0,0}),
		m_vDiffuse		({0,0,0,0}),
		m_vSpecular		({0,0,0,0}),
		m_vEmissive		({0,0,0,0}),
		m_vReflection	({0,0,0,0}),
		m_fShininess	(0)
	{
	}

	CMyType_Material(const std::string &rsName,
		const DirectX::XMFLOAT4	&rvAmbient,
		const DirectX::XMFLOAT4	&rvDiffuse,
		const DirectX::XMFLOAT4	&rvEmissive)
		:m_sName(rsName),
		m_bPhong(false),
		m_vAmbient(rvAmbient),
		m_vDiffuse(rvDiffuse),
		m_vSpecular({ 1,1,1,1 }),
		m_vEmissive(rvEmissive)
	{
	}

	CMyType_Material(const std::string &rsName,
		const DirectX::XMFLOAT4	&rvAmbient,
		const DirectX::XMFLOAT4	&rvDiffuse,
		const DirectX::XMFLOAT4	&rvSpecular,
		const DirectX::XMFLOAT4	&rvEmissive,
		const DirectX::XMFLOAT4	&rvReflection,
		const float					fShininess)
		:m_sName(rsName),
		m_bPhong(true),
		m_vAmbient(rvAmbient),
		m_vDiffuse(rvDiffuse),
		m_vSpecular(rvSpecular),
		m_vEmissive(rvEmissive),
		m_vReflection(rvReflection),
		m_fShininess(fShininess)
	{
	}
};

struct CMyType_GameModel {
	std::vector<CMyType_Mesh> m_Meshs;
	CMyType_Bones m_Bones;
	std::vector<std::vector<DirectX::XMFLOAT4X4>> m_AnimTransform;	//프레임별 본의 변환행렬
	std::vector<CMyType_Material> m_Materials;
	CMyType_Guides m_Guides;
};


#endif