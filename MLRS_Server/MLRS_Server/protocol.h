#pragma once
#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <Windows.h>
#include "UserDefine.h"
#define BUFSIZE 2048
#define MAX_PACKET_SIZE  2048
#define MAX_USER 100
#define SERVERPORT 9000

#define MAX_NICK_SIZE 24
#define MAX_STR_SIZE 120

#define WORD_NODATA 65535

#define CS_SET_NICK		1
#define CS_CHAT			2
#define CS_ROOM_PAGE	3
#define CS_CREATE_ROOM	4
#define CS_ENTER_ROOM	5
#define CS_LEAVE_ROOM	6
#define CS_CHANGE_MECHA 7
#define CS_READY		8
#define CS_START		9
#define CS_ASSIST		10
#define CS_DEBUG_ASSIST 126
#define CS_MOVE 20
#define CS_AIM_MOVE1 21
#define CS_AIM_MOVE2 22
#define CS_AIM_MOVE3 23
#define CS_CHANGE_WEAPON 29
#define CS_SHOOT1 30
#define CS_SHOOT2 31
#define CS_SHOOT3 32

typedef unsigned char FLAG_8;


#pragma pack (push, 1)

// Client -> Server Packet

#define CS_PACKET_NICKNAME_TYPELEN 2
struct cs_packet_nickname {
	BYTE size;
	BYTE type;
	WCHAR nickname[MAX_NICK_SIZE];
};

#define CS_PACKET_CHAT_TYPELEN 2
struct cs_packet_chat {
	BYTE size;
	BYTE type;
	WCHAR message[MAX_STR_SIZE];
};

struct cs_packet_room_page {
	BYTE size;
	BYTE type;
//	BYTE page;	// 요청할 룸목록 페이지
	cs_packet_room_page() {
		size = sizeof(cs_packet_room_page);
		type = CS_ROOM_PAGE;
	}
};

#define CS_PACKET_CREATE_ROOM_TYPELEN 3
struct cs_packet_create_room {
	BYTE size;
	BYTE type;
	cs_packet_create_room() {
		size = sizeof(cs_packet_create_room);
		type = CS_CREATE_ROOM;
	}
//	BYTE maxplayer;
//	WCHAR title[ROOM_MAX_TITLE_LEN];
};

struct cs_packet_enter_room {
	BYTE size;
	BYTE type;
	WORD roomid;
	cs_packet_enter_room() {
		size = sizeof(cs_packet_enter_room);
		type = CS_ENTER_ROOM;
	}
};

struct cs_packet_leave_room {
	BYTE size;
	BYTE type;
	cs_packet_leave_room() {
		size = sizeof(cs_packet_leave_room);
		type = CS_LEAVE_ROOM;
	}	
};

struct cs_packet_change_mecha {
	BYTE size;
	BYTE type;
	BYTE select_mecha;
	cs_packet_change_mecha() {
		size = sizeof(cs_packet_change_mecha);
		type = CS_CHANGE_MECHA;
	}
};

struct cs_packet_ready {
	BYTE size;
	BYTE type;
	BYTE is_ready;
	cs_packet_ready() {
		size = sizeof(cs_packet_ready);
		type = CS_READY;
	}	
};

/* 모두 레디하면 자동으로 게임 스타트 하는 형태로 갈듯
struct cs_packet_start {
	BYTE size;
	BYTE type;
};
*/


enum MOVE_TYPE : FLAG_8
{
	MT_MOVE = 1,
	MT_JUMP = 2,
	MT_BOOST = 4
};
typedef FLAG_8 MOVE_FLAG;
struct cs_packet_move {
	BYTE size;
	BYTE type;
	MOVE_FLAG move;
	float direction_x;
	float direction_z;
	cs_packet_move() {
		size = sizeof(cs_packet_move);
		type = CS_MOVE;
	}
};

// CS_AIM_MOVE 21
struct cs_packet_aim_move1 {
	BYTE size;
	BYTE type;
	MOVE_FLAG move;
	float direction_x;
	float direction_z;
	float look_x;
	float arm_y;
	float look_z;
	cs_packet_aim_move1() {
		size = sizeof(cs_packet_aim_move1);
		type = CS_AIM_MOVE1;
	}
};

struct cs_packet_aim_move2 {
	BYTE size;
	BYTE type;
	float look_x; // 바라보는 방향 x
	float angle_y; // 총구 각도 y
	float look_z; // 바라보는 방향 z
	cs_packet_aim_move2() {
		size = sizeof(cs_packet_aim_move2);
		type = CS_AIM_MOVE2;
	}
};

struct cs_packet_aim_move3 {
	BYTE size;
	BYTE type;
	// 왠지 아무 데이터도 필요없을듯
	cs_packet_aim_move3() {
		size = sizeof(cs_packet_aim_move3);
		type = CS_AIM_MOVE3;
	}
};

struct cs_packet_change_weapon {
	BYTE size;
	BYTE type;
	char w_type;
	cs_packet_change_weapon() {
		size = sizeof(cs_packet_change_weapon);
		type = CS_CHANGE_WEAPON;
	}
};

// CS_SHOOT 22
// CS_SHOOT2 23
struct cs_packet_shoot {
	BYTE size;
	BYTE type;
	BYTE m_type;
	float point_x; // 총구 좌표 x
	float point_y; // 총구 좌표 y
	float point_z; // 총구 좌표 z
	float direction_x; // 미사일 방향 x
	float direction_y; // 미사일 방향 y
	float direction_z; // 미사일 방향 z
	cs_packet_shoot() {
		size = sizeof(cs_packet_shoot);
		type = CS_SHOOT1;
	}
}; 

struct cs_packet_shoot_multiple {
	BYTE size;
	BYTE type;
	float pos[3];
	float look[3];
	float up[3];
	float right[3];
	cs_packet_shoot_multiple() {
		size = sizeof(cs_packet_shoot_multiple);
		type = CS_SHOOT2;
	}
};


struct cs_packet_shoot_lockon {
	BYTE size;
	BYTE type;
	WORD lock_on[12];
	cs_packet_shoot_lockon() {
		size = sizeof(cs_packet_shoot_lockon);
		type = CS_SHOOT3;
	}
};

#define SC_CLIENTID			0
#define SC_SET_NICK 		1
#define SC_ROOMLIST			2
// #define SC_USERINFO			3
#define SC_CHAT				4
#define SC_IS_ENTER_ROOM	5
#define SC_INIT_ROOMDATA	6
#define SC_ENTER_USER		7
#define SC_LEAVE_USER		8
#define SC_CHANGE_MECHA		9
#define SC_READY			10
#define SC_GAME_START		11
#define SC_POS           15
#define SC_PUT_PLAYER    116
#define SC_REMOVE_PLAYER 117

#define SC_JOIN_PLAYER		20
#define SC_PLAYER_MOVE		21
#define SC_FIRE_MOVE1		22
#define SC_CHANGE_WEAPON	23
#define SC_FIRE_MOVE2		24
#define SC_WEAPON_FIRE		25
#define SC_ENEMY_CREATE		40
#define SC_ENEMY_MOVE		41
#define SC_ENEMY_STATE		42
#define SC_BOSS_CREATE		45
#define SC_BOSS_MOVE		46
#define SC_BOSS_STATE		47
#define SC_MISSILE_CREATE	50
#define SC_MISSILE_MOVE		51
#define SC_MISSILE_BOOM		52
#define SC_OBJECT_CREATE	60
#define SC_OBJECT_MOVE		61
#define SC_OBJECT_REMOVE	80
#define SC_EVENT		90
#define SC_HP		91
#define SC_PLAYER_DOWN	92
#define SC_SCRIPT		95
#define SC_TIMER		96
#define SC_DEBUG_TEST		100

// Server -> Client Packet


struct sc_packet_client_id {
	BYTE size;
	BYTE type;
	WORD id;	// 클라이언트의 ID
	sc_packet_client_id() {
		size = sizeof(sc_packet_client_id);
		type = SC_CLIENTID;
	}
};

struct sc_packet_room_info {
	WORD roomid;
	BYTE title_id;
	BYTE curp;
};
#define SC_PACKET_ROOMINFO_TYPELEN 7
// 로비에서 방의 정보를 보내는 패킷
struct sc_packet_room_list {
	BYTE size;
	BYTE type;
	BYTE list_size;
	sc_packet_room_info rlist[25];
//	WCHAR title[ROOM_MAX_TITLE_LEN];
	sc_packet_room_list() {
		type = SC_ROOMLIST;
	}
};

/*
#define SC_LOCATION_LOBBY 0
#define SC_LOCATION_ROOM 1
#define SC_PACKET_USERINFO_TYPELEN 5
struct sc_packet_userinfo {
	BYTE size;
	BYTE type;
	BYTE location;
	WORD id;
	WCHAR nickname[MAX_NICK_SIZE];
	sc_packet_userinfo() {
		type = SC_USERINFO;
	}	
};
*/

#define SC_PACKET_CHAT_TYPELEN 4
struct sc_packet_chat {
	BYTE size;
	BYTE type;
	WORD id;
	WCHAR message[MAX_STR_SIZE];
	sc_packet_chat() {
		type = SC_CHAT;
	}
};

/*
#define CHANGE_ROOMINFO_ADDPLAYER 0
#define CHANGE_ROOMINFO_LEAVEPLAYER 1
#define CHANGE_ROOMINFO_REMOVEROOM 2
struct sc_packet_change_roominfo {
	BYTE size;
	BYTE type;
	WORD roomid;
	BYTE change;
};
*/

#define SC_ENTER_RESULT_NOROOM 0
#define SC_ENTER_RESULT_OK 1
#define SC_ENTER_RESULT_FULL 2
#define SC_ENTER_RESULT_PLAYING 3
#define SC_ENTER_RESULT_MAXROOM 4
struct sc_packet_enter_room {
	BYTE size;
	BYTE type;
	BYTE result;
	sc_packet_enter_room() {
		size = sizeof(sc_packet_enter_room);
		type = SC_IS_ENTER_ROOM;
	}
};

struct sc_packet_user_data {
	WORD id;	// 65535 이면 비어있음
	BYTE mecha;
	BYTE is_ready;
};
struct sc_packet_room_data {
	BYTE size;
	BYTE type;
	WORD roomid;	// 방 ID
	sc_packet_user_data user[4];
	sc_packet_room_data() {
		size = sizeof(sc_packet_room_data);
		type = SC_INIT_ROOMDATA;
	}
};

struct sc_packet_enter_user {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE slot;
	sc_packet_enter_user() {
		size = sizeof(sc_packet_enter_user);
		type = SC_ENTER_USER;
	}	
};

struct sc_packet_leave_user {
	BYTE size;
	BYTE type;
	WORD id;
	sc_packet_leave_user() {
		size = sizeof(sc_packet_leave_user);
		type = SC_LEAVE_USER;
	}
};

struct sc_packet_change_mecha {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE select_mecha;
	sc_packet_change_mecha() {
		size = sizeof(sc_packet_change_mecha);
		type = SC_CHANGE_MECHA;
	}
};

struct sc_packet_ready {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE is_ready;
	sc_packet_ready() {
		size = sizeof(sc_packet_ready);
		type = SC_READY;
	}	
};

struct sc_packet_game_start {
	BYTE size;
	BYTE type;
	sc_packet_game_start() {
		size = sizeof(sc_packet_game_start);
		type = SC_GAME_START;
	}
};

struct sc_packet_enter_player {
	BYTE size;
	BYTE type;
	WORD id;
};

struct sc_packet_remove_player {
	BYTE size;
	BYTE type;
	WORD id;
	sc_packet_remove_player() {
		size = sizeof(sc_packet_remove_player);
		type = SC_REMOVE_PLAYER;
	}
};

struct sc_packet_set_nick {
	BYTE size;
	BYTE type;
	WORD id;
	char nickname[MAX_NICK_SIZE];
};

// SC_JOIN_PLAYER		20
struct sc_packet_join {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE m_type;
	float pos_x;
	float pos_y;
	float pos_z;
	float look_x;
	float look_z;
	sc_packet_join() {
		size = sizeof(sc_packet_join);
		type = SC_JOIN_PLAYER;
	}
};

// SC_PLAYER_MOVE		21
struct sc_packet_move {
	BYTE size;
	BYTE type;
	WORD id;	
	BYTE state;	// { OBJECT_MOVE = 0, OBJECT_STOP = 1 };
	MOVE_FLAG move;
	float pos_x;
	float pos_y;
	float pos_z;
	sc_packet_move() {
		size = sizeof(sc_packet_move);
		type = SC_PLAYER_MOVE;
	}
};

//	SC_FIRE_WEAPON		22
struct sc_packet_aim_move1 {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE state;
	MOVE_FLAG move;
//	BYTE weapon_type;	// 1 : 1번 무기, 2 : 2번무기...
	float pos_x;
	float pos_y;
	float pos_z;
	float look_x;
	float arm_y;
	float look_z;
	sc_packet_aim_move1() {
		size = sizeof(sc_packet_aim_move1);
		type = SC_FIRE_MOVE1;
	}
};

struct sc_packet_aim_move2 {
	BYTE size;
	BYTE type;
	WORD id;
	float look_x;
	float arm_y;
	float look_z;
	sc_packet_aim_move2() {
		size = sizeof(sc_packet_aim_move2);
		type = SC_FIRE_MOVE2;
	}
};


struct sc_packet_change_weapon {
	BYTE size;
	BYTE type;
	WORD id;
	char w_type;
	sc_packet_change_weapon() {
		size = sizeof(sc_packet_change_weapon);
		type = SC_CHANGE_WEAPON;
	}
};

struct sc_fire_packet {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE w_type; // 무기번호
	BYTE point; // 총구번호
	// !@#$%^
	sc_fire_packet() {
		size = sizeof(sc_fire_packet);
		type = SC_WEAPON_FIRE;
	}
};

// SC_ENEMY_CREATE		40
struct sc_packet_ecreate {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE m_type;
	float pos_x;
	float pos_y;
	float pos_z;
	float look_x;
	float look_y;
	float look_z;
	sc_packet_ecreate() {
		size = sizeof(sc_packet_ecreate);
		type = SC_ENEMY_CREATE;
	}
};

// SC_ENEMY_MOVE	41
struct sc_packet_emove {
	BYTE size;
	BYTE type;
	WORD id;	
	BYTE move;	// { OBJECT_MOVE, OBJECT_STOP };
	float pos_x;
	float pos_y;
	float pos_z;
	sc_packet_emove() {
		size = sizeof(sc_packet_emove);
		type = SC_ENEMY_MOVE;
	}
};

// SC_ENEMY_STATE		42
struct sc_packet_estate {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE state;	
	sc_packet_estate() {
		size = sizeof(sc_packet_estate);
		type = SC_ENEMY_STATE;
	}
};

// SC_BOSS_CREATE		45
struct sc_packet_bcreate {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE m_type;
	float pos_x;
	float pos_y;
	float pos_z;
	float look_x;
	float look_y;
	float look_z;
	sc_packet_bcreate() {
		size = sizeof(sc_packet_bcreate);
		type = SC_BOSS_CREATE;
	}
};

// SC_BOSS_MOVE	46
struct sc_packet_bmove {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE move;	// { OBJECT_MOVE, OBJECT_STOP };
	float pos_x;
	float pos_y;
	float pos_z;
	sc_packet_bmove() {
		size = sizeof(sc_packet_bmove);
		type = SC_BOSS_MOVE;
	}
};

// SC_BOSS_STATE		47
struct sc_packet_bstate {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE state;
	sc_packet_bstate() {
		size = sizeof(sc_packet_bstate);
		type = SC_BOSS_STATE;
	}
};

// SC_MISSILE_CREATE	50
struct sc_packet_mcreate {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE m_type;
	float pos_x;
	float pos_y;
	float pos_z;
	float look_x;
	float look_y;
	float look_z;
	sc_packet_mcreate() {
		size = sizeof(sc_packet_mcreate);
		type = SC_MISSILE_CREATE;
	}
};

// SC_MISSILE_MOVE		51
struct sc_packet_mmove {
	BYTE size;
	BYTE type;
	WORD id;
	float pos_x;
	float pos_y;
	float pos_z;
	sc_packet_mmove() {
		size = sizeof(sc_packet_mmove);
		type = SC_MISSILE_MOVE;
	}
};

// SC_MISSILE_BOOM		52
struct sc_packet_mboom {
	BYTE size;
	BYTE type;
	WORD id;
	float pos_x;
	float pos_y;
	float pos_z;
	sc_packet_mboom() {
		size = sizeof(sc_packet_mboom);
		type = SC_MISSILE_BOOM;
	}
};

// SC_OBJECT_CREATE	60
struct sc_packet_ocreate {
	BYTE size;
	BYTE type;
	WORD id;
	BYTE o_type;
	float pos_x;
	float pos_y;
	float pos_z;
	float look_x;
	float look_y;
	float look_z;
	sc_packet_ocreate() {
		size = sizeof(sc_packet_ocreate);
		type = SC_OBJECT_CREATE;
	}
};

// SC_OBJECT_MOVE		61
struct sc_packet_omove {
	BYTE size;
	BYTE type;
	WORD id;
	float pos_x;
	float pos_y;
	float pos_z;
	float look_x;
	float look_y;
	float look_z;
	sc_packet_omove() {
		size = sizeof(sc_packet_omove);
		type = SC_OBJECT_MOVE;
	}
};

// SC_OBJECT_REMOVE	80
struct sc_packet_objRemove {
	BYTE size;
	BYTE type;
	BYTE obj_type;	// enum object_Type;
	WORD id;
	sc_packet_objRemove() {
		size = sizeof(sc_packet_objRemove);
		type = SC_OBJECT_REMOVE;
	}
};

// SC_EVENT	90
struct sc_packet_event {
	BYTE size;
	BYTE type;
	WORD event_type;	// enum EVENTTYPE
	WORD id;
	sc_packet_event() {
		size = sizeof(sc_packet_event);
		type = SC_EVENT;
	}
};

struct sc_packet_HP {
	BYTE size;
	BYTE type;
	BYTE obj_type;
	WORD id;
	float hp;
	sc_packet_HP() {
		size = sizeof(sc_packet_HP);
		type = SC_HP;
	}
};

struct sc_packet_pdown {
	BYTE size;
	BYTE type;
	WORD id;
	sc_packet_pdown() {
		size = sizeof(sc_packet_pdown);
		type = SC_PLAYER_DOWN;
	}
};

struct sc_packet_script {
	BYTE size;
	BYTE type;
	WORD string_id;
	sc_packet_script() {
		size = sizeof(sc_packet_script);
		type = SC_SCRIPT;
	}
};

struct sc_packet_timer {
	BYTE size;
	BYTE type;
	unsigned int timer;
	sc_packet_timer() {
		size = sizeof(sc_packet_timer);
		type = SC_TIMER;
	}
};

struct sc_packetDEBUG {
	BYTE size;
	BYTE type;
	char nickname[125];
};


#pragma pack (pop)


#endif 