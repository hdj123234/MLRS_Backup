#pragma once
#ifndef ENEMY_BOSS_H
#define ENEMY_BOSS_H

#include "Enemy.h"

class CEnemy_Boss : public CEnemy
{
public:
	CEnemy_Boss();
	~CEnemy_Boss();

	void putBoss(int _type, FLOAT3 _pos, FLOAT3 _look);
	vector<FLOAT3>* getPoint();
	bool Hit(float _damage);

};


#endif // !ENEMY_BOSS_H