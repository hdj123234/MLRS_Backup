#include "stdafx.h"
#include "Missile.h"
#include "GameData\GameData.h"


CMissile::CMissile()
{
	pthis = this;
	type = 0;
	owner_type = OT_MASTERLESS;
	actcount = 0;
	state = 0;
	is_translate = 0;
	damage = 0;
	pType = nullptr;
	pAI = nullptr;
	myTarget = nullptr;
	objectType = OBJECT_MISSILE;
}


CMissile::~CMissile()
{
}

void CMissile::InitMissile(int _id)
{
	id = _id;
}

void CMissile::putMissile(FLOAT3 _pos, FLOAT3 _dir, int _owner_id, int _type, OWNER_TYPE _owner_type, CGameObject* _target)
{
	type = _type;
	owner_id = _owner_id;
	owner_type = _owner_type;
	pType = CGameData::getInstance().getMissile(type);
	pAI = CGameData::getInstance().getAI(pType->getAInumber());
	pos = _pos;
	dir = _dir;
	myTarget = _target;
	speed = 0;
	is_translate = OBJECT_CREATE;
	damage = pType->getDamage();
	gravity = 0;

	is_use = true;
}

void CMissile::removeMissile()
{
	is_use = false;
	type = 0;
	owner_type = OT_MASTERLESS;
	dir.clear();
	speed = 0;
	actcount = 0;
	state = 0;
	is_translate = OBJECT_NOSHIFT;
	damage = 0;

	pType = nullptr;
	pAI = nullptr;
	myTarget = nullptr;


}

void CMissile::Accel()
{
	speed += pType->getAcceleration();
	if (speed > pType->getMaxSpeed())
		speed = pType->getMaxSpeed();
}

void CMissile::DeAccel()
{
	speed -= pType->getAcceleration();
	if (speed <= 0)
		speed = 0;
}

void CMissile::Crash()
{
	is_translate = OBJECT_REMOVE;
	state = MS_BOOM;
}

void CMissile::Act(CGameSpace & space)
{
	pAI->Act(space, pthis);
}

void CMissile::Move()
{
	if (is_translate == OBJECT_REMOVE) return;
	if (speed > 0)
		is_translate = OBJECT_MOVE;
	else {
		if (is_translate == OBJECT_MOVE)
			is_translate = OBJECT_STOP;
		else
			is_translate = OBJECT_NOSHIFT;
		return;
	}

	pos += dir*speed;
//	if (id == 0)
//	cout << "미사일 움직여욧 : " << pos.getX() << ", " << pos.getY() << ", " << pos.getZ() << endl;
}

void CMissile::onGravity()
{
	gravity += CGameData::getInstance().getConstData()->getGRAVITY_FPS();
	pos.getY() += gravity;
}
