#pragma once
#include<DirectXMath.h>

struct FLOAT3 {
	DirectX::XMFLOAT3 data;
	//float &x, &y, &z;
	//FLOAT3():x(data.x), y(data.y), z(data.z) { data.x = data.y = data.z = 0; }
	//FLOAT3(float _x, float _y, float _z) :x(data.x), y(data.y), z(data.z) {
	//	data.x = _x; data.y = _y; data.z = _z;
	//}
	FLOAT3() { data.x = data.y = data.z = 0; }
	FLOAT3(float _x, float _y, float _z) {
		data.x = _x; data.y = _y; data.z = _z;
	}
	FLOAT3(const DirectX::XMFLOAT3 &f) {
		data.x = f.x; data.y = f.y; data.z = f.z;
	}
	void clear() { data.x = 0; data.y = 0; data.z = 0; }
	DirectX::XMFLOAT3 getData() { return data; }
	void operator=(const FLOAT3 &f) {
		data.x = f.getX(); data.y = f.getY(); data.z = f.getZ();
	}
	void operator=(const DirectX::XMFLOAT3 &f) {
		data.x = f.x; data.y = f.y; data.z = f.z;
	}
	FLOAT3 operator+(const FLOAT3 &f) {
		using namespace DirectX;
		FLOAT3 result;
		XMStoreFloat3(&result.data, DirectX::XMLoadFloat3(&data) + DirectX::XMLoadFloat3(&f.data));
		return result;
	}
	FLOAT3 operator-(const FLOAT3 &f) {
		using namespace DirectX;
		FLOAT3 result;
		XMStoreFloat3(&result.data, DirectX::XMLoadFloat3(&data) - DirectX::XMLoadFloat3(&f.data));
		return result;
	}
	FLOAT3 operator*(const float &f) {
		using namespace DirectX;
		FLOAT3 result;
		XMStoreFloat3(&result.data, DirectX::XMLoadFloat3(&data) *f);
		return result;
	}
	//FLOAT3 operator*(const FLOAT3 &f) {
	//	return FLOAT3(data.x * f.data.x, data.y * f.data.y, data.z * f.data.z);
	//}
	//void operator+=(const float &f) {
	//	data.x += f; data.y += f; data.z += f;
	//}
	FLOAT3 & operator+=(const FLOAT3 &f) {
		using namespace DirectX;
		XMStoreFloat3(&data, DirectX::XMLoadFloat3(&data) + DirectX::XMLoadFloat3(&f.data));
		return *this;
	}
	FLOAT3 & operator-=(const FLOAT3 &f) {
		using namespace DirectX;
		XMStoreFloat3(&data, DirectX::XMLoadFloat3(&data) - DirectX::XMLoadFloat3(&f.data));
		return *this;
	}
	FLOAT3 & operator*=(const float &f) {
		using namespace DirectX;
		XMStoreFloat3(&data, DirectX::XMLoadFloat3(&data) *f);
		return *this;
	}
	FLOAT3 & operator*=(const FLOAT3 &f) {
		using namespace DirectX;
		XMStoreFloat3(&data, DirectX::XMLoadFloat3(&data) * DirectX::XMLoadFloat3(&f.data));
		return *this;
	}
	//FLOAT3 & operator*=(const FLOAT3 &f) {
	//	data.x *= f.data.x; data.y *= f.data.y; data.z *= f.data.z;
	//	return *this;
	//}
	bool Immovable() {
		return (data.x <= 0 && data.y <= 0 && data.z <= 0);
	}

	DirectX::XMFLOAT3& getXMFLOAT3() { return data; }
	FLOAT3 & Normalize() {
		using namespace DirectX;
		XMStoreFloat3(&data, XMVector3NormalizeEst(XMLoadFloat3(&data)));	//Est : 정확성이 낮고 속도가 빠름
		//float length = sqrt(((data.x*data.x) + (data.y*data.y) + (data.z*data.z)));
		//data.x /= length;
		//data.y /= length;
		//data.z /= length;
		return *this;
	}

	float getLength() {
		using namespace DirectX;
		return XMVectorGetX(XMVector3LengthEst(XMLoadFloat3(&data)));	//Est : 정확성이 낮고 속도가 빠름
		// sqrt(((data.x*data.x) + (data.y*data.y) + (data.z*data.z)));
	}
	float getLength_noSqrt() {
		using namespace DirectX;
		XMVECTOR v = XMLoadFloat3(&data);
		return XMVectorGetX(XMVector3Dot(v, v));
	}	// 제곱근 안씌운거
	FLOAT3 getRotateY(const FLOAT3 &_dir) {
		return FLOAT3((data.x * _dir.data.z) + (data.z * _dir.data.x), data.y, (-data.x * _dir.data.x) + (data.z * _dir.data.z));
	}
	const float dot(const FLOAT3& v)const {
		using namespace DirectX;
		return XMVectorGetX(XMVector3Dot(XMLoadFloat3(&data), XMLoadFloat3(&v.data)));
	}
	static const float dot(const FLOAT3& v1, const FLOAT3& v2) {
		using namespace DirectX;
		return XMVectorGetX(XMVector3Dot(XMLoadFloat3(&v1.data), XMLoadFloat3(&v2.data)));
	}
	static const FLOAT3 cross(const FLOAT3& v1, const FLOAT3& v2) {
		using namespace DirectX;
		FLOAT3 result;
		XMStoreFloat3(&result.data, XMVector3Cross(XMLoadFloat3(&v1.data), XMLoadFloat3(&v2.data)));
		return result;
	}

	float & getX() { return data.x; }
	const float & getX()const { return data.x; }
	float & getY() { return data.y; }
	const float & getY()const { return data.y; }
	float & getZ() { return data.z; }
	const float & getZ()const { return data.z; }
	void setX(float _x) { data.x = _x; }
	void setY(float _y) { data.y = _y; }
	void setZ(float _z) { data.z = _z; }
};

//enum object_translate { OBJECT_MOVE, OBJECT_STOP, OBJECT_NOSHIFT };

/*
DirectX::BoundingSphere s;
s.Center = DirectX::XMFLOAT3(1, 1, 1);
s.Radius = 1;

DirectX::BoundingOrientedBox b;
b.Center = DirectX::XMFLOAT3(1, 1, 1);
b.Extents = DirectX::XMFLOAT3(1, 1, 1);
DirectX::XMFLOAT4 q;
XMStoreFloat4(&q, DirectX::XMQuaternionRotationRollPitchYaw(0, 0, 0));
b.Orientation = q;

if (b.Intersects(s)) {
	//충돌
}
*/

class CGameObject
{
protected:
	bool is_use;
	int id;
	int objectType;

	FLOAT3 pos;

public:
	CGameObject();
	~CGameObject();
	CGameObject(int _id);

	int getObjectType();

	int getID() { return id; };
	bool is_Use() { return is_use; };
	FLOAT3 getPos() { return pos; };

	void setObjectType(int _type) { objectType = _type; }
	void setPos(FLOAT3 f) { pos = f; };
};

