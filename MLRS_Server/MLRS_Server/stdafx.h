#pragma once
#ifndef STDAFX_H
#define STDAFX_H
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <vector>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <thread>
#include <iostream>
#include <fstream>
#include <iterator>
#include <mutex>
#include <string>
#include <concurrent_queue.h>
#include <concurrent_vector.h>
#include <cmath>
#include <DirectXMath.h>

#endif 