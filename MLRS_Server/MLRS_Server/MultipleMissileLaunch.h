#pragma once
#include "GameObject.h"
class CMultipleMissileLaunch
{
	bool is_use;

	int slot;
	FLOAT3 look;
	DirectX::XMMATRIX m_matrix;

	int launch_frame;

public:
	CMultipleMissileLaunch();
	~CMultipleMissileLaunch();

	bool isUse() { return is_use; }
	void setMissileLaunch(int _slot, FLOAT3 _pos, FLOAT3 _look, FLOAT3 _up, FLOAT3 _right);
	int getLaunchFrame();	

	int getSlot() { return slot; }
	FLOAT3 getLook() { return look; }
	DirectX::XMMATRIX getRotationMatrix() { return m_matrix; };
	void endMissileLaunch();
};

