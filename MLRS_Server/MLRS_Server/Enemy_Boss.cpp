#include "stdafx.h"
#include "Enemy_Boss.h"


CEnemy_Boss::CEnemy_Boss()
{
	objectType = OBJECT_BOSS;
}


CEnemy_Boss::~CEnemy_Boss()
{
}


void CEnemy_Boss::putBoss(int _type, FLOAT3 _pos, FLOAT3 _look)
{
	type = _type;

	pos = _pos;
	direction = _look.Normalize();
	speed = 0;
	gravity = 0;
	pMecha = CGameData::getInstance().getBossMecha(type);
	pAI = CGameData::getInstance().getAI(pMecha->getAI());
	hp = pMecha->getHP();

	is_translate = OBJECT_CREATE;

	is_use = true;
}

vector<FLOAT3>* CEnemy_Boss::getPoint()
{
	return pMecha->getPoints();
}

bool CEnemy_Boss::Hit(float _damage)
{
	hp -= _damage;
	if (hp <= 0) {
		state = ES_DOWN;
		return true;
	}
	return false;
}
