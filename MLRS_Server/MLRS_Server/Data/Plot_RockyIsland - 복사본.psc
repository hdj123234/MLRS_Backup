#name rockyIsland
scene 0
	//() 나쁜놈 쳐들어왔다. 빨리 기지 확인하러 가봐라.
	setinitpos(-2163,-415)
	// setplayerpos(-2163,-415)
	setplayerpos(362,493)
	setobjectspeed(0,0,0.5,0)
	wait(3000)
	addsinglemob(1,-1362,-493.1640625)
	wait(1500)
	addmultimob(0,1,-346.6796875,-815.795,250)
	setobjectspeed(0,0,0,0)
	checkmob(0)
	addmultimob(0,15,0,0,200)
	wait(1000)
	break()

scene 1
	//( 저 봐라. 기지 개털리고 있다. 빨리 막아라
	checkmob(5)
	break()

scene 2
	//() 적의 증원이다. 막아라.
	wait(1000)
	addmultimob(0,10,0,0,250)
	wait(10000)
	checkmob(5)
	break()

scene 3
	//() 저기 저쪽에 컴뱃 오는거 같은데 조심해라. 빨라서 잡기 힘들다.
	wait(1000)
	addmultimob(1,10,-100,-500,250)
	checkmob(0)
	break()

scene 4
	//() 커맨드센터 띄울꺼다. 띄울동안 안쳐맞게 엄호해라.
	wait(5000)
	setobjectspeed(0,0,0.1,0)
	wait(5000)
	addsinglemob(0,100,10)
	wait(300)
	addsinglemob(0,100,30)
	wait(300)
	addsinglemob(0,100,50)
	wait(300)
	addsinglemob(0,100,70)
	wait(300)
	addsinglemob(0,100,90)
	wait(300)
	addsinglemob(0,100,110)
	wait(300)
	addsinglemob(0,100,130)
	wait(300)
	addsinglemob(0,100,150)
	wait(300)
	addsinglemob(0,100,170)
	wait(300)
	addsinglemob(0,100,190)
	wait(10000)
	setobjectspeed(0,0,0,0)
	checkmob(0)
	break()

scene 5
	//() 잘 막았다. 이제 도망칠꺼다. 엄호해라.
	wait(5000)
	setobjectspeed(0,0,0,-0.3)
	wait(5000)
	addmultimob(1,10,-100,-500,250)
	break()

#end rockyIsland

