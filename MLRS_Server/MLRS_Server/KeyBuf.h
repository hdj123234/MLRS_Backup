#pragma once
enum KEY_TYPE1 : int {
	KEYTYPE_SCENELINE, 
	KEYTYPE_MOVE, 
	KEYTYPE_AIM1, 
	KEYTYPE_AIM2,
	KEYTYPE_AIM3,
	KEYTYPE_CHANGE_WEAPON,
	KEYTYPE_SHOOT1,
	KEYTYPE_SHOOT2,
	KEYTYPE_SHOOT3
};

enum SceneLineID
{
	SceneLineID_ADDSINGLEMOB = 0,
	SceneLineID_ADDMULTIMOB = 1,
	SceneLineID_ADDBOSSMOB,
	SceneLineID_SETOBJECTPOS,
	SceneLineID_SETOBJECTSPEED,
	SceneLineID_SETINITPOS,
	SceneLineID_SETPLAYERPOS,
	SceneLineID_PRINTSCRIPT,
	SceneLineID_RECOVERHP,
	SceneLineID_SETTIMER,
	SceneLineID_WAIT,
	SceneLineID_BREAK,
	SceneLineID_CHECKMOB,
	SceneLineID_CHECKPOS,
	SceneLineID_CHECKXPOS,
	SceneLineID_CHECKZPOS,
	SceneLineID_REPEATSCENE,
	SceneLineID_WINGAME = 100,
	SceneLineID_DEFEATGAME
};

class KEYBUF {
public:
	int id;			// 키를 입력한 플레이어의 ID
	KEY_TYPE1 type1;		// 키 처리 타입1 (분류)
	int type2;		// 키 처리 타입2 (세부 분류)
	BYTE buf[60];
	float fkey1;	// float 1
	float fkey2;	// float 2
	float fkey3;	// float 3
	float fkey4;
	float fkey5;
	float fkey6;
	int ikey1;		// int 1
	int ikey2;		// int 2
	int ikey3;
	KEYBUF() {
		id = 0;
		type1 = KEYTYPE_MOVE;
		type2 = 0;
		fkey1 = 0;
		fkey2 = 0;
		fkey3 = 0;
		fkey4 = 0;
		fkey5 = 0;
		fkey6 = 0;
		ikey1 = 0;
		ikey2 = 0;
		ikey3 = 0;
	}
	KEYBUF(const KEYBUF& k) {
		id = k.id;
		type1 = k.type1;
		type2 = k.type2;
		memcpy(buf, k.buf, sizeof(buf));
		fkey1 = k.fkey1;
		fkey2 = k.fkey2;
		fkey3 = k.fkey3;
		fkey4 = k.fkey4;
		fkey5 = k.fkey5;
		fkey6 = k.fkey6;
		ikey1 = k.ikey1;
		ikey2 = k.ikey2;
		ikey3 = k.ikey3;
	}
};