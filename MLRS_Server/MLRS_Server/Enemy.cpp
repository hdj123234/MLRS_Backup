#include "stdafx.h"
#include "Enemy_Boss.h"
#include "Enemy.h"

CEnemy::CEnemy()
{
	is_use = false;
	objectType = OBJECT_ENEMY;
	type = 0;
	speed = 0;
	gravity = 0;
	hp = 0;
	actcount = 0;
	state = 0;
	pthis = this;
	is_translate = OBJECT_CREATE;
}


CEnemy::~CEnemy()
{
}

void CEnemy::InitEnemy(int _id)
{
	id = _id;
	is_use = false;
	pos.clear();
	direction.clear();
}

void CEnemy::putEnemy(int _type, float _pos_x, float _pos_y, float _pos_z, float _look_x, float _look_y, float _look_z)
{
	type = _type;

	pos = FLOAT3(_pos_x, _pos_y, _pos_z);
	direction = FLOAT3(_look_x, _look_y, _look_z);
	speed = 0;
	gravity = 0;
	pMecha = CGameData::getInstance().getEnemyMecha(type);
	pAI = CGameData::getInstance().getAI(pMecha->getAI());
	hp = pMecha->getHP();

	is_translate = OBJECT_CREATE;

	is_use = true;
}

void CEnemy::putEnemy(int _type, FLOAT3 _pos, FLOAT3 _look)
{
	type = _type;

	pos = _pos;
	direction = _look.Normalize();
	speed = 0;
	gravity = 0;
	pMecha = CGameData::getInstance().getEnemyMecha(type);
	pAI = CGameData::getInstance().getAI(pMecha->getAI());
	hp = pMecha->getHP();

	is_translate = OBJECT_CREATE;

	is_use = true;
}

void CEnemy::removeEnemy()
{
	is_use = false;
	type = 0;
	hp = 0;
	pos.clear();
	speed = 0;
	gravity = 0;
	actcount = 0;
	state = 0;

	is_translate = OBJECT_CREATE;

	pMecha = nullptr;
	pAI = nullptr;
	myTarget = nullptr;
}

void CEnemy::Accel()
{
	speed += pMecha->getAcceleration();
	if (speed > pMecha->getMaxSpeed())
		speed = pMecha->getMaxSpeed();
}

void CEnemy::DeAccel()
{
	speed -= pMecha->getAcceleration();
	if (speed <= 0)
		speed = 0;
}

void CEnemy::Act(CGameSpace & space)
{
	/* 과부하 테스트 
	volatile int sum = 0;
	for (int i = 0; i < 7000; ++i)
	{
		sum += 2;
	}
	*/
	if (state == ES_BOOM) {
		removeEnemy();
		return;
	}
	pAI->Act(space, pthis);
}

void CEnemy::Move()
{
	//if (is_translate == OBJECT_CREATE) return;
	if (speed > 0 || gravity != 0)
		is_translate = OBJECT_MOVE;
	else {
		if (is_translate == OBJECT_MOVE)
			is_translate = OBJECT_STOP;
		else
			is_translate = OBJECT_NOSHIFT;
		return;
	}
	pos += direction*speed;
	pos.getY() += gravity;
	float height = CGameData::getInstance().getMap(0)->getHeight(pos.getX(), pos.getZ()) + pMecha->getOffsetY();
	
	if (pos.getY() < height) {
		pos.setY(height);
		gravity = 0;
	}
	// !@#$%^ 중력 영향을 여기서 계산할것인가
//	pos_y = CGameData::getInstance().getMap(0).getHeight(pos_x, pos_z);
}

bool CEnemy::Hit(float _damage)
{
	hp -= _damage;
	if (hp <= 0) {
		hp = 0;
		
		is_translate = OBJECT_REMOVE;
		state = ES_BOOM;
		return true;
	}
	return false;
}
