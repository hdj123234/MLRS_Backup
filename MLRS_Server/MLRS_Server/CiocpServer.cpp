#pragma once
#include "stdafx.h"
#include "CIOCPServer.h"
#include "Timer.h"

void err_display(char *msg);
void err_quit(char *msg);

int test = 0;
mutex test_lock;
CIOCPServer* CIOCPServer::pthis = nullptr;

CIOCPServer::CIOCPServer()
{
	pthis = this;
	iLastLogout = 0;
	//	for (int i = 0; i < MAX_USER; ++i) PlayerInit(i);

	srand((unsigned)time(NULL));

	cpList.resize(MAX_USER);

	roomList.resize(ROOM_MAX_CREAT);
	for (int i = 0; i < ROOM_MAX_CREAT; ++i) {
		roomList[i].InitRoom(i);
		//roomList.push_back(CRoom(i));
	}
	cout << "룸 초기화 완료" << endl;
	//	roomList.resize(ROOM_MAX_CREAT);

	CGameData::getInstance();

}


CIOCPServer::~CIOCPServer()
{
}


int CIOCPServer::NetworkInit() {
	WSAStartup(MAKEWORD(2, 2), &wsadata);
	GetSystemInfo(&si);

	hIOCP = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	if (hIOCP == NULL) {
		cout << "IOCP 핸들 생성에 실패했습니다." << endl;
		return -1;
	}

	HANDLE hThread;

	for (int i = 0; i < (int)si.dwNumberOfProcessors * 2; i++) {
		hThread = CreateThread(NULL, 0, WorkerThread, hIOCP, 0, NULL);
		if (hThread == NULL) {
			cout << "WorkerThread 생성에 실패했습니다." << endl;
			return -1;
		}
		CloseHandle(hThread);
	}
	cout << "WorkerThread (" <<
		(int)si.dwNumberOfProcessors * 2 << "개) 생성 완료" << endl;

	// 타이머에 IOCP 핸들 넘겨주기
	mytimer.TimerInit(hIOCP, roomList);
	mytimer.TimerStart();

	/* 성능 테스트
	for (int i = 0; i < 10; ++i) {
		mytimer.AddTimer(i, GetTickCount() + 10, 0);
	}
	//////////////*/
	return 0;
}

void CIOCPServer::Listen() {
	int retval;

	// socket()
	SOCKET listen_sock = WSASocketW(AF_INET, SOCK_STREAM,
		IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (listen_sock == INVALID_SOCKET) err_quit("socket()");

	// bind()
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
	serveraddr.sin_port = htons(SERVERPORT);
	ZeroMemory(&serveraddr.sin_zero, 8);
	retval = ::bind(listen_sock, (SOCKADDR *)&serveraddr, sizeof(serveraddr));
	if (retval == SOCKET_ERROR) err_quit("bind()");

	// listen()
	retval = listen(listen_sock, SOMAXCONN);
	if (retval == SOCKET_ERROR) err_quit("listen()");
	cout << "Listen 함수 실행중" << endl;

	// 데이터 통신에 사용할 변수
	SOCKET client_sock;
	SOCKADDR_IN clientaddr;
	int addrlen;
	DWORD flags;

	while (1) {
		// accept()
		addrlen = sizeof(clientaddr);
		client_sock = WSAAccept(listen_sock, (SOCKADDR *)&clientaddr, &addrlen,
			NULL, NULL);
		if (client_sock == INVALID_SOCKET) {
			err_display("accept()");
			break;
		}
		//	inet_ntop(AF_INET,clientaddr.sin_addr, serveraddr.sin_addr.s_addr);

		printf("[TCP 서버] 클라이언트 접속 : IP 주소 = %s, 포트 번호 = %d\n",
			inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));

		// 플레이어 리스트의 빈 자리 찾아주기
		int id = 0;
		if (iLastLogout != 0 && pthis->cpList[iLastLogout].is_use == false) {
			id = iLastLogout;
			cpList[iLastLogout].is_use = true;
			iLastLogout = 0;
		}
		else {
			for (int i = 0; i < MAX_USER; ++i) {
				if (cpList[i].is_use == false) {
					id = i;
					cpList[i].is_use = true;
					break;
				}
			}
		}
		PlayerInit(id);

		cpList[id].sock = client_sock;

		// 소켓과 입출력 완료 포트 연결
		CreateIoCompletionPort((HANDLE)client_sock, hIOCP, id, 0);

		// 비동기 입출력 시작
		flags = 0;
		retval = WSARecv(client_sock, &cpList[id].my_overapped.wsabuf,
			1, NULL, &flags, &cpList[id].my_overapped.overapped, NULL);
		if (retval == SOCKET_ERROR) {
			if (WSAGetLastError() != ERROR_IO_PENDING) {
				err_display("WSARecv()");
			}
		}

		// 발급받은 클라이언트ID 알려주기
		sc_packet_clientid id_packet;
		id_packet.id = id;
		SendPacket(id, &id_packet);

#ifdef _DEBUG
		cout << id << "번 플레이어가 입장했습니다." << endl;
#endif
		/*
		// 신규 접속자에게 기존 접속자들 위치 전송
		sc_packet_userinfo info_packet;
		info_packet.type = SC_USERINFO;
		info_packet.location = SC_LOCATION_LOBBY;
		int nick_len = 0;
		for (int i = 0; i < MAX_USER; ++i){
			if (cpList[i].is_use == true) {
				if (cpList[i].in_room == -1) {
					if (id == i) continue;
					info_packet.id = i;
					nick_len = strlen(cpList[i].nick);
					memcpy(info_packet.nickname, cpList[i].nick, nick_len);
					info_packet.size = nick_len + SC_PACKET_USERINFO_TYPELEN;
					SendPacket(i, &info_packet);
				}
			}
		}
		*/


	}
}


void CIOCPServer::SendPacket(int id, void *packet)
{
	int packet_size = reinterpret_cast<unsigned char *>(packet)[0];

	OVERAPPED_EX *send_over = new OVERAPPED_EX;
	ZeroMemory(send_over, sizeof(OVERAPPED_EX));
	send_over->type = TYPE_SEND;
	send_over->wsabuf.buf = send_over->IOCPbuf;
	send_over->wsabuf.len = packet_size;
	unsigned long io_size;

	memcpy(send_over->IOCPbuf, packet, packet_size);

	WSASend(pthis->cpList[id].sock, &send_over->wsabuf, 1,
		&io_size, NULL, &send_over->overapped, NULL);
}

// 수정중
void CIOCPServer::SendMultiplePacket(int id, int packet_size, void *packet)
{
	OVERAPPED_EX *send_over = new OVERAPPED_EX;
	ZeroMemory(send_over, sizeof(OVERAPPED_EX));
	send_over->type = TYPE_SEND;
	send_over->wsabuf.buf = send_over->IOCPbuf;
	send_over->wsabuf.len = packet_size;
	unsigned long io_size;

	memcpy(send_over->IOCPbuf, packet, packet_size);

	WSASend(pthis->cpList[id].sock, &send_over->wsabuf, 1,
		&io_size, NULL, &send_over->overapped, NULL);
}

void CIOCPServer::SendBuffer(int _roomid, int &_iter, int _max, BYTE * _packetZip)
{
	if (_iter > _max) {
		for (auto &i : roomList[_roomid].getPlayerIDVector()) {
			if (!i.is_Use())
				continue;
			pthis->SendMultiplePacket(i.getPlayerID(), _iter, _packetZip);
		}
		_iter = 0;
		ZeroMemory(_packetZip, MULTIPLESEND_BUFFERSIZE);
	}
}

void CIOCPServer::SendBufferToPlayer(int _roomid, int _playerid, int & _iter, int _max, BYTE * _packetZip)
{
	if (_iter > _max) {
		if (_playerid >= 0) {
			if (cpList[_playerid].is_use)
				SendMultiplePacket(_playerid, _iter, _packetZip);
		}
		_iter = 0;
		ZeroMemory(_packetZip, MULTIPLESEND_BUFFERSIZE);
	}
}

void CIOCPServer::ProcessPacket(char *packet, int id) {
	int len = packet[0];
	//	cout << "데이터 받았습니다. 사이즈 : " << (int)packet[0] << endl;
	switch (packet[1]) {
	case CS_SET_NICK:
	{
		memcpy(cpList[id].nick, &packet[2], len - CS_PACKET_NICKNAME_TYPELEN);
		cpList[id].nick[len - CS_PACKET_NICKNAME_TYPELEN] = 0;
		cpList[id].in_room = -1;
#ifdef _DEBUG 
		//		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 1);
		cout << id << "번 플레이어 님이 닉네임을 [ " << cpList[id].nick <<
			" ] 으로 설정하셨습니다. " << endl;
#endif

		// 내 정보를 로비 유저들에게 보내기
		sc_packet_userinfo info_packet;
		info_packet.size = len + SC_PACKET_USERINFO_TYPELEN - CS_PACKET_NICKNAME_TYPELEN;
		info_packet.id = id;
		info_packet.location = SC_LOCATION_LOBBY;
		memcpy(info_packet.nickname, cpList[id].nick, len - CS_PACKET_NICKNAME_TYPELEN);

		for (int i = 0; i < MAX_USER; ++i)
			if (cpList[i].is_use == true && cpList[i].in_room < 0)
				SendPacket(i, &info_packet);

		// 신규 접속자에게 기존 접속자들 위치 전송
		size_t nick_len = 0;
		for (int i = 0; i < MAX_USER; ++i) {
			if (cpList[i].is_use == true) {
				if (cpList[i].in_room == -1) {
					if (id == i) continue;
					info_packet.id = i;
					nick_len = strlen(cpList[i].nick);
					memcpy(info_packet.nickname, cpList[i].nick, nick_len);
					info_packet.size = static_cast<BYTE>(nick_len + SC_PACKET_USERINFO_TYPELEN);
					SendPacket(id, &info_packet);
				}
			}
		}
		break;
	}


	case CS_CHAT:
	{
		int roomid = cpList[id].in_room;
		sc_packet_chat chat_packet;
		chat_packet.size = len + SC_PACKET_CHAT_TYPELEN - CS_PACKET_CHAT_TYPELEN;
		chat_packet.id = id;
		memcpy(chat_packet.message, &packet[2], len - CS_PACKET_CHAT_TYPELEN);
#ifdef _DEBUG 
		cout << cpList[id].nick << "(" << id << ")";
#endif
		if (roomid != -1) {
			// 플레이어가 방에 있다면 방에 있는 사람들한테만 보냄
			for (auto i = roomList[roomid].getPlayerIDVector().begin();
				i != roomList[roomid].getPlayerIDVector().end(); ++i) {
				SendPacket(i->getPlayerID(), &chat_packet);
			}
#ifdef _DEBUG 
			cout << "(" << roomid << "번방)";
#endif
		}
		else {
			// 로비면 로비사람들에게 보냄
			for (int i = 0; i < MAX_USER; ++i) {
				if (cpList[i].is_use == true) {
					if (cpList[i].in_room == roomid)
						SendPacket(i, &chat_packet);
				}
			}
#ifdef _DEBUG 
			cout << "(로비)";
#endif
		}
#ifdef _DEBUG 
		char debug_char[MAX_STR_SIZE + 1] = { 0 };
		memcpy(debug_char, &packet[2], len - CS_PACKET_CHAT_TYPELEN);
		cout << " : " << debug_char << endl;
#endif
		break;
	}



	case CS_ROOM_PAGE:
	{
		if (packet[0] < 3) break;
		int page = packet[2];
		int addpage = 0;
		if ((ROOM_MAX_CREAT % CGameData::getInstance().getConstData()->getLOBBY_MAX_SHOW_ROOM()) > 0) addpage = 1;
		if (page > (ROOM_MAX_CREAT / CGameData::getInstance().getConstData()->getLOBBY_MAX_SHOW_ROOM()) + addpage
			|| page == 0) break;

		sc_packet_roominfo roominfo_packet;
		int sendcount = 0;
		for (int i = CGameData::getInstance().getConstData()->getLOBBY_MAX_SHOW_ROOM()*(page - 1); i < CGameData::getInstance().getConstData()->getLOBBY_MAX_SHOW_ROOM()*page; ++i) {
			if (i >= ROOM_MAX_CREAT) break;
			if (roomList[i].isUse() == true) {
				roominfo_packet.curp = roomList[i].getCurrentPlayer();
				roominfo_packet.maxp = roomList[i].getMaxPlayer();
				roominfo_packet.roomid = roomList[i].getRoomID();
				roominfo_packet.state = roomList[i].getState();
				memcpy(roominfo_packet.title, roomList[i].getTitle(), strlen(roomList[i].getTitle()));
				roominfo_packet.size = static_cast<BYTE>(strlen(roomList[i].getTitle()) + 7);
				SendPacket(id, &roominfo_packet);
				sendcount++;
			}
		}
#ifdef _DEBUG
		if (sendcount > 0) {
			cout << id << "번 님이 " << page << "페이지의 룸리스트를 요청하여, 총 " << sendcount << "개의 룸 정보를 보냈습니다." << endl;
		}
		else {
			// 요청한 페이지에 방이 하나도 없으면....
			cout << id << "번 님이 " << page << "페이지의 룸리스트를 요청하였지만, 해당 페이지에는 방이 없어 보내지 못했습니다." << endl;
		}
#endif

		break;
	}

	case CS_CREATE_ROOM:
	{
		if (packet[0] < 3) break;

		sc_packet_enterroom enter_packet;
		enter_packet.size = ENTERROOM_TYPELEN;
		enter_packet.type = SC_IS_ENTER_ROOM;
		enter_packet.result = false;

		for (int i = 0; i < ROOM_MAX_CREAT; ++i) {
			if (roomList[i].isUse() == false) {
				// 방을 만들 수 있으면
				char roomtitle[ROOM_MAX_TITLE_LEN];
				memcpy(roomtitle, &packet[3], len - CS_PACKET_CREATE_ROOM_TYPELEN);
				roomtitle[len - CS_PACKET_CREATE_ROOM_TYPELEN] = 0;

				// !@#$%^ 여기 크리티컬 섹션 입니당
				roomList[i].CreateRoom(i, roomtitle, packet[2]);
				roomList[i].AddPlayer(id);

				cpList[id].in_room = i;

				enter_packet.size = static_cast<BYTE>(strlen(roomList[i].getTitle()) + ENTERROOM_TYPELEN);
				enter_packet.roomid = i;
				enter_packet.curp = roomList[i].getCurrentPlayer();
				enter_packet.maxp = roomList[i].getMaxPlayer();
				enter_packet.state = roomList[i].getState();
				memcpy(enter_packet.title, roomList[i].getTitle(), strlen(roomList[i].getTitle()));
				enter_packet.result = true;


				// 방 생성에 성공하면

				// !@#$%^


				// 로비 인원들에게 방이 생겼다고 알려줘야함


				break;
			}
		}
		// 입장 여부 결과 송신
		SendPacket(id, &enter_packet);

		/*
		if (enter_packet.result != false) {
			// 방 입장에 성공하면 방에 있는 유저들에게 자신의 정보 보내기
			sc_packet_userinfo user_packet;
			user_packet.type = SC_USERINFO;
			user_packet.location = SC_LOCATION_ROOM;
			user_packet.id = id;
			memcpy(user_packet.nickname, cpList[id].nick, strlen(cpList[id].nick));
			user_packet.size = strlen(cpList[id].nick) + SC_PACKET_USERINFO_TYPELEN;
			SendPacket(id, &user_packet);
		}
		*/
		break;
	}

	case CS_ENTER_ROOM:
	{
		if (packet[0] < 3) break;

		WORD pw;
		memcpy(&pw, &packet[2], sizeof(pw));
		int roomid = pw;
		if (roomid < 0 || roomid >= ROOM_MAX_CREAT)
			break;
		bool can_enter = false;
		sc_packet_enterroom enter_packet;
		enter_packet.size = ENTERROOM_TYPELEN;
		enter_packet.type = SC_IS_ENTER_ROOM;
		if (roomList[roomid].canEnter()) {
			can_enter = true;
			roomList[roomid].AddPlayer(id);
			enter_packet.size = static_cast<BYTE>(strlen(roomList[roomid].getTitle()) + ENTERROOM_TYPELEN);
			enter_packet.roomid = roomid;
			enter_packet.curp = roomList[roomid].getCurrentPlayer();
			enter_packet.maxp = roomList[roomid].getMaxPlayer();
			enter_packet.state = roomList[roomid].getState();
			memcpy(enter_packet.title, roomList[roomid].getTitle(), strlen(roomList[roomid].getTitle()));
			enter_packet.result = true;
			cpList[id].in_room = roomid;
#ifdef _DEBUG
			cout << id << "번 님이 " << roomid << "번 방에 들어가셨습니다." << endl;
#endif
		}
		else {
			enter_packet.result = false;
#ifdef _DEBUG
			cout << id << "번 님이 " << roomid << "번 방 접속에 실패하셨습니다." << endl;
#endif
		}
		SendPacket(id, &enter_packet);

		if (can_enter) {
			sc_packet_userinfo user_packet;
			user_packet.type = SC_USERINFO;
			user_packet.location = SC_LOCATION_ROOM;
			// 방 접속에 성공하면 해당 방에 있는 유저들 정보 송신
			for (auto &data : roomList[roomid].getPlayerIDVector()) {
				if (!data.is_Use()) continue;
				user_packet.id = data.getPlayerID();
				memcpy(user_packet.nickname, cpList[data.getPlayerID()].nick, strlen(cpList[data.getPlayerID()].nick));
				user_packet.size = static_cast<BYTE>(strlen(cpList[data.getPlayerID()].nick) + SC_PACKET_USERINFO_TYPELEN);
				SendPacket(id, &user_packet);
			}


			// 방에 있는 유저들에게 새로운 유저가 들어왔다는 정보 송신
			user_packet.id = id;
			memcpy(user_packet.nickname, cpList[id].nick, strlen(cpList[id].nick));
			user_packet.size = static_cast<BYTE>(strlen(cpList[id].nick) + SC_PACKET_USERINFO_TYPELEN);
			for (auto data : roomList[roomid].getPlayerIDVector()) {
				if (data.getPlayerID() == id) continue;
				if (!data.is_Use()) continue;
				SendPacket(data.getPlayerID(), &user_packet);
			}
		}
		break;
	}

	case CS_LEAVE_ROOM:
	{
		if (packet[0] > 2) break;

		int leave_room = cpList[id].in_room;
		if (leave_room > -1) {
			roomList[leave_room].RemovePlayer(id);
			cpList[id].in_room = -1;
#ifdef _DEBUG
			cout << id << "번 님이 " << leave_room << "번 방에서 나가셨습니다." << endl;
#endif

			// 나갔다고 파티원에게 알려주기
			sc_packet_leave_user leave_packet;
			leave_packet.size = sizeof(leave_packet);
			leave_packet.type = SC_LEAVE_USER;
			leave_packet.id = id;
			for (auto &data : roomList[leave_room].getPlayerIDVector()) {
				if (!data.is_Use()) continue;
				SendPacket(data.getPlayerID(), &leave_packet);
			}


			// 로비에 있는 유저들 정보 다시 받기
			sc_packet_userinfo info_packet;
			info_packet.type = SC_USERINFO;
			info_packet.location = SC_LOCATION_LOBBY;
			size_t nick_len = 0;
			for (int i = 0; i < MAX_USER; ++i) {
				if (cpList[i].is_use == true) {
					if (cpList[i].in_room == -1) {
						if (id == i) continue;
						info_packet.id = i;
						nick_len = strlen(cpList[i].nick);
						memcpy(info_packet.nickname, cpList[i].nick, nick_len);
						info_packet.size = static_cast<BYTE>(nick_len + SC_PACKET_USERINFO_TYPELEN);
						SendPacket(id, &info_packet);
					}
				}
			}

			// 로비에 있는 유저들에게 내 정보 알려주기
			info_packet.id = id;
			nick_len = strlen(cpList[id].nick);
			memcpy(info_packet.nickname, cpList[id].nick, nick_len);
			info_packet.size = static_cast<BYTE>(nick_len + SC_PACKET_USERINFO_TYPELEN);
			for (int i = 0; i < MAX_USER; ++i) {
				if (cpList[i].is_use == true) {
					if (cpList[i].in_room == -1) {
						SendPacket(i, &info_packet);
					}
				}
			}

		}
		break;
	}

	case CS_CHANGE_MECHA:
	{
		if (packet[0] < 3) break;

		int mecha = packet[2];
		if (mecha < 0)
			break;

		roomList[cpList[id].in_room].setMecha(id, mecha);

#ifdef _DEBUG
		cout << id << "번 님이 " << mecha << "번 메카로 바꾸셨습니다." << endl;
#endif

		sc_packet_change_mecha change_mecha_packet;
		change_mecha_packet.size = sizeof(change_mecha_packet);
		change_mecha_packet.type = SC_CHANGE_MECHA;
		change_mecha_packet.id = id;
		change_mecha_packet.select_mecha = mecha;
		for (auto &data : roomList[cpList[id].in_room].getPlayerIDVector()) {
			if (!data.is_Use()) continue;
			SendPacket(data.getPlayerID(), &change_mecha_packet);
		}
		break;
	}

	case CS_READY:
	{
		if (packet[0] < 3) break;

		bool ready = packet[2] != 0;

		roomList[cpList[id].in_room].setReady(id, ready);

#ifdef _DEBUG
		cout << id << "번 님이 레디를 ";
		if (ready)
			cout << "하셨습니다." << endl;
		else
			cout << "푸셨습니다." << endl;
#endif

		sc_packet_ready ready_packet;
		ready_packet.size = sizeof(sc_packet_ready);
		ready_packet.type = SC_READY;
		ready_packet.id = id;
		ready_packet.is_ready = ready;
		for (auto &data : roomList[cpList[id].in_room].getPlayerIDVector()) {
			if (!data.is_Use()) continue;
			SendPacket(data.getPlayerID(), &ready_packet);
		}
		break;
	}

	case CS_ASSIST:
	{

		break;
	}

	case CS_DEBUG_ASSIST:
	{
		int roomid = 0;
		if (len == 3)
			roomid = packet[2];
		else
			roomid = 24;
		cpList[id].in_room = roomid;

		if (roomid == 24)
			int a = 0;

		/* 과부하 성능 테스트
		for (int i = 0; i < 24; ++i) {
			roomList[i].CreateRoom(i, "디버그 룸", 8);
			roomList[i].setState(1);
			mytimer.AddTimer(i, GetTickCount(), 0);
			srand((unsigned)time(NULL));
			for (auto j = roomList[i].getEnemyIDVector().begin();
			j != roomList[i].getEnemyIDVector().begin() + 100; ++j) {
				j->putEnemy(0, rand() % 3000 - 1500.f, 300, rand() % 3000 - 1500.f);
			}
		}
		*/

		if(!roomList[roomid].isUse())
		{
			roomList[roomid].coercionStart(id);
#ifdef _DEBUG
		cout << id << "번 님이 " << "디버그 모드로 난입하셨습니다." << endl;
#endif
//		if (roomList[roomid].canStart() == SC_START_RESULT_OK) {
	//		if(
			mytimer.AddTimer(roomid, GetTickCount(), TYPE_ROOM_OPERATION);
		}
		else
			roomList[roomid].AddPlayer(id);



		// 내 위치를 다른 클라이언트에게 전달
		sc_packet_join join_packet;
		join_packet.id = id;
		join_packet.m_type = 0;
		for (auto &i : roomList[roomid].getPlayerIDVector()) {
			if (i.getIndexID() == id) {
				join_packet.pos_x = i.getPos().getX();
				join_packet.pos_y = i.getPos().getY();
				join_packet.pos_z = i.getPos().getZ();
				join_packet.look_x = i.getLook().getX();
				join_packet.look_z = i.getLook().getZ();
				break;
			}
		}
		for (auto &i : roomList[roomid].getPlayerIDVector()) {
			if (i.is_Use())
				SendPacket(i.getPlayerID(), &join_packet);
		}

		// 다른 클라이언트 위치를 받음
		join_packet.m_type = 0;
		for (auto &i : roomList[roomid].getPlayerIDVector()) {
			if (i.is_Use()) {
				if (i.getPlayerID() == id) continue;
				join_packet.id = i.getIndexID();
				join_packet.m_type = i.getMechaID();
				join_packet.pos_x = i.getPos().getX();
				join_packet.pos_y = i.getPos().getY();
				join_packet.pos_z = i.getPos().getZ();
				SendPacket(id, &join_packet);
			}
		}


		BYTE packetZip[MULTIPLESEND_BUFFERSIZE];
		int iter = 0;
		/*
		sc_packet_ecreate enemy_create_packet;
		
		// 몹 생성 패킷		
		for (auto &i : pthis->roomList[cpList[id].in_room].getEnemyIDVector()) {
			if (i.is_Use()) {
				enemy_create_packet.id = i.getID();
				enemy_create_packet.m_type = i.getTypeNumber();
				enemy_create_packet.pos_x = i.getPos().getX();
				enemy_create_packet.pos_y = i.getPos().getY();
				enemy_create_packet.pos_z = i.getPos().getZ();
				enemy_create_packet.look_x = i.getDirection().getX();
				enemy_create_packet.look_y = i.getDirection().getY();
				enemy_create_packet.look_z = i.getDirection().getZ();
				memcpy(&packetZip[iter], &enemy_create_packet, sizeof(enemy_create_packet));
				iter += sizeof(enemy_create_packet);

				if (iter > PACKET_MAXSIZE) {
					pthis->SendMultiplePacket(id, iter, packetZip);
					iter = 0;
				}
			}
		}
		if (iter > 0) {
			pthis->SendMultiplePacket(id, iter, packetZip);
		}
		*/
		sc_packet_bcreate boss_create_packet;
		iter = 0;
		// 보스 생성 패킷		
		for (auto &i : pthis->roomList[cpList[id].in_room].getBossIDVector()) {
			if (i.is_Use()) {
				boss_create_packet.id = i.getID();
				boss_create_packet.m_type = i.getTypeNumber();
				boss_create_packet.pos_x = i.getPos().getX();
				boss_create_packet.pos_y = i.getPos().getY();
				boss_create_packet.pos_z = i.getPos().getZ();
				boss_create_packet.look_x = i.getDirection().getX();
				boss_create_packet.look_y = i.getDirection().getY();
				boss_create_packet.look_z = i.getDirection().getZ();
				memcpy(&packetZip[iter], &boss_create_packet, sizeof(boss_create_packet));
				iter += sizeof(boss_create_packet);

				if (iter > PACKET_MAXSIZE) {
					pthis->SendMultiplePacket(id, iter, packetZip);
					iter = 0;
				}
			}
		}
		if (iter > 0) {
			pthis->SendMultiplePacket(id, iter, packetZip);
		}

		/*
		// 미사일 위치 패킷
		sc_packet_mcreate mcreate_packet;
		ZeroMemory(packetZip, MULTIPLESEND_BUFFERSIZE);
		iter = 0;

		for (auto &i : pthis->roomList[cpList[id].in_room].getMissileIDVector()) {
			if (i.is_Use()) {
				mcreate_packet.id = i.getID();
				mcreate_packet.m_type = i.getTypeNumber();
				mcreate_packet.pos_x = i.getPos().getX();
				mcreate_packet.pos_y = i.getPos().getY();
				mcreate_packet.pos_z = i.getPos().getZ();
				mcreate_packet.look_x = i.getDirection().getX();
				mcreate_packet.look_y = i.getDirection().getY();
				mcreate_packet.look_z = i.getDirection().getZ();
				memcpy(&packetZip[iter], &mcreate_packet, sizeof(mcreate_packet));
				iter += sizeof(mcreate_packet);

				if (iter > PACKET_MAXSIZE) {
					pthis->SendMultiplePacket(id, iter, packetZip);
					iter = 0;
				}
			}
		}
		if (iter > 0) {
			pthis->SendMultiplePacket(id, iter, packetZip);
		}
		*/
		// 오브젝트 움직임 패킷
		sc_packet_ocreate ocreate_packet;
		ZeroMemory(packetZip, MULTIPLESEND_BUFFERSIZE);
		for (auto &i : pthis->roomList[cpList[id].in_room].getObjectIDVector()) {
			if (i.is_Use()) {
				ocreate_packet.id = i.getID();
				ocreate_packet.o_type = i.getObjectType();	// ~~~~~~ 이거 의미없음
				ocreate_packet.pos_x = i.getPos().getX();
				ocreate_packet.pos_y = i.getPos().getY();
				ocreate_packet.pos_z = i.getPos().getZ();
				ocreate_packet.look_x = i.getDirection().getX();
				ocreate_packet.look_y = i.getDirection().getY();
				ocreate_packet.look_z = i.getDirection().getZ();
				memcpy(&packetZip[iter], &ocreate_packet, sizeof(ocreate_packet));
				iter += sizeof(ocreate_packet);
				if (iter > PACKET_MAXSIZE) {
					pthis->SendMultiplePacket(id, iter, packetZip);
					iter = 0;
				}
			}
		}
		if (iter > 0) {
			pthis->SendMultiplePacket(id, iter, packetZip);
		}
		break;
	}

	case CS_MOVE:
	{
		KEYBUF k;
		k.id = id;
		k.type1 = KEYTYPE_MOVE;
		k.type2 = (int)packet[2];

		memcpy(k.buf, &packet[3], 8);
		memcpy(&k.fkey1, &packet[3], sizeof(k.fkey1));
		memcpy(&k.fkey3, &packet[7], sizeof(k.fkey3));

		// 클라이언트가 키값 트롤링시 막아야 함
		// NAN 검증 (Not A Number)
		if (k.fkey1 != k.fkey1 || k.fkey3 != k.fkey3) {
			k.fkey1 = 0;
			k.fkey3 = 0;
		}

		/*
		// 클라이언트가 해킹되어 이동속도 핵을 쓰려고 매우 큰 값을 보낼 경우
		// 클라이언트가 보낸 값을 노말라이즈 한 뒤 키값을 적용해야 하겠지만
		// 성능에 문제가 될 수 있으니 일단 제낍시다.
		float ftemp1 = FLOAT3(k.fkey1, 0, k.fkey3).Normalize().getX();
		float ftemp3 = FLOAT3(k.fkey1, 0, k.fkey3).Normalize().getZ();
		k.fkey1 = ftemp1;
		k.fkey3 = ftemp3;
		*/

		//		cout << "클라한테 받은 키 : " << k.type1<<", "<< k.type2<<",   pos : "<<
		//			k.fkey1 << ", " << k.fkey3 << endl;
		roomList[cpList[id].in_room].putKeybuf(k);

		break;
	}

	case CS_AIM_MOVE1:
	{
		KEYBUF k;
		k.id = id;
		k.type1 = KEYTYPE_AIM1;
		k.type2 = (int)packet[2];

		memcpy(k.buf, &packet[3], 20);

		//		cout << "size : " << (int)packet[0] << ", " << k.type1 << ", " << k.type2 << ", " <<
		//			k.fkey1 << ", " << k.fkey2 << ", " << k.fkey3 << ", " << k.fkey4 << ", " << k.fkey5 << endl;

				// 클라이언트가 키값 트롤링시 막아야 함
				// NAN 검증 (Not A Number)
		if (k.fkey1 != k.fkey1 || k.fkey2 != k.fkey2 || k.fkey3 != k.fkey3) {
			k.fkey1 = 0;
			k.fkey2 = 1;
			k.fkey3 = 0;
		}

		if (k.fkey1 > 100 || k.fkey1 < -100) {
			break;
		}
		if (k.fkey2 > 100 || k.fkey2 < -100) {
			break;
		}


		roomList[cpList[id].in_room].putKeybuf(k);

		break;
	}
	case CS_AIM_MOVE2:
	{
		KEYBUF k;
		k.id = id;
		k.type1 = KEYTYPE_AIM2;

		memcpy(k.buf, &packet[2], 12);
		
		if (k.fkey1 > 100 || k.fkey1 < -100) {
			break;
		}
		if (k.fkey2 > 100 || k.fkey2 < -100) {
			break;
		}

		roomList[cpList[id].in_room].putKeybuf(k);
		break;
	}

	case CS_CHANGE_WEAPON:
	{
		KEYBUF k;
		k.id = id;
		k.type1 = KEYTYPE_CHANGE_WEAPON;
		k.type2 = packet[2];
		roomList[cpList[id].in_room].putKeybuf(k);
		cout << "무기 전환 패킷을 받았습니다. " << k.type2 << "번 무기로 전환" << endl;
		break;
	}

	case CS_SHOOT1:
	{
		cout << "미사일 발사 패킷을 받았습니다." << endl;
		KEYBUF k;
		k.id = id;
		k.type1 = KEYTYPE_SHOOT1;
		k.type2 = 1;
		memcpy(k.buf, &packet[3], 24);

		// 클라이언트가 키값 트롤링시 막아야 함
		// NAN 검증 (Not A Number)
		if (k.fkey1 != k.fkey1 || k.fkey2 != k.fkey2 || k.fkey3 != k.fkey3) {
			k.fkey1 = 0;
			k.fkey2 = 1;
			k.fkey3 = 0;
		}
		roomList[cpList[id].in_room].putKeybuf(k);

		break;
	}


	case CS_SHOOT2:
	{
		KEYBUF k;
		k.id = id;
		k.type1 = KEYTYPE_SHOOT2;
		k.type2 = 2;

		memcpy(k.buf, &packet[2], 60);

		// 클라이언트가 키값 트롤링시 막아야 함
		// NAN 검증 (Not A Number)
		if (k.fkey1 != k.fkey1 || k.fkey2 != k.fkey2 || k.fkey3 != k.fkey3) {
			k.fkey1 = 0;
			k.fkey2 = 1;
			k.fkey3 = 0;
		}
		roomList[cpList[id].in_room].putKeybuf(k);

		break;
	}

	case CS_SHOOT3:
	{
		KEYBUF k;
		k.id = id;
		k.type1 = KEYTYPE_SHOOT3;
		k.type2 = 3;
		WORD t;
		memcpy(&t, &packet[2], 2);
		cout << (int)t << endl;
		memcpy(k.buf, &packet[2], 32);

		// 클라이언트가 키값 트롤링시 막아야 함
		// NAN 검증 (Not A Number)
		if (k.fkey1 != k.fkey1 || k.fkey2 != k.fkey2 || k.fkey3 != k.fkey3) {
			k.fkey1 = 0;
			k.fkey2 = 1;
			k.fkey3 = 0;
		}
		roomList[cpList[id].in_room].putKeybuf(k);

		break;
	}
	default:
		cout << "Unknown Packet!" << endl;
		break;
	}


}

void CIOCPServer::PlayerInit(int id)
{
	ZeroMemory(&cpList[id], sizeof(cpList[id]));
	cpList[id].in_room = -1;
	cpList[id].is_use = true;
	cpList[id].my_overapped.type = TYPE_RECV;
	cpList[id].my_overapped.wsabuf.buf = cpList[id].my_overapped.IOCPbuf;
	cpList[id].my_overapped.wsabuf.len = sizeof(cpList[id].my_overapped.IOCPbuf);
}



DWORD WINAPI CIOCPServer::WorkerThread(LPVOID arg)
{
	int retval;
	HANDLE hcp = (HANDLE)arg;

	while (1) {
		//비동기 입출력 완료 기다리기
		unsigned long io_size;
		unsigned long long key;
		OVERAPPED_EX *over_ex;

		retval = GetQueuedCompletionStatus(pthis->hIOCP, &io_size, &key,
			reinterpret_cast<LPOVERLAPPED *>(&over_ex), INFINITE);

		// 접속 종료시
		if (io_size == 0) {
			sc_packet_remove_player rem_packet;
			rem_packet.id = (WORD)key;

			// 로비에 있었을 경우 로비 유저들에게만 알려줌
			if (pthis->cpList[key].in_room < 0) {
				for (int i = 0; i < MAX_USER; ++i) {
					if (pthis->cpList[i].is_use == false) continue;
					if (key == i) continue;
					pthis->SendPacket(i, &rem_packet);
				}
			}


			// 방에 있었을 경우 방 유저들에게만 알려줌
			else {
				for (auto &i : pthis->roomList[pthis->cpList[key].in_room].getPlayerIDVector()) {
					if (i.getPlayerID() == key) continue;
					if (!i.is_Use()) continue;
					pthis->SendPacket(i.getPlayerID(), &rem_packet);
				}
				pthis->roomList[pthis->cpList[key].in_room].RemovePlayer(static_cast<int>(key));
			}

			// 초기화 작업				
			pthis->cpList[key].is_use = false;
			pthis->iLastLogout = static_cast<int>(key);


			SOCKADDR_IN clientaddr;
			int addrlen = sizeof(clientaddr);
			getpeername(pthis->cpList[key].sock, (SOCKADDR *)&clientaddr, &addrlen);
			printf("[TCP 서버] 클라이언트 종료 : IP 주소= %s, 포트 번호 = %d\n",
				inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));
			closesocket(pthis->cpList[key].sock);

			continue;
		}

		switch (over_ex->type)
		{
		case TYPE_RECV:
		{
			// RECV
			int rest_size = io_size;
			char *buf = over_ex->IOCPbuf;
			int packet_size = over_ex->curr_packet_size;
			while (0 < rest_size) {
				if (0 == packet_size) packet_size = buf[0];
				int remain = packet_size - over_ex->prev_received;
				if (remain > rest_size) {
					// 패킷 만들기에는 부족하다.
					memcpy(over_ex->PacketBuf + over_ex->prev_received,
						buf, rest_size);
					over_ex->prev_received += rest_size;
					break;
				}
				else {
					// 패킷을 만들 수 있다.
					memcpy(over_ex->PacketBuf + over_ex->prev_received,
						buf, remain);
					pthis->ProcessPacket(over_ex->PacketBuf, static_cast<int>(key));
					rest_size -= remain;
					packet_size = 0;
					over_ex->prev_received = 0;
					buf += remain;
				}
			}
			over_ex->curr_packet_size = packet_size;
			unsigned long recv_flag = 0;
			WSARecv(pthis->cpList[key].sock,
				&over_ex->wsabuf, 1, NULL,
				&recv_flag, &over_ex->overapped, NULL);

			break;
		}
		case TYPE_SEND:
		{
			delete over_ex;
			break;
			// SEND
		}
		case TYPE_ROOM_OPERATION:
		{
			// auto start = chrono::high_resolution_clock().now();

			/// /////////// 여기 동시성 제어 때매 뻑나니까 잘 처리해야함 //////////////
			/// 지금 방 제거됐을때도 한 번 더 연산하는 문제가 있는데
			/// 앞으로 키값 처리할 때 키에 '이 방 죽여라' 라는 키 넣어서 처리할 예정
			// 
			auto pRoom = &pthis->roomList[key];
			if (pRoom->isUse()) {
				pRoom->Operation();

				// 플레이어 움직임 패킷
				sc_packet_move move_packet;
				sc_packet_aim_move1 aim_packet;
				sc_packet_aim_move2 aim_packet2;
				//	sc_fire_packet fire_packet;

				BYTE packetZip[MULTIPLESEND_BUFFERSIZE];
				int iter = 0;

				//				fire_packet.id = -1;
				for (auto &i : pRoom->getPlayerIDVector()) {
					if (!i.is_Use())	continue;

					// 무기 바꾸는 동작
					if (i.isWeaponChange() != -2) {
						sc_packet_change_weapon cw_packet;
						cw_packet.id = i.getIndexID();
						cw_packet.w_type = i.getSelectWeapon();
						memcpy(&packetZip[iter], &cw_packet, sizeof(cw_packet));
						iter += sizeof(cw_packet);
					}

					// 발사 동작
					if (i.getFireWeaponType() != 0) {
						sc_fire_packet fire_packet;
						fire_packet.w_type = i.getFireWeaponType();
						i.resetFireWeaponType();
						fire_packet.id = i.getIndexID();
						fire_packet.point = 0; // !@#$%^ 		
						memcpy(&packetZip[iter], &fire_packet, sizeof(fire_packet));
						iter += sizeof(fire_packet);
					}

					// 조준모드일 경우
					if (i.is_AimMode()) {
						switch (i.getSelectWeapon())
						{
						case 0:
							aim_packet.state = i.is_Translation();
							if (aim_packet.state == OBJECT_NOSHIFT) continue;
							aim_packet.move = i.getMoveState();
							if (aim_packet.state == OBJECT_STOP)
								aim_packet.move &= ~PLAYER_MOVE;
							else
								aim_packet.move |= PLAYER_MOVE;
							aim_packet.id = i.getIndexID();
							aim_packet.pos_x = i.getPos().getX();
							aim_packet.pos_y = i.getPos().getY();
							aim_packet.pos_z = i.getPos().getZ();


							aim_packet.look_x = i.getLook().getX();
							aim_packet.arm_y = i.getLook().getY();
							aim_packet.look_z = i.getLook().getZ();
							memcpy(&packetZip[iter], &aim_packet, sizeof(aim_packet));
							iter += sizeof(aim_packet);
							break;
						case 1:
							if (i.is_Translation() == OBJECT_NOSHIFT) continue;
							aim_packet2.id = i.getIndexID();
							aim_packet2.look_x = i.getLook().getX();
							aim_packet2.arm_y = i.getLook().getY();
							aim_packet2.look_z = i.getLook().getZ();
							memcpy(&packetZip[iter], &aim_packet2, sizeof(aim_packet2));
							cout << "2번보내는중" << endl;
							iter += sizeof(aim_packet2);
							break;
						default:
							break;
						}

						
					}

					// 통상모드일 경우
					else {
						move_packet.state = i.is_Translation();
						if (move_packet.state == OBJECT_NOSHIFT) continue;

						move_packet.move = i.getMoveState();
						if (move_packet.state == OBJECT_STOP)
							move_packet.move &= ~PLAYER_MOVE;
						else
							move_packet.move |= PLAYER_MOVE;
						move_packet.id = i.getIndexID();
						move_packet.pos_x = i.getPos().getX();
						move_packet.pos_y = i.getPos().getY();
						move_packet.pos_z = i.getPos().getZ();
						/*						cout << "보내는 패킷 : " << move_packet.id << ", "<<
													(int)move_packet.state << ", " << (int)move_packet.move << ",  pos : " <<
													move_packet.pos_x << ", " <<
													move_packet.pos_y << ", " <<
													move_packet.pos_z << endl;*/
						memcpy(&packetZip[iter], &move_packet, sizeof(move_packet));
						iter += sizeof(move_packet);
					}

					pthis->SendBuffer(static_cast<int>(key), iter, PACKET_MAXSIZE, packetZip);
				}
				pthis->SendBuffer(static_cast<int>(key), iter, 0, packetZip);

				sc_packet_ecreate ecreate_packet;
				sc_packet_emove emove_packet;
				sc_packet_objRemove eremove_packet;

				sc_packet_bcreate bcreate_packet;
				sc_packet_bmove bmove_packet;
				sc_packet_objRemove bremove_packet;

				sc_packet_mcreate mcreate_packet;
				sc_packet_mmove mmove_packet;
				sc_packet_mboom mremove_packet;
				ZeroMemory(packetZip, MULTIPLESEND_BUFFERSIZE);
				iter = 0;


				////////////////////////////////////////////////////////////
				for (auto &i : pRoom->getPlayerIDVector()) {
					auto nearlist = i.getPushList();
					auto viewlist = i.getViewList();
					auto removelist = i.getRemoveList();

					for (auto &j : *nearlist) {
						//	if (viewlist->count(j) == 0) {
						viewlist->insert(j);
						if (j < MISSILE_START_NUM) {
							// 에너미 생성
							auto pEnemy = &pRoom->getEnemyIDVector()[j];
							ecreate_packet.id = pEnemy->getID();
							ecreate_packet.m_type = pEnemy->getTypeNumber();
							ecreate_packet.pos_x = pEnemy->getPos().getX();
							ecreate_packet.pos_y = pEnemy->getPos().getY();
							ecreate_packet.pos_z = pEnemy->getPos().getZ();
							ecreate_packet.look_x = pEnemy->getDirection().getX();
							ecreate_packet.look_y = pEnemy->getDirection().getY();
							ecreate_packet.look_z = pEnemy->getDirection().getZ();
							memcpy(&packetZip[iter], &ecreate_packet, sizeof(ecreate_packet));
							iter += sizeof(ecreate_packet);
						}
						else {
							// 미사일 생성
							auto pMissile = &pRoom->getMissileIDVector().at(j - MISSILE_START_NUM);
							mcreate_packet.id = pMissile->getID();
							mcreate_packet.m_type = pMissile->getTypeNumber();
							mcreate_packet.pos_x = pMissile->getPos().getX();
							mcreate_packet.pos_y = pMissile->getPos().getY();
							mcreate_packet.pos_z = pMissile->getPos().getZ();
							mcreate_packet.look_x = pMissile->getDirection().getX();
							mcreate_packet.look_y = pMissile->getDirection().getY();
							mcreate_packet.look_z = pMissile->getDirection().getZ();
							memcpy(&packetZip[iter], &mcreate_packet, sizeof(mcreate_packet));
							iter += sizeof(mcreate_packet);
						}
						//	}
						pthis->SendBufferToPlayer(static_cast<int>(key), i.getPlayerID(), iter, PACKET_MAXSIZE, packetZip);
					}


					for (auto &j : *viewlist) {
						if (j < MISSILE_START_NUM) {
							// 적 이동
							auto pEnemy = &pRoom->getEnemyIDVector()[j];
							emove_packet.move = pEnemy->isTranslate();
							emove_packet.id = pEnemy->getID();
							emove_packet.pos_x = pEnemy->getPos().getX();
							emove_packet.pos_y = pEnemy->getPos().getY();
							emove_packet.pos_z = pEnemy->getPos().getZ();
							memcpy(&packetZip[iter], &emove_packet, sizeof(emove_packet));
							iter += sizeof(emove_packet);
						}
						else {
							// 미사일 이동
							auto pMissile = &pRoom->getMissileIDVector()[j - MISSILE_START_NUM];
							if (pMissile->getState() == MS_BOOM) {
								mremove_packet.id = pMissile->getID();
								mremove_packet.pos_x = pMissile->getPos().getX();
								mremove_packet.pos_y = pMissile->getPos().getY();
								mremove_packet.pos_z = pMissile->getPos().getZ();
								memcpy(&packetZip[iter], &mremove_packet, sizeof(mremove_packet));
								iter += sizeof(mremove_packet);
							}
							else {
								mmove_packet.id = pMissile->getID();
								mmove_packet.pos_x = pMissile->getPos().getX();
								mmove_packet.pos_y = pMissile->getPos().getY();
								mmove_packet.pos_z = pMissile->getPos().getZ();
								memcpy(&packetZip[iter], &mmove_packet, sizeof(mmove_packet));
								iter += sizeof(mmove_packet);
							}
						}
						
						pthis->SendBufferToPlayer(static_cast<int>(key), i.getPlayerID(), iter, PACKET_MAXSIZE, packetZip);
					}

					for (auto &j : *removelist) {
						if (j < MISSILE_START_NUM) {
							// 적 삭제
							auto pEnemy = &pRoom->getEnemyIDVector()[j];
							eremove_packet.obj_type = object_Type::OBJECT_ENEMY;
							eremove_packet.id = pEnemy->getID();
							memcpy(&packetZip[iter], &eremove_packet, sizeof(eremove_packet));
							iter += sizeof(eremove_packet);
						}
						else {
							// 미사일 삭제
							auto pMissile = &pRoom->getMissileIDVector()[j - MISSILE_START_NUM];
							eremove_packet.id = pMissile->getID();
							eremove_packet.obj_type = object_Type::OBJECT_MISSILE;
							//	mremove_packet.pos_x = pMissile->getPos().getX();
							//	mremove_packet.pos_y = pMissile->getPos().getY();
							//	mremove_packet.pos_z = pMissile->getPos().getZ();
							memcpy(&packetZip[iter], &eremove_packet, sizeof(eremove_packet));
							iter += sizeof(eremove_packet);
						}
						pthis->SendBufferToPlayer(static_cast<int>(key), i.getPlayerID(), iter, PACKET_MAXSIZE, packetZip);
					}
					removelist->clear();
					pthis->SendBufferToPlayer(static_cast<int>(key), i.getPlayerID(), iter, 0, packetZip);

				}

				//		pthis->SendBuffer(static_cast<int>(key), iter, 0, packetZip);



						// 몹 움직임 패킷 
						/*
						for (auto &i : pRoom->getEnemyIDVector()) {
							if (i.is_Use()) {
								switch (i.isTranslate())
								{
								case OBJECT_CREATE:
									ecreate_packet.id = i.getID();
									ecreate_packet.m_type = i.getTypeNumber();
									ecreate_packet.pos_x = i.getPos().getX();
									ecreate_packet.pos_y = i.getPos().getY();
									ecreate_packet.pos_z = i.getPos().getZ();
									ecreate_packet.look_x = i.getDirection().getX();
									ecreate_packet.look_y = i.getDirection().getY();
									ecreate_packet.look_z = i.getDirection().getZ();
									memcpy(&packetZip[iter], &ecreate_packet, sizeof(ecreate_packet));
									iter += sizeof(ecreate_packet);
									i.createOver();
									break;
								case OBJECT_MOVE:
									if (i.getID() == -1) {
										sc_packet_aim_move1 tempp;
										tempp.id = 0;
										tempp.state = OBJECT_MOVE;
										tempp.move = PLAYER_MOVE;
										tempp.pos_x = i.getPos().getX();
										tempp.pos_y = i.getPos().getY();
										tempp.pos_z = i.getPos().getZ();
										tempp.look_x = 0;
										tempp.arm_y = 1;
										tempp.look_z = 0;
										memcpy(&packetZip[iter], &tempp, sizeof(tempp));
										iter += sizeof(tempp);
									}
									emove_packet.move = i.isTranslate();
									emove_packet.id = i.getID();
									emove_packet.pos_x = i.getPos().getX();
									emove_packet.pos_y = i.getPos().getY();
									emove_packet.pos_z = i.getPos().getZ();
									memcpy(&packetZip[iter], &emove_packet, sizeof(emove_packet));
									iter += sizeof(emove_packet);
									break;

								case OBJECT_STOP:
									emove_packet.move = i.isTranslate();
									emove_packet.id = i.getID();
									emove_packet.pos_x = i.getPos().getX();
									emove_packet.pos_y = i.getPos().getY();
									emove_packet.pos_z = i.getPos().getZ();
									memcpy(&packetZip[iter], &emove_packet, sizeof(emove_packet));
									iter += sizeof(emove_packet);
									break;

								case OBJECT_NOSHIFT:
									break;
								case OBJECT_REMOVE:
									eremove_packet.obj_type = OBJECT_ENEMY;
									eremove_packet.id = i.getID();
									i.removeEnemy();
									memcpy(&packetZip[iter], &eremove_packet, sizeof(eremove_packet));
									iter += sizeof(eremove_packet);
									break;
								}
								pthis->SendBuffer(static_cast<int>(key), iter, PACKET_MAXSIZE, packetZip);
							}
						}
						pthis->SendBuffer(static_cast<int>(key), iter, 0, packetZip);







						// 미사일 움직임 패킷
						// 사실 위에꺼 재활용해도 됨....
						ZeroMemory(packetZip, MULTIPLESEND_BUFFERSIZE);
						iter = 0;

						for (auto &i : pRoom->getMissileIDVector()) {
							if (i.is_Use()) {
								switch (i.isTranslate())
								{
								case OBJECT_CREATE:
									mcreate_packet.id = i.getID();
									mcreate_packet.m_type = i.getTypeNumber();
									mcreate_packet.pos_x = i.getPos().getX();
									mcreate_packet.pos_y = i.getPos().getY();
									mcreate_packet.pos_z = i.getPos().getZ();
									mcreate_packet.look_x = i.getDirection().getX();
									mcreate_packet.look_y = i.getDirection().getY();
									mcreate_packet.look_z = i.getDirection().getZ();
									memcpy(&packetZip[iter], &mcreate_packet, sizeof(mcreate_packet));
									iter += sizeof(mcreate_packet);
									i.createOver();
									break;
								case OBJECT_MOVE:
								case OBJECT_STOP:
									mmove_packet.id = i.getID();
									mmove_packet.pos_x = i.getPos().getX();
									mmove_packet.pos_y = i.getPos().getY();
									mmove_packet.pos_z = i.getPos().getZ();
									memcpy(&packetZip[iter], &mmove_packet, sizeof(mmove_packet));
									iter += sizeof(mmove_packet);
									break;
								case OBJECT_NOSHIFT:
									break;
								case OBJECT_REMOVE:
									mremove_packet.pos_x = i.getPos().getX();
									mremove_packet.pos_y = i.getPos().getY();
									mremove_packet.pos_z = i.getPos().getZ();
									mremove_packet.id = i.getID();
									i.removeMissile();
									memcpy(&packetZip[iter], &mremove_packet, sizeof(mremove_packet));
									iter += sizeof(mremove_packet);
									break;
								}
								pthis->SendBuffer(static_cast<int>(key), iter, PACKET_MAXSIZE, packetZip);
							}
						}
						pthis->SendBuffer(static_cast<int>(key), iter, 0, packetZip);
						*/

				ZeroMemory(packetZip, MULTIPLESEND_BUFFERSIZE);
				iter = 0;

				// 보스 움직임 패킷
				for (auto &i : pRoom->getBossIDVector()) {
					if (i.is_Use()) {
						switch (i.isTranslate())
						{
						case OBJECT_CREATE:
							bcreate_packet.id = i.getID();
							bcreate_packet.m_type = i.getTypeNumber();
							bcreate_packet.pos_x = i.getPos().getX();
							bcreate_packet.pos_y = i.getPos().getY();
							bcreate_packet.pos_z = i.getPos().getZ();
							bcreate_packet.look_x = i.getDirection().getX();
							bcreate_packet.look_y = i.getDirection().getY();
							bcreate_packet.look_z = i.getDirection().getZ();
							memcpy(&packetZip[iter], &bcreate_packet, sizeof(bcreate_packet));
							iter += sizeof(bcreate_packet);
							i.createOver();
							break;
						case OBJECT_MOVE:
							bmove_packet.move = i.isTranslate();
							bmove_packet.id = i.getID();
							bmove_packet.pos_x = i.getPos().getX();
							bmove_packet.pos_y = i.getPos().getY();
							bmove_packet.pos_z = i.getPos().getZ();
							memcpy(&packetZip[iter], &bmove_packet, sizeof(bmove_packet));
							iter += sizeof(bmove_packet);
							break;

						case OBJECT_STOP:
							bmove_packet.move = i.isTranslate();
							bmove_packet.id = i.getID();
							bmove_packet.pos_x = i.getPos().getX();
							bmove_packet.pos_y = i.getPos().getY();
							bmove_packet.pos_z = i.getPos().getZ();
							memcpy(&packetZip[iter], &bmove_packet, sizeof(bmove_packet));
							iter += sizeof(bmove_packet);
							break;

						case OBJECT_NOSHIFT:
							break;
						case OBJECT_REMOVE:
							bremove_packet.obj_type = OBJECT_BOSS;
							bremove_packet.id = i.getID();
							i.removeEnemy();
							memcpy(&packetZip[iter], &bremove_packet, sizeof(bremove_packet));
							iter += sizeof(bremove_packet);
							break;
						}
						pthis->SendBuffer(static_cast<int>(key), iter, PACKET_MAXSIZE, packetZip);
					}
				}
				pthis->SendBuffer(static_cast<int>(key), iter, 0, packetZip);





				// 오브젝트 움직임 패킷
				sc_packet_ocreate ocreate_packet;
				sc_packet_omove omove_packet;
				sc_packet_objRemove oremove_packet;
				ZeroMemory(packetZip, MULTIPLESEND_BUFFERSIZE);
				for (auto &i : pRoom->getObjectIDVector()) {
					if (i.is_Use()) {
						switch (i.isTranslate())
						{
						case OBJECT_CREATE:
							ocreate_packet.id = i.getID();
							ocreate_packet.o_type = i.getObjectType();	// ~~~~~~ 이거 의미없음
							ocreate_packet.pos_x = i.getPos().getX();
							ocreate_packet.pos_y = i.getPos().getY();
							ocreate_packet.pos_z = i.getPos().getZ();
							ocreate_packet.look_x = i.getDirection().getX();
							ocreate_packet.look_y = i.getDirection().getY();
							ocreate_packet.look_z = i.getDirection().getZ();
							memcpy(&packetZip[iter], &ocreate_packet, sizeof(ocreate_packet));
							iter += sizeof(ocreate_packet);
							i.createOver();
							break;
						case OBJECT_MOVE:
						case OBJECT_STOP:
							omove_packet.id = i.getID();
							omove_packet.pos_x = i.getPos().getX();
							omove_packet.pos_y = i.getPos().getY();
							omove_packet.pos_z = i.getPos().getZ();
							omove_packet.look_x = i.getDirection().getX();
							omove_packet.look_y = i.getDirection().getY();
							omove_packet.look_z = i.getDirection().getZ();
							memcpy(&packetZip[iter], &omove_packet, sizeof(omove_packet));
							iter += sizeof(omove_packet);
							break;
						case OBJECT_NOSHIFT:
							break;
						case OBJECT_REMOVE:
							oremove_packet.id = i.getID();
							// !@#$%^ 제거작업 필요	i.removeMissile();
							memcpy(&packetZip[iter], &oremove_packet, sizeof(oremove_packet));
							iter += sizeof(oremove_packet);
							break;
						}

						pthis->SendBuffer(static_cast<int>(key), iter, PACKET_MAXSIZE, packetZip);
					}
				}
				pthis->SendBuffer(static_cast<int>(key), iter, 0, packetZip);


				// 이벤트 패킷
				sc_packet_event event_packet;
				ZeroMemory(packetZip, MULTIPLESEND_BUFFERSIZE);

				EVENTKEY event_key;
				auto room = &pthis->roomList[static_cast<int>(key)];
				while (true) {
					if (room->getEventQueue().empty()) break;
					if (room->getEventQueue().try_pop(event_key)) {
						// !@#$% 이거 타임아웃 걸어야함
						switch (event_key.type) {
						case EKT_PLAYER_RECOVER: {
							sc_packet_HP hp_packet;
							hp_packet.obj_type = OBJECT_PLAYER;
							hp_packet.id = static_cast<WORD>(event_key.id);
							hp_packet.hp = event_key.fvalue;
							memcpy(&packetZip[iter], &hp_packet, sizeof(hp_packet));
							iter += sizeof(hp_packet);
							cout << (int)hp_packet.hp << endl;
							break;
						}
						case EKT_PLAYER_HIT: {
							sc_packet_HP hp_packet;
							hp_packet.obj_type = OBJECT_PLAYER;
							hp_packet.id = static_cast<WORD>(event_key.id);
							hp_packet.hp = event_key.fvalue;
							memcpy(&packetZip[iter], &hp_packet, sizeof(hp_packet));
							iter += sizeof(hp_packet);
							cout << (int)hp_packet.hp << endl;
							break;
						}
						case EKT_ENEMY_HIT: {
							sc_packet_HP hp_packet;
							hp_packet.obj_type = OBJECT_ENEMY;
							hp_packet.id = static_cast<WORD>(event_key.id);
							hp_packet.hp  = event_key.fvalue;
						//	memcpy(&packetZip[iter], &hp_packet, sizeof(hp_packet));
						//	iter += sizeof(hp_packet);
							break;
						}
						case EKT_OBJECT_HIT: {
							sc_packet_HP hp_packet;
							hp_packet.obj_type = OBJECT_STATIC;
							hp_packet.id = static_cast<WORD>(event_key.id);
							hp_packet.hp = event_key.fvalue;
							memcpy(&packetZip[iter], &hp_packet, sizeof(hp_packet));
							iter += sizeof(hp_packet);
							break;
						}
						case EKT_PLAYER_DOWN:
						{
							sc_packet_pdown down_packet;
							down_packet.id = static_cast<WORD>(event_key.id);
							memcpy(&packetZip[iter], &down_packet, sizeof(down_packet));
							iter += sizeof(down_packet);
							break;
						}
						case EKT_PRINTSCRIPT: 
						{
							sc_packet_script script_packet;
							script_packet.string_id = static_cast<WORD>(event_key.id);
							memcpy(&packetZip[iter], &script_packet, sizeof(script_packet));
							iter += sizeof(script_packet);
							break;
						}
						case EKT_SETTIMER:
						{
							sc_packet_timer timer_packet;
							timer_packet.timer = static_cast<WORD>(event_key.time);
							memcpy(&packetZip[iter], &timer_packet, sizeof(timer_packet));
							iter += sizeof(timer_packet);
//							pthis->mytimer.AddTimer(static_cast<int>(key), over_ex->curr_packet_size,);
							break;
						}
						case EKT_VICTORY: {
							event_packet.event_type = static_cast<WORD>(event_key.type);
							event_packet.id = static_cast<WORD>(event_key.id);
						//	memcpy(&packetZip[iter], &event_packet, sizeof(event_packet));
						//	iter += sizeof(event_packet);
							break;
						}
						case EKT_DEFEATE: {
							event_packet.event_type = static_cast<WORD>(event_key.type);
							event_packet.id = static_cast<WORD>(event_key.id);
							//	event_packet.value = static_cast<WORD>(event_key.value);
							// memcpy(&packetZip[iter], &event_packet, sizeof(event_packet));
							// iter += sizeof(event_packet);
							//	pRoom.Deactivate();
							break;
						}
						default: {
							event_packet.event_type = static_cast<WORD>(event_key.type);
							event_packet.id = static_cast<WORD>(event_key.id);
							//	event_packet.value = static_cast<WORD>(event_key.value);
							memcpy(&packetZip[iter], &event_packet, sizeof(event_packet));
							iter += sizeof(event_packet);
							break;
						}
						}
						pthis->SendBuffer(static_cast<int>(key), iter, PACKET_MAXSIZE, packetZip);
					}
				}
				pthis->SendBuffer(static_cast<int>(key), iter, 0, packetZip);


				unsigned int time_now = GetTickCount();
				unsigned int time_spare = pRoom->stackSpareTime();
				if (time_now >= over_ex->curr_packet_size + CGameData::getInstance().getConstData()->getOPERATION_FPS_TIME() + time_spare) {
					pthis->mytimer.AddTimer(static_cast<int>(key), time_now, TYPE_ROOM_OPERATION);
					pRoom->zeroSpareTime();
				}
				else
					pthis->mytimer.AddTimer(static_cast<int>(key), over_ex->curr_packet_size + CGameData::getInstance().getConstData()->getOPERATION_FPS_TIME() + time_spare, TYPE_ROOM_OPERATION);

				/*
				auto end = chrono::high_resolution_clock().now() - start;
				cout << chrono::duration_cast<chrono::microseconds>(end).count() << endl;
				*/
			}


			delete over_ex;
			break;
		}
		case TYPE_ROOM_TIMER:
		{


			delete over_ex;
			break;
		}
		default:
		{
			break;
		}

		}


	}

	return 0;

}

void err_quit(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);
}
void err_display(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	printf("[%s] %s", msg, (char *)lpMsgBuf);
	LocalFree(lpMsgBuf);
};