#include "stdafx.h"
#include "GameSpace.h"


CGameSpace::CGameSpace()
{
	debug_missileID = 0;
}


CGameSpace::~CGameSpace()
{
}

void CGameSpace::SpaceInit(int _roomid,
	vector<CPlayer>* _vp,
	vector<CEnemy>* _ve,
	vector<CEnemy_Boss>* _vb,
	vector<CMissile>* _vm,
	vector<CStaticObject>* _vo)
{
	roomid = _roomid;
	vp = _vp;
	ve = _ve;
	vb = _vb;
	vm = _vm;
	vo = _vo;

	vvEnemySector.resize(CGameData::getInstance().getConstData()->getSPACE_PARTITION_X());
	for (int i = 0; i < CGameData::getInstance().getConstData()->getSPACE_PARTITION_X(); ++i) {
		vvEnemySector[i].resize(CGameData::getInstance().getConstData()->getSPACE_PARTITION_Z());
		for (auto &j : vvEnemySector[i]) {
			j.reserve(100);
		}
	}
	vExtraSector.reserve(100);

	vMML.clear();
	vMML.resize(8);
}

void CGameSpace::pushSector(int _enemyID)
{
	FLOAT3 pos = (ve->at(_enemyID).getPos() + FLOAT3(MAP_SIZE_X / 2, 0, MAP_SIZE_Z / 2));
	if (pos.getX() < 0 || pos.getX() >= MAP_SIZE_X || pos.getZ() < 0 || pos.getZ() >= MAP_SIZE_Z) {
		vExtraSector.push_back(_enemyID);
		return;
	}
	pos.getX() /= MAP_SIZE_X / CGameData::getInstance().getConstData()->getSPACE_PARTITION_X();
	pos.getZ() /= MAP_SIZE_Z / CGameData::getInstance().getConstData()->getSPACE_PARTITION_Z();
	vvEnemySector[static_cast<int>(pos.getX())][static_cast<int>(pos.getZ())].push_back(_enemyID);
}

void CGameSpace::clearSector()
{
	for (int i = 0; i < CGameData::getInstance().getConstData()->getSPACE_PARTITION_X(); ++i) {
		for (auto &j : vvEnemySector[i]) {
			j.clear();
		}
	}
	vExtraSector.clear();
}

CGameObject * CGameSpace::setTarget(unsigned int _pid)
{
	CGameObject *target = nullptr;
	if (vp->at(_pid).is_Use() && vp->at(_pid).is_Translation() != OBJECT_DEAD)
		return &vp->at(_pid);
	else
		return nullptr;
}

CGameObject * CGameSpace::setTargetToPlayer(FLOAT3 _pos, float _sight)
{
	CGameObject *target = nullptr;
	float distance = _sight;
	for (auto &i : *vp) {
		if (i.is_Use() && i.is_Translation() != OBJECT_DEAD) {
			float temp = (i.getPos() - _pos).getLength_noSqrt();
			if (temp < distance && temp < _sight) {
				distance = temp;
				target = &i;
			}
		}
	}
	if (target == nullptr) {
		auto &i = *vo->begin();
		if(i.is_Use())
			target = &i;
	}
	return target;
}

CGameObject * CGameSpace::setTargetToPlayerCompulsory(FLOAT3 _pos)
{
	CGameObject *target = nullptr;
	float distance;
	bool start = true;
	for (auto &i : *vp) {
		if (i.is_Use() && i.is_Translation() != OBJECT_DEAD) {
			float temp = (i.getPos() - _pos).getLength_noSqrt();
			if (start) {
				distance = temp;
				target = &i;
				start = false;
			}
			else if (temp < distance) {
				distance = temp;
				target = &i;
			}
		}
	}
	return target;
}

CGameObject * CGameSpace::setTargetToPlayerExceptObject(FLOAT3 _pos, float _sight)
{
	CGameObject *target = nullptr;
	float distance = _sight;
	for (auto &i : *vp) {
		if (i.is_Use() && i.is_Translation() != OBJECT_DEAD) {
			float temp = (i.getPos() - _pos).getLength_noSqrt();
			if (temp < distance && temp < _sight) {
				distance = temp;
				target = &i;
			}
		}
	}
	return target;
}

CGameObject * CGameSpace::setTargetToObject()
{
	return vo->begin()._Ptr;
}

CGameObject * CGameSpace::setTargetToEnemy(int _target_id, FLOAT3 _pos, float _sight)
{
	CGameObject *target = nullptr;
	float distance = _sight;
	for (auto &i : *ve) {
		if (i.is_Use()) {
			float temp = (i.getPos() - _pos).getLength();
			if (temp < distance && temp < _sight) {
				distance = temp;
				target = &i;
			}
		}
	}

	//		float temp = (i.getPos() - _pos).getLength_noSqrt();

		/*
		else
			if (i.isUse()) {
				float temp = (i.getPos() - _pos).getLength_noSqrt();
				if (temp < distance && temp < _sight) {
					distance = temp;
					target = &i;
				}
			}

		if (target == nullptr) {
			auto &i = *vo->begin();
			target = &i;
		}
		*/
	return target;
}

void CGameSpace::ShootMissile(FLOAT3 _pos, FLOAT3 _dir, int _owner_id, int _missile_type, OWNER_TYPE _owner_type, CGameObject* _target)
{
	for (auto &i : *vm) {
		if (i.is_Use()) continue;
		i.putMissile(_pos, _dir.Normalize(), _owner_id, _missile_type, _owner_type, _target);

		//		cout << "미사일 발사, 타입 : " << _missile_type << ", 충돌체크 타입 : " << _owner_type << ", 타겟 ID : ";
		//		if (_target != NULL)
		//			cout << _target->getID() << endl;
		//		else
			//		cout << "NULL" << endl;
		int t = i.getID();
		if (t > debug_missileID) {
			debug_missileID = t;
#ifdef _DEBUG
			//		cout << "최대 미사일 넘버 : " << debug_missileID << endl;
#endif
		}
		break;
	}
}

void CGameSpace::ShootLaunchedMissile()
{
	int i = 0;
	for (auto &data : vMML) {
		if (data.isUse()) 
		{
			int frame = data.getLaunchFrame();
			if (frame >= 0) {
				auto player = &vp->at(i++);
				auto port = player->getpMecha()->getPort(data.getSlot());
				if (frame >= player->getpMecha()->getPort_num(data.getSlot())) {	// 다 쐈다면
					data.endMissileLaunch();					
					continue;
				}
				
				auto pos = port->at(frame).pos;
				auto rotation = data.getRotationMatrix();

				DirectX::XMVECTOR v1 = DirectX::XMLoadFloat3(&pos.data);
				DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, rotation);
				DirectX::XMStoreFloat3(&pos.data, vResult);
				auto dir = data.getLook();
				ShootMissile(pos, dir, player->getPlayerID(), player->getpMecha()->getWeaponType(data.getSlot()),
					OWNER_TYPE::OT_PLAYER, NULL);
			}
			else {
				i++;
				continue;
			}
		}			
		i++;
	}
}

void CGameSpace::setMultipleMissile(int _player_id, int _slot, FLOAT3 _pos, FLOAT3 _look, FLOAT3 _up, FLOAT3 _right)
{
	int t = 0;
	for (auto &i : *vp) {
		if (i.getPlayerID() == _player_id && i.is_Use()) {
			if (vMML[t].isUse()) break;
			vMML[t].setMissileLaunch(_slot, _pos, _look, _up, _right);
			break;
		}
		t++;
	}
}

bool CGameSpace::CollisionChecktoEnemy(int missile_id)
{
	vector<int>* sector;

	int sector_x = static_cast<int>((vm->at(missile_id).getPos().getX()
		+ (MAP_SIZE_X / 2)) / (MAP_SIZE_X / CGameData::getInstance().getConstData()->getSPACE_PARTITION_X()));
	int sector_z = static_cast<int>((vm->at(missile_id).getPos().getZ()
		+ (MAP_SIZE_Z / 2)) / (MAP_SIZE_Z / CGameData::getInstance().getConstData()->getSPACE_PARTITION_Z()));

	if (sector_x < 0 || sector_x >= CGameData::getInstance().getConstData()->getSPACE_PARTITION_X() || sector_z < 0 || sector_z >= CGameData::getInstance().getConstData()->getSPACE_PARTITION_Z())
		sector = &vExtraSector;
	else
		sector = &vvEnemySector[sector_x][sector_z];

	auto mPos = vm->at(missile_id).getPos();
	auto mDir = vm->at(missile_id).getDirection();
	auto mBO = vm->at(missile_id).getType()->getBO(0);	//!@#$ FF

	for (auto &i : *sector) {
		// ve->at(i).is_Use();  <- sector 만들때 쓰는놈인지 확인하고 넣기 때문에 필요없음
		auto ePos = ve->at(i).getPos();
		auto eDir = ve->at(i).getDirection();
		auto eBO = ve->at(i).getType()->getBO(0);;	//!@#$ FF

		if (mBO->checkCollision(mPos, mDir, ePos, eDir, eBO)) {
			//FLOAT3 f = vm->at(missile_id).getPos() - ve->at(i).getPos();
			//if (f.getLength_noSqrt() < enemy_col) {
			vm->at(missile_id).Crash();
			if (ve->at(i).Hit(vm->at(missile_id).getDamage()))
				return true;
			break;
		}
	}
	return false;
}

int CGameSpace::CollisionChecktoBoss(int missile_id)
{
	auto mPos = vm->at(missile_id).getPos();
	auto mDir = vm->at(missile_id).getDirection();
	auto mBO = vm->at(missile_id).getType()->getBO(0);	//!@#$ FF
	
	for (auto &i : *vb) {
		if (i.is_Use()) { //  <- sector 만들때 쓰는놈인지 확인하고 넣기 때문에 필요없음
			auto bPos = i.getPos();
			auto bDir = i.getDirection();
			auto bBO = dynamic_cast<CBoundingObject_Multiple*>(i.getType()->getBO(0));	//!@#$ FF

			int part = bBO->checkCollision_Where(bPos, bDir, mPos, mDir, mBO);
			if (part >= 0) {
				cout << "보스 맞은 부위 : " << part << endl;
				//FLOAT3 f = vm->at(missile_id).getPos() - ve->at(i).getPos();
				//if (f.getLength_noSqrt() < enemy_col) {
				vm->at(missile_id).Crash();
				//		if (ve->at(i).Hit(vm->at(missile_id).getDamage()))
				//			return true;
				break;
			}
		}
	}
	
	return -1;
}

void CGameSpace::ClearNearList()
{
	for (auto &i : *vp) {
		if (i.is_Use()) {
			i.clearViewList();
		}
	}
}

void CGameSpace::pushEnemyToNearList(int _enemyid)
{
	for (auto &i : *vp) {
		if (i.is_Use()) {
			float length_nosqrt = (i.getPos() - ve->at(_enemyid).getPos()).getLength_noSqrt();
			if (length_nosqrt <= CGameData::getInstance().getConstData()->getCLIENT_VIEW_NOSQRT()) {
				i.pushNearList(_enemyid + ENEMY_START_NUM);
			}
		}
	}
}

void CGameSpace::pushMissileToNearList(int _missileid)
{
	for (auto &i : *vp) {
		if (i.is_Use()) {
			float length_nosqrt = (i.getPos() - vm->at(_missileid).getPos()).getLength_noSqrt();
			if (length_nosqrt <= CGameData::getInstance().getConstData()->getCLIENT_VIEW_NOSQRT()) {
				i.pushNearList(_missileid + MISSILE_START_NUM);
			}
		}
	}
}

void CGameSpace::RelocateViewList()
{
	for (auto &i : *vp) {
		i.RelocateViewList();
	}
}
