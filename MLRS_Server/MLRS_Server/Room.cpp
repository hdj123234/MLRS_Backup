#include "stdafx.h"
#include "Room.h"
#include <time.h>

CRoom::CRoom()
{
	ZeroMemory(title, ROOM_MAX_TITLE_LEN);
	is_use = false;
	id = -1;
	maxplayer = 0;
	state = 0;
	map_id = 0;

	pplot = nullptr;
	sceneID = 0;
	lineID = 0;
	linechecker.state = CHECK_DEFAULT;

	vEnemy.reserve(MAXSIZE_ENEMY_NUMBER);
	vMissile.reserve(MAXSIZE_MISSILE_NUMBER);

	space.SpaceInit(id, &connection_player, &vEnemy, &vBoss, &vMissile, &vObject);
}


bool CRoom::CreateRoom(int roomid, char* roomtitle, int maxp)
{
	id = roomid;
	memcpy(title, roomtitle, strlen(roomtitle));
	maxplayer = maxp;
	state = 0;
	map_id = 0;
	connection_player.clear();
	connection_player.resize(maxp);
	is_use = true;

	pplot = CGameData::getInstance().getPlot(CGameData::getInstance().getMap(map_id)->getPlotID());
	sceneID = 0;
	lineID = 0;
	linechecker.state = CHECK_DEFAULT;

#ifdef _DEBUG
	std::cout << id << "번 방이 생성되었습니다.  " <<
		"방제 : " << getTitle() << std::endl <<
		"현재 인원 : " << getCurrentPlayer() <<
		" / " << getMaxPlayer() << std::endl;
#endif
	return true;
}


void CRoom::InitRoom(int _id)
{
	debug_FPScount = 0;
	debug_pre = 0;
	debug_cur = 0;

	ZeroMemory(title, ROOM_MAX_TITLE_LEN);
	is_use = false;
	id = _id;
	maxplayer = 0;
	state = 0;
	map_id = 0;

	pplot = nullptr;
	sceneID = 0;
	lineID = 0;
	linechecker.state = CHECK_DEFAULT;

	spare_time = 0;

	initPlyaerPos.clear();

	vEnemy.resize(MAXSIZE_ENEMY_NUMBER);
	for (int i = 0; i < MAXSIZE_ENEMY_NUMBER; ++i)
		vEnemy[i].InitEnemy(i);

	vBoss.resize(1);
	for (int i = 0; i < 1; ++i)
		vBoss[i].InitEnemy(i);

	vMissile.resize(MAXSIZE_MISSILE_NUMBER);
	for (int i = 0; i < MAXSIZE_MISSILE_NUMBER; ++i)
		vMissile[i].InitMissile(i);


	vObject.clear();
	CStaticObject base(0);
	// base.Activate();
	base.setMaxHP(140);
	base.setHP(CGameData::getInstance().getConstData()->getBASECAMP_HP());
	base.setAngle(0);
	base.setPos(FLOAT3(825, CGameData::getInstance().getMap(map_id)->getHeight(825, 1304), 1304));
	base.setLook(FLOAT3(0, 0, 1));
	base.setCenter(FLOAT3(0, 5, 0));
	base.setBoundingBox(BOUNDING_BOX(30.227f, 0.f, -70, 70, -70, 70));
	base.setObjectType(0);
	vObject.push_back(base);

	space.SpaceInit(id, &connection_player, &vEnemy, &vBoss, &vMissile, &vObject);
}

CRoom::~CRoom()
{
}

void CRoom::setMecha(int playerid, int mecha)
{
	for (auto &i : connection_player) {
		if (i.getPlayerID() == playerid) {
			i.setMecha(mecha);
			break;
		}
	}
}
void CRoom::setReady(int playerid, bool ready)
{
	for (auto &i : connection_player) {
		if (i.getPlayerID() == playerid) {
			i.setReady(ready);
			break;
		}
	}
}



bool CRoom::canEnter()
{
	if (!isUse())
		return false;

	size_t player_size = connection_player.size();
	if (maxplayer > player_size && state == 0)
		return true;
	else
		return false;
}

int CRoom::canStart()
{
	/*	for (auto &i = connection_player.begin(); i != connection_player.end(); ++i) {
			if (i->is_Ready() == false)
				return SC_START_RESULT_NOTREADY;

			//	아직 정해지지 않은 조건에 걸릴 경우
			//	if(i-> ??? == false)
			//		return SC_START_RESULT_ERROR;
		}
	*/
	// 모든 조건을 만족하면 성공을 리턴
	//#debug
	//
	return SC_START_RESULT_OK;
}

void CRoom::AddPlayer(int _id)
{
	int index_id = 0;
	for (auto i = connection_player.begin(); i != connection_player.end(); ++i) {
		if (!i->is_Use()) {
			i->ActivatePlayer(_id, index_id, initPlyaerPos.getX(), initPlyaerPos.getZ());
			break;
		}
		index_id++;
	}
}

void CRoom::RemovePlayer(int playerid)
{
	for (auto i = connection_player.begin(); i != connection_player.end(); ++i)
	{
		if (i->getPlayerID() == playerid) {
			i->RemovePlayer();
			break;
		}
	}

	RemoveRoom();
}


void CRoom::RemoveRoom()
{
	bool is_empty = true;
	for (auto &i = connection_player.begin(); i != connection_player.end(); ++i) {
		if (i->is_Use())
			is_empty = false;
	}

	if (is_empty) {
		is_use = false;
#ifdef _DEBUG
		std::cout << id << "번 방이 사라졌습니다." << std::endl;
#endif
		id = -1;
		ZeroMemory(title, ROOM_MAX_TITLE_LEN);
		maxplayer = 0;
		state = 0;

		for (auto &i : vEnemy)
			i.removeEnemy();
		for (auto &i : vBoss)
			i.removeEnemy();
		for (auto &i : vMissile)
			i.removeMissile();
		for (auto &i : vObject)
			i.initObject();
		space.clearSector();

		keybuffer.clear();
		qEvent.clear();
		current_time = 0;
		state = 0;
	}
}

int CRoom::getCurrentPlayer()
{
	int currentP = 0;
	for (auto &i : connection_player) {
		if (i.is_Use())
			currentP++;
	}
	return currentP;
}


void CRoom::coercionStart(int _id)
{
	if (!isUse()) {
		CreateRoom(24, "디버그 룸", 8);
		// 몹 추가하는 디버깅
		for (auto i = vEnemy.begin(); i != vEnemy.end(); ++i) {
			i->removeEnemy();
		}
		for (auto i = vMissile.begin(); i != vMissile.end(); ++i) {
			i->removeMissile();
		}
	}

	AddPlayer(_id);



	state = 1;
	space.setMap(map_id);

	//	vEnemy.push_back(te);
}

void CRoom::putKeybuf(KEYBUF k)
{
	keybuffer.push(k);
}

void CRoom::Operation()
{
	// GamePlot 대기
	switch (linechecker.state) {
	case CHECK_DEFAULT:
		keybuffer.push(pplot->getSceneKey(&sceneID, &lineID));
		break;
	case CHECK_WAIT:
		if (linechecker.wait_value < GetTickCount())
			linechecker.state = CHECK_DEFAULT;
		break;
	case CHECK_MOB_NUMBER:
	{
		unsigned int count = 0;
		for (auto &i : vEnemy) {
			if (i.is_Use()) {
				count++;
			}
		}
		if (count <= linechecker.wait_value)
			linechecker.state = CHECK_DEFAULT;
		break;
	}
	case CHECK_PLAYER_POS:
	{
		for (auto &i : connection_player) {
			if (!i.is_Use()) continue;
			cout << i.getPos().getX() << ", " << i.getPos().getZ() << endl;
			float rad = linechecker.wait_pos.getY()*linechecker.wait_pos.getY();
			FLOAT3 dist = i.getPos() - FLOAT3(linechecker.wait_pos.getX(), 0, linechecker.wait_pos.getZ());
			if (rad <= dist.getLength_noSqrt()) {
			//	cout << "]" << endl;
				linechecker.state = CHECK_DEFAULT;
				break;
			}
		}
		break;
	}
	case CHECK_PLAYER_XPOS_LEFT:
	{
		for (auto &i : connection_player) {
			if (!i.is_Use()) continue;
			float check_x = linechecker.wait_pos.getX();
			if (i.getPos().getX() <= check_x) {
			//	cout << "오예" << endl;
				linechecker.state = CHECK_DEFAULT;
				break;
			}
		}
		break;
	}
	case CHECK_PLAYER_ZPOS_UPPER:
	{
		for (auto &i : connection_player) {
			if (!i.is_Use()) continue;
			float check_z = linechecker.wait_pos.getZ();
			if (i.getPos().getZ() >= check_z) {
			//	cout << "오예" << endl;
				linechecker.state = CHECK_DEFAULT;
				break;
			}
		}
		break;
	}
	case CHECK_END:
		break;
	default:
		break;
	}


	// 키 버퍼에 쌓인 데이터들 적용
	while (true) {
		// !@#$%^ 타임아웃 걸어야함
		KEYBUF frontKey;
		if (keybuffer.empty()) break;
		if (keybuffer.try_pop(frontKey)) {
			// 자료가 있으면 데이터를 뽑고 키 분석 시작
			switch (frontKey.type1) {
			case KEYTYPE_MOVE:
			{
				for (auto i = connection_player.begin();
					i != connection_player.end(); ++i) {
					if (i->getPlayerID() != frontKey.id) continue;
					if (i->is_Translation() == OBJECT_DEAD)
						break;

					if (i->is_AimMode()) i->unAim();

					BYTE bit_check = static_cast<BYTE>(frontKey.type2);
					//					i->setMoveState(bit_check);
					if (bit_check & MT_MOVE) {
						float x, z;
						memcpy(&x, &frontKey.buf[0], sizeof(x));
						memcpy(&z, &frontKey.buf[4], sizeof(z));
						i->setLookandSpeed(x, z);
					}
					else
						i->setSpeed(0, 0);


					// !@#$DD
					//cout << "이동패킷 수신. 방향 : x = " << frontKey.fkey1 << ", z = " << frontKey.fkey3 <<
					//	", Jump = " << (bool)(bit_check & MT_JUMP) << ", Boost = " << (bool)(bit_check & MT_BOOST) << endl;

					if (bit_check & MT_JUMP)
						i->Jump();
					if (bit_check & MT_BOOST)
						i->BoostOn();
					else
						i->BoostOff();
					break;
				}
				break;
			}
			case KEYTYPE_AIM1:
			{
				for (auto i = connection_player.begin();
					i != connection_player.end(); ++i) {
					if (i->getPlayerID() != frontKey.id) continue;
					if (i->is_Translation() == OBJECT_DEAD) break;
					if (!i->is_AimMode()) i->Aim();
					float pos_x, pos_z;
					memcpy(&pos_x, &frontKey.buf[0], sizeof(pos_x));
					memcpy(&pos_z, &frontKey.buf[4], sizeof(pos_z));
					i->setSpeed(pos_x, pos_z);
					FLOAT3 pre_look = i->getLook();
					float lock_x, lock_y, lock_z;
					memcpy(&lock_x, &frontKey.buf[8], sizeof(lock_x));
					memcpy(&lock_y, &frontKey.buf[12], sizeof(lock_y));
					memcpy(&lock_z, &frontKey.buf[16], sizeof(lock_z));

					i->setLook(lock_x, lock_y, lock_z);

					BYTE bit_check = static_cast<BYTE>(frontKey.type2);

					// !@#$DD
					//cout << "에임패킷 수신. 방향 : x = " << frontKey.fkey1 << ", z = " << frontKey.fkey2 <<
					//	", Jump = " << (bool)(bit_check & MT_JUMP) << ", Boost = " << (bool)(bit_check & MT_BOOST) << endl;


					if (bit_check & MT_JUMP)
						i->Jump();
					if (bit_check & MT_BOOST)
						i->BoostOn();
					else
						i->BoostOff();
					if (pre_look.getX() != lock_x
						|| pre_look.getY() != lock_y
						|| pre_look.getZ() != lock_z)
						i->transRotate();
					break;
				}
				break;
			}
			case KEYTYPE_AIM2:
			{
				for (auto i = connection_player.begin();
					i != connection_player.end(); ++i) {
					if (i->getPlayerID() != frontKey.id) continue;
					if (i->is_Translation() == OBJECT_DEAD) break;
					if (!i->is_AimMode()) i->Aim();

					float lock_x, lock_y, lock_z;
					memcpy(&lock_x, &frontKey.buf[0], sizeof(lock_x));
					memcpy(&lock_y, &frontKey.buf[4], sizeof(lock_y));
					memcpy(&lock_z, &frontKey.buf[8], sizeof(lock_z));

					FLOAT3 pre_look = i->getLook();

					i->setLook(lock_x, lock_y, lock_z);

					if (pre_look.getX() != lock_x
						|| pre_look.getZ() != lock_z)
						i->transRotate();

					// !@#$DD
					//cout << "에임패킷 수신. 방향 : x = " << frontKey.fkey1 << ", z = " << frontKey.fkey2 <<
					//	", Jump = " << (bool)(bit_check & MT_JUMP) << ", Boost = " << (bool)(bit_check & MT_BOOST) << endl;

					break;
				}
				break;
			}
			case KEYTYPE_CHANGE_WEAPON:
			{
				for (auto i = connection_player.begin();
					i != connection_player.end(); ++i) {
					if (i->getPlayerID() != frontKey.id) continue;
					if (i->is_Translation() == OBJECT_DEAD) break;
					i->setSelectWeapon(frontKey.type2);
					if (i->getSelectWeapon() == 1)
						i->Aim();
					else if (i->getSelectWeapon() == 0)
						i->unAim();
					break;
				}
				break;
			}
			case KEYTYPE_SHOOT1:
			{
				for (auto i = connection_player.begin();
					i != connection_player.end(); ++i) {
					if (i->getPlayerID() != frontKey.id) continue;
					if (i->is_Translation() == OBJECT_DEAD) break;
					if (i->canShoot(frontKey.type2)) {
						i->setFireWeaponType(frontKey.type2);
						//	i->setFireWeaponType(frontKey.type2);
						float data[6];
						memcpy(data, frontKey.buf, sizeof(data));
						space.ShootMissile(FLOAT3(data[0], data[1], data[2]),
							FLOAT3(data[3], data[4], data[5]), frontKey.id,
							i->getpMecha()->getWeaponType(frontKey.type2), OWNER_TYPE::OT_PLAYER, NULL);
					}
					break;
				}
				break;
			}
			case KEYTYPE_SHOOT2:
			{
				for (auto i = connection_player.begin();
					i != connection_player.end(); ++i) {
					if (i->getPlayerID() != frontKey.id) continue;
					if (i->is_Translation() == OBJECT_DEAD) break;
					if (i->canShoot(frontKey.type2)) {
						i->setFireWeaponType(frontKey.type2);
						//	i->setFireWeaponType(frontKey.type2);

						float data[12];
						memcpy(data, frontKey.buf, sizeof(data));
						space.setMultipleMissile(frontKey.id, 2, FLOAT3(data[0], data[1], data[2]),
							FLOAT3(data[3], data[4], data[5]), FLOAT3(data[6], data[7], data[8]), FLOAT3(data[9], data[10], data[11]));
						/*
							space.ShootMissile(FLOAT3(frontKey.fkey1, frontKey.fkey2, frontKey.fkey3),
								FLOAT3(frontKey.fkey4, frontKey.fkey5, frontKey.fkey6), frontKey.id,
								i->getpMecha()->getWeaponType(frontKey.type2), OWNER_TYPE::OT_PLAYER, NULL);
								*/
					}
					break;
				}
				break;
			}
			case KEYTYPE_SHOOT3:
			{
				for (auto i = connection_player.begin();
					i != connection_player.end(); ++i) {
					if (!i->is_Use()) continue;
					if (i->getPlayerID() != frontKey.id) continue;
					if (i->is_Translation() == OBJECT_DEAD) break;
					if (i->canShoot(frontKey.type2)) {
						i->setFireWeaponType(frontKey.type2);
						//	i->setFireWeaponType(frontKey.type2);

						WORD data[16];
						memcpy(data, frontKey.buf, sizeof(data));
						auto port = i->getpMecha()->getPort(frontKey.type2);

						auto pPos = i->getPos();
						auto pDir = i->getLook();
						pDir.setY(0);
						pDir.Normalize();
						if (pDir.getX() != pDir.getX())
							cout << "뻑났다쉬발" << endl;

						unsigned int num = 0;
						unsigned int max = i->getpMecha()->getPort_num(frontKey.type2);
						for (num = 0; num < max; ++num) {
							CGameObject* target;
							if (static_cast<unsigned int>(data[num / 2]) < BOSS_START_NUM)
								target = &vEnemy[static_cast<unsigned int>(data[num / 2])];
							else if (static_cast<unsigned int>(data[num / 2]) == BOSS_START_NUM)
								target = &vBoss[0];
							else {
								target = nullptr;
								break;
							}

							auto pos_result = port->at(num).pos.getRotateY(pDir) + pPos;
							auto dir_result = port->at(num).dir.getRotateY(pDir);

							space.ShootMissile(pos_result, dir_result, frontKey.id,
								i->getpMecha()->getWeaponType(frontKey.type2), OWNER_TYPE::OT_PLAYER, target);
						}
					}
					break;
				}
				break;
			}
			case KEYTYPE_SCENELINE:
			{
				if (frontKey.id == -1) {
					switch (frontKey.type2) {
					case SceneLineID_ADDSINGLEMOB:
					{
						for (auto i = vEnemy.begin(); i != vEnemy.end(); ++i) {
							if (!i->is_Use()) {
								float posY = CGameData::getInstance().getEnemyMecha(frontKey.ikey1)->getZenposY();
								FLOAT3 target = vObject.begin()->getPos();
								i->putEnemy(frontKey.ikey1, FLOAT3(frontKey.fkey1, posY, frontKey.fkey2),
									FLOAT3(target.getX() - frontKey.fkey1, 0, target.getZ() - frontKey.fkey2));
								break;
							}
						}
						break;
					}
					case SceneLineID_ADDMULTIMOB:
					{
						KEYBUF f = frontKey;
						int addcount = 0;
						for (auto i = vEnemy.begin(); i != vEnemy.end(); ++i) {
							if (!i->is_Use()) {
								float posX = frontKey.fkey1 - (static_cast<float>(frontKey.ikey3) / 2) + rand() % frontKey.ikey3;
								float posY = CGameData::getInstance().getEnemyMecha(frontKey.ikey1)->getZenposY();
								float posZ = frontKey.fkey2 - (static_cast<float>(frontKey.ikey3) / 2) + rand() % frontKey.ikey3;
								FLOAT3 target = vObject.begin()->getPos();
								i->putEnemy(frontKey.ikey1, posX, posY, posZ, target.getX() - posX, 0, target.getZ() - posZ);

								addcount++;
								if (addcount >= frontKey.ikey2)
									break;
							}
						}
						break;
					}
					case SceneLineID_ADDBOSSMOB:
					{
						for (auto i = vBoss.begin(); i != vBoss.end(); ++i) {
							if (!i->is_Use()) {
								float posY = CGameData::getInstance().getBossMecha(frontKey.ikey1)->getZenposY();
								FLOAT3 target = vObject.begin()->getPos();
								i->putBoss(frontKey.ikey1, FLOAT3(frontKey.fkey1, posY, frontKey.fkey2),
									FLOAT3(target.getX() - frontKey.fkey1, 0, target.getZ() - frontKey.fkey2));
								break;
							}
						}
						break;
					}
					case SceneLineID_SETOBJECTPOS:
					{
						if (frontKey.ikey1 <= vObject.size()) {
							vObject[frontKey.ikey1].setPosOnTerrain(map_id, frontKey.fkey1, frontKey.fkey2);
							vObject[frontKey.ikey1].Activate();
						}
						break;
					}
					case SceneLineID_SETOBJECTSPEED:
					{
						if (frontKey.ikey1 <= vObject.size())
							vObject[frontKey.ikey1].setSpeed(FLOAT3(frontKey.fkey1, frontKey.fkey2, frontKey.fkey3));
						break;
					}
					case SceneLineID_SETINITPOS:
					{
						initPlyaerPos = FLOAT3(frontKey.fkey1,
							CGameData::getInstance().getMap(map_id)->getHeight(frontKey.fkey1, frontKey.fkey2),
							frontKey.fkey2);
						break;
					}
					case SceneLineID_SETPLAYERPOS:
					{
						for (auto &i : connection_player)
							i.shiftPos(FLOAT3(frontKey.fkey1,
								CGameData::getInstance().getMap(map_id)->getHeight(frontKey.fkey1, frontKey.fkey2),
								frontKey.fkey2));
						break;
					}
					case SceneLineID_PRINTSCRIPT:
						qEvent.push(EVENTKEY(EKT_PRINTSCRIPT, frontKey.ikey1));
						break;
					case SceneLineID_RECOVERHP:
						for (auto &i : connection_player) {
							if (!i.is_Use()) continue;
							i.healHP(frontKey.fkey1);
							qEvent.push(EVENTKEY(EKT_PLAYER_RECOVER, i.getPlayerID(), i.getHP()));
						}
						break;
					case SceneLineID_SETTIMER:
						qEvent.push(EVENTKEY(EKT_SETTIMER, frontKey.ikey1, frontKey.ikey2, frontKey.ikey3));
						break;
					case SceneLineID_REPEATSCENE:
						lineID = 0;
						break;
					case SceneLineID_WAIT:
						linechecker.wait_value = GetTickCount() + frontKey.ikey1;
						linechecker.state = CHECK_WAIT;
						break;
					case SceneLineID_BREAK:
						qEvent.push(EVENTKEY(EKT_SCENE, sceneID));
						break;
					case SceneLineID_CHECKMOB:
						linechecker.wait_value = frontKey.ikey1;
						linechecker.state = CHECK_MOB_NUMBER;
						break;
					case SceneLineID_CHECKPOS:
						linechecker.wait_pos = FLOAT3(frontKey.fkey1, frontKey.fkey2, frontKey.fkey3);	// fkey2 는 radius
						linechecker.state = CHECK_PLAYER_POS;
						break;
					case SceneLineID_CHECKXPOS:
						linechecker.wait_pos.setX(frontKey.fkey1);
						linechecker.state = CHECK_PLAYER_XPOS_LEFT;
						break;
					case SceneLineID_CHECKZPOS:
						linechecker.wait_pos.setZ(frontKey.fkey1);
						linechecker.state = CHECK_PLAYER_ZPOS_UPPER;
						break;
					case SceneLineID_WINGAME:
						cout << "Win Game" << endl;
						linechecker.state = CHECK_END;
						break;
					case SceneLineID_DEFEATGAME:
						cout << "Defeat Game" << endl;
						linechecker.state = CHECK_END;
						break;
					default:
						break;
					}

				}
				break;
			}
			case 120:
				// 방 제거하기 키값이 들어왔을 때
				// 혹시나 클라이언트가 해킹되어 잘못된 패킷을 보내서 방이 깨질 수 있음
				// ID는 클라이언트에서 임의 수정을 할 수 없으니 ID로 체크
				if (frontKey.id == -1) {
					// !@#$%^ 방 깨지는거 
					RemoveRoom();
				}
				return;
			}


		}
		else // 뽑기 실패하면 다시 시도
			continue;
	}

	space.ShootLaunchedMissile();


	// 플레이어 움직이기
	for (auto &i : connection_player) {
		if (i.is_Use()) {
			//		if (2번발사모드) {
		//
			//		}
			i.Move();
			if (i.is_Translation() == OBJECT_MOVE ||
				i.is_Translation() == OBJECT_FALL) {
				// 플레이어간 충돌체크
				for (auto &j : connection_player) {
					if (!j.is_Use()) continue;
					if (j.getPlayerID() == i.getPlayerID()) continue;
					FLOAT3 f = j.getPos() - i.getPos();
					if (((f.getX()*f.getX()) + (f.getY()*f.getY()) + (f.getZ()*f.getZ())) < PLAYER_COL) {
						using namespace DirectX;
						float angle = XMScalarACos(FLOAT3::dot(FLOAT3(i.getSpeedX(), 0, i.getSpeedZ()), f));
						if (angle < XMConvertToRadians(90.f / CGameData::getInstance().getConstData()->getOPERATION_FPS()))
							i.reverseMove();
					}
				}

				// 맵 충돌체크

				bool c = space.getMap()->CheckCollision(i.getPos().getX(), i.getPos().getZ());
				if (c) {
					i.reverseMove();
				}


				for (auto &j : vObject) {
					if (!j.is_Use()) continue;
					if (j.Collision(i.getPos()))
						i.reverseMove();
				}
			}
		}
	}

	// 적 움직이기
	space.clearSector();
	for (auto &i : vEnemy) {
		if (i.is_Use()) {
			i.Act(space);
			if (!i.is_Use()) continue;
			space.pushSector(i.getID());
			space.pushEnemyToNearList(i.getID());
		}
	}

	// 보스 움직이기
	// 보스도 공간분할의 대상이 되어야 할 것인가?
	for (auto &i : vBoss) {
		if (i.is_Use()) {
			i.Act(space);
		}
	}

	// 미사일 움직이기
	for (auto &i : vMissile) {
		if (i.is_Use()) {
			i.Act(space);

			// 구 충돌체크
			if (i.isTranslate() != OBJECT_CREATE) {
				switch (i.getOwnerType()) {
				case OT_MASTERLESS:
					break;
				case OT_PLAYER:
				{
					// 충돌체크 to Enemy
					if (space.CollisionChecktoEnemy(i.getID())) {
						qEvent.push(EVENTKEY(EKT_KILL, i.getOwnerID()));
					}

					// 충돌체크 to Player (No Damage)
					for (auto &j : connection_player) {
						if (!j.is_Use()) continue;
						if (i.getOwnerID() == j.getPlayerID()) continue;
						if (i.getType()->getBO(0)->checkCollision(i.getPos(), i.getDirection(),
							j.getPos(), j.getLook(), j.getpMecha()->getBO(0))) {
							//			FLOAT3 f = i.getPos() - (j.getPos() + FLOAT3(0, PLAYER_OFFSET_Y, 0));
							//			if (f.getLength_noSqrt() < PLAYER_COL) {
							i.Crash();
							break;
						}
					}

					// 충돌체크 to 베이스 캠프
					for (auto &j : vObject) {
						if (j.Collision(i.getPos()))
							i.Crash();
					}

					// 충돌체크 to 보스
					int part = space.CollisionChecktoBoss(i.getID());
					if (vBoss[0].getState() == ES_SKILL)
						if (part == 0) {
							qEvent.push(EVENTKEY(EKT_BOSS_HIT, 0));
							vBoss[0].DeAccel();
							if (vBoss[0].Hit(i.getDamage()))
								;
						}


					break;
				}
				case OT_ENEMY:
					for (auto &j : connection_player) {
						if (!j.is_Use()) continue;
						FLOAT3 f = i.getPos() - (j.getPos() + FLOAT3(0, PLAYER_OFFSET_Y, 0));
						if (f.getLength_noSqrt() < PLAYER_COL) {
							if (j.is_Translation() != OBJECT_DEAD) {
								if (j.Hit(i.getDamage())) {
									qEvent.push(EVENTKEY(EKT_PLAYER_DOWN, j.getPlayerID()));
									bool gameover = true;
									for (auto &j : connection_player) {
										if (!j.is_Use()) continue;
										if (j.is_Translation() != OBJECT_DEAD)
											gameover = false;
									}
									if (gameover) {
										qEvent.push(EVENTKEY(EKT_DEFEATE, 0));
										sceneID = 12;
										lineID = 0;
										linechecker.state = CHECK_DEFAULT;
									}
								}
							}
							else
								qEvent.push(EVENTKEY(EKT_PLAYER_HIT, j.getPlayerID(), j.getHP()));
							i.Crash();
							break;
						}
					}
					for (auto &j : vObject) {
						if (j.Collision(i.getPos())) {
							if (j.Hit(i.getDamage())) {
								// qEvent.push(EVENTKEY(EKT_OBJECT_DOWN, j.getID()));
								j.setHP(140);
							}
							else
								qEvent.push(EVENTKEY(EKT_OBJECT_HIT, j.getID(), j.getHP()));
							i.Crash();
						}
						break;
					}
					break;
				case OT_BOSS:
					break;
				}
			}
			space.pushMissileToNearList(i.getID());
		}
	}

	// 오브젝트 움직이기
	for (auto &i : vObject) {
		if (i.is_Use()) {
			i.Act();
		}
	}

	space.RelocateViewList();
	// 플레이어의 시야에 들어온 오브젝트만 모으기


	/* 프레임 출력 */
#ifdef _DEBUG
	debug_FPScount++;
	debug_cur = GetTickCount();
	if (debug_pre + 1000 <= debug_cur) {
		//		cout<<
		//		connection_player.begin()->getPos().getX() << ", " <<
		//			connection_player.begin()->getPos().getY() << ", " <<
		//			connection_player.begin()->getPos().getZ() << endl;
	//	cout << "FPS : " << debug_FPScount << "   time : " << debug_cur << endl;
		debug_pre = debug_cur;
		debug_FPScount = 0;
}
#endif
}

// 남은 시간을 축적합니다. 남은 시간이 1 이상으로 쌓이면 해당 값을 반환합니다.
unsigned int CRoom::stackSpareTime()
{
	spare_time += CGameData::getInstance().getConstData()->getOPERATION_FPS_SPARE_TIME();
	unsigned int t = static_cast<int>(spare_time);
	spare_time -= t;
	return t;
}
