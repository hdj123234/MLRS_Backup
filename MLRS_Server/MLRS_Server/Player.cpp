#include "stdafx.h"
#include "Player.h"
#include "GameData\GameData.h"

CPlayer::CPlayer()
{
	is_use = false;
	objectType = OBJECT_PLAYER;
	id = -1;
	mecha_id = 0;
	is_ready = false;

	hp = 0;
	state = 0;
	pos.getX() = pos.getY() = pos.getZ() = 0;
	pos.getX() = 900;
	look_x = look_y = look_z = 0;
	speed_x = speed_y = speed_z = 0;
	ani_state = 0;
	ani_count = 0;
	select_weapon = 1;
	fire_weapon_type = 0;

	is_translation = OBJECT_NOSHIFT;
	move_state = 0;

	is_aim = false;
	cool_weapon1 = cool_weapon2 = cool_weapon3 = cool_weapon4 = 0;
	debug_gcount = 0;
}

CPlayer::CPlayer(const CPlayer &t)
{
	is_use = t.is_use;
	objectType = OBJECT_PLAYER;
	id = t.id;
	mecha_id = t.mecha_id;
	is_ready = t.is_ready;

	hp = t.hp;
	state = t.state;
	pos.getX() = t.pos.getX(); pos.getY() = t.pos.getY(); pos.getZ() = t.pos.getZ();
	pos.getX() = t.pos.getX();
	look_x = t.look_x; look_y = t.look_y; look_z = t.look_z;
	speed_x = t.speed_x; speed_y = t.speed_y; speed_z = t.speed_z;
	ani_state = t.ani_state;
	ani_count = t.ani_count;
	select_weapon = t.select_weapon;
	fire_weapon_type = t.fire_weapon_type;

	is_translation = OBJECT_NOSHIFT;
	move_state = 0;

	is_aim = t.is_aim;
	cool_weapon1 = t.cool_weapon1;
	cool_weapon2 = t.cool_weapon2;
	cool_weapon3 = t.cool_weapon3;
	cool_weapon4 = t.cool_weapon4;

	pMecha = t.pMecha;
}

CPlayer::CPlayer(int _id)
{
	is_use = false;
	objectType = OBJECT_PLAYER;
	id = _id;
	mecha_id = 0;
	is_ready = false;

	hp = 0;
	state = 0;
	pos.getX() = pos.getY() = pos.getZ() = 0;
	pos.getX() = 900;
	look_x = look_y = look_z = 0;
	speed_x = speed_y = speed_z = 0;
	ani_state = 0;
	ani_count = 0;
	select_weapon = 1;
	fire_weapon_type = 0;

	is_translation = OBJECT_NOSHIFT;
	move_state = 0;
	is_aim = false;
	cool_weapon1 = cool_weapon2 = cool_weapon3 = cool_weapon4 = 0;
}

CPlayer::~CPlayer()
{
}

void CPlayer::operator=(const CPlayer &t)
{
	id = t.id;
	mecha_id = t.mecha_id;
	hp = t.hp;
	state = t.state;
	pos.getX() = t.pos.getX(); pos.getY() = t.pos.getY(); pos.getZ() = t.pos.getZ();
	pos.getX() = t.pos.getX();
	look_x = t.look_x; look_y = t.look_y; look_z = t.look_z;
	speed_x = t.speed_x; speed_y = t.speed_y; speed_z = t.speed_z;
	ani_state = t.ani_state;
	ani_count = t.ani_count;
	select_weapon = t.select_weapon;
	fire_weapon_type = t.fire_weapon_type;

	is_translation = t.is_translation;
	move_state = 0;

	is_aim = t.is_aim;
	cool_weapon1 = t.cool_weapon1;
	cool_weapon2 = t.cool_weapon2;
	cool_weapon3 = t.cool_weapon3;
	cool_weapon4 = t.cool_weapon4;
}




void CPlayer::ActivatePlayer(int p_id, int _index_id, float _x, float _z)
{
	id = p_id;
	index_id = _index_id;
	mecha_id = 0;
	is_ready = false;
	is_translation = OBJECT_NOSHIFT;
	pMecha = CGameData::getInstance().getPlayerMecha(mecha_id);

	hp = pMecha->getHP();
	boost_guage = pMecha->getBoostGuage();
	state = 0;

	move_state = 0;
	boost_speed = pMecha->getBoostSpeed();
	boost_guage = pMecha->getBoostGuage();
	boost_cool = 0;


	// 젠되는 장소는 아직 임시로 설정
//	srand((unsigned)time(NULL));
	pos.getX() = _x;
	pos.getZ() = _z;
	pos.getY() = CGameData::getInstance().getMap(0)->getHeight(pos.getX(), pos.getZ());
	look_x = 0;
	look_y = 0;
	look_z = 1;
	is_use = true;
}

void CPlayer::RemovePlayer()
{
	is_use = false;
	id = -1;
	index_id = 0;
	mecha_id = 0;
	is_ready = false;

	hp = 0;
	state = 0;
	pos.getX() = pos.getY() = pos.getZ() = 0;
	pos.getX() = 900;
	look_x = look_y = look_z = 0;
	speed_x = speed_y = speed_z = 0;
	ani_state = 0;
	ani_count = 0;
	select_weapon = 1;
	fire_weapon_type = 0;

	pMecha = nullptr;

	move_state = 0;
	is_aim = false;
	cool_weapon1 = cool_weapon2 = cool_weapon3 = cool_weapon4 = 0;

	is_translation = OBJECT_NOSHIFT;
}


void CPlayer::healHP(float _hp)
{
	hp += _hp;
	if (hp > pMecha->getHP())
		hp = pMecha->getHP();
}

void CPlayer::ChangeMecha(int _mechaid)
{

}

// -2 : 바뀌지 않음. >= 0 : 바뀐 무기 번호. 결과 반환 후 bool change = false 로 전환
int CPlayer::isWeaponChange()
{
	if (is_weapon_change == true) {
		is_weapon_change = false;
		return select_weapon;
	}
	return -2;
}

bool CPlayer::canShoot(int _w)
{
	// 이것저것 쏠 수 있는 상태인지 확인해봐야 된다....
	// 쏘면 미사일 쿨도 돌려야 한다...
//	if (select_weapon == _w)
	if (is_translation != OBJECT_DEAD)
	{
		DWORD now = GetTickCount();
		DWORD cool = 0;
		switch (_w) {
		case 1:
			cool = now - cool_weapon1;
			break;
		case 2:
			cool = now - cool_weapon2;
			break;
		case 3:
			cool = now - cool_weapon3;
			break;
		case 4:
			cool = now - cool_weapon4;
			break;
		default:
			return false;
		}
		if (cool > CGameData::getInstance().getMissile(pMecha->getWeaponType(_w))->getCoolTime()) {
			cool = now;
			switch (_w) {
			case 1: cool_weapon1 = cool; break;
			case 2: cool_weapon2 = cool; break;
			case 3: cool_weapon3 = cool; break;
			case 4: cool_weapon4 = cool; break;
			default:
				return false;
			}
			fire_weapon_type = _w;
			return true;
		}
	}
	return false;
}

void CPlayer::Move()
{
	if (is_translation == OBJECT_DEAD)
		return;

	if (boost_guage < pMecha->getBoostGuage())
		boost_guage++;
	if (speed_x != 0 || speed_z != 0) // || speed_y != 0)
		is_translation = OBJECT_MOVE;
	else if (speed_y != 0)
		is_translation = OBJECT_FALL;
	else if (is_translation == OBJECT_ROTATE) {
		is_translation = OBJECT_MOVE;
		return;
	}
	else {
		if (is_translation == OBJECT_MOVE) {
			is_translation = OBJECT_STOP;
			move_state &= ~PLAYER_MOVE;
		}
		else if (is_translation == OBJECT_FALL) {
			is_translation = OBJECT_STOP;
			move_state &= ~PLAYER_MOVE;
		}
		else
			is_translation = OBJECT_NOSHIFT;
		return;
	}
	Boost();
	speed_y += CGameData::getInstance().getConstData()->getGRAVITY_FPS();
	pos += FLOAT3(speed_x, speed_y, speed_z);
	float heightY = CGameData::getInstance().getMap(0)->getHeight(pos.getX(), pos.getZ());
	if (pos.getY() <= heightY) {
		pos.setY(heightY);
		speed_y = 0;
		move_state &= ~PLAYER_JUMP;
	}
	move_state |= PLAYER_MOVE;
	// cout << "pos : " << pos.getX() << ", " << pos.getY() << ", " << pos.getZ() << endl;
}

void CPlayer::reverseMove()
{
	is_translation = OBJECT_MOVE;
	pos.getX() -= speed_x;
	pos.getZ() -= speed_z;
	pos.getY() = CGameData::getInstance().getMap(0)->getHeight(pos.getX(), pos.getZ());
}

void CPlayer::PlayerTranslate(float _x, float _y, float _z)
{
	pos.getX() += _x;
	//	y += _y;
	pos.getZ() += _z;
}

void CPlayer::RelocateViewList()
{
	for (auto &i : view_list) {			// 뷰 리스트에 있는 오브젝트가
		if (push_list.count(i) == 0) 	// 푸쉬 리스트에 없으면
			remove_list.push_back(i);	// 리무브 리트스로 옮긴다.
		else							// 푸쉬 리스트에 있으면
			push_list.erase(i);			// 푸쉬 리스트에서 제거한다.
	}	// 결과적으로 푸쉬리스트에는 새로 추가된 오브젝트만 남는다.

	for (auto &i : remove_list) {
		view_list.erase(i);				// 뷰리스트에서 리무브 리스트로 간 애들을 지워준다.
	}
	//	for(auto &i : push_list)
}

void CPlayer::setLookandSpeed(float _x, float _z)
{
	// x, z 가 노말라이즈 된 값인지 체크해봐야 할까
	speed_x = _x*pMecha->getSpeed();
	speed_z = _z*pMecha->getSpeed();
	look_x = _x;
	look_z = _z;
}

void CPlayer::setLook(float _x, float _y, float _z)
{
	look_x = _x;
	look_y = _y;
	look_z = _z;
}
void CPlayer::shiftPos(FLOAT3 _pos)
{
	pos = _pos;
	is_translation = OBJECT_MOVE;
}

void CPlayer::setSpeed(float _x, float _z)
{
	speed_x = _x*pMecha->getSpeed();
	speed_z = _z*pMecha->getSpeed();
}

void CPlayer::transRotate()
{
	is_translation = OBJECT_ROTATE;
}

void CPlayer::Jump()
{
	// !@#$%^ 점프 할 수 있는 상황인지 체크해봐야 한다
	// 피격중이나 점프중에 점프를 하게되면 이상하겠지?
	// if()
	float heightY = CGameData::getInstance().getMap(0)->getHeight(pos.getX(), pos.getZ());
	if (pos.getY() <= heightY) {
		speed_y = pMecha->getJump();
		move_state |= PLAYER_JUMP;
	}

}

bool CPlayer::Hit(float _damage)
{
	hp -= _damage;
	if (hp <= 0) {
		hp = 0;
		is_translation = OBJECT_DEAD;
		//		RemovePlayer();
		return true;
	}
	return false;
}

void CPlayer::Boost()
{
	FLOAT3 t = FLOAT3(speed_x, 0, speed_z).Normalize();;
	if (t.getX() != t.getX() || t.getZ() != t.getZ())
		return;
	if (move_state & PLAYER_BOOST) {
		// 부스터를 켰으면 부스터 게이지가 줄어야지
		if (boost_guage <= boost_cool) {
			move_state &= ~PLAYER_BOOST;
			boost_cool = 90;
			setSpeed(t.getX(), t.getZ());
			return;
		}
		boost_cool = 0;
		move_state |= PLAYER_BOOST;
		boost_guage -= 3;

		speed_x = t.getX() * pMecha->getBoostSpeed();
		speed_z = t.getZ() * pMecha->getBoostSpeed();

		// 에어 부스터!?
		// 어떤 방식으로 할까
		if (move_state | PLAYER_JUMP)
			if (speed_y < 0)
				speed_y = -CGameData::getInstance().getConstData()->getGRAVITY_FPS();

		// speed_y =+ (float)0.16;
	}
	else
		setSpeed(t.getX(), t.getZ());

	if (speed_x != speed_x)
		cout << "뻑남" << endl;
}


