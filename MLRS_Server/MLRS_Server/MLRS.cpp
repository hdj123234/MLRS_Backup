#pragma once
#include "stdafx.h"
#include "CIOCPServer.h"
#include "MiniDump.h"
using namespace std;

PLAYER players[100];
#include "Timer.h"

int main()
{
	//_wsetlocale(LC_ALL, L"korean");

	CMiniDump::Begin();

	CIOCPServer iocpServer;
	if (iocpServer.NetworkInit() == -1)
		return 0;
	else
		cout << "IOCP Server 초기화 성공" << endl;
	// vector <thread *> worker_threads;
	iocpServer.Listen();


	CMiniDump::End();
	Sleep(1000);
	return 0;
}