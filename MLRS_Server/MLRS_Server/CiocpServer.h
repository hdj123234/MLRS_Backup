#pragma once
#pragma comment(lib, "ws2_32")
#ifndef CIOCPSERVER_H
#define CIOCPSERVER_H

#include <concurrent_vector.h>
#include "stdafx.h"
#include "protocol.h"
#include "GameData\Terrain.h"
#include "Room.h"
#include "Timer.h"
#include "GameData\GameData.h"
using namespace std;
using namespace concurrency;


#define TYPE_RECV 0
#define TYPE_SEND 1
#define TYPE_ROOM_OPERATION 2
#define TYPE_ROOM_TIMER		3
struct OVERAPPED_EX {
	WSAOVERLAPPED overapped;
	int type;
	WSABUF wsabuf;
	char IOCPbuf[BUFSIZE];
	char PacketBuf[MAX_PACKET_SIZE];
	unsigned int prev_received;
	unsigned int curr_packet_size;
	OVERAPPED_EX() {};
	OVERAPPED_EX(const OVERAPPED_EX& t) {
		overapped = t.overapped;
		type = t.type;
		wsabuf = t.wsabuf;
		memcpy(IOCPbuf, t.IOCPbuf, sizeof(IOCPbuf));
		memcpy(PacketBuf, t.PacketBuf, sizeof(PacketBuf));
		prev_received = t.prev_received;
		curr_packet_size = t.curr_packet_size;
	}
};

struct PLAYER {
	SOCKET sock;
	bool is_use;
	int in_room;
	int x;
	int y;
	char nick[MAX_NICK_SIZE + 1];
	OVERAPPED_EX my_overapped;

	PLAYER() {
		is_use = false;
		in_room = -2;
		x = 0;
		y = 0;
	}
};




class CIOCPServer
{
	WSADATA wsadata;
	HANDLE hIOCP;
	SYSTEM_INFO si;
	CTimer mytimer;
	//vector <thread *> worker_threads;

	// 아래 두개는 절대로 .erase() 해서는 안됨
//	vector<PLAYER> cpList;	// 접속한 플레이어들을 저장하는 벡터
//	vector<CRoom> roomList;	// 방 리스트를 저장하는 백터
	//concurrent_vector<PLAYER> cpList;
	//concurrent_vector<CRoom> roomList;

	vector<PLAYER> cpList;
	vector<CRoom> roomList;
	int iLastLogout;		// 마지막으로 나간 사람이 갖고있던 ID

	static CIOCPServer* pthis;

	//////////////////

	int number;

public:
	CIOCPServer();
	~CIOCPServer();
	int NetworkInit();
	void Listen();

	void SendPacket(int id, void *packet);	// 클라이언트로 패킷 전송
	void SendMultiplePacket(int id, int packet_size, void *packet);
	void SendBuffer(int _roomid, int &_iter, int _max, BYTE *_packetZip);
	void SendBufferToPlayer(int _roomid, int _playerid, int &_iter, int _max, BYTE *_packetZip);
	void ProcessPacket(char *packet, int id); // 받은 패킷 처리
	void PlayerInit(int id);	// 플레이어 초기화

	static DWORD WINAPI WorkerThread(LPVOID arg);
};

#endif // !CIOCPSERVER_H