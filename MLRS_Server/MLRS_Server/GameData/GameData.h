#pragma once
#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <iostream>
#include <vector>
#include "Terrain.h"
#include "PlayerMecha.h"
#include "EnemyMecha.h"
#include "BossMecha.h"
#include "MissileType.h"
#include "AI\ObjectAI.h"
#include "GamePlot.h"
#include "ConstData.h"

#include "AI\AI_Zephyros.h"
#include "AI\AI_Combat.h"
#include "AI\AI_GoldenbatRider.h"
#include "AI\AI_TurtleSlug.h"
#include "AI\AI_Delta.h"
#include "AI\AI_MeleeRange_Missile.h"
#include "AI\AI_MidRange_Missile.h"
#include "AI\AI_Lockon_Missile.h"
#include "AI\AI_EM1.h"
#include "AI\AI_Ejavelin_Test.h"
#include "AI\AI_AutoLockon_MLRS_test.h"
#include "AI\AI_EM_Delta.h"

#define RESERVE_MAP_NUMBER 2
#define RESERVE_PLAYER_TYPE_NUMBER 4
#define RESERVE_ENEMY_TYPE_NUMBER 6

class CGameData
{
	vector<CTerrain> vMap;
	vector<CPlayerMecha*> vPMecha;
	vector<CEnemyMecha*> vEMecha;
	vector<CBossMecha*> vBMecha;
	vector<CMissileType*> vMissile;
	vector<CObjectAI*> vAI;
	vector<CGamePlot> vPlot;

	CGameData();
	CGameData(const CGameData &);

	CConstData constdata;
	
public:
	~CGameData();
	static CGameData& getInstance()
	{
		static CGameData instance;
		return instance;
	}

	CTerrain* getMap(int id) { return &vMap[id]; };
	CPlayerMecha* getPlayerMecha(int id) { return vPMecha[id]; };
	CEnemyMecha* getEnemyMecha(int id) { return vEMecha[id]; };
	CBossMecha* getBossMecha(int id) { return vBMecha[id]; };
	
	CObjectAI* getAI(int num) { return vAI[num]; };
	CMissileType* getMissile(int _type) { return vMissile[_type]; };
	CGamePlot* getPlot(int _plotid) { return &vPlot[_plotid]; }

	CConstData* getConstData() { 
		return &constdata;
	}
};

#endif // !GAMEDATA_H