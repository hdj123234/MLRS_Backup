#include "stdafx.h"
#include "ObjectTypeData.h"

#include <fstream>
#include <iostream>

CObjectTypeData::CObjectTypeData()
{
}


CObjectTypeData::~CObjectTypeData()
{
	for (auto &i : pBoMap)
		delete i.second;
}

CObjectTypeData::CObjectTypeData(int _id, string _name, string _bo_filename)
{
	id = _id;
	name = _name;
	readBO(_bo_filename);
}

bool CObjectTypeData::checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CObjectTypeData * potherOTD)
{

	return false;
}

void CObjectTypeData::readBO(string _filename)
{
	ifstream bof;
	bof.open("Data\\BoundingObject\\" + _filename, ios::binary);
	if (!bof.is_open()) {
		std::cout << "데이터 초기화 파일을 찾을 수 없습니다. (" << _filename << ")" << std::endl;
		return;
	}
	string ts;
	while (1) {
		ts.clear();
		bof >> ts;
		if (ts.empty())
			break;
		CBoundingObject_Multiple* pmbo = nullptr;
		int frame = stoi(ts);
		bof >> ts;
		int bo_num = stoi(ts);

		if (bo_num > 1)
			pmbo = new CBoundingObject_Multiple();

		CBoundingObject* pbo;
		for (int i = 0; i < bo_num; ++i)
		{
			string name;
			bof >> name;			
			bof >> ts;
			Type_BO bo_type = static_cast<Type_BO>(stoi(ts));
			switch (bo_type)
			{
			case Type_BO::Point:
			{
				bof >> ts; float center_x = stof(ts);
				bof >> ts; float center_y = stof(ts);
				bof >> ts; float center_z = stof(ts);
				pbo = new CBoundingPoint(name, DirectX::XMFLOAT3(center_x, center_y, center_z));
			}	break;
			case Type_BO::Sphere:
			{
				bof >> ts; float center_x = stof(ts);
				bof >> ts; float center_y = stof(ts);
				bof >> ts; float center_z = stof(ts);
				bof >> ts; float radius = stof(ts);
				pbo = new CBoundingSphere(name, DirectX::XMFLOAT3(center_x, center_y, center_z), radius);
			}	break;
			case Type_BO::Box:
			{
				bof >> ts; float center_x = stof(ts);
				bof >> ts; float center_y = stof(ts);
				bof >> ts; float center_z = stof(ts);
				bof >> ts; float extents_x = stof(ts);
				bof >> ts; float extents_y = stof(ts);
				bof >> ts; float extents_z = stof(ts);
				bof >> ts; float orientation_x = stof(ts);
				bof >> ts; float orientation_y = stof(ts);
				bof >> ts; float orientation_z = stof(ts);
				bof >> ts; float orientation_w = stof(ts);
				pbo = new CBoundingBox(name, DirectX::XMFLOAT3(center_x, center_y, center_z),
					DirectX::XMFLOAT3(extents_x, extents_y, extents_z),
					DirectX::XMFLOAT4(orientation_x, orientation_y, orientation_z, orientation_w));
			}	break;
			default:
				cout << "BO 파일에서 문제가 발생했습니다. (" << _filename << ")" << endl;
				return;
			}
			if (bo_num > 1)
				pmbo->pushBO(pbo);
		}

		if (bo_num > 1)
			pBoMap.insert(make_pair(frame, pmbo));
		else
			pBoMap.insert(make_pair(frame, pbo));
	}
}