#include "stdafx.h"
#include "MissileType.h"


CMissileType::CMissileType()
{
}


CMissileType::~CMissileType()
{
}


CMissileType::CMissileType(int _id, string _name, int _type, int _missile_ai, float _accel,	float _max_speed,
	int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename)
	:CObjectTypeData(_id, _name, _bo_filename)
{
	type = _type;
	missile_ai = _missile_ai;
	accel = _accel;
	max_speed = _max_speed;
	lock_on = _lock_on;
	range_fps = _range_fps;
	damage = _damage;
	cooltime = _cooltime;
	max_angle = _max_angle;
}

CMeleeRange_Missile::~CMeleeRange_Missile()
{
}

CMeleeRange_Missile::CMeleeRange_Missile(int _id, string _name, int _type, int _missile_ai, float _accel,
	float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename)
	: CMissileType(_id, _name, _type, _missile_ai, _accel, _max_speed, _lock_on, _range_fps, _damage, _cooltime, _max_angle, _bo_filename)
{
}

CMidRange_Missile::CMidRange_Missile(int _id, string _name, int _type, int _missile_ai, float _accel,
	float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename)
	: CMissileType(_id, _name, _type, _missile_ai, _accel, _max_speed, _lock_on, _range_fps, _damage, _cooltime, _max_angle, _bo_filename)
{
}

CLockon_Missile::CLockon_Missile(int _id, string _name, int _type, int _missile_ai, float _accel,
	float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename)
	: CMissileType(_id, _name, _type, _missile_ai, _accel, _max_speed, _lock_on, _range_fps, _damage, _cooltime, _max_angle, _bo_filename)
{
}

CEnemyMissile::CEnemyMissile(int _id, string _name, int _type, int _missile_ai, float _accel, float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename)
	: CMissileType(_id, _name, _type, _missile_ai, _accel, _max_speed, _lock_on, _range_fps, _damage, _cooltime, _max_angle, _bo_filename)
{
}

CEnemyJavelin::CEnemyJavelin(int _id, string _name, int _type, int _missile_ai, float _accel, float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename)
	: CMissileType(_id, _name, _type, _missile_ai, _accel, _max_speed, _lock_on, _range_fps, _damage, _cooltime, _max_angle, _bo_filename)
{
}

CEnemyMissile_Delta::CEnemyMissile_Delta(int _id, string _name, int _type, int _missile_ai, float _accel, float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename)
	: CMissileType(_id, _name, _type, _missile_ai, _accel, _max_speed, _lock_on, _range_fps, _damage, _cooltime, _max_angle, _bo_filename)
{
}
