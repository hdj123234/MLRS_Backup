#pragma once
#include "GameScene.h"
#include <vector>
#include <string>
#include "..\KeyBuf.h"
using namespace std;
class CGamePlot
{
	int plotID;
	string plotName;
	vector<CGameScene> vscene;
public:
	CGamePlot();
	CGamePlot(int _plotid) : plotID(_plotid) {};
	~CGamePlot();

	void putScene(CGameScene s) { vscene.push_back(s); }
	void setName(string _name) { plotName = _name; }
	CGameScene* getScene(int sceneid) { return &vscene[sceneid]; }

	KEYBUF getSceneKey(int *_psceneid, int *_plineid);
};

