#include "stdafx.h"
#include "GameData.h"
#include "..\UserDefine.h"
#include <string>
using namespace std;



CGameData::CGameData()
{
	vMap.reserve(RESERVE_MAP_NUMBER);
	vPMecha.reserve(RESERVE_PLAYER_TYPE_NUMBER);
	vEMecha.reserve(RESERVE_ENEMY_TYPE_NUMBER);

	ifstream initfile;
	initfile.open("Data\\GameData.ini", ios::binary);
	if (!initfile.is_open()) {
		cout << "데이터 초기화 파일을 찾을 수 없습니다. (GameData.ini)" << endl;
		return;
	}
	string ts;
	while (1) {
		getline(initfile, ts, '\t');

		if (ts.empty())
			break;

		// 주석일 경우
		if (*ts.begin() == '/') {
			getline(initfile, ts, '\n');
			continue;
		}

		// 데이터일 경우
		else if (*ts.begin() == '#') {

			// 맵 데이터일 경우
			if (!strncmp(ts.c_str(), "#map", 4)) {
				getline(initfile, ts, '\t');
				int id = stoi(ts);
				getline(initfile, ts, '\t');
				string name = ts;
				getline(initfile, ts, '\t');
				string route_h = ts;
				getline(initfile, ts, '\t');
				string route_c = ts;
				getline(initfile, ts, '\t');
				float scale_x = stof(ts);
				getline(initfile, ts, '\t');
				float scale_y = stof(ts);
				getline(initfile, ts, '\t');
				float scale_z = stof(ts);
				getline(initfile, ts, '\t');
				int plotid = stoi(ts);
				vMap.push_back(CTerrain(id, name, route_h, route_c, scale_x, scale_y, scale_z, plotid));

				getline(initfile, ts, '\n');
			}

			// 플레이어 기체일 경우
			else if (!strncmp(ts.c_str(), "#player", 7)) {
				getline(initfile, ts, '\t');
				int id = stoi(ts);
				getline(initfile, ts, '\t');
				string name = ts;
				getline(initfile, ts, '\t');
				float hp = stof(ts);

				getline(initfile, ts, '\t');
				float speed = stof(ts) / getConstData()->getOPERATION_FPS();
				getline(initfile, ts, '\t');
				float max_speed = stof(ts);
				getline(initfile, ts, '\t');
				float boost_gauge = stof(ts);
				getline(initfile, ts, '\t');
				float jump = stof(ts) / getConstData()->getOPERATION_FPS();

				getline(initfile, ts, '\t');
				int weapon1 = stoi(ts);
				getline(initfile, ts, '\t');
				int weapon2 = stoi(ts);
				getline(initfile, ts, '\t');
				int weapon3 = stoi(ts);
				getline(initfile, ts, '\t');
				int weapon4 = stoi(ts);
				getline(initfile, ts, '\t');
				string bo_filename = ts;


				CPlayerMecha *cp;
				switch (id) {
				case PT_ABATTRE:
					cp = new CAbattre(id, name, hp, speed, max_speed, boost_gauge, jump,
						weapon1, weapon2, weapon3, weapon4, bo_filename);
					break;
				case PT_NIMBLEDOG:
					cp = new CNimbleDog(id, name, hp, speed, max_speed, boost_gauge, jump,
						weapon1, weapon2, weapon3, weapon4, bo_filename);
					break;
				case PT_SPERANZA:
					cp = new CSperanza(id, name, hp, speed, max_speed, boost_gauge, jump,
						weapon1, weapon2, weapon3, weapon4, bo_filename);
					break;
				default:
					cout << "PlayerMecha 불러오기에 실패했습니다. (기체명 : " << name << ")" <<
						endl << "타입이 존재하지 않습니다. (type : " << id << ")" << endl;
					break;
				}

				vPMecha.push_back(cp);



				//	vPMecha.push_back(new CPlayerMecha(id, name, hp, speed, max_speed, boost_gauge, jump, weapon1, weapon2, weapon3, weapon4, bo_filename));
				getline(initfile, ts, '\n');
			}

			// 적 기체일 경우
			else if (!strncmp(ts.c_str(), "#enemy", 6)) {
				getline(initfile, ts, '\t');
				int id = stoi(ts);
				getline(initfile, ts, '\t');
				string name = ts;
				getline(initfile, ts, '\t');
				int type = stoi(ts);

				getline(initfile, ts, '\t');
				float hp = stof(ts);
				getline(initfile, ts, '\t');
				float acceleration = stof(ts) / (getConstData()->getOPERATION_FPS()*getConstData()->getOPERATION_FPS());
				getline(initfile, ts, '\t');
				float max_speed = stof(ts) / getConstData()->getOPERATION_FPS();
				getline(initfile, ts, '\t');
				int ai = stoi(ts);
				getline(initfile, ts, '\t');
				float sight = stof(ts)*stof(ts);
				getline(initfile, ts, '\t');
				int missile_type = stoi(ts);
				getline(initfile, ts, '\t');
				float zen_posY = stof(ts);
				getline(initfile, ts, '\t');
				bool enableGravity = (stoi(ts) != 0);
				getline(initfile, ts, '\t');
				float offsetY = stof(ts);
				getline(initfile, ts, '\t');
				string bo_filename = ts;

				CEnemyMecha *ce;
				switch (id) {
				case ET_ZEPHYROS:
					ce = new CZephyros(id, name, type, hp, acceleration, max_speed, ai, sight,
						missile_type, zen_posY, enableGravity, offsetY, bo_filename);
					break;
				case ET_COMBAT:
					ce = new CCombat(id, name, type, hp, acceleration, max_speed, ai, sight,
						missile_type, zen_posY, enableGravity, offsetY, bo_filename);
					break;
				case ET_GoldenbatRIDER:
					ce = new CGoldenbatRider(id, name, type, hp, acceleration, max_speed, ai, sight,
						missile_type, zen_posY, enableGravity, offsetY, bo_filename);
					break;
				default:
					cout << "EnemyMecha 불러오기에 실패했습니다. (기체명 : " << name << ")" <<
						endl << "타입이 존재하지 않습니다. (type : " << type << ")" << endl;
					break;
				}

				vEMecha.push_back(ce);

				getline(initfile, ts, '\n');
			}

			// 보스 기체일 경우
			else if (!strncmp(ts.c_str(), "#boss", 5)) {
				getline(initfile, ts, '\t');
				int id = stoi(ts);
				getline(initfile, ts, '\t');
				string name = ts;
				getline(initfile, ts, '\t');
				int type = stoi(ts);

				getline(initfile, ts, '\t');
				float hp = stof(ts);
				getline(initfile, ts, '\t');
				float acceleration = stof(ts) / (getConstData()->getOPERATION_FPS()*getConstData()->getOPERATION_FPS());
				getline(initfile, ts, '\t');
				float max_speed = stof(ts) / getConstData()->getOPERATION_FPS();
				getline(initfile, ts, '\t');
				int ai = stoi(ts);
				getline(initfile, ts, '\t');
				float sight = stof(ts)*stof(ts);
				getline(initfile, ts, '\t');
				int missile_type = stoi(ts);
				getline(initfile, ts, '\t');
				float zen_posY = stof(ts);
				getline(initfile, ts, '\t');
				bool enableGravity = (stoi(ts) != 0);
				getline(initfile, ts, '\t');
				float offsetY = stof(ts);
				getline(initfile, ts, '\t');
				string bo_filename = ts;

				CBossMecha *cb;
				switch (id) {
				case BT_DELTA:
					cb = new CBoss_Delta(id, name, type, hp, acceleration, max_speed, ai, sight,
						missile_type, zen_posY, enableGravity, offsetY, bo_filename);
					break;
				default:
					cout << "BossMecha 불러오기에 실패했습니다. (기체명 : " << name << ")" <<
						endl << "타입이 존재하지 않습니다. (type : " << type << ")" << endl;
					break;
				}

				vBMecha.push_back(cb);

				getline(initfile, ts, '\n');
			}

			// 미사일일 경우
			else if (!strncmp(ts.c_str(), "#missile", 8)) {
				getline(initfile, ts, '\t');
				int id = stoi(ts);
				getline(initfile, ts, '\t');
				string name = ts;
				getline(initfile, ts, '\t');
				int type = stoi(ts);
				getline(initfile, ts, '\t');
				int missile_ai = stoi(ts);

				getline(initfile, ts, '\t');
				float acceleration = stof(ts) / (getConstData()->getOPERATION_FPS()*getConstData()->getOPERATION_FPS());
				getline(initfile, ts, '\t');
				float max_speed = stof(ts) / getConstData()->getOPERATION_FPS();
				getline(initfile, ts, '\t');
				int lookon = stoi(ts);
				getline(initfile, ts, '\t');
				int max_reach = stoi(ts);
				getline(initfile, ts, '\t');
				float damage = stof(ts);
				getline(initfile, ts, '\t');
				// 이거 float로 받을까 int로 받을까???
				int cooltime = stoi(ts);
				getline(initfile, ts, '\t');
				float max_rotate_angle = DirectX::XMConvertToRadians(stof(ts) / getConstData()->getOPERATION_FPS());
				getline(initfile, ts, '\t');
				string bo_filename = ts;

				CMissileType *cm;
				switch (type) {
				case MeleeRange_Missile: {
					cm = new CMeleeRange_Missile(id, name, type, missile_ai, acceleration,
						max_speed, lookon, max_reach, damage, cooltime, max_rotate_angle, bo_filename);
					break;
				}
				case MidRange_Missile: {
					cm = new CMidRange_Missile(id, name, type, missile_ai, acceleration,
						max_speed, lookon, max_reach, damage, cooltime, max_rotate_angle, bo_filename);
					break;
				}
				case Lockon_Missile: {
					cm = new CLockon_Missile(id, name, type, missile_ai, acceleration,
						max_speed, lookon, max_reach, damage, cooltime, max_rotate_angle, bo_filename);
					break;
				}
				case EnemyMissile: {
					cm = new CEnemyMissile(id, name, type, missile_ai, acceleration,
						max_speed, lookon, max_reach, damage, cooltime, max_rotate_angle, bo_filename);
					break;
				}
				case EnemyMissile2: {
					cm = new CEnemyMissile2(id, name, type, missile_ai, acceleration,
						max_speed, lookon, max_reach, damage, cooltime, max_rotate_angle, bo_filename);
					break;
				}
				case EnemyJavelin: {
					cm = new CEnemyJavelin(id, name, type, missile_ai, acceleration,
						max_speed, lookon, max_reach, damage, cooltime, max_rotate_angle, bo_filename);
					break;
				}
				case EnemyMissile_Delta: {
					cm = new CEnemyMissile_Delta(id, name, type, missile_ai, acceleration,
						max_speed, lookon, max_reach, damage, cooltime, max_rotate_angle, bo_filename);
					break;
				}

				default:
					cout << "Missile 불러오기에 실패했습니다. (미사일명 : " << name << ")" <<
						endl << "타입이 존재하지 않습니다. (type : " << type << ")" << endl;
					break;
				}
				vMissile.push_back(cm);

				getline(initfile, ts, '\n');
			}

			// AI일 경우
			else if (!strncmp(ts.c_str(), "#ai", 3)) {
				getline(initfile, ts, '\t');
				int id = stoi(ts);
				getline(initfile, ts, '\t');
				string name = ts;

				CObjectAI *ai;
				switch (id) {
				case AI_Zephyros: {
					ai = new CAI_Zephyros();
					break;
				}
				case AI_Combat: {
					ai = new CAI_Combat();
					break;
				}
				case AI_GoldenbatRider: {
					ai = new CAI_GoldenbatRider();
					break;
				}
				case AI_Delta: {
					ai = new CAI_Delta();
					break;
				}
				case AI_MeleeRange_Missile: {
					ai = new CAI_MeleeRange_Missile();
					break;
				}
				case AI_MidRange_Missile: {
					ai = new CAI_MidRange_Missile();
					break;
				}
				case AI_Lockon_Missile: {
					ai = new CAI_Lockon_Missile();
					break;
				}
				case AI_EM: {
					ai = new CAI_EM1();
					break;
				}
				case AI_Ejavelin_Test: {
					ai = new CAI_Ejavelin_Test();
					break;
				}
				case AI_AutoLockon_MLRS_test: {
					ai = new CAI_AutoLockon_MLRS_test();
					break;
				}
				case AI_EM_Delta: {
					ai = new CAI_EM_Delta();
					break;
				}
				default:
					cout << "AI 불러오기에 실패했습니다. (AI명 : " << name << ")" <<
						endl << "타입이 존재하지 않습니다. (type : " << id << ")" << endl;
					break;
				}
				vAI.push_back(ai);

				getline(initfile, ts, '\n');
			}

			// Plot일 경우
			else if (!strncmp(ts.c_str(), "#plot", 4)) {
				getline(initfile, ts, '\t');
				int plotid = stoi(ts);

				getline(initfile, ts, '\t');
				ifstream plotfile;
				plotfile.open(ts, ios::binary);
				if (!plotfile.is_open()) {
					cout << "플롯 파일을 찾을 수 없습니다. (" << ts << ")" << endl;
					return;
				}
				vPlot.push_back(CGamePlot(plotid));
				string scriptstring;

				int lastscene = 0;
				while (true) {
					getline(plotfile, scriptstring, ' ');

					if (scriptstring.empty())
						break;

					// 주석일 경우
					if (*scriptstring.begin() == '/') {
						getline(initfile, ts, '\n');
						continue;
					}

					if (!strncmp(scriptstring.c_str(), "/t", 1)) {
						getline(plotfile, scriptstring, '\t');
					}
					else if (!strncmp(scriptstring.c_str(), "#name", 5)) {
						getline(plotfile, scriptstring, '\r');
						vPlot[plotid].setName(scriptstring);
						getline(plotfile, scriptstring, '\n');
					}
					else if (!strncmp(scriptstring.c_str(), "scene", 5)) {
#ifdef _DEBUG
						cout << endl << lastscene << " : ";
#endif
						lastscene++;
						getline(plotfile, scriptstring, '\r');
						int sceneid = stoi(scriptstring);
						vPlot[plotid].putScene(CGameScene(sceneid));
						getline(plotfile, scriptstring, '\t');

						int sceneline = 0;
						while (true) {
							getline(plotfile, scriptstring, '(');
							// strlwr(const_cast<char*>(scriptstring.c_str()));
							_strlwr_s(const_cast<char*>(scriptstring.c_str()), 20);
							if (!strncmp(scriptstring.c_str(), "//", 2)) {
								getline(plotfile, scriptstring, '\t');
								continue;
							}
							else if (!strncmp(scriptstring.c_str(), "addsinglemob", 12)) {
								getline(plotfile, scriptstring, ',');
								int type = stoi(scriptstring);
								getline(plotfile, scriptstring, ',');
								float pos_x = stof(scriptstring);
								getline(plotfile, scriptstring, ')');
								float pos_z = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_addSingleMob(sceneline, type, pos_x, pos_z));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "addmultimob", 11)) {
								getline(plotfile, scriptstring, ',');
								int type = stoi(scriptstring);
								getline(plotfile, scriptstring, ',');
								int amount = stoi(scriptstring);
								getline(plotfile, scriptstring, ',');
								float pos_x = stof(scriptstring);
								getline(plotfile, scriptstring, ',');
								float pos_z = stof(scriptstring);
								getline(plotfile, scriptstring, ')');
								int rad = stoi(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_addMultiMob(sceneline, type, amount, pos_x, pos_z, rad));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "addbossmob", 10)) {
								getline(plotfile, scriptstring, ',');
								int type = stoi(scriptstring);
								getline(plotfile, scriptstring, ',');
								float pos_x = stof(scriptstring);
								getline(plotfile, scriptstring, ')');
								float pos_z = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_addBossMob(sceneline, type, pos_x, pos_z));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "setobjectpos", 12)) {
								getline(plotfile, scriptstring, ',');
								int type = stoi(scriptstring);
								getline(plotfile, scriptstring, ',');
								float pos_x = stof(scriptstring);
								getline(plotfile, scriptstring, ')');
								float pos_z = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_setObjectPos(sceneline, type, pos_x, pos_z));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "setobjectspeed", 14)) {
								getline(plotfile, scriptstring, ',');
								int id = stoi(scriptstring);
								getline(plotfile, scriptstring, ',');
								float speed_x = stof(scriptstring);
								getline(plotfile, scriptstring, ',');
								float speed_y = stof(scriptstring);
								getline(plotfile, scriptstring, ')');
								float speed_z = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_setObjectSpeed(sceneline, id, speed_x, speed_y, speed_z));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "setinitpos", 10)) {
								getline(plotfile, scriptstring, ',');
								float pos_x = stof(scriptstring);
								getline(plotfile, scriptstring, ')');
								float pos_z = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_setInitPos(sceneline, pos_x, pos_z));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "setplayerpos", 12)) {
								getline(plotfile, scriptstring, ',');
								float pos_x = stof(scriptstring);
								getline(plotfile, scriptstring, ')');
								float pos_z = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_setPlayerPos(sceneline, pos_x, pos_z));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "wait", 4)) {
								getline(plotfile, scriptstring, ')');
								int time = stoi(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_wait(sceneline, time));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "checkmob", 8)) {
								getline(plotfile, scriptstring, ')');
								int under = stoi(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_checkMob(sceneline, under));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "checkpos", 8)) {
								getline(plotfile, scriptstring, ',');
								float pos_x = stof(scriptstring);
								getline(plotfile, scriptstring, ',');
								float pos_z = stof(scriptstring);
								getline(plotfile, scriptstring, ')');
								float rad = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_checkPos(sceneline, pos_x, pos_z, rad));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "checkxpos", 9)) {
								getline(plotfile, scriptstring, ')');
								float pos_x = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_checkZPos(sceneline, pos_x));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "checkzpos", 9)) {
								getline(plotfile, scriptstring, ')');
								float pos_z = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_checkZPos(sceneline, pos_z));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "printscript", 11)) {
								getline(plotfile, scriptstring, ')');
								int string_id = stoi(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_printScript(sceneline, string_id));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "recoverhp", 9)) {
								getline(plotfile, scriptstring, ')');
								float heal = stof(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_recoverHP(sceneline, heal));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "settimer", 8)) {
								getline(plotfile, scriptstring, ',');
								int curScene = stoi(scriptstring);
								getline(plotfile, scriptstring, ',');
								int goScene = stoi(scriptstring);
								getline(plotfile, scriptstring, ')');
								int time = stoi(scriptstring);
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_setTimer(sceneline, curScene, goScene, time));
								getline(plotfile, scriptstring, '\t');
							}
							else if (!strncmp(scriptstring.c_str(), "repeatscene", 11)) {
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_repeatScene(sceneline));
								getline(plotfile, scriptstring, '\n');
								getline(plotfile, scriptstring, '\n');
								break;
							}
							else if (!strncmp(scriptstring.c_str(), "break", 5)) {
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_break(sceneline));
								getline(plotfile, scriptstring, '\n');
								getline(plotfile, scriptstring, '\n');
								break;
							}
							else if (!strncmp(scriptstring.c_str(), "win", 3)) {
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_Win(sceneline));
								getline(plotfile, scriptstring, '\n');
								getline(plotfile, scriptstring, '\n');
								break;
							}
							else if (!strncmp(scriptstring.c_str(), "defeat", 6)) {
								vPlot[plotid].getScene(sceneid)->putLine(new CSceneLine_Defeat(sceneline));
								getline(plotfile, scriptstring, '\n');
								getline(plotfile, scriptstring, '\n');
								break;
							}
							else
								getline(plotfile, scriptstring, '\n');
							++sceneline;
#ifdef _DEBUG
							cout << sceneline << " ";
#endif
						}
					}

					else if (!strncmp(scriptstring.c_str(), "#end", 4)) {
						vPlot[plotid].putScene(CGameScene(lastscene));
						vPlot[plotid].getScene(lastscene)->putLine(new CSceneLine_Win(0));
						break;
					}
				}
				plotfile.close();
			}
		}
	}

	initfile.close();

	cout << "GameData Load 완료" << endl;
}

CGameData::~CGameData()
{
	for (auto &i : vPMecha)
		delete i;
	for (auto i : vEMecha)
		delete i;
	for (auto &i : vBMecha)
		delete i;
	for (auto &i : vMissile)
		delete i;
	for (auto &i : vAI)
		delete i;
}
