#include "stdafx.h"
#include "PlayerMecha.h"


CPlayerMecha::CPlayerMecha()
{
}


CPlayerMecha::CPlayerMecha(int _id, string _name, float _hp, float _speed, float _boost_speed, float _boost_guage, float _jump, int _weapon1, int _weapon2, int _weapon3, int _weapon4, string _bo_filename)
	:CObjectTypeData(_id, _name, _bo_filename)
{
	hp = _hp;
	speed = _speed;
	boost_speed = _boost_speed;
	boost_guage = _boost_guage;
	jump = _jump;
	weapon1 = _weapon1;
	weapon2 = _weapon2;
	weapon3 = _weapon3;
	weapon4 = _weapon4;
}

CPlayerMecha::~CPlayerMecha()
{
}

const int CPlayerMecha::getWeaponType(int _t)
{
	switch (_t) {
	case 1: return weapon1;
	case 2: return weapon2;
	case 3: return weapon3;
	case 4: return weapon4;
	default:
		return 0;
	}
}

int CPlayerMecha::getPort_num(int _port_num)
{
	switch (_port_num) {
	case 2:
		return port2_num;
	case 3:
		return port3_num;
	default:
		return 0;
	}
}

vector<MISSILE_PORT>* CPlayerMecha::getPort(int _port_num)
{
	switch (_port_num) {
	case 2:
		return &port2;
	case 3:
		return &port3;
	default:
		return &port2;
	}
}

CAbattre::CAbattre(int _id, string _name, float _hp, float _speed, float _boost_speed, float _boost_guage, float _jump, int _weapon1, int _weapon2, int _weapon3, int _weapon4, string _bo_filename)
	:CPlayerMecha(_id, _name, _hp, _speed, _boost_speed, _boost_guage, _jump, _weapon1, _weapon2, _weapon3, _weapon4, _bo_filename)
{
	port2_num = 0;
	port2.push_back(MISSILE_PORT(FLOAT3(-1.f, 3.f, 0.f), FLOAT3(0.12f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(0.f, 3.f, 0.f), FLOAT3(0.1f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(1.f, 3.f, 0.f), FLOAT3(0.08f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(-1.f, 1.f, 0.f), FLOAT3(0.12f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(0.f, 1.f, 0.f), FLOAT3(0.1f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(1.f, 1.f, 0.f), FLOAT3(0.08f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(-1.f, -1.f, 0.f), FLOAT3(0.12f, 1.f, 1.f))); port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(0.f, -1.f, 0.f), FLOAT3(0.1f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(1.f, -1.f, 0.f), FLOAT3(0.08f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(-1.f, -3.f, 0.f), FLOAT3(0.12f, 1.f, 1.f))); port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(0.f, -3.f, 0.f), FLOAT3(0.1f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(1.f, -3.f, 0.f), FLOAT3(0.08f, 1.f, 1.f)));	port2_num++;

	port3_num = 0;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(-2, 6, -3), FLOAT3(-1, 1, 0))); port3_num++;
	port3.push_back(MISSILE_PORT(FLOAT3(2, 6, -3), FLOAT3(1, 1, 0))); port3_num++;
}

CNimbleDog::CNimbleDog(int _id, string _name, float _hp, float _speed, float _boost_speed, float _boost_guage, float _jump, int _weapon1, int _weapon2, int _weapon3, int _weapon4, string _bo_filename)
	: CPlayerMecha(_id, _name, _hp, _speed, _boost_speed, _boost_guage, _jump, _weapon1, _weapon2, _weapon3, _weapon4, _bo_filename)
{
}

CSperanza::CSperanza(int _id, string _name, float _hp, float _speed, float _boost_speed, float _boost_guage, float _jump, int _weapon1, int _weapon2, int _weapon3, int _weapon4, string _bo_filename)
	: CPlayerMecha(_id, _name, _hp, _speed, _boost_speed, _boost_guage, _jump, _weapon1, _weapon2, _weapon3, _weapon4, _bo_filename)
{
	port2_num = 0;
	port2.push_back(MISSILE_PORT(FLOAT3(-1.f, 3.f, 0.f), FLOAT3(0.12f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(0.f, 3.f, 0.f), FLOAT3(0.1f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(1.f, 3.f, 0.f), FLOAT3(0.08f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(-1.f, 1.f, 0.f), FLOAT3(0.12f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(0.f, 1.f, 0.f), FLOAT3(0.1f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(1.f, 1.f, 0.f), FLOAT3(0.08f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(-1.f, -1.f, 0.f), FLOAT3(0.12f, 1.f, 1.f))); port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(0.f, -1.f, 0.f), FLOAT3(0.1f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(1.f, -1.f, 0.f), FLOAT3(0.08f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(-1.f, -3.f, 0.f), FLOAT3(0.12f, 1.f, 1.f))); port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(0.f, -3.f, 0.f), FLOAT3(0.1f, 1.f, 1.f)));	port2_num++;
	port2.push_back(MISSILE_PORT(FLOAT3(1.f, -3.f, 0.f), FLOAT3(0.08f, 1.f, 1.f)));	port2_num++;
}
