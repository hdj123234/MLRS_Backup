#include "stdafx.h"
#include "BoundingObject.h"


CBoundingObject::CBoundingObject()
{
}


CBoundingObject::~CBoundingObject()
{
}
void CBoundingObject::getWorldPos(FLOAT3 *myPos, FLOAT3 *myDir, DirectX::BoundingSphere *pSphere)
{
	using namespace DirectX;
	if (pSphere->Center.x == 0.f && pSphere->Center.y == 0.f &&pSphere->Center.z == 0.f)
	{
		XMVECTOR vPos = XMLoadFloat3(&pSphere->Center);
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vLook = XMLoadFloat3(&myDir->data);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		if (XMVector3Equal(vRight, DirectX::XMVectorZero())) {
			if (myDir->getY() >= 0.f)
				vUp = XMVectorSet(0, 0, -1, 0);
			else
				vUp = XMVectorSet(0, 0, 1, 0);
			vRight = XMVector3Cross(vUp, vLook);
		}
		vUp = XMVector3Cross(vLook, vRight);

		XMMATRIX mRotation = XMMATRIX(vRight, vUp, vLook, XMVectorSet(0, 0, 0, 1));
		XMVECTOR vResult = DirectX::XMVector3TransformCoord(vPos, mRotation);
		vResult += XMLoadFloat3(&myPos->data);
		XMStoreFloat3(&pSphere->Center, vResult);
	}
	else {
		pSphere->Center.x += myPos->data.x;
		pSphere->Center.y += myPos->data.y;
		pSphere->Center.z += myPos->data.z;
	}

}
void CBoundingObject::getWorldPos(FLOAT3 * myPos, FLOAT3 * myDir, DirectX::BoundingOrientedBox * pBox)
{
	using namespace DirectX;
	XMVECTOR vPos = XMLoadFloat3(&pBox->Center);
	XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
	XMVECTOR vLook = XMLoadFloat3(&myDir->data);
	XMVECTOR vRight = XMVector3Cross(vUp, vLook);
	if (XMVector3Equal(vRight, DirectX::XMVectorZero())) {
		if (myDir->getY() >= 0.f)
			vUp = XMVectorSet(0, 0, -1, 0);
		else
			vUp = XMVectorSet(0, 0, 1, 0);
		vRight = XMVector3Cross(vUp, vLook);
	}
	vUp = XMVector3Cross(vLook, vRight);

	XMMATRIX mRotation = XMMATRIX(vRight, vUp, vLook, XMVectorSet(0, 0, 0, 1));
	XMVECTOR vResult = DirectX::XMVector3TransformCoord(vPos, mRotation);
	vResult += XMLoadFloat3(&myPos->data);
	XMStoreFloat3(&pBox->Center, vResult);

	XMVECTOR myDir_Quat = XMQuaternionRotationMatrix(mRotation);
	auto XMRotateResult = XMQuaternionMultiply(XMLoadFloat4(&pBox->Orientation), myDir_Quat);
	XMStoreFloat4(&pBox->Orientation, XMRotateResult);
}

bool CBoundingObject::checkCollision_BO(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 myPoint, FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingSphere otherSphere)
{
	using namespace DirectX;
	{
		XMVECTOR vPos = XMLoadFloat3(&myPoint.data);
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vLook = XMLoadFloat3(&myDir.data);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		if (XMVector3Equal(vRight, DirectX::XMVectorZero())) {
			if (myDir.getY() >= 0.f)
				vUp = XMVectorSet(0, 0, -1, 0);
			else
				vUp = XMVectorSet(0, 0, 1, 0);
			vRight = XMVector3Cross(vUp, vLook);
		}
		vUp = XMVector3Cross(vLook, vRight);

		XMMATRIX mRotation = XMMATRIX(vRight, vUp, vLook, XMVectorSet(0, 0, 0, 1));
		XMVECTOR vResult = DirectX::XMVector3TransformCoord(vPos, mRotation);
		vResult += XMLoadFloat3(&myPos.data);
		XMStoreFloat3(&myPoint.data, vResult);
	}
	{
		XMVECTOR vPos = XMLoadFloat3(&otherSphere.Center);
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vLook = XMLoadFloat3(&otherDir.data);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		if (XMVector3Equal(vRight, DirectX::XMVectorZero())) {
			if (otherDir.getY() >= 0.f)
				vUp = XMVectorSet(0, 0, -1, 0);
			else
				vUp = XMVectorSet(0, 0, 1, 0);
			vRight = XMVector3Cross(vUp, vLook);
		}
		vUp = XMVector3Cross(vLook, vRight);

		XMMATRIX mRotation = XMMATRIX(vRight, vUp, vLook, XMVectorSet(0, 0, 0, 1));
		XMVECTOR vResult = DirectX::XMVector3TransformCoord(vPos, mRotation);
		vResult += XMLoadFloat3(&otherPos.data);
		XMStoreFloat3(&otherSphere.Center, vResult);
	}
	float dist = FLOAT3(myPoint.data.x - otherSphere.Center.x, myPoint.data.y - otherSphere.Center.y, myPoint.data.z - otherSphere.Center.z).getLength_noSqrt();
	float rad = otherSphere.Radius*otherSphere.Radius;
	return rad > dist;
}

bool CBoundingObject::checkCollision_BO(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 myPoint, FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingOrientedBox otherBox)
{
	using namespace DirectX;
	{
		XMVECTOR vPos = XMLoadFloat3(&myPoint.data);
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vLook = XMLoadFloat3(&myDir.data);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		if (XMVector3Equal(vRight, DirectX::XMVectorZero())) {
			if (myDir.getY() >= 0.f)
				vUp = XMVectorSet(0, 0, -1, 0);
			else
				vUp = XMVectorSet(0, 0, 1, 0);
			vRight = XMVector3Cross(vUp, vLook);
		}
		vUp = XMVector3Cross(vLook, vRight);

		XMMATRIX mRotation = XMMATRIX(vRight, vUp, vLook, XMVectorSet(0, 0, 0, 1));
		XMVECTOR vResult = DirectX::XMVector3TransformCoord(vPos, mRotation);
		vResult += XMLoadFloat3(&myPos.data);
		XMStoreFloat3(&myPoint.data, vResult);
	}
	{
		XMVECTOR vPos = XMLoadFloat3(&otherBox.Center);
		XMVECTOR vUp = XMVectorSet(0, 1, 0, 0);
		XMVECTOR vLook = XMLoadFloat3(&otherDir.data);
		XMVECTOR vRight = XMVector3Cross(vUp, vLook);
		if (XMVector3Equal(vRight, DirectX::XMVectorZero())) {
			if (otherDir.getY() >= 0.f)
				vUp = XMVectorSet(0, 0, -1, 0);
			else
				vUp = XMVectorSet(0, 0, 1, 0);
			vRight = XMVector3Cross(vUp, vLook);
		}
		vUp = XMVector3Cross(vLook, vRight);

		XMMATRIX mRotation = XMMATRIX(vRight, vUp, vLook, XMVectorSet(0, 0, 0, 1));
		XMVECTOR vResult = DirectX::XMVector3TransformCoord(vPos, mRotation);
		vResult += XMLoadFloat3(&otherPos.data);
		XMStoreFloat3(&otherBox.Center, vResult);
	}
	return otherBox.Intersects(DirectX::BoundingSphere(myPos.data, 0.1f));
}

bool CBoundingObject::checkCollision_BO(
	FLOAT3 myPos, FLOAT3 myDir, DirectX::BoundingSphere mySphere,
	FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingSphere otherSphere)
{
	getWorldPos(&myPos, &myDir, &mySphere);
	getWorldPos(&otherPos, &otherDir, &otherSphere);
	return mySphere.Intersects(otherSphere);

	/* 삼각함수는 성능 문제있을거 같아서 일단 보류
	XMVECTOR v1 = XMLoadFloat3(&myDir.data);
	XMVECTOR v2 = XMVectorSet(0, 0, -1, 0);
	XMVECTOR v3 = XMLoadFloat3(&mySphere.Center);
	XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
	float angle = DirectX::XMScalarACos(FLOAT3::dot(myDir, FLOAT3(0, 0, -1)));

	if (XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()) )
	{
		mySphere.Center.x += myPos.data.x;
		mySphere.Center.y += myPos.data.y;
		mySphere.Center.z += myPos.data.z;
	}
	else {
		XMMATRIX()
		XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, angle);
		XMVECTOR vResult = DirectX::XMVector3TransformCoord(v3, mRotation);
		vResult += XMLoadFloat3(&myPos.data);
		XMStoreFloat3(&mySphere.Center, vResult);
	}
	*/
}

bool CBoundingObject::checkCollision_BO(FLOAT3 myPos, FLOAT3 myDir, DirectX::BoundingOrientedBox myBox, FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingOrientedBox otherBox)
{
	using namespace DirectX;
	getWorldPos(&myPos, &myDir, &myBox);
	getWorldPos(&otherPos, &otherDir, &otherBox);
	return myBox.Intersects(otherBox);
}

bool CBoundingObject::checkCollision_BO(FLOAT3 myPos, FLOAT3 myDir, DirectX::BoundingSphere mySphere, FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingOrientedBox otherBox)
{
	using namespace DirectX;
	getWorldPos(&myPos, &myDir, &mySphere);
	getWorldPos(&otherPos, &otherDir, &otherBox);
	return mySphere.Intersects(otherBox);
}

bool CBoundingObject::checkCollision_BO(FLOAT3 myPos, FLOAT3 myDir, DirectX::BoundingOrientedBox myBox, FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingSphere otherSphere)
{
	using namespace DirectX;
	getWorldPos(&myPos, &myDir, &myBox);
	getWorldPos(&otherPos, &otherDir, &otherSphere);
	return myBox.Intersects(otherSphere);
}


CBoundingPoint::CBoundingPoint(std::string _name, const DirectX::XMFLOAT3 & _center)
	: CBoundingObject(_name)
{
	center = _center;
}

bool CBoundingPoint::checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject * pbo)
{
	return false;
}


CBoundingSphere::CBoundingSphere(std::string _name, const DirectX::XMFLOAT3 &_center, float _radius)
	: CBoundingObject(_name)
{
	sphere.Center = _center;
	sphere.Radius = _radius;
}

bool CBoundingSphere::checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject * pbo)
{
	switch (pbo->getType())
	{
	case Type_BO::Sphere: {
		auto pbo_shpere = dynamic_cast<CBoundingSphere*>(pbo)->getSphere();
		return checkCollision_BO(myPos, myDir, sphere, otherPos, otherDir, *pbo_shpere);
	} break;
	case Type_BO::Box: {
		auto pbo_Box = dynamic_cast<CBoundingBox*>(pbo)->getBox();
		return checkCollision_BO(myPos, myDir, sphere, otherPos, otherDir, *pbo_Box);
		//실제 처리 혹은 함수
	} break;
	case Type_BO::Multiple: {
		auto pbo_Multiple = dynamic_cast<CBoundingObject_Multiple*>(pbo)->getList();
		for (auto &i : *pbo_Multiple)
		{
			switch (i->getType())
			{
			case Type_BO::Sphere: {
				auto pbo_shpere = dynamic_cast<CBoundingSphere*>(i)->getSphere();
				if (checkCollision_BO(myPos, myDir, sphere, otherPos, otherDir, *pbo_shpere))
					return true;
			} break;
			case Type_BO::Box: {
				auto pbo_Box = dynamic_cast<CBoundingBox*>(i)->getBox();
				if (checkCollision_BO(myPos, myDir, sphere, otherPos, otherDir, *pbo_Box))
					return true;
			} break;
			}
			break;
		}
	}
	}
	return false;
}

CBoundingBox::CBoundingBox()
{
}

CBoundingBox::CBoundingBox(std::string _name,
	const DirectX::XMFLOAT3 &_center,
	const DirectX::XMFLOAT3 &_extents,
	const DirectX::XMFLOAT4 &_orientation)
	: CBoundingObject(_name)
{
	box.Center = _center;
	box.Extents = _extents;
	box.Orientation = _orientation;
}

bool CBoundingBox::checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject * pbo)
{
	switch (pbo->getType())
	{
	case Type_BO::Sphere: {
		auto pbo_shpere = dynamic_cast<CBoundingSphere*>(pbo)->getSphere();
		return checkCollision_BO(myPos, myDir, box, otherPos, otherDir, *pbo_shpere);
	} break;
	case Type_BO::Box: {
		auto pbo_Box = dynamic_cast<CBoundingBox*>(pbo)->getBox();
		return checkCollision_BO(myPos, myDir, box, otherPos, otherDir, *pbo_Box);
		//실제 처리 혹은 함수
	} break;
	case Type_BO::Multiple: {
		auto pbo_Multiple = dynamic_cast<CBoundingObject_Multiple*>(pbo)->getList();
		for (auto &i : *pbo_Multiple)
		{
			switch (i->getType())
			{
			case Type_BO::Sphere: {
				auto pbo_shpere = dynamic_cast<CBoundingSphere*>(pbo)->getSphere();
				return checkCollision_BO(myPos, myDir, box, otherPos, otherDir, *pbo_shpere);
			} break;
			case Type_BO::Box: {
				auto pbo_Box = dynamic_cast<CBoundingBox*>(pbo)->getBox();
				return checkCollision_BO(myPos, myDir, box, otherPos, otherDir, *pbo_Box);
				//실제 처리 혹은 함수
			} break;
			}
			break;
		}
	}
	}
	return false;
}

CBoundingCapsule::CBoundingCapsule(std::string _name, const DirectX::XMFLOAT3 & _point1, const DirectX::XMFLOAT3 & _point2, const float _radius)
	: CBoundingObject(_name)
{
	capsule.point1 = _point1;
	capsule.point2 = _point2;
	capsule.radius = _radius;
}

bool CBoundingCapsule::checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject * pbo)
{
	return false;
}

CBoundingObject_Multiple::CBoundingObject_Multiple()
{
}

CBoundingObject_Multiple::~CBoundingObject_Multiple()
{
	for (auto &i : vBO)
		delete i;
}

bool CBoundingObject_Multiple::checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject * pbo)
{
	switch (pbo->getType())
	{
	case Type_BO::Sphere:
		//실제 처리 혹은 함수
		break;
	case Type_BO::Box:
		//실제 처리 혹은 함수
		break;
	case Type_BO::Multiple:
		break;
	}
	return false;
}

int CBoundingObject_Multiple::checkCollision_Where(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject * pbo)
{
	int index = 0;
	for (auto &i : vBO) {
		switch (i->getType())
		{
		case Type_BO::Point:
		{
			auto myBO = dynamic_cast<CBoundingPoint*>(i)->getPoint();
			switch (pbo->getType())
			{
			case Type_BO::Sphere: {
				auto pbo_shpere = dynamic_cast<CBoundingSphere*>(pbo)->getSphere();
				if (checkCollision_BO(myPos, myDir, *myBO, otherPos, otherDir, *pbo_shpere))
					return index;
			} break;
			case Type_BO::Box: {
				auto pbo_Box = dynamic_cast<CBoundingBox*>(pbo)->getBox();
				if (checkCollision_BO(myPos, myDir, *myBO, otherPos, otherDir, *pbo_Box))
					return index;
				//실제 처리 혹은 함수
			} break;
			}
			break;
		} break;
		case Type_BO::Sphere:
		{
			auto myBO = dynamic_cast<CBoundingSphere*>(i)->getSphere();
			switch (pbo->getType())
			{
			case Type_BO::Sphere: {
				auto pbo_shpere = dynamic_cast<CBoundingSphere*>(pbo)->getSphere();
				if (checkCollision_BO(myPos, myDir, *myBO, otherPos, otherDir, *pbo_shpere))
					return index;
			} break;
			case Type_BO::Box: {
				auto pbo_Box = dynamic_cast<CBoundingBox*>(pbo)->getBox();
				if (checkCollision_BO(myPos, myDir, *myBO, otherPos, otherDir, *pbo_Box))
					return index;
				//실제 처리 혹은 함수
			} break;
			}
			break;
		} break;
		case Type_BO::Box:
		{
			auto myBO = dynamic_cast<CBoundingBox*>(i)->getBox();
			switch (pbo->getType())
			{
			case Type_BO::Sphere: {
				auto pbo_shpere = dynamic_cast<CBoundingSphere*>(pbo)->getSphere();
				if (checkCollision_BO(myPos, myDir, *myBO, otherPos, otherDir, *pbo_shpere))
					return index;
			} break;
			case Type_BO::Box: {
				auto pbo_Box = dynamic_cast<CBoundingBox*>(pbo)->getBox();
				if (checkCollision_BO(myPos, myDir, *myBO, otherPos, otherDir, *pbo_Box))
					return index;
				//실제 처리 혹은 함수
			} break;
			}
			break;
		} break;

		}
		index++;
	}
	return -1;
}


