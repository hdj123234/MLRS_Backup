#pragma once
#include "..\stdafx.h"
#include "..\UserDefine.h"
using namespace std;

class CTerrain
{
	int id;
	string name;
	float scale_x;
	float scale_y;
	float scale_z;

	int plotid;

	unsigned int  nSize;

	vector<vector<float>> m_vvY;
	vector<vector<bool>> m_vvC;

public:
	CTerrain();
	CTerrain(string filename);
	CTerrain(int _id, string _name, string filename_height, string filename_collision,
		float _scale_x, float _scale_y, float _scale_z, int _plotid);
	~CTerrain();

	int getPlotID() { return plotid; }
	bool LoadHeightFromRAWFile(string filename);
	bool LoadCollisionFromRAWFile(string filename);
	bool CheckHeight(float x, float y, float z);
	bool CheckCollision(float x, float z);
	float getHeight(float x, float z);
};

