#pragma once
/*
template <typename T1>
class CDataType
{
	T1* data_type;
public:
	CDataType(T1* _ref) { type_int = _type_int; }
	T1* getData() { return data_type; }

	void setData(T1 _data) { *data_type = _data; }
};
*/

enum DATA_TYPE { TYPE_INT, TYPE_FLOAT };
class CDataType
{
	DATA_TYPE data_type;
	int* type_int;
	float* type_float;
public:
	CDataType(int* _type_int) { data_type = TYPE_INT; type_int = _type_int; }
	CDataType(float* _type_float) { data_type = TYPE_FLOAT; type_float = _type_float; }

	DATA_TYPE getType() { return data_type; }

	void setData(int _data) {
		if (data_type == TYPE_INT) *type_int = _data;
		else *type_int = static_cast<int>(_data);
	}
	void setData(float _data) {
		if (data_type == TYPE_FLOAT) *type_float = _data;
		else *type_float = static_cast<float>(_data);
	}
};


class CConstData
{
	int Port;

	int RoomMaxCreate; 				// 생성할 수 있는 방의 최대 개수
	int RoomMaxTitleLen;			// 방제 최대길이
	int MaxRoominLobby;				// 로비에서 한 페이지에 보이는 방의 개수

	float FPS;						// f. 서버의 연산 FPS
	int OperationFPSTime;			// s. gettickcount 함수를 위한 FPS의 정수 시간
	float OperationFPSSpareTime;	// s. FPS를 정수로 나누고 남은 실수값
	float Gravity;					// f. 중력
	float GravityFPS;				// s. FPS로 나눈 중력의 실제 적용값
	int MaxSizeEnemyNumber;			// f. 적의 최대 개수
	int MaxSizeMissileNumber;		// f. 미사일 최대 개수

	int SpacePartitionX;			// f. 섹터 분할 X축 개수
	int SpacePartitionZ;			// f. 섹터 분할 Z축 개수

	float ClientView;				// f. 클라이언트의 시야범위
	float ClientView_NoSQRT;		// s. 연산 최적화를 위한 시야범위의 제곱값

	float BaseCampHP;				// 베이스 캠프 체력

	void InitData();

public:
	CConstData();
	~CConstData();
	const int getPORT() { return Port;	}

	const int getROOM_MAX_CREAT() { return RoomMaxCreate; }
	const int getROOM_MAX_TITLE_LEN() { return RoomMaxTitleLen; }
	const int getLOBBY_MAX_SHOW_ROOM() { return MaxRoominLobby; }

	const float getOPERATION_FPS() { return FPS; }
	const int getOPERATION_FPS_TIME() { return OperationFPSTime; }
	const float getOPERATION_FPS_SPARE_TIME() { return OperationFPSSpareTime; }
	const float getGRAVITY_FPS() { return GravityFPS; }
	const int getMAXSIZE_ENEMY_NUMBER() { return MaxSizeEnemyNumber; }
	const int getMAXSIZE_MISSILE_NUMBER() { return MaxSizeMissileNumber; }

	//const float getMAP_SIZE_X() { return ; }
	//const float getMAP_SIZE_Z() { return ; }

	const int getSPACE_PARTITION_X() { return SpacePartitionX; }
	const int getSPACE_PARTITION_Z() { return SpacePartitionZ; }

	const float getCLIENT_VIEW() { return ClientView; }
	const float getCLIENT_VIEW_NOSQRT() { return ClientView_NoSQRT; }

	const float getBASECAMP_HP() { return BaseCampHP; }
};