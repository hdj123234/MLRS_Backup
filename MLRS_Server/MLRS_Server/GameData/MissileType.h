#pragma once
#include "ObjectTypeData.h"
using namespace std;

enum mName { NOTHING, 
	MeleeRange_Missile,
	MidRange_Missile, 
	Lockon_Missile,
	me3, 
	EnemyMissile, 
	EnemyMissile2, 
	EnemyMissile3,
	EnemyJavelin,
	EnemyMissile_Delta
};

class CMissileType : public CObjectTypeData
{
	int type;
	float accel;
	float max_speed;
	int lock_on;
	int range_fps;
	float damage;
	unsigned int cooltime;
	float max_angle;

	int missile_ai;

public:
	CMissileType();
	~CMissileType();
	CMissileType(int _id, string _name, int _type, int _missile_ai, float _accel,
		float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename);
	const int getID() { return id; };
	const int getAInumber() { return missile_ai; }
	const float getAcceleration() { return accel; };
	const float getMaxSpeed() { return max_speed; };
	const int getRange_fps() { return range_fps; }
	const float getDamage() { return damage; }
	const unsigned int getCoolTime() { return cooltime; }
	const float getMaxAngle() { return max_angle; }
};

class CMeleeRange_Missile : public CMissileType
{
public:
	CMeleeRange_Missile();
	~CMeleeRange_Missile();
	CMeleeRange_Missile(int _id, string _name, int _type, int _missile_ai, float _accel,
		float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename);
};

class CMidRange_Missile : public CMissileType
{
public:
	CMidRange_Missile();
	~CMidRange_Missile();
	CMidRange_Missile(int _id, string _name, int _type, int _missile_ai, float _accel,
		float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename);
};

class CLockon_Missile: public CMissileType
{
public:
	CLockon_Missile();
	~CLockon_Missile();
	CLockon_Missile(int _id, string _name, int _type, int _missile_ai, float _accel,
		float _max_speed, int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename);
};
	

class CEnemyMissile : public CMissileType
{
public:
	CEnemyMissile();
	~CEnemyMissile();
	CEnemyMissile(int _id, string _name, int _type, int _missile_ai, float _accel, float _max_speed,
		int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename);
};

class CEnemyMissile2 : public CMissileType
{
public:
	CEnemyMissile2();
	~CEnemyMissile2();
	CEnemyMissile2(int _id, string _name, int _type, int _missile_ai, float _accel, float _max_speed,
		int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename)
		: CMissileType(_id, _name, _type, _missile_ai, _accel, _max_speed, _lock_on, _range_fps, _damage, _cooltime, _max_angle, _bo_filename)
	{}
};

class CEnemyJavelin : public CMissileType
{
public:
	CEnemyJavelin();
	~CEnemyJavelin();
	CEnemyJavelin(int _id, string _name, int _type, int _missile_ai, float _accel, float _max_speed,
		int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename);
};

class CEnemyMissile_Delta : public CMissileType
{
public:
	CEnemyMissile_Delta();
	~CEnemyMissile_Delta();
	CEnemyMissile_Delta(int _id, string _name, int _type, int _missile_ai, float _accel, float _max_speed,
		int _lock_on, int _range_fps, float _damage, int _cooltime, float _max_angle, string _bo_filename);
};