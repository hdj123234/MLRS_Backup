#include "stdafx.h"
#include "GameScene.h"



CSceneLine::CSceneLine() : line(0)
{
}

CSceneLine::CSceneLine(int _line)  : line(_line)
{
}


CSceneLine::~CSceneLine()
{
}

KEYBUF CSceneLine_addSingleMob::makeLineKey(int *_psceneid, int *_plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_ADDSINGLEMOB;
	k.ikey1 = type;
	k.fkey1 = pos_x;
	k.fkey2 = pos_z;
	return k;
}

KEYBUF CSceneLine_addMultiMob::makeLineKey(int *_psceneid, int *_plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_ADDMULTIMOB;
	k.ikey1 = type;
	k.ikey2 = amount;
	k.fkey1 = pos_x;
	k.fkey2 = pos_z;
	k.ikey3 = rad;
	return k;
}

KEYBUF CSceneLine_addBossMob::makeLineKey(int *_psceneid, int *_plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_ADDBOSSMOB;
	k.ikey1 = type;
	k.fkey1 = pos_x;
	k.fkey2 = pos_z;
	return k;
}

KEYBUF CSceneLine_setObjectPos::makeLineKey(int *_psceneid, int *_plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_SETOBJECTPOS;
	k.ikey1 = id;
	k.fkey1 = pos_x;
	k.fkey2 = pos_z;
	return k;
}

KEYBUF CSceneLine_setObjectSpeed::makeLineKey(int *_psceneid, int *_plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_SETOBJECTSPEED;
	k.ikey1 = id;
	k.fkey1 = speed_x;
	k.fkey2 = speed_y;
	k.fkey3 = speed_z;
	return k;
}

KEYBUF CSceneLine_setInitPos::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_SETINITPOS;
	k.fkey1 = pos_x;
	k.fkey2 = pos_z;
	return k;
}

KEYBUF CSceneLine_setPlayerPos::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_SETPLAYERPOS;
	k.fkey1 = pos_x;
	k.fkey2 = pos_z;
	return k;
}

KEYBUF CSceneLine_wait::makeLineKey(int *_psceneid, int *_plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_WAIT;
	k.ikey1 = time;
	return k;
}

KEYBUF CSceneLine_checkMob::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_CHECKMOB;
	k.ikey1 = under;
	return k;
}

KEYBUF CSceneLine_checkPos::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_CHECKPOS;
	k.fkey1 = x;
	k.fkey2 = z;
	k.fkey3 = rad;
	return k;
}

KEYBUF CSceneLine_checkXPos::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_CHECKXPOS;
	k.fkey1 = x;
	return k;
}

KEYBUF CSceneLine_checkZPos::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_CHECKZPOS;
	k.fkey1 = z;
	return k;
}

KEYBUF CSceneLine_break::makeLineKey(int *_psceneid, int *_plineid)
{
	*_psceneid += 1;
	*_plineid = 0;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_BREAK;
#ifdef _DEBUG
	cout << "Scene : " << *_psceneid << endl;
#endif
	return k;
}

KEYBUF CSceneLine_Win::makeLineKey(int * _psceneid, int * _plineid)
{
	cout << "GamePlot : Win Game" << endl;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_WINGAME;
	return k;
}

KEYBUF CSceneLine_Defeat::makeLineKey(int * _psceneid, int * _plineid)
{
	cout << "GamePlot : Defeat Game" << endl;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_DEFEATGAME;
	return k;
}

KEYBUF CSceneLine_repeatScene::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid = 0;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_REPEATSCENE;
	return k;
}

KEYBUF CSceneLine_printScript::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_PRINTSCRIPT;
	k.ikey1 = string_id;
	return k;
}

KEYBUF CSceneLine_recoverHP::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_RECOVERHP;
	k.fkey1 = heal;
	return k;
}

KEYBUF CSceneLine_setTimer::makeLineKey(int * _psceneid, int * _plineid)
{
	*_plineid += 1;
	KEYBUF k;
	k.id = -1;
	k.type1 = KEYTYPE_SCENELINE;
	k.type2 = SceneLineID_SETTIMER;
	k.ikey1 = curScene;
	k.ikey2 = goScene;
	k.ikey3 = time;
	return k;
}
