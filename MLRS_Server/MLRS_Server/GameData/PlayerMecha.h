#pragma once
#include "ObjectTypeData.h"
#include <iostream>
#include <string>
using namespace std;

enum PLAYER_TYPE {
	PT_ABATTRE, PT_NIMBLEDOG, PT_SPERANZA
};

struct MISSILE_PORT
{
	FLOAT3 pos;
	FLOAT3 dir;
	MISSILE_PORT(FLOAT3 _pos, FLOAT3 _dir) {
		pos = _pos; dir = _dir;
	}
};

class CPlayerMecha : public CObjectTypeData
{
protected:
	float hp;
	float speed;
	float boost_speed;
	float boost_guage;

	float jump;

	int weapon1, weapon2, weapon3, weapon4;
	
	// 미사일 종류에 맞는 각종 정보들을 보유해야함
	// 기체마다 바운딩 박스의 크기도 다를테니 따로 가져야할테고
	// 충돌체크 정밀도는 아직 안정했지만 그거에 관한 정보도 있어야함

	int port2_num;
	vector<MISSILE_PORT> port2;
	int port3_num;
	vector<MISSILE_PORT> port3;

public:
	CPlayerMecha();
	CPlayerMecha(int _id) { id = _id;};
	CPlayerMecha(int _id, string _name, float _hp, float _speed,
		float _boost_speed, float _boost_guage, float _jump,
		int _weapon1, int _weapon2, int _weapon3, int _weapon4, string _bo_filename);
	~CPlayerMecha();

	void add() const { std::cout << id << "번 기체 : " << endl; }
	const float getHP() { return hp; }
	const float getSpeed() { return speed; };
	const float getBoostSpeed() { return boost_speed; }
	const float getBoostGuage() { return boost_guage; }
	const float getJump() { return jump; }
	const int getWeaponType(int _t);

	int getPort_num(int _port_num);
	vector<MISSILE_PORT>* getPort(int _port_num);
	
};

class CAbattre : public CPlayerMecha
{
public:
	CAbattre();
	CAbattre(int _id, string _name, float _hp, float _speed,
		float _boost_speed, float _boost_guage, float _jump,
		int _weapon1, int _weapon2, int _weapon3, int _weapon4, string _bo_filename);
};

class CNimbleDog : public CPlayerMecha
{
public:
	CNimbleDog();
	CNimbleDog(int _id, string _name, float _hp, float _speed,
		float _boost_speed, float _boost_guage, float _jump,
		int _weapon1, int _weapon2, int _weapon3, int _weapon4, string _bo_filename);
};

class CSperanza : public CPlayerMecha
{
public:
	CSperanza();
	CSperanza(int _id, string _name, float _hp, float _speed,
		float _boost_speed, float _boost_guage, float _jump,
		int _weapon1, int _weapon2, int _weapon3, int _weapon4, string _bo_filename);
};