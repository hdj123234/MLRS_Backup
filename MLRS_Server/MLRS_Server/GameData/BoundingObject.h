#pragma once
#include "../GameObject.h"
#include <DirectXCollision.h>
#include <vector>
#include <string>

enum class Type_BO :int {
	Point,
	Sphere,
	Box,
	Capsule,
	Multiple,
};

struct Capsule
{
	FLOAT3 point1, point2;
	float radius;
};

class CBoundingObject
{
protected:
	std::string name;
public:
	CBoundingObject();
	~CBoundingObject();
	CBoundingObject(std::string _name) { name = _name; }
	std::string getName() { return name; }
	virtual bool checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject* pbo) = 0;
	virtual const Type_BO getType() = 0;

	static void getWorldPos(FLOAT3 *myPos, FLOAT3 *myDir, DirectX::BoundingSphere *pSphere);
	static void getWorldPos(FLOAT3 *myPos, FLOAT3 *myDir, DirectX::BoundingOrientedBox *pBox);

	static bool checkCollision_BO(
		FLOAT3 myPos, FLOAT3 myDir, FLOAT3 myPoint,
		FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingSphere otherSphere);
	static bool checkCollision_BO(
		FLOAT3 myPos, FLOAT3 myDir, FLOAT3 myPoint,
		FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingOrientedBox otherBox);
	static bool checkCollision_BO(
		FLOAT3 myPos, FLOAT3 myDir, FLOAT3 myPoint,
		FLOAT3 otherPos, FLOAT3 otherDir, Capsule otherCapsule);

	static bool checkCollision_BO(
		FLOAT3 myPos, FLOAT3 myDir, DirectX::BoundingSphere mySphere,
		FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingSphere otherSphere);
	static bool checkCollision_BO(
		FLOAT3 myPos, FLOAT3 myDir, DirectX::BoundingOrientedBox myBox,
		FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingOrientedBox otherBox);
	static bool checkCollision_BO(
		FLOAT3 myPos, FLOAT3 myDir, DirectX::BoundingSphere mySphere,
		FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingOrientedBox otherBox);
	static bool checkCollision_BO(
		FLOAT3 myPos, FLOAT3 myDir, DirectX::BoundingOrientedBox myBox,
		FLOAT3 otherPos, FLOAT3 otherDir, DirectX::BoundingSphere otherSphere);
	//		FLOAT3 myPos, FLOAT3 myDir, FLOAT3 myCenter, float my_radius,
	//		FLOAT3 otherPos, FLOAT3 otherDir, FLOAT3 otherCenter, float other_radius);
};

class CBoundingPoint : public CBoundingObject
{
	FLOAT3 center;

public:
	CBoundingPoint();
	~CBoundingPoint();
	CBoundingPoint(std::string _name, const DirectX::XMFLOAT3 &_center);

	virtual bool checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject* pbo);
	virtual const Type_BO getType() { return Type_BO::Point; }
	FLOAT3* getPoint() { return &center; }
};

class CBoundingSphere : public CBoundingObject
{
	DirectX::BoundingSphere sphere;

public:
	CBoundingSphere();
	~CBoundingSphere();
	CBoundingSphere(std::string _name, const DirectX::XMFLOAT3 &_center, const float _radius);

	virtual bool checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject* pbo);
	virtual const Type_BO getType() { return Type_BO::Sphere; }
	DirectX::BoundingSphere* getSphere() { return &sphere; }
};

class CBoundingBox : public CBoundingObject
{
	DirectX::BoundingOrientedBox box;
public:
	CBoundingBox();
	~CBoundingBox();
	CBoundingBox(std::string _name, const DirectX::XMFLOAT3 &_center, const DirectX::XMFLOAT3 &_extents, const DirectX::XMFLOAT4 &_orientation);

	virtual bool checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject* pbo);
	virtual const Type_BO getType() { return Type_BO::Box; }
	DirectX::BoundingOrientedBox* getBox() { return &box; }
};

class CBoundingCapsule : public CBoundingObject
{
	Capsule capsule;
public:
	CBoundingCapsule();
	~CBoundingCapsule();
	CBoundingCapsule(std::string _name, const DirectX::XMFLOAT3 &_point1, const DirectX::XMFLOAT3 &_point2, const float _radius);

	virtual bool checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject* pbo);
	virtual const Type_BO getType() { return Type_BO::Capsule; }
	Capsule* getCapsule() { return &capsule; }
};

class CBoundingObject_Multiple : public CBoundingObject
{
	std::vector<CBoundingObject*> vBO;
public:
	CBoundingObject_Multiple();
	~CBoundingObject_Multiple();

	void pushBO(CBoundingObject* _pbo) { vBO.push_back(_pbo); }

	virtual bool checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject* pbo);
	int checkCollision_Where(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CBoundingObject* pbo);
	virtual const Type_BO getType() { return Type_BO::Multiple; }
	std::vector<CBoundingObject*>* getList() { return &vBO; }
};

