#include "stdafx.h"
#include "Terrain.h"

CTerrain::CTerrain()
{
	LoadHeightFromRAWFile("Data\test.raw");
}


CTerrain::~CTerrain()
{
}

CTerrain::CTerrain(string filename)
{
	LoadHeightFromRAWFile(filename);
}

CTerrain::CTerrain(int _id, string _name, string filename_height, string filename_collision,
	float _scale_x, float _scale_y, float _scale_z, int _plotid)
{
	id = _id;
	name = _name;
	scale_x = _scale_x;
	scale_y = _scale_y;
	scale_z = _scale_z;
	plotid = _plotid;
	LoadHeightFromRAWFile(filename_height);
	LoadCollisionFromRAWFile(filename_collision);
}


bool CTerrain::LoadHeightFromRAWFile(std::string filename)
{
	ifstream inFile(filename, ios::binary);
	if (inFile.fail()) {
		cout << "Map 파일을 열지 못했습니다. (";
		auto i = filename.begin();
		for (i += 5; i != filename.end(); ++i) {
			cout << *i;
		}
		cout << ")" << endl;
		return false;
	}

	cout << "Height Map 불러오는 중......   ";

	inFile.unsetf(ios::skipws);

	//파일 크기 계산
	inFile.seekg(0, ios::end);
	unsigned int  nSize_Full = static_cast<unsigned int>(inFile.tellg());
	inFile.seekg(0, ios::beg);

	nSize = static_cast<unsigned int>(sqrt(nSize_Full));

	m_vvY.resize(nSize);

	//파일 정보 복사
	unsigned char rawData = 0;
	auto iter = std::istreambuf_iterator<char>(inFile.rdbuf());
	unsigned int indexY;
	for (unsigned int i = 0; i < nSize; ++i)
	{
		indexY = nSize - i - 1;
		m_vvY[indexY].resize(nSize);

		auto &rTmp = m_vvY[indexY];
		for (unsigned int j = 0; j < nSize; ++j)
		{
			rawData = 0x00 | *iter;   //char를 unsigned char로 변환하여 대입
			rTmp[j] = 0 + rawData*scale_y;
			++iter;
		}
	}
	inFile.close();
	cout << "완료" << endl;
	return true;
}

bool CTerrain::LoadCollisionFromRAWFile(std::string filename)
{
	ifstream inFile(filename, ios::binary);
	if (inFile.fail()) {
		cout << "Map 파일을 열지 못했습니다. (";
		auto i = filename.begin();
		for (i += 5; i != filename.end(); ++i) {
			cout << *i;
		}
		cout << ")" << endl;
		return false;
	}

	cout << "Collision Map 불러오는 중...   ";

	inFile.unsetf(ios::skipws);

	//파일 크기 계산
	inFile.seekg(0, ios::end);
	unsigned int  nSize_Full = static_cast<unsigned int>(inFile.tellg());
	inFile.seekg(0, ios::beg);

	unsigned int nSize = static_cast<unsigned int>(sqrt(nSize_Full));

	m_vvC.resize(nSize);

	//파일 정보 복사
	unsigned char rawData = 0;
	auto iter = std::istreambuf_iterator<char>(inFile.rdbuf());
	unsigned int indexY;
	for (unsigned int i = 0; i < nSize; ++i)
	{
		indexY = nSize - i - 1;
		m_vvC[indexY].resize(nSize);

		auto &rTmp = m_vvC[indexY];
		for (unsigned int j = 0; j < nSize; ++j)
		{
			rawData = 0x00 | *iter;   //char를 unsigned char로 변환하여 대입
			rTmp[j] = static_cast<bool>(rawData) ^ 1;
			++iter;
		}
	}
	inFile.close();
	cout << "완료" << endl;
	return true;
}


bool CTerrain::CheckHeight(float x, float y, float z)
{
	// 하이트맵보다 높은 값은 바로 false 리턴
	if (0 + 255 * scale_y < y)
		return false;

	// 하이트맵 인덱스에 맞게 x, z 값 수정
	float idx = x / scale_x;
	float idz = z / scale_z;

	idx += (nSize - 1) / 2;
	idz *= -1; idz += (nSize - 1) / 2;

	// 맵 사이즈에서 벗어났으면 true 리턴
	if (idx < 0 || idx >= nSize || idz < 0 || idz >= nSize)	return true;


	// 가까운 값들로 height값 보간
	float tx = idx - int(idx);
	float tz = idz - int(idz);

	int ix = (int)idx;
	int iz = (int)idz;

	float lheight = m_vvY[iz][ix] + (m_vvY[iz + 1][ix] - m_vvY[iz][ix]) * tz;
	float rheight = m_vvY[iz][ix + 1] + (m_vvY[iz + 1][ix + 1] - m_vvY[iz][ix + 1]) * tz;

	float height = lheight + (rheight - lheight) * tx;


	if (height - y >= 0)
		return true;
	else
		return false;
}

float CTerrain::getHeight(float x, float z)
{
	// 하이트맵 인덱스에 맞게 x, z 값 수정
	float idx = x / scale_x;
	float idz = z / scale_z;

	idx += (nSize - 1) / 2;
	idz += (nSize - 1) / 2;

	// 맵 사이즈에서 벗어났으면 true 리턴
	if (idx < 0 || idx >= nSize - 1 || idz < 0 || idz >= nSize - 1)
		return 0;


	// 가까운 값들로 height값 보간
	float tx = idx - static_cast<int>(idx);
	float tz = idz - static_cast<int>(idz);

	int ix = static_cast<int>(idx);
	int iz = static_cast<int>(idz);
	if (ix < 0)
		return 0;

	float lheight = m_vvY[iz][ix] + (m_vvY[iz + 1][ix] - m_vvY[iz][ix]) * tz;
	float rheight = m_vvY[iz][ix + 1] + (m_vvY[iz + 1][ix + 1] - m_vvY[iz][ix + 1]) * tz;

	float height = lheight + (rheight - lheight) * tx;
	// cout << ix << ", " << iz << endl;

	return height;
}

bool CTerrain::CheckCollision(float x, float z)
{
	// 하이트맵 인덱스에 맞게 x, z 값 수정
	float idx = x / scale_x;
	float idz = z / scale_z;

	idx += (nSize - 1) / 2;
	//idz *= -1; 
	idz += (nSize - 1) / 2;

	// 맵 사이즈에서 벗어났으면 true 리턴
	if (idx < 0 || idx >= nSize || idz < 0 || idz >= nSize)	return true;


	// 가까운 값들로 height값 보간
	float tx = idx - int(idx);
	float tz = idz - int(idz);

	int ix = (int)idx;
	int iz = (int)idz;

		bool lcollision = m_vvC[iz][ix] + (m_vvC[iz + 1][ix] - m_vvC[iz][ix]) * tz;
		bool rcollision = m_vvC[iz][ix + 1] + (m_vvC[iz + 1][ix + 1] - m_vvC[iz][ix + 1]) * tz;

		bool collision = lcollision + (rcollision - lcollision) * tx;

	// bool collision = m_vvC[iz][ix];
	//cout << ix << ", " << iz << "   : "<<collision << endl;
	return collision;
}