#pragma once
#include "BoundingObject.h"
#include <string>
#include <map>
using namespace std;

class CObjectTypeData
{
protected:
	int id;
	string name;

	map<int, CBoundingObject*> pBoMap;

public:
	CObjectTypeData();
	~CObjectTypeData();
	CObjectTypeData(int _id, string _name, string _bo_filename);
	void readBO(string _filename);

	CBoundingObject* getBO(int _frame) { return pBoMap.find(_frame)->second; }
	bool checkCollision(FLOAT3 myPos, FLOAT3 myDir, FLOAT3 otherPos, FLOAT3 otherDir, CObjectTypeData* potherOTD);
};

