#include "stdafx.h"
#include "..\..\UserDefine.h"
#include "AI_Lockon_Missile.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"


void CAI_Lockon_Missile::Act(CGameSpace & space, CGameObject * _obj)
{
	if (_obj->getObjectType() != object_Type::OBJECT_MISSILE)
		return;
	CMissile* m = static_cast<CMissile*>(_obj);

	switch (m->getState()) {
	case MS_SPAWN: {
		if (m->getTarget() == nullptr) {
			m->setState(MS_HUNGRY);
			m->increaseActCount();
			break;
		}
		if (!m->getTarget()->is_Use()) {
			m->setState(MS_HUNGRY);
			m->increaseActCount();
			break;
		}
		if (m->getActCount() < 1) {
			m->setSpeed(static_cast<float>(rand() % 5) / 10);
		}
		if (m->getActCount() > 15) {
			if (m->getActCount() >= m->getType()->getRange_fps()) {
				m->resetActCount();
				m->Crash();
				return;
			}
			FLOAT3 dir_to_target;
			if (m->getTarget()->getObjectType() == object_Type::OBJECT_ENEMY)
				dir_to_target = m->getTarget()->getPos() - m->getPos();
			else if (m->getTarget()->getObjectType() == object_Type::OBJECT_BOSS) {
				auto targetType = static_cast<CEnemy_Boss*>(m->getTarget());
				FLOAT3 targetPos = targetType->getPos();
				FLOAT3 targetDir = targetType->getDirection();
				auto targetBO = *static_cast<CBoundingSphere*>(static_cast<CBoundingObject_Multiple*>(targetType->getType()->getBO(0))->getList()->at(0))->getSphere();
				CBoundingObject::getWorldPos(&targetPos, &targetDir, &targetBO);
				dir_to_target = FLOAT3(targetBO.Center) - m->getPos();
			}
			FLOAT3 mydir = m->getDirection();
			dir_to_target.Normalize();

			FLOAT3 result;
			float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
			if (angle < m->getType()->getMaxAngle())
				result = dir_to_target;
			else {
				using namespace DirectX;
				XMVECTOR v1 = XMLoadFloat3(&mydir.data);
				XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

				DirectX::XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				int timeout = 0;
				while (DirectX::XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
				{
					if (timeout > 10)
						break;
					// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산
					v2 *= 1000;
					vRotationAxis = DirectX::XMVector3Cross(v1, v2);
					timeout++;
				}
				DirectX::XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, m->getType()->getMaxAngle());
				DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
				DirectX::XMStoreFloat3(&result.data, vResult);

				/*
				DirectX::XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				if (!DirectX::XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
				{
				DirectX::XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, m->getType()->getMaxAngle());
				DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
				DirectX::XMStoreFloat3(&result.data, vResult);
				}
				else
				result = dir_to_target;
				*/
			}

			m->setDirection(result);
		}
		m->increaseActCount();
		break;
	}

				   // 이거 안써도 될듯싶다.
	case MS_BOOM: {
		m->removeMissile();
		return;

	case MS_HUNGRY: {
		m->increaseActCount();
		if (m->getActCount() >= m->getType()->getRange_fps()) {
			m->resetActCount();
			m->Crash();
			return;
		}
	}

	}

	default:
		m->increaseActCount();
		break;
	}
	m->Accel();
	m->Move();

	float by = space.getMap()->getHeight(m->getPos().getX(), m->getPos().getZ());
	if (m->getPos().getY() < by)
		m->Crash();



	/*
	#ifdef _DEBUG
	if (e->getID() == 0)
	cout << e->getID() << "번 에너미 움직여욧 : " <<
	e->getPos().getX() << ", " << e->getPos().getY() << ", " << e->getPos().getZ() << endl;
	#endif // DEBUG
	*/
}
