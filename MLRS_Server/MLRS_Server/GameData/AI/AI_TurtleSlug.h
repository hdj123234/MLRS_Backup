#pragma once
#include "ObjectAI.h"

class CGameObject;
class CEnemy;
class CGameSpace;

class CAI_TurtleSlug : public CObjectAI
{
	void Act(CGameSpace & space, CGameObject *_obj);
};
