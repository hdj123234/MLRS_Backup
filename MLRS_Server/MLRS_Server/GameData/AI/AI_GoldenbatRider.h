#pragma once
#include "ObjectAI.h"

class CGameObject;
class CEnemy;
class CGameSpace;

class CAI_GoldenbatRider : public CObjectAI
{
	float max_angleY;
public:
	CAI_GoldenbatRider();
	void Act(CGameSpace & space, CGameObject *_obj);
};
