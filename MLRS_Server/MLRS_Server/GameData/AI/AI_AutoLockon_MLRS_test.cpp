#include "stdafx.h"
#include "AI_AutoLockon_MLRS_test.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"


void CAI_AutoLockon_MLRS_test::Act(CGameSpace & space, CGameObject * _obj)
{
	if (_obj->getObjectType() != OBJECT_MISSILE)
		return;
	CMissile* m = static_cast<CMissile*>(_obj);

	switch (m->getState()) {
	case MS_SPAWN: {
		if (m->getActCount() > 80) {
			m->setState(MS_MOVE_TO_TARGET);
			m->resetActCount();
			break;
		}
		m->increaseActCount();
		break;
	}	
	case MS_THINK: {
		if (m->getActCount() > 20) {
			m->setState(MS_MOVE_TO_TARGET);
			m->resetActCount();
			break;
		}
		m->increaseActCount();
		break;
	}
	case MS_MOVE_TO_TARGET:
	{
		if (m->getActCount() >= m->getType()->getRange_fps()) {
			m->resetActCount();
			m->Crash();
			return;
		} 
		m->onGravity();
		m->increaseActCount();
		break;
	}
	case MS_BOOM: {
		m->removeMissile();
		return;
	}
	default:
		m->Accel();
		m->increaseActCount();
		break;
	}
	m->Accel();
	m->Move();
	float by = space.getMap()->getHeight(m->getPos().getX(), m->getPos().getZ());
	if (m->getPos().getY() < by)
		m->Crash();


}
