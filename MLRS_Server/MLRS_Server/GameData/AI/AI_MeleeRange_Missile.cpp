#include "stdafx.h"
#include "AI_MeleeRange_Missile.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"

void CAI_MeleeRange_Missile::Act(CGameSpace & space, CGameObject * _obj)
{
	if (_obj->getObjectType() != OBJECT_MISSILE)
		return;
	CMissile* m = static_cast<CMissile*>(_obj);

	switch (m->getState()) {
	case MS_SPAWN: {
		if (m->getActCount() >= m->getType()->getRange_fps()) {
			m->resetActCount();
			m->Crash();
			return;
		}
		m->increaseActCount();
		break;
	}
	case MS_BOOM: {
		m->removeMissile();
		return;
	}
	default:
		m->Accel();
		m->increaseActCount();
		break;
	}
	m->Accel();
	m->Move();

	float by = space.getMap()->getHeight(m->getPos().getX(), m->getPos().getZ());
	if (m->getPos().getY() < by)
		m->Crash();


}

