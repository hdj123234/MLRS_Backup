#include "stdafx.h"
#include "AI_Zephyros.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"

CAI_Zephyros::CAI_Zephyros()
{
	max_angleY = 60.f;
}

void CAI_Zephyros::Act(CGameSpace & space, CGameObject* _obj)
{
	//	FLOAT3 debug_performance_test;
	//	debug_performance_test.Normalize();

	// dynamic_cast 보다 static_cast 가 더 빠르다고 합니다....
	if (_obj->getObjectType() != OBJECT_ENEMY)
		return;
	CEnemy* e = static_cast<CEnemy*>(_obj);

	switch (e->getState()) {
	case ES_SPAWN:
		e->setDirection(FLOAT3(1, 0, 1));
		e->resetActCount();
		e->setState(ES_IDLE);
		break;

	case ES_IDLE:
		e->DeAccel();
		if (e->getActCount() >= 60) {
			e->resetActCount();
			e->setState(ES_THINK);
			break;
		}
		break;
	case ES_THINK:
		e->setTargetToPlayer(space.setTargetToPlayer(e->getPos(), e->getType()->getSight()));
		if (e->getTarget() == nullptr) {
			e->setTargetToPlayer(space.setTargetToPlayerExceptObject(e->getPos(), 999999999.f));
			break;
		}
		if (e->getSpeed() == 0) {
			e->setState(ES_TURN);
			e->resetActCount();
		}
		else {
			e->setState(ES_MOVE_TO_TARGET);
			e->resetActCount();
		}
		break;
	case ES_TURN:
	{
		if (e->getTarget() == nullptr) {
			e->setState(ES_THINK);
			e->resetActCount();
		}
		if (e->getActCount() == 1)
			e->addSpeed(0.01f);

		FLOAT3 dir_to_target = e->getTarget()->getPos() - e->getPos();
		dir_to_target *= FLOAT3(1, 0, 1);
		FLOAT3 mydir = e->getDirection();
		mydir *= FLOAT3(1, 0, 1);
		mydir.Normalize();
		dir_to_target.Normalize();

		FLOAT3 result;
		float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
		if (angle < DirectX::XMConvertToRadians(0.5)) {
			e->setSpeed(0.f);
			e->setState(ES_MOVE_TO_TARGET);
			e->resetActCount();
			break;
		}
		float max_angle = DirectX::XMConvertToRadians(max_angleY / CGameData::getInstance().getConstData()->getOPERATION_FPS());
		if (angle < max_angle)
			result = dir_to_target;
		else {
			using namespace DirectX;
			XMVECTOR v1 = XMLoadFloat3(&mydir.data);
			XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

			XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			int timeout = 0;
			while (XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
			{
				if (timeout > 10) {
					vRotationAxis = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
					break;
				}
				// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산
				v2 *= 7;
				vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				timeout++;
			}
			XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, max_angle);
			XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
			XMStoreFloat3(&result.data, vResult);
		}
		e->setDirection(result);
		break;
	}

	case ES_MOVE_TO_TARGET:
	{
		if (e->getTarget() == nullptr) {
			e->setState(ES_THINK);
			e->resetActCount();
		}

		FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
		direction.getY() += 50;		// 이거 임시임!
		if (direction.getLength() <= 350) {	// 이거도 임시임! 근데 어떻게 값을 정할지...
			e->setState(ES_READY_TO_ATTACK);
			e->resetActCount();
			break;
		}
		direction.setY(0);
		direction.Normalize();
		e->setDirection(direction);
		e->Accel();
//		if (e->getActCount() >= 120) {
//			e->setState(ES_THINK);
//			e->resetActCount();
//		}
		break;
	}

	case ES_READY_TO_ATTACK:
	{
		e->DeAccel();
		if (e->getSpeed() <= 0) {
			e->setState(ES_ATTACK);
			e->resetActCount();
			break;
		}

		FLOAT3 dir_to_target = e->getTarget()->getPos() - e->getPos();
		dir_to_target *= FLOAT3(1, 0, 1);
		FLOAT3 mydir = e->getDirection();
		mydir *= FLOAT3(1, 0, 1);
		mydir.Normalize();
		dir_to_target.Normalize();

		FLOAT3 result;
		float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
		if (angle < DirectX::XMConvertToRadians(0.5)) {
			break;
		}
		float max_angle = DirectX::XMConvertToRadians(max_angleY / CGameData::getInstance().getConstData()->getOPERATION_FPS());
		if (angle < max_angle)
			result = dir_to_target;
		else {
			using namespace DirectX;
			XMVECTOR v1 = XMLoadFloat3(&mydir.data);
			XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

			XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			int timeout = 0;
			while (XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
			{
				if (timeout > 10) {
					vRotationAxis = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
					break;
				}
				// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산
				v2 *= 7;
				vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				timeout++;
			}
			XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, max_angle);
			XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
			XMStoreFloat3(&result.data, vResult);
		}
		e->setDirection(result);

		break;
	}

	case ES_ATTACK:
	{
		if (e->getActCount() == 1) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();

			/*
			FLOAT3 dir2;
			dir2 = e->getPoint();
			FLOAT3 dir3 = dir2.getRotateY(direction);
			cout << "쐈습니다! 총구초기값 : " << e->getPoint().getX() << ", " << e->getPoint().getY() << ", " << e->getPoint().getZ() << endl<<
			"회전한 총구값 : " << dir3.getX() << ", " <<	dir3.getY() << ", " << dir3.getZ() << endl;

			*/
			for (auto &i : (*e->getPoint())) {
				space.ShootMissile(i.getRotateY(direction) +
					e->getPos(), e->getDirection(), e->getID(), e->getMissileType(), OT_ENEMY, e->getTarget());
			}
		}
		else if (e->getActCount() >= 120) {
			e->setState(ES_THINK);
			e->resetActCount();
			break;
		}
		break;
	}
	case ES_HIT:
	{
		break;
	}
	case ES_HUNGRY:
	{
		// 임시 상태
		if (e->getActCount() == 1) {
			FLOAT3 d = FLOAT3(static_cast<float>(rand() % 10000 - 5000),
				0,
				static_cast<float>(rand() % 10000 - 5000));
			d.Normalize();
			e->setDirection(d.getX(), 0, d.getZ());
		}

		if (e->getActCount() <= 60) {
			e->Accel();
		}
		else if (e->getActCount() > 60) {
			e->DeAccel();
			if (e->getSpeed() == 0) {
				e->setState(ES_IDLE);
				e->resetActCount();
			}
		}
		break;
	}
	default:
		break;
	}
	e->increaseActCount();

	/*
	if (e->getType()->enableGravity()) {
		e->addGravity(CGameData::getInstance().getConstData()->getGRAVITY_FPS());
	}
	else */ {
		float by = space.getMap()->getHeight(e->getPos().getX(), e->getPos().getZ());
		if (e->getPos().getY() < by + 50)
			e->setGravity(0.5);
		//	e->setDirectionY(0.1f);
		else if (e->getPos().getY() > by + 120)
			e->setGravity(-0.5);
		else
			e->setGravity(0);
		//	e->setDirectionY(-0.1f);
	}
	e->Move();

	/*
	#ifdef _DEBUG
	if (e->getID() == 0)
	cout << e->getID() << "번 에너미 움직여욧 : " <<
	e->getPos().getX() << ", " << e->getPos().getY() << ", " << e->getPos().getZ() << endl;
	#endif // DEBUG
	*/
}
