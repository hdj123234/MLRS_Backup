#include "stdafx.h"
#include "AI_GoldenbatRider.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"

CAI_GoldenbatRider::CAI_GoldenbatRider()
{
	max_angleY = 60.f;
}

void CAI_GoldenbatRider::Act(CGameSpace & space, CGameObject* _obj)
{
	//	FLOAT3 debug_performance_test;
	//	debug_performance_test.Normalize();

	// dynamic_cast 보다 static_cast 가 더 빠르다고 합니다....
	if (_obj->getObjectType() != OBJECT_ENEMY)
		return;
	CEnemy* e = static_cast<CEnemy*>(_obj);

	switch (e->getState()) {
	case ES_SPAWN:
		e->setSpeed(9999);
		e->Accel();
		e->resetActCount();
		e->dirNormalize();
		e->setState(ES_IDLE);
		break;

	case ES_IDLE:
		if (e->getActCount() >= 60) {
			e->resetActCount();
			e->setState(ES_THINK);
			break;
		}
		break;
	case ES_THINK:
		e->setTargetToPlayer(space.setTargetToObject());
		e->setState(ES_MOVE_TO_TARGET);
		break;
	case ES_MOVE_TO_TARGET:
	{
		if (e->getTarget() == nullptr) {
			e->resetActCount();
			e->setState(ES_THINK);
		}

		FLOAT3 dir_to_target = e->getTarget()->getPos() - e->getPos();
		dir_to_target *= FLOAT3(1, 0, 1);
		FLOAT3 Length = dir_to_target;
		FLOAT3 mydir = e->getDirection();
		mydir *= FLOAT3(1, 0, 1);
		mydir.Normalize();
		dir_to_target.Normalize();

		FLOAT3 result;
		float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));


		if (Length.getLength_noSqrt() <= 90000) {	// 이거도 임시임! 근데 어떻게 값을 정할지...
			if (angle < DirectX::XMConvertToRadians(3)) {
				e->setState(ES_ATTACK);
				e->resetActCount();
				break;
			}
			else {
				e->setState(ES_HUNGRY);
				e->resetActCount();
				break;
			}
		}


		float max_angle = DirectX::XMConvertToRadians(max_angleY / CGameData::getInstance().getConstData()->getOPERATION_FPS());

		if (angle < max_angle)
			result = dir_to_target;
		else {
			using namespace DirectX;
			XMVECTOR v1 = XMLoadFloat3(&mydir.data);
			XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

			XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			int timeout = 0;
			while (XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
			{
				if (timeout > 10) {
					vRotationAxis = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
					break;
				}
				// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산
				v2 *= 7;
				vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				timeout++;
			}
			XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, max_angle);
			XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
			XMStoreFloat3(&result.data, vResult);
		}

		e->setDirection(result);
		e->Accel();
		if (e->getActCount() >= 180) {
			e->setState(ES_THINK);
			e->resetActCount();
		}
		break;
	}

	case ES_READY_TO_ATTACK:
	{
		if (e->getActCount() > 50) {
			e->setState(ES_ATTACK);
			e->resetActCount();
			break;
		}

		FLOAT3 dir_to_target = e->getTarget()->getPos() - e->getPos();
		dir_to_target *= FLOAT3(1, 0, 1);
		FLOAT3 mydir = e->getDirection();
		mydir *= FLOAT3(1, 0, 1);
		mydir.Normalize();
		dir_to_target.Normalize();

		FLOAT3 result;
		float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
		if (angle < DirectX::XMConvertToRadians(0.5)) {
			break;
		}
		float max_angle = DirectX::XMConvertToRadians(max_angleY / CGameData::getInstance().getConstData()->getOPERATION_FPS());
		if (angle < max_angle)
			result = dir_to_target;
		else {
			using namespace DirectX;
			XMVECTOR v1 = XMLoadFloat3(&mydir.data);
			XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

			XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			int timeout = 0;
			while (XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
			{
				if (timeout > 10) {
					vRotationAxis = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
					break;
				}
				// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산
				v2 *= 7;
				vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				timeout++;
			}
			XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, max_angle);
			XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
			XMStoreFloat3(&result.data, vResult);
		}
		e->setDirection(result);

		break;
	}

	case ES_ATTACK:
	{
		if (e->getActCount() == 1) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();

			/*
			FLOAT3 dir2;
			dir2 = e->getPoint();
			FLOAT3 dir3 = dir2.getRotateY(direction);
			cout << "쐈습니다! 총구초기값 : " << e->getPoint().getX() << ", " << e->getPoint().getY() << ", " << e->getPoint().getZ() << endl<<
			"회전한 총구값 : " << dir3.getX() << ", " <<	dir3.getY() << ", " << dir3.getZ() << endl;

			*/
			for (auto &i : (*e->getPoint())) {
				space.ShootMissile(i.getRotateY(direction) +
					e->getPos(), e->getDirection(), e->getID(), e->getMissileType(), OT_ENEMY, e->getTarget());
			}
		}
		else if (e->getActCount() >= 240) {
			e->setState(ES_THINK);
			e->resetActCount();
			break;
		}
		break;
	}
	case ES_HIT:
	{
		break;
	}
	case ES_HUNGRY:
	{
		if (e->getActCount() >= 120) {
			e->setState(ES_THINK);
			e->resetActCount();
			break;
		}
		break;
	}
	default:
		break;
	}
	e->increaseActCount();

	/*
	if (e->getType()->enableGravity()) {
		e->addGravity(CGameData::getInstance().getConstData()->getGRAVITY_FPS());
	}
	else */ {
		float by = space.getMap()->getHeight(e->getPos().getX(), e->getPos().getZ());
		if (e->getPos().getY() < by + 250)
			e->setDirectionY(0.1f);
		else if (e->getPos().getY() > by + 320)
			e->setDirectionY(-0.1f);
	}
	e->Move();

	/*
	#ifdef _DEBUG
	if (e->getID() == 0)
	cout << e->getID() << "번 에너미 움직여욧 : " <<
	e->getPos().getX() << ", " << e->getPos().getY() << ", " << e->getPos().getZ() << endl;
	#endif // DEBUG
	*/
}
