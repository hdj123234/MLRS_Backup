#include "stdafx.h"
#include "AI_Ejavelin_Test.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"


void CAI_Ejavelin_Test::Act(CGameSpace & space, CGameObject * _obj)
{
	if (_obj->getObjectType() != OBJECT_MISSILE)
		return;
	CMissile* m = static_cast<CMissile*>(_obj);

	switch (m->getState()) {
	case MS_SPAWN: {
		if (m->getActCount() > 20) {
			m->setState(MS_READY_TO_ATTACK);
			m->resetActCount();
			break;
		}
		m->increaseActCount();
		m->Accel();
		break;
	}
	case MS_READY_TO_ATTACK:
	{
		if (m->getActCount() > 18) {
			m->setState(MS_RISE);
			m->resetActCount();
			break;
		}
		FLOAT3 mydir = m->getDirection();
		mydir.getY() -= 0.1f;
		mydir.Normalize();
		m->setDirection(mydir);
		m->DeAccel();
	}
	case MS_RISE: {
		if (m->getActCount() > 180) {
			m->setState(MS_MOVE_TO_TARGET);
			m->resetActCount();
			break;
		}
		FLOAT3 dir_to_target = m->getTarget()->getPos() - m->getPos();
		dir_to_target += FLOAT3(0, 10000, 0);		// !@#$%^ 키 임시입력
		FLOAT3 mydir = m->getDirection();
		dir_to_target.Normalize();

		FLOAT3 result;
		float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
		if (angle < m->getType()->getMaxAngle())
			result = dir_to_target;
		else {
			using namespace DirectX;
			XMVECTOR v1 = XMLoadFloat3(&mydir.data);
			XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

			DirectX::XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			while (DirectX::XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
			{
				// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산 시도
				v2 *= 1000;
				vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			}
			DirectX::XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, m->getType()->getMaxAngle());
			DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
			DirectX::XMStoreFloat3(&result.data, vResult);

			/*
			DirectX::XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			if (!DirectX::XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
			{
			DirectX::XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, m->getType()->getMaxAngle());
			DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
			DirectX::XMStoreFloat3(&result.data, vResult);
			}
			else
			result = dir_to_target;
			*/
		}

		m->setDirection(result);
		m->Accel();
		m->increaseActCount();

		break;
	}
	case MS_MOVE_TO_TARGET: {
		if (m->getActCount() < 50) {
			FLOAT3 dir_to_target = m->getTarget()->getPos() - m->getPos();
			dir_to_target += FLOAT3(0, 10, 0);		// !@#$%^ 키 임시입력
			FLOAT3 mydir = m->getDirection();
			dir_to_target.Normalize();

			FLOAT3 result;
			float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
			if (angle < DirectX::XMConvertToRadians(350/CGameData::getInstance().getConstData()->getOPERATION_FPS()))
				result = dir_to_target;
			else {
				using namespace DirectX;
				XMVECTOR v1 = XMLoadFloat3(&mydir.data);
				XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

				DirectX::XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				while (DirectX::XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
				{
					// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산 시도
					v2 *= 1000;
					vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				}
				DirectX::XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, m->getType()->getMaxAngle());
				DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
				DirectX::XMStoreFloat3(&result.data, vResult);

				/*
				DirectX::XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				if (!DirectX::XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
				{
				DirectX::XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, m->getType()->getMaxAngle());
				DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
				DirectX::XMStoreFloat3(&result.data, vResult);
				}
				else
				result = dir_to_target;
				*/
			}
			m->setDirection(result);

			break;
		}
		else if (m->getActCount() < m->getType()->getRange_fps()) {

			FLOAT3 dir_to_target = m->getTarget()->getPos() - m->getPos();
			dir_to_target += FLOAT3(0, 10, 0);		// !@#$%^ 키 임시입력
			FLOAT3 mydir = m->getDirection();
			dir_to_target.Normalize();

			/*
			FLOAT3 result;
			float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
			if (angle < m->getType()->getMaxAngle())
				result = dir_to_target;
			else {
				using namespace DirectX;
				XMVECTOR v1 = XMLoadFloat3(&mydir.data);
				XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

				DirectX::XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				while (DirectX::XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
				{
					// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산 시도
					v2 *= 1000;
					vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				}
				DirectX::XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, m->getType()->getMaxAngle());
				DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
				DirectX::XMStoreFloat3(&result.data, vResult);
			}
		*/
			m->setDirection(dir_to_target);

			break;
		}
		else
			m->Crash();

		m->Accel();
		m->increaseActCount();
		break;
	}

							// 이거 안써도 될듯싶다.
	case MS_BOOM: {
		m->removeMissile();
		return;
	}

	default:
		m->Accel();
		m->increaseActCount();
		break;
	}
	m->Move();

	float by = space.getMap()->getHeight(m->getPos().getX(), m->getPos().getZ());
	if (m->getPos().getY() < by)
		m->Crash();



	/*
	#ifdef _DEBUG
	if (e->getID() == 0)
	cout << e->getID() << "번 에너미 움직여욧 : " <<
	e->getPos().getX() << ", " << e->getPos().getY() << ", " << e->getPos().getZ() << endl;
	#endif // DEBUG
	*/
}
