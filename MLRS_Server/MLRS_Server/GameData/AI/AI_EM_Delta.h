#pragma once
#include "ObjectAI.h"

class CGameObject;
class CEnemy;
class CGameSpace;


class CAI_EM_Delta : public CObjectAI
{
	void Act(CGameSpace & space, CGameObject *_obj);
};
