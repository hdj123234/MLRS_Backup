#include "stdafx.h"
#include "AI_TurtleSlug.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"

void CAI_TurtleSlug::Act(CGameSpace & space, CGameObject* _obj)
{
	//	FLOAT3 debug_performance_test;
	//	debug_performance_test.Normalize();

	// dynamic_cast 보다 static_cast 가 더 빠르다고 합니다....
	if (_obj->getObjectType() != OBJECT_ENEMY)
		return;
	CEnemy* e = static_cast<CEnemy*>(_obj);

	switch (e->getState()) {

	case ES_IDLE:
		if (e->getActCount() >= 60) {
			e->resetActCount();
			e->setState(ES_THINK);
			break;
		}
		e->increaseActCount();
		break;
	case ES_THINK:
		e->setTargetToPlayer(space.setTargetToPlayer(e->getPos(), e->getType()->getSight()));
		if (e->getTarget() == nullptr)
			e->setState(ES_IDLE);
		else
			e->setState(ES_MOVE_TO_TARGET);
		break;
	case ES_TURN:
		// 플레이어가 계속 횡을 치면 보스가 이 상태에서 못빠져나올것
		// 때문에 일정만큼 돌다가 상태 강제 전환
		// 일단 도는 애니메이션이 나오고 생각합시다...
		if (e->getActCount() >= 240) {
			e->resetActCount();
			e->setState(ES_THINK);
			break;
		}
		break;
	case ES_MOVE_TO_TARGET:
	{
		if (e->getTarget() == nullptr)
			e->setState(ES_THINK);

		FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
		direction.getY() += 50;		// 이거 임시임!
		if (direction.getLength() <= 200) {	// 이거도 임시임! 근데 어떻게 값을 정할지...
			e->setState(ES_READY_TO_ATTACK);
			e->resetActCount();
			break;
		}

		direction.Normalize();
		e->setDirection(direction);
		e->Accel();
		e->increaseActCount();
		if (e->getActCount() >= 120) {
			e->setState(ES_THINK);
			e->resetActCount();
		}
		break;
	}

	case ES_READY_TO_ATTACK:
	{
		if (e->getActCount() <= 5) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			e->setDirection(direction.Normalize());
			e->Accel();
		}
		else
			e->DeAccel();

		e->increaseActCount();
		if (e->getActCount() >= 70) {
			e->setState(ES_ATTACK);
			e->resetActCount();
		}
		break;
	}

	case ES_ATTACK:
		// 공격 종류에 따라 움직임 패턴이 달라야 할까?
		// 
	{
		if (e->getActCount() == 0) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();

			for (auto &i : (*e->getPoint())) {
				space.ShootMissile(i.getRotateY(direction) +
					e->getPos(), e->getDirection(), e->getID(), e->getMissileType(), OT_ENEMY, e->getTarget());
			}
		}
		else if (e->getActCount() >= 120) {
			e->setState(ES_HUNGRY);
			e->resetActCount();
			break;
		}
		e->increaseActCount();
		break;
	}
	case ES_HUNGRY:
	{
		// 임시 상태
		if (e->getActCount() == 0) {
			FLOAT3 d = FLOAT3(static_cast<float>(rand() % 10000 - 5000),
				0,
				static_cast<float>(rand() % 10000 - 5000));
			d.Normalize();
			e->setDirection(d.getX(), 0, d.getZ());
		}
		e->increaseActCount();

		if (e->getActCount() <= 60) {
			e->Accel();
		}
		else if (e->getActCount() > 60) {
			e->DeAccel();
			if (e->getSpeed() == 0) {
				e->setState(ES_IDLE);
				e->resetActCount();
			}
		}
		break;
	}
	default:
		break;
	}

	if (e->getType()->enableGravity()) {
		e->addGravity(CGameData::getInstance().getConstData()->getGRAVITY_FPS());
	}
	else {
		float by = space.getMap()->getHeight(e->getPos().getX(), e->getPos().getZ());
		if (e->getPos().getY() < by + 50)
			e->setGravity((e->getPos().getY() - by) / 100);
		else if (e->getPos().getY() > by + 120)
			e->setGravity(-(e->getPos().getY() - by) / 100);
		else
			e->setGravity(0);
	}
	e->Move();

	/*
	#ifdef _DEBUG
	if (e->getID() == 0)
	cout << e->getID() << "번 에너미 움직여욧 : " <<
	e->getPos().getX() << ", " << e->getPos().getY() << ", " << e->getPos().getZ() << endl;
	#endif // DEBUG
	*/
}
