#pragma once
#ifndef OBJECTAI_H
#define OBJECTAI_H

class CGameObject;
class CEnemy;
class CGameSpace;

enum AI_NAME {
	AI_Zephyros,
	AI_Combat,
	AI_GoldenbatRider,
	AI_TurtleSlug,
	AI_Delta,
	AI_MeleeRange_Missile,
	AI_MidRange_Missile,
	AI_Lockon_Missile,
	AI_AMVC, 
	AI_EM,
	AI_Ejavelin_Test,
	AI_AutoLockon_MLRS_test,
	AI_EM_Delta
};

class CObjectAI
{
public:
	CObjectAI();
	~CObjectAI();

	virtual void Act(CGameSpace & space, CGameObject *e) = 0;
};




#endif // !OBJECTAI_H