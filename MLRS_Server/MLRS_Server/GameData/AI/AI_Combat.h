#pragma once
#include "ObjectAI.h"

class CGameObject;
class CEnemy;
class CGameSpace;

class CAI_Combat : public CObjectAI
{
	float max_angleY;
public:
	CAI_Combat();
	void Act(CGameSpace & space, CGameObject *_obj);
};
