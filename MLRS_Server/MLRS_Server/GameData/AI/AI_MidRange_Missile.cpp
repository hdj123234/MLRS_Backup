#include "stdafx.h"
#include "AI_MidRange_Missile.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"


void CAI_MidRange_Missile::Act(CGameSpace & space, CGameObject * _obj)
{
	if (_obj->getObjectType() != OBJECT_MISSILE)
		return;
	CMissile* m = static_cast<CMissile*>(_obj);

	switch (m->getState()) {
	case MS_SPAWN: {
		m->Accel();
		if (m->getActCount() < 1) {
			m->setSpeed(static_cast<float>(rand()%200)/100 + 2.f);
		}
		if (m->getActCount() > 5) {
			m->setState(MS_RISE);
			m->resetActCount();
			break;
		}
		m->increaseActCount();
		break;
	}	
	case MS_RISE: {
		m->Accel();
//		if (m->getActCount() >= 80) {
//			m->setState(MS_THINK);
//		}
		m->onGravity();
		m->increaseActCount();
		break;
	}
	case MS_THINK:
	{
		m->setTargetToEnemy(space.setTargetToEnemy(m->getID(), m->getPos(), 300));
		if (m->getTarget() != nullptr) {
			m->resetActCount();
			m->setState(MS_MOVE_TO_TARGET);
			float gravity = m->getGravity();
			m->addDirectionY(gravity);
			m->setGravity(0);
			m->setSpeed(7);
		}
		m->onGravity();
		break;
	}
	case MS_MOVE_TO_TARGET:
	{
		if (!m->getTarget()->is_Use()) {
			m->setState(MS_THINK);
			return;
		}
		if (m->getActCount() >= m->getType()->getRange_fps()) {
			m->resetActCount();
			m->Crash();
			return;
		} 
		
		FLOAT3 dir_to_target = m->getTarget()->getPos() - m->getPos();
		FLOAT3 mydir = m->getDirection();
		dir_to_target.Normalize();
		mydir.Normalize();

		FLOAT3 result;
		float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
		float max_angle = m->getType()->getMaxAngle();
		if (angle < m->getType()->getMaxAngle())
			result = dir_to_target;
		else {
			using namespace DirectX;
			XMVECTOR v1 = XMLoadFloat3(&mydir.data);
			XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

			DirectX::XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			int timeout = 0;
			while (DirectX::XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
			{
				if (timeout > 10)
					break;
				v2 *= 7;
				vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				timeout++;
			}
			DirectX::XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, max_angle);
			DirectX::XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
			DirectX::XMStoreFloat3(&result.data, vResult);
		}
		
		m->setDirection(result);
//		m->onGravity();
		m->increaseActCount();
		break;
	}
	case MS_READY_TO_ATTACK:
	{
		m->onGravity();
		break;
	}
	case MS_BOOM: {
		m->removeMissile();
		return;
	}
	default:
		m->Accel();
		m->increaseActCount();
		break;
	}
	m->Move();
	float by = space.getMap()->getHeight(m->getPos().getX(), m->getPos().getZ());
	if (m->getPos().getY() < by)
		m->Crash();


}
