#pragma once
#include "ObjectAI.h"

class CGameObject;
class CEnemy;
class CGameSpace;

class CAI_Delta : public CObjectAI
{
	float max_angleY;
public:
	CAI_Delta();
	void Act(CGameSpace & space, CGameObject *_obj);
};
