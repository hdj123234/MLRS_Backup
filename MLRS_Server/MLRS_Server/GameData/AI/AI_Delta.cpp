#include "stdafx.h"
#include "AI_Delta.h"
#include "..\..\Missile.h"
#include "..\..\GameSpace.h"

CAI_Delta::CAI_Delta()
{
	max_angleY = 1.f;
}

void CAI_Delta::Act(CGameSpace & space, CGameObject* _obj)
{
	if (_obj->getObjectType() != OBJECT_BOSS)
		return;
	CEnemy* e = static_cast<CEnemy_Boss*>(_obj);

	switch (e->getState()) {
	case ES_SPAWN:
		//		if (e->getActCount() >= 60) {
		if (e->getActCount() == 1) {
			e->setTargetToPlayer(space.setTargetToPlayer(e->getPos(), e->getType()->getSight()));
			e->setDirection(FLOAT3(1, 0, 1));

			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			for (auto &i : (*e->getPoint())) {
				space.ShootMissile(i.getRotateY(direction) +
					e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, e->getTarget());
			}
		}
		else if (e->getActCount() > 600) {
			e->resetActCount();
			e->setState(ES_IDLE);
		}
		break;
	case ES_IDLE:
		e->DeAccel();
		if (e->getActCount() >= 60) {
			e->resetActCount();
			e->setState(ES_MOVE_TO_TARGET);
			break;
		}
		break;
	case ES_THINK:
		e->setTargetToPlayer(space.setTargetToPlayer(FLOAT3(0, 0, 0), 10));
		//e->setTargetToPlayer(space.setTargetToObject());
		if (e->getTarget() == nullptr) {
			e->setState(ES_IDLE);
			e->resetActCount();
		}
		else {
			e->setState(ES_MOVE_TO_TARGET);
			e->resetActCount();
		}
		break;
	case ES_MOVE_TO_TARGET:
	{
		if (e->getTarget() == nullptr) {
			e->setState(ES_THINK);
			e->resetActCount();
		}
		
		FLOAT3 dir_to_target = e->getTarget()->getPos() - e->getPos();
		dir_to_target *= FLOAT3(1, 0, 1);		// !@#$%^ 키 임시입력
		if (dir_to_target.getLength() <= 1000) {	// 이거도 임시임! 근데 어떻게 값을 정할지...
			e->setState(ES_READY_TO_ATTACK);
			e->resetActCount();
			break;
		}
		dir_to_target.Normalize();
		e->setDirection(dir_to_target);
		/*
		FLOAT3 mydir = e->getDirection();
		mydir *= FLOAT3(1, 0, 1);
		mydir.Normalize();
		dir_to_target.Normalize();

		FLOAT3 result = dir_to_target;
		float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));	
		if (angle > DirectX::XMConvertToRadians(1)) {
			float max_angle = DirectX::XMConvertToRadians(max_angleY / CGameData::getInstance().getConstData()->getOPERATION_FPS());
			if (angle < max_angle)
				result = dir_to_target;
			else {
				using namespace DirectX;
				XMVECTOR v1 = XMLoadFloat3(&mydir.data);
				XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);

				XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				int timeout = 0;
				while (XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
				{
					if (timeout > 10) {
						vRotationAxis = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
						break;
					}
					// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산
					v2 *= 7;
					vRotationAxis = DirectX::XMVector3Cross(v1, v2);
					timeout++;
				}
				XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, max_angle);
				XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
				XMStoreFloat3(&result.data, vResult);
			}
		}
		e->setDirection(result);

		*/

		e->Accel();
		if (e->getActCount() >= 180) {
			e->setState(ES_THINK);
			e->resetActCount();
		}

		break;
	}

	case ES_READY_TO_ATTACK:
	{
		e->DeAccel();
		if (e->getSpeed() <= 0) {
			e->setState(ES_ATTACK);
			e->resetActCount();
		}
		break;
	}

	case ES_ATTACK:
	{
		if (e->getActCount() == 1) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			for (auto &i : (*e->getPoint())) {
				space.ShootMissile(i.getRotateY(direction) +
					e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, e->getTarget());
			}
		}

		/*
		if (e->getActCount() == 1) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			for (auto &i : (*e->getPoint())) {
				space.ShootMissile(i.getRotateY(direction) +
					e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, e->getTarget());
			} 
		}*/
		if (e->getActCount() == 20) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			if (space.setTarget(0) != nullptr) {
				for (auto &i : (*e->getPoint())) {
					space.ShootMissile(i.getRotateY(direction) +
						e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, space.setTarget(0));
				}
			}
		}

		if (e->getActCount() == 40) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			if (space.setTarget(1) != nullptr) {
				for (auto &i : (*e->getPoint())) {
					space.ShootMissile(i.getRotateY(direction) +
						e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, space.setTarget(1));
				}
			}
		}

		if (e->getActCount() == 60) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			if (space.setTarget(2) != nullptr) {
				for (auto &i : (*e->getPoint())) {
					space.ShootMissile(i.getRotateY(direction) +
						e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, space.setTarget(2));
				}
			}
		}

		if (e->getActCount() == 80) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			if (space.setTarget(3) != nullptr) {
				for (auto &i : (*e->getPoint())) {
					space.ShootMissile(i.getRotateY(direction) +
						e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, space.setTarget(3));
				}
			}
		}
		/*
		if (e->getActCount() == 40) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			for (auto &i : (*e->getPoint())) {
				space.ShootMissile(i.getRotateY(direction) +
					e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, space.setTargetToPlayer(e->getPos(), 10000000));
			}
		}
		if (e->getActCount() == 60) {
			FLOAT3 direction = e->getTarget()->getPos() - e->getPos();
			direction.getY() = 0;
			direction.Normalize();
			for (auto &i : (*e->getPoint())) {
				space.ShootMissile(i.getRotateY(direction) +
					e->getPos(), FLOAT3(0, 1, -0), e->getID(), e->getMissileType(), OT_ENEMY, e->getTarget());
			}
		}
		*/


		else if (e->getActCount() >= 600) {
			e->setState(ES_MOVE_TO_TARGET);
			e->resetActCount();
			break;
		}
		break;
	}
	case ES_HUNGRY:
	{
		if (e->getTarget() == nullptr) {
			e->setState(ES_THINK);
			e->resetActCount();
		}

		FLOAT3 dir_to_target = (e->getTarget()->getPos() - e->getPos()) * -1;
		dir_to_target *= FLOAT3(1, 0, 1);		// !@#$%^ 키 임시입력
		FLOAT3 mydir = e->getDirection();
		mydir *= FLOAT3(1, 0, 1);
		mydir.Normalize();
		dir_to_target.Normalize();

		FLOAT3 result;
		float angle = DirectX::XMScalarACos(FLOAT3::dot(mydir, dir_to_target));
		float max_angle = DirectX::XMConvertToRadians(max_angleY / CGameData::getInstance().getConstData()->getOPERATION_FPS());
		if (angle < max_angle)
			result = dir_to_target;
		else {
			//				if (e->getActCount() == 0) {
			//					max_angle = DirectX::XMConvertToRadians(-max_angle / CGameData::getInstance().getConstData()->getOPERATION_FPS());
			//				}
			using namespace DirectX;
			XMVECTOR v1 = XMLoadFloat3(&mydir.data);
			XMVECTOR v2 = XMLoadFloat3(&dir_to_target.data);
			XMVECTOR vRotationAxis = DirectX::XMVector3Cross(v1, v2);
			int timeout = 0;
			while (XMVector3Equal(vRotationAxis, DirectX::XMVectorZero()))
			{
				if (timeout > 10) {
					vRotationAxis = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
					break;
				}
				// 근사값이라 계산이 잘못될 경우 1,000 을 곱하여 다시 계산
				v2 *= 7;
				vRotationAxis = DirectX::XMVector3Cross(v1, v2);
				timeout++;
			}
			XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, max_angle);
			XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
			XMStoreFloat3(&result.data, vResult);
		}

		e->setDirection(result);
		if (e->getActCount() <= 120) {
			e->Accel();
		}
		else if (e->getActCount() > 120) {
			e->setState(ES_THINK);
			e->resetActCount();
		}
		break;
	}
	default:
		break;
	}
	e->increaseActCount();
	if (e->getType()->enableGravity()) {
		e->addGravity(CGameData::getInstance().getConstData()->getGRAVITY_FPS());
	}
	else {
		float by = space.getMap()->getHeight(e->getPos().getX(), e->getPos().getZ());
		if (e->getPos().getY() < by + 50)
			e->setGravity((e->getPos().getY() - by) / 100);
		else if (e->getPos().getY() > by + 120)
			e->setGravity(-(e->getPos().getY() - by) / 100);
		else
			e->setGravity(0);
	}
	e->Move();
	//float 보정값 = e->getPos().getY();
	//e->setPosY(보정값+12);

	/*
	#ifdef _DEBUG
	if (e->getID() == 0)
	cout << e->getID() << "번 에너미 움직여욧 : " <<
	e->getPos().getX() << ", " << e->getPos().getY() << ", " << e->getPos().getZ() << endl;
	#endif // DEBUG
	*/
}
