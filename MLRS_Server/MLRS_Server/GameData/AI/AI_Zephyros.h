#pragma once
#include "ObjectAI.h"

class CGameObject;
class CEnemy;
class CGameSpace;

class CAI_Zephyros : public CObjectAI
{
	float max_angleY;
public:
	CAI_Zephyros();	
	void Act(CGameSpace & space, CGameObject *_obj);
};
