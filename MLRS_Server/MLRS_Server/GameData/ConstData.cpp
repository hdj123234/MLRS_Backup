#include "stdafx.h"
#include "ConstData.h"
#include <string>
#include <fstream>
#include <iostream>
#include <map>
using namespace std;

// 파일에서 읽어온 데이터를 통해 값이 정해지는 변수들 초기화
void CConstData::InitData()
{
	OperationFPSTime = static_cast<int>(1000 / FPS);
	OperationFPSSpareTime = 1000 / FPS - OperationFPSTime;
	GravityFPS = Gravity/(-FPS);
	ClientView_NoSQRT = ClientView*ClientView;
}

CConstData::CConstData()
{
	map<string, map<string, CDataType>*> upper_class;
	map<string, CDataType> lower_class_Network;
	upper_class.insert(make_pair("Network", &lower_class_Network));
	lower_class_Network.insert(make_pair("PORT", CDataType(&Port))); Port = 9000;

	map<string, CDataType> lower_class_Room;
	upper_class.insert(make_pair("Room", &lower_class_Room));
	lower_class_Room.insert(make_pair("RoomMaxCreate", CDataType(&RoomMaxCreate))); RoomMaxCreate = 25;
	lower_class_Room.insert(make_pair("RoomMaxTitleLen", CDataType(&RoomMaxTitleLen))); RoomMaxTitleLen = 36;
	lower_class_Room.insert(make_pair("MaxRoominLobby", CDataType(&MaxRoominLobby))); MaxRoominLobby = 6;

	map<string, CDataType> lower_class_Operation;
	upper_class.insert(make_pair("Operation", &lower_class_Operation));
	lower_class_Operation.insert(make_pair("FPS", CDataType(&FPS))); FPS = 60.f;
	lower_class_Operation.insert(make_pair("Gravity", CDataType(&Gravity))); Gravity = 9.8f;
	lower_class_Operation.insert(make_pair("MaxSizeEnemyNumber", CDataType(&MaxSizeEnemyNumber))); MaxSizeEnemyNumber = 200;
	lower_class_Operation.insert(make_pair("MaxSizeMissileNumber", CDataType(&MaxSizeMissileNumber))); MaxSizeMissileNumber = 200;
	
	
	map<string, CDataType> lower_class_Sector;
	upper_class.insert(make_pair("Sector", &lower_class_Sector));
	lower_class_Sector.insert(make_pair("SpacePartitionX", CDataType(&SpacePartitionX))); SpacePartitionX = 10;
	lower_class_Sector.insert(make_pair("SpacePartitionZ", CDataType(&SpacePartitionZ))); SpacePartitionZ = 10;

	map<string, CDataType> lower_class_Client;
	upper_class.insert(make_pair("Client", &lower_class_Client));
	lower_class_Client.insert(make_pair("ClientView", CDataType(&ClientView))); ClientView = 500;

	map<string, CDataType> lower_class_Camp;
	upper_class.insert(make_pair("BaseCamp", &lower_class_Camp));
	lower_class_Camp.insert(make_pair("HP", CDataType(&BaseCampHP))); BaseCampHP = 200;

	ifstream initfile;
	initfile.open("Data\\ConstData.ini", ios::binary);
	if (!initfile.is_open()) {
		cout << "데이터 초기화 파일을 찾을 수 없습니다. (ConstData.ini)" << endl;
		return;
	}

	string ts;
	string skipequal;
	string svalue;
	getline(initfile, ts, '[');
	while (1) {
		getline(initfile, ts, ']');

		if (ts.empty())
			break;

		string lower_name;
		auto lower = upper_class.find(ts);
		
		while (1) {
			ts.clear();
			initfile >> ts;
			if (ts.empty())
				break;
			else if (*ts.begin() == '[') {
				int back = ts.length();
				initfile.seekg(-back, initfile.cur);
				getline(initfile, ts, '[');
				break;
			}
			
			auto pvalue = lower->second->find(ts);
			
			initfile >> skipequal;
			initfile >> svalue;

			switch (pvalue->second.getType()) {
			case DATA_TYPE::TYPE_INT:
				pvalue->second.setData(stoi(svalue)); break;
			case DATA_TYPE::TYPE_FLOAT:
				pvalue->second.setData(stof(svalue)); break;
			default:
				break;
			}
		}
	}
	initfile.close();

	InitData();
}

CConstData::~CConstData()
{
}

/*
CConstData::CConstData(int _ROOM_MAX_CREAT,
	int _ROOM_MAX_TITLE_LEN,
	int _LOBBY_MAX_SHOW_ROOM,
	float _HEIGHTMAP_Y_POSITION,
	int _OPERATION_FPS_TIME,
	float _OPERATION_FPS_SPARE_TIME,
	float _OPERATION_FPS,
	float _
	,
	int _MAXSIZE_ENEMY_NUMBER,
	int _MAXSIZE_MISSILE_NUMBER,
	float _MAP_SIZE_X,
	float _MAP_SIZE_Z,
	int _SPACE_PARTITION_X,
	int _SPACE_PARTITION_Z,
	float _CLIENT_VIEW,
	float _CLIENT_VIEW_NOSQRT)
	:ROOM_MAX_CREAT(_ROOM_MAX_CREAT),
	ROOM_MAX_TITLE_LEN(_ROOM_MAX_TITLE_LEN),
	LOBBY_MAX_SHOW_ROOM(_LOBBY_MAX_SHOW_ROOM),
	HEIGHTMAP_Y_POSITION(_HEIGHTMAP_Y_POSITION),
	OPERATION_FPS_TIME(_OPERATION_FPS_TIME),
	OPERATION_FPS_SPARE_TIME(_OPERATION_FPS_SPARE_TIME),
	OPERATION_FPS(_OPERATION_FPS),
	GRAVITY_FPS(_GRAVITY_FPS),
	MAXSIZE_ENEMY_NUMBER(_MAXSIZE_ENEMY_NUMBER),
	MAXSIZE_MISSILE_NUMBER(_MAXSIZE_MISSILE_NUMBER),
	MAP_SIZE_X(_MAP_SIZE_X),
	MAP_SIZE_Z(_MAP_SIZE_Z),
	SPACE_PARTITION_X(_SPACE_PARTITION_X),
	SPACE_PARTITION_Z(_SPACE_PARTITION_Z),
	CLIENT_VIEW(_CLIENT_VIEW),
	CLIENT_VIEW_NOSQRT(_CLIENT_VIEW_NOSQRT)
{};*/