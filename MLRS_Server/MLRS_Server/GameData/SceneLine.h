#pragma once
#include "..\KeyBuf.h"

class CSceneLine
{
	const int line;
public:
	CSceneLine();
	CSceneLine(int _line);
	~CSceneLine();

	virtual KEYBUF makeLineKey(int *_psceneid, int *_plineid) = 0;
};



class CSceneLine_addSingleMob :public CSceneLine
{
	int type;
	float pos_x, pos_z;		// 몹을 추가시킬 x,z 좌표
public:
	CSceneLine_addSingleMob(int _line, int _type, float _pos_x, float _pos_z)
		: CSceneLine(_line), type(_type), pos_x(_pos_x), pos_z(_pos_z) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_addMultiMob :public CSceneLine
{
	int type;
	int amount;				// 추가할 몹의 양
	float pos_x, pos_z;		// 몹을 추가시킬 x,z 좌표
	int rad;				// 위 좌표의 범위
public:
	CSceneLine_addMultiMob(int _line, int _type, int _amount, float _pos_x, float _pos_z, int _rad)
		: CSceneLine(_line), type(_type), amount(_amount), pos_x(_pos_x), pos_z(_pos_z), rad(_rad) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_addBossMob :public CSceneLine
{
	int type;
	float pos_x, pos_z;		// 보스를 추가시킬 x,z 좌표
public:
	CSceneLine_addBossMob(int _line, int _type, float _pos_x, float _pos_z)
		: CSceneLine(_line), type(_type), pos_x(_pos_x), pos_z(_pos_z) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_setObjectPos :public CSceneLine
{
	int id;
	float pos_x, pos_z;		// 오브젝트를 추가시킬 x,z 좌표
public:
	CSceneLine_setObjectPos(int _line, int _id, float _pos_x, float _pos_z)
		: CSceneLine(_line), id(_id), pos_x(_pos_x), pos_z(_pos_z) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_setObjectSpeed :public CSceneLine
{
	int id;
	float speed_x, speed_y, speed_z;		// 변경할 오브젝트의 속도치
public:
	CSceneLine_setObjectSpeed(int _line, int _id, float _speed_x, float _speed_y, float _speed_z)
		: CSceneLine(_line), id(_id), speed_x(_speed_x), speed_y(_speed_y), speed_z(_speed_z) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_setInitPos :public CSceneLine
{
	float pos_x, pos_z;			// 플레이어들이 초기화 될 x, z 위치
public:
	CSceneLine_setInitPos(int _line, float _pos_x, float _pos_z)
		: CSceneLine(_line), pos_x(_pos_x), pos_z(_pos_z) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_setPlayerPos :public CSceneLine
{
	float pos_x, pos_z;			// 플레이어의 위치를 x,z 로 이동
public:
	CSceneLine_setPlayerPos(int _line, float _pos_x, float _pos_z)
		: CSceneLine(_line), pos_x(_pos_x), pos_z(_pos_z) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_printScript :public CSceneLine
{
	int string_id;			// 출력할 string의 ID
public:
	CSceneLine_printScript(int _line, int _string_id)
		: CSceneLine(_line), string_id(_string_id) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_recoverHP :public CSceneLine
{
	float heal;
public:
	CSceneLine_recoverHP(int _line, float _heal)
		: CSceneLine(_line), heal(_heal) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_setTimer :public CSceneLine
{
	int curScene;			// 타이머가 끝났을 때 현재 씬을 확인하기 위한 ID값
	int goScene;			// 만약 타이머가 끝날 때, if(현재 씬 == curScene) 이면 넘어갈 씬 ID
	int time;				// 세팅할 시간(ms)
public:
	CSceneLine_setTimer(int _line, int _curScene, int _goScene, int _time)
		: CSceneLine(_line), curScene(_curScene), goScene(_goScene), time(_time) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_wait :public CSceneLine
{
	int time;				// 기다릴 시간
public:
	CSceneLine_wait(int _line, int _time)
		: CSceneLine(_line), time(_time) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_checkMob :public CSceneLine
{
	int under;				// 몹이 해당치의 이하가 될 때 까지 대기
public:
	CSceneLine_checkMob(int _line, int _under)
		: CSceneLine(_line), under(_under) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_checkPos :public CSceneLine
{
	float x, z;				// 플레이어가 해당 반경에 들어올때까지 대기
	float rad;
public:
	CSceneLine_checkPos(int _line, float _x, float _z, float _rad)
		: CSceneLine(_line), x(_x), z(_z), rad(_rad) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_checkXPos :public CSceneLine
{
	float x;				// 플레이어의 X 좌표가 해당 값보다 작아질때까지 대기
public:
	CSceneLine_checkXPos(int _line, float _x)
		: CSceneLine(_line), x(_x) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_checkZPos :public CSceneLine
{
	float z;				// 플레이어의 좌표가 해당 값을 넘을때까지 대기
public:
	CSceneLine_checkZPos(int _line, float _z)
		: CSceneLine(_line), z(_z) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_break :public CSceneLine
{
public:
	CSceneLine_break(int _line)
		: CSceneLine(_line) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_repeatScene :public CSceneLine
{
public:
	CSceneLine_repeatScene(int _line)
		: CSceneLine(_line) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_Win :public CSceneLine
{
public:
	CSceneLine_Win(int _line)
		: CSceneLine(_line) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};

class CSceneLine_Defeat :public CSceneLine
{
public:
	CSceneLine_Defeat(int _line)
		: CSceneLine(_line) {};
	KEYBUF makeLineKey(int *_psceneid, int *_plineid);
};
