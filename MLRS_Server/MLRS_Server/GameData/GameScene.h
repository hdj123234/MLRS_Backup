#pragma once
#include <vector>
#include "SceneLine.h"
using namespace std;
class CGameScene
{
	const int line;
	vector<CSceneLine*> vline;

public:
	CGameScene();
	CGameScene(int _line);
	~CGameScene();

	void putLine(CSceneLine *s) { vline.push_back(s); }
	vector<CSceneLine*> getLineVector() { return vline; }
	CSceneLine* getLine(int _lineid) { return vline[_lineid]; }
};