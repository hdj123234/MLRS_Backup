#pragma once
#ifndef BOSSMECHA_H
#define BOSSMECHA_H
#include "EnemyMecha.h"
using namespace std;

enum BOSS_TYPE {
	BT_DELTA
};

class CBossMecha : public CEnemyMecha
{
public:
	CBossMecha();
	~CBossMecha();
	CBossMecha(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename);

};

class CBoss_Delta : public CBossMecha
{
	vector<FLOAT3> point;		// �ѱ� ��ġ
public:
	CBoss_Delta();
	CBoss_Delta(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename);

	vector<FLOAT3>* getPoints() { return &point; }
};

#endif // !ENEMYMECHA_H