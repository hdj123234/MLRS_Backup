#include "stdafx.h"
#include "BossMecha.h"
#include "GameData.h"

CBossMecha::CBossMecha()
{
}


CBossMecha::~CBossMecha()
{

}

CBossMecha::CBossMecha(int _id, string _name, int _type,
	float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename)
	:CEnemyMecha(_id, _name, _type, _hp, _accel, _max_speed, _ai, _sight, _missile_type, _zen_posY, _enableGravity, _offsetY, _bo_filename)
{

	/*
	id = _id;
	name = _name;
	type = _type;
	hp = _hp;
	accel = _accel;
	max_speed = _max_speed;
	ai = _ai;
	sight = _sight;
	missile_type = _missile_type;
	zen_posY = _zen_posY;
	enable_gravity = _enableGravity;
	offsetY = _offsetY;
	*/
}



CBoss_Delta::CBoss_Delta(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename)
	: CBossMecha(_id, _name, _type, _hp, _accel, _max_speed, _ai, _sight, _missile_type, _zen_posY, _enableGravity, _offsetY, _bo_filename)
{
	for (auto i = pBoMap.begin(); i != pBoMap.end(); ++i)
	{
		auto pbo = i->second;
		if (pbo->getType() == Type_BO::Multiple) {
			auto v = dynamic_cast<CBoundingObject_Multiple*>(pbo)->getList();
			for (auto &j : *v) {
				if (j->getName() == "�Ӹ�") {
					auto t = v->at(0);
					v->at(0) = j;
					j = t;
					break;
				}
			}
		}
	}

	for (int i = 0; i <= 120; i += 20)
	{
		point.push_back(FLOAT3(-34.3061f, 48.1379f, 84.2866f - i));
		point.push_back(FLOAT3(34.3061f, 48.1379f, 84.2866f - i));
		point.push_back(FLOAT3(0.f, 48.1379f, 84.2866f - i));
	}
}