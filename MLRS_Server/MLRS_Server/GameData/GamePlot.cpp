#include "stdafx.h"
#include "GamePlot.h"



CGamePlot::CGamePlot()
{
}


CGamePlot::~CGamePlot()
{
}

KEYBUF CGamePlot::getSceneKey(int * _psceneid, int * _plineid)
{
	int sceneid = *_psceneid;
	int lineid = *_plineid;
	return vscene[sceneid].getLine(lineid)->makeLineKey(_psceneid, _plineid);
}
