#include "stdafx.h"
#include "EnemyMecha.h"
#include "GameData.h"

CEnemyMecha::CEnemyMecha()
{
}


CEnemyMecha::~CEnemyMecha()
{

}

CEnemyMecha::CEnemyMecha(int _id, string _name, int _type,
	float _hp, float _accel, float _max_speed, int _ai, 
	float _sight, int _missile_type, float _zen_posY, 
	bool _enableGravity, float _offsetY, string _bo_filename)
	:CObjectTypeData(_id, _name, _bo_filename)
{
	type = _type;
	hp = _hp;
	accel = _accel;
	max_speed = _max_speed;
	ai = _ai;
	sight = _sight;
	missile_type = _missile_type;
	zen_posY = _zen_posY;
	enable_gravity = _enableGravity;
	offsetY = _offsetY;
}


CZephyros::CZephyros()
{
}

CZephyros::CZephyros(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename)
	: CEnemyMecha(_id, _name, _type, _hp, _accel, _max_speed, _ai, _sight, _missile_type, _zen_posY, _enableGravity, _offsetY, _bo_filename)
{
	point.push_back(FLOAT3(0, 0, 0));
}

vector<FLOAT3>* CZephyros::getPoints()
{
	return &point;
}

CCombat::CCombat()
{
}

CCombat::CCombat(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename)
	: CEnemyMecha(_id, _name, _type, _hp, _accel, _max_speed, _ai, _sight, _missile_type, _zen_posY, _enableGravity, _offsetY, _bo_filename)
{
	point.push_back(FLOAT3(0, 0, 0));
}

vector<FLOAT3>* CCombat::getPoints()
{
	return &point;
}

CGoldenbatRider::CGoldenbatRider(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename)
	: CEnemyMecha(_id, _name, _type, _hp, _accel, _max_speed, _ai, _sight, _missile_type, _zen_posY, _enableGravity, _offsetY, _bo_filename)
{
	point.push_back(FLOAT3(0, 0, 0));
}

vector<FLOAT3>* CGoldenbatRider::getPoints()
{
	return &point;
}