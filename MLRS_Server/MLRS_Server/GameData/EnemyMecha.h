#pragma once
#ifndef ENEMYMECHA_H
#define ENEMYMECHA_H
#include <string>
#include "ObjectTypeData.h"
#include "AI\ObjectAI.h"
#include "..\GameObject.h"
using namespace std;

enum ENERMY_TYPE {
	ET_ZEPHYROS, ET_COMBAT, ET_GoldenbatRIDER, ET_TURTLESLUG
};
class CEnemyMecha : public CObjectTypeData
{
protected:
	int type;
	float hp;

	float accel;		// 가속도
	float max_speed;	// 최고속도

	int ai;				// ai ID번호
	float sight;		// 시야 거리

	int missile_type;
	float zen_posY;		// 젠 시킬 Y좌표값

	bool enable_gravity;
	float offsetY;
public:
	CEnemyMecha();
	~CEnemyMecha();
	CEnemyMecha(int _id, string _name, int _type, float _hp, float _accel,
		float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, 
		bool _enableGravity, float _offsetY, string _bo_filename);

	float getHP() { return hp; }
	float getAcceleration() { return accel; };
	float getMaxSpeed() { return max_speed; };
	int getAI() { return ai; };
	float getSight() { return sight; };
	int getMissileType() { return missile_type; }
	bool enableGravity() { return enable_gravity; }
	float getZenposY() { return zen_posY; }
	float getOffsetY() { return offsetY; }
	
	virtual vector<FLOAT3>* getPoints() = 0;
};


// 이거 따로 만들 필요가 없는듯?
class CZephyros : public CEnemyMecha
{
	vector<FLOAT3> point;		// 총구 위치
public:
	CZephyros();
	CZephyros(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename);

	vector<FLOAT3>* getPoints();
};

class CCombat : public CEnemyMecha
{
	vector<FLOAT3> point;		// 총구 위치
public:
	CCombat();
	CCombat(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename);

	vector<FLOAT3>* getPoints();
};

class CGoldenbatRider : public CEnemyMecha
{
	vector<FLOAT3> point;		// 총구 위치
public:
	CGoldenbatRider();
	CGoldenbatRider(int _id, string _name, int _type, float _hp, float _accel, float _max_speed, int _ai, float _sight, int _missile_type, float _zen_posY, bool _enableGravity, float _offsetY, string _bo_filename);

	vector<FLOAT3>* getPoints();
};

#endif // !ENEMYMECHA_H