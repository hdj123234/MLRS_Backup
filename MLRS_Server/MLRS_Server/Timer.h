#pragma once
#include "stdafx.h"
#include <chrono>
#include <queue>
#include <concurrent_vector.h>
#include "Room.h"
using namespace std;
using namespace concurrency;


#define OPER_0 0
struct event_type {
	int room_id;
	unsigned int wakeup_time;
	int oper_type;
	int value1, value2;

	/*
	event_type(int _room_id = 0, unsigned _wakeup_time = 0,
		int _oper_type = 0) {
		room_id = _room_id;
		wakeup_time = _wakeup_time;
		oper_type = _oper_type;
		value1 = 0; value2 = 0;
	}*/
	event_type(int _room_id = 0, unsigned _wakeup_time = 0,
		int _oper_type = 0, int _value1 = 0, int _value2 = 0) {
		room_id = _room_id;
		wakeup_time = _wakeup_time;
		oper_type = _oper_type;
		value1 = _value1; 
		value2 = _value2;
	}
	void operator=(const event_type& t)
	{
		oper_type = t.oper_type;
		room_id = t.room_id;
		wakeup_time = t.wakeup_time;
		value1 = t.value1;
		value2 = t.value2;
	}
};

class mycomparison
{
public:
	mycomparison() {}
	bool operator() (const event_type lhs, const event_type rhs) const
	{
		return (lhs.wakeup_time > rhs.wakeup_time);
	}
};

class CTimer
{
	priority_queue<event_type, vector<event_type>, mycomparison> timer_queue;
	mutex tq_lock;
	HANDLE hIOCP;
	vector<CRoom> *roomVector; //concurrent_vector<CRoom> *roomVector;

	HANDLE timeThread;
	unsigned int debug_time;
	
	static CTimer* pthis;
public:
	CTimer();
	~CTimer();

	static DWORD WINAPI Timer_Thread(LPVOID arg);
	void ProcessEvent(event_type);

	void TimerInit(HANDLE hiocp, vector<CRoom>& r) {//concurrent_vector<CRoom>& r) {
		hIOCP = hiocp;
		roomVector = &r;
	}
	/////////////////////////////////
	void AddTimer(int _room_id, unsigned _wakeup_time, int _oper_type) {
		tq_lock.lock();
		timer_queue.push(event_type(_room_id, _wakeup_time, _oper_type));
		tq_lock.unlock();
	}
	void TimerStart();
};
