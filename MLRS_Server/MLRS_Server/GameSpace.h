#pragma once
#ifndef GAMESPACE_H
#define GAMESPACE_H

#include "Player.h"
#include "Missile.h"
#include "GameObject.h"
#include "StaticObject.h"
#include "Enemy_Boss.h"
#include "MultipleMissileLaunch.h"
#include <vector>
#include <list>


class CGameSpace
{
	int roomid;
	int map_id;
	CTerrain *pMap;

	int debug_missileID;

	vector<vector<vector<int>>> vvEnemySector;
	vector<int> vExtraSector;

	vector<CPlayer>* vp;
	vector<CEnemy>* ve;
	vector<CEnemy_Boss>* vb;
	vector<CMissile>* vm;
	vector<CStaticObject>* vo;

	vector<CMultipleMissileLaunch> vMML;

public:
	CGameSpace();
	~CGameSpace();
	void SpaceInit(int _roomid,
		vector<CPlayer>* _vp,
		vector<CEnemy>* _ve,
		vector<CEnemy_Boss>* _vb,
		vector<CMissile>* _vm,
		vector<CStaticObject>* _vo);

	void setMap(int _mapid) { map_id = _mapid; pMap = CGameData::getInstance().getMap(_mapid); }
	CTerrain* getMap() { return pMap; }

	void pushSector(int _enemyID);
	void clearSector();
	// !@#$%^ 맵 아이디 받아오는거 만들어야함
	
	CGameObject* setTarget(unsigned int _pid);
	CGameObject* setTargetToPlayer(FLOAT3 _pos, float _sight);
	CGameObject* setTargetToPlayerCompulsory(FLOAT3 _pos);
	CGameObject* setTargetToPlayerExceptObject(FLOAT3 _pos, float _sight);
	CGameObject * setTargetToObject();
	CGameObject* setTargetToEnemy(int _target_id, FLOAT3 _pos, float _sight);
	void ShootMissile(FLOAT3 _pos, FLOAT3 _dir, int _owner_id, int _missile_type, OWNER_TYPE _owner_type, CGameObject* _target);
	void ShootLaunchedMissile();
	void setMultipleMissile(int _player_id, int _slot, FLOAT3 _pos, FLOAT3 _look, FLOAT3 _up, FLOAT3 _right);

	bool CollisionChecktoEnemy(int missile_id);
	int CollisionChecktoBoss(int missile_id);
	void ClearNearList();
	void pushEnemyToNearList(int _enemyid);
	void pushMissileToNearList(int _missileid);
	void RelocateViewList();
};

#endif // !GAMESPACE_H