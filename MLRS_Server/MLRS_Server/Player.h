#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#include "GameData\PlayerMecha.h"
#include "GameObject.h"
#include <vector>
#include <mutex>
#include <unordered_set>

enum PLAYER_TRANSLATE : BYTE {
	PLAYER_MOVE = 1,
	PLAYER_JUMP = 2,
	PLAYER_BOOST = 4,
	PLAYER_SHOOT = 8
};
typedef unsigned char OBJECT_TRANSINFO;
typedef unsigned char PLAYER_TRANSINFO;
class CPlayer : public CGameObject
{
	int id;	// 플레이어 ID
	int index_id;	// 방에서 부여받은 순서
	int mecha_id;	// 플레이어가 선택한 메카의 id
	bool is_ready;	// 레디 여부

	float hp;			// 체력
	int state;		// 캐릭터의 현재 상태

	float boost_speed;
	float boost_guage;
	int boost_cool;

	float look_x, look_y, look_z;	// 캐릭터가 바라보는 방향
	float speed_x, speed_y, speed_z;	// 캐릭터의 속도
	int ani_state;	// 애니메이션 상태
	int ani_count;	// 애니메이션 키 카운트

	bool is_weapon_change;
	int select_weapon;		// 플레이어가 선택한 무기타입
	int fire_weapon_type;	// 플레이어가 사용한 무기타입

	std::unordered_set<int> view_list;
	std::unordered_set<int> push_list;
	std::vector<int> remove_list;

	OBJECT_TRANSINFO is_translation;
	PLAYER_TRANSINFO move_state;

	CPlayerMecha* pMecha;

	bool is_aim;

	DWORD cool_weapon1, cool_weapon2, cool_weapon3, cool_weapon4;

	int debug_gcount;

public:
	CPlayer();
	CPlayer(const CPlayer &t);
	CPlayer(int _id);
	~CPlayer();
	void operator=(const CPlayer &t);

	void ActivatePlayer(int p_id, int _index_id, float _x, float _z);
	void RemovePlayer();

	int getPlayerID() { return id; };
	int getIndexID() { return index_id; }
	int getMechaID() { return mecha_id; };
	float getHP() { return hp; }
	void healHP(float _hp);
	void setMecha(int id) { mecha_id = id; };
	void setReady(bool ready) { is_ready = ready; };
	bool is_Ready() { return is_ready; };
	int is_Translation() { return is_translation; };
	void ChangeMecha(int _mechaid);

	int getFireWeaponType() { return fire_weapon_type; }
	int getSelectWeapon() { return select_weapon; }
	void setFireWeaponType(int _t) { fire_weapon_type = _t; }
	void setSelectWeapon(int _w) { select_weapon = _w; is_weapon_change = true; }
	int isWeaponChange();	// -1 : 바뀌지 않음. >= 0 : 바뀐 무기 번호
	void resetFireWeaponType() { fire_weapon_type = 0; }
	CPlayerMecha* getpMecha() { return pMecha; }

	void clearViewList() { view_list.clear(); }

	bool canShoot(int _w);

	void PlayerPos(float _x, float _y, float _z);
	void Move();
	void reverseMove();
	void setLookandSpeed(float _x, float _z);
	void setLook(float _x, float _y, float _z);
	void setLookXZ(float _x, float _z) { look_x = _x; look_z = _z; }
	FLOAT3 getLook() { return FLOAT3(look_x, look_y, look_z); }
	void shiftPos(FLOAT3 _pos);
	void setSpeed(float _x, float _z);
	float getSpeedX() { return speed_x; }
	float getSpeedZ() { return speed_z; }

	void transRotate();
	void Jump();
	bool is_AimMode() { return is_aim; }
	void Aim() { is_aim = true; }
	void unAim() { is_aim = false; }
	bool Hit(float _damage);
	void Boost();
	void BoostOn() { move_state |= PLAYER_BOOST; }
	void BoostOff() { move_state &= ~PLAYER_BOOST; }
	void PlayerTranslate(float _x, float _y, float _z);
	void PlayerRotation();
	// 무브
	// 히트
	// 상태변경 등등 할거 너무많다
	int getMoveState() { return move_state; }
	void setMoveState(unsigned char _state) { move_state = _state; }

	void pushNearList(int _objectid) { push_list.insert(_objectid); }
	void pushViewList(int _objectid) { view_list.insert(_objectid); }
	unordered_set<int>* getPushList() { return &push_list; }
	unordered_set<int>* getViewList() { return &view_list; }
	vector<int>* getRemoveList() { return &remove_list; }
	void RelocateViewList();

	int getmc() { return debug_gcount; }
	void addmc() { debug_gcount++; }
};

#endif // !PLAYER_H