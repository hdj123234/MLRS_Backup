#pragma once
#ifndef ROOM_H
#define ROOM_H
#include "protocol.h"
#include "GameSpace.h"
#include "StaticObject.h"
#include <concurrent_vector.h>
#include <concurrent_queue.h>
#include "KeyBuf.h"


enum CHECK_STATE {
	CHECK_DEFAULT = 0,
	CHECK_WAIT = 1,
	CHECK_MOB_NUMBER = 2,
	CHECK_PLAYER_POS = 3,
	CHECK_PLAYER_XPOS_LEFT = 4,
	CHECK_PLAYER_ZPOS_UPPER = 5,
	CHECK_END = 100
};

struct LINECHECKER {
	CHECK_STATE state;
	unsigned int wait_value;
	FLOAT3 wait_pos;
};


enum EVENTKEYTYPE : int {
	EKT_NOTHIMG = 0,
	EKT_SCENE = 1,
	EKT_PRINTSCRIPT,
	EKT_PLAYER_RECOVER,
	EKT_PLAYER_HIT,
	EKT_PLAYER_DOWN,
	EKT_ENEMY_HIT,
	EKT_BOSS_HIT,
	EKT_OBJECT_HIT,
	EKT_KILL,
	EKT_SETTIMER,
	EKT_VICTORY = 30,
	EKT_DEFEATE
};
struct EVENTKEY {
	EVENTKEYTYPE type;
	int id;
	int ivalue;
	unsigned int time;
	float fvalue;
	EVENTKEY() {
		type = EKT_NOTHIMG;
		id = 0;
	}
	EVENTKEY(EVENTKEYTYPE _type, int _id) {
		type = _type;
		id = _id;
	}
	EVENTKEY(EVENTKEYTYPE _type, int _id, float _value) {
		type = _type;
		id = _id;
		fvalue = _value;
	}
	EVENTKEY(EVENTKEYTYPE _type, int _id, int _ivalue, unsigned int _time) {
		type = _type;
		id = _id;
		ivalue = _ivalue;
		time = _time;
	}
};

class CRoom
{
	bool is_use;		// 방이 사용중인지 나타내는 bool값

	int id;				// 방의 ID
	char title[ROOM_MAX_TITLE_LEN];		// 방제
	int maxplayer;		// 최대 플레이 인원
	int map_id;

	vector<CPlayer> connection_player;
	vector<CEnemy> vEnemy;
	vector<CEnemy_Boss> vBoss;
	vector<CMissile> vMissile;
	vector<CStaticObject> vObject;

	CGameSpace space;

	FLOAT3 initPlyaerPos;

	int state;			// 방의 상태
	unsigned current_time;

	concurrency::concurrent_queue<KEYBUF> keybuffer;
	concurrency::concurrent_queue<EVENTKEY> qEvent;


	CGamePlot* pplot;		// 게임 플롯 포인터
	int sceneID;			// 씬 ID
	int lineID;				// 라인 ID

	LINECHECKER linechecker;



	float spare_time;	// 60fps 유지를 위한 남은 시간 계산 변수

	int debug_FPScount;
	unsigned int debug_pre;
	unsigned int debug_cur;

	void RemoveRoom();
public:
	CRoom();
	CRoom(const CRoom&) {};
	CRoom(int _id);
	~CRoom();
	void InitRoom(int id);
	bool CreateRoom(int roomid, char* roomtitle, int maxp);

	void AddPlayer(int _id);
	void RemovePlayer(int id);

	void setMaxPlayer(int mp) { maxplayer = mp; }
	void setMecha(int playerid, int mecha);
	void setReady(int playerid, bool ready);
	void setState(int st) { state = st; }

	bool canEnter();
	int canStart();
	bool isUse() { return is_use; }
	void Deactivate() { is_use = false; }
	int getRoomID() { return id; }
	const char* getTitle() { return title; }
	int getMaxPlayer() { return maxplayer; }
	int getCurrentPlayer();
	int getState() { return state; }
	vector<CPlayer>& getPlayerIDVector() { return connection_player; };
	vector<CEnemy>& getEnemyIDVector() { return vEnemy; };
	vector<CEnemy_Boss>& getBossIDVector() { return vBoss; };
	vector<CMissile>& getMissileIDVector() { return vMissile; };
	vector<CStaticObject>& getObjectIDVector() { return vObject; };

	concurrency::concurrent_queue<EVENTKEY>& getEventQueue() { return qEvent; };

	void coercionStart(int _id);
	void putKeybuf(KEYBUF k);
	void Operation();


	// 60fps로 연산하기 위한 함수들
	unsigned int stackSpareTime();
	void zeroSpareTime() { spare_time = 0; };

};

#endif