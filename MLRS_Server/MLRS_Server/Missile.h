#pragma once
#include "GameObject.h"
#include "GameData\MissileType.h"
#include "GameData\AI\ObjectAI.h"
#include "UserDefine.h"



enum MS_State { MS_SPAWN, MS_THINK, MS_RISE, MS_MOVE_TO_TARGET, MS_READY_TO_ATTACK, MS_ATTACK, MS_HUNGRY, MS_BOOM };

enum OWNER_TYPE : int { OT_MASTERLESS, OT_PLAYER, OT_ENEMY, OT_BOSS };

class CMissile : public CGameObject
{
	CMissile* pthis;

	int type;

	OWNER_TYPE owner_type;
	int owner_id;

	FLOAT3 dir;
	float speed;

	int actcount;
	int state;

	int is_translate;

	float damage;

	CMissileType* pType;
	CObjectAI* pAI;

	float gravity;

	CGameObject* myTarget;
	

public:
	CMissile();
	~CMissile();

	void InitMissile(int _id);

	void putMissile(FLOAT3 _pos, FLOAT3 _dir, int _owner_id, int _type, OWNER_TYPE _owner_type, CGameObject* _target);
	void removeMissile();

	int getState() { return state; };
	int getActCount() { return actcount; };
	CGameObject* getTarget() { return myTarget; };
	int getTypeNumber() { return type; };
	CMissileType* getType() { return pType; };
	void createOver() { is_translate = OBJECT_NOSHIFT; };
	int isTranslate() { return is_translate; };
	float getAccel() { return pType->getAcceleration(); };
	float getSpeed() { return speed; };
	int getOwnerID() { return owner_id; }
	OWNER_TYPE getOwnerType() { return owner_type; }
	FLOAT3 getDirection() { return dir; };
	float getDamage() { return damage; }

	void setState(int _state) { state = _state; };
	void setActCount(int _ac) { actcount = _ac; };
	void resetActCount() { actcount = 0; };
	void increaseActCount() { actcount++; };
	void setPosY(float _y) { pos.getY() = _y; };
	void setSpeed(float _speed) { speed = _speed; };
	void setDirection(float _x, float _y, float _z) { dir.getX() = _x; dir.getY() = _y; dir.getZ() = _z; };
	void setDirection(FLOAT3 _dir) { dir.getX() = _dir.getX(); dir.getY() = _dir.getY(); dir.getZ() = _dir.getZ(); };
	void setDirectionY(float _y) { dir.getY() = _y; dir.Normalize(); };
	void addDirectionY(float _y) { dir.getY() += _y; dir.Normalize(); };
	void setTargetToPlayer(CGameObject* t) { myTarget = t; };
	void setTargetToEnemy(CGameObject* t) { myTarget = t; };

	void Accel();
	void DeAccel();
	void Crash();

	void Act(CGameSpace & space);
	void Move();
	void onGravity();
	float getGravity() { return gravity; };
	void setGravity(float _gravity) { gravity = _gravity; };
};

