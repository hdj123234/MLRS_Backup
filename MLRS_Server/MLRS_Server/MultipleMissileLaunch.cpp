#include "stdafx.h"
#include "MultipleMissileLaunch.h"


CMultipleMissileLaunch::CMultipleMissileLaunch()
{
	is_use = false;
	slot = 0;
	launch_frame = -1;
}


CMultipleMissileLaunch::~CMultipleMissileLaunch()
{
}


void CMultipleMissileLaunch::setMissileLaunch(int _slot, FLOAT3 _pos, FLOAT3 _look, FLOAT3 _up, FLOAT3 _right)
{
	is_use = true;
	slot = _slot;
	m_matrix = DirectX::XMMATRIX(
		_right.getX(), _right.getY(), _right.getZ(), 0,
		_up.getX(), _up.getY(), _up.getZ(), 0,
		_look.getX(), _look.getY(), _look.getZ(), 0,
		_pos.getX(), _pos.getY(), _pos.getZ(), 1);
	launch_frame = 0;
	look = _look;
}

// 발사할 타이밍이 아니면 -1 반환, 맞으면 포트번호 반환
int CMultipleMissileLaunch::getLaunchFrame()	
{
	if (launch_frame % 5 == 0)
		return launch_frame++ / 5;
	else {
		launch_frame++;
		return -1;
	}
}

void CMultipleMissileLaunch::endMissileLaunch()
{
	is_use = false;
	slot = 0;
	launch_frame = -1;
}
