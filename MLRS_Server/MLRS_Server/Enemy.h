#pragma once
#include "GameData\GameData.h"
#include "GameObject.h"

enum ES_State : BYTE { ES_SPAWN, ES_IDLE, ES_THINK, ES_TURN, ES_MOVE_TO_TARGET, ES_READY_TO_ATTACK, ES_ATTACK, ES_HUNGRY, ES_HIT, ES_BOOM, ES_SKILL, ES_DOWN
};

class CEnemy : public CGameObject
{
protected:
	int type;

	FLOAT3 direction;
	float speed;
	float gravity;

	float hp;

	CEnemyMecha *pMecha;		// 메카닉 타입
	CObjectAI *pAI;				// 보유할 AI 타입
	int actcount;
	int state;

	CGameObject* myTarget;

	int is_translate;

public:
	CEnemy* pthis;
	CEnemy();
	~CEnemy();

	void InitEnemy(int id);
	bool is_Use() { return is_use; };
	void putEnemy(int _type, float _pos_x, float _pos_y, float _pos_z, float _look_x, float _look_y, float _look_z);
	void putEnemy(int _type, FLOAT3 _pos, FLOAT3 _look);
	void removeEnemy();

	int getState() { return state; };
	int getActCount() { return actcount; };
	CGameObject* getTarget() { return myTarget; };
	int getTypeNumber() { return type; };
	CEnemyMecha* getType() { return pMecha; };
	void createOver() { is_translate = OBJECT_NOSHIFT; };
	int isTranslate() { return is_translate; };
	float getAccel() { return pMecha->getAcceleration(); };
	float getSpeed() { return speed; };
	FLOAT3 getDirection() { return direction; };
	void dirNormalize() { direction.Normalize(); }
	vector<FLOAT3>* getPoint() { return pMecha->getPoints(); };
	int getMissileType() { return pMecha->getMissileType(); }

	void setState(int _state) {	state = _state;	};
	void setActCount(int _ac) { actcount = _ac; };
	void setSpeed(float _speed) { speed = _speed; };
	void addSpeed(float _speed) { speed += _speed; }
	void resetActCount() { actcount = 0; };
	void increaseActCount() { actcount++; };
	void setPosY(float _y) { pos.setY(_y); };
	void setDirection(float _x, float _y, float _z) { direction.getX() = _x; direction.getY() = _y; direction.getZ() = _z; };
	void setDirection(FLOAT3 _dir) { direction.getX() = _dir.getX(); direction.getY() = _dir.getY(); direction.getZ() = _dir.getZ(); };
	void setDirectionY(float _y) { direction.getY() = _y; };
	void setGravity(float _gravity) { gravity = _gravity; };
	void addGravity(float _gravity) { gravity += _gravity; };
	void setTargetToPlayer(CGameObject* t) { myTarget = t; };

	void Accel();
	void DeAccel();


	void Act(CGameSpace &space);
	void Translate(float x, float y, float z) { pos.getX() += x;	pos.getY() += y; pos.getZ() = z; };
	void Move();
	bool Hit(float _damage);


};

