업데이트 2016-07-15

필요할 것 같은 함수 목록

	while()
	add(종류, 몇마리, 위치)
	checkmob(몹 마리수)
	checkhp(누구의 hp, hp)
	next()
	arrive(pos_x, pos_z, rad)


스크립트 확장자 : .psc (plot script)

스크립트 형식
#name rockyIsland
scene 0
	addsinglemob(0,100,100)
	wait(1000)
	addmultimob(0,10,100,100,10)
	break()
	.....
scene 1
	....
#end

주의사항
1. 모든 스크립트는 대소문자 구분을 하지 않습니다.
2. 모든 Scene의 끝에는 break(), repeatscene(), win(), defeat() 중에 하나로 끝나야 합니다.

구현된 함수 목록
addSingleMob(int type,float pos_x,float pos_z)
	: 하나의 적 기체를 필드에 추가합니다.
	  int type : 추가할 적 기체의 TypeID를 입력합니다.
	  float pos_x : 추가할 위치의 X 좌표를 입력합니다.
	  float pos_z : 추가할 위치의 Z 좌표를 입력합니다.
	ex) addsinglemob(0,100,100) : TypeID가 0인 기체(Zephyros)를 x : 100, z : 100 의 위치에 추가합니다.


addMultiMob(int type,int amount,float pos_x,float pos_z,int radius)
	: 여러개의 적 기체를 필드에 추가합니다.
	  int type : 추가할 적 기체의 TypeID를 입력합니다.
	  int amount : 추가할 적 기체의 개수를 입력합니다.
	  float pos_x : 추가할 위치의 X 좌표를 입력합니다.
	  float pos_z : 추가할 위치의 Z 좌표를 입력합니다.
	  float radius : pos_x, pos_z 좌표를 기준으로 입력받은 값 만큼 분산합니다.
	ex) addmultimob(0,3,100,100,200) : TypeID가 0인 기체(Zephyros)를 10개 추가합니다.
									   x : 100, z : 100 의 위치에 radius 만큼 무작위로 분산시킵니다.

addBossMob(int type,float pos_x,float pos_z)
	: 보스를 필드에 추가합니다.
	  int type : 추가할 보스 기체의 TypeID를 입력합니다.
	  float pos_x : 추가할 위치의 X 좌표를 입력합니다.
	  float pos_z : 추가할 위치의 Z 좌표를 입력합니다.
	ex) addBossMob(0,100,100) : TypeID가 0인 보스 기체(Delta)를 x : 100, z : 100 의 위치에 추가합니다.

setobjectpos(int type,float pos_x,float pos_z)
	: 오브젝트의 좌표를 해당 위치로 이동합니다.
	  int type : 움직일 오브젝트의 ID를 입력합니다.
	  float pos_x : 오브젝트가 이동할 위치의 X 좌표를 입력합니다.
	  float pos_z : 오브젝트가 이동할 위치의 Z 좌표를 입력합니다.
	ex) setobjectpos(0,100,100) : TypeID가 0인 오브젝트(Base Camp)를 x : 100, z : 100 의 위치로 이동합니다.

setobjectspeed(int type,float pos_x,float pos_y,float pos_z)	
	: 오브젝트의 속도를 해당 값으로 바꿉니다.
	  int type : 속도를 바꿀 오브젝트의 ID를 입력합니다.
	  float pos_x : 오브젝트의 X 속도값을 입력합니다.
	  float pos_y : 오브젝트의 Y 속도값을 입력합니다.
	  float pos_z : 오브젝트의 Z 속도값을 입력합니다.
	ex) setobjectspeed(0,8.0,2.1,10.5) : TypeID가 0인 오브젝트(Base Camp)의 속도를 (8, 2.1, 10.5)로 바꿉니다.

setinitpos(float pos_x,float pos_z)
	: 플레이어의 좌표 초기화 지점을 설정합니다. (방 입장, 사망 후 부활 등)
	  float pos_x : 플레이어의 좌표 초기화 지점의 X 좌표를 입력합니다.
	  float pos_z : 플레이어의 좌표 초기화 지점의 Z 좌표를 입력합니다.
	ex) setinitpos(100,100) : 플레이어의 위치 초기화 지점을 100, (지형 높이), 100 의 좌표로 지정합니다.

setplayerpos(float pos_x,float pos_z)
	: 플레이어를 해당 지점으로 옮깁니다. (모든 플레이어)
	  float pos_x : 플레이어가 옮겨질 X 좌표를 입력합니다.
	  float pos_z : 플레이어가 옮겨질 Z 좌표를 입력합니다.
	ex) setplayerpos(100,100) : 모든 플레이어를 100, (지형 높이), 100 의 위치로 옮깁니다.

printscript(int string_id)
	: 클라이언트에게 대사 출력 패킷을 보냅니다.
	  int string_id : 출력할 대사의 ID를 입력합니다.
	ex) printscript(3) : 방의 모든 클라이언트에게 3번 대사를 출력하라고 패킷을 보냅니다.

settimer(int time, int goScene)
	: 타이머를 세팅하고 타이머가 끝나면 설정한 Scene으로 넘어갑니다.
	  int time : 타이머의 시간을 입력합니다. (ms)
	  int goScene : 이동할 Scene을 입력합니다.
	ex) settimer(30000,5) : 30초짜리 타이머를 등록하고 다음줄로 넘어갑니다. 타이머가 끝나면 5번 씬으로 강제로 넘어갑니다.

repeatscene()
	: 현재 Scene의 첫번째 Line으로 되돌아갑니다.
	ex) repeatscene() : 현재 Scene의 첫번째 Line으로 되돌아가 반복합니다.

win()
	: 게임을 승리로 처리합니다.
	ex) win() : 게임을 승리로 처리합니다. 이 후 Scene이나 Line은 실행되지 않습니다.

defeat()
	: 게임을 패배로 처리합니다.
	ex) defeat() : 게임을 패배로 처리합니다. 이 후 Scene이나 Line은 실행되지 않습니다.

wait(int milliseconds) 
	: 인자로 받은 만큼 대기한 후 다음줄로 넘어갑니다.
	  int milliseconds : 대기할 시간(ms)을 입력합니다.
	ex) wait(3000) : 3초(3000ms) 만큼 대기하고 다음줄로 넘어갑니다.

checkMob(int under)
	: 적 기체의 수가 인자로 받은 수 이하로 줄어들때까지 대기합니다.
	  int under : 적 기체의 수를 입력합니다.
	ex) checkmob(2) : 적 기체의 수가 2 이하가 될 때까지 대기한 후 다음줄로 넘어갑니다.

break()
	: 현재 Scene의 끝을 알려줍니다.
	ex) break() : 현재 Scene이 끝나고 다음 씬의 첫줄로 넘어갑니다.
