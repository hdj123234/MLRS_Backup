#pragma once
#ifndef USERDEFINE_H
#define USERDEFINE_H

#define ROOM_MAX_CREAT 25		// 생성할 수 있는 방의 최대 개수


#define ROOM_MAX_TITLE_LEN (signed)36		// 방제 최대길이

#define MAXSIZE_ENEMY_NUMBER	200
#define MAXSIZE_MISSILE_NUMBER	200

#define MAP_SIZE_X		(float)10000
#define MAP_SIZE_Z		(float)10000


#define MULTIPLESEND_BUFFERSIZE 2048
#define PACKET_MAXSIZE			2000		

#define ENEMY_START_NUM			0
#define BOSS_START_NUM			(int)(ENEMY_START_NUM + 1024)
#define MISSILE_START_NUM		(int)(BOSS_START_NUM + 1024)
#define NULL_NUM				65535

// Move 패킷 구분
enum object_Type : BYTE { 
	OBJECT_PLAYER, 
	OBJECT_ENEMY,
	OBJECT_BOSS,
	OBJECT_MISSILE,
	OBJECT_STATIC 
};

enum object_translate : BYTE { 
	OBJECT_CREATE, 
	OBJECT_MOVE, 
	OBJECT_STOP,
	OBJECT_NOSHIFT, 
	OBJECT_REMOVE, 
	OBJECT_BOOST, 
	OBJECT_JUMP, 
	OBJECT_FALL, 
	OBJECT_FIRE, 
	OBJECT_ROTATE, 
	OBJECT_DEAD
};


#define PLAYER_COL 324 // 임시 충돌체크 값
#define PLAYER_OFFSET_Y 18

#define enemy_col 900

#endif // !USERDEFINE_H



// 추후 변경하거나 추가작업 해야하는 주석
// => !@#$%^