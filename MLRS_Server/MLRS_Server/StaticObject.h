#pragma once
#include "GameObject.h"
#include "UserDefine.h"

// 직육면체 충돌체크를 하기 위한 자료형
struct BOUNDING_BOX {
	float top, bottom, left, right, front, back;
	BOUNDING_BOX() {
		top = bottom = left = right = front = back = 0;
	}
	BOUNDING_BOX(float _top, float  _bottom, float _left, float _right, float _back, float _front) {
		top = _top; bottom = _bottom; left = _left; right = _right;
		front = _front; back = _back;
	}
	void operator=(const BOUNDING_BOX &b) {
		top = b.top; bottom = b.bottom; left = b.left; right = b.right;
		front = b.front; back = b.back;
	}
};

class CStaticObject : public CGameObject
{
	BOUNDING_BOX b_box;
	FLOAT3 center;
	float angle;
	float hp;
	float max_hp;

	BYTE is_trans;
	FLOAT3 speed;
	FLOAT3 look;
public:
	CStaticObject();
	~CStaticObject();
	CStaticObject(int _id);

	void initObject();
	void Activate() { is_use = true; }
	void Deactivate() { is_use = false; }

	FLOAT3 getPos() { return center+pos; };
	FLOAT3 getDirection() { return look; }
	float getHP() { return hp; }

	void setAngle(float _angle) { angle = _angle; }
	void setLook(FLOAT3 _look) { look = _look; }
	void setHP(float _hp) { hp = _hp; }
	void setMaxHP(float _maxhp) { max_hp = _maxhp; }
	void setCenter(FLOAT3 _center) { center = _center; }
	void setBoundingBox(BOUNDING_BOX _b) { b_box = _b; }
	bool Collision(FLOAT3 _f);

	BYTE isTranslate() { return is_trans; };
	void createOver() { if (is_trans == OBJECT_CREATE) is_trans = OBJECT_STOP; }
	void setSpeed(FLOAT3 f) { speed = f; }
	void setPosOnTerrain(int _terrain_id, float _pos_x, float _pos_z);
	void Act();

	bool Hit(float _damage);
};

