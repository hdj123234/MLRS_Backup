#include "stdafx.h"
#include "StaticObject.h"
#include "GameData\GameData.h"



CStaticObject::CStaticObject()
{
	objectType = OBJECT_STATIC;
	is_trans = OBJECT_CREATE;
}


CStaticObject::~CStaticObject()
{
}

CStaticObject::CStaticObject(int _id)
	: CGameObject(_id)
{
	objectType = OBJECT_STATIC;
	is_trans = OBJECT_CREATE;

}

void CStaticObject::initObject()
{
	angle = 0;
	speed = FLOAT3( 0,0,0 );
	look = FLOAT3(0, 0, 1);
	is_trans = OBJECT_CREATE;
	hp = max_hp;
}

bool CStaticObject::Collision(FLOAT3 _f)
{
	using namespace DirectX;
	FLOAT3 dir = _f- pos;
	FLOAT3 result = dir.getRotateY(look);
	/*

	
	FLOAT3 result; 
	float angleY = DirectX::XMConvertToRadians(angle);
	XMVECTOR vRotationAxis = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
	XMMATRIX mRotation = DirectX::XMMatrixRotationAxis(vRotationAxis, angleY);
	XMVECTOR v1 = XMLoadFloat3(&_f.data);
	XMVECTOR vResult = DirectX::XMVector3TransformCoord(v1, mRotation);
	XMStoreFloat3(&result.data, vResult);


	if (b_box.right + pos.getX() > result.getX() &&
		b_box.left + pos.getX() < result.getX() &&
		b_box.top + pos.getY() > result.getY() &&
		b_box.bottom + pos.getY() < result.getY() &&
		b_box.front + pos.getZ() > result.getZ() &&
		b_box.bottom + pos.getZ() < result.getZ()) 
		return true;		
	*/
	if (b_box.right > result.getX())
		if(b_box.left < result.getX())
		if(b_box.top > result.getY())
			if(b_box.bottom < result.getY())
				if(b_box.front > result.getZ())
					if(b_box.back < result.getZ())
						return true;
						
	return false;
}

void CStaticObject::Act()
{
	
	if (is_trans == OBJECT_CREATE && OBJECT_REMOVE) return;
	if (speed.getX() != 0 || speed.getY() != 0 || speed.getZ() != 0)
		is_trans = OBJECT_MOVE;
	else {
		if (is_trans == OBJECT_MOVE)
			is_trans = OBJECT_STOP;
		else
			is_trans = OBJECT_NOSHIFT;
		return;
	}
	
	pos += speed;

	using namespace DirectX;
	float angleY = XMConvertToRadians(0.25);
	XMVECTOR vRotationAxis = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
	XMMATRIX mRotation = XMMatrixRotationAxis(vRotationAxis, angleY);
	XMVECTOR v1 = XMLoadFloat3(&look.data);
	XMVECTOR vResult = XMVector3TransformCoord(v1, mRotation);
	XMStoreFloat3(&look.data, vResult);
	look.Normalize();
}

bool CStaticObject::Hit(float _damage)
{
	hp -= _damage;
	if (hp <= 0) {
		hp = 0;
	//	is_trans = OBJECT_REMOVE;
	//	state = MS_BOOM;
		return true;
	}
	return false;
}

void CStaticObject::setPosOnTerrain(int _terrain_id, float _pos_x, float _pos_z)
{
	pos.setX(_pos_x);
	pos.setZ(_pos_z);
	pos.setY(CGameData::getInstance().getMap(_terrain_id)->getHeight(_pos_x, _pos_z));

	is_trans = OBJECT_MOVE;
}
