#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class CAI_Reader
{
	std::vector<int> v;

	CAI_Reader(std::string _file);
	static std::string s_sFileName;

public:
	~CAI_Reader();

	static CAI_Reader& getInstance()
	{
		static CAI_Reader instance(s_sFileName);
		return instance;
	}

	static void setIniFileName(const std::string &rsFileName) { s_sFileName = rsFileName; }

	std::vector<int>::iterator getStart() { std::cout << *v.begin()<< std::endl; return v.begin(); }
	std::vector<int>::iterator getEnd() { return v.end()-1; }
	std::vector<int>::iterator getNext(std::vector<int>::iterator _iter);
};

