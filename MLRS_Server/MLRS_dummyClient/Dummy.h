#pragma once
#include "stdafx.h"
#include "..\MLRS_Server\protocol.h"
#include "AI_Reader.h"
#include <vector>

using namespace std;

struct OVERLAPPED_EX {
	WSAOVERLAPPED overlapped;
	int type;
	WSABUF wsabuf;
	char IOCPbuf[BUFSIZE];
	char PacketBuf[MAX_PACKET_SIZE];
	unsigned int prev_received;
	unsigned int curr_packet_size;
	OVERLAPPED_EX() {};
	OVERLAPPED_EX(const OVERLAPPED_EX& t) {
		overlapped = t.overlapped;
		type = t.type;
		wsabuf = t.wsabuf;
		memcpy(IOCPbuf, t.IOCPbuf, sizeof(IOCPbuf));
		memcpy(PacketBuf, t.PacketBuf, sizeof(PacketBuf));
		prev_received = t.prev_received;
		curr_packet_size = t.curr_packet_size;
	}
};

enum ACT_NUMBER
{
	ACT_MOVE_STOP,
	ACT_MOVE_FORWARD,
	ACT_MOVE_RIGHT,
	ACT_MOVE_LEFT,
	ACT_MOVE_BACKWARD,
	ACT_MOVE_STOP_W1,
	ACT_MOVE_FORWARD_W1,
	ACT_MOVE_RIGHT_W1,
	ACT_MOVE_LEFT_W1,
	ACT_MOVE_BACKWARD_W1,
	ACT_SELECT_WEAPON0,
	ACT_SELECT_WEAPON1,
	ACT_SELECT_WEAPON2,
	ACT_SELECT_WEAPON3,
	ACT_FIRE_WEAPON1,
	ACT_FIRE_WEAPON2,
	ACT_FIRE_WEAPON3
};

struct PLAYER
{
	float pos_x, pos_y, pos_z;
	float look_x, look_y, look_z;
	std::vector<int>::iterator state;
};

class CDummy
{
	int id;
	SOCKET sock;
	OVERLAPPED_EX overlapped;

	PLAYER player;

public:
	CDummy();
	~CDummy();
	CDummy(int _id);
	bool ConnectServer(HANDLE* _hIOCP, SOCKADDR_IN* _addr, string _ip, int _port);
	void setID(int _id) { id = _id; }
	int getID() { return id; }
	SOCKET* getSock() { return &sock; }
	OVERLAPPED_EX* getOverapped(){ return &overlapped; }

	ACT_NUMBER Act();

	void setPos(float _x, float _y, float _z);
	float getPosX() { return player.pos_x; }
	float getPosY() { return player.pos_y; }
	float getPosZ() { return player.pos_z; }
};

