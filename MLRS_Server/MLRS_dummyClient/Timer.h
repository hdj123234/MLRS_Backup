#pragma once
#include "stdafx.h"
using namespace std;
using namespace concurrency;


#define OPER_0 0
struct event_type {
	int id;
	unsigned int wakeup_time;
	int oper_type;

	event_type(int _id = 0, unsigned _wakeup_time = 0,
		int _oper_type = 0) {
		id = _id;
		wakeup_time = _wakeup_time;
		oper_type = _oper_type;
	}
	void operator=(const event_type& t)
	{
		oper_type = t.oper_type;
		id = t.id;
		wakeup_time = t.wakeup_time;
	}
};

class mycomparison
{
public:
	mycomparison() {}
	bool operator() (const event_type lhs, const event_type rhs) const
	{
		return (lhs.wakeup_time > rhs.wakeup_time);
	}
};

class CTimer
{
	priority_queue<event_type, vector<event_type>, mycomparison> timer_queue;
	mutex tq_lock;
	HANDLE hIOCP;

	HANDLE timeThread;
	unsigned int debug_time;

	static CTimer* pthis;
public:
	CTimer();
	~CTimer();

	static DWORD WINAPI Timer_Thread(LPVOID arg);
	void ProcessEvent(event_type);

	void TimerInit(HANDLE hiocp) {//concurrent_vector<CRoom>& r) {
		hIOCP = hiocp;
	}
	/////////////////////////////////
	void AddTimer(int _id, unsigned _wakeup_time, int _oper_type) {
		tq_lock.lock();
		timer_queue.push(event_type(_id, _wakeup_time, _oper_type));
		tq_lock.unlock();
	}
	void TimerStart();
};
