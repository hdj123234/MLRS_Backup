#include "Timer.h"
#include "IOCPDummyClient.h"

CTimer* CTimer::pthis = nullptr;

CTimer::CTimer()
{
	pthis = this;
	timeThread = CreateThread(NULL, 0, Timer_Thread, &hIOCP, 0, NULL);
	debug_time = GetTickCount();
}

CTimer::~CTimer()
{
	// 타이머 쓰레드 종료시켜야함
}

DWORD WINAPI CTimer::Timer_Thread(LPVOID arg)
{
	while (true) {
		Sleep(1);

		pthis->tq_lock.lock();
		if (pthis->timer_queue.empty()) {
			pthis->tq_lock.unlock();
			continue;
		}

		while (pthis->timer_queue.top().wakeup_time <= GetTickCount()) {
			event_type top_event = pthis->timer_queue.top();
			pthis->timer_queue.pop();
			pthis->tq_lock.unlock();
			pthis->ProcessEvent(top_event);

			pthis->tq_lock.lock();
			if (pthis->timer_queue.empty()) {
				break;
			}

		}
		pthis->tq_lock.unlock();

	}
}

void CTimer::ProcessEvent(event_type t)
{
	switch (t.oper_type) {
	case TYPE_ACT:
	{
			  OVERLAPPED_EX *event_over = new OVERLAPPED_EX;
			  event_over->curr_packet_size = t.wakeup_time;	// 변수 새로 생성 안하고 재활용 합니다.
			  event_over->type = TYPE_ACT;
			  PostQueuedCompletionStatus(hIOCP, 1, t.id,
				  reinterpret_cast<LPOVERLAPPED>(event_over));
			  break;
	}
	default:
		break;
	}

}