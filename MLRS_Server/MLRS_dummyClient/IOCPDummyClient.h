#pragma once
#include "stdafx.h"
#include "Timer.h"
#include "Dummy.h"
#include <string>
#include <map>

using namespace std;

enum DATA_TYPE { TYPE_INT, TYPE_FLOAT, TYPE_STRING };
class CDataType
{
	DATA_TYPE data_type;
	int* type_int;
	float* type_float;
	string* type_string;
public:
	CDataType(int* _type_int) { data_type = TYPE_INT; type_int = _type_int; }
	CDataType(float* _type_float) { data_type = TYPE_FLOAT; type_float = _type_float; }
	CDataType(string* _type_string) { data_type = TYPE_STRING; type_string = _type_string; }

	DATA_TYPE getType() { return data_type; }

	void setData(int _data) {
		if (data_type == TYPE_INT) *type_int = _data;
		else *type_int = static_cast<int>(_data);
	}
	void setData(float _data) {
		if (data_type == TYPE_FLOAT) *type_float = _data;
		else *type_float = static_cast<float>(_data);
	}
	void setData(string _data) {
		if (data_type == TYPE_STRING) *type_string = _data;
		else *type_string = _data;
	}
};

class CIOCPDummyClient
{
	WSADATA wsadata;
	HANDLE hIOCP;
	SYSTEM_INFO si;
	CTimer mytimer;

	string ip;
	int port;
	int dummy_num;

	int AI_delay;
	string AI_num_loc;

	SOCKADDR_IN addr;

	vector<CDummy> vDummy;

	vector<HANDLE> vthread;

	static CIOCPDummyClient* pthis;

	int getAIDelay() { return AI_delay; }

public:
	CIOCPDummyClient();
	~CIOCPDummyClient();

	int NetworkInit();
	int ConnectDummy();
	void start();

	static DWORD WINAPI WorkerThread(LPVOID arg);

	void ProcessPacket(char *packet, int id); // 받은 패킷 처리
	void SendPacket(int id, void *packet);
};

