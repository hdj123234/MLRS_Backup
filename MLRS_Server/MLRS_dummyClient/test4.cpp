#define _WINSOCK_DEPRECATED_NO_WARNINGS

#pragma comment(lib, "ws2_32")
#include <winsock2.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <fstream>
#include "..\MLRS_Server\protocol.h"
using namespace std;

#define SERVERIP   "127.0.0.1"
#define SERVERPORT 9000
#define CHAT_LIST_MAX 7

// 소켓 함수 오류 출력 후 종료
void err_quit(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);
}

// 소켓 함수 오류 출력
void err_display(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	printf("[%s] %s", msg, (char *)lpMsgBuf);
	LocalFree(lpMsgBuf);
}

// 사용자 정의 데이터 수신 함수
int recvn(SOCKET s, char *buf, int len, int flags)
{
	int received;
	char *ptr = buf;
	int left = len;

	while (left > 0) {
		received = recv(s, ptr, left, flags);
		if (received == SOCKET_ERROR)
			return SOCKET_ERROR;
		else if (received == 0)
			break;
		left -= received;
		ptr += received;
	}

	return (len - left);
}

struct PLAYER {
	bool is_use = false;
	int id = 0;
	char nick[MAX_NICK_SIZE + 1];
};
int index = 0;
int map[10][10] = { 0 };
SOCKET sock;
PLAYER lobby_players[MAX_USER];
int state = 0;

struct ROOM {
	bool is_use;
	int roomid;
	int curp;
	int maxp;
	int state;
	char title[ROOM_MAX_TITLE_LEN];
	ROOM() {
		is_use = false;
		roomid = -1;
		curp = 0;
		maxp = 0;
		state = 0;
	}
	ROOM(const ROOM &r) {
		is_use = r.is_use;
		roomid = r.roomid;
		curp = r.curp;
		maxp = r.maxp;
		state = r.state;
		strcpy_s(title, r.title);
	}
	void Init() {
		is_use = false;
		roomid = -1;
		curp = 0;
		maxp = 0;
		state = 0;
	}
};
struct PARTY {
	bool is_use;
	int id;
	char nick[MAX_NICK_SIZE + 1];
	int select_mecha;
	bool ready;
	PARTY() {
		is_use = false;
		id = -1;
		ZeroMemory(nick, MAX_NICK_SIZE + 1);
		select_mecha = 0;
		ready = 0;
	}
	PARTY(int pid, char* pnick) {
		is_use = true;
		id = pid;
		strcpy_s(nick, pnick);
		select_mecha = 0;
		ready = 0;
	}
};


ROOM roomlist[ROOM_MAX_CREAT];	// 로비에 보여지는 방들
struct MYROOM {
	ROOM room;
	vector<PARTY> party;
};
MYROOM myroom;
int roompage = 1;	// 내가 보고있는 룸 페이지
int myclientid = -1;
list<string> chatlist;


DWORD WINAPI Render(LPVOID arg)
{
	int packet_size = 0;
	int retval;
	char buf[BUFSIZE + 1];
	while (true) {
		ZeroMemory(buf, BUFSIZE + 1);
		// 데이터 받기
		retval = recvn(sock, buf, 1, 0);
		if (retval == SOCKET_ERROR) {
			err_display("recv()");
			break;
		}
		else if (retval == 0)
			break;

		packet_size = buf[0];
		ZeroMemory(buf, BUFSIZE + 1);
		retval = recvn(sock, buf, packet_size - 1, 0);
		if (retval == SOCKET_ERROR) {
			err_display("recv()");
			break;
		}
		else if (retval == 0)
			break;

		// 받은 데이터 출력

		cout << retval + 1 << " 크기의 패킷을 받았습니다." << endl;
		WORD pw;
		BYTE pb;
		int id, x, y;

		switch (buf[0]) {
		case SC_CLIENTID:
			memcpy(&pw, &buf[1], sizeof(pw));
			myclientid = pw;
			break;

		case SC_ROOMINFO:
			memcpy(&pw, &buf[1], sizeof(pw));
			id = pw;
			roomlist[id].roomid = id;
			roomlist[id].is_use = true;
			memcpy(&pb, &buf[3], sizeof(pb));
			roomlist[id].curp = pb;
			memcpy(&pb, &buf[4], sizeof(pb));
			roomlist[id].maxp = pb;
			memcpy(&pb, &buf[5], sizeof(pb));
			roomlist[id].state = pb;
			memcpy(roomlist[id].title, &buf[6], packet_size - SC_PACKET_ROOMINFO_TYPELEN);
			roomlist[id].title[packet_size - SC_PACKET_ROOMINFO_TYPELEN] = 0;
			break;

		case SC_USERINFO:
			memcpy(&pb, &buf[1], sizeof(pb));
			if (pb == SC_LOCATION_LOBBY) {
				memcpy(&pw, &buf[2], sizeof(pw));
				id = pw;
				lobby_players[id].id = id;
				lobby_players[id].is_use = true;
				memcpy(lobby_players[id].nick, &buf[4], packet_size - SC_PACKET_USERINFO_TYPELEN);
			}
			else if (pb == SC_LOCATION_ROOM) {
				char nicktemp[MAX_NICK_SIZE];
				memcpy(&pw, &buf[2], sizeof(pw));
				id = pw;
				memcpy(nicktemp, &buf[4], packet_size - SC_PACKET_USERINFO_TYPELEN);
				nicktemp[packet_size - SC_PACKET_USERINFO_TYPELEN] = 0;
				myroom.party.push_back(PARTY(id, nicktemp));
			}
			break;


		case SC_CHAT:
		{ {
				memcpy(&pw, &buf[1], sizeof(pw));
				id = pw;
				char message[MAX_STR_SIZE + 1] = { 0 };
				memcpy(message, &buf[3], packet_size - SC_PACKET_CHAT_TYPELEN);
				string s_chat;
				if (myroom.room.is_use == true) {
					// 방에 있으면 방에서 닉을 찾음
					for (auto &data : myroom.party) {
						if (data.id == id) {
							s_chat += data.nick;
							break;
						}
					}
				}
				else {
					// 로비면 로비유저 목록에서 닉을 찾음
					s_chat += lobby_players[id].nick;
				}
				s_chat += " : ";
				s_chat += message;
				chatlist.push_back(s_chat);
				if (chatlist.size() > CHAT_LIST_MAX) {
					chatlist.pop_front();
				}
				break;
			}}

		case SC_IS_ENTER_ROOM:
		{ {



				memcpy(&pb, &buf[1], sizeof(pb));
				if (pb != 0) {
					// 방 접속에 성공하면 상태 갱신
					state = 2;
					myroom.room.is_use = true;

					// 방 접속에 성공하면 로비 인원 초기화해둠
					for (int i = 0; i < MAX_USER; ++i) {
						lobby_players[i].is_use = false;
						lobby_players[i].id = -1;
						ZeroMemory(lobby_players[i].nick, sizeof(lobby_players[i].nick));
					}


					// 방의 정보를 받음
					memcpy(&pw, &buf[2], sizeof(pw));
					myroom.room.roomid = pw;
					memcpy(&pb, &buf[4], sizeof(pb));
					myroom.room.curp = pb;
					memcpy(&pb, &buf[5], sizeof(pb));
					myroom.room.maxp = pb;
					memcpy(&pb, &buf[6], sizeof(pb));
					myroom.room.state = pb;
					memcpy(myroom.room.title, &buf[7], packet_size - ENTERROOM_TYPELEN);


					chatlist.clear();
				}
				break;
			}}

		case SC_ENTER_USER:
		{ {
				// 사용 안할수도 있음.
				break;
			}}
		case SC_LEAVE_USER:
		{ {
				memcpy(&pw, &buf[1], sizeof(pw));
				id = pw;
				string s_chat;
				for (auto i = myroom.party.begin();
					i != myroom.party.end(); ++i) {
					if (i->id == id) {
						s_chat += i->nick;
					}
				}
				s_chat += "님이 나가셨습니다.";
				chatlist.push_back(s_chat);
				if (chatlist.size() > CHAT_LIST_MAX) {
					chatlist.pop_front();
				}
				for (auto i = myroom.party.begin();
					i != myroom.party.end(); ++i) {
					if (i->id == id) {
						myroom.party.erase(i);
						break;
					}
				}
				break;
			}}
		case SC_CHANGE_MECHA:
		{ {
				memcpy(&pw, &buf[1], sizeof(pw));
				id = pw;
				for (auto &data : myroom.party) {
					if (data.id == id) {
						memcpy(&pb, &buf[3], sizeof(pb));
						data.select_mecha = pb;
						break;
					}
				}
				break;
			}}

		case SC_READY:
		{ {
				memcpy(&pw, &buf[1], sizeof(pw));
				id = pw;
				for (auto &data : myroom.party) {
					if (data.id == id) {
						memcpy(&pb, &buf[3], sizeof(pb));
						data.ready = pb != 0;
						break;
					}
				}
				break;
			}}

		case SC_GAME_START:
		{ {
				int result;
				memcpy(&pb, &buf[1], sizeof(pb));
				result = pb;
				switch (result) {
				case SC_START_RESULT_OK:
					cout << "게임을 시작합니다!" << endl;
					break;

				case SC_START_RESULT_ERROR:
					cout << "게임을 시작할 수 없습니다." << endl;
					break;

				case SC_START_RESULT_NOTREADY:
					cout << "모든 유저가 준비되지 않았습니다." << endl;
					break;
				}

				break;
			}}
		case SC_POS:
			memcpy(&pw, &buf[1], sizeof(pw));
			id = pw;
			memcpy(&pb, &buf[3], sizeof(pb));
			x = pb;
			memcpy(&pb, &buf[4], sizeof(pb));
			y = pb;
			/*
			players[id].x = x;
			players[id].y = y;
			*/
			break;

		case SC_PUT_PLAYER:
			memcpy(&pw, &buf[1], sizeof(pw));
			id = pw;
			memcpy(&pb, &buf[3], sizeof(pb));
			x = pb;
			memcpy(&pb, &buf[4], sizeof(pb));
			y = pb;

			//			printf("[TCP 클라이언트] 플레이어 생성 데이터 %d바이트를 받았습니다.\n", retval);
			//			cout << "[받은 데이터] : type : " << (int)buf[0] << ",  id : " << id <<
			//				",  좌표 : " <<	x << ", " << y << endl;

			/*
			players[id].id = id;
			players[id].x = x;
			players[id].y = y;
			players[id].is_use = true;
			*/
			// 플레이어 생성

			break;

		case SC_REMOVE_PLAYER:
			memcpy(&pw, &buf[1], sizeof(pw));
			id = pw;
			/*
			players[id].x = 0;
			players[id].y = 0;
			players[id].is_use = false;
			*/
			break;

		case SC_SET_NICK:
			/*
			memcpy(&pw, &buf[1], sizeof(pw));
			id = pw;
			memcpy(players[id].nick, &buf[3], MAX_NICK_SIZE);
			players[id].nick[MAX_NICK_SIZE] = 0;
			*/
			break;
		}

		switch (state) {
		case 0:

			break;
		case 1:
		{ {
				system("cls");
				for (int i = 6 * (roompage - 1);
					i < 6 * roompage; ++i) {
					if (roomlist[i].is_use == true || i >= ROOM_MAX_CREAT) {
						if (roomlist[i].roomid < 10)
							cout << " ";
						cout << roomlist[i].roomid << ". " <<
							roomlist[i].title;
						int title_len = strlen(roomlist[i].title);
						for (int j = 0; j < (ROOM_MAX_TITLE_LEN - title_len); ++j) {
							cout << " ";
						}
						cout << " " <<
							roomlist[i].curp << " / " <<
							roomlist[i].maxp << "   " <<
							roomlist[i].state << endl;
					}
					else
						cout << "- - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
				}

				cout << endl << "                ◁      " << roompage <<
					"      ▷               " << endl;
				int cp = 0;
				for (int i = 0; i < MAX_USER; ++i) {
					if (lobby_players[i].is_use == true)
						cp++;
				}
				cout << "현재 로비에 접속중인 플레이어 : " << cp << "명" << endl;
				for (auto &i : chatlist) {
					cout << i << endl;
				}
				break;
			}}
		case 2:
		{ {
				system("cls");

				int np = 0;
				cout << "방제 : " << myroom.room.title;
				int title_len = strlen(myroom.room.title);
				for (int j = 0; j < (ROOM_MAX_TITLE_LEN - title_len); ++j) {
					cout << " ";
				}
				cout << endl << "인원 : " << myroom.party.size() <<
					" / " << myroom.room.maxp << "        방 상태 : ";
				if (myroom.room.state == 0)
					cout << "대기중" << endl;
				if (myroom.room.state == 1)
					cout << "게임중" << endl;
				cout << endl << "   닉네임" << "                 " <<
					"메카닉" << "    " << "준비" << endl;
				int maxcount = 0;
				for (auto &data : myroom.party) {
					maxcount++;
					cout << data.id << ". " << data.nick;
					int nick_len = strlen(data.nick);
					for (int j = 0; j < (MAX_NICK_SIZE - nick_len); ++j) {
						cout << " ";
					}
					cout << "  " << data.select_mecha << "      ";
					if (data.ready) {
						cout << "Ready";
					}
					cout << endl;
				}
				for (int i = maxcount; i < myroom.room.maxp; ++i) {
					cout << "- - - - - - - - - - - - - - - - - - - - -" << endl;
				}
				for (auto &i : chatlist) {
					cout << i << endl;
				}
				break;
			}}
		case 3:
			/*
			for (int i = 0; i < MAX_MAP_HEIGHT; i++) {
			for (int j = 0; j < MAX_MAP_WITDH; j++) {
			map[i][j] = -1;
			}
			}
			for (int i = 0; i < MAX_USER; ++i) {

			if (players[i].is_use == true) {
			map[players[i].y][players[i].x] = players[i].id;
			}
			}
			for (int i = 0; i < MAX_MAP_HEIGHT; i++) {
			for (int j = 0; j < MAX_MAP_WITDH; j++) {
			if (map[i][j] == -1)
			printf("■");
			else
			printf(" %d", map[i][j]);
			}
			printf("\n");
			}
			cout << endl << endl;
			for (int i = 0; i < MAX_USER; ++i) {
			if (players[i].is_use == true)
			cout << i << "번 플레이어 닉네임 : " << players[i].nick << endl;
			}
			*/
			break;

		default:
			break;
		}

	}
	return 0;
}


int main(int argc, char *argv[])
{
	int retval;
	system("mode con cols=80 lines=19");
	//SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY);


	// 텍스트파일에서 서버 IP 읽어오기
	char serverIP[20] = SERVERIP;
	ifstream ipf("ip.txt", ios::binary);
	if (!ipf.fail()) {
		ZeroMemory(serverIP, sizeof(serverIP));
		ipf.seekg(0, ios::end);
		unsigned int nSize = static_cast<unsigned int>(ipf.tellg());
		ipf.seekg(0, ios::beg);

		//파일 정보 복사
		auto iter = std::istreambuf_iterator<char>(ipf.rdbuf());
		for (unsigned int i = 0; i < 20; ++i)
		{
			serverIP[i] = *iter;   //char를 unsigned char로 변환하여 대입
			++iter;
			if (i >= nSize - 1) {
				serverIP[i + 1] = 0;
				break;
			}
		}
	}
	ipf.close();



	// 윈속 초기화
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
		return 1;

	// socket()
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET) err_quit("socket()");

	// connect()
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr(serverIP);
	serveraddr.sin_port = htons(SERVERPORT);
	retval = connect(sock, (SOCKADDR *)&serveraddr, sizeof(serveraddr));
	if (retval == SOCKET_ERROR) err_quit("connect()");

	// 데이터 통신에 사용할 변수
	char buf[BUFSIZE + 1];
	int len;
	char keybuf[BUFSIZE + 1];

	// 닉네임 입력
	state = 0;
	cout << "사용할 닉네임을 입력해주세요." << endl <<
		"닉네임 : ";
	cin.getline(keybuf, BUFSIZE);
	cin.clear();

	// 수신 쓰레드 생성
	HANDLE hThread;
	hThread = CreateThread(NULL, 0, Render, 0, 0, NULL);

	state = 1;
	len = strlen(keybuf);
	if (len > MAX_NICK_SIZE)
		len = MAX_NICK_SIZE;
	memcpy(&buf[2], keybuf, len);
	buf[1] = CS_SET_NICK;
	buf[0] = len + 2;
	retval = send(sock, buf, len + 2, 0);
	if (retval == SOCKET_ERROR) {
		err_display("send()");
	}

	buf[0] = 3;
	buf[1] = CS_ROOM_PAGE;
	buf[2] = 1;
	retval = send(sock, buf, 3, 0);
	if (retval == SOCKET_ERROR) {
		err_display("send()");
	}

	// 서버와 데이터 통신
	while (1) {
		// 데이터 입력
		//		printf("\n[보낼 데이터] ");

		cin.getline(keybuf, BUFSIZE);
		cin.clear();
		BYTE type;
		if (keybuf[0] != 126)
			type = keybuf[0] - '0';
		else
			type = keybuf[0];
		len = strlen(keybuf);
		if (strlen(keybuf) == 0)
			break;

		BYTE size;
		switch (type)
		{
		case CS_SET_NICK:
			if (len > MAX_NICK_SIZE)
				len = MAX_NICK_SIZE;
			memcpy(&buf[1], keybuf, len);
			size = len + 1;
			break;

		case CS_CHAT:
			if (len > MAX_STR_SIZE)
				len = MAX_STR_SIZE;
			memcpy(&buf[1], keybuf, len);
			size = len + 1;
			break;

		case CS_ROOM_PAGE:
			memcpy(&buf[1], keybuf, len);
			buf[2] -= '0';
			ZeroMemory(roomlist, sizeof(roomlist));
			roompage = buf[2];
			size = len + 1;
			break;

		case CS_CREATE_ROOM:
			if (len > ROOM_MAX_TITLE_LEN)
				len = ROOM_MAX_TITLE_LEN;
			memcpy(&buf[1], keybuf, len);
			buf[2] -= '0';
			size = len + 1;
			break;

		case CS_ENTER_ROOM:
		{ {
				WORD pw = keybuf[1] - '0';
				cs_packet_enter_room enter_packet;
				enter_packet.size = sizeof(enter_packet);
				enter_packet.type = CS_ENTER_ROOM;
				enter_packet.roomid = pw;
				size = len + 2;
				memcpy(buf, &enter_packet, sizeof(enter_packet));
				break;
			}}
		case CS_LEAVE_ROOM:
			memcpy(&buf[1], keybuf, len);
			buf[2] -= '0';
			size = len + 1;
			break;

		case CS_CHANGE_MECHA:
			memcpy(&buf[1], keybuf, len);
			buf[2] -= '0';
			size = len + 1;
			break;
		case CS_READY:
			memcpy(&buf[1], keybuf, len);
			buf[2] -= '0';
			size = len + 1;
			break;

		case CS_DEBUG_ASSIST:
			size = len + 1;
			break;

		default:
			break;
		}

		buf[0] = size;
		buf[1] = type;


		// 데이터 보내기
		retval = send(sock, buf, buf[0], 0);
		if (retval == SOCKET_ERROR) {
			err_display("send()");
			break;
		}
		if (buf[1] == CS_LEAVE_ROOM)
		{
			state = 1;
			buf[0] = 3;
			buf[1] = CS_ROOM_PAGE;
			buf[2] = 1;
			chatlist.clear();
			retval = send(sock, buf, 3, 0);
			if (retval == SOCKET_ERROR) {
				err_display("send()");
				break;
			}
			myroom.party.clear();
			myroom.room.Init();
		}
		printf("[TCP 클라이언트] %d바이트를 보냈습니다.\n", retval);




	}

	// closesocket()
	closesocket(sock);

	// 윈속 종료
	WSACleanup();
	return 0;
}

/*
클라이언트 주의사항

1. 현재 패킷 구조체를 사용하지 않고 버퍼에 직접 데이터를 쌓아서 보내고 있음.
패킷 구조체를 사용할 것

2. 몇몇 패킷은 보낸 후 추가로 보내줘야 할 데이터가 있음.
1. CS_LEAVE_ROOM
- 나갔다고 알려준 후 로비로 나가야 함
- 로비로 나갔으니 CS_ROOM_PAGE를 보내 로비 룸리스트를 갱신해야함
- 채팅 리스트를 초기화해야함


*/