#pragma once
#ifndef STDAFX_H
#define STDAFX_H
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma comment(lib, "ws2_32")

#include <vector>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <thread>
#include <iostream>
#include <fstream>
#include <iterator>
#include <mutex>
#include <string>
#include <concurrent_queue.h>
#include <concurrent_vector.h>
#include <queue>
#include <cmath>
#include <DirectXMath.h>



#define TYPE_RECV 0
#define TYPE_SEND 1
#define TYPE_ACT	2
#endif 