#include <iostream>
#include "IOCPDummyClient.h"
#include <vector>
using namespace std;


int main()
{
	CIOCPDummyClient DummyClient;

	if (DummyClient.NetworkInit())
		return 0;
	if (DummyClient.ConnectDummy())
		return 0;

	DummyClient.start();

	int d;
	cin >> d;
	return 0;
}