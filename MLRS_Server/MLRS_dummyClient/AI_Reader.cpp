#include "AI_Reader.h"
std::string CAI_Reader::s_sFileName = "AI_num.txt";

CAI_Reader::CAI_Reader(std::string _file)
{
	using namespace std;
	ifstream f;
	f.open(_file, ios::binary);
	if (!f.is_open()) {
		cout << "더미 AI 파일을 찾을 수 없습니다. (" << _file << ")" << endl;
		return;
	}

	while (1)
	{
		int t = -1;
		f >> t;
		if (t == -1)
			break;
		v.push_back(t);
	}
	for (auto &i : v)
		cout << i << " ";
}


CAI_Reader::~CAI_Reader()
{
}

std::vector<int>::iterator CAI_Reader::getNext(std::vector<int>::iterator _iter)
{
	_iter++;
	if (_iter == v.end())
		return v.begin();
	return _iter;
}
