#include "IOCPDummyClient.h"
#include "..\MLRS_Server\protocol.h"
using namespace std;

CIOCPDummyClient* CIOCPDummyClient::pthis = nullptr;


CIOCPDummyClient::CIOCPDummyClient()
{
	pthis = this;

	map<string, map<string, CDataType>*> upper_class;
	map<string, CDataType> lower_class_Network;
	upper_class.insert(make_pair("Network", &lower_class_Network));
	lower_class_Network.insert(make_pair("IP", CDataType(&ip))); ip = "127.0.0.1";
	lower_class_Network.insert(make_pair("Port", CDataType(&port))); port = 9000;

	map<string, CDataType> lower_class_Dummy;
	upper_class.insert(make_pair("Dummy", &lower_class_Dummy));
	lower_class_Dummy.insert(make_pair("Dummy_Number", CDataType(&dummy_num))); dummy_num = 100;

	map<string, CDataType> lower_class_AI;
	upper_class.insert(make_pair("AI", &lower_class_AI));
	lower_class_AI.insert(make_pair("Delay", CDataType(&AI_delay))); AI_delay = 1000;
	lower_class_AI.insert(make_pair("Location", CDataType(&AI_num_loc))); AI_num_loc = "AI_num.txt";

	ifstream initfile;
	initfile.open("Dummy.ini", ios::binary);
	if (!initfile.is_open()) {
		cout << "더미 클라이언트 초기화 파일을 찾을 수 없습니다. (Dummy.ini)" << endl;
		return;
	}

	string ts;
	string skipequal;
	string svalue;
	getline(initfile, ts, '[');
	while (1) {
		getline(initfile, ts, ']');

		if (ts.empty())
			break;

		string lower_name;
		auto lower = upper_class.find(ts);

		while (1) {
			ts.clear();
			initfile >> ts;
			if (ts.empty())
				break;
			else if (*ts.begin() == '[') {
				size_t back = ts.length();
				initfile.seekg(0 - back, initfile.cur);
				getline(initfile, ts, '[');
				break;
			}

			auto pvalue = lower->second->find(ts);

			initfile >> skipequal;
			initfile >> svalue;

			switch (pvalue->second.getType()) {
			case DATA_TYPE::TYPE_INT:
				pvalue->second.setData(stoi(svalue)); break;
			case DATA_TYPE::TYPE_FLOAT:
				pvalue->second.setData(stof(svalue)); break;
			case DATA_TYPE::TYPE_STRING:
				pvalue->second.setData(svalue); break;
			default:
				break;
			}
		}
	}
	initfile.close();

	CAI_Reader::setIniFileName(AI_num_loc);
	CAI_Reader::getInstance();
}


CIOCPDummyClient::~CIOCPDummyClient()
{
	WSACleanup();
}

int CIOCPDummyClient::NetworkInit() {
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(ip.c_str());
	addr.sin_port = htons(port);

	WSAStartup(MAKEWORD(2, 2), &wsadata);
	GetSystemInfo(&si);

	hIOCP = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	if (hIOCP == NULL) {
		cout << "IOCP 핸들 생성에 실패했습니다." << endl;
		return -1;
	}

	for (int i = 0; i < (int)si.dwNumberOfProcessors * 2; i++) {
		vthread.push_back(CreateThread(NULL, 0, WorkerThread, hIOCP, 0, NULL));
		if (*(vthread.end() - 1) == NULL) {
			cout << "WorkerThread 생성에 실패했습니다." << endl;
			return -1;
		}
	}
	cout << "WorkerThread (" <<
		(int)si.dwNumberOfProcessors * 2 << "개) 생성 완료" << endl;

	// 타이머에 IOCP 핸들 넘겨주기
	mytimer.TimerInit(hIOCP);

	return 0;
}

int CIOCPDummyClient::ConnectDummy()
{
	for (int i = 0; i < dummy_num; ++i)
		vDummy.push_back(CDummy(i));

	int id = 0;
	DWORD flags = 0;
	for (auto &i : vDummy) {
		if (!i.ConnectServer(&hIOCP, &addr, ip, port))
			return -1;
		/*
		CreateIoCompletionPort((HANDLE)i.getSock(), hIOCP, id++, 0);
		int retval = WSARecv(*i.getSock(), &(i.getOverapped()->wsabuf),
			1, NULL, &flags, &i.getOverapped()->overlapped, NULL);
		if (retval == SOCKET_ERROR) {
			return -1;
		}
		*/
	}
	return 0;
}

void CIOCPDummyClient::ProcessPacket(char *packet, int id) {
	int len = packet[0];
	//	cout << "데이터 받았습니다. 사이즈 : " << (int)packet[0] << endl;
	switch (packet[1]) {
	case SC_CLIENTID: {
			WORD t;
			memcpy(&t, &packet[2], 2);
			cout << "할당된 ID : " << t << endl;
			vDummy[id].setID(static_cast<int>(t));
			break;
		}
	case SC_PLAYER_MOVE: {
		WORD t;
		memcpy(&t, &packet[2], 2);
		float pos[3];
		memcpy(pos, &packet[6], sizeof(pos));
		for (auto &i : vDummy) {
			if (i.getID() == t) {
				i.setPos(pos[0], pos[1], pos[2]);
				cout << id << " : " << pos[0] << ", " << pos[1] << ", " << pos[2] << endl;
				break;
			}
		}
		break;
	}
	default:
		//		cout << "Unknown Packet!" << endl;
		break;
	}


}

void CIOCPDummyClient::SendPacket(int id, void *packet)
{
	int packet_size = reinterpret_cast<unsigned char *>(packet)[0];

	OVERLAPPED_EX *send_over = new OVERLAPPED_EX;
	ZeroMemory(send_over, sizeof(OVERLAPPED_EX));
	send_over->type = TYPE_SEND;
	send_over->wsabuf.buf = send_over->IOCPbuf;
	send_over->wsabuf.len = packet_size;
	unsigned long io_size;

	memcpy(send_over->IOCPbuf, packet, packet_size);

	WSASend(*vDummy[id].getSock(), &send_over->wsabuf, 1,
		&io_size, NULL, &send_over->overlapped, NULL);
}

void CIOCPDummyClient::start()
{
	int index = 0;
	for (auto &i : vDummy)
	{
		BYTE temp[3];
		temp[0] = 3;
		temp[1] = 126;
		temp[2] = 24;//i.getID() / 4;
		SendPacket(index, temp);
		cout << index << "번 아이디 " << (int)temp[2] << "번방 입장 시도" << endl;
		Sleep(300);
		
		mytimer.AddTimer(index, 1000, TYPE_ACT);
		index++;
	}

}


DWORD WINAPI CIOCPDummyClient::WorkerThread(LPVOID arg)
{
	int retval;
	HANDLE hcp = (HANDLE)arg;

	while (1) {
		//비동기 입출력 완료 기다리기
		unsigned long io_size;
		unsigned long long key;
		OVERLAPPED_EX *over_ex;

		retval = GetQueuedCompletionStatus(pthis->hIOCP, &io_size, &key,
			reinterpret_cast<LPOVERLAPPED *>(&over_ex), INFINITE);

		// 접속 종료시
		if (io_size == 0) {
			sc_packet_remove_player rem_packet;
			rem_packet.id = (WORD)key;

			SOCKADDR_IN clientaddr;
			int addrlen = sizeof(clientaddr);
			getpeername(*pthis->vDummy[key].getSock(), (SOCKADDR *)&clientaddr, &addrlen);
			printf("[TCP 서버] 클라이언트 종료 : IP 주소= %s, 포트 번호 = %d\n",
				inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));
			closesocket(*pthis->vDummy[key].getSock());

			continue;
		}

		switch (over_ex->type)
		{
		case TYPE_RECV:
		{
			// RECV
			int rest_size = io_size;
			char *buf = over_ex->IOCPbuf;
			int packet_size = over_ex->curr_packet_size;
			while (0 < rest_size) {
				if (0 == packet_size) packet_size = buf[0];
				int remain = packet_size - over_ex->prev_received;
				if (remain > rest_size) {
					// 패킷 만들기에는 부족하다.
					memcpy(over_ex->PacketBuf + over_ex->prev_received,
						buf, rest_size);
					over_ex->prev_received += rest_size;
					break;
				}
				else {
					// 패킷을 만들 수 있다.
					memcpy(over_ex->PacketBuf + over_ex->prev_received,
						buf, remain);
					pthis->ProcessPacket(over_ex->PacketBuf, static_cast<int>(key));
					rest_size -= remain;
					packet_size = 0;
					over_ex->prev_received = 0;
					buf += remain;
				}
			}
			over_ex->curr_packet_size = packet_size;
			unsigned long recv_flag = 0;
			WSARecv(*pthis->vDummy[key].getSock(),
				&over_ex->wsabuf, 1, NULL,
				&recv_flag, &over_ex->overlapped, NULL);

			break;
		}
		case TYPE_SEND:
		{
			delete over_ex;
			break;
			// SEND
		}
		case TYPE_ACT:
		{
			auto ai = pthis->vDummy[key].Act();

			switch (ai)
			{
			case ACT_MOVE_STOP: {
				cs_packet_move packet;
				packet.move = 0;
				packet.direction_x = 0;
				packet.direction_z = 1;
				pthis->SendPacket(static_cast<int>(key), &packet);
			//	pthis->SendPacket(static_cast<int>(pthis->vDummy[key].getID()), &packet);
				break;
			}
			case ACT_MOVE_FORWARD: {
				cs_packet_move packet;
				packet.move = MT_MOVE;
				packet.direction_x = 0;
				packet.direction_z = 1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_MOVE_RIGHT: {
				cs_packet_move packet;
				packet.move = MT_MOVE;
				packet.direction_x = 1;
				packet.direction_z = 0;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_MOVE_LEFT: {
				cs_packet_move packet;
				packet.move = MT_MOVE;
				packet.direction_x = -1;
				packet.direction_z = 0;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_MOVE_BACKWARD: {
				cs_packet_move packet;
				packet.move = MT_MOVE;
				packet.direction_x = 0;
				packet.direction_z = -1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_MOVE_STOP_W1: {
				cs_packet_aim_move1 packet;
				packet.move = 0;
				packet.direction_x = 0;
				packet.direction_z = -1;
				packet.look_x = 0;
				packet.arm_y = 1;
				packet.look_z = 1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_MOVE_FORWARD_W1: {
				cs_packet_aim_move1 packet;
				packet.move = MT_MOVE;
				packet.direction_x = 0;
				packet.direction_z = 1;
				packet.look_x = 0;
				packet.arm_y = 0;
				packet.look_z = 1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_MOVE_RIGHT_W1: {
				cs_packet_aim_move1 packet;
				packet.move = MT_MOVE;
				packet.direction_x = 1;
				packet.direction_z = 0;
				packet.look_x = 0;
				packet.arm_y = 0;
				packet.look_z = 1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_MOVE_LEFT_W1: {
				cs_packet_aim_move1 packet;
				packet.move = MT_MOVE;
				packet.direction_x = -1;
				packet.direction_z = 0;
				packet.look_x = 0;
				packet.arm_y = 0;
				packet.look_z = 1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_MOVE_BACKWARD_W1: {
				cs_packet_aim_move1 packet;
				packet.move = MT_MOVE;
				packet.direction_x = 0;
				packet.direction_z = -1;
				packet.look_x = 0;
				packet.arm_y = 0;
				packet.look_z = 1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_SELECT_WEAPON0: {
				cs_packet_change_weapon packet;
				packet.w_type = -1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_SELECT_WEAPON1: {
				cs_packet_change_weapon packet;
				packet.w_type = 0;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_SELECT_WEAPON2: {
				cs_packet_change_weapon packet;
				packet.w_type = 1;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_SELECT_WEAPON3: {
				cs_packet_change_weapon packet;
				packet.w_type = 2;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_FIRE_WEAPON1: {
				cs_packet_shoot packet;
				packet.m_type = 0;
				packet.direction_x = 0;
				packet.direction_y = 0;
				packet.direction_z = 1;
				packet.point_x = pthis->vDummy[static_cast<unsigned int>(key)].getPosX()+ 7.59872;
				packet.point_y = pthis->vDummy[static_cast<unsigned int>(key)].getPosY()+ 27.4032;
				packet.point_z = pthis->vDummy[static_cast<unsigned int>(key)].getPosZ() + 13.97;
				pthis->SendPacket(static_cast<int>(key), &packet);

				break;
			}
			case ACT_FIRE_WEAPON2: {
				cs_packet_shoot_multiple packet;
				packet.pos[0] = pthis->vDummy[static_cast<unsigned int>(key)].getPosX() - 3.34561;
				packet.pos[1] = pthis->vDummy[static_cast<unsigned int>(key)].getPosY() + 34.3524;
				packet.pos[2] = pthis->vDummy[static_cast<unsigned int>(key)].getPosZ() + 16.7393;
				packet.look[0] = 0;
				packet.look[1] = 0.70711f;
				packet.look[2] = 0.70711f;
				packet.up[0] = 0;
				packet.up[1] = 0.70711f;
				packet.up[2] = -0.70711f;
				packet.right[0] = 1;
				packet.right[1] = 0;
				packet.right[2] = 0;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			case ACT_FIRE_WEAPON3: {
				cs_packet_shoot_lockon packet;
				packet.lock_on[0] = 0;
				packet.lock_on[1] = 1;
				packet.lock_on[2] = 2;
				packet.lock_on[3] = 3;
				packet.lock_on[4] = 4;
				packet.lock_on[5] = 5;
				packet.lock_on[6] = 6;
				packet.lock_on[7] = 7;
				packet.lock_on[8] = 8;
				packet.lock_on[9] = 9;
				packet.lock_on[10] = 65535;
				pthis->SendPacket(static_cast<int>(key), &packet);
				break;
			}
			default: {
				break;
			}
			}


			cout << static_cast<int>(key) << " : Act(" << static_cast<int>(ai) << ")" << endl;
			pthis->mytimer.AddTimer(static_cast<int>(key), GetTickCount() + pthis->getAIDelay(), TYPE_ACT);
			delete over_ex;
			break;
		}
		default:
		{
			break;
		}
		}
	}
	return 0;
}


void err_quit(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBoxA(NULL, (LPCSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);
}
void err_display(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	printf("[%s] %s", msg, (char *)lpMsgBuf);
	LocalFree(lpMsgBuf);
};