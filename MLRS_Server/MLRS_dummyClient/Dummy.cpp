#include "Dummy.h"
using namespace std;


CDummy::CDummy()
{
	ZeroMemory(&overlapped, sizeof(overlapped));
}


CDummy::~CDummy()
{
	
}

CDummy::CDummy(int _id)
	:id(_id) 
{
	ZeroMemory(&overlapped, sizeof(overlapped));
}

bool CDummy::ConnectServer(HANDLE* _hIOCP, SOCKADDR_IN* _addr, string _ip, int _port)
{
	// socket()
	sock = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (sock == INVALID_SOCKET) return false;

	HANDLE iocp = CreateIoCompletionPort((HANDLE)sock, *_hIOCP, id, 0);

	if ((WSAConnect(sock, (SOCKADDR*)_addr, sizeof(*_addr), NULL, NULL, NULL, NULL)) == SOCKET_ERROR) {
		printf("WSAConnect() Error!!\n");
		return false;
	}
	ZeroMemory(&overlapped, sizeof(OVERLAPPED_EX));
	overlapped.type = TYPE_RECV;
	overlapped.wsabuf.buf = overlapped.IOCPbuf;
	overlapped.wsabuf.len = sizeof(overlapped.IOCPbuf);

	DWORD flag = 0;

	int result = WSARecv(sock, &overlapped.wsabuf, 1,
		NULL, &flag, reinterpret_cast<LPWSAOVERLAPPED>(&overlapped), NULL);

	if (result == SOCKET_ERROR) {
		int error = WSAGetLastError();
		if (error != WSA_IO_PENDING) {
			printf("WSARecv() error!!\n");
			return false;
		}
	}
	player.state = CAI_Reader::getInstance().getEnd();
	Sleep(10);
	return true;
}

ACT_NUMBER CDummy::Act()
{
	auto act_num = CAI_Reader::getInstance().getNext(player.state);
	player.state = act_num;
	return static_cast<ACT_NUMBER>(*act_num);
}

void CDummy::setPos(float _x, float _y, float _z)
{
	player.pos_x = _x;
	player.pos_y = _y;
	player.pos_z = _z;
}

